<?php  
 
/* 
|-------------------------------------------------------------------------- 
| Caminho do servidor 
|-------------------------------------------------------------------------- 
| Pega automaticamente o caminho do servidor 
|  
*/ 
$base_url  = "http://" . $_SERVER['HTTP_HOST']; 
$base_url .= str_replace(basename($_SERVER['SCRIPT_NAME']), "", $_SERVER['SCRIPT_NAME']); 
define('URLCAMINHO', $base_url);// Consntante utilizada no arquivo de config do CI 
 
/* 
|-------------------------------------------------------------------------- 
| Conexões do FTP do boletoswebBisa 
|-------------------------------------------------------------------------- 
| Constantes para conexão ao ftp do boletoswebbisa 
| é usado no controller Boletos.php para a leitura do arquivo de retorno 
*/ 
 
$host = $_SERVER["HTTP_HOST"]; 
$host = explode('.',$host);

if($host[0] == 'www'){
$dominio = $host[1];	
} else {
$dominio = $host[0];
}


//var_dump($dominio);die();


define('SUBDOMINIO', $dominio);
//define('GRUPOLOJA', 'megacell'); 
define('GRUPOLOJA', 'grupocell'); 
//define('GRUPOLOJA', 'gruporeccell'); 

//define('GRUPOLOJA', 'srdoiphone'); 
 
switch ($dominio){ 

case 'localhost': 
        define('BDCAMINHO', 'grup_'.GRUPOLOJA.'_cell'); 
        define('CLIENTE', 'green');
        break; 

    case 'matriz': 
        define('BDCAMINHO', 'mixcel17_'.GRUPOLOJA.'_matriz'); 
        define('CLIENTE', 'black');
        break; 

    case 'mega': 
        define('BDCAMINHO', 'mixcel17_'.GRUPOLOJA.'_'.$host[0]); 
        define('CLIENTE', 'orange');
        break;  

    case 'carlos': 
        define('BDCAMINHO', 'mixcel17_megacell_'.$host[0]); 
        define('CLIENTE', 'yellow'); // Amarelo claro
        break;    
        
    case 'paloma': 
        define('BDCAMINHO', 'mixcel17_megacell_'.$host[0]); 
        define('CLIENTE', 'green');
        break;

	case 'multpecas': 
        define('BDCAMINHO', 'mixcel17_megacell_'.$host[0]); 
        define('CLIENTE', 'red');
        break;	

	case 'redecell': 
        define('BDCAMINHO', 'mixcel17_megacell_'.$host[0]); 
        define('CLIENTE', 'blue');
        break;		
 
    case 'mix': 
        define('BDCAMINHO', 'mixcel17_megacell_'.$host[0]); 
        define('CLIENTE', 'red-light');
        break;   
	
	case 'cell': 
        define('BDCAMINHO', 'mixcel17_megacell_'.$host[0]); 
        define('CLIENTE', 'blue-light');
        break;

    case 'sriphone': 
        define('BDCAMINHO', 'mixcel17_'.GRUPOLOJA.'_'.$host[0]); 
        define('CLIENTE', 'black');
        break;      	
	
    default: 
        define('BDCAMINHO', 'mixcel17_megacell_matriz');
        define('CLIENTE', 'red');	
       

}
