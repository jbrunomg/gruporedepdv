////////////////////////////// VENDAS ////////////////////////////////////////

function vendasAnoDetalhe(loja) {

   $('#modalBodyVendas').html('');
   $('#modalVendaLoja').html('');


    //função que exibe a imagem em gif do loading
    function loading_show(){
      $('#loadModal').fadeIn('fast');
    }
    
    //função que esconde a imagem da gif de loading
    function loading_hide(){
      $('#loadModal').fadeOut('fast');
    } 

    
    var formatter = new Intl.NumberFormat([], {
       style: 'currency',
       currency: 'BRL'
     })

     $.ajax({
            type: "POST",
            url: base_url+'sistema/vendasAnoDetalhe',
            beforeSend: function(){//Chama a função para mostrar a imagem gif de loading antes do carregamento
                  loading_show();  
                },
            data: {loja : loja},
            dataType: "json",
            success: function (resp) {
               loading_hide();
                let codeAno = '';

                 codeAno += `
                    <div class="table-responsive">
                      <table class="table no-margin">
                        <thead>
                        <tr>
                          <th>Ano</th>
                          <th>Total</th>
                          <th>Custo</th>
                          <th>Lucro</th>
                          <th>Percentual</th>
                        </tr>
                        </thead>
                        <tbody>`

                resp.map(detalheVenda => {  

                 codeAno += `
                        <tr>
                          <td>${detalheVenda.ano}</td>
                          <td>${formatter.format(detalheVenda.total).substr(3)}</td>
                          <td style="color:red;">${formatter.format(detalheVenda.custo).substr(3)}</td>
                          <td style="color:blue;">${formatter.format(detalheVenda.lucro).substr(3)}</td>
                          <td style="color:green;">${detalheVenda.percentual}%</td>`;               

                });

                codeAno += `
                       </tbody>
                     </table>
                    </div>`; 


              $('#modalBodyVendas').html(codeAno);

            }
  

       });

    $('#modalVendaLoja').html('<h4 class="modal-title">Venda Ano - '+loja+'</h4><h5>Detalhamento - Vendas.</h5>');
  }


  function vendasMesDetalhe(loja) {

    $('#modalBodyVendas').html('');
    $('#modalVendaLoja').html('');


    //função que exibe a imagem em gif do loading
    function loading_show(){
      $('#loadModal').fadeIn('fast');
    }
    
    //função que esconde a imagem da gif de loading
    function loading_hide(){
      $('#loadModal').fadeOut('fast');
    } 

    var formatter = new Intl.NumberFormat([], {
       style: 'currency',
       currency: 'BRL'
     })

     $.ajax({
            type: "POST",
            url: base_url+'sistema/vendasMesDetalhe',
            beforeSend: function(){//Chama a função para mostrar a imagem gif de loading antes do carregamento
                  loading_show();  
                },
            data: {loja : loja},
            dataType: "json",
            success: function (resp) {
                loading_hide();
                let codeMes = '';

                 codeMes += `
                    <div class="table-responsive">
                      <table class="table no-margin">
                        <thead>
                        <tr>
                          <th>Mês</th>
                          <th>Total</th>
                          <th>Custo</th>
                          <th>Lucro</th>
                          <th>Percentual</th>
                        </tr>
                        </thead>
                        <tbody>`

                resp.map(detalheVenda => {  

                 codeMes += `
                        <tr>
                          <td>${detalheVenda.mese}</td>
                          <td>${formatter.format(detalheVenda.total).substr(3)}</td>
                          <td style="color:red;">${formatter.format(detalheVenda.custo).substr(3)}</td>
                          <td style="color:blue;">${formatter.format(detalheVenda.lucro).substr(3)}</td>
                          <td style="color:green;">${detalheVenda.percentual}%</td>`;               

                });

                codeMes += `
                       </tbody>
                     </table>
                    </div>`; 


              $('#modalBodyVendas').html(codeMes);

            }
  

       });

     $('#modalVendaLoja').html('<h4 class="modal-title">Venda Mês - '+loja+'</h4><h5>Detalhamento - Vendas.</h5>');                
  }

  $('#dataVenda').click(function () {

    // Create our number formatter.
    var formatter = new Intl.NumberFormat([], {
       style: 'currency',
       currency: 'BRL'
     })

      //função que exibe a imagem em gif do loading
      function loading_show(){
        $('#load').fadeIn('fast');
      }
      
      //função que esconde a imagem da gif de loading
      function loading_hide(){
        $('#load').fadeOut('fast');
      }  
        
      if ($('#iconVenda').attr("class") === 'fa fa-plus') {

          $.ajax({
            type: "POST",
            url: base_url+'sistema/vendasDashboardv2',
            beforeSend: function(){//Chama a função para mostrar a imagem gif de loading antes do carregamento
                  loading_show();  
                },
            data: {},
            dataType: "json",
            success: function (resp) {
                loading_hide();
                let codeAno = '<h3>Vendas - Ano</h3>';
                let codeMes = '<h3>Vendas - Mês</h3>';
                let codeDia = '<h3>Vendas - Dia</h3>';

                resp['vendasAno'].map(vendas => {  

                    codeAno += `
                      <div class="box box-danger">
                        <div class="box-header with-border">
                          <h3 class="box-title" style="color: blue;">${vendas.loja}</h3>
                          <div class="box-tools pull-right">
                            <a  style="color: #333;" data-toggle="modal" data-target="#modal-vendas" onclick='vendasAnoDetalhe("${vendas.loja}");'>
                              <span class="label label-danger">Detalhe</span>
                            </a>
                          </div>
                        </div>
                        <div class="box-body">
                          <div class="row">
                            <h6>Qtd. Vendas: ${vendas.vendasQtd}</h6>
                              <input type="text" class="knob" value="${vendas.percentual * 100}" data-width="90" data-height="90" data-fgColor="${vendas.label}" data-readonly="true">                      
                          </div>
                        </div>
                        <div class="box-footer">
                          <div class="row">
                            <div class="col-sm-4 col-xs-6">
                              <div class="description-block border-right">
                              <span class="description-text">TOTAL</span>
                              <h5 class="description-header" style="color: blue">${formatter.format(vendas.valorTotal).substr(3)}</h5>
                              </div>
                            </div>
                            <div class="col-sm-4 col-xs-6">
                              <div class="description-block border-right">
                              <span class="description-text">CUSTO</span>
                              <h5 class="description-header" style="color: red">${formatter.format(vendas.custoTotal).substr(3)}</h5>                    
                              </div>
                            </div>
                            <div class="col-sm-4 col-xs-6">
                              <div class="description-block border-right">
                                <span class="description-text">LUCRO</span>
                                <h5 class="description-header" style="color: green">${formatter.format(vendas.valorTotal - vendas.custoTotal).substr(3)}</h5>  
                              </div>
                            </div>
                          </div>
                        </div>
                      </div><br>`;               

                });


                resp['vendasMes'].map(vendas => {  

                    codeMes += `
                      <div class="box box-danger">
                        <div class="box-header with-border">
                          <h3 class="box-title" style="color: blue;">${vendas.loja}</h3>
                          <div class="box-tools pull-right">
                            <a  style="color: #333;" data-toggle="modal" data-target="#modal-vendas" onclick='vendasMesDetalhe("${vendas.loja}");'>
                              <span class="label label-danger">Detalhe</span>
                            </a>
                          </div>
                        </div>
                        <div class="box-body">
                          <div class="row">
                            <h6>Qtd. Vendas: ${vendas.vendasQtd}</h6>
                                <input type="text" class="knob" value="${vendas.percentual * 100}" data-width="90" data-height="90" data-fgColor="${vendas.label}" data-readonly="true">                      
                          </div>
                        </div>
                        <div class="box-footer">
                          <div class="row">
                            <div class="col-sm-4 col-xs-6">
                              <div class="description-block border-right">
                              <span class="description-text">TOTAL</span>
                              <h5 class="description-header" style="color: blue">${formatter.format(vendas.valorTotal).substr(3)}</h5>
                              </div>
                            </div>
                            <div class="col-sm-4 col-xs-6">
                              <div class="description-block border-right">
                              <span class="description-text">CUSTO</span>
                              <h5 class="description-header" style="color: red">${formatter.format(vendas.custoTotal).substr(3)}</h5>                    
                              </div>
                            </div>
                            <div class="col-sm-4 col-xs-6">
                              <div class="description-block border-right">
                                <span class="description-text">LUCRO</span>
                                <h5 class="description-header" style="color: green">${formatter.format(vendas.valorTotal - vendas.custoTotal).substr(3)}</h5>  
                              </div>
                            </div>
                          </div>
                        </div>
                      </div><br>`;               


                });



                resp['vendasDia'].map(vendas => {  

                   console.log(vendas.label)

                          codeDia += `
                              <div class="box">
                                <div class="box-header with-border">
                                  <h3 class="box-title" style="color: blue;">${vendas.loja}</h3>
                                </div>
                                <div class="box-body">
                                  <div class="row">
                                    <h6>Qtd. Vendas: ${vendas.vendasQtd}</h6>
                                      <input type="text" class="knob" value="${vendas.percentual * 100}" data-width="90" data-height="90" data-fgColor="${vendas.label}" data-readonly="true">                      
                                  </div>
                                </div>
                                <div class="box-footer">
                                  <div class="row">
                                    <div class="col-sm-4 col-xs-6">
                                      <div class="description-block border-right">
                                      <span class="description-text">TOTAL</span>
                                      <h5 class="description-header" style="color: blue">${formatter.format(vendas.valorTotal).substr(3)}</h5>
                                      </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-6">
                                      <div class="description-block border-right">
                                      <span class="description-text">CUSTO</span>
                                      <h5 class="description-header" style="color: red">${formatter.format(vendas.custoTotal).substr(3)}</h5>                    
                                      </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-6">
                                      <div class="description-block border-right">
                                        <span class="description-text">LUCRO</span>
                                        <h5 class="description-header" style="color: green">${formatter.format(vendas.valorTotal - vendas.custoTotal).substr(3)}</h5>  
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div><br>`;               


                });




                $('#vendasAno').html(codeAno);
                $('#vendasMes').html(codeMes);
                $('#vendasDia').html(codeDia);


                $(".knob").knob({
                    draw: function () {

                      // "tron" case
                      if (this.$.data('skin') == 'tron') {

                        var a = this.angle(this.cv)  // Angle
                            , sa = this.startAngle          // Previous start angle
                            , sat = this.startAngle         // Start angle
                            , ea                            // Previous end angle
                            , eat = sat + a                 // End angle
                            , r = true;

                        this.g.lineWidth = this.lineWidth;

                        this.o.cursor
                        && (sat = eat - 0.3)
                        && (eat = eat + 0.3);

                        if (this.o.displayPrevious) {
                          ea = this.startAngle + this.angle(this.value);
                          this.o.cursor
                          && (sa = ea - 0.3)
                          && (ea = ea + 0.3);
                          this.g.beginPath();
                          this.g.strokeStyle = this.previousColor;
                          this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);
                          this.g.stroke();
                        }

                        this.g.beginPath();
                        this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
                        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);
                        this.g.stroke();

                        this.g.lineWidth = 2;
                        this.g.beginPath();
                        this.g.strokeStyle = this.o.fgColor;
                        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
                        this.g.stroke();

                        return false;
                      }
                    }
                  });
                


            }  
        });

      }else{

        $('#vendasAno').html('');
        $('#vendasMes').html('');
        $('#vendasDia').html('');

      }
  });

////////////////////////////// PRODUTOS VP ////////////////////////////////////////

  $('#dataProdutoVP').click(function () {

    // Create our number formatter.
    var formatter = new Intl.NumberFormat([], {
       style: 'currency',
       currency: 'BRL'
     })

      //função que exibe a imagem em gif do loading
      function loading_show(){
        $('#loadProdutoVP').fadeIn('fast');
      }
      
      //função que esconde a imagem da gif de loading
      function loading_hide(){
        $('#loadProdutoVP').fadeOut('fast');
      }  
        
      if ($('#iconProdutoVP').attr("class") === 'fa fa-plus') {

          $.ajax({
            type: "POST",
            url: base_url+'sistema/produtoVPDashboardv2',
            beforeSend: function(){//Chama a função para mostrar a imagem gif de loading antes do carregamento
                  loading_show();  
                },
            data: {},
            dataType: "json",
            success: function (resp) {
                loading_hide();
                let codeprodutoVP = '';

                resp['produtoVP'].map(produtoVP => {  

                  let percentual = (produtoVP.percentual * 100) > 100 ? 100 : produtoVP.percentual * 100;


                   codeprodutoVP += `
                   <div class="col-md-4 text-center">
                     <div class="box box-danger">
                        <div class="box-header with-border">
                          <h3 class="box-title" style="color: blue;">${produtoVP.loja}</h3>
                          <div class="box-tools pull-right">
                            <a  style="color: #333;" data-toggle="modal" data-target="#modal-produtoVP" onclick='produtoVPDetalhe("${produtoVP.loja}");'>
                              <span class="label label-danger">Detalhe</span>
                            </a>
                          </div>
                        </div>
                        <div class="box-body">
                          <div class="row">
                            <h6>Qtd. Produtos: ${produtoVP.estoqueQtd}</h6>
                                <input type="text" class="knob" value="${percentual}" data-width="90" data-height="90" data-fgColor="${produtoVP.label}" data-readonly="true">                      
                          </div>
                        </div>
                        <div class="box-footer">
                          <div class="row">
                            <div class="col-sm-4 col-xs-6">
                              <div class="description-block border-right">
                              <span class="description-text">TOTAL</span>
                              <h5 class="description-header" style="color: blue">${formatter.format(produtoVP.valorTotal).substr(3)}</h5>
                              </div>
                            </div>
                            <div class="col-sm-4 col-xs-6">
                              <div class="description-block border-right">
                              <span class="description-text">CUSTO</span>
                              <h5 class="description-header" style="color: red">${formatter.format(produtoVP.custoTotal).substr(3)}</h5>                    
                              </div>
                            </div>
                            <div class="col-sm-4 col-xs-6">
                              <div class="description-block border-right">
                                <span class="description-text">LUCRO</span>
                                <h5 class="description-header" style="color: green">${formatter.format(produtoVP.valorTotal - produtoVP.custoTotal).substr(3)}</h5>  
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                   </div>`;                


                });


                $('#produtoVP').html(codeprodutoVP);


                $(".knob").knob({
                    draw: function () {

                      // "tron" case
                      if (this.$.data('skin') == 'tron') {

                        var a = this.angle(this.cv)  // Angle
                            , sa = this.startAngle          // Previous start angle
                            , sat = this.startAngle         // Start angle
                            , ea                            // Previous end angle
                            , eat = sat + a                 // End angle
                            , r = true;

                        this.g.lineWidth = this.lineWidth;

                        this.o.cursor
                        && (sat = eat - 0.3)
                        && (eat = eat + 0.3);

                        if (this.o.displayPrevious) {
                          ea = this.startAngle + this.angle(this.value);
                          this.o.cursor
                          && (sa = ea - 0.3)
                          && (ea = ea + 0.3);
                          this.g.beginPath();
                          this.g.strokeStyle = this.previousColor;
                          this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);
                          this.g.stroke();
                        }

                        this.g.beginPath();
                        this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
                        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);
                        this.g.stroke();

                        this.g.lineWidth = 2;
                        this.g.beginPath();
                        this.g.strokeStyle = this.o.fgColor;
                        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
                        this.g.stroke();

                        return false;
                      }
                    }
                  });
                


            }  
        });

      }else{

        $('#produtoVP').html('');

      }
  });


function produtoVPDetalhe(loja) {

   $('#modalBodyProdutoVP').html('');
   $('#modalProdutoVPLoja').html('');


    //função que exibe a imagem em gif do loading
    function loading_show(){
      $('#loadModalProdutoVP').fadeIn('fast');
    }
    
    //função que esconde a imagem da gif de loading
    function loading_hide(){
      $('#loadModalProdutoVP').fadeOut('fast');
    } 

    
    var formatter = new Intl.NumberFormat([], {
       style: 'currency',
       currency: 'BRL'
     })

     $.ajax({
            type: "POST",
            url: base_url+'sistema/produtoVPDetalhe',
            beforeSend: function(){//Chama a função para mostrar a imagem gif de loading antes do carregamento
                  loading_show();  
                },
            data: {loja : loja},
            dataType: "json",
            success: function (resp) {
               loading_hide();
                let codeprodutoVPDetalhe = '';

                 codeprodutoVPDetalhe += `
                    <div class="table-responsive">
                      <table class="table no-margin">
                        <thead>
                        <tr>
                          <th>Grupo</th>
                          <th>Estoque</th>
                          <th>P. Custo</th>
                          <th>P. Venda</th>
                          <th>Lucro</th>
                          <th>Percentual</th>
                        </tr>
                        </thead>
                        <tbody>`

                resp.map(detalheProdutoVP => {  

                 let percentual = detalheProdutoVP.percentual === null ? 0 : detalheProdutoVP.percentual;

                 codeprodutoVPDetalhe += `
                        <tr>
                          <td>${detalheProdutoVP.produto_descricao}</td>
                          <td>${detalheProdutoVP.produto_estoque}</td>
                          <td style="color:red;">${formatter.format(detalheProdutoVP.produto_preco_custo).substr(3)}</td>
                          <td style="color:blue;">${formatter.format(detalheProdutoVP.produto_preco_venda).substr(3)}</td>
                          <td style="color:green;">${formatter.format(detalheProdutoVP.lucro).substr(3)}</td>
                          <td style="color:green;">${percentual}%</td>`;               

                });

                codeprodutoVPDetalhe += `
                       </tbody>
                     </table>
                    </div>`; 


              $('#modalBodyProdutoVP').html(codeprodutoVPDetalhe);

            }
  

       });

    $('#modalProdutoVPLoja').html('<h4 class="modal-title">Estoque Grupo - '+loja+'</h4><h5>Detalhamento - Grupo.</h5>');
  }


////////////////////////////// PRODUTOS MV ////////////////////////////////////////

  $('#dataProdutoMV').click(function () {

    // Create our number formatter.
    var formatter = new Intl.NumberFormat([], {
       style: 'currency',
       currency: 'BRL'
     })

      //função que exibe a imagem em gif do loading
      function loading_show(){
        $('#loadProdutoMV').fadeIn('fast');
      }
      
      //função que esconde a imagem da gif de loading
      function loading_hide(){
        $('#loadProdutoMV').fadeOut('fast');
      }  
        
      if ($('#iconProdutoMV').attr("class") === 'fa fa-plus') {

          $.ajax({
            type: "POST",
            url: base_url+'sistema/produtoMVDashboardv2',
            beforeSend: function(){//Chama a função para mostrar a imagem gif de loading antes do carregamento
                  loading_show();  
                },
            data: {},
            dataType: "json",
            success: function (resp) {
                loading_hide();
                let codeSemestre = '<h3>10 Produto Mais Vendidos - Semestre</h3>';
                let codeTrimestre = '<h3>10 Produto Mais Vendidos - Trimestre</h3>';
                let codeMes = '<h3>20 Produto Mais Vendidos - Mês</h3>';

                resp['produtoMVSemestre'].map(produtoMV => {  

                   codeSemestre += `
                     <div class="box box-danger">
                        <div class="box-header with-border">
                          <h3 class="box-title" style="color: blue;">${produtoMV.loja}</h3>
                          <div class="box-tools pull-right">
                            <a  style="color: #333;" data-toggle="modal" data-target="#modal-produtoMV" onclick='maisVendidoDetalheS("${produtoMV.loja}");'>
                              <span class="label label-danger">Detalhe</span>
                            </a>
                          </div>
                        </div>
                        <div class="box-body">
                          <div class="row">
                            <h6>Qtd. Produtos: ${produtoMV.qtd}</h6>
                                <input type="text" class="knob" value="${produtoMV.percentual * 100}" data-width="90" data-height="90" data-fgColor="${produtoMV.label}" data-readonly="true">                      
                          </div>
                        </div>
                        <div class="box-footer">
                          <div class="row">
                            <div class="col-sm-4 col-xs-6">
                              <div class="description-block border-right">
                              <span class="description-text">TOTAL</span>
                              <h5 class="description-header" style="color: blue">${formatter.format(produtoMV.valorTotal).substr(3)}</h5>
                              </div>
                            </div>
                            <div class="col-sm-4 col-xs-6">
                              <div class="description-block border-right">
                              <span class="description-text">CUSTO</span>
                              <h5 class="description-header" style="color: red">${formatter.format(produtoMV.custoTotal).substr(3)}</h5>                    
                              </div>
                            </div>
                            <div class="col-sm-4 col-xs-6">
                              <div class="description-block border-right">
                                <span class="description-text">LUCRO</span>
                                <h5 class="description-header" style="color: green">${formatter.format(produtoMV.valorTotal - produtoMV.custoTotal).substr(3)}</h5>  
                              </div>
                            </div>
                          </div>
                        </div>
                      </div><br>`;                


                });

                resp['produtoMVTrimestre'].map(produtoMV => {  

                   codeTrimestre += `
                      <div class="box box-danger">
                        <div class="box-header with-border">
                          <h3 class="box-title" style="color: blue;">${produtoMV.loja}</h3>
                          <div class="box-tools pull-right">
                            <a  style="color: #333;" data-toggle="modal" data-target="#modal-produtoMV" onclick='maisVendidoDetalheT("${produtoMV.loja}");'>
                              <span class="label label-danger">Detalhe</span>
                            </a>
                          </div>
                        </div>
                        <div class="box-body">
                          <div class="row">
                            <h6>Qtd. Produtos: ${produtoMV.qtd}</h6>
                                <input type="text" class="knob" value="${produtoMV.percentual * 100}" data-width="90" data-height="90" data-fgColor="${produtoMV.label}" data-readonly="true">                      
                          </div>
                        </div>
                        <div class="box-footer">
                          <div class="row">
                            <div class="col-sm-4 col-xs-6">
                              <div class="description-block border-right">
                              <span class="description-text">TOTAL</span>
                              <h5 class="description-header" style="color: blue">${formatter.format(produtoMV.valorTotal).substr(3)}</h5>
                              </div>
                            </div>
                            <div class="col-sm-4 col-xs-6">
                              <div class="description-block border-right">
                              <span class="description-text">CUSTO</span>
                              <h5 class="description-header" style="color: red">${formatter.format(produtoMV.custoTotal).substr(3)}</h5>                    
                              </div>
                            </div>
                            <div class="col-sm-4 col-xs-6">
                              <div class="description-block border-right">
                                <span class="description-text">LUCRO</span>
                                <h5 class="description-header" style="color: green">${formatter.format(produtoMV.valorTotal - produtoMV.custoTotal).substr(3)}</h5>  
                              </div>
                            </div>
                          </div>
                        </div>
                      </div><br>`;                


                });


                resp['produtoMVMes'].map(produtoMV => {  

                   codeMes += `
                      <div class="box box-danger">
                        <div class="box-header with-border">
                          <h3 class="box-title" style="color: blue;">${produtoMV.loja}</h3>
                          <div class="box-tools pull-right">
                            <a  style="color: #333;" data-toggle="modal" data-target="#modal-produtoMV" onclick='maisVendidoDetalhe("${produtoMV.loja}");'>
                              <span class="label label-danger">Detalhe</span>
                            </a>
                          </div>
                        </div>
                        <div class="box-body">
                          <div class="row">
                            <h6>Qtd. Produtos: ${produtoMV.qtd}</h6>
                                <input type="text" class="knob" value="${produtoMV.percentual * 100}" data-width="90" data-height="90" data-fgColor="${produtoMV.label}" data-readonly="true">                      
                          </div>
                        </div>
                        <div class="box-footer">
                          <div class="row">
                            <div class="col-sm-4 col-xs-6">
                              <div class="description-block border-right">
                              <span class="description-text">TOTAL</span>
                              <h5 class="description-header" style="color: blue">${formatter.format(produtoMV.valorTotal).substr(3)}</h5>
                              </div>
                            </div>
                            <div class="col-sm-4 col-xs-6">
                              <div class="description-block border-right">
                              <span class="description-text">CUSTO</span>
                              <h5 class="description-header" style="color: red">${formatter.format(produtoMV.custoTotal).substr(3)}</h5>                    
                              </div>
                            </div>
                            <div class="col-sm-4 col-xs-6">
                              <div class="description-block border-right">
                                <span class="description-text">LUCRO</span>
                                <h5 class="description-header" style="color: green">${formatter.format(produtoMV.valorTotal - produtoMV.custoTotal).substr(3)}</h5>  
                              </div>
                            </div>
                          </div>
                        </div>
                      </div><br>`;                


                });



                $('#produtoMVSemestre').html(codeSemestre);
                $('#produtoMVTrimestre').html(codeTrimestre);
                $('#produtoMVMes').html(codeMes);


                $(".knob").knob({
                    draw: function () {

                      // "tron" case
                      if (this.$.data('skin') == 'tron') {

                        var a = this.angle(this.cv)  // Angle
                            , sa = this.startAngle          // Previous start angle
                            , sat = this.startAngle         // Start angle
                            , ea                            // Previous end angle
                            , eat = sat + a                 // End angle
                            , r = true;

                        this.g.lineWidth = this.lineWidth;

                        this.o.cursor
                        && (sat = eat - 0.3)
                        && (eat = eat + 0.3);

                        if (this.o.displayPrevious) {
                          ea = this.startAngle + this.angle(this.value);
                          this.o.cursor
                          && (sa = ea - 0.3)
                          && (ea = ea + 0.3);
                          this.g.beginPath();
                          this.g.strokeStyle = this.previousColor;
                          this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);
                          this.g.stroke();
                        }

                        this.g.beginPath();
                        this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
                        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);
                        this.g.stroke();

                        this.g.lineWidth = 2;
                        this.g.beginPath();
                        this.g.strokeStyle = this.o.fgColor;
                        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
                        this.g.stroke();

                        return false;
                      }
                    }
                  });
                


            }  
        });

      }else{

        $('#produtoMVSemestre').html('');
        $('#produtoMVTrimestre').html();
        $('#produtoMVMes').html();

      }
  });


function maisVendidoDetalheS(loja) {

   $('#modalBodyProdutoMV').html('');
   $('#modalProdutoMVLoja').html('');


    //função que exibe a imagem em gif do loading
    function loading_show(){
      $('#loadModalProdutoMV').fadeIn('fast');
    }
    
    //função que esconde a imagem da gif de loading
    function loading_hide(){
      $('#loadModalProdutoMV').fadeOut('fast');
    } 

    
    var formatter = new Intl.NumberFormat([], {
       style: 'currency',
       currency: 'BRL'
     })

     $.ajax({
            type: "POST",
            url: base_url+'sistema/maisVendidoDetalheS',
            beforeSend: function(){//Chama a função para mostrar a imagem gif de loading antes do carregamento
                  loading_show();  
                },
            data: {loja : loja},
            dataType: "json",
            success: function (resp) {
               loading_hide();
                let codeSemestre = '';

                 codeSemestre += `
                    <div class="table-responsive">
                      <table class="table no-margin">
                        <thead>
                        <tr>
                          <th>Produto</th>
                          <th>Estoque</th>
                          <th>Qtd venda</th>
                          <th>Custo medio</th>
                          <th>Venda media</th>
                          <th>Lucro medio</th>
                          <th>Percentual</th>
                        </tr>
                        </thead>
                        <tbody>`

                resp.map(detalheProdutoMV => {  

                 codeSemestre += `
                        <tr>
                          <td>${detalheProdutoMV.produto_descricao}</td>
                          <td>${detalheProdutoMV.produto_descricao}</td>
                          <td>${detalheProdutoMV.qtd}</td>
                          <td style="color:red;">${formatter.format(detalheProdutoMV.custo_medio * detalheProdutoMV.qtd).substr(3)}</td>
                          <td style="color:blue;">${formatter.format(detalheProdutoMV.venda_media   * detalheProdutoMV.qtd).substr(3)}</td>
                          <td style="color:green;">${formatter.format(detalheProdutoMV.lucro_medio  * detalheProdutoMV.qtd).substr(3)}</td>
                          <td style="color:green;">${detalheProdutoMV.percentual}%</td>`;               

                });

                codeSemestre += `
                       </tbody>
                     </table>
                    </div>`; 


              $('#modalBodyProdutoMV').html(codeSemestre);

            }
  

       });

    $('#modalProdutoMVLoja').html('<h4 class="modal-title">Mais vendido - '+loja+'</h4><h5>Detalhamento Semestral - Produto.</h5>');
  }

  function maisVendidoDetalheT(loja) {

   $('#modalBodyProdutoMV').html('');
   $('#modalProdutoMVLoja').html('');


    //função que exibe a imagem em gif do loading
    function loading_show(){
      $('#loadModalProdutoMV').fadeIn('fast');
    }
    
    //função que esconde a imagem da gif de loading
    function loading_hide(){
      $('#loadModalProdutoMV').fadeOut('fast');
    } 

    
    var formatter = new Intl.NumberFormat([], {
       style: 'currency',
       currency: 'BRL'
     })

     $.ajax({
            type: "POST",
            url: base_url+'sistema/maisVendidoDetalheT',
            beforeSend: function(){//Chama a função para mostrar a imagem gif de loading antes do carregamento
                  loading_show();  
                },
            data: {loja : loja},
            dataType: "json",
            success: function (resp) {
               loading_hide();
                let codeTrimestre = '';

                 codeTrimestre += `
                    <div class="table-responsive">
                      <table class="table no-margin">
                        <thead>
                        <tr>
                          <th>Produto</th>
                          <th>Estoque</th>
                          <th>Qtd venda</th>
                          <th>Custo medio</th>
                          <th>Venda media</th>
                          <th>Lucro medio</th>
                          <th>Percentual</th>
                        </tr>
                        </thead>
                        <tbody>`

                resp.map(detalheProdutoMV => {  

                 codeTrimestre += `
                        <tr>
                          <td>${detalheProdutoMV.produto_descricao}</td>
                          <td>${detalheProdutoMV.produto_estoque}</td>
                          <td>${detalheProdutoMV.qtd}</td>
                          <td style="color:red;">${formatter.format(detalheProdutoMV.custo_medio * detalheProdutoMV.qtd).substr(3)}</td>
                          <td style="color:blue;">${formatter.format(detalheProdutoMV.venda_media   * detalheProdutoMV.qtd).substr(3)}</td>
                          <td style="color:green;">${formatter.format(detalheProdutoMV.lucro_medio  * detalheProdutoMV.qtd).substr(3)}</td>
                          <td style="color:green;">${detalheProdutoMV.percentual}%</td>`;               

                });

                codeTrimestre += `
                       </tbody>
                     </table>
                    </div>`; 


              $('#modalBodyProdutoMV').html(codeTrimestre);

            }
  

       });

    $('#modalProdutoMVLoja').html('<h4 class="modal-title">Mais vendido - '+loja+'</h4><h5>Detalhamento Trimestral - Produto.</h5>');
  }

  function maisVendidoDetalhe(loja) {

   $('#modalBodyProdutoMV').html('');
   $('#modalProdutoMVLoja').html('');


    //função que exibe a imagem em gif do loading
    function loading_show(){
      $('#loadModalProdutoMV').fadeIn('fast');
    }
    
    //função que esconde a imagem da gif de loading
    function loading_hide(){
      $('#loadModalProdutoMV').fadeOut('fast');
    } 

    
    var formatter = new Intl.NumberFormat([], {
       style: 'currency',
       currency: 'BRL'
     })

     $.ajax({
            type: "POST",
            url: base_url+'sistema/maisVendidoDetalhe',
            beforeSend: function(){//Chama a função para mostrar a imagem gif de loading antes do carregamento
                  loading_show();  
                },
            data: {loja : loja},
            dataType: "json",
            success: function (resp) {
               loading_hide();
                let codeMes = '';

                 codeMes += `
                    <div class="table-responsive">
                      <table class="table no-margin">
                        <thead>
                        <tr>
                          <th>Produto</th>
                          <th>Estoque</th>
                          <th>Qtd venda</th>
                          <th>Custo medio</th>
                          <th>Venda media</th>
                          <th>Lucro medio</th>
                          <th>Percentual</th>
                        </tr>
                        </thead>
                        <tbody>`

                resp.map(detalheProdutoMV => {  

                 codeMes += `
                        <tr>
                          <td>${detalheProdutoMV.produto_descricao}</td>
                          <td>${detalheProdutoMV.produto_estoque}</td>
                          <td>${detalheProdutoMV.qtd}</td>
                          <td style="color:red;">${formatter.format(detalheProdutoMV.custo_medio * detalheProdutoMV.qtd).substr(3)}</td>
                          <td style="color:blue;">${formatter.format(detalheProdutoMV.venda_media   * detalheProdutoMV.qtd).substr(3)}</td>
                          <td style="color:green;">${formatter.format(detalheProdutoMV.lucro_medio  * detalheProdutoMV.qtd).substr(3)}</td>
                          <td style="color:green;">${detalheProdutoMV.percentual}%</td>`;               

                });

                codeMes += `
                       </tbody>
                     </table>
                    </div>`; 


              $('#modalBodyProdutoMV').html(codeMes);

            }
  

       });

    $('#modalProdutoMVLoja').html('<h4 class="modal-title">Mais vendido - '+loja+'</h4><h5>Detalhamento Mês - Produto.</h5>');
  }


////////////////////////////// PRODUTOS PE ////////////////////////////////////////

  $('#dataProdutoPE').click(function () {

    // Create our number formatter.
    var formatter = new Intl.NumberFormat([], {
       style: 'currency',
       currency: 'BRL'
     })

      //função que exibe a imagem em gif do loading
      function loading_show(){
        $('#loadProdutoPE').fadeIn('fast');
      }
      
      //função que esconde a imagem da gif de loading
      function loading_hide(){
        $('#loadProdutoPE').fadeOut('fast');
      }  
        
      if ($('#iconProdutoPE').attr("class") === 'fa fa-plus') {

          $.ajax({
            type: "POST",
            url: base_url+'sistema/produtoPEDashboardv2',
            beforeSend: function(){//Chama a função para mostrar a imagem gif de loading antes do carregamento
                  loading_show();  
                },
            data: {},
            dataType: "json",
            success: function (resp) {
                loading_hide();
                let code180Dias = '<h3>Produtos Parados - Semestre</h3>';
                let code90Dias = '<h3>Produtos Parados - Trimestre</h3>';
                let code30Dias = '<h3>Produtos Parados - Mês</h3>';

                resp['produtoPE180Dias'].map(produtoPE => {  

                  let percentual = (produtoPE.percentual * 100) > 100 ? 100 : produtoPE.percentual * 100;

                   code180Dias += `
                      <div class="box box-danger">
                        <div class="box-header with-border">
                          <h3 class="box-title" style="color: blue;">${produtoPE.loja}</h3>
                          <div class="box-tools pull-right">
                            <a  style="color: #333;" data-toggle="modal" data-target="#modal-produtoPE" onclick='maisParadoDetalheS("${produtoPE.loja}");'>
                              <span class="label label-danger">Detalhe</span>
                            </a>
                          </div>
                        </div>
                        <div class="box-body">
                          <div class="row">
                            <h6>Qtd. Produtos: ${produtoPE.qtd}</h6>
                                <input type="text" class="knob" value="${percentual}" data-width="90" data-height="90" data-fgColor="${produtoPE.label}" data-readonly="true">                      
                          </div>
                        </div>
                        <div class="box-footer">
                          <div class="row">
                            <div class="col-sm-4 col-xs-6">
                              <div class="description-block border-right">
                              <span class="description-text">TOTAL</span>
                              <h5 class="description-header" style="color: blue">${formatter.format(produtoPE.valorTotal).substr(3)}</h5>
                              </div>
                            </div>
                            <div class="col-sm-4 col-xs-6">
                              <div class="description-block border-right">
                              <span class="description-text">CUSTO</span>
                              <h5 class="description-header" style="color: red">${formatter.format(produtoPE.custoTotal).substr(3)}</h5>                    
                              </div>
                            </div>
                            <div class="col-sm-4 col-xs-6">
                              <div class="description-block border-right">
                                <span class="description-text">LUCRO</span>
                                <h5 class="description-header" style="color: green">${formatter.format(produtoPE.valorTotal - produtoPE.custoTotal).substr(3)}</h5>  
                              </div>
                            </div>
                          </div>
                        </div>
                      </div><br>`;                


                });

                resp['produtoPE90Dias'].map(produtoPE => {  

                  let percentual = (produtoPE.percentual * 100) > 100 ? 100 : produtoPE.percentual * 100;

                   code90Dias += `
                      <div class="box box-danger">
                        <div class="box-header with-border">
                          <h3 class="box-title" style="color: blue;">${produtoPE.loja}</h3>
                          <div class="box-tools pull-right">
                            <a  style="color: #333;" data-toggle="modal" data-target="#modal-produtoPE" onclick='maisParadoDetalheT("${produtoPE.loja}");'>
                              <span class="label label-danger">Detalhe</span>
                            </a>
                          </div>
                        </div>
                        <div class="box-body">
                          <div class="row">
                            <h6>Qtd. Produtos: ${produtoPE.qtd}</h6>
                                <input type="text" class="knob" value="${percentual}" data-width="90" data-height="90" data-fgColor="${produtoPE.label}" data-readonly="true">                      
                          </div>
                        </div>
                        <div class="box-footer">
                          <div class="row">
                            <div class="col-sm-4 col-xs-6">
                              <div class="description-block border-right">
                              <span class="description-text">TOTAL</span>
                              <h5 class="description-header" style="color: blue">${formatter.format(produtoPE.valorTotal).substr(3)}</h5>
                              </div>
                            </div>
                            <div class="col-sm-4 col-xs-6">
                              <div class="description-block border-right">
                              <span class="description-text">CUSTO</span>
                              <h5 class="description-header" style="color: red">${formatter.format(produtoPE.custoTotal).substr(3)}</h5>                    
                              </div>
                            </div>
                            <div class="col-sm-4 col-xs-6">
                              <div class="description-block border-right">
                                <span class="description-text">LUCRO</span>
                                <h5 class="description-header" style="color: green">${formatter.format(produtoPE.valorTotal - produtoPE.custoTotal).substr(3)}</h5>  
                              </div>
                            </div>
                          </div>
                        </div>
                      </div><br>`;                


                });


                resp['produtoPE30Dias'].map(produtoPE => {  

                  let percentual = (produtoPE.percentual * 100) > 100 ? 100 : produtoPE.percentual * 100;

                   code30Dias += `
                      <div class="box box-danger">
                        <div class="box-header with-border">
                          <h3 class="box-title" style="color: blue;">${produtoPE.loja}</h3>
                          <div class="box-tools pull-right">
                            <a  style="color: #333;" data-toggle="modal" data-target="#modal-produtoPE" onclick='maisParadoDetalhe("${produtoPE.loja}");'>
                              <span class="label label-danger">Detalhe</span>
                            </a>
                          </div>
                        </div>
                        <div class="box-body">
                          <div class="row">
                            <h6>Qtd. Produtos: ${produtoPE.qtd}</h6>
                                <input type="text" class="knob" value="${percentual}" data-width="90" data-height="90" data-fgColor="${produtoPE.label}" data-readonly="true">                      
                          </div>
                        </div>
                        <div class="box-footer">
                          <div class="row">
                            <div class="col-sm-4 col-xs-6">
                              <div class="description-block border-right">
                              <span class="description-text">TOTAL</span>
                              <h5 class="description-header" style="color: blue">${formatter.format(produtoPE.valorTotal).substr(3)}</h5>
                              </div>
                            </div>
                            <div class="col-sm-4 col-xs-6">
                              <div class="description-block border-right">
                              <span class="description-text">CUSTO</span>
                              <h5 class="description-header" style="color: red">${formatter.format(produtoPE.custoTotal).substr(3)}</h5>                    
                              </div>
                            </div>
                            <div class="col-sm-4 col-xs-6">
                              <div class="description-block border-right">
                                <span class="description-text">LUCRO</span>
                                <h5 class="description-header" style="color: green">${formatter.format(produtoPE.valorTotal - produtoPE.custoTotal).substr(3)}</h5>  
                              </div>
                            </div>
                          </div>
                        </div>
                      </div><br>`;                


                });



                $('#produtoPE180Dias').html(code180Dias);
                $('#produtoPE90Dias').html(code90Dias);
                $('#produtoPE30Dias').html(code30Dias);


                $(".knob").knob({
                    draw: function () {

                      // "tron" case
                      if (this.$.data('skin') == 'tron') {

                        var a = this.angle(this.cv)  // Angle
                            , sa = this.startAngle          // Previous start angle
                            , sat = this.startAngle         // Start angle
                            , ea                            // Previous end angle
                            , eat = sat + a                 // End angle
                            , r = true;

                        this.g.lineWidth = this.lineWidth;

                        this.o.cursor
                        && (sat = eat - 0.3)
                        && (eat = eat + 0.3);

                        if (this.o.displayPrevious) {
                          ea = this.startAngle + this.angle(this.value);
                          this.o.cursor
                          && (sa = ea - 0.3)
                          && (ea = ea + 0.3);
                          this.g.beginPath();
                          this.g.strokeStyle = this.previousColor;
                          this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);
                          this.g.stroke();
                        }

                        this.g.beginPath();
                        this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
                        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);
                        this.g.stroke();

                        this.g.lineWidth = 2;
                        this.g.beginPath();
                        this.g.strokeStyle = this.o.fgColor;
                        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
                        this.g.stroke();

                        return false;
                      }
                    }
                  });
                


            }  
        });

      }else{

        $('#produtoPE180Dias').html('');
        $('#produtoPE90Dias').html();
        $('#produtoPE30Dias').html();

      }
  });


function maisParadoDetalheT(loja) {

   $('#modalBodyProdutoPE').html('');
   $('#modalProdutoPELoja').html('');


    //função que exibe a imagem em gif do loading
    function loading_show(){
      $('#loadModalProdutoPE').fadeIn('fast');
    }
    
    //função que esconde a imagem da gif de loading
    function loading_hide(){
      $('#loadModalProdutoPE').fadeOut('fast');
    } 

    
    var formatter = new Intl.NumberFormat([], {
       style: 'currency',
       currency: 'BRL'
     })

     $.ajax({
            type: "POST",
            url: base_url+'sistema/maisParadoDetalheT',
            beforeSend: function(){//Chama a função para mostrar a imagem gif de loading antes do carregamento
                  loading_show();  
                },
            data: {loja : loja},
            dataType: "json",
            success: function (resp) {
               loading_hide();
                let codeTrimestre = '';

                 codeTrimestre += `
                    <div class="table-responsive">
                      <table class="table no-margin">
                        <thead>
                        <tr>
                          <th>Produto</th>
                          <th>Estoque</th>
                          <th>Ult. Venda:</th>
                        </tr>
                        </thead>
                        <tbody>`

                resp.map(detalheProdutoPE => {  

                 codeTrimestre += `
                        <tr>
                          <td>${detalheProdutoPE.produto_descricao}</td>
                          <td>${detalheProdutoPE.estoque}</td>
                          <td>${detalheProdutoPE.produto_data_ultima_venda}</td>`;               

                });

                codeTrimestre += `
                       </tbody>
                     </table>
                    </div>`; 


              $('#modalBodyProdutoPE').html(codeTrimestre);

            }
  

       });

    $('#modalProdutoPELoja').html('<h4 class="modal-title">Mais Parado - '+loja+'</h4><h5>Detalhamento Trimestral - Produto.</h5>');
  }

  function maisParadoDetalhe(loja) {

   $('#modalBodyProdutoPE').html('');
   $('#modalProdutoPELoja').html('');


    //função que exibe a imagem em gif do loading
    function loading_show(){
      $('#loadModalProdutoPE').fadeIn('fast');
    }
    
    //função que esconde a imagem da gif de loading
    function loading_hide(){
      $('#loadModalProdutoPE').fadeOut('fast');
    } 

    
    var formatter = new Intl.NumberFormat([], {
       style: 'currency',
       currency: 'BRL'
     })

     $.ajax({
            type: "POST",
            url: base_url+'sistema/maisParadoDetalhe',
            beforeSend: function(){//Chama a função para mostrar a imagem gif de loading antes do carregamento
                  loading_show();  
                },
            data: {loja : loja},
            dataType: "json",
            success: function (resp) {
               loading_hide();
                let codeMes = '';

                 codeMes += `
                    <div class="table-responsive">
                      <table class="table no-margin">
                        <thead>
                        <tr>
                          <th>Produto</th>
                          <th>Estoque</th>
                          <th>Ult. Venda:</th>
                        </tr>
                        </thead>
                        <tbody>`

                resp.map(detalheProdutoPE => {  

                 codeMes += `
                        <tr>
                          <td>${detalheProdutoPE.produto_descricao}</td>
                          <td>${detalheProdutoPE.estoque}</td>
                          <td>${detalheProdutoPE.produto_data_ultima_venda}</td>`;               

                });

                codeMes += `
                       </tbody>
                     </table>
                    </div>`; 


              $('#modalBodyProdutoPE').html(codeMes);

            }
  

       });

    $('#modalProdutoPELoja').html('<h4 class="modal-title">Mais Parado - '+loja+'</h4><h5>Detalhamento Mês - Produto.</h5>');
  }

  function maisParadoDetalheS(loja) {

   $('#modalBodyProdutoPE').html('');
   $('#modalProdutoPELoja').html('');


    //função que exibe a imagem em gif do loading
    function loading_show(){
      $('#loadModalProdutoPE').fadeIn('fast');
    }
    
    //função que esconde a imagem da gif de loading
    function loading_hide(){
      $('#loadModalProdutoPE').fadeOut('fast');
    } 

    
    var formatter = new Intl.NumberFormat([], {
       style: 'currency',
       currency: 'BRL'
     })

     $.ajax({
            type: "POST",
            url: base_url+'sistema/maisParadoDetalheS',
            beforeSend: function(){//Chama a função para mostrar a imagem gif de loading antes do carregamento
                  loading_show();  
                },
            data: {loja : loja},
            dataType: "json",
            success: function (resp) {
               loading_hide();
                let codeSemestre = '';

                 codeSemestre += `
                    <div class="table-responsive">
                      <table class="table no-margin">
                        <thead>
                        <tr>
                          <th>Produto</th>
                          <th>Estoque</th>
                          <th>Ult. Venda:</th>
                        </tr>
                        </thead>
                        <tbody>`

                resp.map(detalheProdutoPE => {  

                 codeSemestre += `
                        <tr>
                          <td>${detalheProdutoPE.produto_descricao}</td>
                          <td>${detalheProdutoPE.estoque}</td>
                          <td>${detalheProdutoPE.produto_data_ultima_venda}</td>`;               

                });

                codeSemestre += `
                       </tbody>
                     </table>
                    </div>`; 


              $('#modalBodyProdutoPE').html(codeSemestre);

            }
  

       });

    $('#modalProdutoPELoja').html('<h4 class="modal-title">Mais Parado - '+loja+'</h4><h5>Detalhamento Semestral - Produto.</h5>');
  }






////////////////////////////// FINANCEIRO ////////////////////////////////////////

  $('#dataFinanceiro').click(function () {

    // Create our number formatter.
    var formatter = new Intl.NumberFormat([], {
       style: 'currency',
       currency: 'BRL'
     })

      //função que exibe a imagem em gif do loading
      function loading_show(){
        $('#loadFinanceiro').fadeIn('fast');
      }
      
      //função que esconde a imagem da gif de loading
      function loading_hide(){
        $('#loadFinanceiro').fadeOut('fast');
      }  
        
      if ($('#iconFinanceiro').attr("class") === 'fa fa-plus') {

          $.ajax({
            type: "POST",
            url: base_url+'sistema/financeiroDashboardv2',
            beforeSend: function(){//Chama a função para mostrar a imagem gif de loading antes do carregamento
                  loading_show();  
                },
            data: {},
            dataType: "json",
            success: function (resp) {
                loading_hide();
                let codeAno = '<h3>Previsão Financeira - Ano</h3>';
                let codeMes = '<h3>Previsão Financeira - Mês</h3>';

                resp['previsaoFinanceiroAno'].map(financeiro => {  

                    codeAno += `
                      <div class="box box-danger">
                        <div class="box-header with-border">
                          <h3 class="box-title" style="color: blue;">${financeiro.loja}</h3>
                          <div class="box-tools pull-right">
                            <a  style="color: #333;" data-toggle="modal" data-target="#modal-financeiro" onclick='previsaoFinanceiroDetalheAno("${financeiro.loja}");'>
                              <span class="label label-danger">Detalhe</span>
                            </a>
                          </div>
                        </div>
                        <div class="box-body">
                          <div class="row">
                            <h6>Dias Lançamento: ${financeiro.dias_lancamento}</h6>
                                <input type="text" class="knob" value="${financeiro.percentual * 100}" data-width="90" data-height="90" data-fgColor="${financeiro.label}" data-readonly="true">                      
                          </div>
                        </div>
                        <div class="box-footer">
                          <div class="row">
                            <div class="col-sm-4 col-xs-6">
                              <div class="description-block border-right">
                              <span class="description-text">RECEITA</span>
                              <h5 class="description-header" style="color: blue">${formatter.format(financeiro.receita).substr(3)}</h5>
                              </div>
                            </div>
                            <div class="col-sm-4 col-xs-6">
                              <div class="description-block border-right">
                              <span class="description-text">CUSTO</span>
                              <h5 class="description-header" style="color: red">${formatter.format(financeiro.custo).substr(3)}</h5>                    
                              </div>
                            </div>
                            <div class="col-sm-4 col-xs-6">
                              <div class="description-block border-right">
                                <span class="description-text">MEDIA</span>
                                <h5 class="description-header" style="color: green">${formatter.format(financeiro.media).substr(3)}</h5>  
                              </div>
                            </div>
                          </div>
                        </div>
                      </div><br>`;               


                });


                resp['previsaoFinanceiro'].map(financeiro => {  

                    codeMes += `
                     <div class="box box-danger">
                        <div class="box-header with-border">
                          <h3 class="box-title" style="color: blue;">${financeiro.loja}</h3>
                          <div class="box-tools pull-right">
                            <a  style="color: #333;" data-toggle="modal" data-target="#modal-financeiro" onclick='previsaoFinanceiroDetalhe("${financeiro.loja}");'>
                              <span class="label label-danger">Detalhe</span>
                            </a>
                          </div>
                        </div>
                        <div class="box-body">
                          <div class="row">
                            <h6>Dias Lançamento: ${financeiro.dias_lancamento}</h6>
                                <input type="text" class="knob" value="${financeiro.percentual * 100}" data-width="90" data-height="90" data-fgColor="${financeiro.label}" data-readonly="true">                      
                          </div>
                        </div>
                        <div class="box-footer">
                          <div class="row">
                            <div class="col-sm-4 col-xs-6">
                              <div class="description-block border-right">
                              <span class="description-text">RECEITA</span>
                              <h5 class="description-header" style="color: blue">${formatter.format(financeiro.receita).substr(3)}</h5>
                              </div>
                            </div>
                            <div class="col-sm-4 col-xs-6">
                              <div class="description-block border-right">
                              <span class="description-text">CUSTO</span>
                              <h5 class="description-header" style="color: red">${formatter.format(financeiro.custo).substr(3)}</h5>                    
                              </div>
                            </div>
                            <div class="col-sm-4 col-xs-6">
                              <div class="description-block border-right">
                                <span class="description-text">MEDIA</span>
                                <h5 class="description-header" style="color: green">${formatter.format(financeiro.media).substr(3)}</h5>  
                              </div>
                            </div>
                          </div>
                        </div>
                      </div><br>`;                  


                });


                $('#financeiroAno').html(codeAno);
                $('#financeiroMes').html(codeMes);


                $(".knob").knob({
                    draw: function () {

                      // "tron" case
                      if (this.$.data('skin') == 'tron') {

                        var a = this.angle(this.cv)  // Angle
                            , sa = this.startAngle          // Previous start angle
                            , sat = this.startAngle         // Start angle
                            , ea                            // Previous end angle
                            , eat = sat + a                 // End angle
                            , r = true;

                        this.g.lineWidth = this.lineWidth;

                        this.o.cursor
                        && (sat = eat - 0.3)
                        && (eat = eat + 0.3);

                        if (this.o.displayPrevious) {
                          ea = this.startAngle + this.angle(this.value);
                          this.o.cursor
                          && (sa = ea - 0.3)
                          && (ea = ea + 0.3);
                          this.g.beginPath();
                          this.g.strokeStyle = this.previousColor;
                          this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);
                          this.g.stroke();
                        }

                        this.g.beginPath();
                        this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
                        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);
                        this.g.stroke();

                        this.g.lineWidth = 2;
                        this.g.beginPath();
                        this.g.strokeStyle = this.o.fgColor;
                        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
                        this.g.stroke();

                        return false;
                      }
                    }
                  });
                


            }  
        });

      }else{

        $('#financeiroAno').html('');
        $('#financeiroMes').html('');

      }
  });

function previsaoFinanceiroDetalheAno(loja) {

   $('#modalBodyFinanceiro').html('');
   $('#modalFinanceiroLoja').html('');


    //função que exibe a imagem em gif do loading
    function loading_show(){
      $('#loadModalFinanceiro').fadeIn('fast');
    }
    
    //função que esconde a imagem da gif de loading
    function loading_hide(){
      $('#loadModalFinanceiro').fadeOut('fast');
    } 

    
    var formatter = new Intl.NumberFormat([], {
       style: 'currency',
       currency: 'BRL'
     })

     $.ajax({
            type: "POST",
            url: base_url+'sistema/previsaoFinanceiroDetalheAno',
            beforeSend: function(){//Chama a função para mostrar a imagem gif de loading antes do carregamento
                  loading_show();  
                },
            data: {loja : loja},
            dataType: "json",
            success: function (resp) {
               loading_hide();
                let codeAno = '';
                let dias_uteis = 300;

                 codeAno += `
                    <div class="table-responsive">
                      <table class="table no-margin">
                        <thead>`

                resp.map(detalheFinanceiro => {  

                 codeAno += `
                        <tr>
                            <th colspan="6"><center>Ano - ${detalheFinanceiro.ano}</center></th>
                        </tr>
                        <tr>
                            <th  colspan="2">Receita: ${formatter.format(detalheFinanceiro.receita).substr(3)}</th>
                            <th  colspan="2">Dias Laçamento: <span style="color:red;">${detalheFinanceiro.dias_lancamento}</span></th>
                            <th  colspan="2">Média: <span style="color:blue;">${formatter.format(detalheFinanceiro.media).substr(3)}</span></th>
                        </tr>
                        <tr>
                            <th colspan="2">Dias Ulteis: <span style="color:green;"> ${dias_uteis}</span></th>
                            <th colspan="2">Projeção:<span style="color:green;"> ${formatter.format(resp[0].media*dias_uteis).substr(3)}</span></th>
                            <th colspan="2">Despesas: <span style="color:red;">${formatter.format(detalheFinanceiro.despesa).substr(3)}</span></th>
                        </tr>
                        <tr>
                            <th colspan="2">Comissão: <span style="color:red;">${formatter.format(detalheFinanceiro.comissao).substr(3)}</span></th>
                            <th colspan="2">Lucro Bruto: <span style="color:green;">${formatter.format(detalheFinanceiro.receita-detalheFinanceiro.custo).substr(3)}</span></th>
                            <th colspan="2">Lucro Líquido: <span style="color:green;"> ${formatter.format(((detalheFinanceiro.receita-detalheFinanceiro.custo)-detalheFinanceiro.despesa)).substr(3)}</span></th>
                        </tr>
                        <tr>
                            <th colspan="6"><center>Projeção L. Líquido: <span style="color:green;">${formatter.format(((((detalheFinanceiro.receita-detalheFinanceiro.custo)-detalheFinanceiro.despesa)/detalheFinanceiro.dias_lancamento)*dias_uteis)).substr(3)}</span></center></th>
                        <tr>`;               

                });

                codeAno += `
                       </thead>
                     </table>
                    </div>`; 


              $('#modalBodyFinanceiro').html(codeAno);

            }
  

       });

    $('#modalFinanceiroLoja').html('<h4 class="modal-title">Previsão Financeira Ano - '+loja+'</h4><h5>Detalhamento - Previsão Financeira - Ano.</h5>');
  }



  function previsaoFinanceiroDetalhe(loja) {

   $('#modalBodyFinanceiro').html('');
   $('#modalFinanceiroLoja').html('');


    //função que exibe a imagem em gif do loading
    function loading_show(){
      $('#loadModalFinanceiro').fadeIn('fast');
    }
    
    //função que esconde a imagem da gif de loading
    function loading_hide(){
      $('#loadModalFinanceiro').fadeOut('fast');
    } 

    
    var formatter = new Intl.NumberFormat([], {
       style: 'currency',
       currency: 'BRL'
     })

     $.ajax({
            type: "POST",
            url: base_url+'sistema/previsaoFinanceiroDetalhe',
            beforeSend: function(){//Chama a função para mostrar a imagem gif de loading antes do carregamento
                  loading_show();  
                },
            data: {loja : loja},
            dataType: "json",
            success: function (resp) {
               loading_hide();
                let codeMes = '';

                 codeMes += `
                    <div class="table-responsive">
                      <table class="table no-margin">
                        <thead>`

              for (x = 0; x <= 5; x++) {
                          dias_uteis = 22.+x;  

                 codeMes += `
                        <tr>
                            <th colspan="6"><center>${dias_uteis} Dias</center></th>
                        </tr>
                        <tr>
                            <th  colspan="2">Receita: ${formatter.format(resp[0].receita).substr(3)}</th>
                            <th  colspan="2">Dias Laçamento: <span style="color:red;">${resp[0].dias_lancamento}</span></th>
                            <th  colspan="2">Média: <span style="color:blue;">${formatter.format(resp[0].media).substr(3)}</span></th>
                        </tr>
                        <tr>
                            <th colspan="2">Dias Ulteis: <span style="color:green;"> ${dias_uteis}</span></th>
                            <th colspan="2">Projeção:<span style="color:green;"> ${formatter.format(resp[0].media*dias_uteis).substr(3)}</span></th>
                            <th colspan="2">Despesas: <span style="color:red;">${formatter.format(resp[0].despesa).substr(3)}</span></th>
                        </tr>
                        <tr>
                            <th colspan="2">Comissão: <span style="color:red;">${formatter.format(resp[0].comissao).substr(3)}</span></th>
                            <th colspan="2">Lucro Bruto: <span style="color:green;">${formatter.format(resp[0].receita-resp[0].custo).substr(3)}</span></th>
                            <th colspan="2">Lucro Líquido: <span style="color:green;"> ${formatter.format(((resp[0].receita-resp[0].custo)-resp[0].despesa)).substr(3)}</span></th>
                        </tr>
                        <tr>
                            <th colspan="6"><center>Projeção L. Líquido: <span style="color:green;">${formatter.format(((((resp[0].receita-resp[0].custo)-resp[0].despesa)/resp[0].dias_lancamento)*dias_uteis)).substr(3)}</span></center></th>
                        <tr>`;               

                }

                codeMes += `
                       </thead>
                     </table>
                    </div>`; 


              $('#modalBodyFinanceiro').html(codeMes);

            }
  

       });

    $('#modalFinanceiroLoja').html('<h4 class="modal-title">Previsão Financeira Mês - '+loja+'</h4><h5>Detalhamento - Previsão Financeira - Mês.</h5>');
  }

  



////////////////////////////// CLIENTES ////////////////////////////////////////


$('#dataCliente').click(function () {

    // Create our number formatter.
    var formatter = new Intl.NumberFormat([], {
       style: 'currency',
       currency: 'BRL'
     })

      //função que exibe a imagem em gif do loading
      function loading_show(){
        $('#loadCliente').fadeIn('fast');
      }
      
      //função que esconde a imagem da gif de loading
      function loading_hide(){
        $('#loadCliente').fadeOut('fast');
      }  
        
      if ($('#iconCliente').attr("class") === 'fa fa-plus') {

          $.ajax({
            type: "POST",
            url: base_url+'sistema/clienteDashboardv2',
            beforeSend: function(){//Chama a função para mostrar a imagem gif de loading antes do carregamento
                  loading_show();  
                },
            data: {},
            dataType: "json",
            success: function (resp) {
                loading_hide();
                let codeAno = '<h3>Top 20 - Ano</h3>';
                let codeMes = '<h3>Top 20 - Mês</h3>';

                resp['clientetop10Ano'].map(cliente => {  

                    codeAno += `
                      <div class="box box-danger">
                          <div class="box-header with-border">
                            <h3 class="box-title" style="color: blue;">${cliente.loja}</h3>
                            <div class="box-tools pull-right">
                              <a  style="color: #333;" data-toggle="modal" data-target="#modal-cliente" onclick='top10AnoDetalhe("${cliente.loja}");'>
                                <span class="label label-danger">Detalhe</span>
                              </a>
                            </div>
                          </div>
                          <div class="box-body">
                            <div class="row">
                              <h6>Qtd. Vendas: ${cliente.qtd}</h6>
                                  <input type="text" class="knob" value="${cliente.percentual * 100}" data-width="90" data-height="90" data-fgColor="${cliente.label}" data-readonly="true">                      
                            </div>
                          </div>
                          <div class="box-footer">
                            <div class="row">
                              <div class="col-sm-4 col-xs-6">
                                <div class="description-block border-right">
                                <span class="description-text">TOTAL</span>
                                <h5 class="description-header" style="color: blue">${formatter.format(cliente.valorTotal).substr(3)}</h5>
                                </div>
                              </div>
                              <div class="col-sm-4 col-xs-6">
                                <div class="description-block border-right">
                                <span class="description-text">CUSTO</span>
                                <h5 class="description-header" style="color: red">${formatter.format(cliente.custoTotal).substr(3)}</h5>                    
                                </div>
                              </div>
                              <div class="col-sm-4 col-xs-6">
                                <div class="description-block border-right">
                                  <span class="description-text">LUCRO</span>
                                  <h5 class="description-header" style="color: green">${formatter.format(cliente.valorTotal - cliente.custoTotal).substr(3)}</h5>  
                                </div>
                              </div>
                            </div>
                          </div>
                        </div><br>`;               


                });


                resp['clientetop10Mes'].map(cliente => {  

                    codeMes += `
                      <div class="box box-danger">
                            <div class="box-header with-border">
                              <h3 class="box-title" style="color: blue;">${cliente.loja}</h3>
                              <div class="box-tools pull-right">
                                <a  style="color: #333;" data-toggle="modal" data-target="#modal-cliente" onclick='top10MesDetalhe("${cliente.loja}");'>
                                  <span class="label label-danger">Detalhe</span>
                                </a>
                              </div>
                            </div>
                            <div class="box-body">
                              <div class="row">
                                <h6>Qtd. Vendas: ${cliente.qtd}</h6>
                                    <input type="text" class="knob" value="${cliente.percentual * 100}" data-width="90" data-height="90" data-fgColor="${cliente.label}" data-readonly="true">                      
                              </div>
                            </div>
                            <div class="box-footer">
                              <div class="row">
                                <div class="col-sm-4 col-xs-6">
                                  <div class="description-block border-right">
                                  <span class="description-text">TOTAL</span>
                                  <h5 class="description-header" style="color: blue">${formatter.format(cliente.valorTotal).substr(3)}</h5>
                                  </div>
                                </div>
                                <div class="col-sm-4 col-xs-6">
                                  <div class="description-block border-right">
                                  <span class="description-text">CUSTO</span>
                                  <h5 class="description-header" style="color: red">${formatter.format(cliente.custoTotal).substr(3)}</h5>                    
                                  </div>
                                </div>
                                <div class="col-sm-4 col-xs-6">
                                  <div class="description-block border-right">
                                    <span class="description-text">LUCRO</span>
                                    <h5 class="description-header" style="color: green">${formatter.format(cliente.valorTotal - cliente.custoTotal).substr(3)}</h5>  
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div><br>`;               


                });


                $('#clienteAno').html(codeAno);
                $('#clienteMes').html(codeMes);


                $(".knob").knob({
                    draw: function () {

                      // "tron" case
                      if (this.$.data('skin') == 'tron') {

                        var a = this.angle(this.cv)  // Angle
                            , sa = this.startAngle          // Previous start angle
                            , sat = this.startAngle         // Start angle
                            , ea                            // Previous end angle
                            , eat = sat + a                 // End angle
                            , r = true;

                        this.g.lineWidth = this.lineWidth;

                        this.o.cursor
                        && (sat = eat - 0.3)
                        && (eat = eat + 0.3);

                        if (this.o.displayPrevious) {
                          ea = this.startAngle + this.angle(this.value);
                          this.o.cursor
                          && (sa = ea - 0.3)
                          && (ea = ea + 0.3);
                          this.g.beginPath();
                          this.g.strokeStyle = this.previousColor;
                          this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);
                          this.g.stroke();
                        }

                        this.g.beginPath();
                        this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
                        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);
                        this.g.stroke();

                        this.g.lineWidth = 2;
                        this.g.beginPath();
                        this.g.strokeStyle = this.o.fgColor;
                        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
                        this.g.stroke();

                        return false;
                      }
                    }
                  });
                


            }  
        });

      }else{

        $('#clienteAno').html('');
        $('#clienteMes').html('');

      }
  });

function top10AnoDetalhe(loja) {

   $('#modalBodyCliente').html('');
   $('#modalClienteLoja').html('');


    //função que exibe a imagem em gif do loading
    function loading_show(){
      $('#loadModalCliente').fadeIn('fast');
    }
    
    //função que esconde a imagem da gif de loading
    function loading_hide(){
      $('#loadModalCliente').fadeOut('fast');
    } 

    
    var formatter = new Intl.NumberFormat([], {
       style: 'currency',
       currency: 'BRL'
     })

     $.ajax({
            type: "POST",
            url: base_url+'sistema/top10AnoDetalhe',
            beforeSend: function(){//Chama a função para mostrar a imagem gif de loading antes do carregamento
                  loading_show();  
                },
            data: {loja : loja},
            dataType: "json",
            success: function (resp) {
               loading_hide();
                let codeAno = '';

                 codeAno += `
                    <div class="table-responsive">
                      <table class="table no-margin">
                        <thead>
                        <tr>
                          <th>Cliente</th>
                          <th>Qtd. Venda</th>
                          <th>P. Custo</th>
                          <th>P. Venda</th>
                          <th>Lucro</th>
                          <th>Percentual</th>
                        </tr>
                        </thead>
                        <tbody>`

                resp.map(detalheCliente => {  

                 codeAno += `
                        <tr>
                          <td>${detalheCliente.cliente_nome}</td>
                          <td>${detalheCliente.qtdVenda}</td>
                          <td style="color:red;">${formatter.format(detalheCliente.custoTotal).substr(3)}</td>
                          <td style="color:red;">${formatter.format(detalheCliente.valorTotal).substr(3)}</td>
                          <td style="color:blue;">${formatter.format(detalheCliente.lucro).substr(3)}</td>
                          <td style="color:green;">${detalheCliente.percentual}%</td>`;               

                });

                codeAno += `
                       </tbody>
                     </table>
                    </div>`; 


              $('#modalBodyCliente').html(codeAno);

            }
  

       });

    $('#modalClienteLoja').html('<h4 class="modal-title">Clientes top 20 - Ano - '+loja+'</h4><h5>Detalhamento - Clientes.</h5>');
  }

  function top10MesDetalhe(loja) {

   $('#modalBodyCliente').html('');
   $('#modalClienteLoja').html('');


    //função que exibe a imagem em gif do loading
    function loading_show(){
      $('#loadModalCliente').fadeIn('fast');
    }
    
    //função que esconde a imagem da gif de loading
    function loading_hide(){
      $('#loadModalCliente').fadeOut('fast');
    } 

    
    var formatter = new Intl.NumberFormat([], {
       style: 'currency',
       currency: 'BRL'
     })

     $.ajax({
            type: "POST",
            url: base_url+'sistema/top10MesDetalhe',
            beforeSend: function(){//Chama a função para mostrar a imagem gif de loading antes do carregamento
                  loading_show();  
                },
            data: {loja : loja},
            dataType: "json",
            success: function (resp) {
               loading_hide();
                let codeMes = '';

                 codeMes += `
                    <div class="table-responsive">
                      <table class="table no-margin">
                        <thead>
                        <tr>
                          <th>Cliente</th>
                          <th>Qtd. Venda</th>
                          <th>P. Custo</th>
                          <th>P. Venda</th>
                          <th>Lucro</th>
                          <th>Percentual</th>
                        </tr>
                        </thead>
                        <tbody>`

                resp.map(detalheCliente => {  

                 codeMes += `
                        <tr>
                          <td>${detalheCliente.cliente_nome}</td>
                          <td>${detalheCliente.qtdVenda}</td>
                          <td style="color:red;">${formatter.format(detalheCliente.custoTotal).substr(3)}</td>
                          <td style="color:red;">${formatter.format(detalheCliente.valorTotal).substr(3)}</td>
                          <td style="color:blue;">${formatter.format(detalheCliente.lucro).substr(3)}</td>
                          <td style="color:green;">${detalheCliente.percentual}%</td>`;               

                });

                codeMes += `
                       </tbody>
                     </table>
                    </div>`; 


              $('#modalBodyCliente').html(codeMes);

            }
  

       });

    $('#modalClienteLoja').html('<h4 class="modal-title">Clientes top 20 - Mês - '+loja+'</h4><h5>Detalhamento - Clientes.</h5>');
  }



////////////////////////////// VENDEDOR ////////////////////////////////////////

$('#dataVendedor').click(function () {

    // Create our number formatter.
    var formatter = new Intl.NumberFormat([], {
       style: 'currency',
       currency: 'BRL'
     })

      //função que exibe a imagem em gif do loading
      function loading_show(){
        $('#loadVendedor').fadeIn('fast');
      }
      
      //função que esconde a imagem da gif de loading
      function loading_hide(){
        $('#loadVendedor').fadeOut('fast');
      }  
        
      if ($('#iconVendedor').attr("class") === 'fa fa-plus') {

          $.ajax({
            type: "POST",
            url: base_url+'sistema/vendedorDashboardv2',
            beforeSend: function(){//Chama a função para mostrar a imagem gif de loading antes do carregamento
                  loading_show();  
                },
            data: {},
            dataType: "json",
            success: function (resp) {
                loading_hide();
                let codeAno = '<h3>Top 10 - Ano</h3>';
                let codeMes = '<h3>Top 10 - Mês</h3>';
                let codeDia = '<h3>Top 10 - Dia</h3>';

                resp['vendedortop10Ano'].map(vendedor => {  

                    codeAno += `
                        <div class="box box-danger">
                                <div class="box-header with-border">
                                  <h3 class="box-title" style="color: blue;">${vendedor.loja}</h3>
                                  <div class="box-tools pull-right">
                                    <a  style="color: #333;" data-toggle="modal" data-target="#modal-vendedor" onclick='vendedortop10AnoDetalhe("${vendedor.loja}");'>
                                      <span class="label label-danger">Detalhe</span>
                                    </a>
                                  </div>
                                </div>
                                <div class="box-body">
                                  <div class="row">
                                    <h6>Qtd. Vendas: ${vendedor.qtd}</h6>
                                        <input type="text" class="knob" value="${vendedor.percentual * 100}" data-width="90" data-height="90" data-fgColor="${vendedor.label}" data-readonly="true">                      
                                  </div>
                                </div>
                                <div class="box-footer">
                                  <div class="row">
                                    <div class="col-sm-4 col-xs-6">
                                      <div class="description-block border-right">
                                      <span class="description-text">TOTAL</span>
                                      <h5 class="description-header" style="color: blue">${formatter.format(vendedor.valorTotal).substr(3)}</h5>
                                      </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-6">
                                      <div class="description-block border-right">
                                      <span class="description-text">CUSTO</span>
                                      <h5 class="description-header" style="color: red">${formatter.format(vendedor.custoTotal).substr(3)}</h5>                    
                                      </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-6">
                                      <div class="description-block border-right">
                                        <span class="description-text">LUCRO</span>
                                        <h5 class="description-header" style="color: green">${formatter.format(vendedor.valorTotal - vendedor.custoTotal).substr(3)}</h5>  
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </div><br>`;               


                });


                resp['vendedortop10Mes'].map(vendedor => {  

                    codeMes += `
                        <div class="box box-danger">
                                  <div class="box-header with-border">
                                    <h3 class="box-title" style="color: blue;">${vendedor.loja}</h3>
                                    <div class="box-tools pull-right">
                                      <a  style="color: #333;" data-toggle="modal" data-target="#modal-vendedor" onclick='vendedortop10MesDetalhe("${vendedor.loja}");'>
                                        <span class="label label-danger">Detalhe</span>
                                      </a>
                                    </div>
                                  </div>
                                  <div class="box-body">
                                    <div class="row">
                                      <h6>Qtd. Vendas: ${vendedor.qtd}</h6>
                                          <input type="text" class="knob" value="${vendedor.percentual * 100}" data-width="90" data-height="90" data-fgColor="${vendedor.label}" data-readonly="true">                      
                                    </div>
                                  </div>
                                  <div class="box-footer">
                                    <div class="row">
                                      <div class="col-sm-4 col-xs-6">
                                        <div class="description-block border-right">
                                        <span class="description-text">TOTAL</span>
                                        <h5 class="description-header" style="color: blue">${formatter.format(vendedor.valorTotal).substr(3)}</h5>
                                        </div>
                                      </div>
                                      <div class="col-sm-4 col-xs-6">
                                        <div class="description-block border-right">
                                        <span class="description-text">CUSTO</span>
                                        <h5 class="description-header" style="color: red">${formatter.format(vendedor.custoTotal).substr(3)}</h5>                    
                                        </div>
                                      </div>
                                      <div class="col-sm-4 col-xs-6">
                                        <div class="description-block border-right">
                                          <span class="description-text">LUCRO</span>
                                          <h5 class="description-header" style="color: green">${formatter.format(vendedor.valorTotal - vendedor.custoTotal).substr(3)}</h5>  
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                              </div><br>`;               


                });

                 resp['vendedortop10Dia'].map(vendedor => {  

                    codeDia += `
                        <div class="box box-danger">
                                  <div class="box-header with-border">
                                    <h3 class="box-title" style="color: blue;">${vendedor.loja}</h3>
                                    <div class="box-tools pull-right">
                                      <a  style="color: #333;" data-toggle="modal" data-target="#modal-vendedor" onclick='vendedortop10DiaDetalhe("${vendedor.loja}");'>
                                        <span class="label label-danger">Detalhe</span>
                                      </a>
                                    </div>
                                  </div>
                                  <div class="box-body">
                                    <div class="row">
                                      <h6>Qtd. Vendas: ${vendedor.qtd}</h6>
                                          <input type="text" class="knob" value="${vendedor.percentual * 100}" data-width="90" data-height="90" data-fgColor="${vendedor.label}" data-readonly="true">                      
                                    </div>
                                  </div>
                                  <div class="box-footer">
                                    <div class="row">
                                      <div class="col-sm-4 col-xs-6">
                                        <div class="description-block border-right">
                                        <span class="description-text">TOTAL</span>
                                        <h5 class="description-header" style="color: blue">${formatter.format(vendedor.valorTotal).substr(3)}</h5>
                                        </div>
                                      </div>
                                      <div class="col-sm-4 col-xs-6">
                                        <div class="description-block border-right">
                                        <span class="description-text">CUSTO</span>
                                        <h5 class="description-header" style="color: red">${formatter.format(vendedor.custoTotal).substr(3)}</h5>                    
                                        </div>
                                      </div>
                                      <div class="col-sm-4 col-xs-6">
                                        <div class="description-block border-right">
                                          <span class="description-text">LUCRO</span>
                                          <h5 class="description-header" style="color: green">${formatter.format(vendedor.valorTotal - vendedor.custoTotal).substr(3)}</h5>  
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                              </div><br>`;               


                });


                $('#vendedorAno').html(codeAno);
                $('#vendedorMes').html(codeMes);
                $('#vendedorDia').html(codeDia);


                $(".knob").knob({
                    draw: function () {

                      // "tron" case
                      if (this.$.data('skin') == 'tron') {

                        var a = this.angle(this.cv)  // Angle
                            , sa = this.startAngle          // Previous start angle
                            , sat = this.startAngle         // Start angle
                            , ea                            // Previous end angle
                            , eat = sat + a                 // End angle
                            , r = true;

                        this.g.lineWidth = this.lineWidth;

                        this.o.cursor
                        && (sat = eat - 0.3)
                        && (eat = eat + 0.3);

                        if (this.o.displayPrevious) {
                          ea = this.startAngle + this.angle(this.value);
                          this.o.cursor
                          && (sa = ea - 0.3)
                          && (ea = ea + 0.3);
                          this.g.beginPath();
                          this.g.strokeStyle = this.previousColor;
                          this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);
                          this.g.stroke();
                        }

                        this.g.beginPath();
                        this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
                        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);
                        this.g.stroke();

                        this.g.lineWidth = 2;
                        this.g.beginPath();
                        this.g.strokeStyle = this.o.fgColor;
                        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
                        this.g.stroke();

                        return false;
                      }
                    }
                  });
                


            }  
        });

      }else{

        $('#vendedorAno').html('');
        $('#vendedorMes').html('');
        $('#vendedorDia').html('');

      }
  });


function vendedortop10AnoDetalhe(loja) {

   $('#modalBodyVendedor').html('');
   $('#modalVendedorLoja').html('');


    //função que exibe a imagem em gif do loading
    function loading_show(){
      $('#loadModalVendedor').fadeIn('fast');
    }
    
    //função que esconde a imagem da gif de loading
    function loading_hide(){
      $('#loadModalVendedor').fadeOut('fast');
    } 

    
    var formatter = new Intl.NumberFormat([], {
       style: 'currency',
       currency: 'BRL'
     })

     $.ajax({
            type: "POST",
            url: base_url+'sistema/vendedortop10AnoDetalhe',
            beforeSend: function(){//Chama a função para mostrar a imagem gif de loading antes do carregamento
                  loading_show();  
                },
            data: {loja : loja},
            dataType: "json",
            success: function (resp) {
               loading_hide();
                let codeAno = '';

                 codeAno += `
                    <div class="table-responsive">
                      <table class="table no-margin">
                        <thead>
                        <tr>
                          <th>Funcionário</th>
                          <th>Qtd. Venda</th>
                          <th>P. Custo</th>
                          <th>P. Venda</th>
                          <th>Lucro</th>
                          <th>Percentual</th>
                        </tr>
                        </thead>
                        <tbody>`

                resp.map(detalheVendedor => {  

                 codeAno += `
                        <tr>
                          <td>${detalheVendedor.vendedor}</td>
                          <td>${detalheVendedor.qtdVenda}</td>
                          <td style="color:red;">${formatter.format(detalheVendedor.custoTotal).substr(3)}</td>
                          <td style="color:red;">${formatter.format(detalheVendedor.valorTotal).substr(3)}</td>
                          <td style="color:blue;">${formatter.format(detalheVendedor.lucro).substr(3)}</td>
                          <td style="color:green;">${detalheVendedor.percentual}%</td>`;               

                });

                codeAno += `
                       </tbody>
                     </table>
                    </div>`; 


              $('#modalBodyVendedor').html(codeAno);

            }
  

       });

    $('#modalVendedorLoja').html('<h4 class="modal-title">Funcionário Destaque Ano - '+loja+'</h4><h5>Detalhamento - Funcionário.</h5>');
  }


  function vendedortop10MesDetalhe(loja) {

   $('#modalBodyVendedor').html('');
   $('#modalVendedorLoja').html('');


    //função que exibe a imagem em gif do loading
    function loading_show(){
      $('#loadModalVendedor').fadeIn('fast');
    }
    
    //função que esconde a imagem da gif de loading
    function loading_hide(){
      $('#loadModalVendedor').fadeOut('fast');
    } 

    
    var formatter = new Intl.NumberFormat([], {
       style: 'currency',
       currency: 'BRL'
     })

     $.ajax({
            type: "POST",
            url: base_url+'sistema/vendedortop10MesDetalhe',
            beforeSend: function(){//Chama a função para mostrar a imagem gif de loading antes do carregamento
                  loading_show();  
                },
            data: {loja : loja},
            dataType: "json",
            success: function (resp) {
               loading_hide();
                let codeMes = '';

                 codeMes += `
                    <div class="table-responsive">
                      <table class="table no-margin">
                        <thead>
                        <tr>
                          <th>Funcionário</th>
                          <th>Qtd. Venda</th>
                          <th>P. Custo</th>
                          <th>P. Venda</th>
                          <th>Lucro</th>
                          <th>Percentual</th>
                        </tr>
                        </thead>
                        <tbody>`

                resp.map(detalheVendedor => {  

                 codeMes += `
                        <tr>
                          <td>${detalheVendedor.vendedor}</td>
                          <td>${detalheVendedor.qtdVenda}</td>
                          <td style="color:red;">${formatter.format(detalheVendedor.custoTotal).substr(3)}</td>
                          <td style="color:red;">${formatter.format(detalheVendedor.valorTotal).substr(3)}</td>
                          <td style="color:blue;">${formatter.format(detalheVendedor.lucro).substr(3)}</td>
                          <td style="color:green;">${detalheVendedor.percentual}%</td>`;               

                });

                codeMes += `
                       </tbody>
                     </table>
                    </div>`; 


              $('#modalBodyVendedor').html(codeMes);

            }
  

       });

    $('#modalVendedorLoja').html('<h4 class="modal-title">Funcionário Destaque Mês - '+loja+'</h4><h5>Detalhamento - Funcionário.</h5>');
  }


  function vendedortop10DiaDetalhe(loja) {

   $('#modalBodyVendedor').html('');
   $('#modalVendedorLoja').html('');


    //função que exibe a imagem em gif do loading
    function loading_show(){
      $('#loadModalVendedor').fadeIn('fast');
    }
    
    //função que esconde a imagem da gif de loading
    function loading_hide(){
      $('#loadModalVendedor').fadeOut('fast');
    } 

    
    var formatter = new Intl.NumberFormat([], {
       style: 'currency',
       currency: 'BRL'
     })

     $.ajax({
            type: "POST",
            url: base_url+'sistema/vendedortop10DiaDetalhe',
            beforeSend: function(){//Chama a função para mostrar a imagem gif de loading antes do carregamento
                  loading_show();  
                },
            data: {loja : loja},
            dataType: "json",
            success: function (resp) {
               loading_hide();
                let codeDia = '';

                 codeDia += `
                    <div class="table-responsive">
                      <table class="table no-margin">
                        <thead>
                        <tr>
                          <th>Funcionário</th>
                          <th>Qtd. Venda</th>
                          <th>P. Custo</th>
                          <th>P. Venda</th>
                          <th>Lucro</th>
                          <th>Percentual</th>
                        </tr>
                        </thead>
                        <tbody>`

                resp.map(detalheVendedor => {  

                 codeDia += `
                        <tr>
                          <td>${detalheVendedor.vendedor}</td>
                          <td>${detalheVendedor.qtdVenda}</td>
                          <td style="color:red;">${formatter.format(detalheVendedor.custoTotal).substr(3)}</td>
                          <td style="color:red;">${formatter.format(detalheVendedor.valorTotal).substr(3)}</td>
                          <td style="color:blue;">${formatter.format(detalheVendedor.lucro).substr(3)}</td>
                          <td style="color:green;">${detalheVendedor.percentual}%</td>`;               

                });

                codeDia += `
                       </tbody>
                     </table>
                    </div>`; 


              $('#modalBodyVendedor').html(codeDia);

            }
  

       });

    $('#modalVendedorLoja').html('<h4 class="modal-title">Funcionário Destaque Dia - '+loja+'</h4><h5>Detalhamento - Funcionário.</h5>');
  }

