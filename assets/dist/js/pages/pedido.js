//Pedido

$('#grupo_prod_pedido').change(function(){
   
    var grupo = $('#grupo_prod_pedido').val();
    $.ajax({
     method: "POST",
     url: base_url+"pedido/selecionarProduto/",
     dataType: "html",
     data: { grupo: grupo}
     })
     .done(function( response ) {
 
     $('#produto_desc').html(response);
     $('#produto_desc').removeAttr("disabled", "disabled" );
     
     });
    
 });
 
 $('#produto_desc').change(function(){
    
    var produto = $('#produto_desc').val();
    $.ajax({
     method: "POST",
     url: base_url+"pedido/selecionarProdutoID/",
     dataType: "html",
     data: { produto: produto}
     })
     .done(function( response ) {
       console.log(response);
     $('#valord').html(response);
     $('#valord').removeAttr("disabled", "disabled" );    
     
     });
    
 });

 //Pedido x Venda

$('#btnAdicionarProdutoPedido').click(function(){

    var id         = $('#idPedidos').val();
        quantidade = $('#quantidade').val();
        subTotal   = $('#subTotal').val();
        produto    = $('#produto_desc').val();
  
       $.ajax({
            type: "POST",
            url: base_url+"pedido/adicionarProduto",
            data: { idPedidos: id,
                    quantidade: quantidade,
                    subTotal: subTotal,
                    produto_id: produto },
            dataType: 'json',
            success: function(data)
            {
              if(data.result == true){
                  location.reload('#divProdutos');
                  // $("#divProdutos" ).load("<?php echo current_url();?> #divProdutos" );
                  // $("#quantidade").val('');
                  // // $("#precoUnitario").val('');
                  // $("#produto").val('').focus();
              }
              else{
                  alert('Ocorreu um erro ao tentar adicionar produto. Tente Novamente!');
              }
            }
            });
  
  
  });


  $('#btnVendaPedido').click(function(){

    var id         = $('#idPedidos').val();
        total      = $('#total').val();
        cliente    = $('#cliente_id').val();
  
  
       $.ajax({
            type: "POST",
            url: base_url+"pedido/VendaPedido",
            data: { idPedidos: id,
                    total: total,
                    cliente: cliente,
                  },
            dataType: 'json',
            success: function(data)
            {
                if(data.result == true){
                    if (base_subdominio == 'localhost' || base_grupoloja == 'grupocell' || base_grupoloja == 'gruporeccell' || base_grupoloja == 'grupobyte' ){
                        window.location.replace(base_url+"Pdvtablet/finalizarVenda/"+data.venda);
                    }else{
                        window.location.replace(base_url+"pdv2/finalizarVenda/"+data.venda);
                    }
                }else{
                  alert('Os itens marcardos de vermelho não pode ser transformado em venda!.');
                }
            }
            });
  
  
  });