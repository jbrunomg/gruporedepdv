window.addEventListener('online', () => cnxMostrarOnOrOff(true));
window.addEventListener('offline', () => cnxMostrarOnOrOff(false));

function cnxMostrarOnOrOff(on = true) {
    document.getElementById("taja-cnx-geral").style.display = "block";
    if (on === true) {
        document.getElementById("taxa-cnx-off").style.display = "none";
        document.getElementById("taxa-cnx-on").style.display = "block";
    } else {
        document.getElementById("taxa-cnx-on").style.display = "none";
        document.getElementById("taxa-cnx-off").style.display = "block";
    }
}

document.getElementById("taja-cnx-geral").addEventListener("click", () => {
    document.getElementById("taja-cnx-geral").style.display = "none";
});
