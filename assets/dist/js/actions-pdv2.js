
$(document).ready(function(){

  window.onhelp = function() {
    return false;
  };
  window.onkeydown = evt => {
    switch (evt.keyCode) {
      //ESC
      case 113:
        iniciarVenda();
        break;
      case 114:
        excluirItens();
        break;
      case 115:
        cancelarVenda();
        break;
      case 116:
        finalizarVenda();
        break;
      case 121:
        sairVenda();
        break;
      default:
        return true;
    }
    //Returning false overrides default browser event
    return false;
  };


  // $("#addCliente").attr("disabled", true);
   
   // $.ajax({
   //  method: "POST",
   //  url: base_url+"pdv2/selecionarCliente/",
   //  dataType: "html"
   //  })
   //  .done(function( response ) {
   //  $('#cliente_nome').html(response);
   //  $('#cliente_nome').removeAttr("disabled", "disabled" );    
    
   //  });

  //   var loja = $(".filialAtual").text();
  //   switch (loja) {
  //   case 'MATRIZ':
  //     $('.filialAtual').css('background','#a349a4');
  //     break;
  //   case 'CARLOS':
  //     $('.filialAtual').css('background','#f39c12');
  //     break;
  //   case 'GAME':
  //     $('.filialAtual').css('background','#00a2e8');
  //     break;
  //   case 'MEGA':
  //     $('.filialAtual').css('background','#ff7f27');
  //     break;
  //   case 'PALOMA':
  //     $('.filialAtual').css('background','#22b14c');
  //     break;
  // }
   

});

var idItens  = $('#idItens').val();

if (idItens == 0) {
    $("#btn-finalizar-venda").attr("disabled", true);
}else{
    $("#btn-finalizar-venda").attr("disabled", false);
}

function iniciarVenda() {
  incluirVenda(); // Criar uma venda para pegar um ID
  $('#venda-iniciada').show();
  $('#btn-iniciar-venda').prop('disabled', true);
  $('#btn-excluir-item').prop('disabled', false);
  $('#btn-cancelar-venda').prop('disabled', false);
  $('#btn-finalizar-venda').prop('disabled', false);
  $('#btn-sair').prop('disabled', false);
  document.getElementById('grupo_prod').focus();
}

function cancelarVenda() {
  // $('#venda-iniciada').hide();
  // $('#btn-iniciar-venda').prop('disabled', false);
  // $('#btn-excluir-item').prop('disabled', true);
  // $('#btn-cancelar-venda').prop('disabled', true);
  // $('#btn-finalizar-venda').prop('disabled', true);
  // $('#btn-sair').prop('disabled', true);

  var venda  = $('#idVenda').val();
  

  $.ajax({
    method: "POST",
    url: base_url+"pdv2/cancelarVenda/",
    dataType: "JSON",
    data: { venda: venda  },
    success: function(data)
     {
       if(data.result == true){
         location.reload($("#grupo_prod").focus());
       }
       else{
         alert('Ocorreu um erro tente novamente!');
       }
     }
    
  });
}

function finalizarVenda() {
  //$('#modal-default').modal()
  
  var venda  = $('#idVenda').val();

  window.location.assign(base_url+"pdv2/finalizarVenda/"+venda);

 

   // $.ajax({
   //  method: "POST",
   //  url: base_url+"pdv2/finalizarVenda/",
   //  dataType: "JSON",
   //  data: { venda: venda}
   //  })
   //  .done(function( response ) {
   //    console.log(response);
   //  //$("#idVenda").text(response);
   //  window.location.assign(base_url+"pdv2/editarVenda/"+response);
   //  //document.getElementById('grupo_prod').focus();
   //  });

}


function excluirItens() {
  var venda     = $('#idVenda').val();
  var idItens   = $('#idItens').val();
  var lojaItens = $('#lojaItens').val(); 

   $.ajax({
      method: "POST",
      url: base_url+"pdv2/excluirItens/",
      dataType: "JSON",
      data: { venda: venda, idItens: idItens, lojaItens: lojaItens },
      success: function(data)
       {
         if(data.result == true){
           location.reload($("#grupo_prod").focus());
         }
         else{
           alert('Ocorreu um erro tente novamente!');
         }
       }
      
    });
}


function incluirVenda() {

   var clie = 1;

   $.ajax({
    method: "POST",
    url: base_url+"pdv2/adicionarVenda/",
    dataType: "JSON",
    data: { clie: clie}
    })
    .done(function( response ) {
      // console.log(response);
    //$("#idVenda").text(response);
    window.location.assign(base_url+"pdv2/editarVenda/"+response);
    document.getElementById('grupo_prod').focus();
    });

}

function sairVenda() {
  var venda  = $('#idVenda').val();

   $.ajax({
      method: "POST",
      url: base_url+"pdv2/sairVenda/",
      dataType: "JSON",
      data: { venda: venda},
      success: function(data)
       {
         if(data.result == true){
           window.location.assign(base_url+"pdv2/");
         }
         else{
           alert('Ocorreu um erro tente novamente!');
         }
       }
      
    });
}

function sairVendaDashboard() {
  var venda  = $('#idVenda').val();

   $.ajax({
      method: "POST",
      url: base_url+"pdv2/sairVenda/",
      dataType: "JSON",
      data: { venda: venda},
      success: function(data)
       {
         if(data.result == true){
           window.location.assign(base_url+"sistema/dashboard");
         }
         else{
           alert('Ocorreu um erro tente novamente!');
         }
       }
      
    });
}

$('#grupo_prod').blur(function(){
   
   var grupo = $('#grupo_prod').val();
   var filial = $('#filialAtual').val();

   $.ajax({
    method: "POST",
    url: base_url+"pdv2/selecionarProduto/",
    dataType: "html",
    data: { grupo: grupo, filial: filial}
    })
    .done(function( response ) {

    $('#produto_desc').html(response);
    $('#produto_desc').removeAttr("disabled", "disabled" );
    //document.getElementById('prod_desc').focus();
    $('#produto_desc').select2('open');
    
    });
   
});

$('#produto_desc').change(function(){
   
   var prod = $('#produto_desc').val();
   var filial = $('#filialAtual').val();

   $.ajax({
    method: "POST",
    url: base_url+"pdv2/estoqueProdutoId/",
    dataType: "JSON",
    data: { produto: prod, filial: filial}
    })
    .done(function( response ) {
    $("#estoque").text(response[0].produto_estoque);    
    $("#valor_unit").val(response[0].produto_preco_venda);
    $("#precovenda").val(response[0].produto_preco_venda);
    $("#precocusto").val(response[0].produto_preco_custo);
    $("#pvm").text(response[0].produto_preco_minimo_venda);
    $("#pcd").text(response[0].produto_preco_cart_debito);
    $("#pcc").text(response[0].produto_preco_cart_credito);
    $("#codigoProduto").val(response[0].produto_codigo);
    
   // document.getElementById('quantidade').focus();
    $("#quantidade").focus();
    // $('#quantidade').select2('open');
    
    });


$('#quantidade').blur(function(){
   
   var quantidade = $('#quantidade').val();
   var valor = $('#valor_unit').val();
   var total  = parseFloat(quantidade) * parseFloat(valor);

   if (parseFloat(valor) > 0) {
     $('#liberar-senha').val('senha'); 
   }

   $('#total-item').val(total.toFixed(2));
   
});

$('#valor_unit').blur(function(){
   var quantidade = $('#quantidade').val();
   var valor = $('#valor_unit').val();
   var total  = parseFloat(quantidade) * parseFloat(valor);
   
   if (parseFloat(valor) > 0) {
     $('#liberar-senha').val('senha'); 
   }


   $('#total-item').val(total.toFixed(2));

});

$('#quantidade').blur(function(){
   
   var estoque = $('#estoque').text();

  if (estoque == '0') {

    $("#aviso_estoque").css("display", "block"); 

  }else{

    $("#aviso_estoque").css("display", "none");
  }
   
});
     
});


$("#cliente_nome").autocomplete({
  source: base_url+"pdv2/autoCompleteClientes",
  minLength: 2,
  select: function( event, ui ) {    
    $("#cliente_id").val(ui.item.id);   
  }
});

$("#usuario_nome").autocomplete({
  source: base_url+"pdv2/autoCompleteUsuarios",
  minLength: 2,
  select: function( event, ui ) {    
    $("#usuario_id").val(ui.item.id);   
  }
});


$('#desconto').blur(function(){
   
   var desconto = $('#desconto').val();
   if (desconto) {  
     var total  = $('#totalGeral').val();
     // var cal  = parseFloat(total)/100 * parseFloat(desconto);
     var totalGeral = parseFloat(total) - parseFloat(desconto);
     $('#totalGeral').val(totalGeral.toFixed(2));
   }
   
});

$('#valorPG').blur(function(){
   
   var total = $('#totalGeral').val();
   var pago  = $('#valorPG').val();
   var pago2  = $('#forma_pag2_valor').val();
   var troco = (parseFloat(pago) + parseFloat(pago2)) - parseFloat(total);

   $('#troco').val(troco.toFixed(2));
   document.getElementById('forma_pag').focus();
   
});

$('#valorPG').blur(function(){
   
  var troco = $('#troco').val();
  
  if (parseFloat(troco) < 0) {
    $("#aviso_troco").css("display", "block"); 
   }else{
    $("#aviso_troco").css("display", "none");
   }
   
});

$('#forma_pag2_valor').blur(function(){
   
  var total = $('#totalGeral').val();
  var pago  = $('#valorPG').val();
  var pago2  = $('#forma_pag2_valor').val();
  var troco = (parseFloat(pago) + parseFloat(pago2)) - parseFloat(total);

  $('#troco').val(troco.toFixed(2));
  document.getElementById('valorPG').focus();
  
});

$(document).ready(function(){

  $("#finalizarVendabnt").attr("disabled", true);

});

$('#add-forma_pag').click(function () {

  $("#add-forma_pag").css("display", "none"); 
  $("#add-forma_pag_minus").css("display", "block"); 
  $("#forma_pag2_div").css("display", "block"); 
  $("#forma_pag2_valor_div").css("display", "block"); 
  $("#forma_pag2_status").val('1'); 
  $("#forma_pag2").focus();

});

$('#add-forma_pag_minus').click(function () {

  $("#add-forma_pag_minus").css("display", "none"); 
  $("#add-forma_pag").css("display", "block");
  $("#forma_pag2_div").css("display", "none"); 
  $("#forma_pag2_valor_div").css("display", "none"); 
  $("#forma_pag2").val('Dinheiro'); 
  $("#forma_pag2_valor").val('0,00'); 
  $("#forma_pag2_status").val('0'); 
  $("#finalizarVendabnt").attr("disabled", true);
  
});


$("#recebimento").change(function() {

  if ($(this).prop("checked") == false) {
     $("#valorPG").val('');
     $("#valorPG").attr("disabled", true);
     $("#troco").val('');
     $("#aviso_troco").css("display", "none");
     $("#recebimentoDate").css("display", "block");
     $("#forma_pag2_div").css("display", "none"); 
     $("#forma_pag2").val('Dinheiro'); 
     $('#add-forma_pag').css("display", "none"); 
     $('#add-forma_pag_minus').css("display", "block"); 
     $("#forma_pag").val( $('option:contains("Sem pagamento")').val());
     
     if (!$('#cliente_nome').val() || $('#cliente_id').val() == '1') {

      $("#finalizarVendabnt").attr("disabled", true);
      $("#aviso_cliente").css("display", "block"); 

     } else {

      $("#finalizarVendabnt").attr("disabled", false);
     }
  }else{
     $("#forma_pag").val( $('option:contains("Dinheiro")').val() );
     $("#aviso_cliente").css("display", "none"); 
     $("#recebimentoDate").css("display", "none"); 
     $('#add-forma_pag').css("display", "block"); 
     $('#add-forma_pag_minus').css("display", "none");
     $("#valorPG").attr("disabled", false);
     $("#valorPG").focus();
  }
});


$('#cliente_nome').blur(function(){

   var checado = $('#recebimento').is(':checked');

    if (checado == false) {

     if (!$('#cliente_nome').val() || $('#cliente_id').val() == '1') {

      $("#finalizarVendabnt").attr("disabled", true);
      $("#aviso_cliente").css("display", "block"); 

     } else {

      $("#finalizarVendabnt").attr("disabled", false);
      $("#aviso_cliente").css("display", "none"); 
     }
  }
});


$('#valorPG').blur(function(){

  var troco = $('#troco').val();
  var cliente = $('#cliente_nome').val();
  
  if (parseFloat(troco) < 0) {
      $("#finalizarVendabnt").attr("disabled", true);
  }else{
      $("#finalizarVendabnt").attr("disabled", false);
  }
});

$('#forma_pag2_valor').blur(function(){

  var troco = $('#troco').val();
  // var forma_pag2 = $('#forma_pag2').val();

  // console.log(forma_pag2)
  
  if (parseFloat(troco) < 0) {
      $("#finalizarVendabnt").attr("disabled", true);
  }else{
      $("#finalizarVendabnt").attr("disabled", false);
  }
});


$(document).keypress(function(e) {

    if (e.which == 13) {
       var bnt = $('#finalizarVendabnt').attr('disabled');
        if(bnt != 'disabled'){
         $('#finalizarVendabnt').trigger('click'); 
        }
        
    }
});

$('#finalizarVendabnt').click(function() {

  var total = $('#totalGeral').val();
  var venda  = $('#idVenda').val();
  var vedendor  = $('#usuario_id').val();
  var cliente = $('#cliente_id').val();
  var desconto = $('#desconto').val();
  var pago  = $('#valorPG').val();
  var forma_pag = $('#forma_pag').val();
  var tipo_compra = $('#tipo_compra').val(); // Atacado ou Varejo
  var nome_cliente = $('#cliente_nome').val();
  var dataPagamento = $('#datepickerRecebimentoDate').val();
  var forma_pag2_valor = $('#forma_pag2_valor').val();
  var forma_pag2 = $('#forma_pag2').val();
  var forma_pag2_status = $('#forma_pag2_status').val();

   $.ajax({
      method: "POST",
      url: base_url+"pdv2/fecharVenda/",
      dataType: "JSON",
      data: { dataPagamento: dataPagamento, total: total, venda: venda, vedendor: vedendor, cliente: cliente, desconto: desconto, pago: pago, forma_pag: forma_pag, tipo_compra: tipo_compra, nome_cliente: nome_cliente, forma_pag2_valor: forma_pag2_valor, forma_pag2: forma_pag2, forma_pag2_status: forma_pag2_status },
      success: function(data)
       {
         if(data.result == true){
           window.location.href = base_url+"pdv2/index/";
           window.open(base_url+"vendas/visualizarNota/"+venda, "Visualizar", "toolbar=no,location=no,directories=no,status=no,menubar=yes,scrollbars=yes,resizable=yes,width=350,height=600");
         }
         else{
           alert('Ocorreu um erro.');
         }
       }
      
    });

});


$(document).on('click', '.filias', function () {
        var loja = $(this).attr('id');
            result = loja.split("_");
            lojaMenu = result[2];
            cor = $(this).attr('datacor');

        $('.filialAtual').css('background', cor);
        $('.filialAtual').text(lojaMenu.toUpperCase());
        $('#filialAtual').val(loja);


  });

  $('#add-cliente').click(function () {
      $('#customerModal').modal({backdrop:'static'});
  });


  // $('#cliente_nomeModal').blur(function(){

  //   var cliente = $('#cliente_nomeModal').val();
  //   var cpfCnpj = $('#cpfCnpj').val();

  //   console.log('oi');

  //   if (cliente == '' && cpfCnpj == '') {
  //       $("#addCliente").attr("disabled", true);
  //   }else{
  //       $("#addCliente").attr("disabled", false);
  //   }
  // });

  $('#tipo').change(function(event) {
  var tipo = $(this).val();

  if(tipo == 1){      
    $('#cpfCnpj').inputmask('999.999.999-99');
  }else{
    $('#cpfCnpj').inputmask('99.999.999/9999-99');
  }
 });

  $('#addCliente').click(function(event) {

    var cliente_nomeModal    = $('#cliente_nomeModal').val();
        cliente_email   = $('#cliente_email').val();
        cliente_numero  = $('#cliente_numero').val();
        tipo            = $('#tipo').val();
        cpfCnpj         = $('#cpfCnpj').val();
      
      if (cliente_nomeModal != '' && cpfCnpj != '') {

        $.ajax({
            method: "POST",
            url: base_url+"pdv2/verificarCliente/",
            dataType: "html",
            data: {cpfCnpj: cpfCnpj}
            })
            .done(function( response ) {

            if(response == 'true'){

              $('#c-alert').html('Esse CPF/CNPJ já consta cadastro!');
              $('#c-alert').show();
              
            } else {

                $.ajax({
                    method: "POST",
                    url: base_url+"pdv2/addCliente/",
                    dataType: "html",
                    data: {
                        cliente_nomeModal: cliente_nomeModal,
                        cliente_email: cliente_email,
                        cliente_numero: cliente_numero,
                        tipo: tipo,
                        cpfCnpj: cpfCnpj}
                    })
                    .done(function( response ) {
            
                      location.reload();
                    // $('#cliente_desc').html(response);
                    // $('#cliente_desc').removeAttr("disabled", "disabled" );    
                
                });
              
            }   
        
        });

      }else{
        $('#c-alert').html('Existe algum campo obrigatório em branco!');
        $('#c-alert').show();
      }


});




 $('#btn-confirm').click(function(event) {

    var senha = $('#input-liberar-senha').val();

   $.ajax({
      method: "POST",
      url: base_url+"pdv2/validacaoGerente/",
      dataType: "JSON",
      data: { senha },
      success: function(data)
       {
         if(data.result == true){
           $('#liberar-senha').val('true');
         }
         else{
           alert('Senha Invalida.');
            location.reload();
         }
       }
      
    });
});


// $('#total-item').blur(function(){

//   alert('AQUI');
   
//    var quantidade = $('#quantidade').val();
//    var subTotal = $('#total-item').val();
//    var venda  = $('#idVenda').val();
//    var produto = $('#produto_desc').val();
//    var filial = $('.filialAtual').text();

//    $.ajax({
//       method: "POST",
//       url: base_url+"pdv2/additensVendas/",
//       dataType: "JSON",
//       data: { quantidade: quantidade, subTotal: subTotal, venda: venda, produto: produto, filial: filial },
//       success: function(data)
//        {
//          if(data.result == true){
//           $("#grupo_prod").focus();
//            location.reload();
//          }
//          else{
//            alert('Quantidade sem estoque!');
//          }
//        }
      
//     });
   
// });


$('#total-item').blur(function(){
   
   var quantidade = $('#quantidade').val();
   var subTotal = $('#total-item').val();
   var venda  = $('#idVenda').val();
   var produto = $('#produto_desc').val();
   var filial = $('.filialAtual').text();

   // $.ajax({
   //    method: "POST",
   //    url: base_url+"pdv2/additensVendas/",
   //    dataType: "JSON",
   //    data: { quantidade: quantidade, subTotal: subTotal, venda: venda, produto: produto, filial: filial },
   //    success: function(data)
   //     {
   //       if(data.result == true){
   //        $("#grupo_prod").focus();
   //         location.reload();
   //       }
   //       else{
   //         alert('Quantidade sem estoque!');
   //       }
   //     }
      
   //  });
   
});
