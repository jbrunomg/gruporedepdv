
$(document).ready(function () {

  window.onhelp = function () {
    return false;
  };
  window.onkeydown = evt => {
    switch (evt.keyCode) {
      //ESC
      case 113:
        iniciarVenda();
        break;
      case 114:
        excluirItens();
        break;
      case 115:
        cancelarVenda();
        break;
      case 116:
        finalizarVenda();
        break;
      case 121:
        sairVenda();
        break;
      default:
        return true;
    }
    //Returning false overrides default browser event
    return false;
  };


  //  $("#addCliente").attr("disabled", true);



});

var idItens = $('#idItens').val();

if (idItens == 0) {
  $("#btn-finalizar-venda").attr("disabled", true);
} else {
  $("#btn-finalizar-venda").attr("disabled", false);
}

function iniciarVenda() {
  incluirVenda(); // Criar uma venda para pegar um ID
  $('#venda-iniciada').show();
  $('#btn-iniciar-venda').prop('disabled', true);
  $('#btn-excluir-item').prop('disabled', false);
  $('#btn-cancelar-venda').prop('disabled', false);
  $('#btn-finalizar-venda').prop('disabled', false);
  $('#btn-sair').prop('disabled', false);
  document.getElementById('grupo_prod').focus();
}

function cancelarVenda() {
  // $('#venda-iniciada').hide();
  // $('#btn-iniciar-venda').prop('disabled', false);
  // $('#btn-excluir-item').prop('disabled', true);
  // $('#btn-cancelar-venda').prop('disabled', true);
  // $('#btn-finalizar-venda').prop('disabled', true);
  // $('#btn-sair').prop('disabled', true);

  var venda = $('#idVenda').val();


  $.ajax({
    method: "POST",
    url: base_url + "Pdvtablet/cancelarVenda/",
    dataType: "JSON",
    data: { venda: venda },
    success: function (data) {
      if (data.result == true) {
        location.reload($("#grupo_prod").focus());
      }
      else {
        alert('Ocorreu um erro tente novamente!');
      }
    }

  });
}

function finalizarVenda() {
  //$('#modal-default').modal()

  let verificarMarcado = $('.item-checkbox:checked').length > 0;
  var venda = $('#idVenda').val();


  $.ajax({
    method: "GET",
    url: base_url + "parametro/verificarParametro/",
    dataType: "JSON",
    success: function (data) {

      if (data == 1) {
        // Verifica se algum checkbox está marcado
        if (verificarMarcado) {
          $("#modal-default-geral").modal({ backdrop: 'static', keyboard: false });
          $("#input-liberar-senha").focus();
        } else {
          window.location.assign(base_url + "Pdvtablet/finalizarVenda/" + venda);

        }

      } else {
        window.location.assign(base_url + "Pdvtablet/finalizarVenda/" + venda);

      }

    }

  });


  // $.ajax({
  //  method: "POST",
  //  url: base_url+"Pdvtablet/finalizarVenda/",
  //  dataType: "JSON",
  //  data: { venda: venda}
  //  })
  //  .done(function( response ) {
  //    console.log(response);
  //  //$("#idVenda").text(response);
  //  window.location.assign(base_url+"Pdvtablet/editarVenda/"+response);
  //  //document.getElementById('grupo_prod').focus();
  //  });

}


function excluirItens() {
  let venda = $('#idVenda').val();
  let idItens = $('#idItens').val();
  let imei = $('#imei').val();

  let formData = new FormData();
  formData.append("venda", venda);
  formData.append("idItens", idItens);
  formData.append("imei", imei);

  fetch(`${base_url}Pdvtablet/excluirItens/`, {
    method: "POST",
    body: formData,
    headers: {
      'X-Requested-With': 'XMLHttpRequest'
    }
  })
    .then(response => {
      if (!response.ok) {
        return response.text()
          .then(errorText => {
            try {
              const erroData = JSON.parse(errorText);

              if (erroData.result !== undefined && erroData.msg !== undefined) {
                return {
                  result: erroData.result,
                  msg: erroData.msg
                };
              } else {
                throw new Error("Ocorreu um erro inesperado. Tente novamente mais tarde ou entre em contato com o suporte.");
              }
            } catch (err) {
              console.error("Erro ao processar JSON de erro:", errorText);
              throw new Error("Ocorreu um erro inesperado. Tente novamente mais tarde ou entre em contato com o suporte.");
            }
          });
      }
      return response.json();
    })
    .then(data => {
      if (data.result === true) {
        $("#grupo_prod").focus();
        location.reload();
      } else {
        showModalAlert("Erro", data.msg);
      }
    })
    .catch(error => {
      console.error("Erro capturado no catch:", error.message);
      showModalAlert("Erro", "Ocorreu um erro inesperado. Tente novamente mais tarde ou entre em contato com o suporte.");
    });
  // $.ajax({
  //   method: "POST",
  //   url: base_url + "Pdvtablet/excluirItens/",
  //   dataType: "JSON",
  //   data: ,
  //   success: function (data) {
  //     if (data.result == true) {
  //       location.reload($("#grupo_prod").focus());
  //     }
  //     else {
  //       alert('Ocorreu um erro tente novamente!');
  //     }
  //   }

  // });
}


function incluirVenda() {

  var clie = 1;

  $.ajax({
    method: "POST",
    url: base_url + "Pdvtablet/adicionarVenda/",
    dataType: "JSON",
    data: { clie: clie }
  })
    .done(function (response) {
      // console.log(response);
      //$("#idVenda").text(response);
      window.location.assign(base_url + "Pdvtablet/editarVenda/" + response);
      document.getElementById('grupo_prod').focus();
    });

}

function sairVenda() {
  var venda = $('#idVenda').val();

  $.ajax({
    method: "POST",
    url: base_url + "Pdvtablet/sairVenda/",
    dataType: "JSON",
    data: { venda: venda },
    success: function (data) {
      if (data.result == true) {
        window.location.assign(base_url + "Pdvtablet/");
      }
      else {
        alert('Ocorreu um erro tente novamente!');
      }
    }

  });
}

function sairVendaDashboard() {
  var venda = $('#idVenda').val();

  $.ajax({
    method: "POST",
    url: base_url + "Pdvtablet/sairVenda/",
    dataType: "JSON",
    data: { venda: venda },
    success: function (data) {
      if (data.result == true) {
        window.location.assign(base_url + "sistema/dashboard");
      }
      else {
        alert('Ocorreu um erro tente novamente!');
      }
    }

  });
}

$('#grupo_prod').blur(function () {

  var grupo = $('#grupo_prod').val();
  var filial = $('#filialAtual').val();

  $.ajax({
    method: "POST",
    url: base_url + "Pdvtablet/selecionarProduto/",
    dataType: "html",
    data: { grupo: grupo, filial: filial }
  })
    .done(function (response) {

      $('#produto_desc').html(response);
      $('#produto_desc').removeAttr("disabled", "disabled");
      //document.getElementById('prod_desc').focus();
      $('#produto_desc').select2('open');

    });

});

$('#produto_desc').change(function () {

  var prod = $('#produto_desc').val();
  var filial = $('#filialAtual').val();
  var imei = $('#produto_desc').find(':selected').attr('dataImei');

  $.ajax({
    method: "POST",
    url: base_url + "Pdvtablet/estoqueProdutoId/",
    dataType: "JSON",
    data: { produto: prod, filial: filial }
  })
    .done(function (response) {
      $("#estoque").text(response[0].produto_estoque);
      $("#valor_unit").val(response[0].produto_preco_venda);
      $("#precovenda").val(response[0].produto_preco_venda);
      $("#precocusto").val(response[0].produto_preco_custo);
      $("#codigo").val(response[0].produto_codigo);
      $("#pvm").text(response[0].produto_preco_minimo_venda);
      $("#pcd").text(response[0].produto_preco_cart_debito);
      $("#pcc").text(response[0].produto_preco_cart_credito);

      // document.getElementById('quantidade').focus();

      if (imei.length > 0) {
        $("#valor_unit").focus();
        $("#quantidade").val('1');
        $("#quantidade").prop("disabled", true);
      } else {
        $("#quantidade").focus();
        $("#quantidade").prop("disabled", false);
      }

    });


  $('#quantidade').blur(function () {

    var quantidade = $('#quantidade').val();
    var valor = $('#valor_unit').val();
    var total = parseFloat(quantidade) * parseFloat(valor);

    if (parseFloat(valor) > 0) {
      $('#liberar-senha').val('senha');
    }

    $('#total-item').val(total.toFixed(2));

  });

  $('#valor_unit').blur(function () {
    var quantidade = $('#quantidade').val();
    var valor = $('#valor_unit').val();
    var total = parseFloat(quantidade) * parseFloat(valor);

    if (parseFloat(valor) > 0) {
      $('#liberar-senha').val('senha');
    }

    $('#total-item').val(total.toFixed(2));

  });

  $('#quantidade').blur(function () {

    var estoque = $('#estoque').text();

    if (estoque == '0') {

      $("#aviso_estoque").css("display", "block");

    } else {

      $("#aviso_estoque").css("display", "none");
    }

  });

});


$("#cliente_nome").autocomplete({
  source: base_url + "Pdvtablet/autoCompleteClientes",
  minLength: 2,
  select: function (event, ui) {
    $("#cliente_id").val(ui.item.id);
  }
});

$("#usuario_nome").autocomplete({
  source: base_url + "Pdvtablet/autoCompleteUsuarios",
  minLength: 2,
  select: function (event, ui) {
    $("#usuario_id").val(ui.item.id);
  }
});



//    FUNÇÕES.    //
var styleSheet = document.styleSheets[0];

var keyframes =
  `@keyframes is-rotating {
    to {
        transform: rotate(1turn);
    }
}`;

styleSheet.insertRule(keyframes, styleSheet.cssRules.length);
function ativarCarregamentoBotao(idBotao, texto = 'Enviando') {
  let botao = $('#' + idBotao);
  botao.data('html-original', botao.html());
  botao.html('');
  botao.attr('disabled', true);
  botao.css({
    'cursor': 'wait',
    'display': 'flex',
    'align-items': 'center',
    'justify-content': 'center'
  });
  let icone = $('<i class="c-loader"></i>').css({
    'animation': 'is-rotating 1s infinite',
    'border': '4px solid #e5e5e5',
    'border-radius': '50%',
    'border-top-color': '#51d4db',
    'height': '1em',
    'width': '1em',
    'display': 'inline-block',
    'vertical-align': 'middle',
    'margin-right': '0.2em'
  });
  botao.append(texto);
  botao.append('&nbsp;');
  botao.append(icone);
}

function desativarCarregamentoBotao(idBotao) {
  let botao = $('#' + idBotao);
  botao.html(botao.data('html-original'));
  botao.attr('disabled', false);
  botao.css('cursor', 'pointer');
}

function formatarNumero(input) {
  const valorSemNaoNumericos = $(input).val().replace(/\D/g, '');

  if (valorSemNaoNumericos === '' || valorSemNaoNumericos === '0') {
    $(input).val('');
  } else {
    const numeroFormatado = (parseFloat(valorSemNaoNumericos) / 100).toFixed(2);
    $(input).val(numeroFormatado);
  }
}

function calculaTroco(total, totalGeral) {
  let troco = total - totalGeral;

  if (troco < 0) {
    $('#aviso_troco').show();
    $('#finalizarVendabnt').attr('disabled', true);
  }
  else {
    $('#aviso_troco').hide();
    $('#finalizarVendabnt').attr('disabled', false);
  }

  $('#troco').val(troco.toFixed(2));
}

//    COM DESCONTO.    //
// function atualizaTotal() {
//   let total = 0;
//   for (let i = 1; i <= qtd_forma_pagamento; i++) {
//     let valor = parseFloat($('#forma_pag_valor' + i).val());

//     if (valor) total += valor;
//   }

//   let totalGeral = parseFloat($('#totalGeral').val());

//   // Chamar a função calculaTroco
//   calculaTroco(total, totalGeral);

//   $('#box_valor').text(total.toFixed(2));


//   let desconto_atual = parseFloat($('#desconto').val());

//   if (desconto_atual > $('#valor_total').val() || $('#troco').val() < 0) {
//     (desconto_atual > $('#valor_total').val()) ? $('#aviso_desconto').show() : $('#aviso_desconto').hide();
//     $('#finalizarVendabnt').attr('disabled', true);
//   }
//   else {
//     $('#aviso_desconto').hide();
//     $('#finalizarVendabnt').attr('disabled', false);
//   }
// }
//    /COM DESCONTO.    //

//    SEM DESCONTO.    //
function atualizaTotal() {
  let total = 0;
  for (let i = 1; i <= qtd_forma_pagamento; i++) {
    let valor = parseFloat($('#forma_pag_valor' + i).val());

    if (valor) total += valor;
  }

  let totalGeral = parseFloat($('#totalGeral').val());

  // Chamar a função calculaTroco
  calculaTroco(total, totalGeral);

  $('#box_valor').text(total.toFixed(2));

  if ($('#troco').val() < 0) {
    $('#finalizarVendabnt').attr('disabled', true);
  }
  else {
    $('#finalizarVendabnt').attr('disabled', false);
  }
}
//    /SEM DESCONTO.    //

function adicionarRegra(numeroCampo, campo, regra, mensagem) {
  todas_regras[campo + numeroCampo] = regra;
  todas_mensagens[campo + numeroCampo] = mensagem;
}

function adicionarRegrasValidacao(numeroCampo) {
  adicionarRegra(numeroCampo, 'forma_pag_valor', {
    required: true
  }, {
    required: 'Campo Requerido.'
  });

  adicionarRegra(numeroCampo, 'data_vencimento', {
    required: true,
  }, {
    required: 'Campo Requerido.',
  });

  if ($('#forma_pag' + numeroCampo).val() === 'Cartão de Crédito' || $('#forma_pag' + numeroCampo).val() === 'Cartão de Débito') {
    adicionarRegra(numeroCampo, 'bandeira_cartao', {
      required: true
    }, {
      required: 'Campo Requerido.'
    });
  }
}

function mostrarErro(mensagem) {
  let alerta = $("#alertaErro");
  alerta.text(mensagem);
  alerta.show();

  // rolarScrollModal('topo');
  setTimeout(() => {
    alerta.hide();
    alerta.text(null);
  }, 6000);
}
//   /FUNÇÕES.    //

//    DESCONTO.    //
// let desconto_anterior = 0;
// let total_venda = parseFloat($('#valor_total').val());
// let total_original = total_venda;

// $('#desconto').on('blur', function () {
//   const desconto_atual = parseFloat($(this).val());
//   // console.log(desconto_atual);
//   if (isNaN(desconto_atual) || desconto_atual <= 0) {
//     $('#totalGeral').val(total_original.toFixed(2));
//     atualizaTotal();
//     return;
//   }

//   if (desconto_atual > total_venda) {
//     $('#aviso_desconto').show();
//     $('#finalizarVendabnt').attr('disabled', true);
//     $('#totalGeral').val(total_original.toFixed(2));
//     $('#troco').val(null);
//     $(this).val(desconto_atual.toFixed(2));
//   } else {
//     desconto_anterior = parseFloat(desconto_atual);
//     $('#aviso_desconto').hide();
//     $('#finalizarVendabnt').attr('disabled', false);

//     let total_com_desconto = total_original - desconto_atual;
//     $('#totalGeral').val(total_com_desconto.toFixed(2));
//     atualizaTotal();
//   }
// });
//    /DESCONTO.    //

//    SEM DESCONTO.    //
let total_venda = parseFloat($('#valor_total').val());
let total_original = total_venda;

$('#desconto').on('blur', function () {
  $('#totalGeral').val(total_original.toFixed(2));
  atualizaTotal();
});
//    /SEM DESCONTO.    //

//    CHECK FORMA DE PAGAMENTO.   //
if ($('#forma_pag1').val() == 'Cartão de Crédito' || $('#forma_pag1').val() == 'Cartão de Débito') {
  $('#dados_cartao').show();
} else {
  $('#dados_cartao').hide();
}

$('#forma_pagamento1, #forma_pagamento_nova').on('change', '.forma_pag', function () {
  let forma_pag = $(this).val();
  let dados_cartao = $(this).parent().parent().parent().find('#dados_cartao');
  if (forma_pag == 'Cartão de Crédito' || forma_pag == 'Cartão de Débito') {
    dados_cartao.show();
  } else {
    dados_cartao.hide();

    $(this).parent().parent().parent().find('.bandeira_cartao').val(null);
    $(this).parent().parent().parent().find('.id_bandeira').val(null);
    $(this).parent().parent().parent().find('.categoria_cart_numero_parcela').val(null);
    $(this).parent().parent().parent().find('.categoria_cart_percentual_parcela').val(null);
  }
});
//   /CHECK FORMA DE PAGAMENTO.   //

//   CHECK BANDA DE CARTÃO.   //
$('#forma_pagamento1, #forma_pagamento_nova').on('change', '.bandeira_cartao', function () {
  let id_bandeira = $(this).find(':selected').attr('id_bandeira');
  let numero_parcelas = $(this).find(':selected').attr('categoria_cart_numero_parcela');
  let percentual_parcela = $(this).find(':selected').attr('categoria_cart_percentual_parcela');
  $(this).parent().parent().find('.id_bandeira').val(id_bandeira);
  $(this).parent().parent().find('.categoria_cart_numero_parcela').val(numero_parcelas);
  $(this).parent().parent().find('.categoria_cart_percentual_parcela').val(percentual_parcela);
});
//  /CHECK BANDA DE CARTÃO.   //

//    ADICIONAR FORMA DE PAGAMENTO.   //
var qtd_forma_pagamento = $('#qtd_forma_pag').val();
const ELEMENTO_BASE = $("#forma_pagamento1").clone();

$('#adicionar_forma_pagamento').on('click', function () {
  let clone = ELEMENTO_BASE.clone();
  qtd_forma_pagamento++;

  clone.attr('id', 'forma_pagamento' + qtd_forma_pagamento);

  let input_forma_pag = clone.find('#forma_pag1');
  let label_forma_pag = clone.find('label[for="forma_pag1"]');

  let input_valor = clone.find('#forma_pag_valor1');
  let label_valor = clone.find('label[for="forma_pag_valor1"]');

  let input_data_vencimento = clone.find('#data_vencimento1');
  let label_data_vencimento = clone.find('label[for="data_vencimento1"]');

  let input_bandeira = clone.find('#bandeira_cartao1');
  let label_bandeira = clone.find('label[for="bandeira_cartao1"]');

  let input_nsu = clone.find('#n_autorizacao_cartao1');
  let label_nsu = clone.find('label[for="n_autorizacao_cartao1"]');

  let input_id_bandeira = clone.find('#id_bandeira1');
  let input_categoria_cart_numero_parcelas = clone.find('#categoria_cart_numero_parcela1');
  let input_categoria_cart_percentual_parcela = clone.find('#categoria_cart_percentual_parcela1');

  if (input_forma_pag.val() == 'Cartão de Crédito' || input_forma_pag.val() == 'Cartão de Débito') {
    clone.find('#dados_cartao').show();
  } else {
    clone.find('#dados_cartao').hide();
  }


  //      ATUALIZA OS IDS E NAMES DOS CAMPOS CLONADOS.       //
  input_forma_pag.attr('name', 'forma_pag' + qtd_forma_pagamento);
  input_forma_pag.attr('id', 'forma_pag' + qtd_forma_pagamento);
  label_forma_pag.attr('for', 'forma_pag' + qtd_forma_pagamento);
  input_forma_pag.val('Dinheiro');

  input_valor.attr('name', 'forma_pag_valor' + qtd_forma_pagamento);
  input_valor.attr('id', 'forma_pag_valor' + qtd_forma_pagamento);
  label_valor.attr('for', 'forma_pag_valor' + qtd_forma_pagamento);
  input_valor.val(null);

  input_data_vencimento.attr('name', 'data_vencimento' + qtd_forma_pagamento);
  input_data_vencimento.attr('id', 'data_vencimento' + qtd_forma_pagamento);
  label_data_vencimento.attr('for', 'data_vencimento' + qtd_forma_pagamento);

  input_data_vencimento.datepicker({
    autoclose: true,
    format: 'dd/mm/yyyy',
  });
  input_data_vencimento.datepicker('setDate', new Date());

  input_bandeira.attr('name', 'bandeira_cartao' + qtd_forma_pagamento);
  input_bandeira.attr('id', 'bandeira_cartao' + qtd_forma_pagamento);
  label_bandeira.attr('for', 'bandeira_cartao' + qtd_forma_pagamento);
  input_bandeira.val(null);

  input_id_bandeira.attr('name', 'id_bandeira' + qtd_forma_pagamento);
  input_id_bandeira.attr('id', 'id_bandeira' + qtd_forma_pagamento);
  input_id_bandeira.val(null);

  input_categoria_cart_numero_parcelas.attr('name', 'categoria_cart_numero_parcela' + qtd_forma_pagamento);
  input_categoria_cart_numero_parcelas.attr('id', 'categoria_cart_numero_parcela' + qtd_forma_pagamento);
  input_categoria_cart_numero_parcelas.val(null);

  input_categoria_cart_percentual_parcela.attr('name', 'categoria_cart_percentual_parcela' + qtd_forma_pagamento);
  input_categoria_cart_percentual_parcela.attr('id', 'categoria_cart_percentual_parcela' + qtd_forma_pagamento);
  input_categoria_cart_percentual_parcela.val(null);

  input_nsu.attr('name', 'n_autorizacao_cartao' + qtd_forma_pagamento);
  input_nsu.attr('id', 'n_autorizacao_cartao' + qtd_forma_pagamento);
  label_nsu.attr('for', 'n_autorizacao_cartao' + qtd_forma_pagamento);
  input_nsu.val(null);
  //      /ATUALIZA OS IDS E NAMES DOS CAMPOS CLONADOS.       //

  clone.find('.btn-remover').append(
    '<div class="text-center">' +
    '<button type="button" class="btn btn-danger btn-flat remover_forma_pagamento">Remover</button>' +
    '</div>'
  );

  clone.appendTo('#forma_pagamento_nova');

  $('#qtd_forma_pag').val(qtd_forma_pagamento);
});
//   /ADICIONAR FORMA DE PAGAMENTO.   //

//    REMOVE FORMA DE PAGAMENTO.    //
$('#forma_pagamento_nova').on('click', '.remover_forma_pagamento', function () {
  let forma_pagamento_removida = $(this).closest('.forma-pagamento');
  forma_pagamento_removida.remove();
  //      Atualiza os IDs e nomes dos campos restantes.       //
  let contadorInputs = 1;
  let contadorLabels = 1;

  if (contadorInputs <= qtd_forma_pagamento) {
    $('.forma-pagamento').each(function () {
      $(this).attr('id', 'forma_pagamento' + contadorInputs);
      $(this).find(':input').each(function () {
        let id = $(this).attr('id');
        let name = $(this).attr('name');
        if (id) $(this).attr('id', id.replace(/\d+/, contadorInputs));
        if (name) $(this).attr('name', name.replace(/\d+/, contadorInputs));
      });

      $(this).find('label').each(function () {
        let id = $(this).attr('for');
        if (id) $(this).attr('for', id.replace(/\d+/, contadorLabels));
      });

      contadorLabels++;
      contadorInputs++;
    });

  }

  qtd_forma_pagamento--;
  $('#qtd_forma_pag').val(qtd_forma_pagamento);
  atualizaTotal();
  //      /Atualiza os IDs e nomes dos campos restantes.       //
});
//  /REMOVE FORMA DE PAGAMENTO.    //

$('#formas_recebimento').on('keyup', '.forma_pag_valor', function () {
  formatarNumero(this);
  atualizaTotal();
});

$(document).ready(function () {

  $("#finalizarVendabnt").attr("disabled", true);

});

//    COM DESCONTO.    //
// $("#recebimento").change(function () {
//   $("#totalGeral").val($("#valor_total").val());

//   if ($(this).prop("checked") == false) {
//     $('#box_valor').text('0.00');
//     $('#recebimento').val(false);
//     $('#forma_pagamento_nova').empty();
//     qtd_forma_pagamento = 1;
//     $('#qtd_forma_pag').val(qtd_forma_pagamento);

//     $("#forma_pag_valor1").val('');
//     $("#forma_pag_valor1").attr("disabled", true);

//     $('#desconto').val(null);
//     $('#desconto').attr("disabled", true);
//     $('#aviso_desconto').hide();

//     $("#troco").val(null);
//     $("#aviso_troco").css("display", "none");

//     $('#adicionar_forma_pagamento').attr('disabled', true);

//     $("#recebimentoDate").css("display", "none");
//     $("#forma_pag1").val($('option:contains("Sem pagamento")').val());
//     $("#forma_pag1").attr("disabled", true);

//     $("#data_vencimento1").attr("disabled", true);
//     $("#data_vencimento1").val(null);

//     if (!$('#cliente_nome').val() || $('#cliente_id').val() == '1') {

//       $("#finalizarVendabnt").attr("disabled", true);
//       $("#aviso_cliente").css("display", "block");

//     } else {

//       $("#finalizarVendabnt").attr("disabled", false);
//       $("#aviso_troco").css("display", "none");
//     }

//   } else {
//     $('#recebimento').val(true);
//     $('#adicionar_forma_pagamento').attr('disabled', false);

//     $("#forma_pag1").val($('option:contains("Dinheiro")').val());
//     $("#forma_pag1").attr("disabled", false);

//     $("#forma_pag_valor1").attr("disabled", false);
//     $("#forma_pag_valor1").focus();

//     $("#data_vencimento1").attr("disabled", false);
//     $("#data_vencimento1").datepicker('setDate', new Date());

//     // $("#valorPG").attr("disabled", false);

//     $('#desconto').attr("disabled", false);

//     $("#aviso_cliente").css("display", "none");
//     $("#aviso_troco").css("display", "none");

//     $("#recebimentoDate").css("display", "block");
//   }
// });
//    /COM DESCONTO.    //
$('#modelo_nf').change(function () {
  if ($(this).val() == '0') {
    $('#emitir_nf').val(false);
  } else {
    $('#emitir_nf').val(true);
  }
});
//    SEM DESCONTO.    //
$("#recebimento").change(function () {
  $("#totalGeral").val($("#valor_total").val());

  if ($(this).prop("checked") == false) {
    // $('#box_valor').text('0.00');
    $('#recebimento').val(false);
    $('#forma_pagamento_nova').empty();
    qtd_forma_pagamento = 1;
    $('#qtd_forma_pag').val(qtd_forma_pagamento);

    // $("#forma_pag_valor1").val('');
    $("#forma_pag_valor1").attr("disabled", true);

    // $("#troco").val(null);
    $("#aviso_troco").css("display", "none");

    $('#adicionar_forma_pagamento').attr('disabled', true);

    // $("#recebimentoDate").css("display", "none");
    $("#forma_pag1").val($('option:contains("Sem pagamento")').val());
    $("#forma_pag1").attr("disabled", true);

    $("#data_vencimento1").attr("disabled", true);

    $('#datepickerRecebimentoDate').attr('disabled', true);
    // $("#data_vencimento1").val(null);

    if (!$('#cliente_nome').val() || $('#cliente_id').val() == '1') {

      $("#finalizarVendabnt").attr("disabled", true);
      $("#aviso_cliente").css("display", "block");

    } else {

      $("#finalizarVendabnt").attr("disabled", false);
      $("#aviso_troco").css("display", "none");
    }

  } else {
    $('#recebimento').val(true);
    $('#adicionar_forma_pagamento').attr('disabled', false);

    $("#forma_pag1").val($('option:contains("Dinheiro")').val());
    $("#forma_pag1").attr("disabled", false);

    $("#forma_pag_valor1").attr("disabled", false);
    $("#forma_pag_valor1").focus();

    $("#data_vencimento1").attr("disabled", false);
    $("#data_vencimento1").datepicker('setDate', new Date());

    $("#aviso_cliente").css("display", "none");
    $("#aviso_troco").css("display", "none");

    $("#recebimentoDate").css("display", "block");
  }
});
//    /SEM DESCONTO.    //


$('#cliente_nome').blur(function () {

  var checado = $('#recebimento').is(':checked');

  if (checado == false) {

    if (!$('#cliente_nome').val() || $('#cliente_id').val() == '1') {

      $("#finalizarVendabnt").attr("disabled", true);
      $("#aviso_cliente").css("display", "block");
      // $("#valorPG").val('');
      // $("#valorPG").attr("disabled", true);

    } else {

      $("#finalizarVendabnt").attr("disabled", false);
      $("#aviso_cliente").css("display", "none");
    }
  } else {
    $("#finalizarVendabnt").attr("disabled", false);
    $("#aviso_cliente").css("display", "none");
  }
});

// $('#forma_pag2_valor').blur(function () {

//   var troco = $('#troco').val();
//   // var forma_pag2 = $('#forma_pag2').val();

//   // console.log(forma_pag2)

//   if (parseFloat(troco) < 0) {
//     $("#finalizarVendabnt").attr("disabled", true);
//   } else {
//     $("#finalizarVendabnt").attr("disabled", false);
//   }
// });


$(document).keypress(function (e) {

  if (e.which == 13) {
    var bnt = $('#finalizarVendabnt').attr('disabled');
    if (bnt != 'disabled') {
      $('#finalizarVendabnt').trigger('click');
    }

  }
});

const form_venda = $('#form_finalizar_venda');
$('#finalizarVendabnt').click(function () {
  ativarCarregamentoBotao('finalizarVendabnt');

  //    VALIDAÇÃO DE CAMPOS.    //
  todas_regras = {
    data: {
      required: true
    },
    numero_venda: {
      required: true
    },
    cliente_nome: {
      required: true
    },
    usuario_nome: {
      required: true
    },
    data_recebimento: {
      required: ($('#recebimento').is(':checked')) ? true : false,
    }
  };
  todas_mensagens = {
    data: {
      required: 'Campo obrigatório'
    },
    numero_venda: {
      required: 'Campo obrigatório'
    },
    cliente_nome: {
      required: 'Campo obrigatório'
    },
    usuario_nome: {
      required: 'Campo obrigatório'
    },
    data_recebimento: {
      required: 'Campo obrigatório'
    }
  };

  //    CRIANDO REGRAS DE VALIDAÇÃO.    //
  for (let i = 1; i <= qtd_forma_pagamento; i++) {
    adicionarRegrasValidacao(i);
  }
  //    /CRIANDO REGRAS DE VALIDAÇÃO.    //

  //    LIMPA AS VALIDAÇÕES ANTERIORES DO FORMULARIO.    //
  form_venda.data('validator', null);
  //    /LIMPA AS VALIDAÇÕES ANTERIORES DO FORMULARIO.    //

  //    NOVAS VALIDAÇÕES.    //
  form_venda.validate({
    rules: todas_regras,
    messages: todas_mensagens
  });
  //    /NOVAS VALIDAÇÕES.    //

  if (!form_venda.valid()) {
    mostrarErro('Verifique os campos e corrija os erros destacados.');
    desativarCarregamentoBotao('finalizarVendabnt');
    return false;
  }

  if (qtd_forma_pagamento <= 0) {
    mostrarErro('Adicione uma forma de recebimento.');
    desativarCarregamentoBotao('finalizarVendabnt');
    return false;
  }
  //   /VALIDAÇÃO DE CAMPOS.    //

  let formData = new FormData();
  let campos = $('#form_finalizar_venda input, #form_finalizar_venda select');
  // console.log($('#recebido').is(':checked'));
  //   ADICIONA OS CAMPOS DO FORMULÁRIO NO OBJETO FORMDATA.    //
  campos.each(function () {
    var valor = $(this).val();
    var nome = $(this).attr('name');
    formData.append(nome, valor);
  });
  // formData.forEach(function (value, key) {
  //   console.log(key + " = " + value);
  // });
  // console.log('-----------------')
  // return false;

  // var total = $('#totalGeral').val();
  // var venda = $('#idVenda').val();
  // var vedendor = $('#usuario_id').val();
  // var cliente = $('#cliente_id').val();
  // var desconto = $('#desconto').val();
  // var pago = $('#valorPG').val();
  // var forma_pag = $('#forma_pag1').val();
  // var tipo_compra = $('#tipo_compra').val(); // Atacado ou Varejo
  // var nome_cliente = $('#cliente_nome').val();
  // var dataPagamento = $('#datepickerRecebimentoDate').val();
  // // var forma_pag2_valor = $('#forma_pag2_valor').val();
  // // var forma_pag2 = $('#forma_pag2').val();
  // // var forma_pag2_status = $('#forma_pag2_status').val();
  // $.ajax({
  //   method: "POST",
  //   url: base_url + "Pdvtablet/fecharVenda/",
  //   dataType: "JSON",
  //   data: formData,
  //   // data: { dataPagamento: dataPagamento, total: total, venda: venda, vedendor: vedendor, cliente: cliente, desconto: desconto, pago: pago, forma_pag: forma_pag, tipo_compra: tipo_compra, nome_cliente: nome_cliente, forma_pag2_valor: forma_pag2_valor, forma_pag2: forma_pag2, forma_pag2_status: forma_pag2_status },
  //   success: function (data) {
  //     if (data.result == true) {
  //       window.open(base_url + "vendas/visualizarNota/" + venda, "Visualizar", "toolbar=no,location=no,directories=no,status=no,menubar=yes,scrollbars=yes,resizable=yes,width=350,height=600");
  //       window.location.href = base_url + "Pdvtablet/index/";
  //     }
  //     else {
  //       alert('Ocorreu um erro.');
  //     } 1
  //   }
  // });

  fetch(base_url + "Pdvtablet/fecharVenda/", {
    method: 'POST',
    body: formData,
    headers: {
      'Accept': 'application/json'
    }
  })
    .then(response => response.json())
    .then(data => {
      desativarCarregamentoBotao('finalizarVendabnt');
      // console.log(data);
      // return;
      if (data.resultado) {
        let id_venda = data.id_venda;
        window.open(base_url + "vendas/visualizarNota/" + id_venda, "Visualizar", "toolbar=no,location=no,directories=no,status=no,menubar=yes,scrollbars=yes,resizable=yes,width=350,height=600");
        window.location.href = data.danfe == false ? base_url + "emissao/editar/" + data.emissao_id : base_url + "Pdvtablet/index";
      } else {
        mostrarErro(data.msg);
      }
    })
    .catch(error => {
      // console.log(error);
      mostrarErro('Erro ao faturar venda, tente novamente mais tarde.');
      desativarCarregamentoBotao('finalizarVendabnt');
    });

});


$(document).on('click', '.filias', function () {
  var loja = $(this).attr('id');
  result = loja.split("_");
  lojaMenu = result[2];
  cor = $(this).attr('datacor');

  $('.filialAtual').css('background', cor);
  $('.filialAtual').text(lojaMenu.toUpperCase());
  $('#filialAtual').val(loja);


});

$('#add-cliente').click(function () {
  $('#customerModal').modal({ backdrop: 'static' });
});


// $('#cliente_nomeModal').blur(function(){

//   var cliente = $('#cliente_nomeModal').val();
//   var cpfCnpj = $('#cpfCnpj').val();

//   if (cliente == '' && cpfCnpj == '') {
//       $("#addCliente").attr("disabled", true);
//   }else{
//       $("#addCliente").attr("disabled", false);
//   }
// });

$('#tipo').change(function (event) {
  var tipo = $(this).val();

  if (tipo == 1) {
    $('#cpfCnpj').inputmask('999.999.999-99');
  } else {
    $('#cpfCnpj').inputmask('99.999.999/9999-99');
  }
});

$('#addCliente').click(function (event) {

  var cliente_nomeModal = $('#cliente_nomeModal').val();
  cliente_email = $('#cliente_email').val();
  cliente_numero = $('#cliente_numero').val();
  tipo = $('#tipo').val();
  cpfCnpj = $('#cpfCnpj').val();

  if (cliente_nomeModal != '' && cpfCnpj != '') {

    $.ajax({
      method: "POST",
      url: base_url + "Pdvtablet/verificarCliente/",
      dataType: "html",
      data: { cpfCnpj: cpfCnpj }
    })
      .done(function (response) {

        if (response == 'true') {

          $('#c-alert').html('Esse CPF/CNPJ já consta cadastro!');
          $('#c-alert').show();

        } else {

          $.ajax({
            method: "POST",
            url: base_url + "Pdvtablet/addCliente/",
            dataType: "html",
            data: {
              cliente_nomeModal: cliente_nomeModal,
              cliente_email: cliente_email,
              cliente_numero: cliente_numero,
              tipo: tipo,
              cpfCnpj: cpfCnpj
            }
          })
            .done(function (response) {

              location.reload();
              // $('#cliente_desc').html(response);
              // $('#cliente_desc').removeAttr("disabled", "disabled" );    

            });

        }

      });

  } else {
    $('#c-alert').html('Existe algum campo obrigatório em branco!');
    $('#c-alert').show();
  }


});




$('#btn-confirm').click(function (event) {

  var senha = $('#input-liberar-senha').val();

  $.ajax({
    method: "POST",
    url: base_url + "Pdvtablet/validacaoGerente/",
    dataType: "JSON",
    data: { senha },
    success: function (data) {
      if (data.result == true) {
        $('#liberar-senha').val('true');
      }
      else {
        alert('Senha Invalida.');
        location.reload();
      }
    }

  });
});
$('#btn-confirm-geral').click(function (event) {

  var senha = $('#input-liberar-senha-geral').val();
  var venda = $('#idVenda').val();

  $.ajax({
    method: "POST",
    url: base_url + "Pdvtablet/validacaoGerente/",
    dataType: "JSON",
    data: { senha },
    success: function (data) {
      if (data.result == true) {
        window.location.assign(base_url + "Pdvtablet/finalizarVenda/" + venda);
      }
      else {
        alert('Senha Invalida.');
        location.reload();
      }
    }

  });
});


// $('#total-item').blur(function(){

//   alert('AQUI');

//    var quantidade = $('#quantidade').val();
//    var subTotal = $('#total-item').val();
//    var venda  = $('#idVenda').val();
//    var produto = $('#produto_desc').val();
//    var filial = $('.filialAtual').text();

//    $.ajax({
//       method: "POST",
//       url: base_url+"Pdvtablet/additensVendas/",
//       dataType: "JSON",
//       data: { quantidade: quantidade, subTotal: subTotal, venda: venda, produto: produto, filial: filial },
//       success: function(data)
//        {
//          if(data.result == true){
//           $("#grupo_prod").focus();
//            location.reload();
//          }
//          else{
//            alert('Quantidade sem estoque!');
//          }
//        }

//     });

// });


$('#total-item').blur(function () {

  var quantidade = $('#quantidade').val();
  var subTotal = $('#total-item').val();
  var venda = $('#idVenda').val();
  var produto = $('#produto_desc').val();
  var filial = $('.filialAtual').text();

  // $.ajax({
  //    method: "POST",
  //    url: base_url+"Pdvtablet/additensVendas/",
  //    dataType: "JSON",
  //    data: { quantidade: quantidade, subTotal: subTotal, venda: venda, produto: produto, filial: filial },
  //    success: function(data)
  //     {
  //       if(data.result == true){
  //        $("#grupo_prod").focus();
  //         location.reload();
  //       }
  //       else{
  //         alert('Quantidade sem estoque!');
  //       }
  //     }

  //  });

});