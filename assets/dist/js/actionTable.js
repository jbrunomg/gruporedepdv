
$('#tableCliente').DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": {
    "url": base_url+"clientes/listarDados", // URL para buscar os dados
    "type": "POST",
    "data": function (d) {
        // Adicionando os parâmetros esperados pela DataTables à requisição
        d.draw = d.draw || 1; // 'draw' é um número de identificação de requisição
        d.start = d.start || 0; // Início dos registros para a página atual
        d.length = 10; // Quantidade de registros por página
        if (d.search && typeof d.search === 'object' && 'value' in d.search) {
        d.search = d.search.value;
        } else {
        d.search = '';
        }
        // Outros parâmetros opcionais podem ser adicionados aqui conforme necessário
        return d;
    }
    },
    "columns": [
        { "data": "cliente_nome" },
        { "data": "cliente_celular" },
        { "data": "cliente_cpf_cnpj" },
        { "data": "cliente_email" },
        { 
            "data": "icon",
            "orderable": false // Torna a última coluna não ordenável
        }
    ],
    "responsive": true,
    "lengthChange": false, // Permitir a alteração da quantidade de registros por página
    "autoWidth": false
});


$('#tableProduto').DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": {
    "url": base_url+"produto/listarDados", // URL para buscar os dados
    "type": "POST",
    "data": function (d) {
        // Adicionando os parâmetros esperados pela DataTables à requisição
        d.draw = d.draw || 1; // 'draw' é um número de identificação de requisição
        d.start = d.start || 0; // Início dos registros para a página atual
        d.length = 10; // Quantidade de registros por página
        if (d.search && typeof d.search === 'object' && 'value' in d.search) {
        d.search = d.search.value;
        } else {
        d.search = '';
        }
        // Outros parâmetros opcionais podem ser adicionados aqui conforme necessário
        return d;
    }
    },
    "columns": [
        { "data": "produto_codigo" },
        { "data": "produto_codigo_barra" },
        { "data": "produto_descricao" },
        { "data": "produto_estoque" },
        { 
            "data": "icon",
            "orderable": false // Torna a última coluna não ordenável
        }
    ],
    "responsive": true,
    "lengthChange": false, // Permitir a alteração da quantidade de registros por página
    "autoWidth": false
});


$('#tabOrdemServico').DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": {
    "url": base_url+"ordemservico/listarDados", // URL para buscar os dados
    "type": "POST",
    "data": function (d) {
        // Adicionando os parâmetros esperados pela DataTables à requisição
        d.draw = d.draw || 1; // 'draw' é um número de identificação de requisição
        d.start = d.start || 0; // Início dos registros para a página atual
        d.length = 10; // Quantidade de registros por página
        if (d.search && typeof d.search === 'object' && 'value' in d.search) {
        d.search = d.search.value;
        } else {
        d.search = '';
        }
        // Outros parâmetros opcionais podem ser adicionados aqui conforme necessário
        return d;
    }
    },
    "columns": [
        { "data": "os_id",},
        { "data": "cliente_nome" },
        { "data": "entrada" },
        { "data": "saida" },
        { "data": "equipamento" },
        { "data": "status" },
        { "data": "valor" },
        { "data": "icon"}
    ],
    "columnDefs": [
        {
            "orderable": false, // Define a primeira coluna como não ordenável
            "targets": 0 // Índice da primeira coluna
        },
        {
            "orderable": false, // Define a última coluna como não ordenável
            "targets": -1 // Índice da última coluna
        },
        {
            "orderable": false, // Define como não ordenável
            "targets": [2, 3, 6] // Índice da coluna
        }
    ],
    "responsive": true,
    "lengthChange": false, // Permitir a alteração da quantidade de registros por página
    "autoWidth": false,
    "pageLength": 10 
});


    
$('#tableVenda').DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": {
    "url": base_url+"vendas/listarDados", // URL para buscar os dados
    "type": "POST",
    "data": function (d) {
        // Adicionando os parâmetros esperados pela DataTables à requisição
        d.draw = d.draw || 1; // 'draw' é um número de identificação de requisição
        d.start = d.start || 0; // Início dos registros para a página atual
        d.length = 10; // Quantidade de registros por página
        if (d.search && typeof d.search === 'object' && 'value' in d.search) {
        d.search = d.search.value;
        } else {
        d.search = '';
        }
        // Outros parâmetros opcionais podem ser adicionados aqui conforme necessário
        return d;
    }
    },
    "columns": [
        { "data": "usuario_nome"},
        { "data": "cliente_nome"},
        { "data": "idVendas"},
        { "data": "dataVenda"},
        { "data": "valorTotal"},
        { "data": "icon"}
    ],
    "columnDefs": [
        {
            "orderable": false, // Define a primeira coluna como não ordenável
            "targets": '_all' // Índice da primeira coluna
        }
    ],
    "responsive": true,
    "lengthChange": false, // Permitir a alteração da quantidade de registros por página
    "autoWidth": false,
    "pageLength": 10 
}); 


$('#tableMovimentacao').DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": {
    "url": base_url+"movimentacao/listarDados", // URL para buscar os dados
    "type": "POST",
    "data": function (d) {
        // Adicionando os parâmetros esperados pela DataTables à requisição
        d.draw = d.draw || 1; // 'draw' é um número de identificação de requisição
        d.start = d.start || 0; // Início dos registros para a página atual
        d.length = 10; // Quantidade de registros por página
        if (d.search && typeof d.search === 'object' && 'value' in d.search) {
        d.search = d.search.value;
        } else {
        d.search = '';
        }
        // Outros parâmetros opcionais podem ser adicionados aqui conforme necessário
        return d;
    }
    },
    "columns": [
        { "data": "movimentacao_produto_id" },
        { "data": "movimentacao_produto_saida_filial" },
        { "data": "movimentacao_produto_tipo" },
        { "data": "movimentacao_produto_data_cadastro" },
        { "data": "movimentacao_produto_documento" },
        { "data": "total"},
        { "data": "icon"}
    ],
    "columnDefs": [
        {
            "orderable": false, // Define a primeira coluna como não ordenável
            "targets": '_all' // Índice da primeira coluna
        }
    ],
    "responsive": true,
    "lengthChange": false, // Permitir a alteração da quantidade de registros por página
    "autoWidth": false,
    "pageLength": 10 
}); 
 