
function notification(title = '', status = 'success', daley = 2500) {
  // status = success/danger
  const warningAlert = document.createElement("div");
  warningAlert.id = "IdNotificacaoEdit"
  warningAlert.innerHTML = `
  <div style="box-sizing: border-box;position: fixed;top: .9375rem;margin: 0 .9375rem;z-index: 20000;width: 90vw;max-width: 60rem;" class="alert alert-${status} alert-dismissible custom-notification" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close" aria-hidden="true">&times;</button>
    <strong>${title}!</strong>
  </div>`;

  document.body.appendChild(warningAlert);

  setTimeout(function () {
    warningAlert.remove()
  }, daley);
}
