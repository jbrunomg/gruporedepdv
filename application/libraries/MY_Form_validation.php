<?php

class MY_Form_validation extends CI_Form_validation{

    public function edit_unique_produto($str, $field)
    {
        sscanf($field, '%[^.].%[^.].%[^.].%[^.]', $table, $field, $id, $cad);
        return isset($this->CI->db)
            ? ($this->CI->db->limit(1)->get_where($table, array($field => $str, 'produto_id !=' => $id, 'produto_categoria_id =' => $cad))->num_rows() === 0)
            : FALSE;
    }

}