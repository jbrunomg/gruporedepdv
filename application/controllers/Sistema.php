<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sistema extends CI_Controller {

	public function __construct()
	{   
		parent::__construct();
		$this->load->model('Sistema_model');
	}
	
	public function index()
	{
		
		if (GRUPOLOJA == 'grupocell') { 		
			
			$dadosView['emitente'][0]   = array(
	 			'emitente_botao_panico'   => '0'				

	 		);						
			
		} else {

			$dadosView['emitente']  = $this->Sistema_model->botaoPanico();
		}	

		//var_dump($dadosView['emitente']);die();	
	
		$this->load->view('login',$dadosView);	
	}

	public function processarLogin()
	{
		
		$dados['usuario'] = $this->input->post('usuario');
		$dados['senha']   = sha1(md5(strtolower($this->input->post('senha'))));		

		$resultado  = $this->Sistema_model->processarLogin($dados);

		if($resultado){

			$sessao = array(
							'empresa_tipo'               => $resultado[0]->emitente_tipo_empresa,
							'empresa_nome'               => $resultado[0]->emitente_nome,
				            'usuario_id'                 => $resultado[0]->usuario_id,
							'usuario_cpf'                => $resultado[0]->usuario_cpf,
							'usuario_nome'               => $resultado[0]->usuario_nome,
							'perfil_permissoes'          => $resultado[0]->perfil_permissoes,				
							'usuario_perfil'             => $resultado[0]->perfil_id,
							'usuario_perfil_nome'        => $resultado[0]->perfil_descricao,
							'usuario_imagem'             => $resultado[0]->usuario_imagem,
							'usuario_senha'              => $dados['senha'],
							'loja'                       => SUBDOMINIO

							);

			//var_dump($sessao);die();

			$this->session->set_userdata($sessao);

			redirect('Sistema/dashboard');
		
		}else{

			$this->session->set_flashdata('erro','Login ou senha inválidos!');			
			// $this->load->view('login');	
			
			$dadosView['emitente']  = $this->Sistema_model->botaoPanico();		

			$this->load->view('login',$dadosView);
		
		}		
		
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('Sistema');
	}

	public function dashboard()
	{		

		// $feed = file_get_contents('https://www.valor.com.br/empresas/tecnologia/rss');
		
		// $rss = new SimpleXmlElement($feed);

		// $totalNoticias = 3;
		// $contNoticias  = 0;

		// foreach ($rss->channel->item as $noticias) {
		// 	if($contNoticias < $totalNoticias){

				
		// 		$fimimg   = strpos($noticias->description, "<br />");			
		// 		$semimg   = substr($noticias->description, 6 + $fimimg);				
		// 		$noticias->description  = $semimg;

		// 		$dadosView['noticias'][] = (array)$noticias;
		// 	}else{
		// 		continue;
		// 	}
		// 	$contNoticias++;			
		// }

		
		// p($dadosView['noticias']);die();
		$dadosView['noticias']  = '';

		$dadosView['usuarios']  = $this->Sistema_model->totalUsuarios();
		$dadosView['clientes']  = $this->Sistema_model->totalClientes();
		$dadosView['vendas']    = $this->Sistema_model->totalVendas();		
		$dadosView['pedidos']   = $this->Sistema_model->totalPedidos();
		$dadosView['dadosGraficos'] = $this->Sistema_model->graficoHora();

		//var_dump($dadosView['dadosGraficos']);die();


		$dadosView['dadosGraficosAux'] = $this->Sistema_model->dadosGraficosAux();
		$dadosView['evoFaturamento']   = $this->Sistema_model->graficoEmpresaReceita();
		$dadosView['evoDespesa']       = $this->Sistema_model->graficoEmpresaDespesa();


		$dadosView['entrada'] = $this->Sistema_model->totalEntrada();		
		$dadosView['venda']   = $this->Sistema_model->totalVenda();
		$dadosView['estoque'] = $this->Sistema_model->totalEstoque();
		$dadosView['imei']    = $this->Sistema_model->totalImei();

		$dadosView['vendedores']     = $this->Sistema_model->vendedoresCelCustomizadas();


				


        foreach ($dadosView['evoFaturamento'] as $key => $value) {  

            if ($value->atual == null) { 
            $value->atual = '0' ;
            }

            if ($value->meio == null) { 
            $value->meio = '0' ;
            }

            if ($value->fim == null) { 
            $value->fim = '0' ;
            }
                  
        }

        foreach ($dadosView['evoDespesa'] as $key => $value) {  

            if ($value->atual == null) { 
            $value->atual = '0' ;
            }

            if ($value->meio == null) { 
            $value->meio = '0' ;
            }

            if ($value->fim == null) { 
            $value->fim = '0' ;
            }
                  
        }



		$dadosView['meio'] = 'dashboard/dashboard';
		$this->load->view('tema/layout',$dadosView);	
	}
	
	
	
		public function dashboardv2()
	{		

		$dadosFinanceiro  = $this->Sistema_model->resumoFinanceiro();
		$dadosFinanceiroDia   = $this->Sistema_model->resumoFinanceiroDia();

		    $receita = 0.00;
		    $compras = 0.00;
		    $despesa = 0.00;

		    foreach($dadosFinanceiro as $v){

			     if ($v->financeiro_tipo == 'receita') {
			         $receita = $v->valor;
			     }

			     if ($v->financeiro_tipo == 'compras') {
			         $compras = $v->valor;
			     }

			     if ($v->financeiro_tipo == 'despesa') {
			         $despesa = $v->valor;
			     }

		    } 

		    $lucroGeral   = $receita - $compras;
		    $lucroLiquido = ($receita - $compras) - $despesa; 


		    $receitaD = 0.00;
		    $comprasD = 0.00;
		    $despesaD = 0.00;

		    foreach($dadosFinanceiroDia as $v){

			    if ($v->dia == date("j")) {
			            
				       
				     if ($v->financeiro_tipo == 'receita') {
				         $receitaD = $v->valor;
				     }

				     if ($v->financeiro_tipo == 'compras') {
				         $comprasD = $v->valor;
				     }

				     if ($v->financeiro_tipo == 'despesa') {
				         $despesaD = $v->valor;
				     }

			    } 

		    }

	    $lucroGeralD   = $receitaD - $comprasD;
	    $lucroLiquidoD = ($receitaD - $comprasD) - $despesaD; 


	    $dadosView['receitaBrutoDia']     = number_format($receitaD,2,',','.');
	    $dadosView['receitaBrutoMes']     = number_format($receita,2,',','.');
	    $dadosView['custoMercadoriaDia']  = number_format($comprasD,2,',','.');
	    $dadosView['custoMercadoriaMes']  = number_format($compras,2,',','.');
	    $dadosView['lucroGeralDia']       = number_format($lucroGeralD,2,',','.');
	    $dadosView['lucroGeralMes']       = number_format($lucroGeral,2,',','.');
	    $dadosView['despesaGeralDia']     = number_format($despesaD,2,',','.');
	    $dadosView['despesaGeralMes']     = number_format($despesa,2,',','.');
	    $dadosView['lucroLiquidoDia']     = number_format( $lucroLiquidoD,2,',','.');
	    $dadosView['lucroLiquidoMes']     = number_format( $lucroLiquido,2,',','.');

	  //  echo "<pre>"; var_dump($dadosView); exit();


		$dadosView['meio'] = 'dashboardv2/dashboardv2';
		$this->load->view('tema/layout',$dadosView);	
	}

	public function vendasDashboardv2()
	{

		$dadosView['vendasAno']  = $this->Sistema_model->VendasAnoDashboardv2();
		$dadosView['vendasMes']  = $this->Sistema_model->VendasMesDashboardv2();
		$dadosView['vendasDia']  = $this->Sistema_model->VendasDiaDashboardv2();
		echo json_encode($dadosView);

	}

	public function vendasAnoDetalhe()
	{
		
		$dados = $this->Sistema_model->vendasAnoDetalhe(strtolower($_POST['loja']));
		echo json_encode($dados);	
	}

	public function vendasMesDetalhe()
	{
		
		$dados = $this->Sistema_model->vendasMesDetalhe(strtolower($_POST['loja']));
		echo json_encode($dados);	
	}

	public function produtoVPDashboardv2()
	{
		$dadosView['produtoVP'] = $this->Sistema_model->valorEstoque();
		
		echo json_encode($dadosView);
	
	}

	public function produtoMVDashboardv2()
	{

		$dadosView['produtoMVSemestre'] = $this->Sistema_model->maisVendidoS();
		$dadosView['produtoMVTrimestre'] = $this->Sistema_model->maisVendidoT();
		$dadosView['produtoMVMes'] = $this->Sistema_model->maisVendido();

		echo json_encode($dadosView);

	}

	public function produtoVPDetalhe()
	{
		$dados = $this->Sistema_model->valorEstoqueGrupo($_POST['loja']);
		echo json_encode($dados);
	}

	// detalhe do maisVendidoS
	public function maisVendidoDetalheS()
	{
		$dados = $this->Sistema_model->maisVendidoDetalheS($_POST['loja']);
		echo json_encode($dados);
	}

	// detalhe do maisVendidoT
	public function maisVendidoDetalheT()
	{
		$dados = $this->Sistema_model->maisVendidoDetalheT($_POST['loja']);
		echo json_encode($dados);
	}

	// detalhe do maisVendido
	public function maisVendidoDetalhe()
	{
		$dados = $this->Sistema_model->maisVendidoDetalhe($_POST['loja']);
		echo json_encode($dados);
	}

	public function produtoPEDashboardv2()
	{
		
		$dadosView['produtoPE180Dias'] = $this->Sistema_model->maisParadoS();		
		$dadosView['produtoPE90Dias'] = $this->Sistema_model->maisParadoT();
		$dadosView['produtoPE30Dias'] = $this->Sistema_model->maisParado();
		
		echo json_encode($dadosView);
	
	}

	public function maisParadoDetalheS()
	{
		$dados = $this->Sistema_model->maisParadoDetalheS($_POST['loja']);
		echo json_encode($dados);
	
	}

	public function maisParadoDetalheT()
	{
		$dados = $this->Sistema_model->maisParadoDetalheT($_POST['loja']);
		echo json_encode($dados);
	
	}

	public function maisParadoDetalhe()
	{
		$dados = $this->Sistema_model->maisParadoDetalhe($_POST['loja']);
		echo json_encode($dados);
	
	}

	public function financeiroDashboardv2()
	{
		 
	    @set_time_limit( 300 );
	    ini_set('max_execution_time', 300);
	    ini_set('max_input_time', 300);
	    ini_set('memory_limit', '512M');

		$dadosView['previsaoFinanceiroAno']  = $this->Sistema_model->previsaoFinanceiroAno();
		$dadosView['previsaoFinanceiro']  = $this->Sistema_model->previsaoFinanceiro();

		echo json_encode($dadosView);

	}

	public function previsaoFinanceiroDetalhe()
	{
		@set_time_limit( 300 );
	    ini_set('max_execution_time', 300);
	    ini_set('max_input_time', 300);
	    ini_set('memory_limit', '512M');

		$dados = $this->Sistema_model->previsaoFinanceiroDetalhe($_POST['loja']);
		echo json_encode($dados);
	
	}

	public function previsaoFinanceiroDetalheAno()
	{	
	    @set_time_limit( 300 );
	    ini_set('max_execution_time', 300);
	    ini_set('max_input_time', 300);
	    ini_set('memory_limit', '512M');

		$dados = $this->Sistema_model->previsaoFinanceiroDetalheAno($_POST['loja']);
		echo json_encode($dados);
	
	}

	public function clienteDashboardv2()
	{

		$dadosView['clientetop10Ano']  = $this->Sistema_model->top10Ano();
		$dadosView['clientetop10Mes']  = $this->Sistema_model->top10Mes();
		echo json_encode($dadosView);

	}

	public function top10MesDetalhe()
	{
		$dados = $this->Sistema_model->top10MesDetalhe($_POST['loja']);
		echo json_encode($dados);
	
	}

	public function top10AnoDetalhe()
	{		
		$dados = $this->Sistema_model->top10AnoDetalhe($_POST['loja']);
		echo json_encode($dados);
	
	}


	public function vendedorDashboardv2()
	{

		$dadosView['vendedortop10Ano']  = $this->Sistema_model->vendedortop10Ano();
		$dadosView['vendedortop10Mes']  = $this->Sistema_model->vendedortop10Mes();
		$dadosView['vendedortop10Dia']  = $this->Sistema_model->vendedortop10Dia();
		echo json_encode($dadosView);

	}

	public function vendedortop10AnoDetalhe()
	{
		
		$dados = $this->Sistema_model->vendedortop10AnoDetalhe(strtolower($_POST['loja']));
		echo json_encode($dados);	
	}

	public function vendedortop10MesDetalhe()
	{
		
		$dados = $this->Sistema_model->vendedortop10MesDetalhe(strtolower($_POST['loja']));
		echo json_encode($dados);	
	}

	public function vendedortop10DiaDetalhe()
	{
		
		$dados = $this->Sistema_model->vendedortop10DiaDetalhe(strtolower($_POST['loja']));
		echo json_encode($dados);	
	}
	
	
	

	public function emitente()
	{
		$emitente = $this->input->post();

		if($emitente){

			unset($emitente['id']);

			$dados = array();

			foreach ($emitente as $key => $value) {
				$dados[$key] = $value;
			}

			if($_FILES['userfile']['name'] != ''){
                $arquivo = $this->do_upload(); 
                $file = $arquivo['file_name'];
            }else{
                $file = $this->input->post('imagemProduto');
            }

            // NOVO EMITIENTE ADICIONAR
		 	// if($_FILES['userfile']['name'] != ''){
    //             $arquivo = $this->do_upload(); 
    //             $file = $arquivo['file_name'];
    //         }else{
    //             $file = 'produto_sem_imagem.jpg';
    //         }

            $dadosImg = array(           
			    'emitente_url_logo' => $file							  
        	);


        	$resultado = $this->Sistema_model->editarLogoEmitente($dadosImg,$this->input->post('id'));
			
			$resultado = $this->Sistema_model->editarEmitente($dados,$this->input->post('id'));

			if($resultado){
	    		$this->session->set_flashdata('success','Registro adicionado com sucesso!');
	    	}else{
	    		$this->session->set_flashdata('erro','Erro ao adicionar o registro!');
	    	}

		}

		$dadosView['dados'] = $this->Sistema_model->pegarEmitente();
		$dadosView['meio'] = 'emitente/emitente';
		$this->load->view('tema/layout',$dadosView);
	}

	public function wscep()
    {
        $cep =  explode("-",$this->input->post('cep'));
        $cep = $cep[0].$cep[1];
        // $xml = file_get_contents('https://webservices.profissionaisdaweb.com.br/cep/'.$cep.'.xml');
        $xml = file_get_contents('https://viacep.com.br/ws/'.$cep.'/xml/');
        $xml = simplexml_load_string($xml);
        echo json_encode($xml);
    }

    public function selecionarCidade(){
		
		$estado  = $this->input->post('estado');
		$cidades = $this->Sistema_model->selecionarCidades($estado);

		echo  "<option value=''>Selecionar uma cidade</option>";
		foreach ($cidades as $cidade) {
			echo "<option value='".$cidade->nome."'>".$cidade->nome."</option>";
		}

	}

	public function pesquisa()
	{
		if($this->session->userdata('usuario_id')==null) redirect('sistema');
		$pesquisa  = $this->input->post('pesquisa');

		// var_dump($pesquisa);die();


		$dadosView['lojas']     = $this->Sistema_model->getAllFilias();

		// $dadosView = '';
		$dadosView['clientes']     = $this->Sistema_model->pesquisarClientes($pesquisa);
		$dadosView['fornecedores'] = $this->Sistema_model->pesquisarfornecedores($pesquisa);
		$dadosView['vendas']       = $this->Sistema_model->pesquisarVendas($pesquisa);
		$dadosView['financeiro']   = $this->Sistema_model->pesquisarFinanceiro($pesquisa);
		$dadosView['produtos']     = $this->Sistema_model->pesquisarProdutos($pesquisa);

		//var_dump($dadosView['produtos']);die();

		$dadosView['produtoVenda'] = $this->Sistema_model->pesquisarProdutoVenda($pesquisa);
		
		// Novos elementos de busca 02/06/21		
		$dadosView['prodMercadoria']    = $this->Sistema_model->pesquisarprodMercadoria($pesquisa);
				
		$dadosView['prodAjusteEstoque'] = $this->Sistema_model->pesquisarprodAjusteEstoque($pesquisa);		
		$dadosView['prodEstornado']     = $this->Sistema_model->pesquisarprodEstornado($pesquisa);

		// Novos elementos de busca 30/08/22	
		$dadosView['os']          = $this->Sistema_model->pesquisarOs(trim($pesquisa));

		//var_dump($dadosView['os']);die();
		//$dadosView['servico']   = $this->Sistema_model->pesquisarServico($pesquisa);

		$dadosView['meio'] = 'dashboard/pesquisa';
		$this->load->view('tema/layout',$dadosView);
	}

	public function backup()
	{
		$blakLinst = array('estados','tipo_combustivel','tipo_seguro','tipo_veiculo');

		$bkpcf = array(
		        'tables'        => array(),  
		        'ignore'        => $blakLinst,
		        'format'        => 'txt',                         
		        'filename'      => 'backup.'.date('d-m-Y').'.txt',                  
		        'add_drop'      => TRUE,                          
		        'add_insert'    => TRUE,
		        'newline'       => "\n"                           
		);

		// Carrega a classe DB utility 
		$this->load->dbutil();

		// Executa o backup do banco de dados armazenado-o em uma variável
		$backup = $this->dbutil->backup($bkpcf);
		
		// carrega o helper File e cria um arquivo com o conteúdo do backup
		$this->load->helper('file');
		write_file(FCPATH.'assets/arquivos/backups/backup.'.date('d-m-Y').'.txt', $backup);

		// Carrega o helper Download e força o download do arquivo que foi criado com 'write_file'
		force_download('backup.'.date('d-m-Y').'.txt', $backup);
	}

	public function alterarSenha()
	{

		$senha    = sha1(md5(strtolower($this->input->post('senha'))));	
		$novaSenha = sha1(md5(strtolower($this->input->post('novaSenha'))));

		if($senha != $this->session->userdata('usuario_senha')){
			$dadosView['msg']    = "Senha Atual não corresponde ao usuário logado!";
			$dadosView['status'] = false;
		}else{

			$dados['usuario_senha'] = $novaSenha;

			$resultado = $this->Sistema_model->alterarSenha($dados);

			if($resultado){
				$this->session->set_userdata('usuario_senha', $novaSenha);
	    		$dadosView['msg']    = "Senha alterada com Sucesso!";
				$dadosView['status'] = true;
	    	}else{
	    		$dadosView['msg']    = "Erro ao alterar a senha, tente mais tarde!";
				$dadosView['status'] = false;
	    	}
		}		

		echo json_encode($dadosView);

	}

	public function notificacoes()
	{	
		// Entrada de mercadoria enviado da Matriz
		$dados['resultadoApi']  = $this->Sistema_model->apiEntrada();
		$resposta['api']        = $dados['resultadoApi'][0]->total;

		// 20 Dias sem vender o produto(s)
		$dados['resultadoProd20']  = $this->Sistema_model->apiProd20();
		$resposta['apiProd20']     = $dados['resultadoProd20'][0]->total;

		// Qtd produto(s) vendidos no dia loja(s) parceira(s) 
		$dados['resultadoProdLjParc']   = $this->Sistema_model->apiProdLjParc();
		$resposta['apiProdLjParc']      = $dados['resultadoProdLjParc'][0]->total;


		echo json_encode($resposta);
	}

	public function notificacoes2()
	{			

		// Vendas em aberto sem faturar
	    $dados['resultApiVenda'] = $this->Sistema_model->apiVenda();
		$resposta2['apiVenda']   = count($dados['resultApiVenda']); 

	
		// Vendas Zerada(s)
	    $dados['resultApiVendaZerada'] = $this->Sistema_model->apiVendaZerada();
		$resposta2['apiVendaZerada']   = count($dados['resultApiVendaZerada']); 


		// Venda incompleta(s)
	    $dados['resultApiVendaIncompleta'] = $this->Sistema_model->apiVendaIncompleta();
		$resposta2['apiVendaIncompleta']   = count($dados['resultApiVendaIncompleta']); 

		echo json_encode($resposta2);
	}




	public function do_upload(){
        
        $config['upload_path']   = realpath('./assets/img/');
        $config['allowed_types'] = 'txt|jpg|jpeg|gif|png|pdf|PDF|JPG|JPEG|GIF|PNG';
        $config['max_size']      = 0;
        // $config['max_width']  = '3000';
        // $config['max_height'] = '2000';
        $config['encrypt_name']  = true;


        if (!is_dir('./assets/img/')) {
            mkdir('./assets/img/', 0777, TRUE);
        }
        
        $this->load->library('upload', $config);
        $this->upload->initialize($config);       

        if ( ! $this->upload->do_upload())
        {
            $error = array('error' => $this->upload->display_errors());
           // var_dump($error);die();
            $this->session->set_flashdata('erro','Erro ao fazer upload do arquivo, verifique se a extensão do arquivo é permitida.');
        	redirect(base_url() . 'index.php/sistema/emitente/');

        }
        else
        {
            //$data = array('upload_data' => $this->upload->data());
            return $this->upload->data();

        }

    }


}
