<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perfil extends CI_Controller {

		public function __construct()
	{   
		parent::__construct();
		$this->load->model('Perfil_model');
	}

	public function index()
	{
		$dadosView['dados'] = $this->Perfil_model->listar();

		$dadosView['meio'] = 'perfil/listar';
		$this->load->view('tema/layout',$dadosView);	
	}

	public function adicionar()
	{
	    $this->form_validation->set_rules('perfil_descricao', 'Descrição', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
		{
        	$this->session->set_flashdata('erro',validation_errors());
        } else {

        	$dados = array(        		 
				  'perfil_descricao'             => $this->input->post('perfil_descricao'),
				  'perfil_visivel'               => 1				  
        	);

        	$resultado = $this->Perfil_model->adicionar($dados);

        	if($resultado){
        		$this->session->set_flashdata('success','Registro adicionado com sucesso!');
        	}else{
        		$this->session->set_flashdata('erro','Erro ao adicionado o registro!');
        	}
        }

		$dadosView['meio'] = 'perfil/adicionar';
		$this->load->view('tema/layout',$dadosView);
	}

	public function editar()
	{
		$this->form_validation->set_rules('perfil_descricao', 'Descrição', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
		{
        	$this->session->set_flashdata('erro',validation_errors());
        } else {

        	$dados = array(        		 
				  'perfil_descricao'                 => $this->input->post('perfil_descricao'),
				  'perfil_visivel'               => 1				  
        	);
     
        	$resultado = $this->Perfil_model->editar($dados,$this->input->post('id'));

        	if($resultado){
        		$this->session->set_flashdata('success','Registro editado com sucesso!');
        	}else{
        		$this->session->set_flashdata('erro','Erro ao editar o registro!');
        	}
        }

        $this->load->model('Sistema_model');

        $dadosView['dados']   = $this->Perfil_model->listarId($this->uri->segment(3));
		$dadosView['meio']    = 'perfil/editar';

		$this->load->view('tema/layout',$dadosView);

	}

	public function visualizar()
	{
		$this->load->model('Sistema_model');
		
		$dadosView['dados']   = $this->Perfil_model->listarId($this->uri->segment(3));
		$dadosView['meio']    = 'perfil/visualizar';
		$this->load->view('tema/layout',$dadosView);
	}

	public function excluir()
	{
		$id = $this->uri->segment(3);

		$dados  = array(
						'perfil_visivel' => 0						
					  );
		$resultado = $this->Perfil_model->excluir($dados,$id);

		if($resultado){
			$this->session->set_flashdata('success','registro excluidos com sucesso!');
		}else{
			$this->session->set_flashdata('erro','Erro ao excluir o registro!');
		}

		redirect('Perfil','refresh');
	}

	public function permissoes()
	{
		$permissoesNivel = $this->input->post();

		if($permissoesNivel){

			unset($permissoesNivel['id']);

			$permissoes = array();

			foreach ($permissoesNivel as $key => $value) {
				$permissoes[$key] = 1;
			}
			$dados['perfil_permissoes'] = serialize($permissoes);

			$resultado = $this->Perfil_model->editar($dados,$this->input->post('id'));

	    	if($resultado){

	    		if($this->input->post('id') == $this->session->userdata('usuario_perfil')){
	    			$this->session->set_userdata($dados);
	    		}	    		
	    		$this->session->set_flashdata('success','Registro adicionado com sucesso!');
	    	}else{
	    		$this->session->set_flashdata('erro','Erro ao adicionar o registro!');
	    	}

	    	redirect('Perfil','refresh');

	    }else{

	    	$this->load->model('Sistema_model');
			
			$dadosView['dados']   = $this->Perfil_model->listarId($this->uri->segment(3));
			$dadosView['meio']    = 'perfil/permissoes';
			$this->load->view('tema/layout',$dadosView);
	    }

		
	}

}
