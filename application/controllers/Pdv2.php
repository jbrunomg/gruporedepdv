<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class pdv2 extends CI_Controller {

  public function __construct()
	{   
		parent::__construct();
		$this->load->model('PDV_model');		
	}


  public function index(){
 
    if ($this->session->userdata('usuario_id') == null) {
      redirect('sistema','refresh');
    }else{

    $loja = $this->input->get('loja');
    if($loja == NULL){

      $loja = BDCAMINHO;
    }  
       
     // $data['emitente']   = $this->PDV_model->emitente();
     $data['categorias'] = $this->PDV_model->getAllCategorias($loja);
     // $data['produtos']   = $this->ajaxproducts('1', 1);
     $data['filias']     = $this->PDV_model->getAllFilias();
     $data['historico']  = $this->PDV_model->getAllHistorico($this->session->userdata('usuario_id'));
     $data['vendedores']  = $this->PDV_model->getAllVendedores();

     // echo "<pre>"; var_dump($data['filias']); exit();
     $data['meio']       = 'pdv2/pdv';
		 $this->load->view('tema/layout',$data);
    }
	}


  public function selecionarProduto(){
      
      $grupo   = $this->input->post('grupo');
      $filial    = $this->input->post('filial');
      $produto = $this->PDV_model->pegarProdutos($grupo, $filial);

      echo  "<option value=''>Selecionar um produto</option>";
      foreach ($produto as $p) {
          echo "<option value='".$p->produto_id."'>".$p->produto_descricao.' | PVM: '.$p->produto_preco_minimo_venda.' | PV: '.$p->produto_preco_venda."</option>";
      }

  }

  public function estoqueProdutoId(){
      
    $produto   = $this->input->post('produto');
    $filial    = $this->input->post('filial');
    $produto   = $this->PDV_model->pegarProdutosId($produto, $filial);

    echo json_encode($produto);

  }


  public function adicionarVenda()
  {

    $dados = array(                    
      'usuarios_id'   => $this->session->userdata('usuario_id'),
      'dataVenda'     => date('Y-m-d'),
      'horaVenda'     => date("H:i:00"),
      'clientes_id'   => 1                 
    );

    if (is_numeric($id = $this->PDV_model->adicionarVenda($dados)) ) {      

      echo json_encode($id);

    }else{
      
       echo false;
    }

  }


  public function editarVenda()
  {
    
    if ($this->session->userdata('usuario_id') == null) {
      redirect('sistema','refresh');
    }else{

      $loja = $this->input->get('loja');
      if($loja == NULL){

       $loja = BDCAMINHO;
      }
        
      $data['emitente']   = $this->PDV_model->emitente();
      $data['categorias'] = $this->PDV_model->getAllCategorias($loja);

      $id = $this->uri->segment(3);
      
      $data['venda'] = $this->PDV_model->getVenda($id);

      if ($data['venda'][0]->faturado == '1') {
        redirect('pdv2','refresh');
      } else {
      $data['produtos'] = $this->PDV_model->getVendaProdutos($id);
      $data['filias']     = $this->PDV_model->getAllFilias();


      for($i=0; $i<count($data['produtos']); $i++) {
            
            for($j=0; $j<count($data['filias']); $j++) {

                    $filias = explode("_".GRUPOLOJA."_", $data['filias'][$j]->schema_name);

                    if ($data['produtos'][$i]['filial'] == strtoupper($filias[1])) {

                        $data['produtos'][$i]['cor'] = $data['filias'][$j]->cor;
                    }
                 
               }
         
        }


      $data['historico']  = $this->PDV_model->getAllHistorico($this->session->userdata('usuario_id'));
      $data['vendedores']  = $this->PDV_model->getAllVendedores();
      $data['meio']       = 'pdv2/editar';
      $this->load->view('tema/layout',$data);

      }

    }
  }


  public function additensVendas()
  {        
    
      $dados = array(
          'quantidade'      => $this->input->post('quantidade'),
          'subTotal '       => $this->input->post('subTotal'),
          'vendas_id'       => $this->input->post('venda'),
          'produtos_id'     => $this->input->post('produto'),
          'filial'          => $this->input->post('filial'),

          'prod_preco_minimo_venda' => $this->input->post('vlpvm'),
          'prod_preco_venda'         => $this->input->post('vlpv'),
          'prod_preco_custo'         => $this->input->post('vlpc'),
          'prod_preco_cart_debito'   => $this->input->post('vlpcd'),
          'prod_preco_cart_credito'  => $this->input->post('vlpcc'),
          'produtos_codigo'  => $this->input->post('codigo')
      );

      $filial = BDCAMINHO;
      $filial = explode("_", $filial);
      $filial = strtoupper($filial[2]);

      if ($filial == $this->input->post('filial')) {
       
        $validacao = $this->PDV_model->validacao($this->input->post('produto'), $this->input->post('quantidade')); 

        if ($validacao) {


              if($this->PDV_model->additensVendas('itens_de_vendas', $dados) == true){  

                  $this->PDV_model->estoqueProduto($this->input->post('produto'), $this->input->post('quantidade'));                                         

                  echo json_encode(array('result'=> true));
              }else{
                  echo json_encode(array('result'=> false));
              }  

        }else{

          echo json_encode(array('result'=> false));

        }

      }else{

         if($this->PDV_model->additensVendas('itens_de_vendas', $dados) == true){                                           

                  echo json_encode(array('result'=> true));
              }else{
                  echo json_encode(array('result'=> false));
              }  
      }


  }

  public function excluirItens($vendaID = NULL, $idItens = NULL)
  {   
      $loja = $this->input->get('loja');
      if($loja == NULL){

        $loja = BDCAMINHO;
      }

      $filial = BDCAMINHO;
      $filial = explode("_", $filial);
      $filial = strtoupper($filial[2]);

      if ($vendaID == NULL and $idItens == NULL) {
        $venda = $this->input->post('venda');
        $itens = $this->input->post('idItens');
      }else{

        $venda = $vendaID;
        $itens = $idItens;

      }


      $ConsultaItens = $this->PDV_model->consultarEstoque($venda,$itens);

      if ($filial == $ConsultaItens[0]['filial']) {
        $this->PDV_model->estoqueAdd($ConsultaItens[0]['quantidade'], $ConsultaItens[0]['produtos_id']);
      }

      if($this->PDV_model->excluirItensPDV2($venda,$itens) == true){                                           

      $data['emitente']   = $this->PDV_model->emitente();
      $data['categorias'] = $this->PDV_model->getAllCategorias($loja);

      $id = $venda;
      
      $data['venda']     = $this->PDV_model->getVenda($id);
      $data['produtos']  = $this->PDV_model->getVendaProdutos($id);
      $data['filias']     = $this->PDV_model->getAllFilias();


      for($i=0; $i<count($data['produtos']); $i++) {
            
            for($j=0; $j<count($data['filias']); $j++) {

                    $filias = explode("_".GRUPOLOJA."_", $data['filias'][$j]->schema_name);

                    if ($data['produtos'][$i]['filial'] == strtoupper($filias[1])) {

                        $data['produtos'][$i]['cor'] = $data['filias'][$j]->cor;
                    }
                 
               }
         
        }


      if ($vendaID == NULL and $idItens == NULL) {

       echo json_encode(array('result'=> true));

      }else{

        redirect('pdv2/editarVenda/'.$vendaID,'refresh');

      }


      }else{
          echo json_encode(array('result'=> false));
      } 

  }

  public function cancelarVenda()
  {        
    $venda = $this->input->post('venda');
      

    // $this->PDV_model->cancelarVendaPDV2($venda); // Não exclui, fica apenas em aberto.

      $loja = $this->input->get('loja');
      if($loja == NULL){

        $loja = BDCAMINHO;
      }
                 
    $data['emitente']   = $this->PDV_model->emitente();
    $data['categorias'] = $this->PDV_model->getAllCategorias($loja);
       
    $data['venda']     = $this->PDV_model->getVenda($venda);

    $data['produtos']  = $this->PDV_model->getVendaProdutos($venda);
      
    $data['filias']     = $this->PDV_model->getAllFilias();


    for($i=0; $i<count($data['produtos']); $i++) {
          
          for($j=0; $j<count($data['filias']); $j++) {

                  $filias = explode("_".GRUPOLOJA."_", $data['filias'][$j]->schema_name);

                  if ($data['produtos'][$i]['filial'] == strtoupper($filias[1])) {

                      $data['produtos'][$i]['cor'] = $data['filias'][$j]->cor;
                  }
               
             }
       
      }

      echo json_encode(array('result'=> true));
      // $data['filias']    = $this->PDV_model->getAllFilias();
      // $data['meio']      = 'pdv2/editar';
      // $this->load->view('tema/layout',$data);

      // }else{
        
      //     echo json_encode(array('result'=> false));
      // } 

  }
  
  public function selecionarCliente(){
        
        $cliente = $this->PDV_model->selecionarCliente();

        echo  "<option value=''>Selecionar um Cliente</option>";
        foreach ($cliente as $c) {
            echo "<option data-item='".$c->cliente_nome."' value='".$c->cliente_id."'>".$c->cliente_nome."</option>";
        }

    }

  public function autoCompleteClientes()
  {
        $termo = strtolower($this->input->get('term'));
        $this->PDV_model->autoCompleteClientes($termo);
  } 


  public function autoCompleteUsuarios()
  {
        $termo = strtolower($this->input->get('term'));
        $this->PDV_model->autoCompleteUsuarios($termo);
  } 

  public function finalizarVenda()
  {

    if ($this->session->userdata('usuario_id') == null) {
      redirect('sistema','refresh');
    }else{

      $loja = $this->input->get('loja');
   
      if($loja == NULL){
         $loja = BDCAMINHO;
      }
          
      $data['emitente']   = $this->PDV_model->emitente();
      $data['categorias'] = $this->PDV_model->getAllCategorias($loja);

      $id = $this->uri->segment(3);
      
      $data['venda']     = $this->PDV_model->getVenda($id);
      $data['produtos']  = $this->PDV_model->getVendaProdutos($id);
      $data['filias']    = $this->PDV_model->getAllFilias();


      for($i=0; $i<count($data['produtos']); $i++) {
            
          for($j=0; $j<count($data['filias']); $j++) {

            $filias = explode("_".GRUPOLOJA."_", $data['filias'][$j]->schema_name);

            if ($data['produtos'][$i]['filial'] == strtoupper($filias[1])) {

                $data['produtos'][$i]['cor'] = $data['filias'][$j]->cor;
            }
               
          }
         
      }
      $data['historico']  = $this->PDV_model->getAllHistorico($this->session->userdata('usuario_id'));
      $data['vendedores']  = $this->PDV_model->getAllVendedores();
      $data['meio']       = 'pdv2/finalizar';
      $this->load->view('tema/layout',$data);
    }

  } 

  public function fecharVenda()
  {

      $nome_cliente = $this->input->post('nome_cliente');
      $idCliente = $this->input->post('cliente');
      $desconto  = ($this->input->post('desconto') >= 0 and $this->input->post('desconto') != '')  ?  $this->input->post('desconto') : 0 ; 
      $valorFomrPag  = ($this->input->post('forma_pag2_status') == '1')  ?  $this->input->post('forma_pag2_valor') : 0 ; 
      
      if ($nome_cliente == NULL or $idCliente == NULL or $idCliente == '') {
         $nome_cliente = 'Cliente Balcão';
         $idCliente = 1;
      } 

      if ($this->input->post('forma_pag') == 'Sem pagamento') {
        $faturado = '0';
        $baixado = '0';
        $dataPagamento = empty($this->input->post('dataPagamento')) ? date('Y-m-d')  : date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('dataPagamento'))));
        $valorRecebido = ($this->input->post('pago')) ? $this->input->post('pago') : $this->input->post('total') ;
      } else {
        $faturado = '1';
        $baixado = '1';
        $dataPagamento = date('Y-m-d');
        $valorRecebido = $this->input->post('pago');
      }

      $id = $this->input->post('venda');
      $data = array(
        'dataVenda' => date('Y-m-d'),
        'valorTotal' =>  $this->input->post('total'),
        'clientes_id' => $idCliente,
        'usuarios_id' => $this->input->post('vedendor'),
        'desconto'    => $this->input->post('desconto'),
        'emitirNota'  => '0',        
        'valorRecebido'  => ($valorRecebido + $valorFomrPag),
        'observacao'  => '',
        'faturado'  => $faturado
        );

      $filial = BDCAMINHO;
      $filial = explode("_", $filial);
      $consultarFinanceiro = $this->PDV_model->consultarFinanceiro($id);
      $verificarFinanceiro = $this->PDV_model->verificarFinanceiro($id);

      if (!$verificarFinanceiro) {
         foreach ($consultarFinanceiro as $key) {

          if (strtoupper($filial[2]) == $key->filial) { 
            $financeiro = array(
              'financeiro_descricao' => 'Fatura de Venda - #'.$id.' - '.$key->filial,
              'financeiro_valor' => (($key->total - $desconto) - $valorFomrPag),
              'data_vencimento' => date('Y-m-d'),
              'data_pagamento' => $dataPagamento,
              'financeiro_baixado' => $baixado,
              'financeiro_forn_clie' => $nome_cliente,
              'financeiro_forn_clie_id' => $idCliente,
              'financeiro_forma_pgto' => $this->input->post('forma_pag'),
              'financeiro_tipo_compra' => $this->input->post('tipo_compra'),
              'financeiro_tipo' => 'receita',
              'vendas_id' => $id
            );

            $this->PDV_model->financeiro($financeiro);

            if ($this->input->post('forma_pag2_status') == '1') {
              $financeiro_pag2 = array(
                'financeiro_descricao' => 'Fatura de Venda - #'.$id.' - '.$key->filial,
                'financeiro_valor' => $this->input->post('forma_pag2_valor'),
                'data_vencimento' => date('Y-m-d'),
                'data_pagamento' => $dataPagamento,
                'financeiro_baixado' => $baixado,
                'financeiro_forn_clie' => $nome_cliente,
                'financeiro_forn_clie_id' => $idCliente,
                'financeiro_forma_pgto' => $this->input->post('forma_pag2'),
                'financeiro_tipo_compra' => $this->input->post('tipo_compra'),
                'financeiro_tipo' => 'receita',
                'vendas_id' => $id
              );
              $this->PDV_model->financeiro($financeiro_pag2);
            }

          } 
          // Bruno desconmentou 020620
          else{

                $financeiro = array(
                  'financeiro_descricao' => 'Fatura de Venda - #'.$id.' - '.$key->filial,
                  'financeiro_valor' => ($key->total - $valorFomrPag),
                  'data_vencimento' => date('Y-m-d'),
                  'data_pagamento' => $dataPagamento,
                  'financeiro_baixado' => '1',
                  'financeiro_forn_clie' => $nome_cliente,
                  'financeiro_forn_clie_id' => $idCliente,
                  'financeiro_forma_pgto' => $this->input->post('forma_pag'),
                  'financeiro_tipo' => 'despesa',
                  'vendas_id' => $id,
                  'financeiro_visivel' => 0 // Para não contabilizar no financeiro. (Apenas relacionar com a venda)
                );

              $this->PDV_model->financeiro($financeiro);

              // if ($this->input->post('forma_pag2_status') == '1') {
              //   $financeiro_pag2 = array(
              //     'financeiro_descricao' => 'Fatura de Venda - #'.$id.' - '.$key->filial,
              //     'financeiro_valor' => $this->input->post('forma_pag2_valor'),
              //     'data_vencimento' => date('Y-m-d'),
              //     'data_pagamento' => $dataPagamento,
              //     'financeiro_baixado' => '1',
              //     'financeiro_forn_clie' => $nome_cliente,
              //     'financeiro_forn_clie_id' => $idCliente,
              //     'financeiro_forma_pgto' => $this->input->post('forma_pag2'),
              //     'financeiro_tipo' => 'despesa',
              //     'vendas_id' => $id,
              //     'financeiro_visivel' => 0 // Para não contabilizar no financeiro. (Apenas relacionar com a venda)
              //   );
              //   $this->PDV_model->financeiro($financeiro_pag2);
              // }

          }
        } 
      }else{

          foreach ($consultarFinanceiro as $key) {

            if (strtoupper($filial[2]) == $key->filial) { 

              $financeiro = array(
                'data_pagamento' => date('Y-m-d'),
                'financeiro_baixado' => $baixado,
                'financeiro_forma_pgto' => $this->input->post('forma_pag'),
              );

              $this->PDV_model->financeiroUpdate($financeiro, $key->idFinanceiro);

            } 
            // Bruno desconmentou 020620
            else{

              $financeiro = array(
                'data_pagamento' => date('Y-m-d'),
                'financeiro_baixado' => $baixado,
                'financeiro_forma_pgto' => $this->input->post('forma_pag'),
              );

              $this->PDV_model->financeiroUpdate($financeiro, $key->idFinanceiro);

            }
          } 

      }


      if($this->PDV_model->finalizarVenda($data, $id) == true){                                        
          
          echo json_encode(array('result'=> true));
      }else{
          echo json_encode(array('result'=> false));
      } 
   }


  public function sairVenda()
  {
      $id = $this->input->post('venda');

      if($this->PDV_model->excluirVenda($id) == true){                                       
          echo json_encode(array('result'=> true));
      }else{
          echo json_encode(array('result'=> false));
      } 
   }


  public function addCliente(){

       $dados = array(                    
          'cliente_cpf_cnpj'            => $this->input->post('cpfCnpj'),
          'cliente_tipo'                => $this->input->post('tipo'),
          'cliente_nome'                => $this->input->post('cliente_nomeModal'),
          'cliente_endereco'            => $this->input->post('cliente_endereco'),
          'cliente_celular'             => $this->input->post('cliente_numero'),
          'cliente_data_cadastro'       => date('Y-m-d'),
          'cliente_email'               => $this->input->post('cliente_email'),                  
          );
        
        $cliente = $this->PDV_model->addCliente($dados);

    }

  public function verificarCliente(){

      $dados = array(                    
         'cliente_cpf_cnpj' => $this->input->post('cpfCnpj')               
      );
       
      if($this->PDV_model->verificarCliente($dados)){                                       
        echo json_encode(true);
      }else{
        echo json_encode(false);
      } 

   }


  public function validacaoGerente()
  {
      $senha = sha1(md5(strtolower($this->input->post('senha'))));

      if($this->PDV_model->validacaoGerente($senha) == true){ 

          echo json_encode(array('result'=> true));
      }else{
          echo json_encode(array('result'=> false));
      } 
   }


  public function adicionarRetirada()
  {
    # Retirada parcial do CAIXA

    $dados = array(
      'valor'        => $this->input->post('valor'),
      'dataRetirada' => date('Y-m-d H:i:s'),
      'usuario_id'   => $this->input->post('usuario_id')
    );


    if($this->PDV_model->addRetirada('sangria_pdv', $dados) == true){ 

      $this->session->set_flashdata('success','Registro editado com sucesso!');
      redirect('pdv2','refresh'); 

    } else {

      $this->session->set_flashdata('error','Erro ao editar o registro!');
      redirect('pdv2','refresh'); 

    }

  } 


}


