<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cadastroimei extends CI_Controller {

		public function __construct()
	{
		parent::__construct();
		$this->load->model('Cadastroimei_model');
        $this->load->model('Movimentacao_model');
	}

	public function index()
	{
		$dadosView['dados'] = $this->Cadastroimei_model->listar();

		$dadosView['meio'] = 'cadastroimei/listar';
		$this->load->view('tema/layout',$dadosView);
	}

    public function editar()
	{
        $consultaTipo = $this->Movimentacao_model->consultaTipo($this->uri->segment(3));

        $dadosView['categoria_prod'] = $this->Cadastroimei_model->pegarCategoriaProd($this->uri->segment(3));

        //var_dump($dadosView['categoria_prod']);die();pegarCategoriaProd

        $dadosView['dados']   = $this->Cadastroimei_model->listarId($this->uri->segment(3), $consultaTipo[0]->movimentacao_produto_saida_tipo);
	    $dadosView['meio']    = 'cadastroimei/editar';

		$this->load->view('tema/layout',$dadosView);

	}

    public function selecionarProduto(){

        $grupo   = $this->input->post('grupo');
        $idMovimento   = $this->input->post('idMovimento');
        $produto = $this->Cadastroimei_model->getProdutosListGrupo($grupo, $idMovimento);

        echo  "<option value=''>Selecione um produto</option>";
        foreach ($produto as $p) {
            $quantidade = $this->Cadastroimei_model->consultaQuantidade($p->idProduto, $idMovimento);
            echo "<option value='".$p->idProduto."' dataProdutoDesc='".$p->descricaoProduto."' dataQuant='".$p->quantidadeProduto."'>".$p->descricaoProduto." -- ".$p->quantidadeProduto."/".$quantidade."</option>";
        }

    }

    public function adicionarProduto(){

        $this->form_validation->set_rules('grupo', 'grupo', 'trim|required');
        $this->form_validation->set_rules('idMovimento', 'idMovimento', 'trim|required');
        $this->form_validation->set_rules('imei', 'Imei já existe e', 'trim|required|is_unique[itens_de_imei.imei_valor]');
        $this->form_validation->set_rules('produto', 'produto', 'trim|required');

        if($this->form_validation->run() == FALSE)
        {

            echo json_encode(array('result'=> false, 'erro'=> str_replace("</p>", "", str_replace("<p>", "", validation_errors()))));

        }else{

            $grupo   = $this->input->post('grupo');
            $idMovimento = $this->input->post('idMovimento');
            $imei        = $this->input->post('imei');
            $bateria     = $this->input->post('bateria');
            $obs         = $this->input->post('obs');
            $produto     = $this->input->post('produto');

            $quantidade = $this->Cadastroimei_model->consultaQuantidade($produto, $idMovimento);
            if($this->input->post('quantidadeProduto') <= $quantidade){

                echo json_encode(array('result'=> false, 'erroQuantidade'=> 'Ocorreu um erro, quantidade produto chegou ao limite!'));

            }else{

                $data = array(
                    'categoria_prod_id'      => $grupo,
                    'movimentacao_id'        => $idMovimento,
                    'imei_valor'             => $imei,
                    'imei_bateria'           => $bateria,
                    'imei_obs'               => $obs,
                    'produtos_id'            => $produto,
                    'usuario_id'             => $this->session->userdata('usuario_id')
                );

                $idItens = $this->Cadastroimei_model->add('itens_de_imei', $data);
                if ($idItens) {
                    echo json_encode(array('result'=> true, 'id'=> $idItens));
                } else {
                    echo json_encode(array('result'=> false, 'erro'=> 'Ocorreu um error entre em contato com o Suporte!'));
                }

            }

        }

    }

    public function validarImei(){

        $imei   = $this->input->post('imei');
        $produto = $this->Cadastroimei_model->getImeisList($imei);

        if ($produto) {
            if ($produto[0]->vendas_id) {
                echo json_encode(array('result'=> true, "venda"=> true));
                return;
            }
            echo json_encode(array('result'=> true, "venda"=> false));
        } else {
            echo json_encode(array('result'=> false, "venda"=> false));
        }
    }

    public function editarImeiProdutoVendido()
    {
        $imei = $this->input->post('imei');
        $usuario = $this->session->userdata('usuario_id');

        // verificar se o IMEI já foi cadastrado
        $sql = "SELECT * FROM itens_de_imei WHERE imei_valor = $imei AND `imei_visivel` = 1 ";
        $result =  $this->db->query($sql)->result();

        if ($result) {
            $imeiR = $imei. '-R';

            $data = array('usuario_id' => $usuario, 'imei_valor' => $imeiR);
            $imeiId = $result[0]->imei_id;


            if($this->Cadastroimei_model->edit("itens_de_imei",$data,'imei_id',$imeiId) == false){
                echo json_encode(array('result'=> false, 'infor' => 'O IMEI não foi encontrado' ));
                return;
            }

            echo json_encode(array('result'=> true, 'infor' => 'IMEI editado' ));
        } else {
            echo json_encode(array('result'=> false, 'infor' => 'O IMEI não foi encontrado' ));
        }
    }

    public function excluirProduto()
    {

    $id = $this->uri->segment(3);
    $idMovimento = $this->uri->segment(4);
    $pedido_visivel = 0 ;

    $resultado = $this->Cadastroimei_model->excluir($pedido_visivel,$id);

        if($resultado){
            $this->session->set_flashdata('success','Dados excluidos com sucesso!');
        }else{
            $this->session->set_flashdata('erro','Erro ao excluir os dados!');
        }

        redirect('cadastroimei/editar/'.$idMovimento,'refresh');

    }



    public function buscarItensProdutos()
    {
        $dados = $this->Cadastroimei_model->getProdutosList($_POST['id']);
        echo json_encode($dados);
    }






}
