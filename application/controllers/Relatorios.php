<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Relatorios extends CI_Controller {

	public function __construct()
	{   
		parent::__construct();
		$this->load->model('Relatorios_model');
		$this->load->model('Sistema_model');

		$this->load->library('Ciqrcode');
		$this->load->library('Zend');
	}

	public function clientes()
	{

		if($this->input->post() || $this->uri->segment(3) <> null){

			if($this->uri->segment(3) == 'todos'){
				$dadosView['dados']    = $this->Relatorios_model->clientes();
			}

			if($this->uri->segment(3) == 'mes'){
				$mes = true;
				$dadosView['dados']    = $this->Relatorios_model->clientes($mes);
			}
			
			$dadosView['emitente'] = $this->Sistema_model->pegarEmitente();
			$dadosView['meio']     = 'relatorios/clientes/clientes_simples';

			$html = $this->load->view('relatorios/impressao',$dadosView, true);	
			
			gerarPDF($html);
		} else {
			$dadosView['totalcliente']        = $this->Relatorios_model->clientesTotal();	
			$dadosView['totalAniversariante'] = $this->Relatorios_model->clientesTotalAnive();
			$dadosView['totalSemComprar']     = $this->Relatorios_model->clientesTotalSemComprar();
			$dadosView['clientesRPA']        = $this->Relatorios_model->clientesRPA();
			$dadosView['meio']     = 'relatorios/clientes/clientes';
			$this->load->view('tema/layout',$dadosView);
		}
	}


	public function clientesSemComprar()
	{
		$mes = $this->uri->segment(3);
				
		$dadosView['dados']    = $this->Relatorios_model->clientesSemComprar($mes);

		// var_dump($dadosView['dados']);die();			
		
		$dadosView['emitente'] = $this->Sistema_model->pegarEmitente();
		$dadosView['meio']     = 'relatorios/clientes/clientes_sem_comprar';

		$html = $this->load->view('relatorios/impressao',$dadosView, true);	
		
		gerarPDF($html);			
		
	}


	public function clientesVendasCustom()
	{	
		// die('AQUI');
		$dataInicio  = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('dataInicio'))));	
		$dataFim     = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('dataFim'))));

		$tipoRelatorio     = $this->input->post('tipo'); // Resulmido/Detalhado
		$formato  = 'A4-L'; 
		// $vedendor = $this->input->post('usuario_id');
		$cliente  = $this->input->post('cliente_id');
		
		$ordenacao = $this->input->post('ordenacao');
		
		
        $clieVendas = $this->Relatorios_model->clienteVendasCustomizadas($dataInicio, $dataFim, $cliente,  $ordenacao); 


       // v($clieVendas);

        if ($tipoRelatorio == '1') {    	
	  
	        $itensVendas = $this->Relatorios_model->clienteVendasCustomizadasItens($dataInicio, $dataFim, $cliente);

            $dadosView['clieVendas'] = $clieVendas;
	        $dadosView['itensVendas'] = $itensVendas;
	        $dadosView['meio']  = 'relatorios/clientes/clieVenda_customizado';

         } else {

         	$dadosView['meio']  = 'relatorios/clientes/clieVenda_customizado';
         	$dadosView['clieVendas'] = $clieVendas;

        }    

        $dadosView['titulo']        =  ' de: '.$this->input->post('dataInicio').' a '.$this->input->post('dataFim'); 
		$dadosView['emitente']      =  $this->Sistema_model->pegarEmitente();
		$dadosView['tipoRelatorio'] =  $tipoRelatorio;
		$html = $this->load->view('relatorios/impressao',$dadosView, true);

		// echo $html;	
				
		gerarPDF($html, $formato );

	}


	
	public function clientesPagamentoAberto()
	{	

		$cliente  = $this->input->post('cliente_idRPA');
        $dadosPago = $this->Relatorios_model->detalhamentoPago($cliente);
		$dadosCompra = $this->Relatorios_model->detalhamentoCompra($cliente);
		$dadosAberto = $this->Relatorios_model->detalhamentoAberto($cliente);

		foreach ($dadosPago as $pago) {

			foreach ($dadosCompra as $compra) {
				// code...
				if (($pago->mes == $compra->mes) and ($pago->ano == $compra->ano) ) {
					// code...
					$pago->totalCompra = $compra->totalCompra; 
					break;
				}else{
					$pago->totalCompra = 0;
				}
			}

			foreach ($dadosAberto as $aberto) {
				// code...
				if (($pago->mes == $aberto->mes) and ($pago->ano == $aberto->ano) ) {
					// code...
					$pago->totalAberto = $aberto->totalAberto; 
					break;
				}else{
					$pago->totalAberto = 0;
				}
			}

		}

	//	v($dadosPago);

		$dadosView['dados'] = $dadosPago;
		$dadosView['meio']  = 'relatorios/clientes/clientes_pagamento_aberto';
        $dadosView['titulo']        =  'Relatório de Pagamento Em Aberto'; 
		$dadosView['emitente']      =  $this->Sistema_model->pegarEmitente();
		$formato  = 'A4-L'; 
		$html = $this->load->view('relatorios/impressao',$dadosView, true);

		// echo $html;	
				
		gerarPDF($html, $formato );

		
	}


	public function vendedorExterno()
	{
		
		$dadosView['grupo'] = $this->Relatorios_model->pegarGrupoProduto();
		
		$dadosView['meio']  = 'relatorios/vendedorExt/vendedorExt';
		$this->load->view('tema/layout',$dadosView);
		
	}


	public function produtos()
	{

		if($this->input->post() || $this->uri->segment(3) <> null){

			if($this->uri->segment(3) == 'todos'){
				$dadosView['dados']    = $this->Relatorios_model->produtos();
			}

			if($this->uri->segment(3) == 'minimo'){				
				$dadosView['dados']    = $this->Relatorios_model->estoqueMinimo();
			}

			if($this->uri->segment(3) == 'valorestoque'){				
				$dadosView['dados']    = $this->Relatorios_model->valorEstoque();
			}

			if($this->uri->segment(3) == 'valorestoquegrupo'){				
				$dadosView['dados']    = $this->Relatorios_model->valorEstoqueGrupo();
				//var_dump($dadosView['dados']);die();
			}
			
			$dadosView['emitente'] = $this->Sistema_model->pegarEmitente();
			$dadosView['meio']     = 'relatorios/produtos/produtos_simples';

			$html = $this->load->view('relatorios/impressao',$dadosView, true);	
			
			gerarPDF($html);

		} else {

			$dadosView['totalProduto']           = $this->Relatorios_model->totalProduto();	
			$dadosView['totalEstoqueMinimo']     = $this->Relatorios_model->totalEstoqueMinimo();
			$dadosView['totalValorEstoque']      = $this->Relatorios_model->totalValorEstoque();
			

			$dadosView['grupo'] = $this->Relatorios_model->pegarGrupoProduto();
			
			$dadosView['meio']     = 'relatorios/produtos/produtos';
			$this->load->view('tema/layout',$dadosView);
		}
	}

	public function produtosEtiquetaCustom()
	{	

		$tipo  = $this->input->post('tipo_etiqueta');
		$grupo = $this->input->post('produto_categoria_id');

		
		$dadosView['tipo']       =  $tipo;
		$etiquetas  =  $this->Relatorios_model->pegarEtiquetaProduto($grupo);

		if ($tipo == '01') { // Cód Barra
			foreach ($etiquetas as $e) {			
				$e->url = base_url('relatorios/Barcode/'.$e->produto_codigo_barra);
				
			}	
		}

		if ($tipo == '02') { // Qr-Code			
			foreach ($etiquetas as $e) {
				$e->url = base_url('relatorios/QRcode/'.$e->produto_codigo_barra);
				
			}	
		}

		$dadosView['etiquetas'] =  $etiquetas;
		$dadosView['emitente']  =  $this->Sistema_model->pegarEmitente();
		//$dadosView['meio']    = 'relatorios/produtos/produtos_etiquetas';

		$this->load->view('relatorios/produtos/produtos_etiquetas',$dadosView);
				
		// gerarPDF($html);

	}

	public function produtosAvaria()
	{	

		$dataInicio  = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('dataInicio'))));	
		$dataFim = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('dataFim'))));
		$motivo = $this->input->post('motivo');

        $produto_avaria = $this->Relatorios_model->produtosAvaria($dataInicio, $dataFim, $motivo);

        $dadosView['dados']  = $produto_avaria;
        $dadosView['titulo'] = ' de: '.$this->input->post('dataInicio').' a '.$this->input->post('dataFim');
		$dadosView['emitente']  =  $this->Sistema_model->pegarEmitente();
		$formato = 'A4-L';
		$dadosView['meio']     = 'relatorios/produtos/produtos_avaria';
		$html = $this->load->view('relatorios/impressao',$dadosView, true);	
				
		gerarPDF($html, $formato );

	}


	public function produtosVendidosCustom()
	{	

		$dataInicio  = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('dataInicio'))));	
		$dataFim     = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('dataFim'))));

		$limite    = $this->input->post('limite');
		$grupo     = $this->input->post('produto_categoria_id');
		$unificar  = $this->input->post('unificar');

		


        $dadosView['dados'] = $this->Relatorios_model->produtosVendidos($dataInicio, $dataFim, $limite, $grupo, $unificar );
       
		//var_dump($dadosView['dados']);die();

		$dadosView['emitente']  =  $this->Sistema_model->pegarEmitente();

		$dadosView['meio']     = 'relatorios/produtos/produtos_vendidos';

		$html = $this->load->view('relatorios/impressao',$dadosView, true);	
				
		gerarPDF($html);

	}


	public function produtosParadoCustom()
	{	

		$limite  = $this->input->post('limite');
		$grupo = $this->input->post('produto_categoria_id');



        $dadosView['dados'] = $this->Relatorios_model->produtosParado($limite, $grupo );
       
		$dadosView['emitente']  =  $this->Sistema_model->pegarEmitente();

		$dadosView['meio']     = 'relatorios/produtos/produtos_parado';

		$html = $this->load->view('relatorios/impressao',$dadosView, true);	
				
		gerarPDF($html);

	}


	public function produtosVendasCustom()
	{	
	
		$dataInicio  = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('dataInicio'))));	
		$dataFim = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('dataFim'))));
		$produto = $this->input->post('produto_desc');


        $dadosView['dados'] = $this->Relatorios_model->produtosVendas($dataInicio, $dataFim, $produto );        
       
		$dadosView['emitente']  =  $this->Sistema_model->pegarEmitente();

		$dadosView['data']     = $this->input->post('dataInicio').' - '.$this->input->post('dataFim');

		$dadosView['meio']     = 'relatorios/produtos/produtosVendas';

		$html = $this->load->view('relatorios/impressao',$dadosView, true);	
				
		gerarPDF($html);

	}

   public function produtosCustom()
	{	
		$produto        = $this->input->post('produto_desc');
		$categoria      = $this->input->post('produto_categoria_id');
		$tipo_ordenacao = $this->input->post('tipo_ordenacao');

		$estoque   = $this->input->post('estoqueProduto');
		$imei      = $this->input->post('exibirImei');

        $dadosView['dados']  = $this->Relatorios_model->produtosCustom($categoria, $produto, $estoque, $tipo_ordenacao ,$imei);

        $dadosView['preco']  =  $this->input->post('precoProduto');

        $dadosView['imei']   =  $this->input->post('exibirImei');
       
		$dadosView['emitente']  =  $this->Sistema_model->pegarEmitente();

		$dadosView['meio']     = 'relatorios/produtos/produtosCustom';

		$html = $this->load->view('relatorios/impressao',$dadosView, true);	
				
		gerarPDF($html);

	}

	public function produtosCatalogoCustom()
	{	
		$produto   = $this->input->post('produto_desc');
		$categoria = $this->input->post('produto_categoria_id');
		$estoque   = $this->input->post('estoque');
		$tipo_ordenacao   = $this->input->post('tipo_ordenacao');

        $dadosView['dados'] = $this->Relatorios_model->produtosCatalogoCustom($categoria, $produto, $estoque, $tipo_ordenacao);

        $dadosView['pc']   =  $this->input->post('pc');
        $dadosView['pa']   =  $this->input->post('pa');
        $dadosView['pr']   =  $this->input->post('pr');
        $dadosView['pv']   =  $this->input->post('pv');
        $dadosView['qtd']  =  $this->input->post('qtd');
       
		$dadosView['emitente']  =  $this->Sistema_model->pegarEmitente();

		$dadosView['meio']      = 'relatorios/produtos/produtosCatalogoCustom';

		$html = $this->load->view('relatorios/impressao',$dadosView, true);	
				
		gerarPDF($html);

	}


	public function produtosVendasPMVCustom()
	{
		$dataInicio  = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('dataInicio'))));	
		$dataFim     = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('dataFim'))));
		$categoria   = $this->input->post('produto_categoria_id');


        $dadosView['dados']     = $this->Relatorios_model->produtosVendasPMV($dataInicio, $dataFim, $categoria );        
       	
       //	var_dump($dadosView['dados']);die();

		$dadosView['emitente']  = $this->Sistema_model->pegarEmitente();

		$dadosView['data']      = $this->input->post('dataInicio').' - '.$this->input->post('dataFim');

		$dadosView['meio']      = 'relatorios/produtos/produtosVendasPMV';

		$html = $this->load->view('relatorios/impressao',$dadosView, true);	
				
		gerarPDF($html);
	}




   public function vendas()
	{

		if($this->input->post() || $this->uri->segment(3) <> null){

			if($this->uri->segment(3) == 'vendasDia'){
				$dadosView['dados']    = $this->Relatorios_model->VendasDia();
				$dadosView['emitente'] = $this->Sistema_model->pegarEmitente();
				$dadosView['meio']     = 'relatorios/vendas/vendas_padrao';
				$dadosView['data']     =  date("d/m/Y");
				$html = $this->load->view('relatorios/impressao',$dadosView, true);	
				
				gerarPDF($html);
			} elseif ($this->uri->segment(3) == 'vendaAnual') {
				
				$dadosView['dados'] = $this->Relatorios_model->VendasAnual();
				$dadosView['emitente'] = $this->Sistema_model->pegarEmitente();
				$dadosView['tipo'] = 'Geral';
				$dadosView['meio']     = 'relatorios/vendas/vendas_anual';
				$html = $this->load->view('relatorios/impressao',$dadosView, true);	
				
				gerarPDF($html);

			} elseif ($this->uri->segment(3) == 'vendaAnualLoja') {
				
				$dadosView['dados'] = $this->Relatorios_model->VendasAnualLoja();
				$dadosView['emitente'] = $this->Sistema_model->pegarEmitente();
				$dadosView['tipo'] = 'Loja';
				$dadosView['meio']     = 'relatorios/vendas/vendas_anual';
				$html = $this->load->view('relatorios/impressao',$dadosView, true);	
				
				gerarPDF($html);

			} elseif ($this->uri->segment(3) == 'VendasDiaDashbord') {
				
				$dadosView['dados'] = $this->Relatorios_model->VendasDiaDashbord();
				$dadosView['emitente'] = $this->Sistema_model->pegarEmitente();
				$dadosView['data']     =  date("d/m/Y");
				$dadosView['meio']     = 'relatorios/vendas/vendas_dashbord';
				$html = $this->load->view('relatorios/impressao',$dadosView, true);	
				
				gerarPDF($html);

			}else{

                $dadosView['dados'] = $this->Relatorios_model->VendasMensal($this->uri->segment(4));

              
                $y = 0;
                foreach ($dadosView['dados'] as $v) {
             	

                	$valor = explode(',', $v['cart_cred_liquido1']);
                	$perc  = explode(',', $v['cart_cred_liquido2']);
                	$saldo = 0;                	

                	for ($i=0; $i < count($valor) ; $i++) { 
                		
                		for ($j=0; $j < count($perc) ; $j++) { 
                			// Valor da transação - percentual banco 

                			if ($i == $j) {   

                			if (is_numeric($valor[$i]) && is_numeric($perc[$j])) {
							  $saldo = $saldo + ( $valor[$i] * ($perc[$j]/100));
							}      			

               
                			continue;
						
						} else {

							continue;
						}

                		}
           
                	}                

                	$dadosView['dados'][$y]['cart_cred_liquido']  = $v['cart_cred_bruto'] - $saldo;

                    $dadosView['dados'][$y]['cart_deb_liquido']   = 0;

                	$y++;
                	
                }        


                $dadosView['parametros'] = $this->Relatorios_model->getAllParametros(); 
				$dadosView['emitente'] = $this->Sistema_model->pegarEmitente();
				$dadosView['meio']     = 'relatorios/vendas/vendas_mensal';
				$html = $this->load->view('relatorios/impressao',$dadosView, true);	
				
				gerarPDF($html);

			}
			

		}else{

			$base = explode('_', BDCAMINHO);
			$dadosView['totalVendasDia']           = $this->Relatorios_model->totalVendasDia();
			$dadosView['totalVendasMesAtual']      = $this->Relatorios_model->totalVendasMesAtual();	
            $dadosView['totalVendasMesAnterior']   = $this->Relatorios_model->totalVendasMesAnterior();
            $dadosView['totalVendasAnual']         = $this->Relatorios_model->totalVendasAnual();
            $dadosView['totalVendasAnualLoja']     = $this->Relatorios_model->totalVendasAnualLoja();
            $dadosView['fornecedor']   = $this->Relatorios_model->pegarFornecedor();
			$dadosView['grupo']        = $this->Relatorios_model->pegarGrupoProduto();

			$dadosView['grupoCartao']  = $this->Relatorios_model->pegarGrupoCartao();

			$dadosView['vendedor']     = $this->Relatorios_model->pegarVendedor();			
			$dadosView['base']         = $base[2];
			$dadosView['lojas']        = $this->Relatorios_model->getAllFilias();
			$dadosView['meio']         = 'relatorios/vendas/vendas';
			$this->load->view('tema/layout',$dadosView);
		}
	}

    public function vendasCustomizadas()
	{	
		$dataInicio  = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('dataInicio'))));	
		// $dataFim = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('dataFim'))));
		$vedendor = $this->input->post('usuario_id');
		$cliente = $this->input->post('cliente_id');
		$lojas = $this->input->post('lojas');
		$ordenacao = $this->input->post('ordenacao');
		$tipo = $this->input->post('tipo');
		$faturado = $this->input->post('faturado');
        
		$vendas = $this->Relatorios_model->vendasCustomizadas($dataInicio, $vedendor, $cliente, $lojas, $ordenacao,$faturado);

		//var_dump($vendas);die();

        foreach ($vendas as $key) {
        	$itensVendas[]     = $this->Relatorios_model->vendasCustomizadasItens($key['idVendas'], $lojas);

        	$idVendas[] = $key['idVendas'];
        	
        }

        if (!empty($idVendas)) {
        	$dadosView['financeiro']  = $this->Relatorios_model->vendasFinanceiroCustomizadas($idVendas);
        }
     

         for($i=0; $i<count($vendas); $i++) {
            
            $itens = array();

            for($j=0; $j<count($itensVendas); $j++) {

                for($k=0; $k<count($itensVendas[$j]); $k++){
                    if ($vendas[$i]["idVendas"] === $itensVendas[$j][$k]["vendas_id"]) {
                        array_push($itens, $itensVendas[$j][$k]);
                    }
                }
          }
          array_push($vendas[$i], $itens);
        }

        // echo "<pre>"; var_dump($vendas); exit();

        $dadosView['dados'] = $vendas;
        $dadosView['data']  = $this->input->post('dataInicio');
		$dadosView['emitente']  =  $this->Sistema_model->pegarEmitente();

		if ($tipo == '1') {
		   $dadosView['meio']     = 'relatorios/vendas/vendas_padrao';
		}else{
		   $dadosView['meio']     = 'relatorios/vendas/vendas_customizadas';
		}
		
		$html = $this->load->view('relatorios/impressao',$dadosView, true);	
				
		gerarPDF($html);

	}

	public function vendasAnualGrupoCustomizado()
	{	

		$data = explode('/', $this->input->post('dataInicio'));
		$ano  = $data[1];	
		$mes  = $data[0];	
		$grupo = $this->input->post('produto_categoria_id');
 
        $dadosView['dados'] = $this->Relatorios_model->VendasMensalGrupoTotal($ano, $mes);

        $dadosView['detalhes'] = $this->Relatorios_model->VendasMensalGrupoDetalhe($ano, $mes, $grupo);

		$dadosView['emitente']  =  $this->Sistema_model->pegarEmitente();

	    $dadosView['meio']     = 'relatorios/vendas/vendas_anualGrupo';
		
		$html = $this->load->view('relatorios/impressao',$dadosView, true);	
				
		gerarPDF($html);

	}

	public function vendasVendasExcluidasCustomizado()
	{	

		$data = explode('/', $this->input->post('dataInicio'));
		$ano  = $data[1];	
		$mes  = $data[0];	
		$vendedor = $this->input->post('usuario_id');

		$dadosView['data']     = $mes.' - '.$ano;
 
        $dadosView['dados'] = $this->Relatorios_model->VendasExcluidas($ano, $mes, $vendedor);

		$dadosView['emitente']  =  $this->Sistema_model->pegarEmitente();

	    $dadosView['meio']     = 'relatorios/vendas/vendas_excluidas';
		
		$html = $this->load->view('relatorios/impressao',$dadosView, true);	
				
		gerarPDF($html);

	}

	public function vendasAnualCustomizado()
	{	

		// $data = explode('/', $this->input->post('anoInicio'));
		$ano  = $this->input->post('anoInicio');	
			
		$tipo = $this->input->post('tipo');
		
 
 		if($tipo == 'vendaAnual') {
				
				$dadosView['dados'] = $this->Relatorios_model->VendasAnual($ano);
				$dadosView['emitente'] = $this->Sistema_model->pegarEmitente();
				$dadosView['tipo'] = 'Geral';
				$dadosView['meio']     = 'relatorios/vendas/vendas_anual';
				$html = $this->load->view('relatorios/impressao',$dadosView, true);	
				
				gerarPDF($html);

			} elseif ($tipo == 'vendaAnualLoja') {
				
				$dadosView['dados'] = $this->Relatorios_model->VendasAnualLoja($ano);
				$dadosView['emitente'] = $this->Sistema_model->pegarEmitente();
				$dadosView['tipo'] = 'Loja';
				$dadosView['meio']     = 'relatorios/vendas/vendas_anual';
				$html = $this->load->view('relatorios/impressao',$dadosView, true);	
				
				gerarPDF($html);

			}

	}


	public function vendasFornecedorCustomizado()
	{	

		$dataIni = explode('/', $this->input->post('dataInicio'));
		$dataFim = explode('/', $this->input->post('dataFim'));
	
		$fornecedor = $this->input->post('fornecedor_id');
		$vendedor   = $this->input->post('usuario_id');
		$grupo      = $this->input->post('produto_categoria_id');

      $dadosView['dados']     = $this->Relatorios_model->vendasFornecedor($dataIni, $dataFim, $fornecedor, $vendedor, $grupo);

		$dadosView['emitente']  =  $this->Sistema_model->pegarEmitente();

	    $dadosView['meio']     = 'relatorios/vendas/vendas_fornecedores';
		
		$html = $this->load->view('relatorios/impressao',$dadosView, true);	
				
		gerarPDF($html);

	}



	public function vendasPagamentoCustomizado()
	{	

		$dataIni  = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('dataInicio'))));	
		$dataFim  = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('dataFim'))));
			
		$vendedor  = $this->input->post('usuario_id');		

        $dadosView['dados']     = $this->Relatorios_model->vendasPagamento($dataIni, $dataFim,  $vendedor);


        // var_dump( $dadosView['dados'] );die();

		$dadosView['emitente']  =  $this->Sistema_model->pegarEmitente();

	    $dadosView['meio']      = 'relatorios/vendas/vendas_pagamento';
		
		$html = $this->load->view('relatorios/impressao',$dadosView, true);	
				
		gerarPDF($html);

	}

	public function vendasPagamentoCartao()
	{	

		$dataIni  = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('dataInicio'))));	
		$dataFim  = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('dataFim'))));

		$maquina  = $this->input->post('nome_credenciadora');
			
		$dadosView['dados']     = $this->Relatorios_model->vendasPagamentoCartao($dataIni, $dataFim, $maquina);

		$dadosView['emitente']  =  $this->Sistema_model->pegarEmitente();

	    $dadosView['meio']      = 'relatorios/vendas/vendas_pagamentoCartao';
		
		$html = $this->load->view('relatorios/impressao',$dadosView, true);	
				
		gerarPDF($html);

	}

	public function vendedores()
	{

		if($this->input->post() || $this->uri->segment(3) <> null){

			if($this->uri->segment(3) == 'vendedoresDia'){
				$dadosView['dados']    = $this->Relatorios_model->vendedoresDia();
			}

			if($this->uri->segment(3) == 'vendedoresDiaOutraloja'){
				$dadosView['dados']    = $this->Relatorios_model->vendedoresDiaOutraloja();
			}
			
			
			$dadosView['emitente'] = $this->Sistema_model->pegarEmitente();
			$dadosView['meio']     = 'relatorios/vendedores/vendedores_simples';

			$html = $this->load->view('relatorios/impressao',$dadosView, true);	
			
			gerarPDF($html);
		}else{
			$dadosView['totalVendasDia']           = $this->Relatorios_model->totalVendasVendedoresDia();
			$dadosView['totalVendasDiaOutraLoja']  = $this->Relatorios_model->totalVendasVendedoresDiaOutraloja();	

			$dadosView['grupo'] = $this->Relatorios_model->pegarGrupoProduto();
			$dadosView['lojas'] = $this->Relatorios_model->getAllFilias();
			$dadosView['meio']     = 'relatorios/vendedores/vendedores';
			$this->load->view('tema/layout',$dadosView);
		}
	}

    public function vendedoresCustomizadas()
	{	
		$dataInicio  = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('dataInicio'))));	
		$dataFim = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('dataFim'))));
		$vedendor = $this->input->post('usuario_id');
		$ordenacao = $this->input->post('ordenacao');
		$loja = $this->input->post('lojas');
		$formato = $this->input->post('formato');


        $vendas = $this->Relatorios_model->vendedoresCustomizadas($dataInicio, $dataFim, $vedendor, $ordenacao, $loja);
 
        $dadosView['detalhes'] = $this->Relatorios_model->vendedoresCustoGrupoDetalhe($dataInicio, $dataFim, $vedendor);
        
        $dadosView['dados'] = $vendas;
	
		$dadosView['emitente']  =  $this->Sistema_model->pegarEmitente();

		$dadosView['meio']     = 'relatorios/vendedores/vendedores_padrao';

        $html = $this->load->view('relatorios/impressao', $dadosView, true);				
		gerarPDF($html, $formato);

	}


	public function vendedoresCelularCustomizadas()
	{	

		$dataIni   = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('dataInicio'))));	
		$dataFim   = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('dataFim'))));
		$vendedor  = $this->input->post('usuario_id');
		$loja = $this->input->post('lojas');

		// var_dump($dataIni, $dataFim, $vendedor, $tipo);die();


		$dadosView['dados']     = $this->Relatorios_model->vendedoresCelCustomizadas($dataIni, $dataFim, $vendedor, $loja);

		$dadosView['detalhes']  = $this->Relatorios_model->vendedoresCelCustoGrupoDetalhe($dataIni, $dataFim, $vendedor, $loja);

	  	// $dadosView['dados']  = $this->Relatorios_model->vendasCelularVendedor($dataIni, $dataFim, $vendedor, $tipo);

		$dadosView['emitente'] =  $this->Sistema_model->pegarEmitente();

	    $dadosView['meio']     = 'relatorios/vendedores/vendedores_padrao_celular';
		
		$html = $this->load->view('relatorios/impressao',$dadosView, true);	
				
		gerarPDF($html);

	}


	public function simulacaoCompra()
	{

		if($this->input->post() || $this->uri->segment(3) <> null){

			$dataInicio  = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('dataInicio'))));	
			$dataFim = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('dataFim'))));
			$lojas = $this->input->post('lojas');
			$grupo = $this->input->post('produto_categoria_id');


			$dadosView['dados']    = $this->Relatorios_model->simulacaoCompra_padrao($dataInicio, $dataFim, $lojas, $grupo);
			$dadosView['emitente'] = $this->Sistema_model->pegarEmitente();
			$dadosView['data']     = $this->input->post('dataInicio').' - '.$this->input->post('dataFim');
			$dadosView['meio']     = 'relatorios/simulacao/simulacaoCompra_padrao';

			$html = $this->load->view('relatorios/impressao',$dadosView, true);	
			
			gerarPDF($html);
		}else{

		$dadosView['grupo'] = $this->Relatorios_model->pegarGrupoProduto();
		$dadosView['lojas'] = $this->Relatorios_model->getAllFilias();
		$dadosView['meio']     = 'relatorios/simulacao/simulacaoCompra';
		$this->load->view('tema/layout',$dadosView);

	    }
		
	}

	public function compras()
	{

		if($this->input->post() || $this->uri->segment(3) <> null){			

			
			if($this->uri->segment(4) == 'atual'){				
				$dadosView['dados']       = $this->Relatorios_model->compraMensal($this->uri->segment(4));
				$dadosView['pagamento']   = $this->Relatorios_model->compraPagaMensal($this->uri->segment(4));
				
				$dadosView['data']    = date('m');
			}

			if($this->uri->segment(4) == 'anterior'){				
				$dadosView['dados']       = $this->Relatorios_model->compraMensal($this->uri->segment(4));
				$dadosView['pagamento']   = $this->Relatorios_model->compraPagaMensal($this->uri->segment(4));
				$dadosView['data']    = date('m', strtotime('last day of last month'));

				//var_dump($dadosView['pagamento']);die();
			}

			$dadosView['emitente'] = $this->Sistema_model->pegarEmitente();
			$dadosView['meio']     = 'relatorios/compras/compras_simples';

			$html = $this->load->view('relatorios/impressao',$dadosView, true);	
			
			gerarPDF($html);
		}else{
			$dadosView['totalMesAtual']    = $this->Relatorios_model->totalComprasMesAtual();	
			$dadosView['totalMesAnterior'] = $this->Relatorios_model->totalComprasMesAnterior();


			$dadosView['fornecedor']       = $this->Relatorios_model->pegarFornecedor();



			$dadosView['meio']     = 'relatorios/compras/compras';
			$this->load->view('tema/layout',$dadosView);
		}
	}

	public function comprasCustomizadas()
	{	
		$dataInicio  = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('dataInicio'))));	
		$dataFim = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('dataFim'))));
		$tipo = $this->input->post('tipo');
		$formato = $this->input->post('formato');

		$dadosView['infoRelatorio'] = array(
			'dataInicio' => $this->input->post('dataInicio'),
			'dataFim' => $this->input->post('dataFim'),
			'tipo' => $tipo
			 );
				
		
        $dadosView['dados']   = $this->Relatorios_model->comprasCustomizadas($dataInicio, $dataFim);

        if ($tipo == '2') {
        $dadosView['detalhe'] = $this->Relatorios_model->comprasSaidaCustomizadas($dataInicio, $dataFim);       

        }
        

		$dadosView['emitente'] =  $this->Sistema_model->pegarEmitente();

		$dadosView['meio']     = 'relatorios/compras/compras_customizadas';

		$html = $this->load->view('relatorios/impressao',$dadosView, true);	
				
		gerarPDF($html, $formato );

	}

	public function comprasPagamentoCustomizadas()
	{	
		// $dataInicio  = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('dataInicio'))));	
		// $dataFim = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('dataFim'))));		
		$formato = $this->input->post('formato');

		$fornecedor = $this->input->post('fornecedor_id');
		$tipo = 1;
		$dadosView['infoRelatorio'] = array(
			'tipo' => $tipo,
			'fornecedor' => $fornecedor
			 );
				
		
			 if ($fornecedor) {
				$dadosView['dados'] = $this->Relatorios_model->fornecedorComprasCustomizadas($fornecedor);
			} else {
				$dadosView['dados'] = $this->Relatorios_model->todosFornecedoresComprasCustomizadas();
			}
		
        // $dadosView['pagamento']   = $this->Relatorios_model->fornecedorPagamentoCustomizadas($fornecedor);

        // if ($tipo == '2') 
        // $dadosView['detalhe'] = $this->Relatorios_model->comprasSaidaCustomizadas($dataInicio, $dataFim);       

        // }
        

		$dadosView['emitente'] =  $this->Sistema_model->pegarEmitente();

		$dadosView['meio']     = 'relatorios/compras/compras_customizadas';

		$html = $this->load->view('relatorios/impressao',$dadosView, true);	

		gerarPDF($html, $formato );

	}

	public function chat()
	{

		if($this->input->post() || $this->uri->segment(3) <> null){

			if($this->uri->segment(3) == 'sim'){
				$dadosView['dados']    = $this->Relatorios_model->chat('encontrado');
			}

			if($this->uri->segment(3) == 'nao'){				
				$dadosView['dados']    = $this->Relatorios_model->chat('nao encontrado');
			}
			
			$dadosView['emitente'] = $this->Sistema_model->pegarEmitente();
			$dadosView['meio']     = 'relatorios/chat/chat_simples';

			$html = $this->load->view('relatorios/impressao',$dadosView, true);	
			
			gerarPDF($html);
		}else{
			$dadosView['totalChatSim']  = $this->Relatorios_model->chatEncontradoTotal();	
			$dadosView['totalChatNao']  = $this->Relatorios_model->chatNaoEncontradoTotal();




			$dadosView['meio']     = 'relatorios/chat/chat';
			$this->load->view('tema/layout',$dadosView);
		}
	}

	public function chatCustom()
	{	
		$dataInicio  = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('dataInicio'))));	
		$dataFim     = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('dataFim'))));
		$tipo        = $this->input->post('situacao');		
			
		
        $dadosView['dados']   = $this->Relatorios_model->chatCustom($dataInicio, $dataFim, $tipo);
        

		$dadosView['emitente'] = $this->Sistema_model->pegarEmitente();
		$dadosView['meio']     = 'relatorios/chat/chat_simples';

		$html = $this->load->view('relatorios/impressao',$dadosView, true);	
			
		gerarPDF($html);

	}


	public function financeiro()
	{

		if($this->input->post() || $this->uri->segment(3) <> null){

			if($this->uri->segment(3) == 'dia'){
				$dadosView['dados']    = $this->Relatorios_model->financeiroRapid('dia');
			}

		
			if($this->uri->segment(3) == 'mes'){				
				$dadosView['dados']    = $this->Relatorios_model->financeiroRapid('mes');
			}

			if($this->uri->segment(3) == 'mesPassado'){				
				$dadosView['dados']    = $this->Relatorios_model->financeiroRapid('mesPassado');
			}

			$formato = 'A4-L';
			$dadosView['emitente'] =  $this->Sistema_model->pegarEmitente();
			$dadosView['tipo']     =  $this->uri->segment(3);
			$dadosView['meio']     = 'relatorios/financeiro/fluxo_simples';

			$html = $this->load->view('relatorios/impressao',$dadosView, true);	
			
			gerarPDF($html, $formato);
		}else{

			$base = explode('_', BDCAMINHO);
			$dadosView['ccusto'] = $this->Relatorios_model->pegarCentroCusto();
			$dadosView['grupo']  = $this->Relatorios_model->pegarGrupoProduto();
			$dadosView['base']   = $base[2];
			$dadosView['lojas']  = $this->Relatorios_model->getAllFilias();


			$dadosView['meio']     = 'relatorios/financeiro/financeiro';
			$this->load->view('tema/layout',$dadosView);
		}
	}


	public function financeiroCustom(){

		if ($this->input->get('dataInicio')) {
			$dataInicio  = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->get('dataInicio'))));	
            $dataFim     = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->get('dataFim'))));
		}else{
			$dataInicio  = null;
            $dataFim     = null;
		}


		if ($this->input->get('pdataInicio')) {
			$pdataInicio  = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->get('pdataInicio'))));	
       		$pdataFim     = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->get('pdataFim'))));	
		}else{
			$pdataInicio  = null;
            $pdataFim     = null;
		}
      	

        	

        $categoria   = $this->input->get('categoria_fina_id');

        $cliente     = $this->input->get('cliente_id');
        $fornecedor  = $this->input->get('fornecedor_id');
        
        $tipo = $this->input->get('tipo');
        $situacao = $this->input->get('situacao');

        $formaPagamento = 'todos'; //$this->input->get('tipoPagamento');
        //$bandeira = $this->input->get('bandeira');
        $formato = 'A4-L';

        $dadosView['emitente'] =  $this->Sistema_model->pegarEmitente();

        $dadosView['lancamentos'] = $this->Relatorios_model->financeiroCustom($dataInicio,$dataFim,$pdataInicio,$pdataFim,$categoria,$cliente,$fornecedor,$formaPagamento,$tipo,$situacao);

        // var_dump($dadosView['lancamentos']);die();

        $dadosView['meio']  = 'relatorios/financeiro/fluxo_custom';

        $html = $this->load->view('relatorios/impressao',$dadosView, true);
					
		gerarPDF($html, $formato);

    }

	public function financeiroCustomizadas()
	{	
		// echo "<pre>"; var_dump($_POST); exit();
		$dataInicio  = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('dataInicio'))));	
		$dataFim = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('dataFim'))));
		$tipo = $this->input->post('tipo'); // Resulmido/Detalhado
		$formato = 'A4-L';
		$vedendor = $this->input->post('usuario_id');
		$cliente = $this->input->post('cliente_id');
		$centro = $this->input->post('categoria_fina_id');
		$ordenacao = $this->input->post('ordenacao');
		$tipoRelatorio = $this->input->post('tipoRelatorio'); // Receita/Despesa/Ambos
		
        $financeiros = $this->Relatorios_model->financeiroCustomizadas($dataInicio, $dataFim, $vedendor, $cliente, $centro, $ordenacao); 

        if ($tipo == '2') {    	
	        // foreach ($financeiros as $key) {
	        // 	$itensFinanceiros = $this->Relatorios_model->financeiroCustomizadasItens($key['dia'], $vedendor, $cliente, $centro, $tipoRelatorio);
	        // }

	        $itensFinanceiros = $this->Relatorios_model->financeiroCustomizadasItens($dataInicio, $dataFim, $vedendor, $cliente, $centro, $tipoRelatorio);

            $dadosView['dados'] = $financeiros;
	        $dadosView['itens'] = $itensFinanceiros;
	        $dadosView['meio']  = 'relatorios/financeiro/fluxo_customizado_detalhado';

         } else {

         	$dadosView['meio']  = 'relatorios/financeiro/fluxo_customizado';
         	$dadosView['dados'] = $financeiros;

        }    

		$dadosView['emitente'] =  $this->Sistema_model->pegarEmitente();
		$dadosView['tipoRelatorio'] = $tipoRelatorio;
		$html = $this->load->view('relatorios/impressao',$dadosView, true);

		// echo $html;	
				
		gerarPDF($html, $formato );

	}

	public function financeiroEmAberto()
	{
		$cliente = $this->input->post('cliente_id');
		$ano  = $this->input->post('anoRelatorio');
		$mes  = $this->input->post('mesRelatorio');
		$tipo = $this->input->post('tipoRelatorio');
		
        $financeiros = $this->Relatorios_model->financeiroEmAberto($ano, $mes, $cliente, $tipo); 

        $dadosView['dados'] = $financeiros;
        $dadosView['dataAno']  = $ano;	
		$dadosView['dataMes'] = $mes;
        $dadosView['tipo'] = 'Financeiro em Aberto';
        $formato = 'A4-P';
        $dadosView['emitente'] =  $this->Sistema_model->pegarEmitente();
        if ($tipo == '1') {
        	$dadosView['meio']  = 'relatorios/financeiro/aberto_resumido';
        } else {
        	$dadosView['meio']  = 'relatorios/financeiro/aberto_detalhado';
        }

        $html = $this->load->view('relatorios/impressao',$dadosView, true);

		gerarPDF($html, $formato );
	}
	

	public function financeiroPrevisaoFaturamento()
	{
		$data = explode('/', $this->input->post('mesInicio'));
		$ano  = $data[1];	
		$mes  = $data[0];	
	
		$dias_uteis = $this->input->post('dias_uteis'); 	
		$comissao = $this->input->post('comissao');

		
        $financeiros = $this->Relatorios_model->financeiroPrevisaoFaturamento($ano, $mes); 

        $dadosView['dados'] = $financeiros;
        $dadosView['dias_uteis'] = $dias_uteis;
        $dadosView['comissao'] = $comissao;
        $dadosView['tipo'] = 'Previsão do Faturamento';
        $formato = 'A4-P';
        $dadosView['emitente'] =  $this->Sistema_model->pegarEmitente();
        $dadosView['meio']  = 'relatorios/financeiro/fluxo_previsao_faturamento';

        $html = $this->load->view('relatorios/impressao',$dadosView, true);

		gerarPDF($html, $formato );
	}
	
	
	public function financeiroFluxoCaixa()
	{

		$ano = $this->input->post('anoRelatorio'); 	
		$periodo = $this->input->post('periodoRelatorio');
		$tipo = $this->input->post('tipoRelatorio');
		
        $receita = $this->Relatorios_model->financeiroFluxoCaixaReceita($ano, $periodo); 
        $despesa = $this->Relatorios_model->financeiroFluxoCaixaDespesa($ano, $periodo);

        if ($periodo == '2') {

	      //$ano = date('Y', strtotime('-1 years', strtotime($ano)));
	      $receitaAnterior = $this->Relatorios_model->financeiroFluxoCaixaReceitaAnterior($ano); 
	      $despesaAnterior = $this->Relatorios_model->financeiroFluxoCaixaDespesaAnterior($ano); 

          $previstoAntetior =  ($receitaAnterior[0]->receitaprevitoTotal6 - $despesaAnterior[0]->despesaprevitoTotal6) + ($receitaAnterior[0]->receitaprevitoTotal5 - $despesaAnterior[0]->despesaprevitoTotal5) + ($receitaAnterior[0]->receitaprevitoTotal4 - $despesaAnterior[0]->despesaprevitoTotal4) + ($receitaAnterior[0]->receitaprevitoTotal3 - $despesaAnterior[0]->despesaprevitoTotal3) + ($receitaAnterior[0]->receitaprevitoTotal2 - $despesaAnterior[0]->despesaprevitoTotal2) + ($receitaAnterior[0]->receitaprevitoTotal1 - $despesaAnterior[0]->despesaprevitoTotal1);

          $realizadoAntetior =  ($receitaAnterior[0]->receitarealizadoTotal6 - $despesaAnterior[0]->despesarealizadoTotal6) + ($receitaAnterior[0]->receitarealizadoTotal5 - $despesaAnterior[0]->despesarealizadoTotal5) + ($receitaAnterior[0]->receitarealizadoTotal4 - $despesaAnterior[0]->despesarealizadoTotal4) + ($receitaAnterior[0]->receitarealizadoTotal3 - $despesaAnterior[0]->despesarealizadoTotal3) + ($receitaAnterior[0]->receitarealizadoTotal2 - $despesaAnterior[0]->despesarealizadoTotal2) + ($receitaAnterior[0]->receitarealizadoTotal1 - $despesaAnterior[0]->despesarealizadoTotal1);

        $dadosView['realizadoAntetior'] = $realizadoAntetior;
        $dadosView['previstoAntetior'] = $previstoAntetior;

        }
        
        $dadosView['periodoRelatorio'] = $periodo;
        $dadosView['tipoRelatorio'] = $tipo;
        $dadosView['dadosreceita'] = $receita;
        $dadosView['dadosdespesa'] = $despesa;
      
        $dadosView['ano'] = $ano;
        $formato = 'A4-L';
        $dadosView['emitente'] =  $this->Sistema_model->pegarEmitente();
        $dadosView['meio']  = 'relatorios/financeiro/fluxo_caixa';

        $html = $this->load->view('relatorios/impressao',$dadosView, true);

		gerarPDF($html, $formato );
	}


	public function financeiroBaixaParcial()
	{

		$dataInicio  = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('dataInicio'))));	
		$dataFim  = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('dataFim'))));
		$cliente  = $this->input->post('cliente_id');
		$tipo = $this->input->post('tipo');

        $dadosView['dados'] = $this->Relatorios_model->financeiroBaixaParcial($dataInicio, $dataFim, $cliente, $tipo); 
        $formato = 'A4-L';
		$dadosView['tipo'] =  $tipo == '1' ? "Resumido - " : "Detalhado - ";
		$dadosView['date'] = $this->input->post('dataInicio').' - '.$this->input->post('dataFim');
        $dadosView['emitente'] =  $this->Sistema_model->pegarEmitente();
        $dadosView['meio']  = 'relatorios/financeiro/baixa_parcial';

        $html = $this->load->view('relatorios/impressao',$dadosView, true);

		gerarPDF($html, $formato );
	}



	public function inconsistencia()
	{

		if($this->input->post() || $this->uri->segment(3) <> null){

			if($this->uri->segment(3) == 'sim'){
				$dadosView['dados']    = $this->Relatorios_model->VendaSemFinanceiro();
			}

			if($this->uri->segment(3) == 'nao'){				
				$dadosView['dados']    = $this->Relatorios_model->chat('nao encontrado');
			}
			
			$dadosView['emitente'] = $this->Sistema_model->pegarEmitente();
			$dadosView['meio']     = 'relatorios/inconsistencia/inconsistencia_simples';

			$html = $this->load->view('relatorios/impressao',$dadosView, true);	
			
			gerarPDF($html);

		}else{

			$dadosView['totalVendaSemFinanceiro']  = $this->Relatorios_model->totalVendaSemFinanceiro();	
			$dadosView['grupo'] = $this->Relatorios_model->pegarGrupoProduto();

			$dadosView['meio']     = 'relatorios/inconsistencia/inconsistencia';
			$this->load->view('tema/layout',$dadosView);
		}
	}



	public function inconsistenciaCustom()
	{

		$dataInicio  = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('dataInicio'))));	
		$dataFim     = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('dataFim'))));

	   $cliente  = $this->input->post('cliente_id');
		$usuario = $this->input->post('usuario_id');
		
				
		$dadosView['dados']    = $this->Relatorios_model->VendaSemFinanceiroCustom($dataInicio, $dataFim, $cliente, $usuario);
			
		$dadosView['emitente'] = $this->Sistema_model->pegarEmitente();
		$dadosView['meio']     = 'relatorios/inconsistencia/inconsistencia_simples';

		$html = $this->load->view('relatorios/impressao',$dadosView, true);	
		
		gerarPDF($html);

	}


	public function inconsistenciaEVEI()
	{
		$grupo  = $this->input->post('produto_categoria_id'); 


		$dadosView['emitente'] = $this->Sistema_model->pegarEmitente();

		$dadosView['entrada'] = $this->Relatorios_model->
		$dadosView['venda']  = $this->Relatorios_model->
	//	$dadosView['estoque'] 
	//	$dadosView['imei']

		$dadosView['dados']    = $this->Relatorios_model->chat('nao encontrado');

		$dadosView['meio']     = 'relatorios/inconsistencia/inconsistencia_EVEI';

		$html = $this->load->view('relatorios/impressao',$dadosView, true);	
		
		gerarPDF($html);




	}


    public function inconsistenciaProdImei()
	{
		$grupo  = $this->input->post('produto_categoria_id'); 


		$dadosView['emitente'] = $this->Sistema_model->pegarEmitente();

	
		$dadosView['dados']    = $this->Relatorios_model->inconsistenciaProdImei($grupo);


		$dadosView['meio']     = 'relatorios/inconsistencia/inconsistencia_ProdIMEI';

		$html = $this->load->view('relatorios/impressao',$dadosView, true);	
		
		gerarPDF($html);
		
	}



	public function QRcode($value)
	{	
		QRcode::png(
			$value,
			$outfile = false,
			$level = QR_ECLEVEL_H,
			$size  = 6,
			$margin = 1
		);
	}

	public function Barcode($value)
	{
		$this->zend->load('Zend/Barcode');
		Zend_Barcode::render('code128','image', array('text' =>  $value));
	}

	public function autoCompleteClientes()
	{
		$termo = strtolower($this->input->get('term'));
        $this->Relatorios_model->autoCompleteClientes($termo);
	}

	public function autoCompleteUsuarios()
	{
        $termo = strtolower($this->input->get('term'));
        $this->Relatorios_model->autoCompleteUsuarios($termo);
	} 

	public function autoCompleteCentro()
	{
        $termo = strtolower($this->input->get('term'));
        $this->Relatorios_model->autoCompleteCentro($termo);
	} 

	public function balancoproduto($id)
	{
		$dadosView['emitente'] = $this->Sistema_model->pegarEmitente();
		$dadosView['imeisProdutosIncompativeis'] = $this->Relatorios_model->balancoProdutoImeis($id, 1, false);
		$dadosView['imeisEnviadosIncompativeis'] = $this->Relatorios_model->balancoProdutoImeis($id, 2, false);
		$dadosView['imeisProdutosCompativeis'] = $this->Relatorios_model->balancoProdutoImeis($id, 1, true);
		$dadosView['imeisEnviadosCompativeis'] = $this->Relatorios_model->balancoProdutoImeis($id, 2, true);
		$dadosView['imeisProdutos'] = $this->Relatorios_model->balancoProdutoImeis($id, 1);
		$dadosView['imeisEnviados'] = $this->Relatorios_model->balancoProdutoImeis($id, 2);
		$dadosView['numBalanco'] = $id;
		$dadosView['meio'] = 'relatorios/balancoproduto/balancoproduto';
		$html = $this->load->view('relatorios/impressao', $dadosView, true);

		gerarPDF($html);
	}

	public function fechamentoCaixa()
	{

		if($this->input->post() || $this->uri->segment(3) <> null){ 
			$campo = $this->input->post('dataInicio');
			$dataFormatada = date('Y-m-d', strtotime(str_replace('/', '-', $campo)));

			$data['dataFech'] = $campo;

			$data['dados']    = $this->Relatorios_model->fechamentoCaixa($dataFormatada);

			$data['emitente'] =  $this->Sistema_model->pegarEmitente();
			$data['meio']      = "relatorios/fechamentoCaixa/visualizarCaixa";

			$this->load->view('tema/impressaoCupom', $data);
		}else{ 
			$dadosView['meio']     = 'relatorios/fechamentoCaixa/fechamentoCaixa';
			$this->load->view('tema/layout',$dadosView);
		}

	}
}
