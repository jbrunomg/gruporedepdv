<?php 
class Controlecotacao extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('Controlecotacao_model');
	}

	public function index()
	{

		$dados['dados'] = $this->Controlecotacao_model->listarControlecotacao();
        
        foreach ($dados['dados'] as $d) {           
            if (is_null($d->categoria)) {
            $d->categoria = 'TODOS';
            }         
        }
      
		$dados['meio'] = 'controlecotacao/listar';
		
		$this->load->view('tema/layout', $dados);
	}

	public function adicionar()
	{
        // var_dump($this->input->post('cotacao_compra_fornecedor'));die();

		$this->form_validation->set_rules('cotacao_compra_categoria_id', 'Grupo de Produto', 'trim|required');

		//Adicionar a validação das datas
       
        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('erro',validation_errors());
        } else {
            $categoria  = $this->input->post('cotacao_compra_categoria_id');
            $dataInicio = $this->input->post('cotacao_compra_simulacao_data_inicio');
            $dataFim    = $this->input->post('cotacao_compra_simulacao_data_final');
            $dados = array(  
                'cotacao_compra_categoria_id'   		=> $categoria,     
                'cotacao_compra_fornecedor_id'          => implode(',',$this->input->post('cotacao_compra_fornecedor')),        
                'cotacao_compra_observacao'     		=> $this->input->post('cotacao_compra_observacao'),                             
                'cotacao_compra_usuarios_id'     		=> $this->session->userdata('usuario_id'),
                'cotacao_compra_simulacao_data_inicio'  => $dataInicio,
                'cotacao_compra_simulacao_data_final' 	=> $dataFim,
                'cotacao_compra_data'					=> date('Y-m-d'),                        
                'cotacao_compra_visivel'        		=> 1                  
            );        

            if (is_numeric($id = $this->Controlecotacao_model->adicionar($dados))) {

               $this->Controlecotacao_model->prepararItens($categoria,$id,$dataInicio,$dataFim); 
               // $this->session->set_flashdata('success','Produto iniciada com sucesso, ajuste seu valores.');

                redirect('controlecotacao/editar/'.$id);
      
            }else{
                $this->session->set_flashdata('erro','Erro ao adicionado o registro!');
            }
        }       

		$dadosView['grupo'] = $this->Controlecotacao_model->listarCategoriaProduto(); 
        $dadosView['meio'] = 'controlecotacao/adicionar';
        $this->load->view('tema/layout',$dadosView);
	}

	public function editar($id)
    {         

        $dadosView['produtos']  = $this->Controlecotacao_model->listarId($id);
        foreach ($dadosView['produtos'] as $p) {           
            if (is_null($p->categoria)) {
            $p->categoria = 'TODOS';
            }         
        } 
    
        $dadosView['meio']    	= 'controlecotacao/editar';

        $this->load->view('tema/layout',$dadosView);

    }

    public function atualizaQuantidadeProdutos($id)
    {

    	$dados = array(
    		'quantidade' 					=> $this->input->post('quantProduto'),
    		'produtos_id' 					=> $this->input->post('idProduto'),
    		'simulacao_compra_usuario_id' 	=> $this->session->userdata('usuario_id'),
    	);

    	$result = $this->Controlecotacao_model->atualizaQuantidadeProdutos($dados, $id);

    	echo json_encode($result);
    }

    public function excluir($id)
    {
        $dados  = array( 'cotacao_compra_visivel' => 0 );

        $resultado = $this->Controlecotacao_model->excluir($dados,$id);

        if($resultado){
            $this->session->set_flashdata('success','registro excluidos com sucesso!');
        }else{
            $this->session->set_flashdata('erro','Erro ao excluir o registro!');
        }

        redirect('Controlecotacao','refresh');
    }

    public function prepararControlecotacao($idControlecotacao)
    {
    	
    	$this->Controlecotacao_model->prepararControlecotacao($idControlecotacao);
    	$this->Controlecotacao_model->excluirItensControlecotacao($idControlecotacao);

    	$this->session->set_flashdata('success','registro excluidos com sucesso!');

    	redirect('Controlecotacao');

    }

    public function prepararCotacaovendedor($idControlecotacao)
    {
        $id = $this->uri->segment(3);

        $lista  = $this->Controlecotacao_model->listarFornecedorCotacao($id);        

        $fornecedor  = $this->Controlecotacao_model->listarFornecedor($lista[0]->cotacao_compra_fornecedor_id);        

        foreach ($fornecedor as $f) {              
          $resultado =  $this->Controlecotacao_model->prepararFornecedorCotacao($idControlecotacao,$f->fornecedor_id,$f->fornecedor_grupoprod_id);
        }

        if ($resultado) {

            $tabela = 'itens_cotacao_compra';
            $chave = 'cotacao_compra_id';
            $dados = array(               
                'cotacao_compra_usuario_id'      => $this->session->userdata('usuario_id')                                   
            );
            $this->Controlecotacao_model->editar($tabela, $chave, $dados, $id);   

            $tabela = 'cotacao_compra';
            $chave = 'cotacao_compra_id';
            $dados = array(               
                'cotacao_compra_situacao'      => 'Preparada'                                   
            );
            $this->Controlecotacao_model->editar($tabela, $chave, $dados, $id);    

            $this->session->set_flashdata('success','registro excluidos com sucesso!');  
        }else{
           $this->session->set_flashdata('erro','Erro ao adicionado o registro!');   
        }           

        redirect('Controlecotacao');   
    }
    

    public function visualizar($id)
    {
    
        $dadosView['produtos']   	= $this->Controlecotacao_model->listarId($id); 
        foreach ($dadosView['produtos'] as $p) {           
            if (is_null($p->categoria)) {
            $p->categoria = 'TODOS';
            }         
        }       
        $dadosView['meio']    		= 'controlecotacao/visualizar';

        $this->load->view('tema/layout',$dadosView);
    }

   public function situacaoCotacao($id)
    {
        @set_time_limit( 600 );
        ini_set('max_execution_time', 600);
        ini_set('max_input_time', 600);
        ini_set('memory_limit', '1024M');

        $dadosView['fornecedor']   = $this->Controlecotacao_model->listarParticipanteCotacao($id);
        $dadosView['produtos']     = $this->Controlecotacao_model->listarId($id); 
        foreach ($dadosView['produtos'] as $p) {           
            if (is_null($p->categoria)) {
            $p->categoria = 'TODOS';
            }         
        }   

        $dadosView['listagemCotacaoGeral'] = $this->Controlecotacao_model->listarCotacaoGeral($dadosView['fornecedor'],$id);

        $dadosView['meio']                 = 'controlecotacao/situacaocotacao';

        $this->load->view('tema/layout',$dadosView);
    }

    public function situacaoCotacaoExcel($id)
    {

        @set_time_limit( 600 );
        ini_set('max_execution_time', 600);
        ini_set('max_input_time', 600);
        ini_set('memory_limit', '1024M');

        $dadosView['fornecedor']   = $this->Controlecotacao_model->listarParticipanteCotacao($id);
        $dadosView['listagemCotacaoGeral'] = $this->Controlecotacao_model->listarCotacaoGeral($dadosView['fornecedor'],$id);
 
        ######## INICIO EXCEL ##########
        $result = array();
        foreach ($dadosView['listagemCotacaoGeral']  as $key) {
            $arr = get_object_vars($key);
            array_push($result, $arr);
        }
        
        $letrasColunas = ['A','B','C','D','E','F','G','H','I','J','L','M','N','O','P','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AL','AM','AN','AO','AP','AR','AS','AT','AU','AV','AW','AX','AY','AZ'];
        $resultLetrasColunas = array();
        for ( $i = 0; $i < count($result[0]); $i++ ){ 
             array_push($resultLetrasColunas, $letrasColunas[$i]);
        } 

        $this->load->library('PHPExcel');
        $arquivo = './assets/arquivos/planilhas/cotacao_compra.xlsx';
        $planilha = $this->phpexcel;

        $styleArray = array(
            'font' => array(
                'bold' => true,
                'color' => array('rgb' => '2F4F4F')
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            )
        );       

        //COLUNA
        $CL = -1;
        $colunas = array();
        $novoArray = reset($result);
        foreach($novoArray as $key => $value){
        $CL++;
        $colunas[] = $key;      
        $planilha->setActiveSheetIndex(0)->setCellValue($resultLetrasColunas[$CL].'1', strtoupper($key));         
        }

        //LINHA
        $contador = 1;
        foreach ($result as $l){ 
        $contador++;
            for ($i=0; $i < count($colunas) ; $i++) { 
            $arr = $l;
            $planilha->setActiveSheetIndex(0)->setCellValue($resultLetrasColunas[$i].$contador, $arr[$colunas[$i]]);
            }
        } 


        $planilha->getActiveSheet()->setTitle('Planilha 1');
        $planilha->getActiveSheet()->getStyle('A1:Q1')->applyFromArray($styleArray);
        for ($i=0; $i < count($resultLetrasColunas); $i++) { 
           $planilha->getActiveSheet()->getColumnDimension($resultLetrasColunas[$i])->setWidth(20.54);
        }
        $objgravar = PHPExcel_IOFactory::createWriter($planilha, 'Excel2007');
        $objgravar->save($arquivo);

        ######## FIM EXCEL ##########

        $dados = file_get_contents($arquivo); // Lê o conteúdo do arquivo
        $nome = 'cotacao_compra.xlsx';

        force_download($nome, $dados);


    }

    public function selecionarFornecedor(){
        
        $grupo   = $this->input->post('grupo');
        $fornecedor = $this->Controlecotacao_model->selecionarFornecedor($grupo);

        foreach ($fornecedor as $p) {
            echo "<option value='".$p->fornecedor_id."'>".$p->fornecedor_nome."</option>";
        }

    }



}