<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Simulador extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('simulador_model');
    }

    public function index()
    {
        $dataView = [];
        $dataView['meio'] = 'simulador/simular';
        $this->load->view('tema/layout', $dataView);
    }

    public function simular()
    {
        $valorProd = $this->input->post('vlprod');
        $valorEntr = $this->input->post('vlentr');

        $valorProd = $valorProd ? $valorProd : 0;
        $valorEntr = $valorEntr ? $valorEntr : 0;

        $qtdParcela = 18;
        $valorSimular = $valorProd - $valorEntr;

        $dataView['simulado'] = [
            'valorCompra' => $valorProd,
            'valorEntrada' => $valorEntr ? $valorEntr : '0.00',
            'valorSimular' => $valorSimular,
            'qtdParcela' => $qtdParcela,
        ];

        $dataView['resultado'] = $this->simulador_model->getSimular($valorSimular);
        $dataView['meio'] = 'simulador/simular';
        $this->load->view('tema/layout', $dataView);
    }


    public function configurar()
    {
        $dataView['taxas'] = $this->simulador_model->pegarTaxas();
        $dataView['meio'] = 'simulador/configuracaoCartao';

        $this->load->view('tema/layout', $dataView);
    }

    public function editarTaxasExe()
    {
        $dados_formulario = $this->input->post();

        unset($dados_formulario['ci_csrf_token']);

        $this->db->trans_begin();

        foreach ($dados_formulario as $chave => $valor) {
            $percentualParcela = floatval($valor);
            $idSimulador = $chave;

            $respostaTaxa = $this->simulador_model->pegarTaxaPorId($idSimulador, 'percentual_parcela');
            $percentualParcelaNoBanco = $respostaTaxa[0]->percentual_parcela ?? null;

            if($percentualParcelaNoBanco != $percentualParcela){
                $dados = ['percentual_parcela' => $percentualParcela];

                $resultado = $this->simulador_model->editarTaxa($dados, $idSimulador);

                if (!$resultado) {
                    $this->db->trans_rollback();
                    $this->session->set_flashdata('erro', 'Tivemos problema para alterado o registro!');
                    return redirect('simulador/configurar', 'refresh');
                }
            }
        }

        $this->db->trans_commit();
        $this->session->set_flashdata('success', 'Alteração concluída com sucesso');
        redirect('simulador/configurar', 'refresh');
    }
}

