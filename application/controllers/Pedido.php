<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pedido extends CI_Controller {

  public function __construct()
	{   
		parent::__construct();
		$this->load->model('Pedido_model');		
	}


  public function index()
	{

        $data['dados']  = $this->Pedido_model->listar();
        $data['meio']   = 'pedidos/listar';
		    $this->load->view('tema/layout',$data);

	}
  
  public function adicionar()
	{

	   $this->form_validation->set_rules('observacao', 'Observação', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
		{
        	$this->session->set_flashdata('erro',validation_errors());
        } else {

        	$data = $this->input->post('dataPedido');
        	$data = str_replace("/", "-", $data);

        	if ($this->input->post('nota') == NULL ) {
        		$nota = 0 ;
        	} else {
        		$nota = 1 ;
        	}
        	
        
        	$dados = array(  
        		    'dataPedido'          => date('Y-m-d',  strtotime($data)),             
    				'clientes_id'         => $this->input->post('cliente_id'),							    
    				'usuarios_id'         => $this->session->userdata('usuario_id'),
    				'observacao'          => $this->input->post('observacao'),  				
    				'emitirNota'          => $nota 		  
        	);

             if (is_numeric($id = $this->Pedido_model->adicionar($dados)) ) {
                $this->session->set_flashdata('success','Pedido iniciado com sucesso, adicione os produtos.');

                redirect('pedido/editar/'.$id);
      
        	}else{
        		$this->session->set_flashdata('erro','Erro ao adicionado o registro!');
        	}
        } 

		$dadosView['meio'] = 'pedidos/adicionar';
		$this->load->view('tema/layout',$dadosView);
	}

  public function editar()
	{

        $dadosView['categoria_prod'] = $this->Pedido_model->pegarCategoriaProd();
        $dadosView['dados']   = $this->Pedido_model->listarId($this->uri->segment(3));
        $dadosView['produtos'] = $this->Pedido_model->getProdutos($this->uri->segment(3));
	    $dadosView['meio']    = 'pedidos/editar';

		$this->load->view('tema/layout',$dadosView);

	}

  public function editarExe(){

    $id = $this->input->post('id');
    $this->form_validation->set_rules('observacao', 'Observação', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('erro',validation_errors());
            $this->editar($id);
        } else {

            $data = $this->input->post('dataPedido');
            $data = str_replace("/", "-", $data);

            if ($this->input->post('nota') == NULL ) {
                $nota = 0 ;
            } else {
                $nota = 1 ;
            }
            
        
            $dados = array(  
                'dataPedido'          => date('Y-m-d',  strtotime($data)),             
                'clientes_id'         => $this->input->post('cliente_id'),                              
                'usuarios_id'         => $this->session->userdata('usuario_id'),
                'observacao'          => $this->input->post('observacao'),                  
                'emitirNota'          => $nota        
            );
     
            $resultado = $this->Pedido_model->editar($dados,$this->input->post('id'));

            if($resultado){
                $this->session->set_flashdata('success','Registro editado com sucesso!');
            }else{
                $this->session->set_flashdata('erro','Erro ao editar o registro!');
            }
        }

        redirect('pedido/editar/'.$id);

   }


  public function adicionarProduto(){ 

        $id = $this->input->post('idPedidos');

        $this->form_validation->set_rules('quantidade', 'Quantidade', 'trim|required');
        // $this->form_validation->set_rules('produto', 'Produto', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
        {
            // $this->session->set_flashdata('erro',validation_errors());
            echo json_encode(array('result'=> false));

        }
        else{            


            $quantidade = $this->input->post('quantidade');
            $valor      = $this->input->post('subTotal');
            $produto    = $this->input->post('produto_id');

            $calc = floatval($valor) * floatval($quantidade);
            $subTotal = $calc;

            
            $data = array(
                'valorPago'       => $valor,
                'quantidade'      => $quantidade,
                'subTotal '       => $subTotal,
                'pedidos_id'      => $this->input->post('idPedidos'),
                'produtos_id'     => $produto
            );

            if($this->Pedido_model->add('itens_de_pedidos', $data) == true){ 

                $this->Pedido_model->addTotal($this->input->post('idPedidos'), $subTotal);                                            

                echo json_encode(array('result'=> true));
            }else{
                echo json_encode(array('result'=> false));
            }   

            
        }  

    }


 public function selecionarProduto(){
        
        $grupo   = $this->input->post('grupo');
        $produto = $this->Pedido_model->pegarProdutos($grupo);

        echo  "<option value=''>Selecionar um produto</option>";
        foreach ($produto as $p) {
            echo "<option value='".$p->produto_id."'>".$p->produto_descricao."</option>";
        }

    }


  public function selecionarProdutoID(){
        
        $produto   = $this->input->post('produto');
        $produtoId = $this->Pedido_model->pegarProdutosID($produto);
        
        foreach ($produtoId as $p) {
            echo  "<input type='text'  class='form-control' name='subTotal' id='subTotal' value='".$p->produto_preco_venda."'>
                   <p><center><font color='red'> Preço Min ".$p->produto_preco_minimo_venda."</font></center> </p>";

        }

    }

     public function VendaPedido(){ 

     	$id  = $this->input->post('idPedidos');
     	$cliente  = $this->input->post('cliente');
     	$valorTotal  =   $this->input->post('total');
     	$usuario     = $this->session->userdata('usuario_id');

	     $data = array(
	        
	        'valorTotal'     =>  $this->input->post('total'),
	        'finalizado'     =>  1,
	     );

       $validacao = $this->Pedido_model->validacao($id); 

        if ($validacao) {

                if($this->Pedido_model->Pedido($data,$id) == true){                              

                $idVenda = $this->Pedido_model->itensVenda($id, $valorTotal, $cliente, $usuario);  

                   echo json_encode(array('result'=> true, 'venda'=> $idVenda));
                }else{
                   echo json_encode(array('result'=> false));
                }   

        }else{

          echo json_encode(array('result'=> false));

        }

     }

   public function excluirProduto(){
            $idPedido = $this->uri->segment(3);
            $ID = $this->uri->segment(4);
            $produto = $this->uri->segment(5);
            $quantidade = $this->uri->segment(6);
            $subTotal = $this->uri->segment(7);
            
            if($this->Pedido_model->delete('itens_de_pedidos','idItens',$ID) == true){     

                $this->Pedido_model->retirarValor($subTotal, $idPedido);
                redirect('pedido/editar/'.$idPedido);

            }
            else{  

                $this->session->set_flashdata('erro','Erro ao editar o registro!');
                redirect('pedido/editar/'.$idPedido);
            }        

    }  

   public function visualizar()
    {
        $id = $this->uri->segment(3);
        $resultado       = $this->Pedido_model->visualizar($id);
        $emitente        = $this->Pedido_model->emitente();

        $data['pedido']     = $id;
        $data['cliente']    = $resultado[0]->cliente_nome;
        $data['email']      = $resultado[0]->cliente_email;
        $data['dataPedido'] =  date('d/m/Y',  strtotime($resultado[0]->dataPedido));
        $data['total']      = $resultado[0]->valorTotal;
        $data['dados']      = $resultado;
        $data['emitente']   = $emitente;
        $data['meio']       = "pedidos/visualizar";

    $this->load->view('tema/impressao',$data);

   }

  public function excluir()
    {
  
       $id = $this->uri->segment(3);
       $pedido_visivel = 0 ;               

       $resultado = $this->Pedido_model->excluir($pedido_visivel,$id);   

        if($resultado){
            $this->session->set_flashdata('success','Dados excluidos com sucesso!');
        }else{
            $this->session->set_flashdata('erro','Erro ao excluir os dados!');
        }

        redirect('pedido','refresh');       

    }


}