<?php
class Emissao extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Emissaonf_model');
        $this->load->model('Sistema_model');
    }

    public function index()
    {
        $dados['dados'] = $this->Emissaonf_model->listar();
        $dados['meio'] = 'emissaonf/listar';
        $this->load->view('tema/layout', $dados);
    }

    public function adicionar()
    {
        $dados['meio'] = 'emissaonf/adicionar';
        $this->load->view('tema/layout', $dados);
    }

    public function add()
    {
        // die(var_dump($this->input->post()));
        $this->form_validation->set_rules('cliente_nome', 'Cliente', 'required|trim');
        $this->form_validation->set_rules('usuario_nome', 'Vendedor', 'required|trim');
        $this->form_validation->set_rules('operacao', 'Tipo da Nota Fiscal', 'required|trim');
        $this->form_validation->set_rules('modelo', 'Modelo', 'required|trim');
        $this->form_validation->set_rules('natureza_operacao', 'Natureza Operacão', 'required|trim');
        $this->form_validation->set_rules('finalidade', 'Forma de Emissão', 'required|trim');
        $this->form_validation->set_rules('pagamento', 'Forma de pagamento', 'required|trim');
        $this->form_validation->set_rules('presenca', 'Presença do comprador', 'required|trim');
        $this->form_validation->set_rules('modalidade_frete', 'Modalidade do Frete', 'required|trim');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('erro', validation_errors());
            redirect('emissao/adicionar');
        } else {
            $dados = array(
                'fiscal_nf_data_Fiscal'                 => date('Y-m-d'),
                'fiscal_nf_operacao'                    => $this->input->post('operacao'),
                'fiscal_nf_natureza_operacao'           => $this->input->post('natureza_operacao'),
                'fiscal_nf_modelo'                      => $this->input->post('modelo'),
                'fiscal_nf_consumidor_final'            => $this->input->post('consumidor_final'),
                'fiscal_nf_finalidade'                  => $this->input->post('finalidade'),
                'fiscal_nf_ambiente'                    => 2, // 1 - Produção  // 2 - Homologação
                'fiscal_nf_nfe_referenciada'            => $this->input->post('nfe_referenciada'),
                'fiscal_nf_cefop_devolucao'             => $this->input->post('fiscal_cefop_devolucao'),
                'fiscal_nf_usuarios_id'                 => $this->input->post('usuario_id'),
                'fiscal_nf_clientes_id'                 => $this->input->post('cliente_id'),
                'fiscal_nf_volume'                      => $this->input->post('volume'),
                'fiscal_nf_especie'                     => $this->input->post('especie'),
                'fiscal_nf_peso_bruto'                  => $this->input->post('peso_bruto'),
                'fiscal_nf_peso_liquido'                => $this->input->post('peso_liquido'),
                'fiscal_nf_forma_pgto'                  => $this->input->post('pagamento'),
                'fiscal_nf_presenca'                    => $this->input->post('presenca'),
                'fiscal_nf_modalidade_frete'            => $this->input->post('modalidade_frete'),
                'fiscal_nf_valor_frete'                 => $this->input->post('frete'),
                'fiscal_nf_valor_desconto'              => $this->input->post('desconto'),
                'fiscal_nf_total'                       => $this->input->post('total'),
                'fiscal_nf_despesas_acessorias'         => $this->input->post('despesas_acessorias'),
                'fiscal_nf_informacoes_complementares'  => $this->input->post('informacoes_complementares'),
            );

            $query = $this->Emissaonf_model->add($dados);
            // die(var_dump($query));
            if ($query) {
                $this->session->set_flashdata('success', 'Nota Fiscal emitida com sucesso!');
                redirect('emissao/editar/' . $query);
            } else {
                $this->session->set_flashdata('erro', 'Erro ao emitir a Nota Fiscal!');
                redirect('emissao/adicionar');
            }
        }
    }

    public function visualizar()
    {
        $id = $this->uri->segment(3);
        $dados['dados'] = $this->Emissaonf_model->pegarNota($id);
        $dados['vendas'] = $this->Emissaonf_model->listarVendas($id);
        $dados['meio'] = 'emissaonf/visualizar';
        $this->load->view('tema/layout', $dados);
    }

    public function adicionarVenda()
    {
        $fiscal_nf_id = $this->input->post('fiscal_nf_id');
        $venda_id = $this->input->post('venda_id');

        // die(var_dump($this->input->post()));
        $this->form_validation->set_rules('venda_id', 'Número da Venda', 'required|trim');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('erro', validation_errors());
        } else {
            $venda = $this->Emissaonf_model->verificarVenda($venda_id, $fiscal_nf_id);
            if (empty($venda)) {
                $this->session->set_flashdata('erro', 'Número da venda não encontrado!');
            } else {
                $dados = array(
                    'fiscal_nf_vendas_venda_id'     => $venda_id,
                    'fiscal_nf_vendas_fiscal_nf_id' => $fiscal_nf_id
                );

                $query = $this->Emissaonf_model->addVenda($dados, $fiscal_nf_id, $venda_id);
                // die(var_dump($query));
                if ($query) {
                    $this->session->set_flashdata('success', 'Venda vinculada com sucesso!');
                } else {
                    $this->session->set_flashdata('erro', 'Venda já vinculada a esta Nota Fiscal!');
                }
            }
        }
        redirect('emissao/editar/' . $fiscal_nf_id);
    }

    public function emitirNotaFiscal()
    {
        $this->data['emitente'] = $this->Sistema_model->pegarEmitente();
        // die(var_dump($this->data['emitente']));
        $sefaz_oline    = statusSefaz($this->data['emitente']);
        $id_nota_fiscal = trim($this->input->post('nf_id'));
        $final = '';
        if ($sefaz_oline) {
            $this->data['result'] = $this->Emissaonf_model->getByIdNota($id_nota_fiscal);

            if ($this->data['result']->fiscal_nf_finalidade == 1 || $this->data['result']->fiscal_nf_finalidade == 4) {

                $this->data['vendas_ids']   = $this->Emissaonf_model->pegarIdVendas($id_nota_fiscal);
                $this->data['vendas']       = $this->Emissaonf_model->getVendasProdutos($this->data['vendas_ids']);
                $this->data['PedidoNFCe']   = $this->Emissaonf_model->getPedidoNFCe($this->data['vendas_ids']); // Salvar valor na listagem - Fiscal

                // die(var_dump($this->data['PedidoNFCe']))
                // die(var_dump($this->data));
                foreach ($this->data['vendas'] as $venda) {
                    if ($venda->produto_peso == '') {
                        // die(var_dump($venda));
                        $this->session->set_flashdata('error', 'Peso do produto não cadastrado! <br> Número da venda: ' . $venda->idVendas . ' <br> Produto: ' . $venda->produto_descricao);
                        die(redirect('emissao/editar/' . $id_nota_fiscal));
                    }
                }

                $resultado = emitirNotaFiscal($this->data); // FUNCAO DO HELPER 'FISCAL_HELPER'
                // var_dump($this->data);
                // die();
                date_default_timezone_set('America/Sao_Paulo');
                $caminho     = 'assets/notas/' . $id_nota_fiscal . '-' . date('d-m-Y H-i-s') . '.txt';

                $file = fopen($caminho, 'w+');
                fwrite($file, json_encode($resultado));
                fclose($file);

                if (isset($resultado['resposta']->status) && $resultado['resposta']->status !== 'reprovado') {
                    $dadosAtualizar = array(
                        // 'fiscal_nf_total'         => $this->input->post('totals'), // $somaValores,
                        'fiscal_nf_fiscal_danfe'  => $resultado['resposta']->danfe,
                        'fiscal_nf_fiscal_xml'    => $resultado['resposta']->xml,
                        'fiscal_nf_fiscal_chave'  => $resultado['resposta']->chave,
                        'fiscal_nf_fiscal_nfe'    => $resultado['resposta']->nfe,
                        'fiscal_nf_fiscal_serie'  => $resultado['resposta']->serie,
                        'fiscal_nf_fiscal_data_operacao' => date('Y-m-d H:i:s'),
                        'fiscal_nf_status_nota'   => 1
                    );
                    // die(var_dump($dadosAtualizar));
                    $this->Emissaonf_model->atualizarDanfeXml($id_nota_fiscal, $dadosAtualizar);
                    $this->session->set_flashdata('success', 'Nota emitida com sucesso, <a href="' . $resultado['resposta']->danfe . '" target="_Blank"><b>clique aqui</b></a> para visualizar sua DANFE!');
                    $final = 'success';
                } else {
                    if (isset($resultado['resposta'])) {
                        if (isset($resultado['resposta']->log->aProt[0]->xMotivo)) {
                            $motivo = $resultado['resposta']->log->aProt[0]->xMotivo;
                        } else {
                            $motivo = '';
                        }
                        if ($motivo == '') {
                            $motivo = $resultado['resposta']->log->xMotivo;
                        }
                        if ($resultado['resposta']->status === 'reprovado') {
                            $this->session->set_flashdata('error', $motivo);
                        } else {
                            $this->session->set_flashdata('error', $resultado['resposta']->msg);
                            log_message('debug', $resultado['resposta']->msg);
                        }
                    } else {
                        $this->session->set_flashdata('error', $resultado['msg']);
                    }
                }
            } else { // DEVOLUCAO
                $return =  devolucaoNotaFiscal($this->data);

                date_default_timezone_set('America/Sao_Paulo');
                $caminho     = 'assets/notas/' . $idNotaFiscal . '-' . date('d-m-Y H-i-s') . '.txt';
                $file = fopen($caminho, 'w+');
                fwrite($file, json_encode($return));
                fclose($file);

                if (isset($return['resposta']->status) && $return['resposta']->status !== 'reprovado') {
                    $dadosAtualizar = array(
                        'fiscal_danfe'  => $return['resposta']->danfe,
                        'fiscal_xml'    => $return['resposta']->xml,
                        'fiscal_chave'  => $return['resposta']->chave,
                        'fiscal_nfe'    => $return['resposta']->nfe,
                        'fiscal_serie'  => $return['resposta']->serie,
                        'fiscal_recibo' => $return['resposta']->recibo,
                        'fiscal_data_operacao' => date('Y-m-d H:i:s'),
                        'status_nota'   => 1
                    );

                    $this->Emissaonf_model->atualizarDanfeXml($idNotaFiscal, $dadosAtualizar);
                    $this->session->set_flashdata('success', 'Nota emitida com sucesso, <a href="' . $return['resposta']->danfe . '" target="_Blank"><b>clique aqui</b></a> para visualizar sua DANFE!');
                } else {
                    if (isset($return['resposta'])) {
                        if (isset($return['resposta']->log->aProt[0]->xMotivo)) {
                            $motivo = $return['resposta']->log->aProt[0]->xMotivo;
                        } else {
                            $motivo = '';
                        }
                        if ($motivo == '') {
                            $motivo = $return['resposta']->log->xMotivo;
                        }
                        if ($return['resposta']->status === 'reprovado') {
                            $this->session->set_flashdata('error', $motivo);
                        } else {
                            $this->session->set_flashdata('error', $return['resposta']->msg);
                            log_message('debug', $return['resposta']->msg);
                        }
                    } else {
                        $this->session->set_flashdata('error', $return['msg']);
                    }
                }
            }
        } else {
            $this->session->set_flashdata('error', 'No momento a SEFAZ esta fora do AR, favor tente em alguns instantes.<a href="https://www.nfe.fazenda.gov.br/portal/disponibilidade.aspx?versao=0.00&tipoConteudo=Skeuqr8PQBY=" target="_Blank"> Clique aqui para acompanhar</a>');
        }
        // redirect('emissao/editar/' . $id_nota_fiscal);
        redirect($final == 'success' ? 'emissao/' : 'emissao/editar/' . $id_nota_fiscal);
    }

    public function cancelar()
    {
        // var_dump($this->input->post());
        // die();
        $id_nota_fiscal         = $this->input->post('id_cancelar');
        $this->data['result'] = $this->Emissaonf_model->getByIdNota($id_nota_fiscal);
        $this->data['emitente'] = $this->Sistema_model->pegarEmitente();

        $resultado = cancelamentoNotaFiscal($this->data);

        if ($resultado['status'] == 'cancelado') {
            $this->session->set_flashdata('success', 'Nota cancelada com sucesso!');
            $dadosAtualizar = array(
                'fiscal_nf_status_nota'   => 2 //Cancelado API
            );
            $this->Emissaonf_model->atualizarDanfeXml($id_nota_fiscal, $dadosAtualizar);
        } else {
            $this->session->set_flashdata('error', $resultado['msg']);
        }

        redirect('emissao');
    }


    public function downloadXml()
    {
        $id_nota_fiscal = trim($this->uri->segment(3));
        // die(var_dump($id_nota_fiscal));
        $resultado = $this->Emissaonf_model->pegarNota($id_nota_fiscal);

        // echo "<pre>";
        // var_dump($resultado);die();

        $file_name = 'nota_' . $resultado->fiscal_nf_fiscal_nfe . '.xml';
        $mime = 'application/force-download';
        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Cache-Control: private', false);
        header('Content-Type: ' . $mime);
        header('Content-Disposition: attachment; filename="' . basename($file_name) . '"');
        header('Content-Transfer-Encoding: binary');
        header('Connection: close');

        $xml = file_get_contents($resultado->fiscal_nf_fiscal_xml);

        echo ($xml);
    }

    public function editar()
    {

        $id = $this->uri->segment(3);
        if (isset($_SESSION['EMITIR_NF_MSG'])) {
            // ROTINA DA EMISSAO DE NOTA FISCAL AO FINALIZAR VENDA. //
            $this->session->set_flashdata($_SESSION['EMITIR_NF_MSG']['TIPO'] ==  'error' ? 'error' : 'success', $_SESSION['EMITIR_NF_MSG']['MSG']);
            // /ROTINA DA EMISSAO DE NOTA FISCAL AO FINALIZAR VENDA. //
        }
        unset($_SESSION['EMITIR_NF_MSG']);

        $dados['dados']     = $this->Emissaonf_model->pegarNota($id);
        $dados['vendas']    = $this->Emissaonf_model->listarVendas($id);
        $dados['meio']      = 'emissaonf/editar';
        $this->load->view('tema/layout', $dados);
    }

    public function edt()
    {
        $id = $this->input->post('fiscal_nf_id');

        $this->form_validation->set_rules('cliente_nome', 'Cliente', 'required|trim');
        $this->form_validation->set_rules('usuario_nome', 'Vendedor', 'required|trim');
        $this->form_validation->set_rules('operacao', 'Tipo da Nota Fiscal', 'required|trim');
        $this->form_validation->set_rules('modelo', 'Modelo', 'required|trim');
        $this->form_validation->set_rules('natureza_operacao', 'Natureza Operacão', 'required|trim');
        $this->form_validation->set_rules('finalidade', 'Forma de Emissão', 'required|trim');
        $this->form_validation->set_rules('pagamento', 'Forma de pagamento', 'required|trim');
        $this->form_validation->set_rules('presenca', 'Presença do comprador', 'required|trim');
        $this->form_validation->set_rules('modalidade_frete', 'Modalidade do Frete', 'required|trim');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('erro', validation_errors());
            redirect('emissao/editar/' . $id);
        } else {
            $dados = array(
                'fiscal_nf_operacao'                    => $this->input->post('operacao'),
                'fiscal_nf_natureza_operacao'           => $this->input->post('natureza_operacao'),
                'fiscal_nf_modelo'                      => $this->input->post('modelo'),
                'fiscal_nf_consumidor_final'            => $this->input->post('consumidor_final'),
                'fiscal_nf_finalidade'                  => $this->input->post('finalidade'),
                'fiscal_nf_ambiente'                    => 2, // 1 - Produção  // 2 - Homologação
                'fiscal_nf_nfe_referenciada'            => $this->input->post('nfe_referenciada'),
                'fiscal_nf_cefop_devolucao'                => $this->input->post('fiscal_cefop_devolucao'),
                'fiscal_nf_usuarios_id'                 => $this->input->post('usuario_id'),
                'fiscal_nf_clientes_id'                 => $this->input->post('cliente_id'),
                'fiscal_nf_volume'                      => $this->input->post('volume'),
                'fiscal_nf_especie'                     => $this->input->post('especie'),
                'fiscal_nf_peso_bruto'                  => $this->input->post('peso_bruto'),
                'fiscal_nf_peso_liquido'                => $this->input->post('peso_liquido'),
                'fiscal_nf_forma_pgto'                  => $this->input->post('pagamento'),
                'fiscal_nf_presenca'                    => $this->input->post('presenca'),
                'fiscal_nf_modalidade_frete'            => $this->input->post('modalidade_frete'),
                'fiscal_nf_valor_frete'                 => $this->input->post('frete'),
                'fiscal_nf_valor_desconto'              => $this->input->post('desconto'),
                'fiscal_nf_total'                       => $this->input->post('total'),
                'fiscal_nf_despesas_acessorias'         => $this->input->post('despesas_acessorias'),
                'fiscal_nf_informacoes_complementares'  => $this->input->post('informacoes_complementares'),
            );
            // die(var_dump($dados));
            $query = $this->Emissaonf_model->edt($dados, $id);

            if ($query) {
                $this->session->set_flashdata('success', 'Nota Fiscal editada com sucesso!');
            } else {
                $this->session->set_flashdata('erro', 'Erro ao editar a Nota Fiscal!');
            }
            redirect('emissao/editar/' . $id);
        }
    }

    public function excluir()
    {
        if (verificarPermissao('dFiscal')) {
            $id     = $this->uri->segment(3);
            $query  = $this->Emissaonf_model->excluir($id);

            if (!$query) {
                $this->session->set_flashdata('erro', 'Erro ao excluir a Nota Fiscal!');
            } else {
                $this->session->set_flashdata('success', 'Nota Fiscal excluída com successo!');
            }
        } else {
            $this->session->set_flashdata('erro', 'Você não tem permissão para excluir Nota Fiscal!');
        }
        redirect('emissao');
    }

    public function excluirVenda()
    {
        $venda_id = $this->uri->segment(3);
        $fiscal_nf_id = $this->uri->segment(4);

        $query = $this->Emissaonf_model->excluirVenda($fiscal_nf_id, $venda_id);

        if (!$query) {
            $this->session->set_flashdata('erro', 'Erro ao excluir a Venda!');
        } else {
            $this->session->set_flashdata('success', 'Venda excluída com successo!');
        }
        redirect('emissao/editar/' . $fiscal_nf_id);
    }

    public function enviarEmail()
    {
        $emitente = $this->Sistema_model->pegarEmitente();

        $view   = 'De: ' . $emitente[0]->emitente_nome . '<br>';
        $view   = 'Email: ' . $emitente[0]->emitente_email . '<br><br>';
        $view   = $this->input->post('urlNota') . '<br>';

        $this->load->library('email');
        $this->email->from('emailsite@wdmtecnologia.com.br', 'WDM-Controller');
        $this->email->to($this->input->post('email'));

        $this->email->reply_to($emitente[0]->emitente_email);
        $this->email->subject('Olá, sua NF chegou!');
        $this->email->message($view);
        // $this->email->send(FALSE);
        // var_dump($this->email->print_debugger());
        // die();
        if ($this->email->send()) {
            $this->session->set_flashdata('success', 'Email enviado com sucesso.');
        } else {
            $this->session->set_flashdata('error', 'Erro ao tentar enviar o email');
        }
        redirect('emissao');
    }
}
