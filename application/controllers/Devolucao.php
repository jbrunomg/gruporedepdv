<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Devolucao extends CI_Controller {

  public function __construct()
	{   
		parent::__construct();
		$this->load->model('Devolucao_model');		
	}


  public function index()
	{

        $data['dados']  = $this->Devolucao_model->listar();
        $data['meio']   = 'devolucao/listar';
		    $this->load->view('tema/layout',$data);

	}
  
  public function adicionar()
	{

	   $this->form_validation->set_rules('observacao', 'Observação', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
		{
        	$this->session->set_flashdata('erro',validation_errors());
        } else {

        	$data = $this->input->post('dataDevolucao');
        	$data = str_replace("/", "-", $data);
        	
        
        	$dados = array(  

        		    'dataDevolucao'       => date('Y-m-d',  strtotime($data)),             
    				'fornecedor_id'       => $this->input->post('fornecedor_id'),							    
    				'usuarios_id'         => $this->session->userdata('usuario_id'),
    				'observacao'          => $this->input->post('observacao') 				
    				 		  
        	);
         

            if (is_numeric($id = $this->Devolucao_model->adicionar($dados)) ) {
                $this->session->set_flashdata('success','Devolucao iniciado com sucesso, adicione os produtos.');

                redirect('devolucao/editar/'.$id);
      
        	}else{
        		$this->session->set_flashdata('erro','Erro ao adicionado o registro!');
        	}
        } 

		$dadosView['meio'] = 'devolucao/adicionar';
		$this->load->view('tema/layout',$dadosView);
	}

    public function editar()
	{

        //  $dadosView['categoria_prod'] = $this->Devolucao_model->pegarCategoriaProd();


        $this->load->model('Fornecedores_model');


        $consultaTipo = $this->Devolucao_model->consultaTipo($this->uri->segment(3));

        if($this->session->userdata('empresa_tipo') == '1' ){    
            $dadosForn  = $this->Fornecedores_model->listarId($consultaTipo[0]->fornecedor_id);
            $dadosView['categoria_prod'] = $this->Devolucao_model->pegarCategoriaProd($dadosForn[0]->fornecedor_grupoprod_id);
        }else{
            $dadosView['categoria_prod'] = $this->Devolucao_model->pegarCategoriaProd($ids = false);
        }

        $dadosView['dados']    = $this->Devolucao_model->listarId($this->uri->segment(3));
        $dadosView['produtos'] = $this->Devolucao_model->getProdutos($this->uri->segment(3));
	    $dadosView['meio']     = 'devolucao/editar';

		$this->load->view('tema/layout',$dadosView);

	}

  public function editarExe(){

    $id = $this->input->post('id');
    $this->form_validation->set_rules('observacao', 'Observação', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('erro',validation_errors());
            $this->editar($id);
        } else {

            $data = $this->input->post('dataDevolucao');
            $data = str_replace("/", "-", $data);

            if ($this->input->post('nota') == NULL ) {
                $nota = 0 ;
            } else {
                $nota = 1 ;
            }
            
        
            $dados = array(             

                'dataDevolucao'       => date('Y-m-d',  strtotime($data)),             
                'fornecedor_id'       => $this->input->post('fornecedor_id'),                               
                'usuarios_id'         => $this->session->userdata('usuario_id'),
                'observacao'          => $this->input->post('observacao')       
            );
     
            $resultado = $this->Devolucao_model->editar($dados,$this->input->post('id'));

            if($resultado){
                $this->session->set_flashdata('success','Registro editado com sucesso!');
            }else{
                $this->session->set_flashdata('erro','Erro ao editar o registro!');
            }
        }

        redirect('devolucao/editar/'.$id);

   }


  public function adicionarProduto(){ 

        $id = $this->input->post('idDevolucao');

        $this->form_validation->set_rules('quantidade', 'Quantidade', 'trim|required');
        // $this->form_validation->set_rules('produto', 'Produto', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
        {
            // $this->session->set_flashdata('erro',validation_errors());
            echo json_encode(array('result'=> false));

        }
        else{            


            $quantidade = $this->input->post('quantidade');
            $valor      = $this->input->post('subTotal');
            $produto    = $this->input->post('produtos_id');

            $calc = floatval($valor) * floatval($quantidade);
            // $subTotal = number_format($calc,2,".",".");
            $subTotal = $calc;

            
            $data = array(        
                'subTotal '        => $subTotal,
                'quantidade'       => $quantidade,                
                'devolucao_id'     => $this->input->post('idDevolucao'),
                'produtos_id'      => $produto,
                'prod_preco_custo' => $valor
            );

            if($this->Devolucao_model->add('itens_de_devolucao', $data) == true){ 

                $this->Devolucao_model->addTotal($this->input->post('idDevolucao'), $subTotal);                                            

                echo json_encode(array('result'=> true));
            }else{
                echo json_encode(array('result'=> false));
            }   

            
        }  

    }


 public function selecionarProduto(){
        
        $grupo   = $this->input->post('grupo');
        $produto = $this->Devolucao_model->pegarProdutos($grupo);

        echo  "<option value=''>Selecionar um produto</option>";
        foreach ($produto as $p) {
            echo "<option value='".$p->produto_id."'>".$p->produto_descricao."</option>";
        }

    }


  public function selecionarProdutoID(){
        
        $produto   = $this->input->post('produto');
        $produtoId = $this->Devolucao_model->pegarProdutosID($produto);
        
        foreach ($produtoId as $p) {
            echo  "<input type='text'  class='form-control' name='subTotal' id='subTotal' value='".$p->produto_preco_custo."'>";

        }

    }

    public function finalizarDevolucao(){ 

     	$id  = $this->input->post('idDevolucao');     	
     	$valorTotal  =   $this->input->post('total');
     	$usuario     = $this->session->userdata('usuario_id');

	    $data = array(
	        
	        'valorTotal'     =>  $this->input->post('total'),
	        'finalizado'     =>  1,
	     );

        if($this->Devolucao_model->Devolucao($data,$id) == true){                               

           echo json_encode(array('result'=> true));
        }else{
           echo json_encode(array('result'=> false));
        }   


    }

   public function excluirProduto(){
            $idDevolucao = $this->uri->segment(3);
            $ID = $this->uri->segment(4);
            $produto = $this->uri->segment(5);
            $quantidade = $this->uri->segment(6);
            $subTotal = $this->uri->segment(7);
            
            if($this->Devolucao_model->delete('itens_de_devolucao','idItens',$ID) == true){     

                $this->Devolucao_model->retirarValor($subTotal, $idDevolucao);
                redirect('devolucao/editar/'.$idDevolucao);

            }
            else{  

                $this->session->set_flashdata('erro','Erro ao editar o registro!');
                redirect('devolucao/editar/'.$idDevolucao);
            }        

    }  

   public function visualizar()
    {

        $dadosView['dados']    = $this->Devolucao_model->visualizar($this->uri->segment(3));

        $dadosView['meio']     = 'devolucao/visualizar';

        $this->load->view('tema/layout',$dadosView);


   }

    public function imprimirDevolucao(){

        $this->load->model('Sistema_model');
       
        
        $dadosView['emitente'] = $this->Sistema_model->pegarEmitente();

        $dadosView['dados']    = $this->Devolucao_model->visualizar($this->uri->segment(3));
        
        $dadosView['meio']    = 'devolucao/imprimirDevolucao';

        $html = $this->load->view('relatorios/impressao',$dadosView, true); 
            
        gerarPDF($html);
       
    }

  public function excluir()
    {
  
       $id = $this->uri->segment(3);
       $pedido_visivel = 0 ;               

       $resultado = $this->Devolucao_model->excluir($pedido_visivel,$id);   

        if($resultado){
            $this->session->set_flashdata('success','Dados excluidos com sucesso!');
        }else{
            $this->session->set_flashdata('erro','Erro ao excluir os dados!');
        }

        redirect('pedido','refresh');       

    }


  public function devolucaoExcel($id)
    {

        $dadosView['dados'] = $this->Devolucao_model->visualizar($id);

        $this->load->library('PHPExcel');
        $arquivo = './assets/arquivos/planilhas/devolucao.xlsx';
        $planilha = $this->phpexcel;

        $styleArray = array(
            'font' => array(
                'bold' => true,
                'color' => array('rgb' => '2F4F4F')
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );  

        $planilha->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $planilha->getActiveSheet()->getColumnDimension('B')->setWidth(40);
        $planilha->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $planilha->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $planilha->getActiveSheet()->getColumnDimension('E')->setWidth(40);


        $planilha->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'CODIGO')
                    ->setCellValue('B1', 'PRODUTO')
                    ->setCellValue('C1', 'QUANTIDADE')
                    ->setCellValue('D1', 'CUSTO')
                    ->setCellValue('E1', 'TOTAL');

        $contador = 1;
        $total = 0;
        $qtd = 0;
        $totalDocumento = 0;
        foreach ($dadosView['dados'] as $p){ 

          $contador++;
          $total_item = number_format(($p->quantidadee * $p->prod_preco_custo), 2, '.', '');
          $qtd = $qtd + $p->quantidadee;    
          $totalDocumento = $totalDocumento + $total_item;

            $planilha->setActiveSheetIndex(0)
                ->setCellValue('A'.$contador, $p->produto_codigo)
                ->setCellValue('B'.$contador, $p->produto_descricao)
                ->setCellValue('C'.$contador, $p->quantidadee)
                ->setCellValue('D'.$contador, $p->prod_preco_custo)
                ->setCellValue('E'.$contador, 'R$ '.$total_item);
        }

        $contador = $contador + 1;
        $planilha->setActiveSheetIndex(0)
            ->setCellValue('A'.$contador, 'ITENS:'.count($dadosView['dados']))
            ->setCellValue('B'.$contador, '')
            ->setCellValue('C'.$contador, 'VOLUME:'.$qtd)
            ->setCellValue('D'.$contador, '')
            ->setCellValue('E'.$contador, 'VALOR TOTAL: R$ '.number_format($totalDocumento, 2, '.', ''));

        $planilha->getActiveSheet()->setTitle('Planilha 1');
        $planilha->getActiveSheet()->getStyle('A1:Q1')->applyFromArray($styleArray);
        $planilha->getActiveSheet()->getStyle('A'.$contador.':Q'.$contador.'')->applyFromArray($styleArray);
        $objgravar = PHPExcel_IOFactory::createWriter($planilha, 'Excel2007');
        $objgravar->save($arquivo);

        ######## FIM EXCEL ##########

        $dados = file_get_contents($arquivo); // Lê o conteúdo do arquivo
        $nome = 'develucao.xlsx';

        force_download($nome, $dados);


    }


}