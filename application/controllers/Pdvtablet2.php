<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pdvtablet2 extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('PDV_model');
  }


  public function index()
  {

    $loja = $this->input->get('loja');
    if (isset($_SESSION['EMITIR_NF_MSG'])) {
      // ROTINA DA EMISSAO DE NOTA FISCAL AO FINALIZAR VENDA. //
      $this->session->set_flashdata($_SESSION['EMITIR_NF_MSG']['TIPO'] ==  'error' ? 'error' : 'success', $_SESSION['EMITIR_NF_MSG']['MSG']);
      // /ROTINA DA EMISSAO DE NOTA FISCAL AO FINALIZAR VENDA. //
    }
    unset($_SESSION['EMITIR_NF_MSG']);
    if ($loja == NULL) {
      $loja = BDCAMINHO;
    }

    // $data['emitente']   = $this->PDV_model->emitente();
    $data['categorias'] = $this->PDV_model->getAllCategorias($loja);
    $data['filias']     = $this->PDV_model->getAllFilias();
    $data['historico']  = $this->PDV_model->getAllHistorico($this->session->userdata('usuario_id'));
    $data['vendedores'] = $this->PDV_model->getAllVendedores();

    // echo "<pre>"; var_dump($data['filias']); exit();
    $data['meio']       = 'pdvtablet2/pdv';
    $this->load->view('tema/layout', $data);
  }


  public function selecionarProduto()
  {

    $filial    = $this->input->post('filial');
    $produto = $this->PDV_model->getAllProdutos($filial);

    echo  "<option value=''>Selecionar um produto</option>";
    foreach ($produto as $p) {
      if (empty($p->imei_valor)) {
        // desabilitado
        echo "<option value='" . $p->produto_id . "'>" . $p->produto_descricao . ' | CTP: ' . $p->categoria_prod_descricao . ' | PVM: ' . $p->produto_preco_minimo_venda . ' | PV: ' . $p->produto_preco_venda . ' | EST: ' . $p->produto_estoque . "</option>";
      } else {
        echo "<option value='" . $p->produto_id . "' dataImei='" . $p->imei_valor . "'>" . $p->produto_descricao . ' | CTP: ' . $p->categoria_prod_descricao . ' | PVM: ' . $p->produto_preco_minimo_venda . ' | PV: ' . $p->produto_preco_venda . ' | IMEI: ' . $p->imei_valor . "</option>";
      }
    }
  }

  public function estoqueProdutoId()
  {

    $produto   = $this->input->post('produto');
    $filial    = $this->input->post('filial');
    $produto   = $this->PDV_model->pegarProdutosId($produto, $filial);

    echo json_encode($produto);
  }


  public function adicionarVenda()
  {

    $dados = array(
      'usuarios_id'   => $this->session->userdata('usuario_id'),
      'dataVenda'     => date('Y-m-d'),
      'horaVenda'     => date("H:i:00"),
      'clientes_id'   => 1
    );

    if (is_numeric($id = $this->PDV_model->adicionarVenda($dados))) {

      echo json_encode($id);
    } else {

      echo false;
    }
  }


  public function editarVenda()
  {
    $loja = $this->input->get('loja');
    if ($loja == NULL) {

      $loja = BDCAMINHO;
    }

    $data['emitente']   = $this->PDV_model->emitente();
    $data['categorias'] = $this->PDV_model->getAllCategorias($loja);
    $data['produtosGeral']   =  $this->PDV_model->getAllProdutos($loja);

    $id = $this->uri->segment(3);

    $data['venda'] = $this->PDV_model->getVenda($id);
    $data['produtos'] = $this->PDV_model->getVendaProdutos($id);
    $data['filias']     = $this->PDV_model->getAllFilias();

    if (count($data['venda']) == 0) {
      $this->session->set_flashdata('erro', 'Esta venda foi excluida!');
      redirect('vendas', 'refresh');
    }

    for ($i = 0; $i < count($data['produtos']); $i++) {

      for ($j = 0; $j < count($data['filias']); $j++) {

        $filias = explode("_" . GRUPOLOJA . "_", $data['filias'][$j]->schema_name);

        if ($data['produtos'][$i]['filial'] == strtoupper($filias[1])) {

          $data['produtos'][$i]['cor'] = $data['filias'][$j]->cor;
        }
      }
    }



    $data['historico']   = $this->PDV_model->getAllHistorico($this->session->userdata('usuario_id'));
    $data['vendedores']  = $this->PDV_model->getAllVendedores();
    $data['meio']        = 'pdvtablet2/editar';
    $this->load->view('tema/layout', $data);
  }


  public function additensVendas()
  {

    $dados = array(
      'quantidade'      => $this->input->post('quantidade'),
      'subTotal '       => $this->input->post('subTotal'),
      'vendas_id'       => $this->input->post('venda'),
      'produtos_id'     => $this->input->post('produto'),
      'filial'          => $this->input->post('filial'),
      'imei_valor'      => $this->input->post('imei'),
      //'produto_codigo'  => $this->input->post('codigo'),

      'prod_preco_minimo_venda'  => $this->input->post('vlpvm'),
      'prod_preco_venda'         => $this->input->post('vlpv'),
      'prod_preco_custo'         => $this->input->post('vlpc'),
      'prod_preco_cart_debito'   => $this->input->post('vlpcd'),
      'prod_preco_cart_credito'  => $this->input->post('vlpcc')
    );



    $filial = BDCAMINHO;
    $filial = explode("_", $filial);
    $filial = strtoupper($filial[2]);

    if ($filial == $this->input->post('filial')) {

      $validacao = $this->PDV_model->validacao($this->input->post('produto'), $this->input->post('quantidade'));

      if ($validacao) {

        $resultado = $this->PDV_model->additensVendas('itens_de_vendas', $dados);
        if (!$resultado) {
          http_response_code(400);
          echo json_encode(array(
            "result"  => false,
            "msg"     => "Erro ao adicionar item na venda"
          ));
          exit;
        }

        $resultado = $this->PDV_model->estoqueProduto($this->input->post('produto'), $this->input->post('quantidade'));
        if (!$resultado) {
          http_response_code(400);
          echo json_encode(array(
            "result"  => false,
            "msg"     => "Erro ao atualizar estoque do produto"
          ));
          exit;
        }

        $resultado = $this->PDV_model->imeiVenda($this->input->post('imei'), $this->input->post('venda'));
        if (!$resultado && !empty($this->input->post('imei'))) {
          http_response_code(400);
          echo json_encode(array(
            "result"  => false,
            "msg"     => "Erro ao atualizar ID da venda no IMEI do item"
          ));
          exit;
        }

        http_response_code(200);
        echo json_encode(array(
          "result"  => true,
          "msg"     => "Item adicionado"
        ));
        exit;
      } else {
        http_response_code(400);
        echo json_encode(array(
          "result"  => false,
          "msg"     => "Produto sem estoque"
        ));
        exit;
      }
    } else {
      if ($this->PDV_model->additensVendas('itens_de_vendas', $dados) == true) {

        $resultado = $this->PDV_model->imeiVendaFilial($this->input->post('imei'), $this->input->post('filial'));
        if (!$resultado) {
          http_response_code(400);
          echo json_encode(array(
            "result"  => false,
            "msg"     => "Erro ao atuailizar valor do IMEI na filial"
          ));
          exit;
        }

        http_response_code(200);
        echo json_encode(array(
          "result"  => true,
          "msg"     => "Item adicionado"
        ));
        exit;
      } else {
        http_response_code(400);
        echo json_encode(array(
          "result"  => false,
          "msg"     => "Erro ao adicionar item na venda"
        ));
        exit;
      }
    }
  }

  public function excluirItens($vendaID = NULL, $idItens = NULL, $imei = NULL)
  {
    $ajax = $this->input->is_ajax_request();
    $loja = $this->input->get('loja');

    if (empty($loja)) {
      $loja = BDCAMINHO;
    }

    $filial = BDCAMINHO;
    $filial = explode("_", $filial);
    $filial = strtoupper($filial[2]);

    $venda      = $ajax ? $this->input->post('venda')   : $vendaID;
    $itens      = $ajax ? $this->input->post('idItens') : $idItens;
    $imeiValor  = $ajax ? $this->input->post('imei')    : $imei;

    $ConsultaItens = $this->PDV_model->consultarEstoque($venda, $itens);
    if ($filial == $ConsultaItens[0]['filial']) {
      $resultado    = $this->PDV_model->estoqueAdd($ConsultaItens[0]['quantidade'], $ConsultaItens[0]['produtos_id']);
      $responseMsg  = "Erro ao atuailizar estoque do produto";
    } else {
      $resultado    = $this->PDV_model->imeiRetiradaFilial($imeiValor, $ConsultaItens[0]['filial']);
      $responseMsg  = "Erro ao realizar retirada de IMEI da filial";
    }

    if (!$resultado) {
      if ($ajax) {
        http_response_code(400);
        echo json_encode(array(
          "result"  => false,
          "msg"     => $responseMsg
        ));
        exit;
      } else {
        $this->session->set_flashdata("error", $responseMsg);
        redirect('Pdvtablet2/editarVenda/' . $vendaID, 'refresh');
      }
    }

    if ($this->PDV_model->excluirItensPDV2($venda, $itens) == true) {

      $resultado = $this->PDV_model->imeiRetirada($imeiValor);

      if (!$resultado && !empty($imeiValor)) {
        if ($ajax) {
          http_response_code(400);
          echo json_encode(array(
            "result"  => false,
            "msg"     => "Erro ao realizar retirada de IMEI da venda"
          ));
          exit;
        } else {
          $this->session->set_flashdata("error", "Erro ao realizar retirada de IMEI da venda");
          redirect('Pdvtablet2/editarVenda/' . "$vendaID", 'refresh');
        }
      }

      // $data['emitente']   = $this->PDV_model->emitente();
      // $data['categorias'] = $this->PDV_model->getAllCategorias($loja);

      // $id = $venda;

      // $data['venda']     = $this->PDV_model->getVenda($id);
      // $data['produtos']  = $this->PDV_model->getVendaProdutos($id);
      // $data['filias']    = $this->PDV_model->getAllFilias();


      // for ($i = 0; $i < count($data['produtos']); $i++) {

      //   for ($j = 0; $j < count($data['filias']); $j++) {

      //     $filias = explode("_" . GRUPOLOJA . "_", $data['filias'][$j]->SCHEMA_NAME);

      //     if ($data['produtos'][$i]['filial'] == strtoupper($filias[1])) {

      //       $data['produtos'][$i]['cor'] = $data['filias'][$j]->cor;
      //     }
      //   }
      // }

      if ($ajax) {
        http_response_code(200);
        echo json_encode(array(
          "result"  => true,
          "msg"     => "Processo realizado"
        ));
        exit;
      } else {
        $this->session->set_flashdata("success", "Processo realizado");
        redirect('Pdvtablet2/editarVenda/' . "$vendaID", 'refresh');
      }
    } else {
      if ($ajax) {
        http_response_code(400);
        echo json_encode(array(
          "result"  => false,
          "msg"     => "Ocorreu um erro inesperado. Tente novamente mais tarde ou entre em contato com o suporte."
        ));
        exit;
      } else {
        $this->session->set_flashdata("error", "Ocorreu um erro inesperado. Tente novamente mais tarde ou entre em contato com o suporte.");
        redirect('Pdvtablet2/editarVenda/' . "$vendaID", 'refresh');
      }
    }
  }

  public function cancelarVenda()
  {
    $venda = $this->input->post('venda');


    //$this->PDV_model->cancelarVendaPDV2($venda);

    $loja = $this->input->get('loja');
    if ($loja == NULL) {

      $loja = BDCAMINHO;
    }

    $data['emitente']   = $this->PDV_model->emitente();
    $data['categorias'] = $this->PDV_model->getAllCategorias($loja);

    $data['venda']     = $this->PDV_model->getVenda($venda);

    $data['produtos']  = $this->PDV_model->getVendaProdutos($venda);

    $data['filias']     = $this->PDV_model->getAllFilias();


    for ($i = 0; $i < count($data['produtos']); $i++) {

      for ($j = 0; $j < count($data['filias']); $j++) {

        $filias = explode("_" . GRUPOLOJA . "_", $data['filias'][$j]->schema_name);

        if ($data['produtos'][$i]['filial'] == strtoupper($filias[1])) {

          $data['produtos'][$i]['cor'] = $data['filias'][$j]->cor;
        }
      }
    }

    echo json_encode(array('result' => true));
    // $data['filias']    = $this->PDV_model->getAllFilias();
    // $data['meio']      = 'pdvtablet2/editar';
    // $this->load->view('tema/layout',$data);

    // }else{

    //     echo json_encode(array('result'=> false));
    // } 

  }

  public function selecionarCliente()
  {

    $cliente = $this->PDV_model->selecionarCliente();

    echo  "<option value=''>Selecionar um Cliente</option>";
    foreach ($cliente as $c) {
      echo "<option data-item='" . $c->cliente_nome . "' value='" . $c->cliente_id . "'>" . $c->cliente_nome . "</option>";
    }
  }

  public function autoCompleteClientes()
  {
    $termo = strtolower($this->input->get('term'));
    $this->PDV_model->autoCompleteClientes($termo);
  }


  public function autoCompleteUsuarios()
  {
    $termo = strtolower($this->input->get('term'));
    $this->PDV_model->autoCompleteUsuarios($termo);
  }

  public function finalizarVenda()
  {
    $this->load->model('Categoriacartao_model');
    $loja = $this->input->get('loja');
    if ($loja == NULL) {

      $loja = BDCAMINHO;
    }

    $data['emitente']   = $this->PDV_model->emitente();
    $data['categorias'] = $this->PDV_model->getAllCategorias($loja);

    $id = $this->uri->segment(3);

    $data['venda'] = $this->PDV_model->getVenda($id);
    $data['produtos'] = $this->PDV_model->getVendaProdutos($id);
    $data['filias']     = $this->PDV_model->getAllFilias();

    if (count($data['venda']) == 0) {
      $this->session->set_flashdata('erro', 'Esta venda foi excluida!');
      redirect('vendas', 'refresh');
    }


    for ($i = 0; $i < count($data['produtos']); $i++) {

      for ($j = 0; $j < count($data['filias']); $j++) {

        $filias = explode("_" . GRUPOLOJA . "_", $data['filias'][$j]->schema_name);

        if ($data['produtos'][$i]['filial'] == strtoupper($filias[1])) {

          $data['produtos'][$i]['cor'] = $data['filias'][$j]->cor;
        }
      }
    }

    $data['historico']   = $this->PDV_model->getAllHistorico($this->session->userdata('usuario_id'));
    $data['vendedores']  = $this->PDV_model->getAllVendedores();
    $data['cartoes']     = $this->Categoriacartao_model->listarDescricao();
    $data['meio']        = 'pdvtablet2/finalizar';
    $this->load->view('tema/layout', $data);
  }

  public function fecharVenda()
  {
    // echo json_encode($this->input->post());
    // die();
    //      DADOS GERAIS.   //
    $id_venda            = $this->input->post('idVenda');
    $qtd_forma_pagamento = $this->input->post('qtd_forma_pag');
    $recebido            = json_decode($this->input->post('recebimento'));
    $emitir_nf           = json_decode($this->input->post('emitir_nf'));
    //    /DADOS GERAIS.   //

    if ($qtd_forma_pagamento <= 0) {
      $json = array(
        'resultado' => false,
        'msg'       => 'Nenhuma forma de pagamento foi selecionada.'
      );
      echo json_encode($json);
      die();
    }

    $this->load->library('form_validation');

    //    VALIDAÇÕES.   //
    $this->form_validation->set_rules('data', '(Data)', 'required|trim');
    $this->form_validation->set_rules('numero_venda', '(Número Venda)', 'required|trim');
    $this->form_validation->set_rules('cliente_nome', '(Cliente)', 'required|trim');
    $this->form_validation->set_rules('usuario_nome', '(Vendedor)', 'required|trim');

    if ($recebido) {
      $this->form_validation->set_rules('data_recebimento', '(Data de recebimento)', 'required|trim');

      //      VALIDANDO FORMAS DE PAGAMENTO.       //
      for ($i = 1; $i <= $qtd_forma_pagamento; $i++) {
        $this->form_validation->set_rules('forma_pag_valor' . $i, '(Valor ' . $i . ')', 'required|numeric|trim');
        $this->form_validation->set_rules('data_vencimento' . $i, '(Data de vencimento ' . $i . ')', 'required|trim');

        if ($this->input->post('forma_pag' . $i) == 'Cartão de Crédito' || $this->input->post('forma_pag' . $i) == 'Cartão de Débito') {
          $this->form_validation->set_rules('forma_pag' . $i, '(Forma de Pagamento ' . $i . ')', 'required|trim');
          $this->form_validation->set_rules('bandeira_cartao' . $i, '(Bandeira ' . $i . ')', 'required|trim');
        }
      }
      //      /VALIDANDO FORMAS DE PAGAMENTO.      //
    }
    if ($this->form_validation->run() == FALSE) {
      $json = array(
        'resultado' => false,
        'msg'       => strip_tags(validation_errors())
      );
      echo json_encode($json);
      die();
    }
    //    /VALIDAÇÕES.   //
    $formas_de_pagamento  = array();
    $valor_pago           = 0;

    for ($i = 1; $i <= $qtd_forma_pagamento; $i++) {
      $cartao = $this->input->post('forma_pag' . $i) == 'Cartão de Crédito' || $this->input->post('forma_pag' . $i) == 'Cartão de Débito';

      $forma_de_pagamento = array(
        'valor'                 => floatval($this->input->post('forma_pag_valor' . $i)),
        'forma_pag'             => $this->input->post('forma_pag' . $i),
        'data_vencimento'       => empty($this->input->post('data_vencimento' . $i)) ? '0000-00-00' : date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('data_vencimento' . $i)))),
        //      DADOS DO CARTÃO.       //
        'id_bandeira'           => ($cartao) ? $this->input->post('id_bandeira' . $i) : null,
        'bandeira_cartao'       => ($cartao) ? $this->input->post('bandeira_cartao' . $i) : null,
        'financeiro_parcela_cart' => ($cartao) ? $this->input->post('categoria_cart_numero_parcela' . $i) : null,
        'financeiro_percentual_cart' => ($cartao) ? $this->input->post('categoria_cart_percentual_parcela' . $i) : null,
        'n_autorizacao_cartao'  => ($cartao && (!empty($this->input->post('n_autorizacao_cartao' . $i))) ? $this->input->post('n_autorizacao_cartao' . $i) : null),
        //      /DADOS DO CARTÃO.      //
      );

      $formas_de_pagamento[] = $forma_de_pagamento;
      $valor_pago += $forma_de_pagamento['valor'];
    }

    // echo json_encode($formas_de_pagamento);
    // die();

    $filial               = BDCAMINHO;
    $filial               = explode("_", $filial);
    $consultarFinanceiro  = $this->PDV_model->consultarFinanceiro($id_venda);
    $verificarFinanceiro  = $this->PDV_model->verificarFinanceiro($id_venda);

    // echo json_encode($verificarFinanceiro);
    // die();

    $dados_financeiro = array();

    if (!$verificarFinanceiro) {
      foreach ($consultarFinanceiro as $key) {

        if (strtoupper($filial[2]) == $key->filial) {
          foreach ($formas_de_pagamento as $formaPagamento) {

            $dados_financeiro = array(
              'financeiro_descricao'        => 'Fatura de Venda - #' . $id_venda . ' - ' . $key->filial,
              'financeiro_valor'            => $formaPagamento['valor'],
              'data_vencimento'             => $formaPagamento['data_vencimento'],
              'data_pagamento'              => $recebido ? date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('data_recebimento')))) : date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('data_recebimento'))) + ($this->PDV_model->getDiaCliente($this->input->post('cliente_id')) * 86400)),
              'financeiro_baixado'          => ($recebido) ? 1 : 0,
              'financeiro_forn_clie'        => $this->input->post('cliente_nome'),
              'financeiro_forn_clie_id'     => $this->input->post('cliente_id'),
              'financeiro_forma_pgto'       => $formaPagamento['forma_pag'],
              'financeiro_tipo_compra'      => $this->input->post('tipo_compra'),
              //    DADOS DO CARTÃO.       //
              'financeiro_bandeira_cart'    => $formaPagamento['id_bandeira'],
              'financeiro_autorizacao_NSU'  => $formaPagamento['n_autorizacao_cartao'],
              'financeiro_parcela_cart'     => $formaPagamento['financeiro_parcela_cart'],
              'financeiro_percentual_cart'  => $formaPagamento['financeiro_percentual_cart'],
              //    /DADOS DO CARTÃO.      //
              'financeiro_tipo'             => 'receita',
              'vendas_id'                   => $id_venda
            );

            $this->PDV_model->financeiro($dados_financeiro);
          }
        }
        // Bruno desconmentou 020620
        else {

          foreach ($formas_de_pagamento as $formaPagamento) {

            $dados_financeiro = array(
              'financeiro_descricao'        => 'Fatura de Venda - #' . $id_venda . ' - ' . $key->filial,
              'financeiro_valor'            => $formaPagamento['valor'],
              'data_vencimento'             => $formaPagamento['data_vencimento'],
              'data_pagamento'              => date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('data_recebimento')))),
              'financeiro_baixado'          => 1,
              'financeiro_forn_clie'        => $this->input->post('cliente_nome'),
              'financeiro_forn_clie_id'     => $this->input->post('cliente_id'),
              'financeiro_forma_pgto'       => $formaPagamento['forma_pag'],
              'financeiro_tipo_compra'      => $this->input->post('tipo_compra'),
              //    DADOS DO CARTÃO.       //
              'financeiro_bandeira_cart'    => $formaPagamento['id_bandeira'],
              'financeiro_autorizacao_NSU'  => $formaPagamento['n_autorizacao_cartao'],
              'financeiro_parcela_cart'     => $formaPagamento['financeiro_parcela_cart'],
              'financeiro_percentual_cart'  => $formaPagamento['financeiro_percentual_cart'],
              //    /DADOS DO CARTÃO.      //
              'financeiro_tipo'             => 'despesa',
              'vendas_id'                   => $id_venda,
              'financeiro_visivel'          => 0
            );

            $this->PDV_model->financeiro($dados_financeiro);
          }
        }
      }
    } else {
      foreach ($consultarFinanceiro as $key) {
        if (strtoupper($filial[2]) == $key->filial) {
          foreach ($formas_de_pagamento as $formaPagamento) {

            $financeiro = array(
              'data_pagamento'        => $recebido ? date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('data_recebimento')))) : date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('data_recebimento'))) + ($this->PDV_model->getDiaCliente($this->input->post('cliente_id')) * 86400)),
              'financeiro_baixado'    => ($recebido) ? 1 : 0,
              'financeiro_forma_pgto' => $formaPagamento['forma_pag'],
            );

            $this->PDV_model->financeiroUpdate($financeiro, $key->idFinanceiro);
          }
        }
        // Bruno desconmentou 020620
        else {
          foreach ($formas_de_pagamento as $formaPagamento) {
            $financeiro = array(
              'data_pagamento'        => $recebido ? date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('data_recebimento')))) : date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('data_recebimento'))) + ($this->PDV_model->getDiaCliente($this->input->post('cliente_id')) * 86400)),
              'financeiro_baixado'    => ($recebido) ? 1 : 0,
              'financeiro_forma_pgto' => $formaPagamento['forma_pag'],
            );
            $this->PDV_model->financeiroUpdate($financeiro, $key->idFinanceiro);
          }
        }
      }
    }

    $dados_venda = array(
      'dataVenda'       => date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('data')))),
      'valorTotal'      => $this->input->post('valor_total'),
      'clientes_id'     => $this->input->post('cliente_id'),
      'usuarios_id'     => $this->input->post('usuario_id'),
      'emitirNota'      => ($emitir_nf) ? 1 : 0,
      'valorRecebido'   => ($recebido) ? $valor_pago : 0,
      'observacao'      => '',
      'faturado'        => ($recebido) ? 1 : 0,
    );

    if ($this->PDV_model->finalizarVenda($dados_venda, $id_venda) == true) {

      if ($emitir_nf) {
        $this->load->model('Sistema_model');
        $this->data['emitente'] = $this->Sistema_model->pegarEmitente();
        $sefaz_online = statusSefaz($this->data['emitente']);
        // $sefaz_online = false;
        if ($sefaz_online) {
          $this->prepararNF($id_venda, $this->input->post('cliente_id'), $this->input->post('usuario_id'), $this->input->post('valor_total'), $this->input->post('modelo_nf'));
        } else {
          $_SESSION['EMITIR_NF_MSG']['TIPO']  = 'error';
          $_SESSION['EMITIR_NF_MSG']['MSG']   = 'Não foi possível gerar a nota fiscal, no momento a SEFAZ esta fora do AR, favor tente em alguns instantes.<a href="https://www.nfe.fazenda.gov.br/portal/disponibilidade.aspx?versao=0.00&tipoConteudo=Skeuqr8PQBY=" target="_Blank"> Clique aqui para acompanhar</a>';
        }
      }
      die(json_encode(array(
        'resultado' => true,
        'id_venda'  => $id_venda
      )));
    } else {
      echo json_encode(array(
        'resultado' => false
      ));
    }
  }

  public function prepararNF($id_venda, $id_cliente, $id_usuario, $valor_total, $modelo)
  {

    $dados = array(
      'fiscal_nf_data_Fiscal'       => date('Y-m-d'),
      'fiscal_nf_operacao'          => 1,
      'fiscal_nf_natureza_operacao' => 'Venda',
      'fiscal_nf_modelo'            => $modelo,
      'fiscal_nf_emissao'           => 0,
      'fiscal_nf_finalidade'        => 1, // 1 - NF-e normal  4 - Devolução/Retorno
      'fiscal_nf_ambiente'          => 2, // 1 - Produção     2 - Homologação
      'fiscal_nf_usuarios_id'       => $id_usuario,
      'fiscal_nf_clientes_id'       => $id_cliente,
      'fiscal_nf_forma_pgto'        => 0,
      'fiscal_nf_presenca'          => 1,
      'fiscal_nf_modalidade_frete'  => 9,
      'fiscal_nf_total'             => $valor_total,
    );

    $this->load->model('Emissaonf_model');
    $id_fiscal = $this->Emissaonf_model->add($dados);
    if ($id_fiscal) {
      $dados_fiscal_vendas = array(
        'fiscal_nf_vendas_venda_id'     => $id_venda,
        'fiscal_nf_vendas_fiscal_nf_id' => $id_fiscal
      );

      $this->Emissaonf_model->addVenda($dados_fiscal_vendas, $id_fiscal, $id_venda);

      $this->data['result']       = $this->Emissaonf_model->getByIdNota($id_fiscal);
      $this->data['vendas_ids']   = $this->Emissaonf_model->pegarIdVendas($id_fiscal);
      $this->data['vendas']       = $this->Emissaonf_model->getVendasProdutos($id_venda);
      $this->data['PedidoNFCe']   = $this->Emissaonf_model->getPedidoNFCe($id_venda); // Salvar valor na listagem - Fiscal

      foreach ($this->data['vendas'] as $venda) {
        if ($venda->produto_peso == '') {
          $_SESSION['EMITIR_NF_MSG']['TIPO']  = 'error';
          $_SESSION['EMITIR_NF_MSG']['MSG']   = 'Nota não emitida. <br> Peso do produto não cadastrado! <br> Número da venda: ' . $venda->idVendas . ' <br> Produto: ' . $venda->produto_descricao . ' <br> <a href="' . base_url('produto/editar/' . $venda->produto_id) . '" > Clique aqui para editar o produto.</a>';
          die(json_encode(array(
            'resultado'   => true,
            'id_venda'    => $id_venda,
            'danfe'       => false,
            'emissao_id'  => $id_fiscal
          )));
        }
      }

      $resultado    = emitirNotaFiscal($this->data); // FUNCAO DO HELPER 'FISCAL_HELPER'
      date_default_timezone_set('America/Sao_Paulo');
      $caminho      = 'assets/notas/' . $id_fiscal . '-' . date('d-m-Y H-i-s') . '.txt';

      $file = fopen($caminho, 'w+');
      fwrite($file, json_encode($resultado));
      fclose($file);

      // die(var_dump($resultado));
      if (isset($resultado['resposta']->status) && $resultado['resposta']->status !== 'reprovado') {
        $dadosAtualizar = array(
          // 'fiscal_nf_total'         => $this->input->post('totals'), // $somaValores,
          'fiscal_nf_fiscal_danfe'  => $resultado['resposta']->danfe,
          'fiscal_nf_fiscal_xml'    => $resultado['resposta']->xml,
          'fiscal_nf_fiscal_chave'  => $resultado['resposta']->chave,
          'fiscal_nf_fiscal_nfe'    => $resultado['resposta']->nfe,
          'fiscal_nf_fiscal_serie'  => $resultado['resposta']->serie,
          'fiscal_nf_fiscal_data_operacao' => date('Y-m-d H:i:s'),
          'fiscal_nf_status_nota'   => 1
        );
        // die(var_dump($dadosAtualizar));
        $this->Emissaonf_model->atualizarDanfeXml($id_fiscal, $dadosAtualizar);
        $_SESSION['EMITIR_NF_MSG']['TIPO']  = 'success';
        $_SESSION['EMITIR_NF_MSG']['MSG']   = 'Nota emitida com sucesso, <a href="' . $resultado['resposta']->danfe . '" target="_Blank"><b>clique aqui</b></a> para visualizar sua DANFE!';
        die(json_encode(array(
          'resultado' => true,
          'id_venda' => $id_venda
        )));
      } else {
        if (isset($resultado['resposta'])) {
          if (isset($resultado['resposta']->log->aProt[0]->xMotivo)) {
            $motivo = $resultado['resposta']->log->aProt[0]->xMotivo;
          } else {
            $motivo = '';
          }

          if ($motivo == '') {
            $motivo = $resultado['resposta']->log->xMotivo;
          }

          if ($resultado['resposta']->status === 'reprovado') {
            $_SESSION['EMITIR_NF_MSG']['TIPO']  = 'error';
            $_SESSION['EMITIR_NF_MSG']['MSG']   = 'Nota não emitida. <br> ' . $motivo;
          } else {
            $_SESSION['EMITIR_NF_MSG']['TIPO']  = 'error';
            $_SESSION['EMITIR_NF_MSG']['MSG']   = 'Nota não emitida. <br> ' . $resultado['resposta']->msg;
            log_message('debug', $resultado['resposta']->msg);
          }
        } else {
          $_SESSION['EMITIR_NF_MSG']['TIPO']  = 'error';
          $_SESSION['EMITIR_NF_MSG']['MSG']   = 'Nota não emitida. <br> ' . $resultado['msg'];
        }
        die(json_encode(array(
          'resultado' => true,
          'id_venda' => $id_venda,
          'danfe'       => false,
          'emissao_id'  => $id_fiscal
        )));
      }
    } else {
      $_SESSION['EMITIR_NF_MSG']['TIPO']  = 'error';
      $_SESSION['EMITIR_NF_MSG']['MSG']   = 'Houve um erro ao criar a nota fiscal.<a href="' . base_url('emissao/adicionar') . '" > Clique aqui para gerar uma nova emissão</a>';
      die(json_encode(array(
        'resultado' => true,
        'id_venda' => $id_venda
      )));
    }
  }

  public function sairVenda()
  {
    $id = $this->input->post('venda');

    if ($this->PDV_model->excluirVenda($id) == true) {
      echo json_encode(array('result' => true));
    } else {
      echo json_encode(array('result' => false));
    }
  }


  public function addCliente()
  {

    $dados = array(
      'cliente_cpf_cnpj'            => $this->input->post('cpfCnpj'),
      'cliente_tipo'                => $this->input->post('tipo'),
      'cliente_nome'                => $this->input->post('cliente_nomeModal'),
      'cliente_endereco'            => $this->input->post('cliente_endereco'),
      'cliente_celular'             => $this->input->post('cliente_numero'),
      'cliente_data_cadastro'       => date('Y-m-d'),
      'cliente_email'               => $this->input->post('cliente_email'),
    );

    $cliente = $this->PDV_model->addCliente($dados);
  }

  public function verificarCliente()
  {

    $dados = array(
      'cliente_cpf_cnpj' => $this->input->post('cpfCnpj')
    );

    if ($this->PDV_model->verificarCliente($dados)) {
      echo json_encode(true);
    } else {
      echo json_encode(false);
    }
  }

  public function validacaoGerente()
  {
    $senha = sha1(md5(strtolower($this->input->post('senha'))));

    if ($this->PDV_model->validacaoGerente($senha) == true) {

      echo json_encode(array('result' => true));
    } else {
      echo json_encode(array('result' => false));
    }
  }

  public function consultaCreditoCliente()
  {
    $cliente = $this->input->post('cliente_id');
    $credito = $this->PDV_model->getCreditoCliente($cliente);
    $consulta = $this->PDV_model->consultaCreditoCliente($cliente)[0]->valor;

    echo json_encode(
      array(
        'result' => true,
        'consulta' => $consulta,
        'credito' => $credito
      )
    );
  }
}
