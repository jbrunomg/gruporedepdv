<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Precoajuste extends CI_Controller {

        public function __construct()
    {   
        parent::__construct();
        $this->load->model('Precoajuste_model');        
    }

    public function index()
    {
        $dadosView['dados'] = $this->Precoajuste_model->listar();

        $dadosView['meio'] = 'precoajuste/listar';
        $this->load->view('tema/layout',$dadosView);    
    }

    public function adicionar()
    {
        $this->form_validation->set_rules('produto_preco_ajuste_categoria_id', 'Categoria', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('erro',validation_errors());
        } else {
            $categoria = $this->input->post('produto_preco_ajuste_categoria_id');
            $dados = array(  
                'produto_preco_ajuste_categoria_id'   => $categoria,            
                'produto_preco_ajuste_observacao'     => $this->input->post('produto_preco_ajuste_observacao'),                             
                'produto_preco_usuarios_id'           => $this->session->userdata('usuario_id'),              
                'produto_preco_ajuste_data'           => date('Y-m-d'),   
                'produto_preco_ajuste_visivel'        => 1                  
            );         

            if (is_numeric($id = $this->Precoajuste_model->adicionar($dados)) ) {
               $this->Precoajuste_model->prepararItens($categoria,$id); 
               $this->session->set_flashdata('success','Produto iniciada com sucesso, ajuste seu valores.');

                redirect('precoajuste/editar/'.$id);
      
            }else{
                $this->session->set_flashdata('erro','Erro ao adicionado o registro!');
            }
        }       

        $dadosView['grupo'] = $this->Precoajuste_model->pegarGrupoProduto(); 
        $dadosView['meio'] = 'precoajuste/adicionar';
        $this->load->view('tema/layout',$dadosView);
    }

    public function editar($id = mull)
    {
        $this->form_validation->set_rules('movimentacao_produto_documento', 'Documento', 'trim|required');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('erro',validation_errors());
        } else {

            $dados = array(              
                
                'movimentacao_produto_documento'     => $this->input->post('movimentacao_produto_documento'),             
                'movimentacao_produto_tipo'          => $this->input->post('movimentacao_produto_tipo'),                                
                'movimentacao_produto_usuario_id'      => $this->session->userdata('usuario_id'),               
                'movimentacao_produto_data_cadastro'   => date('Y-m-d')   
                                          
            );
     
            $resultado = $this->Precoajuste_model->editar($dados,$this->input->post('id'));

            if($resultado){
                $this->session->set_flashdata('success','Registro editado com sucesso!');
            }else{
                $this->session->set_flashdata('erro','Erro ao editar o registro!');
            }
        }
          
        
        $dadosView['dados']   = $this->Precoajuste_model->listarId($this->uri->segment(3));
        $dadosView['parametros'] = $this->Precoajuste_model->getAllParametros();       
        $dadosView['meio']    = 'precoajuste/editar';

        $this->load->view('tema/layout',$dadosView);

    }

    public function visualizar()
    {
        $dadosView['dados']   = $this->Precoajuste_model->visualizar($this->uri->segment(3));        

        $dadosView['meio']    = 'precoajuste/visualizar';
        $this->load->view('tema/layout',$dadosView);
    }

    public function excluir()
    {
        $id = $this->uri->segment(3);

        $dados  = array(
                        'produto_preco_ajuste_visivel' => 0                       
                      );

        $resultado = $this->Precoajuste_model->excluir($dados,$id);

        if($resultado){
            $this->session->set_flashdata('success','registro excluidos com sucesso!');
        }else{
            $this->session->set_flashdata('erro','Erro ao excluir o registro!');
        }

        redirect('precoajuste','refresh');
    }


    public function ajustarPrecoExe()
    {

        $dados = array(              
                
            'produto_preco_venda' => $this->input->post('valor'),
            'produto_preco_cart_debito' => $this->input->post('debito'),
            'produto_preco_cart_credito' => $this->input->post('credito'),             
            'idProduto'           => $this->input->post('idProduto'),                                
            'idAjuste'            => $this->input->post('idAjuste'),
            'itens_produto_preco_usuario_id'      => $this->session->userdata('usuario_id'),             
                                          
        );

        $this->Precoajuste_model->editar($dados);
        return $this->Precoajuste_model->editarAjustePreco($dados);

    }

   public function ajustarPrecoPMVExe()
    {

        $dados = array(              
                
            'produto_preco_minimo_venda' => $this->input->post('valor'),
            'idProduto'           => $this->input->post('idProduto'),                                
            'idAjuste'            => $this->input->post('idAjuste'),
            'itens_produto_preco_usuario_id'      => $this->session->userdata('usuario_id'),             
                                          
        );

        $this->Precoajuste_model->editarPMV($dados);
        return $this->Precoajuste_model->editarAjustePrecoPMV($dados);

    }

   public function ajustarPrecoCDExe()
    {

        $dados = array(              
                
            'produto_preco_cart_debito' => $this->input->post('valor'),             
            'idProduto'           => $this->input->post('idProduto'),                                
            'idAjuste'            => $this->input->post('idAjuste'),
            'itens_produto_preco_usuario_id'      => $this->session->userdata('usuario_id'),             
                                          
        );

        $this->Precoajuste_model->editarCD($dados);

        return $this->Precoajuste_model->editarAjustePrecoCD($dados);

    }

   public function ajustarPrecoCCExe()
    {

        $dados = array(              
                
            'produto_preco_cart_credito' => $this->input->post('valor'),             
            'idProduto'           => $this->input->post('idProduto'),                                
            'idAjuste'            => $this->input->post('idAjuste'),
            'itens_produto_preco_usuario_id'      => $this->session->userdata('usuario_id'),             
                                          
        );

        $this->Precoajuste_model->editarCC($dados);

        return $this->Precoajuste_model->editarAjustePrecoCC($dados);

    }



}
