<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Balancoproduto extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Balancoproduto_model');
    }

    public function index()
	{
		$dadosView['dados'] = $this->Balancoproduto_model->pegarBalancoProdutoCompleto();

		$dadosView['meio'] = 'balancoproduto/listar';
		$this->load->view('tema/layout', $dadosView);
	}

    public function adicionar()
    {
		if ($this->input->post() || $this->uri->segment(3) <> null) {
            $dadosParaInsercao = [
                'balanco_produto_categoria_produto_id' => $this->input->post('produto_categoria_id'),
                'balanco_produto_codigo_produto' => $this->input->post('codigo_produto'),
                'balanco_produto_data_cadastro' => date('Y-m-d H:i:s'),
            ];

			$balancoProdutoId = $this->Balancoproduto_model->adicionar($dadosParaInsercao);

            redirect('balancoproduto/editar/' . $balancoProdutoId);
		} else {
			$dadosView['grupo'] = $this->Balancoproduto_model->pegarGrupoProduto();

			$dadosView['meio'] = 'balancoproduto/adicionar';
			$this->load->view('tema/layout', $dadosView);
        }
    }

    public function editar()
	{
		$this->form_validation->set_rules('imeis', 'Imei(s)', 'trim|required');
		
		$imeisEnviados = preg_split("/\r\n|\n|\r/", $this->input->post('imeis'));

        if ($this->form_validation->run() == FALSE || (count($imeisEnviados) !== count(array_unique($imeisEnviados)))) {
        	$this->session->set_flashdata('erro', validation_errors() ? validation_errors() : ($this->input->post() ? 'Não podem existir imeis duplicados ao enviar o formulário.' : ''));
        } else {
            $imeisProdutos = $this->Balancoproduto_model->pegarImeisDosProdutosDeUmCodigo($this->input->post('codigo_produto'));

			foreach ($imeisProdutos as $imei) {
				$this->Balancoproduto_model->adicionarRelacaoProduto([
					'itens_de_balanco_balanco_produto_id' => $this->uri->segment(3),
					'itens_de_balanco_imei' => $imei,
					'itens_de_balanco_compativel' => in_array($imei, $imeisEnviados),
					'itens_de_balanco_tipo' => 1, // tipo 1 quer dizer que estava no banco
				]);
			}

			foreach ($imeisEnviados as $imei) {
                $this->Balancoproduto_model->adicionarRelacaoProduto([
					'itens_de_balanco_balanco_produto_id' => $this->uri->segment(3),
					'itens_de_balanco_imei' => $imei,
					'itens_de_balanco_compativel' => in_array($imei, $imeisProdutos),
					'itens_de_balanco_tipo' => 2, // tipo 2 quer dizer que foi um digitado no textarea
				]);
            }

        	$resultado = $this->Balancoproduto_model->editar(['balanco_produto_data_finalizacao' => date('Y-m-d H:i:s')], $this->uri->segment(3));

        	if ($resultado) {
        		$this->session->set_flashdata('success', 'Registro editado com sucesso!');
        	} else {
        		$this->session->set_flashdata('erro', 'Erro ao editar o registro!');
        	}
        }

        $dadosView['dados'] = $this->Balancoproduto_model->pegarPorId($this->uri->segment(3));

		if (!$dadosView['dados']) {
        	$this->session->set_flashdata('erro', 'Não foi possível encontrar o registro!');

			redirect('balancoproduto');
		}

		$dadosView['meio'] = 'balancoproduto/editar';

		$this->load->view('tema/layout', $dadosView);
	}

	public function preencherSelectCodigos()
	{
		$codigos = $this->Balancoproduto_model->preencherSelectCodigos($this->input->post('grupo'));
    	echo json_encode($codigos);
	}

	
}