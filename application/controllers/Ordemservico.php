<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ordemservico extends CI_Controller {

  public function __construct()
    {   
        parent::__construct();
        $this->load->model('Ordemservico_model');     
    }


  public function index()
    {
        $data['meio']   = 'ordemservico/listar';
        $this->load->view('tema/layout',$data);
    }
  
  public function adicionar()
    {

       $this->form_validation->set_rules('cliente_nome', 'Cliente', 'trim|required');
       $this->form_validation->set_rules('tec_nome', 'Técnico', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('erro',validation_errors());
        } else {

            $data = $this->input->post('os_dataInicial');
            $data = str_replace("/", "-", $data);   

            $datafinal = $this->input->post('os_dataFinal');
            $datafinal = str_replace("/", "-", $datafinal);  
            
        
            $dados = array(  
                    'clientes_id'       => $this->input->post('cliente_id'),  
                    'os_status'         => $this->input->post('os_status'),            
                    'os_dataInicial'    => date('Y-m-d',  strtotime($data)), 
                    'os_horaInicial'    => $this->input->post('os_horaInicial'),  
                    'os_dataFinal'      => date('Y-m-d',  strtotime($datafinal)), 
                    'os_horaFinal'      => $this->input->post('os_horaFinal'), 
                    'os_valorTotal'     => 0,                     

                    'os_equipamento'    => $this->input->post('os_equipamento'), 
                    'os_marca'          => $this->input->post('os_marca'), 
                    'os_modelo'         => $this->input->post('os_modelo'), 
                    'os_serie'          => $this->input->post('os_serie'),

                    'usuarios_id'       => $this->input->post('usuario_id'),
                    'os_data_cadastro'  => date('Y-m-d'),  
                    'tec_id'            => $this->input->post('tec_id')                  
                            
            );

            // var_dump($dados);die();

             if (is_numeric($id = $this->Ordemservico_model->adicionar($dados)) ) {
                
                $data = array(  
                    'situacao_status' => $this->input->post('os_status'),
                    'situacao_os_id'  => $id,
                    'usuarios_id'     => $this->input->post('usuario_id')                           
                    ); 
                $this->Ordemservico_model->add('os_situacao', $data, false); 

                $this->session->set_flashdata('success','Ordemservico iniciado com sucesso, adicione os serviços e produtos.');

                redirect('ordemservico/editar/'.$id);
      
            }else{
                $this->session->set_flashdata('erro','Erro ao adicionado o registro!');
            }
        } 

        $dadosView['meio'] = 'ordemservico/adicionar';
        $this->load->view('tema/layout',$dadosView);
    }

  public function editar()
    {
        
        $dadosView['dados']          = $this->Ordemservico_model->listarId($this->uri->segment(3));
        $dadosView['servicos']       = $this->Ordemservico_model->listarServicos();
        $dadosView['categoria_prod'] = $this->Ordemservico_model->pegarCategoriaProd();
        $dadosView['produtos']       = $this->Ordemservico_model->getProdutos($this->uri->segment(3));
        $dadosView['servicosItens']  = $this->Ordemservico_model->getServicos($this->uri->segment(3));
        $dadosView['anexosItens']    = $this->Ordemservico_model->getAnexos($this->uri->segment(3));
        $dadosView['meio']           = 'ordemservico/editar';

        $this->load->view('tema/layout',$dadosView);

    }

  public function editarExe()
  {

    $id = $this->input->post('id');
    $this->form_validation->set_rules('cliente_nome', 'Nome Cliente', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('erro',validation_errors());
            $this->editar($id);
        } else {

            $data = $this->input->post('os_dataInicial');
            $data = str_replace("/", "-", $data);   

            $datafinal = $this->input->post('os_dataFinal');
            $datafinal = str_replace("/", "-", $datafinal);  
            
        
            $dados = array(  
                    // ABA -  Detalhe OS
                    'clientes_id'       => $this->input->post('cliente_id'),  
                    'os_status'         => $this->input->post('os_status'),            
                    'os_dataInicial'    => date('Y-m-d',  strtotime($data)), 
                    'os_horaInicial'    => $this->input->post('os_horaInicial'),  
                    'os_dataFinal'      => date('Y-m-d',  strtotime($datafinal)), 
                    'os_horaFinal'      => $this->input->post('os_horaFinal'),                  
                    'os_equipamento'    => $this->input->post('os_equipamento'), 
                    'os_marca'          => $this->input->post('os_marca'), 
                    'os_modelo'         => $this->input->post('os_modelo'), 
                    'os_serie'          => $this->input->post('os_serie'),

                    'usuarios_id'       => $this->input->post('usuario_id'),
                    'os_data_cadastro'  => date('Y-m-d'),  
                    'tec_id'            => $this->input->post('tec_id'), 

                    // ABA - Laudo Técnico
                    'os_carcaca'         => $this->input->post('os_carcaca'),
                    'os_senhaaparelho'   => $this->input->post('os_senhaaparelho'),
                    'os_condicoes'       => $this->input->post('os_condicoes'),
                    'os_defeito'         => $this->input->post('os_defeito'),
                    'os_acessorio'       => $this->input->post('os_acessorio'),
                    'os_solucao'         => $this->input->post('os_solucao'),
                    'os_laudotecnico'    => $this->input->post('os_laudotecnico'),
                    'os_termogarantia'   => $this->input->post('os_termogarantia'),
                    'os_obsinterna'      => $this->input->post('os_obsinterna')                    
                            
            );

            // var_dump($dados);die();
     
            $resultado = $this->Ordemservico_model->editar($dados,$this->input->post('id'));

            if($resultado){
                if ($this->input->post('os_status') <> $this->input->post('os_status_anterior')) {
                    // Insert na tabela SITUACAO_OS...
                    $data = array(  
                    'situacao_status'  => $this->input->post('os_status'),
                    'situacao_os_id'  => $this->input->post('id'),
                    'usuarios_id'       => $this->input->post('usuario_id')                           
                    ); 
                    $this->Ordemservico_model->add('os_situacao', $data, false); 

                }

                $this->session->set_flashdata('success','Registro editado com sucesso!');
            }else{
                $this->session->set_flashdata('erro','Erro ao editar o registro!');
            }
        }

        redirect('ordemservico/editar/'.$id);

   }


  public function adicionarProduto()
  { 

        $id = $this->input->post('os_id');
        $this->form_validation->set_rules('quantidade', 'Quantidade', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
        {
            echo json_encode(array('result'=> false));

        }
        else{            

            $quantidade = $this->input->post('quantidade');
            $valor      = $this->input->post('subTotal');
            $produto    = $this->input->post('produto_id');
            $dateMV     = $this->input->post('dateMV');
            $datePC     = $this->input->post('datePC');
            $datePV     = $this->input->post('datePV');
            $datePCD    = $this->input->post('datePCD');
            $datePCC    = $this->input->post('datePCC');
            $calc = floatval($valor) * floatval($quantidade);
            $subTotal = $calc;

            $data = array(
                'produtos_os_quantidade'  => $quantidade,
                'produtos_os_subTotal '   => $subTotal,
                'os_id'           => $this->input->post('os_id'),
                'produtos_id'     => $produto,
                'prod_preco_custo'  => $datePC,
                'prod_preco_minimo_venda'  => $dateMV,
                'prod_preco_venda' => $datePV,
                'prod_preco_cart_debito'  => $datePCD,
                'prod_preco_cart_credito' => $datePCC,
                'filial'          => SUBDOMINIO
            );

            $idItens = $this->Ordemservico_model->add('itens_de_os_produto', $data, true);
            $dadosidItens = $this->Ordemservico_model->getProdutosUltimo($id, $idItens);

            if($idItens){ 
                $sql = "UPDATE produto set produto_estoque = produto_estoque - ? WHERE produto_id = ?";          
                $this->db->query($sql, array($quantidade, $produto));  
                $this->Ordemservico_model->addTotal($this->input->post('os_id'), $subTotal);                                            
                echo json_encode(array('result'=> true, 'dados' => $dadosidItens));
            }else{
                echo json_encode(array('result'=> false));
            }   
            
        }  

    }


    public function adicionarServico()
    { 
  
          $id = $this->input->post('os_id');
          $this->form_validation->set_rules('quantidade', 'Quantidade', 'trim|required');
         
          if($this->form_validation->run() == FALSE)
          {
              echo json_encode(array('result'=> false));
  
          }
          else{            
  
              $quantidade = $this->input->post('quantidade');
              $valor      = $this->input->post('subTotal');
              $servico    = $this->input->post('servico_id');
              $calc = floatval($valor) * floatval($quantidade);
              $subTotal = $calc;
  
              $data = array(
                  'servico_os_quantidade'  => $quantidade,
                  'servico_os_subTotal '   => $subTotal,
                  'os_id'           => $this->input->post('os_id'),
                  'servicos_id'     => $servico
              );
  
              $idItens = $this->Ordemservico_model->add('itens_de_os_servicos', $data, true);
              $dadosidItens = $this->Ordemservico_model->getServicosUltimo($id, $idItens);
  
              if($idItens){ 
                  $this->Ordemservico_model->addTotal($this->input->post('os_id'), $subTotal);                                            
                  echo json_encode(array('result'=> true, 'dados' => $dadosidItens));
              }else{
                  echo json_encode(array('result'=> false));
              }   
              
          }  
  
      }


      public function adicionarAnexo()
      { 
    
            $id = $this->input->post('os_id');
            $this->form_validation->set_rules('os_id', 'os_id', 'trim|required');
           
            if($this->form_validation->run() == FALSE)
            {
                echo json_encode(array('result'=> false)); die('d');
    
            } else{        
 
                if($_FILES['arquivo']['name'] != ''){
                    $arquivo = $this->do_upload(); 
                    $file = $arquivo['file_name'];

                    if ($arquivo == false) { 
                        echo json_encode(array('result'=> false));
                    }else{

                        $data = array(
                            'anexo_thumb'  => $file,
                            'anexo_url '   => '/assets/servicos/',
                            'os_id'           => $this->input->post('os_id')
                        );
    
                        $idItens = $this->Ordemservico_model->add('anexos', $data, true);
                        $dadosidItens = $this->Ordemservico_model->getAnexoUltimo($id, $idItens);
                        if($idItens){                                            
                            echo json_encode(array('result'=> true, 'dados' => $dadosidItens));
                        }else{
                            echo json_encode(array('result'=> false));
                        }  
                        
                    }

                }else{
                    echo json_encode(array('result'=> false));
                }
                
            }  
    
        }


 public function selecionarProduto()
    {
        
        $grupo   = $this->input->post('grupo');
        $produto = $this->Ordemservico_model->pegarProdutos($grupo);

        echo  "<option value=''>Selecionar um produto</option>";
        foreach ($produto as $p) {
            echo "<option value='".$p->produto_id."'>".$p->produto_descricao."  - ".$p->produto_estoque."</option>";
        }

    }


  public function selecionarProdutoID(){
        
        $produto   = $this->input->post('produto');
        $produtoId = $this->Ordemservico_model->pegarProdutosID($produto);
        
        foreach ($produtoId as $p) {
            echo  "<input type='text'  class='form-control' 
                    dateEstoque='".$p->produto_estoque."' 
                    dateMV='".$p->produto_preco_minimo_venda."'
                    datePC='".$p->produto_preco_custo."'
                    datePV='".$p->produto_preco_venda."'
                    datePCD='".$p->produto_preco_cart_debito."'
                    datePCC='".$p->produto_preco_cart_credito."'
                    name='subTotal' id='subTotal' value='".$p->produto_preco_venda."'>
                   <p><center><font color='red'> Preço Min ".$p->produto_preco_minimo_venda."</font></center> </p>";

        }

    }

    public function selecionarServicoID(){
        
        $servico   = $this->input->post('servico');
        $servicoId = $this->Ordemservico_model->pegarServicosID($servico);
        
        foreach ($servicoId as $p) {
            echo  "<input type='text'  class='form-control' name='subTotalServico' id='subTotalServico' value='".$p->servico_valor."'>";

        }

    }

     public function VendaOrdemservico(){ 

        $id  = $this->input->post('idOrdemservicos');
        $cliente  = $this->input->post('cliente');
        $valorTotal  =   $this->input->post('total');
        $usuario     = $this->session->userdata('usuario_id');

         $data = array(
            
            'valorTotal'     =>  $this->input->post('total'),
            'finalizado'     =>  1,
         );

       $validacao = $this->Ordemservico_model->validacao($id); 

        if ($validacao) {

                if($this->Ordemservico_model->Ordemservico($data,$id) == true){                              

                  $this->Ordemservico_model->itensVenda($id, $valorTotal, $cliente, $usuario);  

                   echo json_encode(array('result'=> true));
                }else{
                   echo json_encode(array('result'=> false));
                }   

        }else{

          echo json_encode(array('result'=> false));

        }

     }

   public function excluirProduto(){
            $os_id = $this->uri->segment(3);
            $ID = $this->uri->segment(4);
            $produto = $this->uri->segment(5);
            $quantidade = $this->uri->segment(6);
            $subTotal = $this->uri->segment(7);
            
            if($this->Ordemservico_model->delete('itens_de_os_produto','idItens',$ID) == true){  
                
                $sql = "UPDATE produto set produto_estoque = produto_estoque + ? WHERE produto_id = ?";          
                $this->db->query($sql, array($quantidade, $produto));  

                $this->Ordemservico_model->retirarValor($subTotal, $os_id);
                redirect('ordemservico/editar/'.$os_id);

            }
            else{  

                $this->session->set_flashdata('erro','Erro ao editar o registro!');
                redirect('ordemservico/editar/'.$os_id);
            }        

    }  

    public function excluirServico(){
        $os_id = $this->uri->segment(3);
        $ID = $this->uri->segment(4);
        $produto = $this->uri->segment(5);
        $quantidade = $this->uri->segment(6);
        $subTotal = $this->uri->segment(7);
        
        if($this->Ordemservico_model->delete('itens_de_os_servicos','idItens',$ID) == true){     

            $this->Ordemservico_model->retirarValor($subTotal, $os_id);
            redirect('ordemservico/editar/'.$os_id);

        }
        else{  

            $this->session->set_flashdata('erro','Erro ao editar o registro!');
            redirect('ordemservico/editar/'.$os_id);
        }        

    } 

   public function visualizar()
    {

        $dadosView['dados']          = $this->Ordemservico_model->listarId($this->uri->segment(3));

        // var_dump( $dadosView['dados'] );die();

        $dadosView['hist_status']    = $this->Ordemservico_model->listarSituacaoOsId($this->uri->segment(3));
        $dadosView['categoria_prod'] = $this->Ordemservico_model->pegarCategoriaProd();
        $dadosView['produtos']       = $this->Ordemservico_model->getProdutos($this->uri->segment(3));
        $dadosView['servicos']       = $this->Ordemservico_model->getServicos($this->uri->segment(3));
        
        $dadosView['meio']           = 'ordemservico/visualizar';

        $this->load->view('tema/layout',$dadosView);


   }


    public function visualizarNota($id = null)
    {
        
        $id = $this->uri->segment(3);
        $tipo = $this->uri->segment(4);

        $data['resultado']   = $this->Ordemservico_model->listarId($id);
        $data['emitente']    = $this->Ordemservico_model->emitente();

        $data['produtos']       = $this->Ordemservico_model->getProdutos($this->uri->segment(3));
        $data['servicos']       = $this->Ordemservico_model->getServicos($this->uri->segment(3));

        //var_dump($data['resultado']);die();


        if ($tipo == 'c') {
            $data['meio']    = "ordemservico/visualizarCupom";            

        }else{

            $data['meio']    = "ordemservico/visualizarNota";           
         
        }  

        $this->load->view('tema/impressaoOs',$data);        


    }



  public function excluir()
    {
  
       $id = $this->uri->segment(3);
       $os_visivel = 0 ;               

       $resultado = $this->Ordemservico_model->excluir($os_visivel,$id);   

        if($resultado){
            $this->db->set('financeiro_visivel', 0);
            $this->db->where('os_id',$id);
            $this->db->update('financeiro'); 

            $this->session->set_flashdata('success','Dados excluidos com sucesso!');
        }else{
            $this->session->set_flashdata('erro','Erro ao excluir os dados!');
        }

        redirect('ordemservico','refresh');       

    }


   public function faturar()
    {

        if ($this->input->post('formaPgto') == 'Sem pagamento') {
            $faturado = '0';
            $baixado = '0';
            $dataPagamento = empty($this->input->post('dataPagamento')) ? date('Y-m-d')  : date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('dataPagamento'))));
        } else {
            $faturado = '1';
            $baixado = '1';
            $dataPagamento = date('Y-m-d');
        }

        $financeiro = array(
            'financeiro_descricao' => 'Fatura de Ondem Serviço - #'.$this->input->post('osID').' - '.SUBDOMINIO,
            'financeiro_valor' =>  $this->input->post('valor'),
            'data_vencimento' => date('Y-m-d'),
            'data_pagamento' => $dataPagamento,
            'financeiro_baixado' => $baixado,
            'financeiro_forn_clie' => $this->input->post('clie_receita'),
            'financeiro_forn_clie_id' => $this->input->post('clie_receita_id'),
            'financeiro_forma_pgto' => $this->input->post('formaPgto'),
            'financeiro_tipo_compra' => 'varejo',
            'financeiro_tipo' => 'receita',
            'os_id' => $this->input->post('osID')
        );

        $os = array('os_faturado' => '1', 'os_status' => 'FATURADO', 'os_datafaturado' => date('Y-m-d'));

        $resultado = $this->Ordemservico_model->financeiro($financeiro);
        if($resultado){
            $this->Ordemservico_model->editar($os,$this->input->post('osID'));
            $this->session->set_flashdata('success','Dados faturado com sucesso!');
        }else{
            $this->session->set_flashdata('erro','Erro ao faturar os dados!');
        }

        redirect('ordemservico','refresh');   

    }

    public function do_upload(){
        
        $config['upload_path']   = realpath('./assets/servicos/');
        $config['allowed_types'] = 'txt|jpg|jpeg|gif|png|pdf|PDF|JPG|JPEG|GIF|PNG';
        $config['max_size']      = 0;
        // $config['max_width']  = '3000';
        // $config['max_height'] = '2000';
        $config['encrypt_name']  = true;


        if (!is_dir('./assets/servicos/')) {
            mkdir('./assets/servicos/', 0777, TRUE);
        }
        
        $this->load->library('upload', $config);
        $this->upload->initialize($config);       

        if (!$this->upload->do_upload('arquivo'))
        {
            $error = array('error' => $this->upload->display_errors());
            v($error);
            return false;

        }
        else
        {
            return $this->upload->data();

        }

    }



    public function sendmail()
    {    
        $emitente  = $this->Ordemservico_model->emitente();


        $view  = 'De: '.$emitente[0]->emitente_nome.'<br>';
        $view .= 'Email: '.$emitente[0]->emitente_email.'<br><br>';
        $view .= $this->input->post('mensagem');

   
        

        $this->load->library('email');
        $this->email->from('emailsite@wdmtecnologia.com.br', 'WDM-Controller');
        $this->email->to($this->input->post('email'));
      
        $this->email->reply_to($emitente[0]->emitente_email);

        $this->email->subject('Olá, sua O.S chegou!');
       
        $this->email->message($view);
    
        if ($this->email->send()){
            $this->session->set_flashdata('success','Email enviado com sucesso.');
            //return true;
            echo json_encode(array('status' => true, 'msg' => 'E-mail enviado com sucesso!'));
        }
        else{
            $this->session->set_flashdata('error','Erro ao tentar enviar o email');
            //return false;
            echo json_encode(array('status' => false, 'msg' => 'Falha ao enviar o email, favor tente mais tarde!'));
        }
    }



    public function situacaoOs()
    {
        
        $dadosView['dados']          = $this->Ordemservico_model->listarSituacaoOsId($this->uri->segment(3));

        // var_dump($dadosView['dados']);die();
    
        $dadosView['meio']           = 'ordemservico/editarSituacao';

        // $this->load->view('tema/layout',$dadosView);
        $this->load->view('tema/impressaoOs',$dadosView);

    }

    public function editarSituacao()
    {
        $this->form_validation->set_rules('situacao_status', 'Situação', 'trim|required');

        $os_id = $this->input->post('id');
       
        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('erro',validation_errors());
        } else {


            $dados = array(              
                'situacao_status'     => $this->input->post('situacao_status'),                 
                'situacao_obs'        => $this->input->post('situacao_obs'),
                'situacao_os_id'      => $os_id,
                'usuarios_id'         => $this->input->post('usuario_id')                  
            );

            $resultado = $this->Ordemservico_model->add('os_situacao',$dados);

            $sql = "UPDATE os set os_status = ? WHERE os_id = ?";
            $this->db->query($sql, array($this->input->post('situacao_status'), $os_id));


            if($resultado){
                $this->session->set_flashdata('success','Registro adicionado com sucesso!');
            }else{
                $this->session->set_flashdata('erro','Erro ao adicionado o registro!');
            }
        }

        redirect('ordemservico/situacaoOs/'.$os_id);
    }


    public function editarAssinatura()
    {
        $this->form_validation->set_rules('assinatura', 'A Assinatura e obrigada', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $resposta = $this->form_validation->error_array();

            //outputJSON(['message'=> $resposta['assinatura']], 400);
            echo json_encode(['message'=> $resposta['assinatura']], 400);
        } else {
            $id = $this->uri->segment(3);

            $assinatura = $this->input->post('assinatura');

            $this->db->trans_start();

            $resposta = $this->Ordemservico_model->updateAssinatura($id, $assinatura);

            if ($resposta) {
                $this->db->trans_commit();
               // outputJSON(['message'=> 'adição da assinatura com sucesso'], 200);
                echo json_encode(['message'=> 'adição da assinatura com sucesso'], 200);
            } else {
                $this->db->trans_rollback();
                //outputJSON(['message'=> 'Não foi possivel adicionado a acinatura no sistema'], 400);
                echo json_encode(['message'=> 'Não foi possivel adicionado a acinatura no sistema'], 400);
            }
        }
    }

	public function listarDados(){

        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $search = $this->input->post('search') ?? ''; // Verifica se 'value' existe

        // Parâmetros de ordenação
        $order_column_index = $this->input->post('order')[0]['column'] ?? 0; // Índice da coluna a ser ordenada (padrão: 0)
        $order_column_name = $this->input->post('columns')[$order_column_index]['data'] ?? ''; // Nome da coluna a ser ordenada
        $order_dir = $this->input->post('order')[0]['dir'] ?? 'asc'; // Direção da ordenação (padrão: asc)

		$dados = $this->Ordemservico_model->listar($limit, $start, $search, $order_column_name, $order_dir);

        $formatted_data = [];
		$icon = '';

        foreach ($dados['dados'] as $d) { 

			if (verificarPermissao('vOrdemServico')) { 
				$icon .= '<a href="'.base_url().$this->uri->segment(1).'/visualizar/'.$d->os_id.'" data-popup="tooltip" title="Visualizar"><i class="fa fa-search text-success"></i></a>';
			} 
			if (verificarPermissao('eOrdemServico')) { 
                if($d->os_faturado == '0'){ 
                    $icon .= '<a href="'.base_url().$this->uri->segment(1).'/editar/'.$d->os_id.'" data-toggle="tooltip" title="Editar"><i class="fa fa-edit"></i></a>';
                }else{ 
                    $icon .= '<a style="cursor:pointer;" data-toggle="modal" data-target="#modalSenha" onclick="idOsModal('.$d->os_id.');" data-toggle="tooltip" title="Editar Gerente"><i class="fa fa-repeat"></i></a>';
                } 
			} 
			if (verificarPermissao('dOrdemServico')) { 
				$icon .= '<a style="cursor:pointer;" data-toggle="modal" data-target="#excluir" onclick="excluir('.base_url().$this->uri->segment(1).'/excluir/'.$d->os_id.');"><i class="fa fa-trash text-danger"></i></a>';
			}

                $icon .= '<div class="btn-group">
                                <a href="#" style="cursor:pointer;"  data-toggle="dropdown" title="Mais Opções"><i class="fa fa-navicon"></i> </a>                        
                                <ul class="dropdown-menu dropdown-menu-right">              
                                    <li><a data-toggle="modal" data-target="#modalsituacao" href="#" onclick="idSituacaoOsModal('.$d->os_id.')" datasituacao="'.$d->os_id.'" ><i class="text-danger fa fa-line-chart margin-right-5px"></i>Alterar situação</a></li>
                                    <li><a href="'.base_url().$this->uri->segment(1).'/visualizarNota/'.$d->os_id.'/n" target="_blank"><i class="text-danger fa fa-file-pdf-o margin-right-5px"></i>Imprimir A4</a></li>
                                    <li><a href="'.base_url().$this->uri->segment(1).'/visualizarNota/'.$d->os_id.'/c" target="_blank"><i class="text-danger fa fa-file-pdf-o margin-right-5px"></i>Imprimir cupom</a></li>
                                    <li><a data-toggle="modal" data-target="#modalwhatsapp"  celular="'.$d->cliente_celular.'" os_id="'.$d->os_id.'" href="'.base_url().$this->uri->segment(1).'/enviarwhatsapp/'.$d->os_id.'/c" target="_blank"><i class="text-danger fa fa-whatsapp margin-right-5px"></i>Enviar por WhatsApp</a></li>
                                    <li><a data-toggle="modal" data-target="#modalemail"  email="'.$d->cliente_email.'" os_id_email="'.$d->os_id.'" target="_blank"><i class="text-danger fa fa-envelope margin-right-5px"></i>Enviar por E-mail</a></li>
                                </ul>
                            </div>';

            // Verificar e formatar a data de entrada
            $entrada = '';
            if ($d->os_dataInicial != '1970-01-01') {
                $entrada = date('d/m/Y', strtotime($d->os_dataInicial)) . ' - ' . $d->os_horaInicial;
            }

            // Verificar e formatar a data de saída
            $saida = '';
            if ($d->os_dataFinal != '1970-01-01') {
                $saida = date('d/m/Y', strtotime($d->os_dataFinal)) . ' - ' . $d->os_horaFinal;
            }

            // Adicionar os dados formatados à matriz $formatted_data
            $formatted_data[] = array(
                'os_id' => $d->os_id,
                'entrada' => $entrada,
                'saida' => $saida,
                'cliente_nome' => '<td class="text-left"><a href="' . base_url() . 'clientes/visualizar/' . $d->cliente_id . '">' . $d->cliente_nome . '</a></td>',
                'equipamento' => $d->os_equipamento,
                'status' => $d->os_status,
                'valor' => number_format($d->os_valorTotal,2,".",""),
                'icon' => $icon
            );

			$icon = '';
        }

        $output = [
            "draw" => intval($this->input->post('draw')),
            "recordsTotal" => $dados['total_registros'],
            "recordsFiltered" => $dados['total_registros'],
            "data" => $formatted_data
        ];
        
        echo json_encode($output);
 	}



}