<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Estoque extends CI_Controller {

		public function __construct()
	{   
		parent::__construct();
		$this->load->model('Estoque_model');		
	}

	public function index()
	{
		$dadosView['dados'] = $this->Estoque_model->listar();

		$dadosView['meio'] = 'estoqueajuste/listar';
		$this->load->view('tema/layout',$dadosView);	
	}

	public function adicionar()
	{
	    $this->form_validation->set_rules('estoque_ajuste_categoria_id', 'Categoria', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
		{
        	$this->session->set_flashdata('erro',validation_errors());
        } else {
            $categoria = $this->input->post('estoque_ajuste_categoria_id');
        	$dados = array(  
        		'estoque_ajuste_categoria_id'   => $categoria,            
                'estoque_ajuste_observacao'     => $this->input->post('estoque_ajuste_observacao'), 						    
				'estoque_usuarios_id'           => $this->session->userdata('usuario_id'), 				
				'estoque_ajuste_data'           => date('Y-m-d'),   
				'estoque_ajuste_visivel'        => 1				  
        	);

            if (is_numeric($id = $this->Estoque_model->adicionar($dados)) ) {
               $this->Estoque_model->prepararItens($categoria,$id); 
               $this->session->set_flashdata('success','Balanço iniciada com sucesso, ajuste seu estoque.');

                redirect('estoque/editar/'.$id);
      
        	}else{
        		$this->session->set_flashdata('erro','Erro ao adicionado o registro!');
        	}
        }       

        $dadosView['grupo'] = $this->Estoque_model->pegarGrupoProduto(); 
		$dadosView['meio'] = 'estoqueajuste/adicionar';
		$this->load->view('tema/layout',$dadosView);
	}

	public function editar($id = mull)
	{
		$this->form_validation->set_rules('movimentacao_produto_documento', 'Documento', 'trim|required');
        
        if($this->form_validation->run() == FALSE)
		{
        	$this->session->set_flashdata('erro',validation_errors());
        } else {

        	$dados = array(        		 
				
				'movimentacao_produto_documento'     => $this->input->post('movimentacao_produto_documento'),             
				'movimentacao_produto_tipo'          => $this->input->post('movimentacao_produto_tipo'),							    
				'movimentacao_produto_usuario_id'      => $this->session->userdata('usuario_id'), 				
				'movimentacao_produto_data_cadastro'   => date('Y-m-d')   
										  
        	);
     
        	$resultado = $this->Estoque_model->editar($dados,$this->input->post('id'));

        	if($resultado){
        		$this->session->set_flashdata('success','Registro editado com sucesso!');
        	}else{
        		$this->session->set_flashdata('erro','Erro ao editar o registro!');
        	}
        }
          
        
        $dadosView['dados']   = $this->Estoque_model->listarId($this->uri->segment(3));       
		$dadosView['meio']    = 'estoqueajuste/editar';

		$this->load->view('tema/layout',$dadosView);

	}

	public function visualizar()
	{
		$dadosView['dados']   = $this->Estoque_model->visualizar($this->uri->segment(3));        

		$dadosView['meio']    = 'estoqueajuste/visualizar';
		$this->load->view('tema/layout',$dadosView);
	}

	public function excluir()
	{
		$id = $this->uri->segment(3);

		$dados  = array(
						'estoque_ajuste_visivel' => 0						
					  );

		$resultado = $this->Estoque_model->excluir($dados,$id);

		if($resultado){
			$this->session->set_flashdata('success','registro excluidos com sucesso!');
		}else{
			$this->session->set_flashdata('erro','Erro ao excluir o registro!');
		}

		redirect('Estoque','refresh');
	}

	public function ajustarEstoqueExe()
    {

        $dados = array(              
                
            'quantidade_atual'    => $this->input->post('valor'),             
            'idProduto'           => $this->input->post('idProduto'),                                
            'idAjuste'            => $this->input->post('idAjuste'),
            'itens_estoque_ajuste_usuario_id'      => $this->session->userdata('usuario_id')             
                                          
        );

        $this->Estoque_model->editar($dados);

        return $this->Estoque_model->editarAjusteEstoque($dados);

    }



}
