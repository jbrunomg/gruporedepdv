<?php

class Chat extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Chat_model');
	}
    
    
    public function index(){
       

        // $ip =  $this->input->ip_address();
        // $ip = $_SERVER["REMOTE_ADDR"];

        
        $parametro = $this->Chat_model->parametro();

            $sessao = array(
                    'parametro_chat_estoque'          => $parametro[0]->parametro_chat_estoque,
                    'parametro_chat_preco'            => $parametro[0]->parametro_chat_preco,
                    'parametro_chat_tentiva_busca'    => $parametro[0]->parametro_chat_tentiva_busca,
                    'parametro_chat_n_whatsapp'       => $parametro[0]->parametro_chat_n_whatsapp               

                );

            $this->session->set_userdata($sessao);



            $this->load->view('chat/index');

    }

    public function verificar(){
        $texto     = $this->input->post('texto');        

        if(is_numeric($texto)){

        $this->session->set_userdata('categoria', $texto);

        $grupo = $this->session->userdata('categoria');

        if ($grupo == '0') {
            # VOLTAR AO MENU PRINCIPAL
            $retorno['mensagem'] = '
                <div class="text">
                    <span>
                        <center>SEJA BEM VINDO AO NOSSO ATENDIMENTO AUTOMÁTICO.</center> </br>

                        Para darmos continuidade em seu atendimento, por favor escolha uma das 
                        <strong>opções</strong> abaixo. </br>
                        <center><strong>MENU DE OPÇÕES</strong></center>
                        1️⃣ - BATERIA</br>
                        2️⃣ - CARCAÇA</br>
                        3️⃣ - EQUIPAMENTO</br>
                        4️⃣ - IMPORTADO</br>
                        5️⃣ - LCD</br>
                        6️⃣ - PEÇAS</br>
                        7️⃣ - TABLET</br>
                        8️⃣ - TOUCH</br>
                        9️⃣ - ORIGINAL</br>
                        🔟 - VAPORIZADOR</br>
                        1️⃣1️⃣  - PEÇAS VAPE</br>

                    </span>
                </div>';
            echo json_encode($retorno);

        } else {
        
        switch ($grupo){ 

        case '1': 
                $grupoNome = 'BATERIA';
                break; 
            case '2': 
                $grupoNome = 'CARCACA';
                break; 
            case '3': 
                $grupoNome = 'EQUIPAMENTO';
                break;  
            case '4': 
                $grupoNome = 'IMPORTADO';
                break; 
            case '5': 
                $grupoNome = 'LCD';
                break;                
            case '6': 
                $grupoNome = 'PECAS';
                break;
            case '7': 
                $grupoNome = 'TABLET';
                break;   
            case '8': 
                $grupoNome = 'TOUCH';
                break;  
            case '9': 
                $grupoNome = 'ORIGINAL';
                break;  
            case '10': 
                $grupoNome = 'VAPORIZADOR';
                break;  
            case '11': 
                $grupoNome = 'PEÇAS VAPE';
                break;     
                                               
         
            default: 
                $grupoNome = 'Sem Grupo';     
               

        }

        $retorno['mensagem'] = '
                        <div class="text">
                    <span>
                        <center>TODA PESQUISA SERA RELACIONADA AO GRUPO <strong>'.$grupoNome.'</strong> </center> </br>

                        Favor inserir apenas marca e modelo do produto sem textos adicionais. </br>
                        Ex: Iphone 5c </br>
                        Ex 2 : Moto G7 Play </br>

                        0️⃣ - Menu Inicial</br>                      

                    </span>
                </div>';
        echo json_encode($retorno);

        }
        
        



        } else {

    
        $grupo = $this->session->userdata('categoria');    

        $result = $this->Chat_model->pesquisar($texto,$grupo);

        if(!isset($result[0])){

            $result = $this->Chat_model->pesquisar2($texto,$grupo);

             if(isset($result[0])){          

                $result = $this->Chat_model->pesquisar($result[0]->nome_sistema,$grupo);

             }

        }
      
       

            if(isset($result[0])){

            $dados = array(
                'situacao' => 'Encontrado',
                'chave' => $texto,
                'categotia_id' => $grupo
             
            );   

            $this->Chat_model->salvarPesquisa($dados);

            foreach ($result as $chave => $v) {

                if ($this->session->userdata('parametro_chat_estoque') == 'S') {                    
                    # Exibir apenar com estoque...
                    if ($v->produto_estoque >= 1) {

                    $retorno[$chave]['mensagem'] = $v->produto_descricao. ' </br> '.$v->valor;    
                        
                    } else {

                    $retorno[$chave]['mensagem'] = $v->produto_descricao. ' </br> em falta no momento.';     

                    }

                } else{

                $retorno[$chave]['mensagem'] = $v->produto_descricao. ' </br> '.$v->valor;

                }

            }
             

                $padrao['mensagem'] = 'Algo mais Sr(a)? 😊';

                array_push($retorno,$padrao);
             
                echo json_encode($retorno);
            } else {

                $dados = array(
                    'situacao' => 'Não Encontrado',
                    'chave' => $texto,
                    'categotia_id' => $grupo
                 
                );   

                $this->Chat_model->salvarPesquisa($dados);
             

                $retorno['mensagem'] = 'Não encontramos o produto digitado, por favor tente ser mais claro.';
                echo json_encode($retorno);
            }



        }
        
    }
}