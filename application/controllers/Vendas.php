<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Vendas extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Vendas_model');
  }

  public function index()
  {
    $data['meio']   = 'vendas/listar';
    $this->load->view('tema/layout', $data);
  }

  public function editar()
  {
    $dadosView['emitente']        = $this->Vendas_model->emitente();
    $dadosView['categoria_prod']  = $this->Vendas_model->pegarCategoriaProd();
    $dadosView['dados']          = $this->Vendas_model->listarId($this->uri->segment(3));
    $dadosView['produtos']       = $this->Vendas_model->getProdutos($this->uri->segment(3));
    $dadosView['financeiro']     = $this->Vendas_model->financeiroOLD($this->uri->segment(3));
    $dadosView['fornecedor']     = $this->Vendas_model->pegarFornecedor();
    $dadosView['meio']           = 'vendas/editar';

    $this->load->view('tema/layout', $dadosView);
  }

  public function editarExe()
  {
    $id = $this->input->post('id');
    $this->form_validation->set_rules('observacao', 'Observação', 'trim|required');

    if ($this->form_validation->run() == FALSE) {

      $this->session->set_flashdata('erro', validation_errors());
      $this->editar($id);
    } else {

      $data = $this->input->post('dataVenda');
      $data = str_replace("/", "-", $data);

      if ($this->input->post('nota') == NULL) {
        $nota = 0;
      } else {
        $nota = 1;
      }


      $dados = array(
        'dataVenda'          => date('Y-m-d',  strtotime($data)),
        'clientes_id'         => $this->input->post('cliente_id'),
        'usuarios_id'         => $this->session->userdata('usuario_id'),
        'observacao'          => $this->input->post('observacao'),
        'emitirNota'          => $nota
      );

      $financeiro = array(
        'financeiro_forn_clie'          => trim(explode('|', $this->input->post('cliente_nome'))[0]),
        'financeiro_forn_clie_id'         => $this->input->post('cliente_id'),
      );


      $resultado = $this->Vendas_model->editar($dados, $financeiro, $this->input->post('id'));

      if ($resultado) {
        $this->session->set_flashdata('success', 'Registro editado com sucesso!');
      } else {
        $this->session->set_flashdata('error', 'Erro ao editar o registro!');
      }
    }

    redirect('vendas/editar/' . $id);
  }


  public function visualizar()
  {
    $id              = $this->uri->segment(3);
    $resultado       = $this->Vendas_model->visualizar($id);
    $emitente        = $this->Vendas_model->emitente();
    $financeiro      = $this->Vendas_model->financeiro($id);

    $data['emitente']  = $emitente;
    $data['venda']     = $id;
    $data['cliente']   = $resultado[0]->cliente_nome;
    $data['vendendor']   = $resultado[0]->usuario_nome;
    $data['email']     = $resultado[0]->cliente_email;
    $data['dataVenda'] =  date('d/m/Y H:i:s',  strtotime($resultado[0]->data_atualizacao));
    $data['total']     = $resultado[0]->valorTotal;

    if (empty($resultado[0]->valorRecebido)) {

      $data['ValorPago'] = 0.00;
      $troco = 0.00;
    } else {
      $data['ValorPago'] = $resultado[0]->valorRecebido;
      $troco = floatval($resultado[0]->valorRecebido) - floatval($resultado[0]->valorTotal);
    }

    $data['forma_pagamento'] = $financeiro[0]->financeiro_forma_pgto;
    $data['troco']     = number_format($troco, 2, '.', '.');
    $data['dados']     = $resultado;
    $data['meio']      = "vendas/visualizar";

    $this->load->view('tema/impressao', $data);
  }

  public function visualizarNota($id = null, $visivel = NULL)
  {
    $id = $this->uri->segment(3);
    $resultado       = $this->Vendas_model->visualizar($id, $visivel);
    $emitente        = $this->Vendas_model->emitente();
    $financeiro      = $this->Vendas_model->financeiro($id);


    // var_dump($financeiro);die();

    $str     = date('d/m/Y H:i:s',  strtotime($resultado[0]->data_atualizacao));
    $order   = array("/", ":", " ");
    $replace = '';
    $protocolo = str_replace($order, $replace, $str);
    $protocolo = $id . $protocolo;

    $data['notaExcl']  = $visivel;
    // var_dump($data['notaExcl'] );die();

    $data['emitente']  = $emitente;
    $data['venda']     = $id;
    $data['cliente']   = $resultado[0]->cliente_nome;
    $data['vendendor'] = $resultado[0]->usuario_nome;
    $data['email']     = $resultado[0]->cliente_email;
    $data['dataVenda'] =  date('d/m/Y H:i:s',  strtotime($resultado[0]->dataVenda . '' . $resultado[0]->horaVenda));
    $data['dataAtualizacao'] =  date('d/m/Y H:i:s',  strtotime($resultado[0]->data_atualizacao));
    $data['total']     = $resultado[0]->valorTotal;
    $data['protocolo']     = $protocolo;
    $resultado[0]->desconto == NULL ?  $data['desconto'] = 0.00 : $data['desconto'] = $resultado[0]->desconto;


    if (empty($resultado[0]->valorRecebido)) {

      $data['ValorPago'] = 0.00;
      $troco = 0.00;
    } else {
      $data['ValorPago'] = $resultado[0]->valorRecebido;
      $troco = floatval($resultado[0]->valorRecebido) - floatval($resultado[0]->valorTotal);
    }

    // $data['forma_pagamento'] = empty($financeiro[0]->financeiro_forma_pgto)  ? '--' : $financeiro[0]->financeiro_forma_pgto;


    $data['financeiro'] = $financeiro;

    $data['troco']     = number_format($troco, 2, '.', '.');
    $data['dados']     = $resultado;
    $data['meio']      = "vendas/visualizarNota";

    $this->load->view('tema/impressaoCupom', $data);
  }


  public function excluirProduto()
  {
    $idVendas       = $this->input->post('idVendas');
    $quantidade     = $this->input->post('quantidadeM');
    $produto        = $this->input->post('produtoID');
    $idItens        = $this->input->post('idItens');
    $motivo         = $this->input->post('motivo');
    $observacao     = $this->input->post('observacao');
    $precoCusto     = $this->input->post('precoCusto');
    $precoVendido   = $this->input->post('precoVendido');
    $fornecedor_id  = $this->input->post('fornecedor_id');
    $usuario        = $this->session->userdata('usuario_id');

    $this->form_validation->set_rules('quantidadeM', 'Quantidade', 'trim|required');

    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('erro', validation_errors());
      redirect('vendas/editar/' . $idVendas);
    } else {

      if ($motivo == '1') { // Peça ruim com defeito (trocar por outra da mesma)

        $resultado = $this->Vendas_model->ConsultaEstoque($produto);

        if (intval($resultado[0]->produto_estoque) > 0) {

          $resultado = $this->Vendas_model->estoqueRetirada($quantidade, $produto);
          if(!$resultado) {
            $this->session->set_flashdata('erro', 'Erro ao atualizar o estoque dos produtos!');
            redirect('vendas/editar/' . $idVendas);
          }

          $resultado = $this->Vendas_model->avaria($idVendas, $produto, $quantidade, $motivo, $precoCusto, $precoVendido, $observacao, $usuario, $fornecedor_id);
          if(!$resultado) {
            $this->session->set_flashdata('erro', 'Erro ao adicionar informação de avaria!');
            redirect('vendas/editar/' . $idVendas);
          }

          $this->session->set_flashdata('success', 'Dados alterados com sucesso!');
          redirect('vendas/editar/' . $idVendas);
        } else {
          $this->session->set_flashdata('erro', 'Erro produto não se encontra no estoque!');
          redirect('vendas/editar/' . $idVendas);
        }

      } elseif ($motivo == '2') { // Cliente não quis o produto, porem sem defeito (cliente pediu $$ de volta)

        $resultado = $this->Vendas_model->avaria($idVendas, $produto, $quantidade, $motivo, $precoCusto, $precoVendido, $observacao, $usuario, $fornecedor_id);
        if(!$resultado) {
          $this->session->set_flashdata('erro', 'Erro ao adicionar informação de avaria!');
          redirect('vendas/editar/' . $idVendas);
        }
        
        $resultado = $this->Vendas_model->estoqueAdd($quantidade, $produto);
        if(!$resultado) {
          $this->session->set_flashdata('erro', 'Erro ao atualizar estoque do produto!');
          redirect('vendas/editar/' . $idVendas);
        }
        
        $resultado = $this->Vendas_model->retirarItens($quantidade, $idItens);
        if(!$resultado) {
          $this->session->set_flashdata('erro', 'Erro ao retirar item da venda!');
          redirect('vendas/editar/' . $idVendas);
        }

        $resultado = $this->Vendas_model->atualizarVenda($idVendas);
        if(!$resultado) {
          $this->session->set_flashdata('erro', 'Erro ao atualizar venda!');
          redirect('vendas/editar/' . $idVendas);
        }

        $resultado = $this->Vendas_model->atualizarFinanceiro($idVendas);
        if(!$resultado) {
          $this->session->set_flashdata('erro', 'Erro atualizar financeiro!');
          redirect('vendas/editar/' . $idVendas);
        }
        $this->session->set_flashdata('success', 'Dados alterados com sucesso!');
        redirect('vendas/editar/' . $idVendas);
      } else { //  Peça ruim com defeito (cliente pediu $$ de volta)

        $resultado = $this->Vendas_model->avaria($idVendas, $produto, $quantidade, $motivo, $precoCusto, $precoVendido, $observacao, $usuario, $fornecedor_id);
        if(!$resultado) {
          $this->session->set_flashdata('erro', 'Erro ao adicionar informação de avaria!');
          redirect('vendas/editar/' . $idVendas);
        }

        $resultado = $this->Vendas_model->retirarItens($quantidade, $idItens);
        if(!$resultado) {
          $this->session->set_flashdata('erro', 'Erro ao retirar item da venda!');
          redirect('vendas/editar/' . $idVendas);
        }

        $resultado = $this->Vendas_model->atualizarVenda($idVendas);
        if(!$resultado) {
          $this->session->set_flashdata('erro', 'Erro ao atualizar venda!');
          redirect('vendas/editar/' . $idVendas);
        }
        
        $resultado = $this->Vendas_model->atualizarFinanceiro($idVendas);
        if(!$resultado) {
          $this->session->set_flashdata('erro', 'Erro atualizar financeiro!');
          redirect('vendas/editar/' . $idVendas);
        }

        $this->session->set_flashdata('success', 'Dados alterados com sucesso!');
        redirect('vendas/editar/' . $idVendas);
      }
    }
  }

  public function excluir()
  {
    $id = $this->uri->segment(3);
    if(empty($id)){
      $this->session->set_flashdata('erro', 'Venda não encontrada.');
      redirect('vendas', 'refresh');
    }
    
    $vendaAberto =  ($this->uri->segment(4) == 'A') ? TRUE : FALSE;

    $venda_visivel = 0;
    $usuario = $this->session->userdata('usuario_id');
    $itens = $this->Vendas_model->getItensVendasQuantidadeProdutos($id);

    if ($itens) {
      $atualizarEstoqueItens = $this->Vendas_model->atualizarEstoqueExclusao($itens);
      if (!$atualizarEstoqueItens) {
        $this->session->set_flashdata('erro', 'Erro ao atualizar o estoque dos produtos!');
        redirect('vendas', 'refresh');
      }
    }

    $resultado = $this->Vendas_model->excluir($id, $usuario);

    if ($resultado) {
      $this->session->set_flashdata('success', 'Venda excluída com sucesso!');
    } else {
      $this->session->set_flashdata('erro', 'Erro ao excluir os dados!');
      redirect('vendas', 'refresh');
    }

    if ($vendaAberto) {
      redirect('vendas/listarAberto', 'refresh');
    } else {
      redirect('vendas', 'refresh');
    }
  }




  public function visualizarDanfe($id)
  {
    # code...       
    $id   = $this->uri->segment(3);
    $resultado       = $this->Vendas_model->visualizar($id);
    $emitente        = $this->Vendas_model->emitente();
    $financeiro      = $this->Vendas_model->financeiro($id);

    $str     = date('d/m/Y H:i:s',  strtotime($resultado[0]->data_atualizacao));
    $order   = array("/", ":", " ");
    $replace = '';
    $protocolo = str_replace($order, $replace, $str);
    $protocolo = $id . $protocolo;

    $data['emitente']   = $emitente;

    // $data['venda']      = $id;
    // $data['cliente']    = $resultado[0]->cliente_nome;
    // $data['vendendor']  = $resultado[0]->usuario_nome;
    // $data['email']      = $resultado[0]->cliente_email;
    // $data['dataVenda']  = date('d/m/Y H:i:s',  strtotime($resultado[0]->data_atualizacao));
    // $data['total']      = $resultado[0]->valorTotal;
    $data['protocolo']     = $protocolo;




    if (empty($resultado[0]->valorRecebido)) {

      $data['ValorPago'] = 0.00;
      $troco = 0.00;
    } else {
      $data['ValorPago'] = $resultado[0]->valorRecebido;
      $troco = floatval($resultado[0]->valorRecebido) - floatval($resultado[0]->valorTotal);
    }

    $data['forma_pagamento'] = empty($financeiro[0]->financeiro_forma_pgto)  ? '--' : $financeiro[0]->financeiro_forma_pgto;

    $data['troco']     = number_format($troco, 2, '.', '.');
    $data['dados']     = $resultado;
    $data['financeiro']   = $financeiro;


    if ((count($resultado) > 0) && (count($resultado) <= 35)) {
      $data['pagina'] = 1;
    } elseif ((count($resultado) > 35) && (count($resultado) <= 70)) {
      $data['pagina'] = 2;
    } elseif ((count($resultado) > 70) && (count($resultado) <= 105)) {
      $data['pagina'] = 3;
    } elseif (count($resultado) > 105) {
      $data['pagina'] = 4;
    }

    // echo "<pre>";
    // var_dump($data);die();


    if (SUBDOMINIO == 'localhost'  or GRUPOLOJA == 'gruporeccell' or GRUPOLOJA == 'grupobyte') {
      $this->load->view('vendas/visualizarDanfe2', $data);
    } else {
      $this->load->view('vendas/visualizarDanfe', $data);
    }
  }



  public function visualizarNtGarantia($id)
  {
    # code...       

    $id   = $this->uri->segment(3);
    $resultado       = $this->Vendas_model->visualizar($id);
    $emitente        = $this->Vendas_model->emitente();
    $financeiro      = $this->Vendas_model->financeiro($id);


    $str     = date('d/m/Y H:i:s',  strtotime($resultado[0]->data_atualizacao));
    $order   = array("/", ":", " ");
    $replace = '';
    $protocolo = str_replace($order, $replace, $str);
    $protocolo = $id . $protocolo;
    $data['emitente']   = $emitente;
    $data['protocolo']  = $protocolo;

    if (empty($resultado[0]->valorRecebido)) {
      $data['ValorPago'] = 0.00;
      $troco = 0.00;
    } else {
      $data['ValorPago'] = $resultado[0]->valorRecebido;
      $troco = floatval($resultado[0]->valorRecebido) - floatval($resultado[0]->valorTotal);
    }

    $data['forma_pagamento'] = empty($financeiro[0]->financeiro_forma_pgto)  ? '--' : $financeiro[0]->financeiro_forma_pgto;
    $data['troco']     = number_format($troco, 2, '.', '.');
    $data['dados']     = $resultado;

    if ((count($resultado) > 0) && (count($resultado) <= 25)) {
      $data['pagina'] = 1;
    } elseif ((count($resultado) > 25) && (count($resultado) <= 60)) {
      $data['pagina'] = 2;
    } elseif ((count($resultado) > 60) && (count($resultado) <= 95)) {
      $data['pagina'] = 3;
    } elseif (count($resultado) > 95) {
      $data['pagina'] = 4;
    }

    // echo "<pre>";
    // var_dump($data);die();

    $this->load->view('vendas/visualizarNtGarantia', $data);
  }


  // public function listarAberto($page = 1, $pesquisa = NULL)
  // {
  //   $pesquisa = isset($_GET['pesquisa']) ? $_GET['pesquisa'] : $pesquisa;
  //   $itens_per_page = 15;
  //   $data['dados']  = $this->Vendas_model->listarAberto($page, $itens_per_page, $pesquisa);    

  //   $total = $this->Vendas_model->listarAbertoContador($page, $pesquisa);    
  //   if ($total){
  //       $data['total_vendas']   = $total[0]->total;
  //   }else{
  //       $data['total_vendas']   = null;
  //   }

  //   $data['page']           = $page;
  //   $data['pesquisa']       = $pesquisa;
  //   $data['itens_per_page'] = $itens_per_page;

  //   $data['meio']   = 'vendas/listar';
  //   $this->load->view('tema/layout',$data);
  // }


  public function listarAberto()
  {
    $dadosView['dados'] = $this->Vendas_model->listarAberto();

    $dadosView['meio']  = 'vendasaberto/listar';
    $this->load->view('tema/layout', $dadosView);
  }

  public function listarZerada()
  {
    $dadosView['dados'] = $this->Vendas_model->listarZerada();

    $dadosView['meio']  = 'vendaszerada/listar';
    $this->load->view('tema/layout', $dadosView);
  }


  public function listarVendaIncompleta()
  {
    $dadosView['dados'] = $this->Vendas_model->listarVendaIncompleta();

    $dadosView['meio']  = 'vendas/listarVendaIncompleta';
    $this->load->view('tema/layout', $dadosView);
  }

  public function reativaZeradas()
  {
    $resultado = $this->Vendas_model->reativaZeradas($this->uri->segment(3));

    if ($resultado) {
      $this->session->set_flashdata('success', 'Venda reativada com sucesso!');
    } else {
      $this->session->set_flashdata('erro', 'Erro ao reativar os dados!');
    }
    redirect('vendas/listarZerada', 'refresh');
  }

  public function buscarImei()
  {

    $produto   = $this->input->post('produto_id');
    $imei = $this->Vendas_model->buscarImei($produto);

    if (count($imei) == 0) {
      echo "<option value=''>Sem Imei</option>";
    } else {
      foreach ($imei as $p) {
        echo "<option value='" . $p->imei_valor . "'>" . $p->produto_descricao . " - " . $p->imei_valor . "</option>";
      }
    }
  }

  public function buscarImeiTodos()
  {

    $imei = $this->Vendas_model->buscarImeiTodos();

    if (count($imei) == 0) {
      echo "<option value=''>Sem Imei</option>";
    } else {
      foreach ($imei as $p) {
        echo "<option value='" . $p->imei_valor . "'>" . $p->produto_descricao . " - " . $p->imei_valor . "</option>";
      }
    }
  }

  public function trocaImei()
  {
    $idVendas   = $this->input->post('idVendasM');
    $produto    = $this->input->post('produtoIDM');
    $quantidade = '1';
    $motivo     = $this->input->post('motivoMT');
    $precoCusto = $this->input->post('precoCustoM');
    $precoVendido = $this->input->post('precoVendidoM');
    $observacao   = $this->input->post('observacaoM');
    $usuario      = $this->session->userdata('usuario_id');
    $fornecedor_id = 1;
    $idItens    = $this->input->post('idItensM');
    $antigoImei = $this->input->post('antigoImei');
    $novoImei   = $this->input->post('novoImei');

    if (empty($novoImei)) {
      $this->session->set_flashdata('erro', 'Sem Imei Disponível!');
      redirect('vendas/editar/' . $idVendas);
    }

    $produtoNovo =  $this->Vendas_model->imeiBuscar($novoImei)[0]->produto_id;
    $precoCustoProdutoNovo = $this->Vendas_model->imeiBuscar($novoImei)[0]->produto_preco_custo;

    $avariaId = $this->Vendas_model->avariaId($idVendas, $produto, $quantidade, $motivo, $precoCusto, $precoVendido, $observacao, $usuario, $fornecedor_id, $antigoImei, $novoImei);

    if ($avariaId !== FALSE) {  // Verifica se o retorno não é FALSE, indicando que o ID foi inserido com sucesso
      if ($motivo == '4') {
        $dados = array('imei_valor' => $novoImei);
        $where = array('produtos_id' => $produto, 'vendas_id' => $idVendas, 'imei_valor' => $antigoImei);
        $this->Vendas_model->trocaImei('itens_de_vendas', $dados, null, null, $where);
      } else {
        $dados = array('imei_valor' => $novoImei, 'produtos_id' => $produtoNovo, 'prod_preco_custo' => $precoCustoProdutoNovo);
        $where = array('produtos_id' => $produto, 'vendas_id' => $idVendas, 'imei_valor' => $antigoImei);
        $this->Vendas_model->trocaImei('itens_de_vendas', $dados, null, null, $where);

        $this->Vendas_model->estoqueRetirada($quantidade, $produtoNovo);
        $this->Vendas_model->estoqueAdd($quantidade, $produto);
      }

      $dados = array('vendas_id' => null);
      $this->Vendas_model->trocaImei('itens_de_imei', $dados, 'imei_valor', $antigoImei);

      $dados = array('vendas_id' => $idVendas);
      $this->Vendas_model->trocaImei('itens_de_imei', $dados, 'imei_valor', $novoImei);

      $this->session->set_flashdata('success', 'Dados alterados com sucesso!');

      // Carregar uma view temporária com o script JavaScript
      $data['redirect_url'] = base_url("vendas/editar/{$idVendas}");
      $data['popup_url'] = base_url("vendas/visualizarTroca/{$avariaId}");
      $this->load->view('vendas/redirect_and_open_view', $data);
      return;
    }


    $this->session->set_flashdata('erro', 'Ocorreu um erro ao tentar atualizar os dados.');
    redirect('vendas/editar/' . $idVendas);
  }

  public function visualizarTroca($id = null)
  {
    $id = $this->uri->segment(3);
    $resultado = $this->Vendas_model->buscarAvaria($id);
    $emitente = $this->Vendas_model->emitente();

    $str     = date('d/m/Y H:i:s',  strtotime($resultado[0]->data_atualizacao));
    $order   = array("/", ":", " ");
    $replace = '';
    $protocolo = str_replace($order, $replace, $str);
    $protocolo = $id . $protocolo;


    $data['emitente']  = $emitente;
    $data['troca']     = $id;
    $data['cliente']   = $resultado[0]->cliente_nome;
    $data['vendendor'] = $resultado[0]->usuario_nome;
    $data['email']     = $resultado[0]->cliente_email;
    $data['dataVenda'] =  date('d/m/Y',  strtotime($resultado[0]->produto_avaria_cadastro));
    $data['dataAtualizacao'] =  date('d/m/Y H:i:s',  strtotime($resultado[0]->produto_data_atualizacao));
    $data['protocolo']     = $protocolo;
    $data['antigo']     = $this->Vendas_model->imeiBuscarAvaria($resultado[0]->produto_avaria_antigo_imei);
    $data['novo']     = $this->Vendas_model->imeiBuscarAvaria($resultado[0]->produto_avaria_novo_imei);
    $data['dados']     = $resultado;
    $data['meio']      = "vendas/visualizarTroca";

    $this->load->view('tema/impressaoCupom', $data);
  }

  public function visualizarTrocaVenda($id = null)
  {
    $id = $this->uri->segment(3);
    $resultados = $this->Vendas_model->buscarAvariaVenda($id);
    $emitente = $this->Vendas_model->emitente();

    $data = [];
    foreach ($resultados as $resultado) {
      $str = date('d/m/Y H:i:s', strtotime($resultado->data_atualizacao));
      $order = ["/", ":", " "];
      $replace = '';
      $protocolo = str_replace($order, $replace, $str);
      $protocolo = $id . $protocolo;

      $dados[] = [
        'cliente' => $resultado->cliente_nome,
        'vendendor' => $resultado->usuario_nome,
        'email' => $resultado->cliente_email,
        'dataVenda' => date('d/m/Y', strtotime($resultado->produto_avaria_cadastro)),
        'dataAtualizacao' => date('d/m/Y H:i:s', strtotime($resultado->produto_data_atualizacao)),
        'protocolo' => $protocolo,
        'antigo' => $this->Vendas_model->imeiBuscarAvaria($resultado->produto_avaria_antigo_imei),
        'novo' => $this->Vendas_model->imeiBuscarAvaria($resultado->produto_avaria_novo_imei),
        'dados' => $resultado
      ];
    }

    $data['emitente'] = $emitente;
    $data['resultados'] = $dados;
    $data['meio'] = "vendas/visualizarTrocaVenda";

    $this->load->view('tema/impressaoCupom', $data);
  }

  public function inverteVenda()
  {

    $idVendas   = $this->input->post('id');
    $idFinanceiro = $this->input->post('idFinanceiro');
    $forma_pag = 'Sem pagamento';
    $faturado = '0';
    $baixado = '0';

    $dataVenda = array(
      'faturado'  => $faturado
    );

    $dataFinanceiro = array(
      'financeiro_baixado' => $baixado,
      'financeiro_forma_pgto' => $forma_pag
    );

    if ($this->Vendas_model->inverteVenda($idVendas, $idFinanceiro, $dataVenda, $dataFinanceiro) == true) {
      echo json_encode(array('result' => true));
    } else {
      echo json_encode(array('result' => false));
    }
  }

  public function notaXML()
  {
    $idVendas  =   $id = $this->uri->segment(3);
    $xml = $this->montarXML($id);
    $dom = new DOMDocument;
    $dom->preserveWhiteSpace = FALSE;
    $dom->loadXML($xml);
    $dom->formatOutput = TRUE;
    $name = strftime('nota_' . $idVendas . '.xml');
    header('Content-Disposition: attachment;filename=' . $name);
    header('Content-Type: text/xml');
    echo $dom->saveXML();
  }

  public function montarXML($id)
  {
    $dados           = $this->Vendas_model->visualizar($id);
    $emitente        = $this->Vendas_model->emitente();
    $financeiro      = $this->Vendas_model->financeiro($id);
    $xml = gerarXML($dados, $emitente, $financeiro);
    return $xml;
  }

  public function listarDados()
  {

    $limit = $this->input->post('length');
    $start = $this->input->post('start');
    $search = $this->input->post('search') ?? ''; // Verifica se 'value' existe

    $dados = $this->Vendas_model->listar($limit, $start, $search);
    $formatted_data = [];
    $icon = '';

    foreach ($dados['dados'] as $d) {

      if (verificarPermissao('vVenda')) {
        if ($d->faturado == '1') {
          $icon .= '<a href="#" onClick="(function(){window.open(\'' . base_url() . $this->uri->segment(1) . '/visualizarNota/' . $d->idVendas . '\',\'MyWindow\',\'toolbar=no,location=no,directories=no,status=no,menubar=yes,scrollbars=yes,resizable=yes,width=350,height=600\');return false;})();return false;" data-toggle="tooltip" title="Visualizar"><i class="fa fa-search text-success"></i></a>';
        }

        if ($d->avaria > 0) {
          $icon .= '<a href="#" onClick="(function(){window.open(\'' . base_url() . $this->uri->segment(1) . '/visualizarTrocaVenda/' . $d->idVendas . '\',\'MyWindow\',\'toolbar=no,location=no,directories=no,status=no,menubar=yes,scrollbars=yes,resizable=yes,width=350,height=600\');return false;})();return false;" data-toggle="tooltip" title="Visualizar Trocas"><i class="fa fa-print text-warning"></i></a>';
        }
      }

      if (verificarPermissao('eVenda')) {
        $icon .= '<a href="' . base_url() . $this->uri->segment(1) . '/notaXML/' . $d->idVendas . '" data-toggle="tooltip" title="XML"><i class="fa fa-download text-info"></i></a>';
        $icon .= '<a href="' . base_url() . $this->uri->segment(1) . '/editar/' . $d->idVendas . '" data-toggle="tooltip" title="Editar"><i class="fa fa-edit"></i></a>';
      }

      if (verificarPermissao('dVenda')) {
        $icon .= '<a style="cursor:pointer;" title="Deletar" data-toggle="modal" data-target="#excluirVendas" onclick="excluirVendasModal(\'' . base_url() . $this->uri->segment(1) . '/excluir/' . $d->idVendas . '\', \'' . $d->idVendas . '\');" ><i class="fa fa-trash text-danger"></i></a>';
      }

      // Verifica se a data da venda está definida e não é vazia
      if (!empty($d->dataVenda)) {
        // Converte a data para o formato brasileiro (dd/mm/aaaa)
        $dataFormatada = date('d/m/Y', strtotime($d->dataVenda));
      } else {
        // Se a data da venda não estiver definida, atribui uma string vazia
        $dataFormatada = '';
      }

      $formatted_data[] = array(
        'usuario_nome' =>  $d->usuario_nome,
        'cliente_nome' => $d->cliente_nome,
        'idVendas' => $d->idVendas,
        'dataVenda' => $dataFormatada, // Adiciona a data formatada aqui
        'valorTotal' => number_format($d->valorTotal, 2, ".", ""),
        'icon' => $icon
      );

      $icon = '';
    }

    $output = [
      "draw" => intval($this->input->post('draw')),
      "recordsTotal" => $dados['total_registros'],
      "recordsFiltered" => $dados['total_registros'],
      "data" => $formatted_data
    ];

    echo json_encode($output);
  }
}
