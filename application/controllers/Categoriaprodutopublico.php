<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categoriaprodutopublico extends CI_Controller {

		public function __construct()
	{   
		parent::__construct();
        $this->load->model('Categoriaproduto_model');
        $this->load->model('Cadastroimei_model');
        $this->load->model('simulador_model');
	}

    public function buscar($short_link = null)
    {
        if(empty($short_link) or !isset($short_link)){
            header("Location: http://www.wdmtecnologia.com.br");
            exit();
        }

        $id = base64_decode(urldecode($short_link));
        $dadosView['dados']   = $this->Categoriaproduto_model->listarProdutoId($id);
        $dadosView['meio']    = 'categoriaproduto/visualizar';
        $this->load->view('categoriaprodutopublico/listar',$dadosView);
    }

    public function visualizarImei()
    {
        $id   = $this->input->post('id');
		$dados   = $this->Cadastroimei_model->getImeisListProduto($id);
		echo json_encode($dados);
    }
    
    public function simular()
    {
        $valorProd = $this->input->post('vlprod');
        $valorEntr = $this->input->post('vlentr');
    
        // Convertendo os valores de string para float
        $valorProd = $valorProd ? floatval(str_replace(',', '.', $valorProd)) : 0;
        $valorEntr = $valorEntr ? floatval(str_replace(',', '.', $valorEntr)) : 0;        
    
        $qtdParcela = 18;
        $valorSimular = $valorProd - $valorEntr;
    
        $simulado = [
            'valorCompra' => number_format($valorProd, 2, ',', '.'), // Formatação em PT-BR
            'valorEntrada' => number_format($valorEntr, 2, ',', '.'),
            'valorSimular' => number_format($valorSimular, 2, ',', '.'),
            'qtdParcela' => $qtdParcela,
        ];

    
        // Obter opções de parcelamento do modelo
        $resultadoParcelamento = $this->simulador_model->getSimular($valorSimular);
    
        // Formatando os valores do parcelamento
        $resultado = [];
        foreach ($resultadoParcelamento as $parcela) {
            // Verifica se o número de parcelas é maior que zero
            if ($parcela->numero_parcela > 0) {
                
                if ($parcela->tipo_parcela == 'pix') {
                    $vlParcela = $valorSimular + $parcela->valor_parcela;
                }else{
                    $vlParcela = ($valorSimular + floatval($parcela->valor_parcela)) / $parcela->numero_parcela;
                }

                $resultado[] = [
                    'tipo_parcela' => $parcela->tipo_parcela,
                    'valor_parcela_formatado' => number_format($vlParcela, 2, ',', '.'),
                    'valor_total_formatado' => number_format($valorSimular + floatval($parcela->valor_parcela), 2, ',', '.'),
                ];
            } else {
                // caso de debito
            }
        }
        
    
        // Montar a resposta JSON
        $dados = [
            'simulado' => $simulado,
            'resultado' => $resultado,
        ];
    
        // Retornar a resposta como JSON
        echo json_encode($dados);
    }
    

}