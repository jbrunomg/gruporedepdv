<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categoriaproduto extends CI_Controller {

    public function __construct()
    {   
        parent::__construct();
        $this->load->model('Categoriaproduto_model');
    }

    public function index()
    {
        $dadosView['dados'] = $this->Categoriaproduto_model->listar();

        $dadosView['meio'] = 'categoriaproduto/listar';
        $this->load->view('tema/layout',$dadosView);    
    }

    public function adicionar()
    {
        $this->form_validation->set_rules('categoria_prod_descricao', 'Descrição', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('erro',validation_errors());
        } else {

            $link = $this->input->post('categoria_prod_link') == 'on' ? '1' : '0';
            $dados = array(              
                  'categoria_prod_descricao'     => $this->input->post('categoria_prod_descricao'),
                  'categoria_prod_margem'     => $this->input->post('categoria_prod_margem'),                  
                  'categoria_prod_data_cadastro' => date('Y-m-d'), 
                  'categoria_prod_link'          => $link,
                  'categoria_prod_visivel'       => 1                  
            );

            $resultado = $this->Categoriaproduto_model->adicionar($dados);

            if($resultado){
                $this->session->set_flashdata('success','Registro adicionado com sucesso!');
            }else{
                $this->session->set_flashdata('erro','Erro ao adicionado o registro!');
            }
        }

        $dadosView['meio'] = 'categoriaproduto/adicionar';
        $dadosView['lojas'] = $this->Categoriaproduto_model->getAllFilias();       
        $this->load->view('tema/layout',$dadosView);
    }

    public function editar()
    {
        $this->form_validation->set_rules('categoria_prod_descricao', 'Descrição', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('erro',validation_errors());
        } else {

            $link = $this->input->post('categoria_prod_link') == 'on' ? '1' : '0';
            $dados = array(              
                  'categoria_prod_descricao'  => $this->input->post('categoria_prod_descricao'),
                  'categoria_prod_margem'     => $this->input->post('categoria_prod_margem'),
                  'categoria_prod_link'       => $link               
            );
     
            $resultado = $this->Categoriaproduto_model->editar($dados,$this->input->post('id'));

            if($resultado){
                $this->session->set_flashdata('success','Registro editado com sucesso!');
            }else{
                $this->session->set_flashdata('erro','Erro ao editar o registro!');
            }
        }
      
        $dadosView['dados']   = $this->Categoriaproduto_model->listarId($this->uri->segment(3));
        $dadosView['lojas'] = $this->Categoriaproduto_model->getAllFilias();
        $dadosView['meio']    = 'categoriaproduto/editar';

        $this->load->view('tema/layout',$dadosView);

    }

    public function visualizar()
    {
        // Construir a URL
        $dadosView['link'] = URLCAMINHO . 'categoriaprodutopublico/buscar/' . urlencode(base64_encode($this->uri->segment(3)));
        $dadosView['dados']   = $this->Categoriaproduto_model->listarId($this->uri->segment(3));
        $dadosView['meio']    = 'categoriaproduto/visualizar';
        $this->load->view('tema/layout',$dadosView);
    }

    public function excluir()
    {
        $id = $this->uri->segment(3);

        $dados  = array(
                        'categoria_prod_visivel' => 0                        
                      );
        $resultado = $this->Categoriaproduto_model->excluir($dados,$id);

        if($resultado){
            $this->session->set_flashdata('success','registro excluidos com sucesso!');
        }else{
            $this->session->set_flashdata('erro','Erro ao excluir o registro!');
        }

        redirect('Categoriaproduto','refresh');
    }
}
