<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categoriafinanceiro extends CI_Controller {

    public function __construct()
    {   
        parent::__construct();
        $this->load->model('Categoriafinanceiro_model');
    }

    public function index()
    {
        $dadosView['dados'] = $this->Categoriafinanceiro_model->listar();

        $dadosView['meio'] = 'categoriafinanceiro/listar';
        $this->load->view('tema/layout',$dadosView);    
    }

    public function adicionar()
    {
        $this->form_validation->set_rules('categoria_fina_descricao', 'Descrição', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('erro',validation_errors());
        } else {

            $dados = array(              
                  'categoria_fina_descricao'     => $this->input->post('categoria_fina_descricao'),
                  'categoria_fina_data_cadastro' => date('Y-m-d'), 
                  'categoria_fina_visivel'       => 1                  
            );

            $resultado = $this->Categoriafinanceiro_model->adicionar($dados);

            if($resultado){
                $this->session->set_flashdata('success','Registro adicionado com sucesso!');
            }else{
                $this->session->set_flashdata('erro','Erro ao adicionado o registro!');
            }
        }

        $dadosView['meio'] = 'categoriafinanceiro/adicionar';
        $this->load->view('tema/layout',$dadosView);
    }

    public function editar()
    {
        $this->form_validation->set_rules('categoria_fina_descricao', 'Descrição', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('erro',validation_errors());
        } else {

            $dados = array(              
                  'categoria_fina_descricao'     => $this->input->post('categoria_fina_descricao')
                                 
            );
     
            $resultado = $this->Categoriafinanceiro_model->editar($dados,$this->input->post('id'));

            if($resultado){
                $this->session->set_flashdata('success','Registro editado com sucesso!');
            }else{
                $this->session->set_flashdata('erro','Erro ao editar o registro!');
            }
        }
      
        $dadosView['dados']   = $this->Categoriafinanceiro_model->listarId($this->uri->segment(3));
        $dadosView['meio']    = 'categoriafinanceiro/editar';

        $this->load->view('tema/layout',$dadosView);

    }

    public function visualizar()
    {
                
        $dadosView['dados']   = $this->Categoriafinanceiro_model->listarId($this->uri->segment(3));
        $dadosView['meio']    = 'categoriafinanceiro/visualizar';
        $this->load->view('tema/layout',$dadosView);
    }

    public function excluir()
    {
        $id = $this->uri->segment(3);

        $dados  = array(
                        'categoria_fina_visivel' => 0                        
                      );
        $resultado = $this->Categoriafinanceiro_model->excluir($dados,$id);

        if($resultado){
            $this->session->set_flashdata('success','registro excluidos com sucesso!');
        }else{
            $this->session->set_flashdata('erro','Erro ao excluir o registro!');
        }

        redirect('Categoriafinanceiro','refresh');
    }
}
