<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {

		public function __construct()
	{   
		parent::__construct();
		$this->load->model('Usuarios_model');
	}

	public function index()
	{
		$dadosView['dados'] = $this->Usuarios_model->listar();

		$dadosView['meio'] = 'usuarios/listar';
		$this->load->view('tema/layout',$dadosView);	
	}

	public function adicionar()
	{
	    $this->form_validation->set_rules('usuario_cpf', 'CPF', 'trim|required|is_unique[usuarios.usuario_cpf]');
       
        if($this->form_validation->run() == FALSE)
		{
        	$this->session->set_flashdata('erro',validation_errors());
        } else {

        	$dataNasc = ($this->input->post('usuario_datanasc')) ? date('Y-m-d', strtotime(str_replace('/','-',$this->input->post('usuario_datanasc')))) : NULL;

        	$dados = array(        		 				 
				  'usuario_cpf'                 => $this->input->post('usuario_cpf'),
				  'usuario_nome'                => $this->input->post('usuario_nome'),	
				  'usuario_data_nascimento'     => $dataNasc, 			 
				  'usuario_data_cadastro' 		=> date('Y-m-d'),
				  'usuario_email' 				=> $this->input->post('usuario_email'),				  
				  'usuario_senha' 		        => sha1(md5(strtolower($this->input->post('usuario_senha')))),
				  'usuario_ativo' 		        => $this->input->post('situacao'),	
				  'usuario_perfil'              => $this->input->post('usuario_perfil'),
        	);
        

        	$resultado = $this->Usuarios_model->adicionar($dados);

        	if($resultado){
        		$this->session->set_flashdata('success','Registro adicionado com sucesso!');
        	}else{
        		$this->session->set_flashdata('erro','Erro ao adicionado o registro!');
        	}
        }

		$dadosView['estados'] = $this->Usuarios_model->pegarEstados();
		$dadosView['perfil']   = $this->Usuarios_model->pegarPerfil();

		$dadosView['meio'] = 'usuarios/adicionar';
		$this->load->view('tema/layout',$dadosView);
	}

	public function editar()
	{
		$this->form_validation->set_rules('usuario_cpf', 'CPF', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
		{
        	$this->session->set_flashdata('erro',validation_errors());
        } else {
        	$dataNasc = ($this->input->post('usuario_datanasc')) ? date('Y-m-d', strtotime(str_replace('/','-',$this->input->post('usuario_datanasc')))) : NULL;

        	if($_FILES['userfile']['name'] != ''){
                $arquivo = $this->do_upload(); 
                $file = $arquivo['file_name'];
            }else{
                $file = $this->input->post('usuario_imagem');
            }

        	$dados = array(        		 				 
				  'usuario_cpf'                 => $this->input->post('usuario_cpf'),
				  'usuario_nome'                => $this->input->post('usuario_nome'),
				  'usuario_data_nascimento'     => $dataNasc, 
				  'usuario_email' 				=> $this->input->post('usuario_email'),	
				  'usuario_ativo' 		        => $this->input->post('situacao'),			  				 
				  'usuario_perfil'              => $this->input->post('usuario_perfil'),
				  'usuario_imagem'              => $file	
        	);

        	if($this->input->post('usuario_senha')){
        		
        		$dados = array_merge($dados,['usuario_senha' => sha1(md5(strtolower($this->input->post('usuario_senha'))))]);
        	}
     
        	$resultado = $this->Usuarios_model->editar($dados,$this->input->post('id'));

        	if($resultado){
        		$this->session->set_flashdata('success','Registro editado com sucesso!');
        	}else{
        		$this->session->set_flashdata('erro','Erro ao editado o registro!');
        	}
        }

        $this->load->model('Sistema_model');

        $dadosView['dados']   = $this->Usuarios_model->listarId($this->uri->segment(3));
		$dadosView['perfil']   = $this->Usuarios_model->pegarPerfil();
		//$dadosView['estados'] = $this->Usuarios_model->pegarEstados();
		//$dadosView['cidades'] = $this->Sistema_model->selecionarCidades($dadosView['dados'][0]->usuario_estado);
		$dadosView['meio']    = 'usuarios/editar';

		$this->load->view('tema/layout',$dadosView);

	}

	public function visualizar()
	{
		$this->load->model('Sistema_model');
		
		$dadosView['dados']     = $this->Usuarios_model->listarId($this->uri->segment(3));
		$dadosView['perfil']    = $this->Usuarios_model->pegarPerfil();
		//$dadosView['estados'] = $this->Usuarios_model->pegarEstados();
		//$dadosView['cidades'] = $this->Sistema_model->selecionarCidades($dadosView['dados'][0]->usuario_estado);

		$dadosView['venda']   = $this->Usuarios_model->venda($this->uri->segment(3));
		$dadosView['grupo']   = $this->Usuarios_model->grupo($this->uri->segment(3));
		$dadosView['ranck']   = $this->Usuarios_model->ranck($this->uri->segment(3));  

		// var_dump($dadosView['venda']);die();

		$dadosView['meio']  = 'usuarios/visualizar';
		$this->load->view('tema/layout',$dadosView);
	}

	public function excluir()
	{
		$id = $this->uri->segment(3);

		$dados  = array(
						'usuario_visivel' => 0					
					  );
		$resultado = $this->Usuarios_model->excluir($dados,$id);

		if($resultado){
			$this->session->set_flashdata('success','registro excluidos com sucesso!');
		}else{
			$this->session->set_flashdata('erro','Erro ao excluir o registro!');
		}

		redirect('Usuarios','refresh');
	}


	public function do_upload(){
        
        $config['upload_path']   = realpath('./assets/usuarios/');
        $config['allowed_types'] = 'txt|jpg|jpeg|gif|png|pdf|PDF|JPG|JPEG|GIF|PNG';
        $config['max_size']      = 0;
        // $config['max_width']  = '3000';
        // $config['max_height'] = '2000';
        $config['encrypt_name']  = true;


        if (!is_dir('./assets/usuarios/')) {
            mkdir('./assets/usuarios/', 0777, TRUE);
        }
        
        $this->load->library('upload', $config);
        $this->upload->initialize($config);       

        if ( ! $this->upload->do_upload())
        {
            $error = array('error' => $this->upload->display_errors());
           // var_dump($error);die();
            $this->session->set_flashdata('erro','Erro ao fazer upload do arquivo, verifique se a extensão do arquivo é permitida.');
        	redirect(base_url() . 'index.php/usuarios/adicionar/');

        }
        else
        {
            //$data = array('upload_data' => $this->upload->data());
            return $this->upload->data();

        }

    }
}
