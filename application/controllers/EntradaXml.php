<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Entradaxml extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Entradaxml_model');
        $this->load->model('Fornecedores_model');
    }


    public function index()
    {
        $data['dados'] = $this->Entradaxml_model->listar();
        $data['meio'] = 'entradaxml/listar';
        $this->load->view('tema/layout', $data);
    }

    public function adicionar()
    {
        $dadosView['meio'] = 'entradaxml/adicionar';
        $this->load->view('tema/layout', $dadosView);
    }

    /**
     * Faz o upload de um arquivo para o servidor.
     *
     * @param string $nomeInputFile Nome do input do arquivo no formulário HTML.
     * @return array Retorna informações sobre o upload ou uma mensagem de erro.
     */
    private function do_upload($nomeInputFile)
    {
        // Obtém a data atual.
        $date = date('d-m-Y');

        // Configurações para o upload.
        $config['upload_path'] = './assets/arquivos/' . $date;
        $config['allowed_types'] = 'txt|xml|XML';

        // Cria o diretório de destino se não existir.
        if (!is_dir('./assets/arquivos/' . $date)) {
            mkdir('./assets/arquivos/' . $date, 0777, TRUE);
        }

        // Inicializa a biblioteca de upload.
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        // Executa o upload do arquivo.
        if (!$this->upload->do_upload($nomeInputFile)) {
            // Retorna mensagem de erro se o upload falhar.
            return ['erro' => $this->upload->display_errors()];
        } else {
            // Retorna informações sobre o upload se bem-sucedido.
            return $this->upload->data();
        }
    }

    /**
     * Valida um arquivo XML e extrai os dados se for válido.
     *
     * @param string $file_path Caminho do arquivo XML.
     * @return array Retorna os dados do XML se for válido, ou uma mensagem de erro se a validação falhar.
     */
    private function validar_arquivo_xml($file_path)
    {
        // Lê o conteúdo do arquivo XML.
        $xmlString = file_get_contents($file_path);

        // Configura a manipulação interna de erros XML.
        libxml_use_internal_errors(true);
        $dom = new DOMDocument;

        // Tenta carregar o XML usando a classe DOMDocument.
        if (!$dom->loadXML($xmlString)) {
            // Retorna mensagem de erro se a validação falhar.
            return ['erro' => 'O arquivo fornecido é inválido.'];
        }

        // Retorna os dados do XML se for válido.
        return ['dados' => simplexml_load_string($xmlString)];
    }


    /**
     * Valida os dados do destinatario (emitente) extraídos de um arquivo XML.
     *
     * @param SimpleXMLElement[] $resultDest Array contendo os elementos XML do destinatario.
     * @return array Retorna informações sobre o destinatario ou uma mensagem de erro se a validação falhar.
     */
    private function validarDadosDest($resultDest)
    {
        if (!isset($resultDest[0]->CNPJ)) {
            return ['erro' => "Xml invalido, A estrutura do xml não e compativel com a leitor."];
        }

        $respostaEmitente = $this->Entradaxml_model->pegarEmitentePorCNPJ($resultDest[0]->CNPJ, 'emitente_id');

        if (!isset($respostaEmitente[0]['emitente_id'])) {
            return ['erro' => "A compra não foi feita por um cnpj da empresa."];
        }

        return $respostaEmitente;
    }

    /**
     * Valida os dados do emissor (fornecedor) extraídos de um arquivo XML.
     *
     * @param SimpleXMLElement[] $resultEmit Array contendo os elementos XML do emissor.
     * @return array Retorna informações sobre o emissor ou uma mensagem de erro se a validação falhar.
     */
    private function validarDadosEmit($resultEmit)
    {
        $dadosFornecedor = [
            'xml_cnpj' => '00000000000',
            'xml_nome' => 'indefinido',
            'fornecedor_nome' => 'indefinido',
        ];

        // Verifica se o XML contém dados de emitente.
        if (!isset($resultEmit[0])) {
            return ['erro' => 'O XML informado não é válido.', 'dados' => $dadosFornecedor];
        }

        // Extrai informações do emitente.
        $emitDados = $resultEmit[0];
        $CNPJ = (string) $emitDados->CNPJ;
        $xNome = (string) $emitDados->xNome;

        // Atualiza as informações do fornecedor.
        $dadosFornecedor['cnpj'] = !empty($CNPJ) ? $CNPJ : '00000000000';
        $dadosFornecedor['nome'] = !empty($xNome) ? $xNome : 'indefinido';
        //  == '00000000000'  '01204589625'
        // Verifica se o CNPJ do fornecedor consta no XML.
        if ($dadosFornecedor['cnpj'] == '00000000000') {
            return ['erro' => 'CNPJ do fornecedor não consta no XML', 'dados' => $dadosFornecedor];
        }

        // Valida o CNPJ do fornecedor no sistema.
        $respostaValidacaoCNPJ = $this->Entradaxml_model->pegarFornecedorPorCNPJ($dadosFornecedor['cnpj'], 'fornecedor_id,fornecedor_nome');

        // Verifica se o CNPJ do fornecedor consta no sistema.
        if (!isset($respostaValidacaoCNPJ[0]['fornecedor_id'])) {
            return ['erro' => 'CNPJ do fornecedor não consta no sistema.', 'dados' => $dadosFornecedor];
        }

        // Atualiza as informações do fornecedor com o ID do sistema.
        $dadosFornecedor['fornecedor_id'] = $respostaValidacaoCNPJ[0]['fornecedor_id'];
        $dadosFornecedor['fornecedor_nome'] = $respostaValidacaoCNPJ[0]['fornecedor_nome'];

        // Retorna as informações do fornecedor.
        return ['dados' => $dadosFornecedor];
    }

    /**
     * Valida os produtos contidos em um array de dados e retorna as informações ou erro.
     *
     * @param array $resultProd Array contendo dados dos produtos.
     * @return array Contendo informações dos produtos ou mensagem de erro.
     */
    private function validarProdutos($resultProd)
    {

        $produtos = [];
        $quantidadeDadosInvalidos = 0;

        // Itera sobre os produtos e valida as informações
        foreach ($resultProd as $value) {
            // Extrai informações do produto.
            $xProd = (string) isset($value->xProd) ? $value->xProd : '';
            $arrayXProd = explode(" | ", $xProd);
            $imeiValor = !empty(end($arrayXProd)) ? end($arrayXProd) : 0;
            $nomeProduto = (string) $xProd;
            $produtoDescricao = (string) (isset($arrayXProd[0]) ? $arrayXProd[0] : 'indefinido');
            $totalProduto = (string) (isset($value->vProd) ? $value->vProd : '0');
            $produtoCodigo = (string) (isset($value->cProd) ? $value->cProd : '0');
            $valorUnitario = (string) (floatval(isset($value->vUnCom) ? $value->vUnCom : '0'));
            $quantidade = (string) (isset($value->qCom) ? $value->qCom : '0');

            // Pegar o produto_id relacionado ao produto do xml.
            $resposta = $this->Entradaxml_model->validacaoItenDaVenda($produtoCodigo, 'produto_id,produto_categoria_id');
            $produtoId = isset($resposta[0]['produto_id']) ? $resposta[0]['produto_id'] : 0;
            $produtoCategoriaId = isset($resposta[0]['produto_categoria_id']) ? $resposta[0]['produto_categoria_id'] : 0;

            // Adiciona informações do produto ao array $produtos
            $dados = [
                'nomeProduto' => $nomeProduto,
                'nomeDescricao' => $produtoDescricao,
                'totalProduto' => $totalProduto,
                'produtoCodigo' => $produtoCodigo,
                'valorUnitario' => $valorUnitario,
                'quantidade' => $quantidade,
                'imeiValor' => $imeiValor,
                'existe' => true,
                'existeNome' => 'Encontrado',
                'existeTitulo' => 'Produto foi encontado no sistema',
                'produtoId' => $produtoId,
                'produtoCategoriaId' => $produtoCategoriaId,
            ];

            if ($dados['quantidade'] == 0) {
                $quantidadeDadosInvalidos += 1;

                $dados['existe'] = false;
                $dados['existeNome'] = 'Quantidade';
                $dados['existeTitulo'] = 'A quantidade de produtos não pode ser 0';
            } else if (count($resposta) == 0) {
                $quantidadeDadosInvalidos += 1;

                $dados['existe'] = false;
                $dados['existeNome'] = 'Não Encontrado';
                $dados['existeTitulo'] = 'Produto não encontrado';
            } else if (count($resposta) > 1) {
                $quantidadeDadosInvalidos += 1;

                $dados['existe'] = false;
                $dados['existeNome'] = 'Cod. Duplicado';
                $dados['existeTitulo'] = 'Existe dois produtos com o mesmo cod. no sistema:' . $produtoCodigo;
            }

            $produtos[] = $dados;
        }


        // Retorna informações dos produtos e quantidade de dados inválidos
        return ['quantidadeErros' => $quantidadeDadosInvalidos, 'dados' => $produtos];
    }

    /**
     * Adiciona produtos ao banco de dados.
     *
     * @param array $produtos Array contendo informações sobre os produtos.
     * @param int $id ID da entrada associada aos produtos.
     * @return array Retorna um array com os IDs dos produtos adicionados ou um array vazio em caso de erro.
     */
    private function adicionarProdutosNoBanco($produtos, $entradaXmlId, $dalorValor = '0.00')
    {

        $this->db->trans_begin();

        $usuario_id = $this->session->userdata('usuario_id');

        $idProdutosAdicionadosNobanco = [];

        try {
            // Itera sobre os produtos.
            foreach ($produtos as $produto) {

                // Prepara dados para inserção no banco.
                $data = [
                    'item_movimentacao_produto_quantidade' => $produto['quantidade'],
                    'item_movimentacao_produto_preco_unitario' => $produto['valorUnitario'],
                    'item_movimentacao_produto_custo' => $produto['valorUnitario'],
                    'item_movimentacao_produto_minimo' => '0.00',
                    'item_movimentacao_produto_dolar' => $dalorValor,
                    'item_movimentacao_produto_movimentacao_produto_id' => $entradaXmlId,  // Substituído por movimentacao_produto_id
                    'item_movimentacao_produto_produto_id' => $produto['produtoId'],
                    'item_movimentacao_produto_visivel' => 1,
                ];

                // Adiciona o item ao banco de dados.
                $resposta = $this->Entradaxml_model->add('itens_movimentacao_produtos', $data, true);

                if (!$resposta) {
                    throw new Exception('erro ao adicionar produto');
                }
                if (!empty($produto['imeiValor'])) {

                    $dadosImei = [
                        "categoria_prod_id" => $produto['produtoCategoriaId'],
                        "movimentacao_id" => $entradaXmlId,
                        "produtos_id" => $produto['produtoId'],
                        "usuario_id" => $usuario_id,
                        "imei_valor" => $produto['imeiValor'],
                        "imei_visivel" => 1,
                    ];

                    $respostaListarImei = $this->Entradaxml_model->listarImeiValor($produto['imeiValor'], 'imei_valor');
                    if (isset($respostaListarImei[0]['imei_valor'])) {
                        throw new Exception('O IMEI já foi cadastrado: ' . $respostaListarImei[0]['imei_valor']);
                    }

                    $respostaImei = $this->Entradaxml_model->add('itens_de_imei', $dadosImei, true);

                    if (!$respostaImei) {
                        throw new Exception('Falha ao adicionar o IMEI: ' . $respostaListarImei[0]['imei_valor']);
                    }
                }

                $idProdutosAdicionadosNobanco[] = $resposta;
            }
            $this->db->trans_commit();

            return $idProdutosAdicionadosNobanco;
        } catch (Exception $e) {
            $this->db->trans_rollback();

            return ['error'=> $e->getMessage()];
        }
    }

    /**
     * Adiciona produtos ao banco de dados.
     *
     * @param array $produtos Array contendo informações sobre os produtos.
     * @param int $id ID da entrada associada aos produtos.
     * @return array Retorna um array com os IDs dos produtos adicionados ou um array vazio em caso de erro.
     */
    private function adicionarEntradaNoBanco($dados)
    {

        // Prepara dados para inserção no banco.
        $data = [
            'movimentacao_produto_documento' => $dados['descricao'],
            'movimentacao_produto_chave_xml' => $dados['infNfeId'],
            'movimentacao_produto_usuario_id' => $dados['usuario_id'],
            'movimentacao_fornecedor_id' => $dados['fornecedor_id'],
            'movimentacao_produto_tipo' => $dados['tipo'],
            'movimentacao_tipo_moeda' => $dados['tipoMoeda'],
            'movimentacao_produto_saida_tipo' => $dados['saidaTipo'],
            // 'movimentacao_produto_saida_filial'  => 'byte',
            'movimentacao_produto_porcentagem' => $dados['porcentagem'],
            'movimentacao_produto_data_cadastro' => $dados['dataEnvio'],
            'movimentacao_cotacao_moeda' => $dados['cotacaoMoeda'],
            'movimentacao_produto_visivel' => 1
        ];

        // Adiciona o item ao banco de dados.
        return $this->Entradaxml_model->add('movimentacao_produtos', $data, true);
    }

    /**
     * Adiciona um arquivo XML e suas informações associadas ao banco de dados.
     *
     * @return void
     */
    public function adicionarExe()
    {
        // Validação do formulário
        $this->form_validation->set_rules('dataEnvio', 'Data da Adição', 'trim|required');
        // $this->form_validation->set_rules('descricao', 'Descrição', 'trim|required|min_length[1]|max_length[255]');
        // $this->form_validation->set_rules('tipo_moeda', 'Tipo de Moeda', 'trim|required');
        // $this->form_validation->set_rules(' ', ' ', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            // Se a validação falhar, define uma mensagem de erro e redireciona.
            $this->session->set_flashdata('erro', validation_errors());
            return redirect('entradaxml/adicionar');
        } else {
            // Se a validação for bem-sucedida, continua com o processamento.

            // Obtém a data do envio do formulário.
            $dataEnvio = str_replace("/", "-", $this->input->post('dataEnvio'));
            // * dados fixos
            $tipoMoeda = 'real';
            $cotacaoMoeda = '0.00';
            $tipo = 'Entrada';
            $saidaTipo = 'fornecedor';
            $porcentagem = '0';
            // * dados dinamicos(adicionar os inputs no front)
            // $tipoMoeda = $this->input->post('tipo_moeda');
            // $cotacaoMoeda = $this->input->post('cotacao_moeda');
            // $tipo = $this->input->post('tipo');
            // $saidaTipo = $this->input->post('saida_tipo');
            // $porcentagem = $this->input->post('porcentagem');

            // Realiza o upload do arquivo XML.
            $arquivo = $this->do_upload('xmlFileEnvio');

            if (isset($arquivo['erro'])) {
                // Se houver erro no upload, define uma mensagem de erro e redireciona.
                $this->session->set_flashdata('erro', $arquivo['erro']);
                redirect('entradaxml/adicionar');
            }

            $full_path = $arquivo['full_path'];
            $descricao = $arquivo['file_name'];

            // Valida o arquivo XML e obtém informações validadas.
            $arquivoXmlValidado = $this->validar_arquivo_xml($full_path);

            if (isset($arquivoXmlValidado['erro'])) {
                // Se o XML for inválido, define uma mensagem de erro e redireciona.
                unlink($full_path);
                $this->session->set_flashdata('erro', $arquivoXmlValidado['erro']);
                redirect('entradaxml/adicionar');
            }

            $xml = $arquivoXmlValidado['dados'];

            // Obtém o ID do infNFe (atributo Id de NFe).
            $infNfeId = (string) (isset($xml->NFe->infNFe->attributes()->Id) ? $xml->NFe->infNFe->attributes()->Id : null);

            if (empty($infNfeId)) {
                // Se não houver ID de infNFe, o XML é inválido, define uma mensagem de erro e redireciona.
                unlink($full_path);
                $this->session->set_flashdata('erro', 'O XML fornecido é inválido');

                $dadosView['meio'] = 'entradaxml/adicionar';
                return $this->load->view('tema/layout', $dadosView);
            }


            // Verifica se o XML já foi adicionado no sistema.
            $respostaInfNFeId = $this->Entradaxml_model->pegarXmlComInfNFeId($infNfeId, 'movimentacao_produto_chave_xml');

            if (isset($respostaInfNFeId[0]['movimentacao_produto_chave_xml'])) {
                // Se o XML já existe no sistema, define uma mensagem de erro e redireciona.
                unlink($full_path);
                $this->session->set_flashdata('erro', 'O XML já foi adicionado.');

                $dadosView['meio'] = 'entradaxml/adicionar';
                return $this->load->view('tema/layout', $dadosView);
            }

            $xml->registerXPathNamespace('nfe', 'http://www.portalfiscal.inf.br/nfe');

            // Obtém dados do destinatario (tabela emitente).
            $resultDest = $xml->xpath('//nfe:infNFe/nfe:dest');

            $respostaEmitente = $this->validarDadosDest($resultDest);

            if (isset($respostaEmitente['erro'])) {
                // Se houver erro nos dados do destinatario, define uma mensagem de erro e redireciona.
                unlink($full_path);
                $this->session->set_flashdata('erro', $respostaEmitente['erro']);

                $dadosView['meio'] = 'entradaxml/adicionar';
                return $this->load->view('tema/layout', $dadosView);
            }

            // Obtém dados do emitente (tabela fornecedor).
            $resultEmit = $xml->xpath('//nfe:infNFe/nfe:emit');
            $respostaFornecedor = $this->validarDadosEmit($resultEmit);

            $dadosFornecedor = $respostaFornecedor['dados'];

            if (isset($respostaFornecedor['erro'])) {
                // Se houver erro nos dados do emitente, define uma mensagem de erro e redireciona.
                unlink($full_path);
                $this->session->set_flashdata('erro', $respostaFornecedor['erro']);

                $dadosView['meio'] = 'entradaxml/adicionar';
                return $this->load->view('tema/layout', $dadosView);
            }

            // Obtém dados dos produtos (tabela produtos)
            $resultProd = $xml->xpath('//nfe:det/nfe:prod');
            $respostaValidarProdutos = $this->validarProdutos($resultProd);


            $dadosProdutos = $respostaValidarProdutos['dados'];
            $quantidadeDadosInvalidos = $respostaValidarProdutos['quantidadeErros'];

            if ($quantidadeDadosInvalidos > 0) {
                // Se houver itens inválidos, define uma mensagem de erro e redireciona.
                unlink($full_path);
                $this->session->set_flashdata('erro', 'Itens do arquivo XML inválidos');

                $dadosView['meio'] = 'entradaxml/adicionar';
                $dadosView['produtos'] = $dadosProdutos;
                return $this->load->view('tema/layout', $dadosView);
            }

            // Adiciona informações ao banco de dados.
            $dadosFornecedor['infNfeId'] = $infNfeId;
            $dadosFornecedor['tipo'] = $tipo;
            $dadosFornecedor['usuario_id'] = $this->session->userdata('usuario_id');
            $dadosFornecedor['dataEnvio'] = $dataEnvio;
            $dadosFornecedor['descricao'] = $descricao;
            $dadosFornecedor['cotacaoMoeda'] = $cotacaoMoeda;
            $dadosFornecedor['tipoMoeda'] = $tipoMoeda;
            $dadosFornecedor['saidaTipo'] = $saidaTipo;
            $dadosFornecedor['porcentagem'] = $porcentagem;

            $xmlAdicaoId = $this->adicionarEntradaNoBanco($dadosFornecedor);

            if (empty($xmlAdicaoId)) {
                // Se houver erro ao adicionar no banco de dados, define uma mensagem de erro e redireciona.
                unlink($full_path);
                $this->session->set_flashdata('erro', 'Itens do arquivo XML inválidos');

                $dadosView['meio'] = 'entradaxml/adicionar';
                $dadosView['produtos'] = $dadosProdutos;
                return $this->load->view('tema/layout', $dadosView);
            }

            // Adiciona os produtos ao banco de dados e obtém os IDs.
            $respostaArray = $this->adicionarProdutosNoBanco($dadosProdutos, $xmlAdicaoId, $cotacaoMoeda);

            if (!isset($respostaArray['error'])) {
                // Se tudo estiver correto, define uma mensagem de sucesso e redireciona.
                unlink($full_path);
                $this->session->set_flashdata('success', 'XML inserido com sucesso, verificar informações');
                redirect('entradaxml/editar/' . $xmlAdicaoId);
            } else {
                // Se houver erro ao adicionar produtos, reverte as operações e define uma mensagem de erro.
                unlink($full_path);
                $this->Entradaxml_model->delete('movimentacao_produtos', 'movimentacao_produto_id', $xmlAdicaoId);
                $this->session->set_flashdata('erro', $respostaArray['error']);
                redirect('entradaxml/adicionar');
            }
        }
    }

    public function editar()
    {
        $this->form_validation->set_rules('movimentacao_produto_documento', 'Documento', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('erro', validation_errors());
        } else {

            $dados = array(

                'movimentacao_produto_documento' => $this->input->post('movimentacao_produto_documento'),
                'movimentacao_produto_tipo' => $this->input->post('movimentacao_produto_tipo'),
                'movimentacao_produto_usuario_id' => $this->session->userdata('usuario_id'),
                'movimentacao_produto_data_cadastro' => date('Y-m-d')

            );

            $resultado = $this->Entradaxml_model->editar($dados, $this->input->post('id'));

            if ($resultado) {
                $this->session->set_flashdata('success', 'Registro editado com sucesso!');
            } else {
                $this->session->set_flashdata('erro', 'Erro ao editar o registro!');
            }
        }

        $consultaTipo = $this->Entradaxml_model->consultaTipo($this->uri->segment(3));

        // if($this->session->userdata('empresa_tipo') == '1'){
        if ($this->session->userdata('empresa_tipo') == '1' and $consultaTipo[0]->movimentacao_saida_origem_nome == null) {
            $dadosForn = $this->Fornecedores_model->listarId($consultaTipo[0]->movimentacao_fornecedor_id);
            $dadosView['categoria_prod'] = $this->Entradaxml_model->pegarCategoriaProd($dadosForn[0]->fornecedor_grupoprod_id);
        } else {
            $dadosView['categoria_prod'] = $this->Entradaxml_model->pegarCategoriaProd($ids = false);
        }
        $dadosView['dados'] = $this->Entradaxml_model->listarId($this->uri->segment(3));
        $dadosView['produtos'] = $this->Entradaxml_model->getProdutosList($this->uri->segment(3));
        $dadosView['meio'] = 'entradaxml/editar';

        $this->load->view('tema/layout', $dadosView);
    }

    public function addProdutoAjax()
    {

        $dados = array(
            "produto_categoria_id" => $this->input->post('produto_categoria_id'),
            "produto_descricao" => $this->input->post('produto_descricao'),
            "produto_codigo" => $this->input->post('produto_codigo'),
            "produto_estoque" => 0
        );

        $result = $this->Entradaxml_model->addProduto($dados);

        echo json_encode($result);
    }

    /**
     * Processa um produto da entrada XML, atualizando informações no banco de dados.
     *
     * @param object $produto - Objeto contendo informações do produto da entrada XML.
     * @throws Exception Caso ocorra algum erro durante o processamento.
     * @return bool Retorna true se o processamento for bem-sucedido.
     */
    private function processarProduto($produto)
    {

        // Lista informações sobre o produto do banco de dados
        $respostaProdutos = $this->Entradaxml_model->listarIdTabelaVisivel('produto', 'produto_id', $produto->idProduto, 'produto_visivel');

        // Verifica se o produto foi encontrado
        if (empty($respostaProdutos)) {
            throw new Exception('Não foi encontrado o produto: ' . $produto->descricaoProduto);
        }

        // Atualiza o produto na entrada XML
        $respostaProduto = $this->Entradaxml_model->updateProdutoEntrada($produto);

        // Lança uma exceção se a atualização do produto falhar
        if (!$respostaProduto) {
            throw new Exception('Falha ao adicionar produto: ' . $produto->descricaoProduto);
        }

        // Retorna true para indicar que o processamento foi bem-sucedido
        return true;
    }

    /**
     * Processa a entrada de produtos a partir de uma entrada XML.
     *
     * @param int $id - ID da entrada XML a ser processada.
     */
    public function entradaProdutos($id)
    {
        // Inicia uma transação no banco de dados
        $this->db->trans_begin();

        try {
            // Obtém a lista de produtos agrupados
            $produtos = $this->Entradaxml_model->getProdutosList_Agrupado($id);

            // Divide as listas de IDs e códigos IMEI
            foreach ($produtos as $value) {
                $value->itemMovimentacaoProdutosId = explode(",", $value->itemMovimentacaoProdutosId);
            }

            // Verifica se a lista de produtos está vazia
            if (empty($produtos)) {
                throw new Exception('Não foi encontrada a entrada XML com o ID: ' . $id);
            }

            // Processa cada produto na lista
            foreach ($produtos as $produto) {
                $this->processarProduto($produto);
            }

            // Atualiza o status da entrada xml no banco de dados
            $resposta = $this->Entradaxml_model->updateStatusEntradaXml($id);

            // Verifica se a atualização foi bem-sucedida
            if ($resposta) {
                // Confirma a transação se tudo ocorreu bem
                $this->db->trans_commit();

                $this->session->set_flashdata('success', 'Item adicionado com sucesso.');
                redirect('entradaxml/index');
            } else {
                // Lança uma exceção se houver falha na atualização do status da movimentação
                throw new Exception('Falha ao mudar o status da entrada para finalizado');
            }
        } catch (Exception $e) {
            // Reverte a transação em caso de exceção
            $this->db->trans_rollback();

            // Lida com a exceção e redireciona para a página de entrada XML
            $this->session->set_flashdata('erro', $e->getMessage());
            redirect("entradaxml/index");
        }
    }

    public function visualizar()
    {
        // $consultaTipo = $this->Entradaxml_model->consultaTipo($this->uri->segment(3));
        // $dadosView['categoria_prod'] = $this->Entradaxml_model->pegarCategoriaProd();
        $dadosView['dados'] = $this->Entradaxml_model->listarId($this->uri->segment(3));
        $dadosView['produtos'] = $this->Entradaxml_model->getProdutosList($this->uri->segment(3));
        $dadosView['meio'] = 'entradaxml/visualizar';

        $this->load->view('tema/layout', $dadosView);
    }

    public function imprimirEntradaSaida()
    {

        $this->load->model('Sistema_model');

        $consultaTipo = $this->Entradaxml_model->consultaTipo($this->uri->segment(3));

        $dadosView['emitente'] = $this->Sistema_model->pegarEmitente();
        // $dadosView['categoria_prod'] = $this->Entradaxml_model->pegarCategoriaProd();
        $dadosView['dados'] = $this->Entradaxml_model->listarId($this->uri->segment(3), $consultaTipo[0]->movimentacao_produto_saida_tipo);
        $dadosView['produtos'] = $this->Entradaxml_model->getProdutosList($this->uri->segment(3));
        $dadosView['meio'] = 'movimentacao/imprimirEntradaSaida';

        $html = $this->load->view('relatorios/impressao', $dadosView, true);

        gerarPDF($html);
    }

    /**
     * Exclui informações relacionadas a uma entrada XML e seus itens.
     *
     * @param int $id - ID da entrada XML para apagar as informações.
     */
    public function excluir($id)
    {

        // Executa a exclusão da entrada XML e de seus itens no banco de dados
        $resultado = $this->Entradaxml_model->delete('movimentacao_produtos', 'movimentacao_produto_id', $id);

        // Define uma mensagem de sucesso para exibir na próxima requisição
        if ($resultado) {
            $this->session->set_flashdata('success', 'Registros excluídos com sucesso!');
        } else {
            // Define uma mensagem de erro para exibir na próxima requisição
            $this->session->set_flashdata('erro', 'Falha ao excluir a entrada XML');
        }

        // Redireciona para a página de entrada XML após a exclusão
        redirect('entradaxml', 'refresh');
    }

    /**
     * Atualiza o IMEI de um produto na tabela de itens de IMEI.
     *
     * @param int $idNovoProduto - ID do novo produto.
     * @param string $novoImei - Novo IMEI a ser atribuído ao produto.
     * @param string $imeiInicial - IMEI inicial do produto.
     */
    private function atualizarImei($idNovoProduto, $novoImei, $imeiInicial)
    {
        // Verificar se o produto com o ImeiInicial ja foi vendido
        $produtoImeiJaVendido = $this->Entradaxml_model->pegarImeiProdutoVendido($imeiInicial);
        // Se ele encontar o imei segnifica que ele ja foi vendido
        if ($produtoImeiJaVendido) {
            throw new Exception('O produto ja foi vendido');
        }

        // Verifica se o novo IMEI é diferente do inicial
        if ($novoImei != $imeiInicial) {
            // Verifica se o novo IMEI tem 15 caracteres
            if (strlen($novoImei) != 15) {
                throw new Exception('O campo Imei deve conter exatamente 15 caracteres');
            }

            // Verifica se o novo IMEI já está cadastrado
            $imeiJaCadastrado = $this->Entradaxml_model->listarImeiValor($novoImei);
            if ($imeiJaCadastrado) {
                throw new Exception('Este número de IMEI já foi cadastrado.');
            }
        }

        // Atualiza o IMEI na tabela de itens de IMEI
        $dadosUpdateImei = ['produtos_id' => $idNovoProduto, 'imei_valor' => $novoImei];
        $respostaEdicaoImei = $this->Entradaxml_model->editarTabela('itens_de_imei', 'imei_valor', $imeiInicial, $dadosUpdateImei);
        if (!$respostaEdicaoImei) {
            throw new Exception('Ocorreu um erro ao fazer a alteração do IMEI.');
        }
    }

    /**
     * Edita um item de movimentação de produtos.
     *
     * @param int $idNovoProduto - ID do novo produto.
     * @param int $itemMovimentacaoProdutoId - ID do item de movimentação a ser editado.
     * @param int $idProdutoInicial - ID do produto inicial do item de movimentação.
     * @param int $quantidade - Quantidade do produto.
     */
    private function editarItemMovimentacao($idNovoProduto, $itemMovimentacaoProdutoId, $idProdutoInicial, $quantidade)
    {
        // Edita o item de movimentação
        $dadosEditacaoItemMovimentacao = ['item_movimentacao_produto_produto_id' => $idNovoProduto];
        $whereEditacaoItemMovimentacao = [
            'item_movimentacao_produto_movimentacao_produto_id' => $itemMovimentacaoProdutoId,
            'item_movimentacao_produto_produto_id' => $idProdutoInicial,
            'item_movimentacao_produto_quantidade' => $quantidade
        ];
        $respotaEdicaoItemMovimentacao = $this->Entradaxml_model->editarItemMovimentacao($dadosEditacaoItemMovimentacao, $whereEditacaoItemMovimentacao, 1);
        if (!$respotaEdicaoItemMovimentacao) {
            throw new Exception('Ocorreu um erro ao fazer a alteração do item.');
        }
    }

    public function atualizarMovimentacaoDeProdutosRotaEditar()
    {
        $this->form_validation->set_rules('imeiNovo', 'Imei', 'trim|required|exact_length[15]');

        if ($this->form_validation->run() == FALSE) {
            outputJSON(["message" => $this->form_validation->error_array()], 401);
        } else {
            // Obtém os dados do POST
            $itemMovimentacaoProdutoId = $this->input->post('itemMovimentacaoProdutoId');
            $idProdutoInicial = $this->input->post('produtoIdInicial');
            $idNovoProduto = $this->input->post('produtoIdNovo');
            $novoImei = $this->input->post('imeiNovo');
            $imeiInicial = $this->input->post('imeiInicial');
            $quantidade = $this->input->post('quantidadeItemMovimentacaoProduto');

            try {
                // Inicia uma transação no banco de dados
                $this->db->trans_begin();

                // Atualiza o IMEI
                $this->atualizarImei($idNovoProduto, $novoImei, $imeiInicial);

                // Edita o item de movimentação
                $this->editarItemMovimentacao($idNovoProduto, $itemMovimentacaoProdutoId, $idProdutoInicial, $quantidade);

                // Comete a transação
                $this->db->trans_commit();

                // Retorna uma mensagem de sucesso
                outputJSON(["message" => "Edição realizada com sucesso"], 201);
            } catch (Exception $e) {
                // Em caso de exceção, reverte a transação e retorna a mensagem de erro
                $this->db->trans_rollback();
                outputJSON(["message" => $e->getMessage()], 401);
            }
        }
    }


    public function atualizarMovimentacaoDeProdutosRotaVisializar()
    {
        $this->form_validation->set_rules('imeiNovo', 'Imei', 'trim|required|exact_length[15]');

        if ($this->form_validation->run() == FALSE) {
            outputJSON(["message" => $this->form_validation->error_array()], 401);
        } else {
            // Obtém os dados do POST
            $itemMovimentacaoProdutoId = $this->input->post('itemMovimentacaoProdutoId');
            $idProdutoInicial = $this->input->post('produtoIdInicial');
            $idNovoProduto = $this->input->post('produtoIdNovo');
            $novoImei = $this->input->post('imeiNovo');
            $imeiInicial = $this->input->post('imeiInicial');
            $quantidade = $this->input->post('quantidadeItemMovimentacaoProduto');

            try {
                // Inicia uma transação no banco de dados
                $this->db->trans_begin();

                // Atualiza o IMEI
                $this->atualizarImei($idNovoProduto, $novoImei, $imeiInicial);

                // Edita o item de movimentação
                $this->editarItemMovimentacao($idNovoProduto, $itemMovimentacaoProdutoId, $idProdutoInicial, $quantidade);

                // Atualiza o estoque do produto inicial
                $estoqueProdutoInicial = $this->Entradaxml_model->pegarProdutosID($idProdutoInicial, 'produto_estoque');
                if (!isset($estoqueProdutoInicial[0]->produto_estoque)) {
                    throw new Exception('Falha ao atualizar a quantidade do produto antigo.');
                }

                $resultadoDecremento = $this->Entradaxml_model->updateProdutoSaidaEstoque($idProdutoInicial, $estoqueProdutoInicial[0]->produto_estoque, $quantidade);
                if (!$resultadoDecremento) {
                    throw new Exception('Falha ao atualizar a quantidade do produto antigo.');
                }

                // Atualiza o estoque do novo produto
                $estoqueNovoProduto = $this->Entradaxml_model->pegarProdutosID($idNovoProduto, 'produto_estoque');
                if (!isset($estoqueNovoProduto[0]->produto_estoque)) {
                    throw new Exception('Falha ao atualizar a quantidade do novo produto.');
                }

                $resultadoIncremento = $this->Entradaxml_model->updateProdutoEntradaEstoque($idNovoProduto, $estoqueNovoProduto[0]->produto_estoque, $quantidade);
                if (!$resultadoIncremento) {
                    throw new Exception('Falha ao atualizar a quantidade do novo produto.');
                }

                // Comete a transação
                $this->db->trans_commit();

                // Retorna uma mensagem de sucesso
                outputJSON(["message" => "Edição realizada com sucesso"], 201);
            } catch (Exception $e) {
                // Em caso de exceção, reverte a transação e retorna a mensagem de erro
                $this->db->trans_rollback();
                outputJSON(["message" => $e->getMessage()], 401);
            }
        }
    }

    public function autocompleteProduto()
    {
        $value = strtolower($this->input->get('lorem'));
        $dadosAutocompleteProduto = $this->Entradaxml_model->autocompleteProduto($value);
        outputJSON($dadosAutocompleteProduto, 201);
    }
}
