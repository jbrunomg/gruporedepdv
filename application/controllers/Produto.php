<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Produto extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Produto_model');
	}

	public function index()
	{
		$dadosView['meio'] = 'produto/listar';
		$this->load->view('tema/layout', $dadosView);
	}

	public function adicionar()
	{
		// $cad = $this->input->post('produto_categoria_id');
		// $this->form_validation->set_rules('produto_descricao', 'Descrição', 'required|edit_unique_produto[produto.produto_descricao.' . $id . '.' . $cad . ']');


		$this->form_validation->set_rules('produto_descricao', 'Descrição', 'trim|required|is_unique[produto.produto_descricao]');

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('erro', validation_errors());
		} else {

			// echo'<pre>'; var_dump($_POST); exit();

			if ($_FILES['userfile']['name'] != '') {
				$arquivo = $this->do_upload();
				$file = $arquivo['file_name'];
			} else {
				$file = 'produto_sem_imagem.jpg';
			}

			// $lojas = $this->input->post('lojas');

			$dados = array(

				'produto_categoria_id'    => $this->input->post('produto_categoria_id'),
				'produto_codigo'          => $this->input->post('codigo_produto'),
				'produto_descricao'       => $this->input->post('produto_descricao'),
				'produto_unidade'         => $this->input->post('produto_unidade'),
				'produto_codigo_barra'    => $this->input->post('produto_codigo_barra'),
				'produto_estoque_minimo'  => $this->input->post('produto_estoque_minimo'),
				'produto_preco_dolar'     => $this->input->post('produto_preco_dolar'),
				'produto_preco_custo'     => $this->input->post('produto_preco_custo'),
				'produto_preco_minimo_venda'  => $this->input->post('produto_preco_minimo_venda'),
				'produto_preco_venda'         => $this->input->post('produto_preco_venda'),
				'produto_preco_cart_debito'   => $this->input->post('produto_preco_cart_debito'),
				'produto_preco_cart_credito'  => $this->input->post('produto_preco_cart_credito'),
				'produto_imagem'          => $file,
				'produto_data_cadastro'   => date('Y-m-d'),
				'produto_visivel'         => 1
			);

			$dadosGaveta = array(

				'produto_gaveta'          => $this->input->post('produto_gaveta'),
				'produto_gaveta_cor'      => $this->input->post('produto_gaveta_cor')

			);

			



			$resultado = $this->Produto_model->adicionar($dados, $dadosGaveta);

			if ($resultado) {
				$this->session->set_flashdata('success', 'Registro adicionado com sucesso!');
			} else {
				$this->session->set_flashdata('erro', 'Erro ao adicionado o registro!');
			}
		}

		
		$dadosView['grupo'] = $this->Produto_model->pegarGrupoProduto();
		
		
	

		$dadosView['lojas'] = $this->Produto_model->getAllFilias();
		$dadosView['parametros'] = $this->Produto_model->getAllParametros();
		// $dadosView['impostos'] = $this->Produto_model->listarImpostos();

		$dadosView['meio'] = 'produto/adicionar';
		$this->load->view('tema/layout', $dadosView);
	}

	public function preencherSelectCodigos()
	{
		$codigos = $this->Produto_model->preencherSelectCodigos($this->input->post('grupo'));
    	echo json_encode($codigos);
	}

	public function editar()
	{
		$id = $this->uri->segment(3);
		$cad = $this->input->post('produto_categoria_id');
		$this->form_validation->set_rules('produto_descricao', 'Descrição', 'trim|required|edit_unique_produto[produto.produto_descricao.' . $id . '.' . $cad . ']');
		
		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('erro', validation_errors());
		} else {
			
			if ($_FILES['userfile']['name'] != '') {
				$arquivo = $this->do_upload();
				$file = $arquivo['file_name'];
			} else {
				$file = $this->input->post('imagemProduto');
			}
			
			// die(var_dump($this->input->post()));
			$dados = array(

				'produto_categoria_id'    => $this->input->post('produto_categoria_id'),
				'produto_codigo'          => $this->input->post('codigo_produto'),
				'produto_descricao'       => $this->input->post('produto_descricao'),
				'produto_unidade'         => $this->input->post('produto_unidade'),
				'produto_codigo_barra'    => $this->input->post('produto_codigo_barra'),
				'produto_estoque_minimo'  => $this->input->post('produto_estoque_minimo'),
				'produto_preco_dolar'     => $this->input->post('produto_preco_dolar'),

				'produto_preco_minimo_venda'  => $this->input->post('produto_preco_minimo_venda'),
				'produto_preco_venda'         => $this->input->post('produto_preco_venda'),
				'produto_preco_cart_debito'   => $this->input->post('produto_preco_cart_debito'),
				'produto_preco_cart_credito'  => $this->input->post('produto_preco_cart_credito'),
				'produto_imagem' => $file,

				'produto_imposto_id' 	=> $this->input->post('produto_imposto_id'),
				'produto_peso' 			=> $this->input->post('produto_peso'),
				'produto_ncm' 			=> $this->input->post('produto_ncm'),
				'produto_cest' 			=> $this->input->post('produto_cest'),
				'produto_origem' 		=> $this->input->post('produto_origem'),
				//  'produto_data_cadastro'   => date('Y-m-d'),   
				//  'produto_visivel'         => 1						  
			);
			// die(var_dump($dados));	
			$dadosGaveta = array(
				'produto_preco_custo'     => $this->input->post('produto_preco_custo'),
				'produto_gaveta'          => $this->input->post('produto_gaveta'),
				'produto_gaveta_cor'      => $this->input->post('produto_gaveta_cor')

			);

			$resultado = $this->Produto_model->editar($dados, $dadosGaveta, $this->input->post('id'));

			if ($resultado) {
				$this->session->set_flashdata('success', 'Registro editado com sucesso!');
			} else {
				$this->session->set_flashdata('erro', 'Erro ao editar o registro!');
			}
		}


		$dadosView['grupo'] = $this->Produto_model->pegarGrupoProduto();

		$dadosView['codigos'] = $this->Produto_model->preencherSelectCodigosListar();

		$dadosView['dados']   = $this->Produto_model->listarId($this->uri->segment(3));
		$dadosView['margem'] = $this->Produto_model->selecionarCategoria($dadosView['dados'][0]->produto_categoria_id);
		$dadosView['lojas'] = $this->Produto_model->getAllFilias();
		$dadosView['parametros'] = $this->Produto_model->getAllParametros();
		$dadosView['impostos'] = $this->Produto_model->listarImpostos();

		$dadosView['meio']    = 'produto/editar';
		$this->load->view('tema/layout', $dadosView);
	}

	public function visualizar()
	{
		$this->load->model('Sistema_model');

		$dadosView['grupo']         = $this->Produto_model->pegarGrupoProduto();
		$dadosView['dados']         = $this->Produto_model->listarId($this->uri->segment(3));
		$dadosView['graficoAno']    = $this->Produto_model->graficoAno($this->uri->segment(3));
		$dadosView['movimentacao']  = $this->Produto_model->movimentacao($this->uri->segment(3));

		foreach ($dadosView['graficoAno'] as $key => $value) {

			if ($value->atual == null) {
				$value->atual = '0';
			}

			if ($value->passado == null) {
				$value->passado = '0';
			}
		}

		$dadosView['graficoMes']  = $this->Produto_model->graficoMes($this->uri->segment(3));

		foreach ($dadosView['graficoMes'] as $key => $value) {

			if ($value->atual == null) {
				$value->atual = '0';
			}

			if ($value->passado == null) {
				$value->passado = '0';
			}
		}

		// echo "<pre>";
		//  	var_dump($dadosView['graficoMes']);die();

		$dadosView['meio']    = 'produto/visualizar';
		$this->load->view('tema/layout', $dadosView);
	}

	public function excluir()
	{
		$id = $this->uri->segment(3);

		$dados  = array(
			'produto_visivel' => 0
		);
		$resultado = $this->Produto_model->excluir($dados, $id);

		if ($resultado) {
			$this->session->set_flashdata('success', 'registro excluidos com sucesso!');
		} else {
			$this->session->set_flashdata('erro', 'Erro ao excluir o registro!');
		}

		redirect('Produto', 'refresh');
	}


	public function do_upload()
	{

		$config['upload_path']   = realpath('./assets/produtos/');
		$config['allowed_types'] = 'txt|jpg|jpeg|gif|png|pdf|PDF|JPG|JPEG|GIF|PNG';
		$config['max_size']      = 0;
		// $config['max_width']  = '3000';
		// $config['max_height'] = '2000';
		$config['encrypt_name']  = true;


		if (!is_dir('./assets/produtos/')) {
			mkdir('./assets/produtos/', 0777, TRUE);
		}

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if (!$this->upload->do_upload()) {
			$error = array('error' => $this->upload->display_errors());
			// var_dump($error);die();
			$this->session->set_flashdata('erro', 'Erro ao fazer upload do arquivo, verifique se a extensão do arquivo é permitida.');
			redirect(base_url() . 'index.php/produto/adicionar/');
		} else {
			//$data = array('upload_data' => $this->upload->data());
			return $this->upload->data();
		}
	}

	public function selecionarCategoria()
	{

		$grupo   = $this->input->post('grupo');
		$grupoId = $this->Produto_model->selecionarCategoria($grupo);
		foreach ($grupoId as $g) {
			echo  "<input type='hidden' class='form-control' name='margem_categoria' id='margem_categoria' value='" . $g->categoria_prod_margem . "'>  ";
		}
	}

	public function alterarEstoque()
	{
		$dados = array(
			'estoque_ajuste_categoria_id'   => $this->input->post('categoria'),
			'estoque_ajuste_observacao'     => 'Rotina de Ajuste Estoque - ' . $this->input->post('descricao') . ' - ' . date('Y-m-d'),
			'estoque_usuarios_id'           => $this->session->userdata('usuario_id'),
			'estoque_ajuste_data'           => date('Y-m-d'),
			'estoque_ajuste_visivel'        => 1
		);

		$quantidade = $this->input->post('quantidade');
		$produto = $this->input->post('id');
		$estoque = $this->input->post('estoque');

		if ($this->Produto_model->adicionarEstoqueRotina($dados, $quantidade, $produto, $estoque)) {
			echo json_encode(array('result' => true));
		} else {
			echo json_encode(array('result' => false));
		}
	}

	public function listarDados(){

        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $search = $this->input->post('search') ?? ''; // Verifica se 'value' existe

        // Parâmetros de ordenação
        $order_column_index = $this->input->post('order')[0]['column'] ?? 0; // Índice da coluna a ser ordenada (padrão: 0)
        $order_column_name = $this->input->post('columns')[$order_column_index]['data'] ?? ''; // Nome da coluna a ser ordenada
        $order_dir = $this->input->post('order')[0]['dir'] ?? 'asc'; // Direção da ordenação (padrão: asc)

		$dados = $this->Produto_model->listar($limit, $start, $search, $order_column_name, $order_dir);
        $formatted_data = [];
		$icon = '';

        foreach ($dados['dados'] as $d) { 

			if (verificarPermissao('vAlmoxarifado')) { 
				$icon .= '<a href="'.base_url().$this->uri->segment(1).'/visualizar/'.$d->produto_id.'" data-popup="tooltip" title="Visualizar"><i class="fa fa-search text-success"></i></a>';
			} 
			if (verificarPermissao('eAlmoxarifado')) { 
				$icon .= '<a href="'.base_url().$this->uri->segment(1).'/editar/'.$d->produto_id.'" data-toggle="tooltip" title="Editar"><i class="fa fa-edit"></i></a>';
			} 
			if (verificarPermissao('dAlmoxarifado')) { 
				$icon .= '<a style="cursor:pointer;" title="Deletar" data-toggle="modal" data-target="#excluir" onclick="excluir(\''.base_url().$this->uri->segment(1).'/excluir/'.$d->produto_id.'\');" ><i class="fa fa-trash text-danger"></i></a>';
			}
			if (verificarPermissao('eAlmoxarifado')) { 
				$icon .= '<a style="cursor:pointer;" data-toggle="modal" data-target="#modalSenhaEstoque" onclick="idProdutoEstoqueModal('.$d->produto_id.', '.$d->produto_categoria_id.', \''.$d->produto_descricao.'\', '.$d->produto_estoque.');"><i class="fa fa-truck text-warning"></i></a>';
			}

            $formatted_data[] = array(
                'produto_codigo' =>  $d->produto_codigo,$d->produto_categoria_id,
                'produto_codigo_barra' => $d->produto_codigo_barra,
                'produto_descricao' => $d->produto_descricao,
                'produto_estoque' => $d->produto_estoque,
                'icon' => $icon
            );

			$icon = '';
        }

        $output = [
            "draw" => intval($this->input->post('draw')),
            "recordsTotal" => $dados['total_registros'],
            "recordsFiltered" => $dados['total_registros'],
            "data" => $formatted_data
        ];
        
        echo json_encode($output);
 	}



}
