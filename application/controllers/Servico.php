<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Servico extends CI_Controller {

    public function __construct()
    {   
        parent::__construct();
        $this->load->model('Servico_model');
    }

    public function index()
    {
        $dadosView['dados'] = $this->Servico_model->listar();

        $dadosView['meio'] = 'servico/listar';
        $this->load->view('tema/layout',$dadosView);    
    }

    public function adicionar()
    {
        $this->form_validation->set_rules('servico_nome', 'Nome Serviço', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('erro',validation_errors());
        } else {

            $dados = array(              
                  'servico_nome'      => $this->input->post('servico_nome'),
                  'servico_codigo'    => $this->input->post('servico_codigo'),
                  'servico_valor'     => $this->input->post('servico_valor'),
                  'servico_descricao' => $this->input->post('servico_descricao'),
                  'servico_data_cadastro' => date('Y-m-d'), 
                  'servico_visivel'       => 1                  
            );

            $resultado = $this->Servico_model->adicionar($dados);

            if($resultado){
                $this->session->set_flashdata('success','Registro adicionado com sucesso!');
            }else{
                $this->session->set_flashdata('erro','Erro ao adicionado o registro!');
            }
        }

        $dadosView['meio'] = 'servico/adicionar';
        $this->load->view('tema/layout',$dadosView);
    }

    public function editar()
    {
        $this->form_validation->set_rules('servico_nome', 'Nome Serviço', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('erro',validation_errors());
        } else {

            $dados = array(              
                    'servico_nome'      => $this->input->post('servico_nome'),
                    'servico_codigo'    => $this->input->post('servico_codigo'),
                    'servico_valor'     => $this->input->post('servico_valor'),
                    'servico_descricao' => $this->input->post('servico_descricao')
                                 
            );
     
            $resultado = $this->Servico_model->editar($dados,$this->input->post('id'));

            if($resultado){
                $this->session->set_flashdata('success','Registro editado com sucesso!');
            }else{
                $this->session->set_flashdata('erro','Erro ao editar o registro!');
            }
        }
      
        $dadosView['dados']   = $this->Servico_model->listarId($this->uri->segment(3));
        $dadosView['meio']    = 'servico/editar';

        $this->load->view('tema/layout',$dadosView);

    }

    public function visualizar()
    {
                
        $dadosView['dados']   = $this->Servico_model->listarId($this->uri->segment(3));
        $dadosView['meio']    = 'servico/visualizar';
        $this->load->view('tema/layout',$dadosView);
    }

    public function excluir()
    {
        $id = $this->uri->segment(3);

        $dados  = array(
                        'servico_visivel' => 0                        
                      );
        $resultado = $this->Servico_model->excluir($dados,$id);

        if($resultado){
            $this->session->set_flashdata('success','registro excluidos com sucesso!');
        }else{
            $this->session->set_flashdata('erro','Erro ao excluir o registro!');
        }

        redirect('Servico','refresh');
    }
}
