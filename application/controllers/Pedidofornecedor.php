<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pedidofornecedor extends CI_Controller {

  public function __construct()
	{   
		parent::__construct();
		$this->load->model('Pedidofornecedor_model');		
	}


  public function index()
	{

        $data['dados']  = $this->Pedidofornecedor_model->listar();
        $data['meio']   = 'pedidofornecedor/listar';
		    $this->load->view('tema/layout',$data);

	}
  
  public function adicionar()
	{

	   $this->form_validation->set_rules('observacao', 'Observação', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
		{
        	$this->session->set_flashdata('erro',validation_errors());
        } else {

        	$data = $this->input->post('dataPedido');
        	$data = str_replace("/", "-", $data);

           	
        
        	$dados = array(  
        		    'dataPedido'          => date('Y-m-d',  strtotime($data)),             
    				'fornecedor_id'       => $this->input->post('fornecedor_id'),							    
    				'usuarios_id'         => $this->session->userdata('usuario_id'),
    				'observacao'          => $this->input->post('observacao')	  
        	);

             if (is_numeric($id = $this->Pedidofornecedor_model->adicionar($dados)) ) {
                $this->session->set_flashdata('success','Pedido iniciado com sucesso, adicione os produtos.');

                redirect('Pedidofornecedor/editar/'.$id);
      
        	}else{
        		$this->session->set_flashdata('erro','Erro ao adicionado o registro!');
        	}
        } 

        $idFornecedor = $this->uri->segment(3);;

        $dadosView['fornecedor'] = $this->Pedidofornecedor_model->pegarFornecedor($idFornecedor);
        // var_dump($dadosView['fornecedor']);die();

		$dadosView['meio'] = 'pedidofornecedor/adicionar';
		$this->load->view('tema/layout',$dadosView);
	}

  public function editar()
	{

        $dadosView['categoria_prod'] = $this->Pedidofornecedor_model->pegarCategoriaProd();
        $dadosView['dados']   = $this->Pedidofornecedor_model->listarId($this->uri->segment(3));
        $dadosView['produtos'] = $this->Pedidofornecedor_model->getProdutos($this->uri->segment(3));
	    $dadosView['meio']    = 'pedidofornecedor/editar';

		$this->load->view('tema/layout',$dadosView);

	}

  public function editarExe(){

    $id = $this->input->post('id');
    $this->form_validation->set_rules('observacao', 'Observação', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('erro',validation_errors());
            $this->editar($id);
        } else {

            $data = $this->input->post('dataPedido');
            $data = str_replace("/", "-", $data);

            if ($this->input->post('nota') == NULL ) {
                $nota = 0 ;
            } else {
                $nota = 1 ;
            }
            
        
            $dados = array(  
                'dataPedido'          => date('Y-m-d',  strtotime($data)),             
                'clientes_id'         => $this->input->post('cliente_id'),                              
                'usuarios_id'         => $this->session->userdata('usuario_id'),
                'observacao'          => $this->input->post('observacao'),                  
                'emitirNota'          => $nota        
            );
     
            $resultado = $this->Pedidofornecedor_model->editar($dados,$this->input->post('id'));

            if($resultado){
                $this->session->set_flashdata('success','Registro editado com sucesso!');
            }else{
                $this->session->set_flashdata('erro','Erro ao editar o registro!');
            }
        }

        redirect('pedido/editar/'.$id);

   }


  public function adicionarProduto(){ 

    //die('AQUI');

        $id = $this->input->post('idPedidos');

        $this->form_validation->set_rules('quantidade', 'Quantidade', 'trim|required');
        // $this->form_validation->set_rules('produto', 'Produto', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
        {
            // $this->session->set_flashdata('erro',validation_errors());
            echo json_encode(array('result'=> false));

        }
        else{            


            $quantidade = $this->input->post('quantidade');
           // $valor      = $this->input->post('subTotal');
            $produto    = $this->input->post('produto_id');

            // $calc = floatval($valor) * floatval($quantidade);
            // $subTotal = number_format($calc,2,".",".");
            // $subTotal = $calc;

            
            $data = array(
                // 'valorPago'          => $valor,
                'quantidade'            => $quantidade,
                //'subTotal '           => $subTotal,
                'pedidos_fornecedor_id' => $this->input->post('idPedidos'),
                'produtos_id'           => $produto
            );

            if($this->Pedidofornecedor_model->add('itens_de_pedidos_fornecedor', $data) == true){ 

                // $this->Pedidofornecedor_model->addTotal($this->input->post('idPedidos'), $subTotal);                                            

                echo json_encode(array('result'=> true));
            }else{
                echo json_encode(array('result'=> false));
            }   

            
        }  

    }


 public function selecionarProduto(){
        
        $grupo   = $this->input->post('grupo');
        $produto = $this->Pedidofornecedor_model->pegarProdutos($grupo);

        echo  "<option value=''>Selecionar um produto</option>";
        foreach ($produto as $p) {
            echo "<option value='".$p->produto_id."'>".$p->produto_descricao."</option>";
        }

    }


  public function selecionarProdutoID(){
        
        $produto   = $this->input->post('produto');
        $produtoId = $this->Pedidofornecedor_model->pegarProdutosID($produto);
        
        foreach ($produtoId as $p) {
            echo  "<input type='text'  class='form-control' name='subTotal' id='subTotal' value='".$p->produto_preco_venda."'>
                   <p><center><font color='red'> Preço Min ".$p->produto_preco_minimo_venda."</font></center> </p>";

        }

    }

     public function VendaPedido(){ 

     	$id  = $this->input->post('idPedidos');
     	$cliente  = $this->input->post('cliente');
     	$valorTotal  =   $this->input->post('total');
     	$usuario     = $this->session->userdata('usuario_id');

	     $data = array(
	        
	        'valorTotal'     =>  $this->input->post('total'),
	        'finalizado'     =>  1,
	     );

       $validacao = $this->Pedidofornecedor_model->validacao($id); 

        if ($validacao) {

                if($this->Pedidofornecedor_model->Pedido($data,$id) == true){                              

                  $this->Pedidofornecedor_model->itensVenda($id, $valorTotal, $cliente, $usuario);  

                   echo json_encode(array('result'=> true));
                }else{
                   echo json_encode(array('result'=> false));
                }   

        }else{

          echo json_encode(array('result'=> false));

        }

     }

   public function excluirProduto(){
            $idPedido = $this->uri->segment(3);
            $ID = $this->uri->segment(4);
            // $produto = $this->uri->segment(5);
            // $quantidade = $this->uri->segment(6);
            // $subTotal = $this->uri->segment(7);
            
            if($this->Pedidofornecedor_model->delete('itens_de_pedidos_fornecedor','idItens',$ID) == true){     

               // $this->Pedidofornecedor_model->retirarValor($subTotal, $idPedido);
                redirect('pedidofornecedor/editar/'.$idPedido);

            }
            else{  

                $this->session->set_flashdata('erro','Erro ao editar o registro!');
                redirect('pedidofornecedor/editar/'.$idPedido);
            }        

    }  

   public function visualizar()
    {

       $dadosView['categoria_prod'] = $this->Pedidofornecedor_model->pegarCategoriaProd();
       $dadosView['dados']   = $this->Pedidofornecedor_model->listarId($this->uri->segment(3));
       $dadosView['produtos'] = $this->Pedidofornecedor_model->getProdutos($this->uri->segment(3));
       $dadosView['meio']    = 'pedidofornecedor/visualizar';

       $this->load->view('tema/layout',$dadosView);

   }

  public function excluir()
    {
  
       $id = $this->uri->segment(3);
       $idFornecedor = $this->uri->segment(4);
       $Pedidofornecedor_visivel = 0 ;               

       $resultado = $this->Pedidofornecedor_model->excluir($Pedidofornecedor_visivel,$id);   

        if($resultado){
            $this->session->set_flashdata('success','Dados excluidos com sucesso!');
        }else{
            $this->session->set_flashdata('erro','Erro ao excluir os dados!');
        }

        redirect('fornecedores/visualizar/'.$idFornecedor,'refresh');       

    }


}