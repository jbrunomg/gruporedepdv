<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fornecedores extends CI_Controller {

	public function __construct()
	{   
		parent::__construct();
		$this->load->model('Fornecedores_model');
	}

	public function index()
	{
		$dadosView['dados'] = $this->Fornecedores_model->listar();

		$dadosView['meio'] = 'fornecedores/listar';
		$this->load->view('tema/layout',$dadosView);	
	}

	public function adicionar()
	{
	    $this->form_validation->set_rules('fornecedor_cnpj_cpf', 'CPF/CNPJ', 'trim|required|is_unique[fornecedores.fornecedor_cnpj_cpf]');
	    $this->form_validation->set_rules('fornecedor_grupoprod_id[]', 'GRUPO PRODUTO', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
		{
        	$this->session->set_flashdata('erro',validation_errors());
        } else {      	
        	

        	// if ($this->input->post('fornecedor_estado')) {

        	// 	$idEstado = $this->Fornecedores_model->pegarEstadoId($this->input->post('fornecedor_estado'));
        	// 	$estado = $idEstado[0]->id_estado;        		

        	// 	$idCidade = $this->Fornecedores_model->pegarCidadeId($idEstado[0]->id_estado, $this->input->post('fornecedor_cidade'));
        	// 	$cidade = $idCidade[0]->id_cidade;
        		     		
        	// }else{

        	// 	$estado = 28;
        	// 	$cidade = 20045;

        	// }


        	$estado = $this->input->post('fornecedor_estado');
    		$cidade =  $this->input->post('fornecedor_cidade');

        	$dados = array(        		 
				  'fornecedor_cnpj_cpf'            => $this->input->post('fornecedor_cnpj_cpf'),
				  'fornecedor_tipo'                => $this->input->post('fornecedor_tipo'),				  
				  'fornecedor_nome'                => $this->input->post('fornecedor_nome'),
				  'fornecedor_endereco'            => $this->input->post('fornecedor_endereco'),
				  'fornecedor_numero' 			   => $this->input->post('fornecedor_numero'),
				  'fornecedor_complemento' 		   => $this->input->post('fornecedor_complemento'),
				  'fornecedor_bairro' 			   => $this->input->post('fornecedor_bairro'),				  
				
				  'fornecedor_estado' 			   => $estado, // $this->input->post('fornecedor_estado'),
				  'fornecedor_cidade' 			   => $cidade, // $this->input->post('fornecedor_cidade'),

				  'fornecedor_cep' 				   => $this->input->post('fornecedor_cep'),
				  'fornecedor_telefone' 		   => $this->input->post('fornecedor_telefone'),
				  'fornecedor_celular' 			   => $this->input->post('fornecedor_celular'),
				  'fornecedor_data_cadastro' 	   => date('Y-m-d'),
				  'fornecedor_email' 			   => $this->input->post('fornecedor_email'),
				  'fornecedor_agencia' 		       => $this->input->post('fornecedor_agencia'),
				  'fornecedor_conta' 		       => $this->input->post('fornecedor_conta'),
				  'fornecedor_banco' 		       => $this->input->post('fornecedor_banco'),
				  'fornecedor_titular' 		       => $this->input->post('fornecedor_titular'),
				  'fornecedor_grupoprod_id'        => implode(',',$this->input->post('fornecedor_grupoprod_id')),				  
				  'fornecedor_visivel' 	           => 1,
				  'fornecedor_data_atualizacao'    => date('Y-m-d H:i:s')
        	);

// var_dump($dados);die();

        	$resultado = $this->Fornecedores_model->adicionar($dados);

        	if($resultado){
        		$this->session->set_flashdata('success','Registro adicionado com sucesso!');
        	}else{
        		$this->session->set_flashdata('erro','Erro ao adicionado o registro!');
        	}
        }

		$dadosView['estados']          = $this->Fornecedores_model->pegarEstados();
		$dadosView['bancos']           = $this->Fornecedores_model->pegarBancos();
		$dadosView['grupo']            = $this->Fornecedores_model->pegarGrupoProduto();

		$dadosView['meio'] = 'fornecedores/adicionar';
		$this->load->view('tema/layout',$dadosView);
	}

	public function editar()
	{
		$this->form_validation->set_rules('fornecedor_cnpj_cpf', 'CPF/CNPJ', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
		{
        	$this->session->set_flashdata('erro',validation_errors());
        } else {

        	// $idEstado = $this->Fornecedores_model->pegarEstadoId($this->input->post('fornecedor_estado'));
        	// $idCidade = $this->Fornecedores_model->pegarCidadeId($idEstado[0]->id_estado, $this->input->post('fornecedor_cidade'));

        	// if (empty($idCidade)) {
        	// 	$cidade = $this->input->post('fornecedor_cidade');                	
        	// } else {
        	// 	$cidade = $idCidade[0]->id_cidade;
        	// }

  

        	$estado = $this->input->post('fornecedor_estado');
    		$cidade =  $this->input->post('fornecedor_cidade');
        		



        	// var_dump($cidade);die();

        	$dados = array(        		 
				  'fornecedor_cnpj_cpf'            => $this->input->post('fornecedor_cnpj_cpf'),
				  'fornecedor_tipo'                => $this->input->post('fornecedor_tipo'),				  
				  'fornecedor_nome'                => $this->input->post('fornecedor_nome'),
				  'fornecedor_endereco'            => $this->input->post('fornecedor_endereco'),
				  'fornecedor_numero' 			   => $this->input->post('fornecedor_numero'),
				  'fornecedor_complemento' 		   => $this->input->post('fornecedor_complemento'),
				  'fornecedor_bairro' 			   => $this->input->post('fornecedor_bairro'),

				  // 'fornecedor_estado' 			   => $idEstado[0]->id_estado, 
				  // 'fornecedor_cidade' 			   => $cidade, 

				  'fornecedor_estado' 			   => $estado, // $this->input->post('fornecedor_estado'),
				  'fornecedor_cidade' 			   => $cidade, // $this->input->post('fornecedor_cidade'),

				  'fornecedor_cep' 				   => $this->input->post('fornecedor_cep'),
				  'fornecedor_telefone' 		   => $this->input->post('fornecedor_telefone'),
				  'fornecedor_celular' 			   => $this->input->post('fornecedor_celular'),
				  'fornecedor_data_cadastro' 	   => date('Y-m-d'),
				  'fornecedor_email' 			   => $this->input->post('fornecedor_email'),
				  'fornecedor_agencia' 		       => $this->input->post('fornecedor_agencia'),
				  'fornecedor_conta' 		       => $this->input->post('fornecedor_conta'),
				  'fornecedor_banco' 		       => $this->input->post('fornecedor_banco'),
				  'fornecedor_titular' 		       => $this->input->post('fornecedor_titular'),
				  'fornecedor_grupoprod_id'        => implode(',',$this->input->post('fornecedor_grupoprod_id')),	
				  'fornecedor_visivel' 	           => 1,
				  'fornecedor_data_atualizacao'    => date('Y-m-d H:i:s')
        	);
     
        	$resultado = $this->Fornecedores_model->editar($dados,$this->input->post('id'));

        	if($resultado){
        		$this->session->set_flashdata('success','Registro editado com sucesso!');
        	}else{
        		$this->session->set_flashdata('erro','Erro ao editado o registro!');
        	}
        }

        $this->load->model('Sistema_model');

        $dadosView['dados']       = $this->Fornecedores_model->listarId($this->uri->segment(3));        
		$dadosView['grupoforn']   = explode(',',$dadosView['dados'][0]->fornecedor_grupoprod_id);
		$dadosView['grupo']       = $this->Fornecedores_model->pegarGrupoProduto();
        $dadosView['estados']     = $this->Fornecedores_model->pegarEstados();
		$dadosView['bancos']      = $this->Fornecedores_model->pegarBancos();
				
		$dadosView['cidades']	 = $this->Sistema_model->selecionarCidades($dadosView['dados'][0]->fornecedor_estado);
		$dadosView['meio']       = 'fornecedores/editar';

		$this->load->view('tema/layout',$dadosView);

	}

	public function visualizar()
	{
		$this->load->model('Sistema_model');
		
		$dadosView['dados']       = $this->Fornecedores_model->visualizar($this->uri->segment(3));
		$dadosView['pedidos']     = $this->Fornecedores_model->listarPedidos($this->uri->segment(3));
		$dadosView['grupoforn']   = explode(',',$dadosView['dados'][0]->fornecedor_grupoprod_id);
		$dadosView['grupo']       = $this->Fornecedores_model->pegarGrupoProduto();		
        $dadosView['estados']     = $this->Fornecedores_model->pegarEstados();
		$dadosView['bancos']      = $this->Fornecedores_model->pegarBancos();				
		$dadosView['cidades']     = $this->Sistema_model->selecionarCidades($dadosView['dados'][0]->fornecedor_estado);

		$dadosView['meio']  = 'fornecedores/visualizar';
		$this->load->view('tema/layout',$dadosView);
	}

	public function excluir()
	{
		$id = $this->uri->segment(3);

		$dados  = array(
				'fornecedor_visivel' => 0,
				'fornecedor_data_atualizacao' => date('Y-m-d H:i:s')
			  );
		$resultado = $this->Fornecedores_model->excluir($dados,$id);

		if($resultado){
			$this->session->set_flashdata('success','registro excluidos com sucesso!');
		}else{
			$this->session->set_flashdata('erro','Erro ao excluir o registro!');
		}

		redirect('Fornecedores','refresh');
	}

	public function autoCompleteFornecedor()
	{		

        $termo = strtolower($this->input->get('term'));
        $this->Fornecedores_model->autoCompleteFornecedor($termo);

	}
}
