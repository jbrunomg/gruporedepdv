<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes extends CI_Controller {

		public function __construct()
	{   
		parent::__construct();
		$this->load->model('Clientes_model');
	}

	public function index()
	{
		$dadosView['meio'] = 'clientes/listar';
		$this->load->view('tema/layout',$dadosView);	
	}

	public function adicionar()
	{

		$validacaoCPF = $this->Clientes_model->validacaoCPF($this->input->post('cliente_cpf_cnpj'));

		if($validacaoCPF != NULL){
          
          redirect('clientes/editar/'.$validacaoCPF[0]->cliente_id); 
		}

	    $this->form_validation->set_rules('cliente_cpf_cnpj', 'CPF', 'trim|required|is_unique[clientes.cliente_cpf_cnpj]');
	    $this->form_validation->set_rules('cliente_celular', 'Celular', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
		{
        	$this->session->set_flashdata('erro',validation_errors());
        } else {
       	
        	
        	$dados = array(        		 				 
				  'cliente_cpf_cnpj'            => $this->input->post('cliente_cpf_cnpj'),
				  'cliente_tipo'                => $this->input->post('cliente_tipo'),
				  'cliente_nome'                => $this->input->post('cliente_nome'),
				  'cliente_data_nasc'           => $this->input->post('cliente_data_nasc'),
				  'cliente_endereco'            => $this->input->post('cliente_endereco'),
				  'cliente_numero' 				=> $this->input->post('cliente_numero'),
				  'cliente_complemento' 		=> $this->input->post('cliente_complemento'),
				  'cliente_bairro' 				=> $this->input->post('cliente_bairro'),				  
				  'cliente_estado' 				=> $this->input->post('cliente_estado'),
				  'cliente_cidade' 				=> $this->input->post('cliente_cidade'),
				  'cliente_cep' 				=> $this->input->post('cliente_cep'),
				  'cliente_telefone' 			=> $this->input->post('cliente_telefone'),
				  'cliente_celular' 			=> $this->input->post('cliente_celular'),
				  'cliente_data_cadastro' 		=> date('Y-m-d'),
				  'cliente_email' 				=> $this->input->post('cliente_email'),	
				  'cliente_face' 				=> $this->input->post('cliente_face'),
				  'cliente_insta' 				=> $this->input->post('cliente_insta'),
				  'cliente_ativo' 				=> $this->input->post('cliente_ativo'),
				  'cliente_app' 				=> $this->input->post('cliente_app'),
				  'cliente_categoria'           => $this->input->post('cliente_categoria'),
				  'cliente_limite_cred_cliente'  	=> $this->input->post('cliente_limite_cred_cliente'),
				  'cliente_limite_dias_cliente'  	=> (int)$this->input->post('cliente_limite_dias_cliente'),
				  'cliente_rg_ie'					=> $this->input->post('cliente_rg_ie'),			  				  
        	);
        
			// var_dump($dados);die();

        	$resultado = $this->Clientes_model->adicionar($dados);

        	if($resultado){
        		$this->session->set_flashdata('success','Registro adicionado com sucesso!');
        	}else{
        		$this->session->set_flashdata('erro','Erro ao adicionado o registro!');
        	}
        }

		$dadosView['estados']    = $this->Clientes_model->pegarEstados();
		$dadosView['categoria']  = $this->Clientes_model->pegarCategoria();
		$dadosView['lojas']      = $this->Clientes_model->getAllFilias();


		$dadosView['meio'] = 'clientes/adicionar';
		$this->load->view('tema/layout',$dadosView);
	}

	public function editar()
	{
		$this->form_validation->set_rules('cliente_cpf_cnpj', 'CPF', 'trim|required');
		$this->form_validation->set_rules('cliente_celular', 'Celular', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
		{
        	$this->session->set_flashdata('erro',validation_errors());
        } else {


        	$dados = array(        		 				 
				  'cliente_cpf_cnpj'            => $this->input->post('cliente_cpf_cnpj'),
				  'cliente_tipo'                => $this->input->post('cliente_tipo'),
				  'cliente_nome'                => $this->input->post('cliente_nome'),
				  'cliente_data_nasc'           => $this->input->post('cliente_data_nasc'),
				  'cliente_endereco'            => $this->input->post('cliente_endereco'),
				  'cliente_numero' 				=> $this->input->post('cliente_numero'),
				  'cliente_complemento' 		=> $this->input->post('cliente_complemento'),
				  'cliente_bairro' 				=> $this->input->post('cliente_bairro'),	

				  'cliente_estado' 				=> $this->input->post('cliente_estado'),
				  'cliente_cidade' 				=> $this->input->post('cliente_cidade'),
				  
				  'cliente_cep' 				 => $this->input->post('cliente_cep'),
				  'cliente_telefone' 			 => $this->input->post('cliente_telefone'),
				  'cliente_celular' 			 => $this->input->post('cliente_celular'),
				  'cliente_email' 				 => $this->input->post('cliente_email'),
				  'cliente_face' 				 => $this->input->post('cliente_face'),
				  'cliente_insta' 				 => $this->input->post('cliente_insta'),	
				  'cliente_ativo' 				 => $this->input->post('cliente_ativo'),
				  'cliente_app' 				 => $this->input->post('cliente_app'),
				  'cliente_categoria'            => $this->input->post('cliente_categoria'),
				  'cliente_limite_cred_cliente'  => $this->input->post('cliente_limite_cred_cliente'),
				  'cliente_limite_dias_cliente'  => $this->input->post('cliente_limite_dias_cliente'),
				  'cliente_rg_ie'					=> $this->input->post('cliente_rg_ie'),		
				  'cliente_visivel'              	=> '1'			  				 				  
        	);

        	// var_dump($dados);die();
        
        	$resultado = $this->Clientes_model->editar($dados,$this->input->post('id'));

        	if($resultado){
        		$this->session->set_flashdata('success','Registro editado com sucesso!');
        	}else{
        		$this->session->set_flashdata('erro','Erro ao editado o registro!');
        	}
        }

        $this->load->model('Sistema_model');

        $dadosView['dados']   = $this->Clientes_model->listarId($this->uri->segment(3));
		// $dadosView['perfil']   = $this->Clientes_model->pegarPerfil();
		$dadosView['estados']    = $this->Clientes_model->pegarEstados();
		$dadosView['categoria']  = $this->Clientes_model->pegarCategoria();
		$dadosView['lojas']   = $this->Clientes_model->getAllFilias();
		$dadosView['cidades'] = $this->Sistema_model->selecionarCidades($dadosView['dados'][0]->cliente_estado);
		$dadosView['meio']    = 'clientes/editar';

		$this->load->view('tema/layout',$dadosView);

	}

	public function visualizar()
	{
		$this->load->model('Sistema_model');
		
		$dadosView['dados']   = $this->Clientes_model->listarId($this->uri->segment(3));
		$dadosView['vendas']  = $this->Clientes_model->listarVendasId($this->uri->segment(3));
		$dadosView['nivel']   = $this->Clientes_model->pegarPerfil();
		$dadosView['estados'] = $this->Clientes_model->pegarEstados();
		$dadosView['cidades'] = $this->Sistema_model->selecionarCidades($dadosView['dados'][0]->cliente_estado);

		$dadosView['meio']  = 'clientes/visualizar';
		$this->load->view('tema/layout',$dadosView);
	}

	public function excluir()
	{
		$id = $this->uri->segment(3);

		$dados  = array(
						'cliente_visivel' => 0					
					  );
		$resultado = $this->Clientes_model->excluir($dados,$id);

		if($resultado){
			$this->session->set_flashdata('success','registro excluidos com sucesso!');
		}else{
			$this->session->set_flashdata('erro','Erro ao excluir o registro!');
		}

		redirect('Clientes','refresh');
	}

	public function autoCompleteClientes()
	{
		$termo = strtolower($this->input->get('term'));
        $this->Clientes_model->autoCompleteClientes($termo);
	}

	public function wscnpj()
    {
        $caracteres = array(".","/","-");
        $cpfCnpj  = $this->input->post('documento');        
        $cpfCnpj = str_replace($caracteres, "", $cpfCnpj);       
        $json = file_get_contents('https://receitaws.com.br/v1/cnpj/'.$cpfCnpj);
        
        echo $json;
    }

    public function listarDados(){

        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $search = $this->input->post('search') ?? ''; // Verifica se 'value' existe

        // Parâmetros de ordenação
        $order_column_index = $this->input->post('order')[0]['column'] ?? 0; // Índice da coluna a ser ordenada (padrão: 0)
        $order_column_name = $this->input->post('columns')[$order_column_index]['data'] ?? ''; // Nome da coluna a ser ordenada
        $order_dir = $this->input->post('order')[0]['dir'] ?? 'asc'; // Direção da ordenação (padrão: asc)

		$dados = $this->Clientes_model->listar($limit, $start, $search, $order_column_name, $order_dir);
        $formatted_data = [];
		$icon = '';

        foreach ($dados['dados'] as $d) { 

			if (verificarPermissao('vCliente')) { 
				$icon .= '<a href="'.base_url().$this->uri->segment(1).'/visualizar/'.$d->cliente_id.'" data-popup="tooltip" title="Visualizar"><i class="fa fa-search text-success"></i></a>';
			} 
			if (verificarPermissao('eCliente') && $d->cliente_padrao == 0) { 
				$icon .= '<a href="'.base_url().$this->uri->segment(1).'/editar/'.$d->cliente_id.'" data-toggle="tooltip" title="Editar"><i class="fa fa-edit"></i></a>';
			} 
			if (verificarPermissao('dCliente') && $d->cliente_padrao == 0) { 
				$icon .= '<a style="cursor:pointer;" title="Deletar" data-toggle="modal" data-target="#excluir" onclick="excluir(\''.base_url().$this->uri->segment(1).'/excluir/'.$d->cliente_id.'\');" ><i class="fa fa-trash text-danger"></i></a>';
			}

            $formatted_data[] = array(
                'cliente_nome' =>  $d->cliente_nome,
                'cliente_celular' => $d->cliente_celular,
                'cliente_cpf_cnpj' => $d->cliente_cpf_cnpj,
                'cliente_email' => $d->cliente_email,
                'icon' => $icon
            );

			$icon = '';
        }

        $output = [
            "draw" => intval($this->input->post('draw')),
            "recordsTotal" => $dados['total_registros'],
            "recordsFiltered" => $dados['total_registros'],
            "data" => $formatted_data
        ];
        
        echo json_encode($output);
  
 }



}
