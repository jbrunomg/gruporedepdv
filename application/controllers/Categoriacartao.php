<?php

class Categoriacartao extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Categoriacartao_model');
    }

    public function index()
    {
        $dadosView['dados'] = $this->Categoriacartao_model->listar();

        $dadosView['meio'] = 'categoriacartao/listar';
        $this->load->view('tema/layout', $dadosView);
    }


    public function adicionar()
    {
        $this->form_validation->set_rules('categoria_cart_cnpj_credenciadora', 'CNPJ Cartão', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('erro', validation_errors());
        } else {

            /*
            01 - Visa / Visa Electron
            02 - Mastercard / Maestro
            03 - American Express
            04 - Sorocred

            05 - Diners Club
            06 - Elo
            07 - Hipercard
            08 - Aura
            09 - Cabal
            99 - Outros  */

            $bandeira = array(
                'Visa / Mastercard',
                'Elo / Hiper / Outros'

            );

            foreach ($bandeira as $b) {

                for ($i = 0; $i < 19; $i++) {
                    # Inserir 18 Registro para cada bandeira

                    $dados = array(
                        'categoria_cart_cnpj_credenciadora'   => $this->input->post('categoria_cart_cnpj_credenciadora'),
                        'categoria_cart_nome_credenciadora'   => $this->input->post('categoria_cart_nome_credenciadora'),
                        'categoria_cart_descricao'      => $b,
                        'categoria_cart_numero_parcela' => $i,
                        'categoria_cart_data_cadastro'  => date('Y-m-d'),
                        'categoria_cart_visivel'        => 1
                    );

                    $result = $this->Categoriacartao_model->adicionar($dados);
                }
            };

            if ($result == TRUE) {
                $this->session->set_flashdata('success', 'Registro adicionado com sucesso!');
                redirect(base_url() . 'index.php/categoriacartao/adicionar/');
            } else {
                $this->session->set_flashdata('erro', 'Erro ao adicionado o registro!');
                redirect(base_url() . 'index.php/categoriacartao/adicionar/');
            }
        }

        $dadosView['meio'] = 'categoriacartao/adicionar';
        $this->load->view('tema/layout', $dadosView);
    }


    public function excluir()
    {

        $data = array(
            'categoria_cart_visivel' => 0
        );
        $id = $this->uri->segment(3) . '/' . $this->uri->segment(4);

        // var_dump($id);die();

        $resultado = $this->Categoriacartao_model->excluir('categoria_cart_cnpj_credenciadora', $data, $id);

        if ($resultado) {
            $this->session->set_flashdata('success', 'Categoria excluida com sucesso!');
            redirect(base_url() . 'index.php/categoriacartao/');
        } else {
            $this->session->set_flashdata('erro', 'Erro ao deletar o registro!');
            redirect(base_url() . 'index.php/categoriacartao/');
        }
        //redirect('Categoriacartao', 'refresh');
    }

    public function editar()
    {
        if ($_SERVER["REQUEST_METHOD"] == "GET") {
            $dadosView['dados'] = $this->Categoriacartao_model->getCartaoCategoria($this->uri->segment(3));
            $dadosView['meio'] = 'categoriacartao/visualizar';
            $this->load->view('tema/layout', $dadosView);
        } else {
            //      VARIAVEIS.      //
            $id_user            = $this->session->userdata('usuario_id');
            $id_categoria_cart  = $this->input->post('categoria_cart_id');
            //     /VARIAVEIS.      //

            $this->form_validation->set_rules('categoria_cart_percentual_parcela', 'Percentual Parcela', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('erro', strip_tags(validation_errors()));
                $resposta = array(
                    'status' => FALSE
                );
                die(json_encode($resposta));
            }

            $dados = array(
                'categoria_cart_percentual_parcela' => floatval($this->input->post('categoria_cart_percentual_parcela'))
            );

            $result = $this->Categoriacartao_model->editar($dados, $id_categoria_cart);

            if ($result == TRUE) {
                $this->session->set_flashdata('success', 'Alterado com sucesso!');
                $resposta = array(
                    'status' => TRUE
                );
                die(json_encode($resposta));
            } else {
                $this->session->set_flashdata('erro', 'Erro ao alterar!');
                $resposta = array(
                    'status' => FALSE
                );
                die(json_encode($resposta));
            }
        }
    }
}
