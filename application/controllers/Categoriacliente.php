<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categoriacliente extends CI_Controller {

    public function __construct()
    {   
        parent::__construct();
        $this->load->model('Categoriacliente_model');
    }

    public function index()
    {
        $dadosView['dados'] = $this->Categoriacliente_model->listar();

        $dadosView['meio'] = 'categoriacliente/listar';
        $this->load->view('tema/layout',$dadosView);    
    }

    public function adicionar()
    {
        $this->form_validation->set_rules('categoria_clie_descricao', 'Descrição', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('erro',validation_errors());
        } else {

            $dados = array(              
                  'categoria_clie_descricao'     => $this->input->post('categoria_clie_descricao'),
                  'categoria_clie_data_cadastro' => date('Y-m-d'), 
                  'categoria_clie_visivel'       => 1                  
            );

            $resultado = $this->Categoriacliente_model->adicionar($dados);

            if($resultado){
                $this->session->set_flashdata('success','Registro adicionado com sucesso!');
            }else{
                $this->session->set_flashdata('erro','Erro ao adicionado o registro!');
            }
        }

        $dadosView['meio'] = 'categoriacliente/adicionar';
        $this->load->view('tema/layout',$dadosView);
    }

    public function editar()
    {
        $this->form_validation->set_rules('categoria_clie_descricao', 'Descrição', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('erro',validation_errors());
        } else {

            $dados = array(              
                  'categoria_clie_descricao'     => $this->input->post('categoria_clie_descricao')
                                 
            );
     
            $resultado = $this->Categoriacliente_model->editar($dados,$this->input->post('id'));

            if($resultado){
                $this->session->set_flashdata('success','Registro editado com sucesso!');
            }else{
                $this->session->set_flashdata('erro','Erro ao editar o registro!');
            }
        }
      
        $dadosView['dados']   = $this->Categoriacliente_model->listarId($this->uri->segment(3));
        $dadosView['meio']    = 'categoriacliente/editar';

        $this->load->view('tema/layout',$dadosView);

    }

    public function visualizar()
    {
                
        $dadosView['dados']   = $this->Categoriacliente_model->listarId($this->uri->segment(3));
        $dadosView['meio']    = 'categoriacliente/visualizar';
        $this->load->view('tema/layout',$dadosView);
    }

    public function excluir()
    {
        $id = $this->uri->segment(3);

        $dados  = array(
                        'categoria_clie_visivel' => 0                        
                      );
        $resultado = $this->Categoriacliente_model->excluir($dados,$id);

        if($resultado){
            $this->session->set_flashdata('success','registro excluidos com sucesso!');
        }else{
            $this->session->set_flashdata('erro','Erro ao excluir o registro!');
        }

        redirect('Categoriacliente','refresh');
    }
}
