<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Palavrachave extends CI_Controller {

    public function __construct()
    {   
        parent::__construct();
        $this->load->model('Palavrachave_model');
    }

    public function index()
    {
        $dadosView['dados'] = $this->Palavrachave_model->listar();

        $dadosView['meio'] = 'palavrachave/listar';
        $this->load->view('tema/layout',$dadosView);    
    }

    public function adicionar()
    {
        $this->form_validation->set_rules('nome_digitado', 'nome digitado', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('erro',validation_errors());
        } else {

            $dados = array(              
                  'nome_digitado'          => $this->input->post('nome_digitado'),
                  'produto_categoria_id'   => $this->input->post('produto_categoria_id'), 
                  'operador'               => $this->input->post('operador'),                   
                  'nome_sistema'           => $this->input->post('nome_sistema'), 
                  'visivel'                => 1                  
            );

            $resultado = $this->Palavrachave_model->adicionar($dados);

            if($resultado){
                $this->session->set_flashdata('success','Registro adicionado com sucesso!');
            }else{
                $this->session->set_flashdata('erro','Erro ao adicionado o registro!');
            }
        }

        $dadosView['meio'] = 'palavrachave/adicionar';
        $dadosView['grupo'] = $this->Palavrachave_model->pegarGrupoProduto();
        $this->load->view('tema/layout',$dadosView);
    }

    public function editar()
    {
        $this->form_validation->set_rules('nome_digitado', 'nome digitado', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('erro',validation_errors());
        } else {

            $dados = array(              
                  'nome_digitado'          => $this->input->post('nome_digitado'),
                  'produto_categoria_id'   => $this->input->post('produto_categoria_id'), 
                  'operador'               => $this->input->post('operador'),                   
                  'nome_sistema'           => $this->input->post('nome_sistema')                 
                                 
            );
     
            $resultado = $this->Palavrachave_model->editar($dados,$this->input->post('id'));

            if($resultado){
                $this->session->set_flashdata('success','Registro editado com sucesso!');
            }else{
                $this->session->set_flashdata('erro','Erro ao editar o registro!');
            }
        }
      
        $dadosView['dados']   = $this->Palavrachave_model->listarId($this->uri->segment(3));
        $dadosView['grupo']   = $this->Palavrachave_model->pegarGrupoProduto();       
        $dadosView['meio']    = 'palavrachave/editar';

        $this->load->view('tema/layout',$dadosView);

    }

    public function visualizar()
    {
                
        $dadosView['dados']   = $this->Palavrachave_model->listarId($this->uri->segment(3));
        $dadosView['grupo']   = $this->Palavrachave_model->pegarGrupoProduto();  
        $dadosView['meio']    = 'palavrachave/visualizar';
        $this->load->view('tema/layout',$dadosView);
    }

    public function excluir()
    {
        $id = $this->uri->segment(3);

        $dados  = array(
                        'visivel' => 0                        
                      );
        $resultado = $this->Palavrachave_model->excluir($dados,$id);

        if($resultado){
            $this->session->set_flashdata('success','registro excluidos com sucesso!');
        }else{
            $this->session->set_flashdata('erro','Erro ao excluir o registro!');
        }

        redirect('Palavrachave','refresh');
    }
}
