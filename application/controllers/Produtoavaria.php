<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produtoavaria extends CI_Controller {

    public function __construct()
    {   
        parent::__construct();
        $this->load->model('Produtoavaria_model');
    }

    public function index()
    {
        $dadosView['dados'] = $this->Produtoavaria_model->listar();

        $dadosView['meio'] = 'produtoavaria/listar';
        $this->load->view('tema/layout',$dadosView);    
    }

    public function adicionar()
    {
        $this->form_validation->set_rules('produto_avaria_observação', 'Descrição', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('erro',validation_errors());
        } else {

            $dados = array(              
                  'categoria_clie_descricao'     => $this->input->post('categoria_clie_descricao'),
                  'categoria_clie_data_cadastro' => date('Y-m-d'), 
                  'categoria_clie_visivel'       => 1                  
            );

            $resultado = $this->Produtoavaria_model->adicionar($dados);

            if($resultado){
                $this->session->set_flashdata('success','Registro adicionado com sucesso!');
            }else{
                $this->session->set_flashdata('erro','Erro ao adicionado o registro!');
            }
        }

        $dadosView['meio'] = 'produtoavaria/adicionar';
        $this->load->view('tema/layout',$dadosView);
    }

    public function editar()
    {
        $this->form_validation->set_rules('produto_avaria_observação', 'Descrição', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('erro',validation_errors());
        } else {

            $dados = array(              
                  'categoria_clie_descricao'     => $this->input->post('categoria_clie_descricao')
                                 
            );
     
            $resultado = $this->Produtoavaria_model->editar($dados,$this->input->post('id'));

            if($resultado){
                $this->session->set_flashdata('success','Registro editado com sucesso!');
            }else{
                $this->session->set_flashdata('erro','Erro ao editar o registro!');
            }
        }
      
        $dadosView['dados']   = $this->Produtoavaria_model->listarId($this->uri->segment(3));
        $dadosView['meio']    = 'produtoavaria/editar';

        $this->load->view('tema/layout',$dadosView);

    }

    public function visualizar()
    {
                
        $dadosView['dados']   = $this->Produtoavaria_model->listarId($this->uri->segment(3));
        $dadosView['meio']    = 'produtoavaria/visualizar';
        $this->load->view('tema/layout',$dadosView);
    }

    public function excluir()
    {
        $id = $this->uri->segment(3);

        $dados  = array(
                        'produto_avaria_visivel' => 0                        
                        );
        $resultado = $this->Produtoavaria_model->excluir($dados,$id);

        if($resultado){
            $this->session->set_flashdata('success','registro excluidos com sucesso!');
        }else{
            $this->session->set_flashdata('erro','Erro ao excluir o registro!');
        }

        redirect('Produtoavaria','refresh');
    }
}
