<?php 
class Compra extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('Compra_model');
	}

	public function index()
	{

		$dados['dados'] = $this->Compra_model->listarCompras();
		$dados['meio'] = 'compra/listar';
		
		$this->load->view('tema/layout', $dados);
	}

	public function adicionar()
	{

		$this->form_validation->set_rules('compra_categoria_id', 'Grupo de Produto', 'trim|required');

		//Adicionar a validação das datas
       
        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('erro',validation_errors());
        } else {
            $categoria = $this->input->post('compra_categoria_id');
            $dados = array(  
                'simulacao_compra_categoria_id'   		=> $categoria,            
                'simulacao_compra_observacao'     		=> $this->input->post('compra_observacao'), 
                'simulacao_compra_tipo_aponta'          => $this->input->post('tipo'),               
                'simulacao_compra_usuarios_id'     		=> $this->session->userdata('usuario_id'),
                'simulacao_compra_vendas_data_inicio'  	=> $this->input->post('compra_data_inicio'),
                'simulacao_compra_vendas_data_final' 	=> $this->input->post('compra_data_final'),
                'simulacao_compra_data'					=> date('Y-m-d'),                        
                'simulacao_compra_visivel'        		=> 1                  
            );        

            if (is_numeric($id = $this->Compra_model->adicionar($dados))) {

               $this->Compra_model->prepararItens($categoria,$id); 
               // $this->session->set_flashdata('success','Produto iniciada com sucesso, ajuste seu valores.');

                redirect('compra/editar/'.$id);
      
            }else{
                $this->session->set_flashdata('erro','Erro ao adicionado o registro!');
            }
        }       


		$dadosView['grupo'] = $this->Compra_model->listarCategoriaProduto(); 
        $dadosView['meio'] = 'compra/adicionar';
        $this->load->view('tema/layout',$dadosView);
	}

	public function editar($id)
    {
          
        $simulacao_compra = $this->Compra_model->consultarCompra($id);

        $dataInicio  = $simulacao_compra->simulacao_compra_vendas_data_inicio;
        $dataFim     = $simulacao_compra->simulacao_compra_vendas_data_final;
        $categoriaId = $simulacao_compra->simulacao_compra_categoria_id;
        $tipo        = ($simulacao_compra->simulacao_compra_tipo_aponta <> null) ? $simulacao_compra->simulacao_compra_tipo_aponta : 'produto_id' ; 

        $dadosView['observacao']	= $simulacao_compra->simulacao_compra_observacao;
        $dadosView['tipo']          = $simulacao_compra->simulacao_compra_tipo_aponta;
        $dadosView['categoria'] 	= $simulacao_compra->categoria_descricao;
        $dadosView['dataInicio']	= date('d/m/Y', strtotime($dataInicio));
        $dadosView['dataFim']		= date('d/m/Y', strtotime($dataFim));
        $dadosView['produtos']   	= $this->Compra_model->listarId($id, $dataInicio, $dataFim, $categoriaId, $tipo);       
        $dadosView['meio']    		= 'compra/editar';

        $this->load->view('tema/layout',$dadosView);

    }

    public function atualizaQuantidadeProdutos($id)
    {

    	$dados = array(
    		'quantidade' 					=> $this->input->post('quantProduto'),
    		'produtos_id' 					=> $this->input->post('idProduto'),
    		'simulacao_compra_usuario_id' 	=> $this->session->userdata('usuario_id'),
    	);

    	$result = $this->Compra_model->atualizaQuantidadeProdutos($dados, $id);

    	echo json_encode($result);
    }

    public function excluir($id)
    {
        $dados  = array( 'simulacao_compra_visivel' => 0 );

        $resultado = $this->Compra_model->excluir($dados,$id);

        if($resultado){
            $this->session->set_flashdata('success','registro excluidos com sucesso!');
        }else{
            $this->session->set_flashdata('erro','Erro ao excluir o registro!');
        }

        redirect('Compra','refresh');
    }

    public function prepararCompra($idCompra)
    {
    	
    	$this->Compra_model->prepararCompra($idCompra);
    	$this->Compra_model->excluirItensCompra($idCompra);

    	$this->session->set_flashdata('success','registro excluidos com sucesso!');

    	redirect('Compra');

    }

    public function visualizar($id)
    {
    	$simulacao_compra = $this->Compra_model->consultarCompra($id);

        $dataInicio  = $simulacao_compra->simulacao_compra_vendas_data_inicio;
        $dataFim     = $simulacao_compra->simulacao_compra_vendas_data_final;
        $categoriaId = $simulacao_compra->simulacao_compra_categoria_id;
        $tipo        = ($simulacao_compra->simulacao_compra_tipo_aponta <> null) ? $simulacao_compra->simulacao_compra_tipo_aponta : 'produto_descricao' ; 

        $dadosView['observacao']	= $simulacao_compra->simulacao_compra_observacao;
        $dadosView['categoria'] 	= $simulacao_compra->categoria_descricao;
        $dadosView['dataInicio']	= date('d/m/Y', strtotime($dataInicio));
        $dadosView['dataFim']		= date('d/m/Y', strtotime($dataFim));
        $dadosView['produtos']   	= $this->Compra_model->listarId($id, $dataInicio, $dataFim, $categoriaId, $tipo);       
        $dadosView['meio']    		= 'compra/visualizar';

        $this->load->view('tema/layout',$dadosView);
    }



    public function imprimirCompra($id){

        $this->load->model('Sistema_model');

        $simulacao_compra = $this->Compra_model->consultarCompra($id);

        $dataInicio  = $simulacao_compra->simulacao_compra_vendas_data_inicio;
        $dataFim     = $simulacao_compra->simulacao_compra_vendas_data_final;
        $categoriaId = $simulacao_compra->simulacao_compra_categoria_id;
        $tipo        = ($simulacao_compra->simulacao_compra_tipo_aponta <> null) ? $simulacao_compra->simulacao_compra_tipo_aponta : 'produto_descricao' ; 

        $dadosView['simulacao']     = $simulacao_compra->simulacao_compra_id;
        $dadosView['observacao']    = $simulacao_compra->simulacao_compra_observacao;
        $dadosView['categoria']     = $simulacao_compra->categoria_descricao;
        $dadosView['dataInicio']    = date('d/m/Y', strtotime($dataInicio));
        $dadosView['dataFim']       = date('d/m/Y', strtotime($dataFim));
        $dadosView['produtos']      = $this->Compra_model->listarId($id, $dataInicio, $dataFim, $categoriaId, $tipo);
        $dadosView['emitente'] = $this->Sistema_model->pegarEmitente();     
        $dadosView['meio']    = 'compra/imprimirCompra';

        $html = $this->load->view('relatorios/impressao',$dadosView, true); 
            
        gerarPDF($html);
       
    }

}