<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Codigoproduto extends CI_Controller {

    public function __construct()
    {   
        parent::__construct();
        $this->load->model('Codigoproduto_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $dadosView['dados'] = $this->Codigoproduto_model->listar();
        $dadosView['meio'] = 'codigoproduto/listar';
        $this->load->view('tema/layout',$dadosView);
    }

    public function adicionar()
    {
        $this->form_validation->set_rules('codigo_prod_codigo', 'Código do Produto', 'trim|required');
        $this->form_validation->set_rules('codigo_prod_descricao', 'Descrição', 'trim|required');
        $this->form_validation->set_rules('codigo_prod_codigo', 'Código do Produto', 'is_unique[codigo_produto.codigo_prod_codigo]');

        
        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('erro',validation_errors());
        } else {

            $dados = array(              

                  'codigo_prod_categoria_id' => $this->input->post('codigo_prod_categoria_id'), // 'codigo_prod_categoria_id' => 'categoria_prod_id'                 
                  'codigo_prod_codigo'     => $this->input->post('codigo_prod_codigo'),
                  'codigo_prod_descricao'     => $this->input->post('codigo_prod_descricao'),                  
                  'codigo_prod_data_cadastro' => date('Y-m-d'), 
                  'codigo_prod_visivel'       => 1                  
            );

            

            $resultado = $this->Codigoproduto_model->adicionar($dados);

            if($resultado){
                $this->session->set_flashdata('success','Registro adicionado com sucesso!');
            }else{
                $this->session->set_flashdata('erro','Erro ao adicionado o registro!');
            }
        }

        
        $dadosView['meio'] = 'codigoproduto/adicionar';
        $dadosView['grupo'] = $this->Codigoproduto_model->listarCategoria();
        $dadosView['lojas'] = $this->Codigoproduto_model->getAllFilias();
        $this->load->view('tema/layout',$dadosView);

    }


    public function editar()
    {
        $this->form_validation->set_rules('codigo_prod_codigo', 'Codigo do Produto', 'trim|required');
        $this->form_validation->set_rules('codigo_prod_descricao', 'Descrição', 'trim|required');
        $this->form_validation->set_rules('codigo_prod_codigo', 'Código do Produto', 'callback_check_unique_codigo');

        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('erro',validation_errors());
        } else {
            $dados = array(
                'codigo_prod_categoria_id' => $this->input->post('codigo_prod_categoria_id'),
                  'codigo_prod_codigo' => $this->input->post('codigo_prod_codigo'),
                    'codigo_prod_descricao'  => $this->input->post('codigo_prod_descricao'),
            );

        $resultado = $this->Codigoproduto_model->editar($dados,$this->input->post('id'));

            if($resultado){
                $this->session->set_flashdata('success','Registro editado com sucesso!');
            }else{
                $this->session->set_flashdata('erro','Erro ao editar o registro, ou este código já existe!');
            }

        }
        
        
        $dadosView['dados']   = $this->Codigoproduto_model->listarId($this->uri->segment(3));
        $dadosView['grupo'] = $this->Codigoproduto_model->listarCategoria();
        $dadosView['lojas'] = $this->Codigoproduto_model->getAllFilias();
        $dadosView['meio']    = 'codigoproduto/editar';
        $this->load->view('tema/layout',$dadosView);
    }


    public function visualizar()
    {
        $dadosView['dados'] = $this->Codigoproduto_model->listarId($this->uri->segment(3));
        $dadosView['grupo'] = $this->Codigoproduto_model->listarCategoria();
        $dadosView['meio'] = 'codigoproduto/visualizar';
        $this->load->view('tema/layout',$dadosView);
    }

    public function excluir()
    {
        $id = $this->uri->segment(3);
        $resultado = $this->Codigoproduto_model->excluir($id);
        
        if ($resultado) {
            $this->session->set_flashdata('success', 'Registro excluído com sucesso!');
        } else {
            $this->session->set_flashdata('erro', 'Erro ao excluir o registro!');
        }
    
        redirect($this->uri->segment(1));
    }

    public function check_unique_codigo($new_codigo) {

        $new_codigo = $this->input->post('codigo_prod_codigo'); // se ver esta mensagem, é de onde parei
    $current_codigo = $this->Codigoproduto_model->get_current_codigo($new_codigo); // pega o código atual do produto

    // Se o código não foi alterado, retorna true
    if ($new_codigo == $current_codigo) {
        return true;
    }

    // Se o código foi alterado, verifica se é único
    $this->form_validation->set_message('check_unique_codigo', 'O %s já está em uso.');

    // Verifica no banco de dados se o novo código é único
    if ($this->Codigoproduto_model->is_unique_codigo($new_codigo)) {
        return true;
    } else {
        return false;
    }
}
// até aqui o código estava inserindo corretamente na base de dados

}
