<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Imposto extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Imposto_model');
    }

    public function index()
    {
        $dados_view['meio'] = 'imposto/listar';
        $dados_view['impostos'] = $this->Imposto_model->listarImpostos();
        $this->load->view('tema/layout', $dados_view);
    }

    public function adicionar()
    {
        $dados_view['meio'] = 'imposto/adicionar';
        $this->load->view('tema/layout', $dados_view);
    }

    public function add()
    {
        // die(var_dump($this->input->post()));
        $this->form_validation->set_rules('imposto_descricao', 'Descrição', 'required|trim');
        $this->form_validation->set_rules('imposto_tipo_tributacao', 'Tipo de Tributação', 'required|trim');
        $this->form_validation->set_rules('imposto_icms_codigo_CFOP', 'Código CFOP', 'required|trim');
        $this->form_validation->set_rules('imposto_icms_situacao_tributaria', 'Situação Tributaria', 'required|trim');

        $this->form_validation->set_rules('imposto_ipi_situacao_tributaria', 'Situação Tributaria IPI', 'required|trim');
        $this->form_validation->set_rules('imposto_ipi_codigo_enquadramento', 'Código de enquadramento', 'required|trim');
        $this->form_validation->set_rules('imposto_ipi_aliquota', 'Alíquota IPI', 'required|trim');

        $this->form_validation->set_rules('imposto_pis_situacao_tributaria', 'Situação Tributaria PIS', 'required|trim');
        $this->form_validation->set_rules('imposto_pis_aliquota', 'Alíquota PIS', 'required|trim');

        $this->form_validation->set_rules('imposto_cofins_situacao_tributaria', 'Situação Tributaria COFINS', 'required|trim');
        $this->form_validation->set_rules('imposto_cofins_aliquota', 'Alíquota COFINS', 'required|trim');

        $this->form_validation->set_rules('imposto_tributos_federais', 'Tributos Federais', 'required|trim');
        $this->form_validation->set_rules('imposto_tributos_estaduais', 'Tributos Estaduais', 'required|trim');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('erro', validation_errors());
            redirect('imposto/adicionar');
        } else {

            $dados = array(
                'imposto_descricao' => $this->input->post('imposto_descricao'),
                'imposto_tipo_tributacao' => $this->input->post('imposto_tipo_tributacao'),
                'imposto_icms_codigo_CFOP' => $this->input->post('imposto_icms_codigo_CFOP'),
                'imposto_icms_situacao_tributaria' => $this->input->post('imposto_icms_situacao_tributaria'),
                'imposto_ipi_situacao_tributaria' => $this->input->post('imposto_ipi_situacao_tributaria'),
                'imposto_ipi_codigo_enquadramento' => $this->input->post('imposto_ipi_codigo_enquadramento'),
                'imposto_ipi_aliquota' => $this->input->post('imposto_ipi_aliquota'),
                'imposto_pis_situacao_tributaria' => $this->input->post('imposto_pis_situacao_tributaria'),
                'imposto_pis_aliquota' => $this->input->post('imposto_pis_aliquota'),
                'imposto_cofins_situacao_tributaria' => $this->input->post('imposto_cofins_situacao_tributaria'),
                'imposto_cofins_aliquota' => $this->input->post('imposto_cofins_aliquota'),
                'imposto_tributos_federais' => $this->input->post('imposto_tributos_federais'),
                'imposto_tributos_estaduais' => $this->input->post('imposto_tributos_estaduais'),
            );

            $this->Imposto_model->adicionar($dados);
            $this->session->set_flashdata('success', 'Imposto adicionado');
            redirect('imposto');
        }
    }

    public function editar($id)
    {
        $dados_view['imposto'] = $this->Imposto_model->editar($id);
        $dados_view['meio'] = 'imposto/editar';
        $this->load->view('tema/layout', $dados_view);
    }

    public function edt()
    {
        $id = $this->input->post('imposto_id');

        $this->form_validation->set_rules('imposto_descricao', 'Descrição', 'required|trim');
        $this->form_validation->set_rules('imposto_tipo_tributacao', 'Tipo de Tributação', 'required|trim');
        $this->form_validation->set_rules('imposto_icms_codigo_CFOP', 'Código CFOP', 'required|trim');
        $this->form_validation->set_rules('imposto_icms_situacao_tributaria', 'Situação Tributaria', 'required|trim');

        $this->form_validation->set_rules('imposto_ipi_situacao_tributaria', 'Situação Tributaria IPI', 'required|trim');
        $this->form_validation->set_rules('imposto_ipi_codigo_enquadramento', 'Código de enquadramento', 'required|trim');
        $this->form_validation->set_rules('imposto_ipi_aliquota', 'Alíquota IPI', 'required|trim');

        $this->form_validation->set_rules('imposto_pis_situacao_tributaria', 'Situação Tributaria PIS', 'required|trim');
        $this->form_validation->set_rules('imposto_pis_aliquota', 'Alíquota PIS', 'required|trim');

        $this->form_validation->set_rules('imposto_cofins_situacao_tributaria', 'Situação Tributaria COFINS', 'required|trim');
        $this->form_validation->set_rules('imposto_cofins_aliquota', 'Alíquota COFINS', 'required|trim');

        $this->form_validation->set_rules('imposto_tributos_federais', 'Tributos Federais', 'required|trim');
        $this->form_validation->set_rules('imposto_tributos_estaduais', 'Tributos Estaduais', 'required|trim');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('erro', validation_errors());
        } else {

            $dados = array(
                'imposto_descricao' => $this->input->post('imposto_descricao'),
                'imposto_tipo_tributacao' => $this->input->post('imposto_tipo_tributacao'),
                'imposto_icms_codigo_CFOP' => $this->input->post('imposto_icms_codigo_CFOP'),
                'imposto_icms_situacao_tributaria' => $this->input->post('imposto_icms_situacao_tributaria'),
                'imposto_ipi_situacao_tributaria' => $this->input->post('imposto_ipi_situacao_tributaria'),
                'imposto_ipi_codigo_enquadramento' => $this->input->post('imposto_ipi_codigo_enquadramento'),
                'imposto_ipi_aliquota' => $this->input->post('imposto_ipi_aliquota'),
                'imposto_pis_situacao_tributaria' => $this->input->post('imposto_pis_situacao_tributaria'),
                'imposto_pis_aliquota' => $this->input->post('imposto_pis_aliquota'),
                'imposto_cofins_situacao_tributaria' => $this->input->post('imposto_cofins_situacao_tributaria'),
                'imposto_cofins_aliquota' => $this->input->post('imposto_cofins_aliquota'),
                'imposto_tributos_federais' => $this->input->post('imposto_tributos_federais'),
                'imposto_tributos_estaduais' => $this->input->post('imposto_tributos_estaduais'),
            );

            $this->Imposto_model->atualizar($this->input->post('imposto_id'), $dados);
            $this->session->set_flashdata('success', 'Imposto editado');
        }

        redirect('imposto/editar/' . $id);
    }

    public function excluir()
    {
        $id     = $this->uri->segment(3);
        $query  = $this->Imposto_model->excluir($id);
        $this->session->set_flashdata($query ? 'success' : 'erro', $query ? 'Imposto excluído' : 'Erro ao excluir');
        redirect('imposto');
    }
}
