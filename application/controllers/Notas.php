<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Notas extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Vendas_model');
        $this->load->model('Usuarios_model');
        $this->load->model('Relatorios_model');
    }



    public function visualizarDanfe($id)
    {
        # code...       
        $id   = $this->uri->segment(3);
        $resultado       = $this->Vendas_model->visualizar($id);
        $emitente        = $this->Vendas_model->emitente();
        $financeiro      = $this->Vendas_model->financeiro($id);

        $str     = date('d/m/Y H:i:s',  strtotime($resultado[0]->data_atualizacao));
        $order   = array("/", ":", " ");
        $replace = '';
        $protocolo = str_replace($order, $replace, $str);
        $protocolo = $id . $protocolo;

        $data['emitente']   = $emitente;

        $data['protocolo']     = $protocolo;


        if ($resultado[0]->valorRecebido == NULL) {

            $data['ValorPago'] = 0.00;
            $troco = 0.00;
        } else {
            $data['ValorPago'] = $resultado[0]->valorRecebido;
            $troco = floatval($resultado[0]->valorRecebido) - floatval($resultado[0]->valorTotal);
        }

        $data['forma_pagamento'] = empty($financeiro[0]->financeiro_forma_pgto)  ? '--' : $financeiro[0]->financeiro_forma_pgto;

        $data['troco']     = number_format($troco, 2, '.', '.');
        $data['dados']     = $resultado;


        if ((count($resultado) > 0) && (count($resultado) <= 35)) {
            $data['pagina'] = 1;
        } elseif ((count($resultado) > 35) && (count($resultado) <= 70)) {
            $data['pagina'] = 2;
        } elseif ((count($resultado) > 70) && (count($resultado) <= 105)) {
            $data['pagina'] = 3;
        } elseif (count($resultado) > 105) {
            $data['pagina'] = 4;
        }

        // echo "<pre>";
        // var_dump($data);die();

        $this->load->view('vendas/visualizarDanfe', $data);
    }



    public function dashboardV3()
    {
        $dadosView['meio'] = 'dashboardv3/dashboardv3';  
        $this->load->view('tema/layout', $dadosView);
    }

    public function pegarVendasMes()
    {
        $anoAtual = date('Y');
        $mesAtual = date('m');

        $dados = $this->Relatorios_model->VendasMensalGrupoTotal($anoAtual, $mesAtual);

        $detalhes = $this->Relatorios_model->VendasMensalGrupoDetalheGrafico($anoAtual, $mesAtual, null);

        $retorno['dados'] =  $dados;
        $retorno['detalhes'] =  $detalhes;

        echo json_encode($retorno);

    }

    public function pegarVendasPorVendedor()
    {
        $anoAtual = date('Y');
        $mesAtual = date('m');

        $dados = $this->Relatorios_model->pegarVendasPorVendedorGrafico($anoAtual, $mesAtual);
        
        $retorno['dados'] =  $dados;

        echo json_encode($retorno);
    }

    public function pegarVendedoresDinamicamente()
    {
        $mesAtual = date('m');
        $anoAtual = date('Y');
        $diaAtual = date('d');
        $diaAnterior = date('d', strtotime('-1 day'));

        $dados = $this->Relatorios_model->pegarVendasDiaAnterior($mesAtual, $anoAtual, $diaAnterior);

        $dadosDiaAtual = $this->Relatorios_model->pegarVendasDiaAtual($anoAtual, $mesAtual, $diaAtual);

        $retorno['dadosDA'] =  $dados;
        $retorno['dadosDiaAtual'] =  $dadosDiaAtual;

        echo json_encode($retorno);
    }

    public function pegarAniversariantes()
    {
        $dados = $this->Usuarios_model->pegarAniversariantesMes();

        foreach ($dados as $key => $value) {
            if($value->usuario_imagem == null){
                $dados[$key]->usuario_imagem = 'no_image.png';
            }
            
            if(!file_exists('./assets/usuarios/'.$dados[$key]->usuario_imagem)){
                $dados[$key]->usuario_imagem = 'no_image.png';
            }


            $dados[$key]->usuario_imagem =  base_url().'assets/usuarios/'.$dados[$key]->usuario_imagem;
            $dados[$key]->usuario_data_nascimento = date('d/m',  strtotime($dados[$key]->usuario_data_nascimento));
        }

        $retorno['dados'] =  $dados;
        $retorno['qtd'] = $dados ? count($dados) : 0;

        echo json_encode($retorno);
    }

    public function pegarVendaCategoriaDia()
    {
        $dataIni  = date('y-m-d');
        $dataFim  = date('y-m-d');
        $loja     = 'todas';
        $vendedor = null;

        $dados = $this->Relatorios_model->vendedoresCelCustoGrupoDetalhe($dataIni, $dataFim, $vendedor, $loja);


        echo json_encode($dados);
       

    }
}
