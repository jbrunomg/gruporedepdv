<?php 
class Cotacaofornecedor extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('Cotacaofornecedor_model');
	}

    public function index()
    {
        die('Aqui');
    }

	public function carregarcotacao()
	{
        $idForne = $this->uri->segment(3);
        $idCota  = $this->uri->segment(4);
        $dadosView['produtos']   =  $this->Cotacaofornecedor_model->cotacaoFornecedor($idForne , $idCota);
        $dadosView['fornecedor'] =  $this->Cotacaofornecedor_model->pegarFornecedor($idForne);


        $dadosView['meio']  = 'controlecotacao/cotacaofornecedor';
        $this->load->view('tema/iframe',$dadosView);

	}


    public function ajustarPrecoExe()
    {

        $dados = array(              
                
            'cotacao_fornecedor_valor'        => $this->input->post('valor'),             
            'cotacao_fornecedor_id'           => $this->input->post('idProduto'),                                
            'cotacao_fornecedor_contacao_id'  => $this->input->post('idAjuste')                         
                                          
        );

        return $this->Cotacaofornecedor_model->editarAjustePreco($dados);

    }



}