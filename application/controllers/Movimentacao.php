<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Movimentacao extends CI_Controller {

		public function __construct()
	{   
		parent::__construct();
		$this->load->model('Movimentacao_model');	
        $this->load->model('Fornecedores_model');	
	}

	public function index()
	{
		$dadosView['meio'] = 'movimentacao/listar';
		$this->load->view('tema/layout',$dadosView);	
	}

	public function adicionar()
	{
	    $this->form_validation->set_rules('movimentacao_produto_documento', 'Documento', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
		{
        	$this->session->set_flashdata('erro',validation_errors());
        } else {

            if($this->input->post('movimentacao_produto_tipo') == 'Entrada'){ 
                $filial = ''; 
            }else{ 
                $filial = $this->input->post('filial'); 
            } 

            $movimentacao_data = ($this->input->post('movimentacao_produto_data_cadastro')) ? date('Y-m-d', strtotime(str_replace('/','-',$this->input->post('movimentacao_produto_data_cadastro')))) : date('Y-m-d') ;
        
        	$dados = array(  
        		'movimentacao_produto_documento'        => $this->input->post('movimentacao_produto_documento'),             
				'movimentacao_produto_tipo'             => $this->input->post('movimentacao_produto_tipo'),
                'movimentacao_produto_saida_tipo'       => $this->input->post('tipo_saida'),
                'movimentacao_produto_saida_filial'     => $filial,	
                'movimentacao_fornecedor_id'            => $this->input->post('movimentacao_fornecedor_id'), 						    
				'movimentacao_produto_usuario_id'       => $this->session->userdata('usuario_id'), 				
				'movimentacao_produto_data_cadastro'    => $movimentacao_data,
                'movimentacao_tipo_moeda'               => $this->input->post('tipo_moeda'),
                'movimentacao_cotacao_moeda'            => $this->input->post('cotacao_moeda'),
                'movimentacao_produto_porcentagem'      => $this->input->post('percent'),
                'movimentacao_produto_imei'             => $this->input->post('imei'),
				'movimentacao_produto_visivel'          => 1				  
        	);


             if (is_numeric($id = $this->Movimentacao_model->adicionar($dados)) ) {
                $this->session->set_flashdata('success','Entrada/Saída iniciada com sucesso, adicione os produtos.');

                redirect('movimentacao/editar/'.$id);
      
        	}else{
        		$this->session->set_flashdata('erro','Erro ao adicionado o registro!');
        	}
        }

        $dadosView['filias'] = $this->Movimentacao_model->getFilias();

        

        $dadosView['fornecedor'] = $this->Movimentacao_model->pegarFornecedor();
		$dadosView['meio'] = 'movimentacao/adicionar';
		$this->load->view('tema/layout',$dadosView);
	}

	public function editar()
	{
		$this->form_validation->set_rules('movimentacao_produto_documento', 'Documento', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
		{
        	$this->session->set_flashdata('erro',validation_errors());
        } else {

        	$dados = array(        		 
				
				'movimentacao_produto_documento'     => $this->input->post('movimentacao_produto_documento'),             
				'movimentacao_produto_tipo'          => $this->input->post('movimentacao_produto_tipo'),							    
				'movimentacao_produto_usuario_id'      => $this->session->userdata('usuario_id'), 				
				'movimentacao_produto_data_cadastro'   => date('Y-m-d')   
										  
        	);
     
        	$resultado = $this->Movimentacao_model->editar($dados,$this->input->post('id'));

        	if($resultado){
        		$this->session->set_flashdata('success','Registro editado com sucesso!');
        	}else{
        		$this->session->set_flashdata('erro','Erro ao editar o registro!');
        	}
        }

        $consultaTipo = $this->Movimentacao_model->consultaTipo($this->uri->segment(3));

        // if($this->session->userdata('empresa_tipo') == '1'){
        if($this->session->userdata('empresa_tipo') == '1' and $consultaTipo[0]->movimentacao_saida_origem_nome == null ){    
            $dadosForn  = $this->Fornecedores_model->listarId($consultaTipo[0]->movimentacao_fornecedor_id);
            $dadosView['categoria_prod'] = $this->Movimentacao_model->pegarCategoriaProd($dadosForn[0]->fornecedor_grupoprod_id);
        }else{
            $dadosView['categoria_prod'] = $this->Movimentacao_model->pegarCategoriaProd($ids = false);
        }
        $dadosView['dados']   = $this->Movimentacao_model->listarId($this->uri->segment(3), $consultaTipo[0]->movimentacao_produto_saida_tipo);
        $dadosView['produtos'] = $this->Movimentacao_model->getProdutosList($this->uri->segment(3));        
		$dadosView['meio']    = $dadosView['dados'][0]->movimentacao_produto_tipo != 'Saída' ? 'movimentacao/editar' : 'movimentacao/editarRetirada';

		$this->load->view('tema/layout',$dadosView);

	}

	public function visualizar()
	{
        $consultaTipo = $this->Movimentacao_model->consultaTipo($this->uri->segment(3));
        // $dadosView['categoria_prod'] = $this->Movimentacao_model->pegarCategoriaProd();
        $dadosView['dados']   = $this->Movimentacao_model->listarId($this->uri->segment(3), $consultaTipo[0]->movimentacao_produto_saida_tipo);
        $dadosView['produtos'] = $this->Movimentacao_model->getProdutosList($this->uri->segment(3));
        $dadosView['meio']    = 'movimentacao/visualizar';

        $this->load->view('tema/layout',$dadosView);
	}

    public function imprimirEntradaSaida(){

        $this->load->model('Sistema_model');
       
        $consultaTipo = $this->Movimentacao_model->consultaTipo($this->uri->segment(3));

        $dadosView['emitente'] = $this->Sistema_model->pegarEmitente();
        // $dadosView['categoria_prod'] = $this->Movimentacao_model->pegarCategoriaProd();
        $dadosView['dados']   = $this->Movimentacao_model->listarId($this->uri->segment(3), $consultaTipo[0]->movimentacao_produto_saida_tipo);
        $dadosView['produtos'] = $this->Movimentacao_model->getProdutosList($this->uri->segment(3));
        $dadosView['meio']    = 'movimentacao/imprimirEntradaSaida';

        $html = $this->load->view('relatorios/impressao',$dadosView, true); 
            
        gerarPDF($html);
       
    }


	public function excluir()
	{
		$id = $this->uri->segment(3);

		$dados  = array(
						'movimentacao_produto_visivel' => 0						
					  );

		$resultado = $this->Movimentacao_model->excluir($dados,$id);

		if($resultado){
			$this->session->set_flashdata('success','registro excluidos com sucesso!');
		}else{
			$this->session->set_flashdata('erro','Erro ao excluir o registro!');
		}

		redirect('Movimentacao','refresh');
	}


	public function adicionarProduto(){ 



        $this->form_validation->set_rules('quantidade', 'Quantidade', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
        {
            //echo json_encode(array('result'=> false)); 
            $this->session->set_flashdata('erro',validation_errors());
        }
        else{            

            $tipo          = $this->input->post('tipo');
            $quantidade    = $this->input->post('quantidade');
            $precoUnitario = $this->input->post('precoUnitario');
            $produto       = $this->input->post('produto_id');
            
            $data = array(
                'item_movimentacao_produto_quantidade'=> $quantidade,
                'item_movimentacao_produto_preco_unitario'=> $precoUnitario,
                'item_movimentacao_produto_produto_id' => $this->input->post('movimentacao_produto_id'),
                'item_movimentacao_produto_movimentacao_produto_id'=> $produto,
            );

            //var_dump($data);die();

            if($tipo == 'Entrada'){
                  if($this->Movimentacao_model->add('itens_movimentacao_produtos', $data) == true){                               

                        $sql = "UPDATE produtos set produto_estoque = produto_estoque + ? WHERE produto_id = ?";          

                        $this->db->query($sql, array($quantidade, $produto));                   

                        echo json_encode(array('result'=> true));
                    }else{
                        echo json_encode(array('result'=> false));
                    }
            }else{
                    if($this->Movimentacao_model->add('itens_movimentacao_produtos', $data) == true){                              

                        $sql = "UPDATE produtos set produto_estoque = produto_estoque - ? WHERE produto_id = ?";                 

                        $this->db->query($sql, array($quantidade, $produto));                   

                        echo json_encode(array('result'=> true));
                    }else{
                        echo json_encode(array('result'=> false));
                    }   

            } 
        }  

    }



    public function excluirProduto(){
            $ID = $this->uri->segment(3);
            $produto = $this->uri->segment(4);
            $quantidade = $this->uri->segment(5);

            
            if($this->Movimentacao_model->delete('itens_movimentacao_produtos','item_movimentacao_produto_id',$ID) == true){     

                $sql = "UPDATE produtos set produto_estoque = produto_estoque - ? WHERE produto_id = ?";
                $this->db->query($sql, array($quantidade, $produto));          

                echo json_encode(array('result'=> true));

            }
            else{         

                echo json_encode(array('result'=> false));
            }        

    }


    public function selecionarProduto(){
        
        $grupo   = $this->input->post('grupo');
        $produto = $this->Movimentacao_model->pegarProdutos($grupo);

        echo  "<option value=''>Selecionar um produto</option>";
        foreach ($produto as $p) {
            echo "<option value='".$p->produto_id."'>".$p->produto_descricao."</option>";
        }

    }


    public function autoCompleteProduto()
    {
        $termo = strtolower($this->input->get('term'));
        $this->Movimentacao_model->autoCompleteProduto($termo);
    }

    public function produtoProId()
    {
        $produto = $this->input->post('produto');

        $result = $this->Movimentacao_model->pegarProdutoId($produto);

        echo json_encode($result[0]);
    }

    public function addProdutoAjax(){

        $dados = array(
            "produto_categoria_id"          => $this->input->post('produto_categoria_id'),
            "produto_descricao"             => $this->input->post('produto_descricao'),
            "produto_codigo"                => $this->input->post('produto_codigo'),
            "produto_estoque"               => 0
        );

        $result = $this->Movimentacao_model->addProduto($dados);

        echo json_encode($result);
    }

    public function addItemMovimentoProduto($movimentacaoId)
    {

        $verificarProduto = $this->Movimentacao_model->verificaMovimentoProduto($movimentacaoId, $this->input->post('idProduto'));
        if(intval($verificarProduto[0]->total) < 1){

            $dados = array(
                'item_movimentacao_produto_quantidade'                  => $this->input->post('quantidade'), 
                'item_movimentacao_produto_preco_unitario'              => $this->input->post('venda'), 
                'item_movimentacao_produto_custo'                       => $this->input->post('custo'), 
                'item_movimentacao_produto_minimo'                      => $this->input->post('minimo'), 
                'item_movimentacao_produto_produto_id'                  => $this->input->post('idProduto'),
                'item_movimentacao_produto_dolar'                       => $this->input->post('dolar'),
                'item_movimentacao_produto_movimentacao_produto_id'     => $movimentacaoId
            );
    
            $result = $this->Movimentacao_model->addItemMovimentoProduto($dados);
    
            echo json_encode($result);

        }else{

            echo json_encode(false);

        }

    }

    public function deleteItemMovimentoProduto($produtoId)
    {
         $result = $this->Movimentacao_model->deleteItemMovimentoProduto($produtoId);

         echo json_encode($result);
    }

    public function entradaProdutos($idMovimentacao)
    {
        $produtos = $this->Movimentacao_model->getProdutosList($idMovimentacao);

        foreach ($produtos as $produto) {
            $this->Movimentacao_model->updateProdutoEntrada($produto);
        }

        $this->Movimentacao_model->updateStatusMovimentacao($idMovimentacao);

        redirect('movimentacao/index');
    }

    public function saidaProdutos($idMovimentacao)
    {
        $produtos = $this->Movimentacao_model->getProdutosList($idMovimentacao);

        foreach ($produtos as $produto) {
            if($produto->Estoque < $produto->quantidadeProduto){
                $this->session->set_flashdata('error','A quantidade do produto '.$produto->descricaoProduto.', excede o estoque !');
                redirect('movimentacao/editar/'.$idMovimentacao);
        }

        foreach ($produtos as $produto) {
            $this->Movimentacao_model->updateProdutoSaida($produto);
        }

        $this->Movimentacao_model->updateStatusMovimentacao($idMovimentacao);

        redirect('movimentacao/index');
     }

    }

    public function mercadoriaLiberada()
    {
        $dadosView['dados'] = $this->Movimentacao_model->mercadoriaLiberada();

        $dadosView['meio'] = 'movimentacao/listar';
        $this->load->view('tema/layout',$dadosView);    
        
    }

    public function rejeitar()
    {
        $id = $this->uri->segment(3);


        $resultado = $this->Movimentacao_model->EstornoMovimentacao($this->uri->segment(3));


        if($resultado){
            $this->session->set_flashdata('success','Entrada rejeitada com sucesso!');
        }else{
            $this->session->set_flashdata('erro','Erro ao excluir o registro!');
        }

        redirect('Movimentacao','refresh');
    }

    public function listarDados(){

        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $search = $this->input->post('search') ?? ''; // Verifica se 'value' existe
    
        $dados = $this->Movimentacao_model->listar($limit, $start, $search);
        // var_dump($dados);die();

        $formatted_data = [];
        $icon = '';
    
        foreach ($dados['dados'] as $d) { 
    
            if (verificarPermissao('vAlmoxarifado')) {
                $icon .= '<a href="' . base_url() . $this->uri->segment(1) . '/visualizar/' . $d->movimentacao_produto_id . '" data-toggle="tooltip" title="Visualizar"><i class="fa fa-search text-success"></i></a>';
            }
            if (verificarPermissao('eAlmoxarifado') && $d->movimentacao_produto_status != 0) {
                $icon .= '<a href="' . base_url() . $this->uri->segment(1) . '/editar/' . $d->movimentacao_produto_id . '" data-toggle="tooltip" title="Editar"><i class="fa fa-edit"></i></a>';
            }
            if (verificarPermissao('dAlmoxarifado') && $d->movimentacao_produto_status != 0) {
                $icon .= '<a style="cursor:pointer;" title="Deletar" data-toggle="modal" data-target="#excluir" onclick="excluir(\'' . base_url() . $this->uri->segment(1) . '/excluir/' . $d->movimentacao_produto_id . '\');" ><i class="fa fa-trash text-danger"></i></a>';
            }
            if ($d->movimentacao_produto_status != 0 && $d->movimentacao_saida_origem_id != '') {
                $icon .= '<a style="cursor:pointer;" title="Rejeitar Entrada" data-toggle="modal" data-target="#rejeitar" onclick="rejeitar(\'' . base_url() . $this->uri->segment(1) . '/rejeitar/' . $d->movimentacao_produto_id . '\');" ><i class="fa fa-retweet text-yellow"></i></a>';
            }
    
          if (!empty($d->movimentacao_produto_data_cadastro)) {
            // Converte a data para o formato brasileiro (dd/mm/aaaa)
            $dataFormatada = date('d/m/Y', strtotime($d->movimentacao_produto_data_cadastro));
          } else {
            $dataFormatada = '';
          }

          $formatted_data[] = array(
              'movimentacao_produto_id' =>  $d->movimentacao_produto_id,
              'movimentacao_produto_saida_filial' => $d->movimentacao_produto_saida_filial == NULL ? ' -------' : $d->movimentacao_produto_saida_filial,              'movimentacao_produto_tipo' => $d->movimentacao_produto_tipo,
              'movimentacao_produto_data_cadastro' => $dataFormatada, // Adiciona a data formatada aqui
              'movimentacao_produto_documento' => $d->movimentacao_produto_documento,
              'total' => number_format($d->total,2,".",""),            
              'icon' => $icon
          );
    
          $icon = '';
        }
    
          $output = [
              "draw" => intval($this->input->post('draw')),
              "recordsTotal" => $dados['total_registros'],
              "recordsFiltered" => $dados['total_registros'],
              "data" => $formatted_data
          ];
          
          echo json_encode($output);
      }


      
      public function fornecedorBaixarCompraListar()
      {
          $dadosView['dados'] = $this->Movimentacao_model->fornecedorBaixarListar();
          $dadosView['fornecedorPendentes'] = $this->Movimentacao_model->fornecedorPendentes();
          $dadosView['meio'] = 'baixarcompra/listar';
          $this->load->view('tema/layout', $dadosView);
      }

      public function fornecedorBaixarAdd()
      {
        
          $valor_total  = $this->input->post('valor_total');
          $valor_pago   = $this->input->post('valor_pago');
          $valor_debito = $this->input->post('valor_debito');
          $fornecedor   = $this->input->post('fornecedor');
          $usuario      = $this->session->userdata('usuario_id');
          $data_baixa   = $this->input->post('data_baixa'); 
          $data_baixa_formatada = DateTime::createFromFormat('d/m/Y', $data_baixa)->format('Y-m-d'); 

          if (!($valor_total and $valor_pago and $valor_debito <> '' and  $fornecedor and $usuario)) {
              echo json_encode(array('result' => false));
              $this->session->set_flashdata('erro', 'Dados Invalidos!');
          } else {
  
              $dados = array(
                  'id_fornecedor'    => $this->input->post('fornecedor'),
                  'id_usuario'       => $this->session->userdata('usuario_id'),
                  'valor_baixa'      => $this->input->post('valor_pago'),
                  'valor_debito'     => $this->input->post('valor_debito'),
                  'valor_total'      => $this->input->post('valor_total'),
                  'data_baixa'       => $data_baixa_formatada,

              );
  
              // BEGIN - trasaction, garanti que a baixas foram feitas 100%
  
              $resultado = $this->Movimentacao_model->fornecedorBaixarAdd($dados);
              // END - trasaction, garanti que a baixas foram feitas 100%
            //   die($resultado);
  
            //   die($resultado);
              if ($resultado) {
                  $this->session->set_flashdata('success', 'Registro adicionado com sucesso!');
                  echo json_encode(array('result' => true));
              } else {
                  $this->session->set_flashdata('erro', 'Erro ao adicionado o registro!');
                  echo json_encode(array('result' => false));
              }
          }
      }

    public function verificarBaixar()
    {
        $valor_pago   = $this->input->post('valor_pago');
        $cliente      = $this->input->post('cliente');
        $resultado = $this->Financeiro_model->verificarBaixar($cliente, $valor_pago);

        if ($resultado) {
            echo json_encode(true);
        } else {
            echo json_encode(false);
        }
    }

 

 


}
