<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Centrocustos extends CI_Controller {

		public function __construct()
	{   
		parent::__construct();
		$this->load->model('Centrocustos_model');
	}

	public function index()
	{
		$dadosView['dados'] = $this->Centrocustos_model->listar();

		$dadosView['meio'] = 'centrocustos/listar';
		$this->load->view('tema/layout',$dadosView);	
	}

	public function adicionar()
	{
	    $this->form_validation->set_rules('centro_custos_descricao', 'Descrição', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
		{
        	$this->session->set_flashdata('erro',validation_errors());
        } else {

        	$dados = array(        		 
				  'centro_custos_descricao'             => $this->input->post('centro_custos_descricao'),
				  'centro_custos_visivel'               => 1				  
        	);

        	$resultado = $this->Centrocustos_model->adicionar($dados);

        	if($resultado){
        		$this->session->set_flashdata('success','Registro adicionado com sucesso!');
        	}else{
        		$this->session->set_flashdata('erro','Erro ao adicionado o registro!');
        	}
        }

		$dadosView['meio'] = 'centrocustos/adicionar';
		$this->load->view('tema/layout',$dadosView);
	}

	public function editar()
	{
		$this->form_validation->set_rules('centro_custos_descricao', 'Descrição', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
		{
        	$this->session->set_flashdata('erro',validation_errors());
        } else {

        	$dados = array(        		 
				  'centro_custos_descricao'                 => $this->input->post('centro_custos_descricao'),
				  'centro_custos_visivel'               => 1				  
        	);
     
        	$resultado = $this->Centrocustos_model->editar($dados,$this->input->post('id'));

        	if($resultado){
        		$this->session->set_flashdata('success','Registro editado com sucesso!');
        	}else{
        		$this->session->set_flashdata('erro','Erro ao editar o registro!');
        	}
        }

        $this->load->model('Sistema_model');

        $dadosView['dados']   = $this->Centrocustos_model->listarId($this->uri->segment(3));
		$dadosView['meio']    = 'centrocustos/editar';

		$this->load->view('tema/layout',$dadosView);

	}

	public function visualizar()
	{
		$this->load->model('Sistema_model');
		
		$dadosView['dados']   = $this->Centrocustos_model->listarId($this->uri->segment(3));
		$dadosView['meio']    = 'centrocustos/visualizar';
		$this->load->view('tema/layout',$dadosView);
	}

	public function excluir()
	{
		$id = $this->uri->segment(3);

		$dados  = array(
						'centro_custos_visivel' => 0						
					  );
		$resultado = $this->Centrocustos_model->excluir($dados,$id);

		if($resultado){
			$this->session->set_flashdata('success','registro excluidos com sucesso!');
		}else{
			$this->session->set_flashdata('erro','Erro ao excluir o registro!');
		}

		redirect('cargos','refresh');
	}
}
