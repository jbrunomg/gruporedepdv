<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PDV extends CI_Controller {

  public function __construct()
	{   
		parent::__construct();
		$this->load->model('PDV_model');		
	}


  public function index(){

     $loja = $this->input->get('loja');
     if($loja == NULL){

       $loja = BDCAMINHO;
     }
        
     $data['emitente']   = $this->PDV_model->emitente();
     $data['categorias'] = $this->PDV_model->getAllCategorias($loja);
     $data['produtos']   = $this->ajaxproducts('1', 1);
     $data['filias']     = $this->PDV_model->getAllFilias();

     // echo "<pre>"; var_dump($data['filias']); exit();
     $data['meio']       = 'pdv/pdv';
		 $this->load->view('tema/layout',$data);
	}

  public function selecionarCliente(){
        
        $cliente = $this->PDV_model->selecionarCliente();

        echo  "<option value=''>Selecionar um Cliente</option>";
        foreach ($cliente as $c) {
            echo "<option value='".$c->cliente_id."'>".$c->cliente_nome."</option>";
        }

    }

  public function categorias(){

     $loja = $this->input->get('loja');
     if($loja == NULL){

       $loja = BDCAMINHO;
     }
     $result = $this->PDV_model->getAllCategorias($loja);

     echo json_encode($result);


   }

  public function addCliente(){

      $this->form_validation->set_rules('cliente_nome', 'Nome', 'trim|required');
      $this->form_validation->set_rules('cpfCnpj', 'CPF', 'trim|required|is_unique[clientes.cliente_cpf_cnpj]');

       
       if($this->form_validation->run() == FALSE) {

          $this->session->set_flashdata('erro',validation_errors());

        } else {

           $dados = array(                    
              'cliente_cpf_cnpj'            => $this->input->post('cpfCnpj'),
              'cliente_tipo'                => $this->input->post('tipo'),
              'cliente_nome'                => $this->input->post('cliente_nome'),
              'cliente_endereco'            => $this->input->post('cliente_endereco'),
              'cliente_celular'             => $this->input->post('cliente_numero'),
              'cliente_data_cadastro'       => date('Y-m-d'),
              'cliente_email'               => $this->input->post('cliente_email'),                  
              );
            
            $cliente = $this->PDV_model->addCliente($dados);

        if($cliente){
          $this->session->set_flashdata('success','Registro adicionado com sucesso!');
        }else{
          $this->session->set_flashdata('erro','Erro ao adicionado o registro!');
        }

    }

  }

   public function get_product($code = NULL, $loja) {

    if($this->input->get('code')) { $code = $this->input->get('code'); }
    $combo_items = FALSE;
    if($product = $this->PDV_model->getProductByCode($code, $loja)) {
      // unset($product->cost, $product->details);
      $teste = 'ok' ;
      $product->qty = 1;
      $product->discount = '0';
      $product->real_unit_price = $product->produto_preco_venda;
      $product->unit_price = 0 ? ($product->produto_preco_venda+(($product->produto_preco_venda*0)/100)) : $product->produto_preco_venda;
      if ($teste == 'combo') {
        $combo_items = $this->PDV_model->getComboItemsByPID($product->produto_id);
      }
      echo json_encode(array('id' => str_replace(".", "", microtime(true)), 'item_id' => $product->produto_id, 'label' => $product->produto_descricao . " (" . $product->produto_codigo . ")", 'row' => $product, 'combo_items' => $combo_items));
    } else {
      echo NULL;
    }

  }


  public function ajaxproducts( $category_id = NULL, $return = NULL) {

    if($this->input->get('category_id')) { $category_id = $this->input->get('category_id'); } elseif(!$category_id) { $category_id = $this->Settings->default_category; }
    if($this->input->get('per_page') == 'n' ) { $page = 0; } else { $page = $this->input->get('per_page'); }
    if($this->input->get('tcp') == 1 ) { $tcp = TRUE; } else { $tcp = FALSE; }

     $loja = $this->input->get('loja');
     if($loja == NULL){

       $loja = BDCAMINHO;
     }

    $pro_limit = 99;
    $products = $this->PDV_model->listarProdutos($category_id, $pro_limit, $page, $loja);
    $pro = 1;
    $exibicao = 1;
    $prods = "<div>";
    if($products) {
      if($exibicao == 1) {
        foreach($products as $product) {
          $count = $product->produto_id;
          $CountQuandidade = $product->produto_estoque;
          if($count < 10) { $count = "0".($count /100) *100;  }
          if($category_id < 10) { $category_id = "0".($category_id /100) *100;  }
          $prods .= "<button  type=\"button\" data-name=\"".$product->produto_descricao."\" id=\"product-".$category_id.$count."\" type=\"button\" value='".$product->produto_codigo."' quantidade='".$CountQuandidade."' loja=".$loja." data-selected=".$product->produto_id." class=\"btn btn-both btn-flat product addCountQuandidade prod_".$product->produto_id."\"><span class=\"bg-img\">PV ".$product->produto_preco_venda."<br>PM ".$product->produto_preco_minimo_venda."<br>CD ".$product->produto_preco_cart_debito."<br>CC ".$product->produto_preco_cart_credito."</span><span><span><font id='font_id_".$product->produto_id."' color='#AA0000'>".$CountQuandidade."</font>  ".$product->produto_descricao."</span></span></button>";
          $pro++;
        }
      } elseif($exibicao == 2) {
        foreach($products as $product) {
          $count = $product->produto_id;
          if($count < 10) { $count = "0".($count /100) *100;  }
          if($category_id < 10) { $category_id = "0".($category_id /100) *100;  }
          $prods .= "<button type=\"button\" data-name=\"".$product->produto_descricao."\" id=\"product-".$category_id.$count."\" type=\"button\" value='".$product->produto_codigo."' class=\"btn btn-img btn-flat product\"><img src=\"http://localhost/sistemapdv/uploads/thumbs/99ba15cb71eafc2bf6675c47eda93853.gif\" alt=\"".$product->produto_descricao."\" style=\"width: 110px; height: 110px;\"></button>";
          $pro++;
        }
      } elseif($exibicao == 3) {
        foreach($products as $product) {
          $count = $product->produto_id;
          //var_dump($loja);die();
          $CountQuandidade = $product->produto_estoque;


          if($count < 10) { $count = "0".($count /100) *100;  }
          if($category_id < 10) { $category_id = "0".($category_id /100) *100;  }
          $prods .= "<button  type=\"button\" data-name=\"".$product->produto_descricao."\" id=\"product-".$category_id.$count."\" type=\"button\" value='".$product->produto_codigo."' quantidade='".$CountQuandidade."' loja=".$loja." data-selected=".$product->produto_id." class=\"btn btn-both btn-flat product addCountQuandidade prod_".$product->produto_id."\"><span class=\"bg-img\"><img src=\"http://localhost/sistemapdv/uploads/thumbs/99ba15cb71eafc2bf6675c47eda93853.gif\" alt=\"".$product->produto_descricao."\" style=\"width: 100px; height: 100px;\"></span><span><span><font id='font_id_".$product->produto_id."' color='#AA0000'>".$CountQuandidade."</font>  ".$product->produto_descricao."</span></span></button>";
          $pro++;
        }
      }
    } else {
      $prods .= '<h4 class="text-center text-info" style="margin-top:50px;">'.lang('category_is_empty').'</h4>';
    }

    $prods .= "</div>";

    if(!$return) {
      if(!$tcp) {
        echo $prods;
      } else {
        $category_products = $this->PDV_model->listarProdutosGrupo($category_id, $loja);
        header('Content-Type: application/json');
        echo json_encode(array('products' => $prods, 'tcp' => $category_products));
      }
    } else {
      return $prods;
    }

  }

 public function suggestions()
  {
    $term = $this->input->get('term', TRUE);

    $rows = $this->PDV_model->getProductNames($term);
    if ($rows) {
      foreach ($rows as $row) {
        unset($row->cost, $row->details);
        $teste = 'ok' ;
        $row->qty = 1;
        $row->discount = '0';
        $row->real_unit_price = $row->produto_preco_venda;
        // $row->unit_price = $row->tax ? ($row->price+(($row->price*$row->tax)/100)) : $row->price;
        $combo_items = FALSE;
        if ($teste == 'combo') {
            $combo_items = $this->PDV_model->getComboItemsByPID($row->id);
        }
        $pr[] = array('id' => str_replace(".", "", microtime(true)), 'item_id' => $row->produto_id, 'label' => $row->produto_descricao . " (" . $row->produto_codigo . ")", 'row' => $row, 'combo_items' => $combo_items);
      }
      echo json_encode($pr);
    } else {
      echo json_encode(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
    }
  }

 public function venda()
  {
  
    if( $this->input->get('hold') ) { $sid = $this->input->get('hold'); }
    if( $this->input->get('edit') ) { $eid = $this->input->get('edit'); }
    if( $this->input->post('eid') ) { $eid = $this->input->post('eid'); }
    if( $this->input->post('did') ) { $did = $this->input->post('did'); } else { $did = NULL; }


    $suspend = $this->input->post('suspend') ? TRUE : FALSE;

    $this->form_validation->set_rules('customer', lang("customer"), 'trim|required');

    if ($this->form_validation->run() == true) {


     // echo "<pre>"; var_dump($_POST); exit();

      $total = 0;

      $i = isset($_POST['product_id']) ? sizeof($_POST['product_id']) : 0;
      for ($r = 0; $r < $i; $r++) {
        $item_id = $_POST['product_id'][$r];
        $real_unit_price = $_POST['produto_preco_minimo_venda'][$r];
        $item_quantity = $_POST['quantity'][$r];
        $item_discount = isset($_POST['product_discount'][$r]) ? $_POST['product_discount'][$r] : '0';

          if (isset($item_id) && isset($real_unit_price) && isset($item_quantity)) {
          
          $loja = explode("_", $item_id); 
          $item_id = $loja[3];
          $filial = $loja[2];
     
          $product_details = $this->PDV_model->getProductByID($item_id);
          $unit_price = $real_unit_price;
          $item_net_price = $unit_price;
          $subtotal = (($item_net_price * $item_quantity));

          $products[] = array(
            'produtos_id' => $item_id,
            'quantidade'  => $item_quantity,
            'filial'      => $filial,
            // 'unit_price' => $unit_price,
            // 'net_unit_price' => $item_net_price,
            'subtotal' => $subtotal,
            // 'real_unit_price' => $real_unit_price
            );



          $total += $item_net_price * $item_quantity;

        }

      }

      if (empty($products)) {
        $this->form_validation->set_rules('product', lang("order_items"), 'required');
      } else {
        krsort($products);
      }

      $data = array(
        'dataVenda' => date('Y-m-d'),
        'valorTotal' => $total,
        'clientes_id' => $this->input->post('customer_id'),
        'usuarios_id' => $this->session->userdata('usuario_id'),
        'emitirNota'  => '0',
        'valorRecebido'  => $this->input->post('amount'),
        'observacao'  => $this->input->post('spos_note'),
        'forma_pagamento' => $this->input->post('paid_by')

        );
      if($suspend) {
        $data['hold_ref'] = $this->input->post('hold_ref');
      }

   }

  if ( $this->form_validation->run() == true && !empty($products) )
    {
      if($sale = $this->PDV_model->addSale($data, $products)) {
        $this->session->set_userdata('rmspos', 1);
        $this->session->set_flashdata('success','Venda cadastrada com sucesso!');
        redirect("vendas/editar/" . $sale['sale_id']);
      }else {
        $this->session->set_flashdata('erro','Erro ao inserir os dados!');
        redirect("pdv");
      }

    }


 }


}