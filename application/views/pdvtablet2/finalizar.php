<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo strtoupper(SUBDOMINIO) ?> | Controler</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/main.css">
  <link href="<?php echo base_url(); ?>assets/dist/css/custom.css" rel="stylesheet" type="text/css" />
  <!-- Autocomplete -->
  <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
  <link href="<?php echo base_url(); ?>/assets/dist/css/jquery-ui.css" rel="stylesheet" type="text/css" />
  <style>
    .ui-front {
      z-index: 1051;
    }

    #formas_recebimento::-webkit-scrollbar {
      width: 5px;
      margin-left: 3px;
    }

    #formas_recebimento::-webkit-scrollbar-track {
      background: #f1f1f1;
    }

    #formas_recebimento::-webkit-scrollbar-thumb {
      background: #3C8DBC;
    }

    #formas_recebimento::-webkit-scrollbar-thumb:hover {
      background: #b3b3b3;
    }

    .bottom-border {
      border-bottom: 3px solid #3C8DBC;
    }

    .required {
      color: red;
    }

    label.error {
      color: red;
    }
  </style>

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/verificar-conexao.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!-- ADD THE CLASS sidebar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->

<body class="hold-transition skin-<?php echo CLIENTE ?> sidebar-collapse sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">

    <header class="main-header">
      <!-- Logo -->
      <a style="cursor:pointer;" data-toggle="modal" data-target="#excluirVenda" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>P</b>DV2</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Admin</b>LTE</span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- Messages: style can be found in dropdown.less-->
            <li><a href="#" class="filialAtual" style="background:#ff0; color:#fff;"><?php $loja = BDCAMINHO;
                                                                                      $loja = explode("_", $loja);
                                                                                      echo strtoupper($loja[2]); ?></a>
              <input type="hidden" id="filialAtual" value="<?php echo BDCAMINHO; ?>">
            </li>
            <li><a href="#" class="clock"></a></li>
            <!--           <li><a href="http://localhost/sistemapdv/"><i class="fa fa-dashboard"></i></a></li>
          <li><a href="http://localhost/sistemapdv/settings"><i class="fa fa-cogs"></i></a></li>
          <li><a href="http://localhost/sistemapdv/pos/view_bill" target="_blank"><i class="fa fa-file-text-o"></i></a></li> -->
            <li><a href="#" data-toggle="modal" data-target=".historico">Histórico de Vendas</a></li>
            <li><a href="#" data-toggle="modal" data-target=".rank">Ranck do Vendedores</a></li>
            <!--           <li><a href="http://localhost/sistemapdv/pos/close_register" data-toggle="ajax">Fechar Caixa</a></li> -->

            <li class="dropdown notifications-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bell-o"></i>
                <span class="label label-warning">1</span>
              </a>
              <ul class="dropdown-menu">
                <li class="header">Vendas recentes em aberto</li>
                <li>
                  <ul class="menu">
                    <li>
                      <a href="http://localhost/sistemapdv/pos/?hold=2" class="load_suspended">3030/0303/2016161616 08:11:01 PM (Cliente Padr?o)<br><strong>teste</strong></a>
                    </li>
                  </ul>
                </li>
                <li class="footer"><a href="http://localhost/sistemapdv/sales/opened">Ver tudo</a></li>
              </ul>
            </li>

            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="<?php echo base_url(); ?>assets/dist/img/user2-160x160.jpg" class="user-image" alt="Avatar" />
                <span class="hidden-xs"><?php echo $this->session->userdata('usuario_nome'); ?></span>
              </a>
              <ul class="dropdown-menu">
                <li class="user-header">
                  <img src="<?php echo base_url(); ?>assets/dist/img/user2-160x160.jpg" class="img-circle" alt="Avatar" />
                  <p>
                    <?php echo $this->session->userdata('usuario_nome'); ?> <br> <?php echo $this->session->userdata('usuario_perfil_nome'); ?>
                  </p>
                </li>
                <li class="user-footer">
                  <div class="pull-left">
                    <a href="#" class="btn btn-default btn-flat" data-toggle="modal" data-target=".bs-example-modal-sm">Alterar Senha</a>
                  </div>
                  <div class="pull-right">
                    <a href="<?php echo base_url(); ?>sistema/logout" class="btn btn-default btn-flat">Sair</a>
                  </div>
                </li>
              </ul>
            </li>
            <!-- Control Sidebar Toggle Button -->
            <li>
              <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
            </li>
          </ul>
        </div>
      </nav>
    </header>

    <!-- =============================================== -->

    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
            <img src="<?php echo base_url(); ?>assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
          </div>
          <div class="pull-left info">
            <p><?php echo $this->session->userdata('usuario_nome'); ?></p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
          <div class="input-group">
            <input type="text" name="q" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
          </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header">MENU NAVEGAÇÃO</li>



          <li><a style="cursor:pointer;" data-toggle="modal" data-target="#excluirVenda"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
          <li class="header">LABELS</li>
          <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Sair</span></a></li>
          <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Finalizar Venda</span></a></li>
          <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Iniciar Venda</span></a></li>
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>

    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <!--     <section class="content-header">
      <h1>
        Sidebar Collapsed
        <small>Layout with collapsed sidebar on load</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Layout</a></li>
        <li class="active">Collapsed Sidebar</li>
      </ol>
    </section> -->

      <!-- Main content -->
      <form id="form_finalizar_venda">

        <section class="content" id="conteudo" onclick="hideSideBar()">
          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <input type="hidden" id="idVenda" name="idVenda" value="<?php echo $this->uri->segment(3) ?>">
                <div class="info-box p-m">
                  <div class="row">
                    <div class="col-md-7">
                      <div class="row">
                        <div class="col-md-3">
                          <div class="form-group">
                            <label>Data:<span class="required">*</span></label>
                            <div class="input-group date">
                              <div class="input-group-addon">
                                <a href="#" class="external" data-toggle="modal" data-target="#senha-gerente"><i class="fa fa-calendar"></i></a>
                              </div>
                              <input type="text" class="form-control pull-right" id="datepicker" name="data" disabled>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <label>Número Venda:<span class="required">*</span></label>
                            <input type="text" class="form-control" name="numero_venda" value="<?php echo $this->uri->segment(3) ?>" disabled>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <label>Cliente:<span class="required">*</span></label>
                            <div class="input-group date">
                              <div class="input-group-addon">
                                <a href="#" id="add-cliente" class="external" data-toggle="modal" data-target="#myModal"><i class="fa fa fa-plus-circle" id="addIcon"></i></a>
                              </div>
                              <!-- 											  <select name="cliente_nome" id="cliente_nome" data-placeholder="Selecione Cliente" required="required" class="form-control select2" style="width:100%;">
                              <option value="">Selecione um Cliente</option>
                            </select> -->
                              <!-- <input type="text" name="cliente_nome" id="cliente_nome" required="required" class="form-control"> -->
                              <input type="text" class="form-control" name="cliente_nome" id="cliente_nome" value="<?php echo $venda[0]->cliente_nome ?>" placeholder="Cliente">
                              <input type="hidden" id="cliente_id" name="cliente_id" value="<?php echo $venda[0]->cliente_id  ?>">
                              <input type="hidden" id="cliente_limite_cred_cliente" name="cliente_limite_cred_cliente" value="<?php echo $venda[0]->cliente_limite_cred_cliente  ?>">
                            </div>
                            <center>
                              <p id="aviso_cliente" style="display: none">
                                <font color="red">Venda(fiado) apenas com identificação cliente!</font>
                              </p>
                            </center>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <!-- O VALOR TRUE É ATRIBUÍDO AO CHECKBOX QUANDO OS VALORES DO MODELO É DIFERENTE DE "SELECIONAR". -->
                          <input type="hidden" id="emitir_nf" name="emitir_nf" value="false">

                          <div class="form-group" id="container_tipo_nf">
                            <label for="modelo_nf">Emitir Nota Fiscal</label>
                            <select class="form-control" id="modelo_nf" name="modelo_nf">
                              <option value="0" selected>Selecionar</option>
                              <option value="1">NF-e</option>
                              <option value="2">NFC-e Cupom</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="box-field">
                            <span class="box-field-title">Vendedor:</span>
                            <div class="row">
                              <div class="col-md-12">
                                <div class="form-group">
                                  <label>Nome:<span class="required">*</span></label>
                                  <!-- <input type="text" class="form-control" /> -->
                                  <input type="text" class="form-control" name="usuario_nome" id="usuario_nome" value="<?php echo $this->session->userdata('usuario_nome'); ?>" placeholder="Vendedor">
                                  <input type="hidden" id="usuario_id" name="usuario_id" value="<?php echo $this->session->userdata('usuario_id');  ?>">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div style="display: flex; align-items: center; justify-content: center;">
                        <button type="button" class="btn btn-primary btn-flat" id="adicionar_forma_pagamento">Adicionar forma de recebimento</button>
                      </div>

                      <input type="hidden" id="qtd_forma_pag" name="qtd_forma_pag" value="1">

                      <div class="row" style="margin-top: 30px;">
                        <div class="col-md-12">
                          <div class="box-field">
                            <span class="box-field-title">Forma de recebimento:</span>

                            <div class="row" style="padding: 3%;">


                              <div id="div-credito-cliente" style="display: none; width: 100%;">
                                <span style="display: flex; width: 100%; justify-content: center;">
                                  <strong>
                                     <span id="text-credito-cliente"></span>
                                     <input type="hidden" id="text-credito-cliente-divida">
                                  </strong>
                                </span>
                              </div>

                              <div id="formas_recebimento" style=" max-height: 300px; overflow: auto;">

                                <?php
                                $total = 0;
                                foreach ($produtos as $p) {
                                  $total += $p['subTotal'] ?>
                                <?php } ?>

                                <!--    FORMA DE PAGAMENTO INICIAL.     -->
                                <div class="forma-pagamento bottom-border" id="forma_pagamento1" style="margin-top: 20px;">

                                  <div class="form-group" style="display: flex;">
                                    <div class="date" style="width: 80%;">
                                      <label for="forma_pag1" class=" truncated-text">Forma de recebimento:<span class="required">*</span></label>
                                      <select class="form-control forma_pag" id="forma_pag1" name="forma_pag1">
                                        <option value="Dinheiro">Dinheiro</option>
                                        <option value="Pagamento Instantâneo (PIX)">Pagamento Instantâneo (PIX)</option>
                                        <option value="Cartão de Crédito">Cartão de Crédito</option>
                                        <option value="Cartão de Débito">Cartão de Débito</option>
                                         <option value="Boleto Bancário">Boleto Bancário</option>
                                        <option value="Crédito Loja">Crédito Loja</option>
                                        <option value="Sem pagamento">Sem pagamento</option>
                                        <option value="Outros">Outros</option>
                                      </select>
                                    </div>

                                    <div>
                                      <label for="forma_pag_valor1" class="truncated-text">Valor:<span class="required">*</span></label>
                                      <input type="text" class="form-control money forma_pag_valor" id="forma_pag_valor1" name="forma_pag_valor1" placeholder="R$ 0.00" value="<?= number_format($total, 2, '.', '');  ?>" />
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label for="data_vencimento1" class="truncated-text">Data de vencimento:<span class="required">*</span></label>
                                    <input type="text" class="form-control" id="data_vencimento1" name="data_vencimento1" />
                                  </div>


                                  <!-- <?= var_dump($cartoes) ?> -->
                                  <!--    DADOS DO CARTAO.     -->
                                  <div class="form-group" id="dados_cartao" style="display: none; padding: 3px;">
                                    <div>
                                      <label for="bandeira_cartao1" class="truncated-text">Bandeira:<span class="required">*</span></label>
                                      <select class="form-control bandeira_cartao" id="bandeira_cartao1" name="bandeira_cartao1">
                                        <option value="" selected>Bandeira</option>
                                        <?php foreach ($cartoes as $cartao) : ?>
                                          <option value="<?= $cartao->categoria_cart_descricao; ?>" id_bandeira="<?= $cartao->categoria_cart_id ?>" categoria_cart_numero_parcela="<?= $cartao->categoria_cart_numero_parcela ?>" categoria_cart_percentual_parcela="<?= $cartao->categoria_cart_percentual_parcela ?>">
                                            <?= $cartao->categoria_cart_numero_parcela . 'x - ' . $cartao->categoria_cart_descricao . ' - ' . $cartao->categoria_cart_nome_credenciadora ?>
                                          </option>
                                        <?php endforeach ?>
                                      </select>

                                      <input type="hidden" class="form-control id_bandeira" id="id_bandeira1" name="id_bandeira1">
                                      <input type="hidden" class="categoria_cart_numero_parcela" id="categoria_cart_numero_parcela1" name="categoria_cart_numero_parcela1">
                                      <input type="hidden" class="categoria_cart_percentual_parcela" id="categoria_cart_percentual_parcela1" name="categoria_cart_percentual_parcela1">

                                    </div>

                                    <div style="margin-top: 4px;">
                                      <label for="n_autorizacao_cartao1" class="truncated-text ">Autorização(NSU):</label>
                                      <input type="text" class="form-control" id="n_autorizacao_cartao1" name="n_autorizacao_cartao1" />
                                    </div>

                                  </div>

                                  <!--   /DADOS DO CARTAO.     -->
                                  <div class="btn-remover" style="margin-bottom: 5px;">

                                  </div>
                                </div>
                                <!--   /FORMA DE PAGAMENTO INICIAL.     -->

                                <!--    NOVAS FORMAS DE PAGAMENTO.     -->
                                <div id="forma_pagamento_nova">

                                </div>
                                <!--   /NOVAS FORMAS DE PAGAMENTO.     -->
                              </div>

                              <div class="form-group" style="display: flex;">
                                <!-- <div class="form-group">
                              <br>
                              <select class="form-control" id="tipo_compra">
                                <option value="atacado">Atacado</option>
                                <option value="varejo">Varejo</option>                       
                              </select>
                              </div> -->
                                <input type="hidden" id="tipo_compra" name="tipo_compra" value="atacado">

                                <div style="margin-top: 10px;">
                                  <div class="form-group">
                                    <label for="recebimento">Recebido?</label><br>
                                    <input type="checkbox" id="recebimento" name="recebimento" checked="checked" value="true">
                                  </div>
                                </div>

                                <div class="col-md-10" style="margin-top: 10px; width: 100%;">
                                  <div class="form-group" id="recebimentoDate">
                                    <div class="date">
                                      <label for="">Data de recebimento:<span class="required">*</span></label>
                                      <input type="text" class="form-control pull-right" id="datepickerRecebimentoDate" name="data_recebimento" placeholder="dd/mm/aaaa">
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div class="col-md-12" style=" display: flex; align-items: center; justify-content: center;">
                                <div class="value-full col-md-3">
                                  <strong>Valor Pago&lpar;<i id="box_valor"></i>&rpar;</strong>
                                </div>
                              </div>
                            </div>
                          </div>


                        </div>


                      </div>

                      <div class="row" style="display: flex; justify-content: space-evenly;">
                        <!-- <div class="col-md-3">
                          <div class="form-group">
                            <label>Desconto:</label>
                            <input type="tel" class="form-control money" id="desconto" name="desconto" placeholder="R$ 0,00" />
                            <p id="aviso_desconto" style="color: red; display: none;">Desconto Inválido!</p>
                          </div>
                        </div> -->
                        <div class="col-md-3">
                          <div class="form-group">
                            <label>Total Venda:</label>
                            <input type="text" class="form-control money" id="totalGeral" name="total_geral" value="<?php echo number_format($total, 2, '.', '');  ?>" disabled />
                          </div>
                        </div>
                        <!-- <div class="col-md-3">
                          <div class="form-group">
                            <label>Valor Pago:</label>
                          </div>
                        </div> -->
                        <input type="hidden" class="form-control money" id="valor_total" name="valor_total" value="<?php echo number_format($total, 2, '.', '');  ?>" />

                        <div class="col-md-3">
                          <div class="form-group">
                            <label>Troco:</label>
                            <input type="text" class="form-control" id="troco" name="troco" placeholder="R$ 0,00" disabled />
                            <center>
                              <p id="aviso_troco" style="display: none">
                                <font color="red">Troco Inválido!</font>
                              </p>
                            </center>
                          </div>
                        </div>
                      </div>

                      <div class="alert alert-error" id="alertaErro" style="margin-left: 0;display: none; font-size: 15px;"></div>

                      <div class="row">
                        <div class="col-md-12">

                          <div class="box-actions-venda" style="display: flex; justify-content: space-evenly;">
                            <button type="button" id="finalizarVendabnt" class="btn btn-success btn-flat">Confirmar venda &nbsp; <i class="fa fa-money"></i></button>
                            &nbsp;
                            <button type="button" onclick="location.href='<?php echo base_url(); ?>Pdvtablet2/editarVenda/<?php echo $this->uri->segment(3) ?>'" class="btn btn-danger btn-flat">Cancelar &nbsp; <i class="fa fa-refresh"></i></button>
                          </div>

                        </div>

                      </div>
                    </div>
                    <div class="col-md-5">
                      <div>
                        <table class="printer-ticket">
                          <thead>
                            <tr>
                              <th class="title" colspan="3"><?php echo $emitente[0]->emitente_nome ?></th>
                            </tr>
                            <tr>
                              <th colspan="3" id="data_nota"></th>
                            </tr>
                            <tr>
                              <th colspan="3">
                                Nome do cliente <br />
                                <!-- 000.000.000-00 -->
                              </th>
                            </tr>
                            <tr>
                              <th class="ttu" colspan="3">
                                <b>Cupom não fiscal</b>
                              </th>
                            </tr>
                          </thead>
                          <tbody>

                            <?php foreach ($produtos as $p) {  ?>
                              <tr class="top">
                                <td colspan="3"><?php echo $p['produto_descricao'] ?></td>
                              </tr>
                              <tr>
                                <td><?php echo 'R$' . $p['valor_unit'] ?></td>
                                <td><?php echo $p['quantidade'] ?></td>
                                <td><?php echo 'R$' . $p['subTotal'] ?></td>
                              </tr>
                            <?php  } ?>

                            <!-- 										  								<tr class="top">
																				  <td colspan="3">Doce de brigadeiro</td>
																			  </tr>
																			  <tr>
																				  <td>R$7,99</td>
																				  <td>2.0</td>
																				  <td>R$15,98</td>
																			  </tr>
																			  <tr>
																				  <td colspan="3">Opcional Adicicional: grande</td>
																			  </tr>
																			  <tr>
																				  <td>R$0,33</td>
																				  <td>2.0</td>
																				  <td>R$0,66</td>
																			  </tr> -->
                          </tbody>
                          <tfoot>
                            <!-- 									<tr class="sup ttu p--0">
																				  <td colspan="3">
																					  <b>Total</b>
																				  </td>
																			  </tr> -->
                            <!-- 									<tr class="ttu">
																				  <td colspan="2">Sub-total</td>
																				  <td align="right">R$43,60</td>
																			  </tr>
																			  <tr class="ttu">
																				  <td colspan="2">Taxa de serviço</td>
																				  <td align="right">R$4,60</td>
																			  </tr>
																			  <tr class="ttu">
																				  <td colspan="2">Desconto</td>
																				  <td align="right">5,00%</td>
																			  </tr> -->
                            <tr class="ttu">
                              <td colspan="2">Total</td>
                              <td align="right" id="valor_venda">R$<?php echo number_format($total, 2, '.', '');  ?></td>
                            </tr>
                            <!-- <tr class="sup ttu p--0">
											  <td colspan="3">
												  <b>Pagamentos</b>
											  </td>
										  </tr>
										  <tr class="ttu">
											  <td colspan="2">Voucher</td>
											  <td align="right">R$10,00</td>
										  </tr>
										  <tr class="ttu">
											  <td colspan="2">Dinheiro</td>
											  <td align="right">R$10,00</td>
										  </tr>
										  <tr class="ttu">
											  <td colspan="2">Total pago</td>
											  <td align="right">R$10,00</td>
										  </tr>
										  <tr class="ttu">
											  <td colspan="2">Troco</td>
											  <td align="right">R$0,44</td>
										  </tr>
										  <tr class="sup">
											  <td colspan="3" align="center">
												  <b>Pedido:</b>
											  </td>
										  </tr>
										  <tr class="sup">
											  <td colspan="3" align="center">
												  www.site.com
											  </td>
										  </tr> -->
                          </tfoot>
                        </table>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </section>

      </form>
    </div>
    <!-- /.content-wrapper -->


    <!-- /.content-wrapper -->
    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <b>Version</b> 1.0.0
      </div>
      <strong>Copyright &copy; 2019- <?php echo date('Y'); ?> <a href="www.wdmtecnologia.com.br">WDM-Tecnologia</a>.</strong> All rights
      reserved.
    </footer>

    <!-- VERIFICAR CONEXAO  -->
    <div id="taja-cnx-geral" class="taja-cnx-geral">
      <div id="taxa-cnx-off" class="taja-cnx-geral">
        <svg viewBox="0 0 24 24" id="svg-cnx-off" height="24" width="24">
          <path d="M23.7805.2195c.2925.293.2925.768 0 1.061l-22.5 22.5C1.134 23.927.942 24 .75 24c-.192 0-.384-.073-.5305-.2195-.2925-.293-.2925-.768 0-1.061l12.1967947-12.1971509c-2.3773758-.1150468-4.79162015.7286756-6.6038447 2.5401009-.3905.3905-1.0235.3905-1.414 0-.3905-.3905-.3905-1.024 0-1.414 2.65728981-2.65728981 6.3696459-3.62280965 9.8005664-2.90852144l2.430738-2.43186193C11.7816792 4.58111626 6.15198938 5.65271062 2.27735 9.52735c-.3905.3905-1.0235.3905-1.414 0-.3905-.3905-.3905-1.0235 0-1.414 4.6714534-4.67191574 11.571522-5.78608437 17.3096457-3.34843552L22.7195.2195c.293-.2925.768-.2925 1.061 0zM12 18.5c.6905 0 1.25.5595 1.25 1.25S12.6905 21 12 21s-1.25-.5595-1.25-1.25.5595-1.25 1.25-1.25zm1.4175-4.81495c.9705.2455 1.8905.741 2.6485 1.499.3905.3905.3905 1.0235 0 1.414-.1955.1955-.451.293-.707.293-.256 0-.512-.0975-.707-.293-.7835333-.7835333-1.8301422-1.1445778-2.8581093-1.0880116L11.574 15.52855l1.8435-1.8435zm3.8154-3.8154c.848.4725 1.649 1.059 2.3685 1.779.391.39.391 1.023 0 1.414-.195.195-.451.293-.707.293-.2555 0-.5115-.098-.707-.293-.7285-.728-1.5575-1.291-2.439-1.7085zm2.1908-2.1908l1.4425-1.4425c.8.545 1.5615 1.168 2.2705 1.877.3905.3905.3905 1.0235 0 1.414-.1955.1955-.451.293-.707.293-.256 0-.5115-.0975-.707-.293-.5933333-.59333333-1.2283333-1.11861111-1.895162-1.57959491L19.4237 7.67885l1.4425-1.4425z">
          </path>
        </svg>
        <span>Você está offline no momento. </span> <a href="" id="btn-atualizar-cnx">Atualizar</a>
      </div>
      <div id="taxa-cnx-on" class="taja-cnx-geral">
        <svg width="24" id="svg-cnx-on" height="24" viewBox="0 0 24 24">
          <path d="M8.213 16.984c.97-1.028 2.308-1.664 3.787-1.664s2.817.636 3.787 1.664l-3.787 4.016-3.787-4.016zm-1.747-1.854c1.417-1.502 3.373-2.431 5.534-2.431s4.118.929 5.534 2.431l2.33-2.472c-2.012-2.134-4.793-3.454-7.864-3.454s-5.852 1.32-7.864 3.455l2.33 2.471zm-4.078-4.325c2.46-2.609 5.859-4.222 9.612-4.222s7.152 1.613 9.612 4.222l2.388-2.533c-3.071-3.257-7.313-5.272-12-5.272s-8.929 2.015-12 5.272l2.388 2.533z" />
        </svg> <span>Sua conexão com a internet foi restaurada.</span>
      </div>
    </div>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark" id="control-sidebar">
      <!-- Create the tabs -->
      <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
        <li class="active"><a href="#control-sidebar-theme-demo-options-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
        <!-- <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li> -->
        <!-- <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li> -->
      </ul>
      <!-- Tab panes -->
      <div class="tab-content">
        <div id="control-sidebar-theme-demo-options-tab" class="tab-pane active">
          <div>
            <h3 class="control-sidebar-heading">Filias</h3>
            <div class="tab-pane active" id="control-sidebar-home-tab">
              <ul class="control-sidebar-menu">

                <?php foreach ($filias as $fili) {

                  $f = $fili->schema_name;
                  $filias = explode("_" . GRUPOLOJA . "_", $f);

                  echo '<li>
                      <a href="#" class="filias' . ($fili->schema_name == BDCAMINHO ? ' active' : '') . '" id="' . $fili->schema_name . '" datacor="' . $fili->cor . '"><i class="menu-icon fa fa-folder-open bg-' . $fili->cor . '"></i>
                        <div class="menu-info">
                          <h4 class="control-sidebar-subheading">' . strtoupper($filias[1]) . '</h4>
                        </div>
                      </a>
                    </li>';
                }
                ?>
              </ul>
            </div>
          </div>
        </div>
        <!--       <div class="tab-pane" id="control-sidebar-settings-tab">
          <h3 class="control-sidebar-heading">Categorias</h3>
          <div class="tab-pane active" id="control-sidebar-home-tab">
          <ul class="control-sidebar-menu" id='categorias_prod_filias'>
            <?php
            foreach ($categorias as $category) {
              $filias = explode("_" . GRUPOLOJA . "_", BDCAMINHO);
              echo '<li><a href="#" class="category' . ($category->categoria_prod_id == '1' ? ' active' : '') . '" id="' . $category->categoria_prod_id . '">';
              if ($filias[1] == 'matriz') {
                echo '<div class="menu-icon"><img src="' . base_url('assets/img/matriz.png') . '" alt="" class="img-thumbnail img-circle img-responsive"></div>';
              } elseif ($filias[1] == 'game') {
                echo '<div class="menu-icon"><img src="' . base_url('assets/img/game.png') . '" alt="" class="img-thumbnail img-circle img-responsive"></div>';
              } elseif ($filias[1] == 'carlos') {
                echo '<div class="menu-icon"><img src="' . base_url('assets/img/carlos.png') . '" alt="" class="img-thumbnail img-circle img-responsive"></div>';
              } elseif ($filias[1] == 'mega') {
                echo '<div class="menu-icon"><img src="' . base_url('assets/img/mega.png') . '" alt="" class="img-thumbnail img-circle img-responsive"></div>';
              } elseif ($filias[1] == 'paloma') {
                echo '<div class="menu-icon"><img src="' . base_url('assets/img/paloma.png') . '" alt="" class="img-thumbnail img-circle img-responsive"></div>';
              } else {
                echo '<i class="menu-icon fa fa-folder-open bg-red"></i>';
              }
              echo '<div class="menu-info"><h4 class="control-sidebar-subheading">' . $category->categoria_prod_id . '</h4><p><strong>' . $category->categoria_prod_descricao . '</strong></p></div>
            </a></li>';
            }
            ?>
        </ul>
      </div>
      </div> -->
        <!-- /.tab-pane -->
      </div>
    </aside>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

  <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title" id="mySmallModalLabel">Alterar Senha</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal">
            <div class="box-body">
              <div class="form-group">
                <label for="inputEmail3" class="">Senha Atual</label>
                <input class="form-control" id="senha_atual" type="password">
              </div>
              <div class="form-group">
                <label for="inputPassword3" class="">Nova Senha</label>
                <input class="form-control" id="nova_senha" type="password">
              </div>
              <div class="form-group">
                <label for="inputPassword3" class="">Confirma Senha</label>
                <input class="form-control" id="confirma_senha" type="password">
              </div>
            </div>
            <span class="msg-error text-danger"></span>
            <span class="msg-success text-success"></span>
            <!-- /.box-body -->
            <div class="box-footer">
              <span id="alterarSenha" class="btn btn-primary pull-right">Alterar Senha</span>
            </div>
            <!-- /.box-footer -->
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" data-easein="flipYIn" id="customerModal" tabindex="-1" role="dialog" aria-labelledby="cModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Adicionar Cliente </h4>
        </div>
        <div class="modal-body">
          <div id="c-alert" class="alert alert-danger" style="display:none;"></div>
          <div class="row">
            <div class="col-xs-12">
              <div class="form-group">
                <label class="control-label" for="cliente_nomeModal">Nome*</label>
                <input type="text" name="cliente_nomeModal" id="cliente_nomeModal" value="" class="form-control input-sm kb-text">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-6">
              <div class="form-group">
                <label class="control-label" for="cliente_email">Endereço de e-mail</label>
                <input type="text" name="cliente_email" id="cliente_email" value="" class="form-control input-sm kb-text">
              </div>
            </div>
            <div class="col-xs-6">
              <div class="form-group">
                <label class="control-label" for="cliente_numero">Telefone</label>
                <input type="text" name="cliente_numero" id="cliente_numero" value="" class="form-control input-sm kb-pad">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-6">
              <div class="form-group">
                <label class="control-label" for="tipo">Tipo</label>
                <select required class="form-control input-sm kb-pad" name="cliente_tipo" id="tipo">
                  <option value='1'>Pessoa Física</option>
                  <option value='2'>Pessoa Jurídica</option>
                </select>
              </div>
            </div>
            <div class="col-xs-6">
              <div class="form-group">
                <label class="control-label" for="cpfCnpj">CPF/CNPJ*</label>
                <input type="text" name="cpfCnpj" id="cpfCnpj" value="" class="form-control input-sm kb-text">
              </div>
            </div>

          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Fechar</button>
          <button type="button" class="btn btn-success" id='addCliente'>Adicionar</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade historico" data-easein="flipYIn" tabindex="-1" role="dialog" aria-labelledby="cModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Histórico de Venda (<?php echo $this->session->userdata('usuario_nome'); ?>) </h4>
        </div>
        <div class="modal-body">
          <table class="stable" width="100%">
            <tbody>
              <?php $total = 0;
              foreach ($historico as $h) {
                $total += $h['total'] ?>

                <tr>
                  <td style="border-bottom: 1px solid #EEE;">
                    <h4><?php echo $h['forma_pgto'];  ?>:</h4>
                  </td>
                  <td style="text-align:right; border-bottom: 1px solid #EEE;">
                    <h4>
                      <span><?php echo $h['total'];  ?></span>
                    </h4>
                  </td>
                </tr>
              <?php  }   ?>

              <tr>
                <td style="font-weight:bold;" width="300px;">
                  <h4><strong>Total em Vendas</strong>:</h4>
                </td>
                <td style="text-align:right;">
                  <h4>
                    <span><strong><?php echo $total; ?></strong></span>
                  </h4>
                </td>
              </tr>
            </tbody>
          </table>

        </div>
      </div>
    </div>
  </div>
  <div class="modal fade rank" data-easein="flipYIn" tabindex="-1" role="dialog" aria-labelledby="cModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Ranck do Vendedores</h4>
        </div>
        <div class="modal-body">
          <table class="stable" width="100%">
            <tbody>
              <tr>
                <th>Vendendor</th>
                <th>Total</th>
              </tr>
              <?php $contador = 1;
              foreach ($vendedores as $v) { ?>

                <tr>
                  <td style="border-bottom: 1px solid #EEE;">
                    <h4><?php echo '#' . $contador . ' - ' . $v['vendedor'];  ?>:</h4>
                  </td>
                  <td style="text-align:left; border-bottom: 1px solid #EEE;">
                    <h4>
                      <span><?php echo $v['total'];  ?></span>
                    </h4>
                  </td>
                </tr>
              <?php $contador++;
              }   ?>

            </tbody>
          </table>

        </div>
      </div>
    </div>
  </div>

  <!-- Modal Excluir -->
  <div class="example-modal">
    <div class="modal" id="excluirVenda">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Sair</h4>
          </div>
          <div class="modal-body">
            <p>Deseja realmente sair ? Existe uma venda em aberto.</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
            <a onclick="sairVendaDashboard()" class="btn btn-danger" id="excluirVendaId">Sair</a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal Senha Gerente -->
  <div class="modal fade" id="senha-gerente" data-backdrop="static" data-keyboard="false" style="z-index: 99999" tabindex="-1" role="dialog">
    <div class="modal-dialog" style="width: 350px; margin: 100px auto">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" onclick="closeModal()" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title text-center">Informar senha</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <p>Informe a senha do gerente para liberação desta ação</p>
              <div class="form-group">
                <input type="password" onkeyup="habilitarBTN(this.value)" id="input-liberar-senha" class="form-control" placeholder="Senha do gerente" />
              </div>
              <button id="btn-confirm-senha" disabled class="btn btn-primary btn-block">CONFIRMAR</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<!-- Modal Senha Gerente -->
<div class="modal fade" id="senha-gerente-divida" data-backdrop="static" data-keyboard="false" style="z-index: 99999" tabindex="-1" role="dialog">
  <div class="modal-dialog" style="width: 350px; margin: 100px auto">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" onclick="closeModalDivida()" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center">Informar senha</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <p>Informe a senha do gerente para liberação desta ação</p>
            <div class="form-group">
              <input type="password" onkeyup="habilitarBTNDivida(this.value)" id="input-liberar-senha-divida" class="form-control" placeholder="Senha do gerente" />
            </div>
            <button id="btn-confirm-senha-divida" disabled class="btn btn-primary btn-block">CONFIRMAR</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

  <script type="text/javascript">
    var base_url = "<?php echo base_url(); ?>";
  </script>

  <!-- jQuery 3 -->
  <!-- <script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script> --><!-- Buga o Autocomplete -->
  <!-- Bootstrap 3.3.7 -->
  <script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <!-- SlimScroll -->
  <script src="<?php echo base_url(); ?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
  <!-- FastClick -->
  <script src="<?php echo base_url(); ?>assets/bower_components/fastclick/lib/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="<?php echo base_url(); ?>assets/dist/js/adminlte.min.js"></script>
  <!--Date picker-->
  <script src="<?php echo base_url(); ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/bower_components/moment/min/moment.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
  <script src="<?php echo base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

  <script src="<?php echo base_url(); ?>/assets/dist/js/jquery-ui.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>
  <script src="<?php echo base_url(); ?>assets/dist/js/actions-pdvtablet2.js"></script>
  <script src="<?php echo base_url(); ?>assets/dist/js/jquery.validate.js"></script>
  <script src="<?php echo base_url(); ?>assets/dist/js/maskmoney.js"></script>


  <script src="<?php echo base_url(); ?>assets/dist/js/verificar-conexao.js"></script>

  <script type="text/javascript">
    $('#box_valor').text($('#valor_total').val());

    $('#data_nota').text(moment().format('DD/MM/YYYY'));

    $('#datepicker').datepicker({
      autoclose: true,
      format: 'dd/mm/yyyy',
    });
    $('#datepicker').datepicker('setDate', new Date());

    $(document).ready(function() {

      // $('#cliente_id').val() != 1 ? $('#div-credito-cliente').show() : $('#div-credito-cliente').hide();

      $(".money").maskMoney();
      $("#cliente_nome").focus();
      $('#datepickerRecebimentoDate').datepicker({
        autoclose: true,
        format: 'dd/mm/yyyy',
      });
      $('#datepickerRecebimentoDate').datepicker('setDate', new Date());

      $('#data_vencimento1').datepicker({
        autoclose: true,
        format: 'dd/mm/yyyy',
      });
      $('#data_vencimento1').datepicker('setDate', new Date());
    });

    $(function() {
      //Initialize Select2 Elements
      $('.select2').select2()
      // $('#control-sidebar').addClass('control-sidebar-open')
      // $('#control-sidebar').onBlur().removeClass('control-sidebar-open')
    });

    function hideSideBar() {
      $('#control-sidebar').removeClass('control-sidebar-open')
    }


    $('#grupo_prod').focus().select()
  </script>

  <script type="text/javascript">
    /**  
 noback v0.0.1 
 library for prevent backbutton 
 Author: Kiko Mesquita: http://twitter.com/kikomesquita 
 Based on stackoverflow 
 * Copyright (c) 2015 @ kikomesquita 
*/

    (function(window) {
      'use strict';

      var noback = {

        //globals 
        version: '0.0.1',
        history_api: typeof history.pushState !== 'undefined',

        init: function() {
          window.location.hash = '#no-back';
          noback.configure();
        },

        hasChanged: function() {
          if (window.location.hash == '#no-back') {
            window.location.hash = '#BLOQUEIO';
            //mostra mensagem que não pode usar o btn volta do browser
            if ($("#msgAviso").css('display') == 'none') {
              $("#msgAviso").slideToggle("slow");
            }
          }
        },

        checkCompat: function() {
          if (window.addEventListener) {
            window.addEventListener("hashchange", noback.hasChanged, false);
          } else if (window.attachEvent) {
            window.attachEvent("onhashchange", noback.hasChanged);
          } else {
            window.onhashchange = noback.hasChanged;
          }
        },

        configure: function() {
          if (window.location.hash == '#no-back') {
            if (this.history_api) {
              history.pushState(null, '', '#BLOQUEIO');
            } else {
              window.location.hash = '#BLOQUEIO';
              //mostra mensagem que não pode usar o btn volta do browser
              if ($("#msgAviso").css('display') == 'none') {
                $("#msgAviso").slideToggle("slow");
              }
            }
          }
          noback.checkCompat();
          noback.hasChanged();
        }

      };

      // AMD support 
      if (typeof define === 'function' && define.amd) {
        define(function() {
          return noback;
        });
      }
      // For CommonJS and CommonJS-like 
      else if (typeof module === 'object' && module.exports) {
        module.exports = noback;
      } else {
        window.noback = noback;
      }
      noback.init();
    }(window));

    function habilitarBTN(valor) {
      const el = document.querySelector('#btn-confirm-senha')
      if (valor.length > 0) {
        el.disabled = false
      } else {
        el.disabled = true
      }
    }

    function closeModal() {
      $('#senha-gerente').modal('hide');
      document.querySelector('#input-liberar-senha').value = null;
      const el = document.querySelector('#btn-confirm-senha')
      el.disabled = true
    }

    $('#btn-confirm-senha').click(function(event) {
      let senha = $('#input-liberar-senha').val();
      $.ajax({
        method: "POST",
        url: base_url + "Pdvtablet/validacaoGerente/",
        dataType: "JSON",
        data: {
          senha
        },
        success: function(data) {

          if (data.result == true) {
            $('#datepicker').removeAttr('disabled');
            closeModal();
          } else {
            alert('Senha Invalida.');
            location.reload();
          }
        }
      });
    });
  </script>


</body>

</html>