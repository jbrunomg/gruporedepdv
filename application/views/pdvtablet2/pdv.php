<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo strtoupper(SUBDOMINIO) ?> | Controler</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/main.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/verificar-conexao.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!-- ADD THE CLASS sidebar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->

<body class="hold-transition skin-<?php echo CLIENTE ?> sidebar-collapse sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">

    <header class="main-header">
      <!-- Logo -->
      <a href="<?php echo base_url(); ?>sistema/dashboard" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>P</b>DV</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>M</b>-Controler</span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- Messages: style can be found in dropdown.less-->
            <li><a href="#" id="filialAtual" name="filialAtual"><?php $loja = BDCAMINHO;
                                                                $loja = explode("_", $loja);
                                                                echo strtoupper($loja[2]); ?></a></li>

            <!--   <li><a href="#" class="clock"></a></li>
          <li><a href="http://localhost/sistemapdv/"><i class="fa fa-dashboard"></i></a></li>
          <li><a href="http://localhost/sistemapdv/settings"><i class="fa fa-cogs"></i></a></li>
          <li><a href="http://localhost/sistemapdv/pos/view_bill" target="_blank"><i class="fa fa-file-text-o"></i></a></li> -->


            <li><a title="" href="javascript:void(0);" onclick="fullScreen();"><i class="fa fa-arrows-alt"></i></a></li>

            <li><a href="#" data-toggle="modal" data-target=".historico">Histórico de Vendas</a></li>
            <li><a href="#" data-toggle="modal" data-target=".rank">Ranck do Vendedores</a></li>
            <!-- <li><a href="http://localhost/sistemapdv/pos/close_register" data-toggle="ajax">Fechar Caixa</a></li> -->

            <li class="dropdown notifications-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bell-o"></i>
                <span class="label label-warning">1</span>
              </a>
              <ul class="dropdown-menu">
                <li class="header">Fluxo de caixa</li>
                <li>
                  <ul class="menu">
                    <li>
                      <a href="<?php echo base_url() ?>relatorios/financeiro/dia" class="load_suspended">
                        <strong>Caixa dia</strong><br><?php echo date('d/m/Y'); ?></a>

                      <a href="#" onClick="(function(){window.open('<?php echo base_url(); ?>financeiro/visualizarCaixaDia/dia','MyWindow','toolbar=no,location=no,directories=no,status=no,menubar=yes,scrollbars=yes,resizable=yes,width=350,height=600');return false;})();return false;" data-toggle="tooltip" title="Visualizar"><i class="fa fa-search  text-success"></i><strong>Caixa dia</strong><br><?php echo date('d/m/Y'); ?> </a>

                    </li>
                  </ul>
                </li>
                <li class="footer"><a href="<?php echo base_url() ?>relatorios/financeiro">Ver tudo</a></li>
              </ul>
            </li>

            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="<?php echo base_url(); ?>assets/dist/img/user2-160x160.jpg" class="user-image" alt="Avatar" />
                <span class="hidden-xs"><?php echo $this->session->userdata('usuario_nome'); ?></span>
              </a>
              <ul class="dropdown-menu">
                <li class="user-header">
                  <img src="<?php echo base_url(); ?>assets/dist/img/user2-160x160.jpg" class="img-circle" alt="Avatar" />
                  <p>
                    <?php echo $this->session->userdata('usuario_nome'); ?> <br> <?php echo $this->session->userdata('usuario_perfil_nome'); ?>
                  </p>
                </li>
                <li class="user-footer">
                  <div class="pull-left">
                    <a href="#" class="btn btn-default btn-flat" data-toggle="modal" data-target=".bs-example-modal-sm">Alterar Senha</a>
                  </div>
                  <div class="pull-right">
                    <a href="<?php echo base_url(); ?>sistema/logout" class="btn btn-default btn-flat">Sair</a>
                  </div>
                </li>
              </ul>
            </li>
            <!-- Control Sidebar Toggle Button -->
            <!--          <li>-->
            <!--            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>-->
            <!--          </li>-->
          </ul>
        </div>
      </nav>
    </header>

    <!-- =============================================== -->

    <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url(); ?>assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $this->session->userdata('usuario_nome'); ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
            </button>
          </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU NAVEGAÇÃO</li>


        <!-- <li><a href="<?php echo base_url(); ?>sistema/dashboard"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>	
        <li><a href="<?php echo base_url(); ?>pdv2"><i class="fa fa-shopping-cart"></i> <span>PDV</span></a></li> -->
        <!-- <li class="header">LABELS</li> -->
        <li><a onclick="iniciarVenda()" id="btn-iniciar-venda"><i class="fa fa-circle-o text-blue"></i> <span>Iniciar Venda</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Excluir Item</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Cancelar Venda</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Finalizar Venda</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Sair</span></a></li>

      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!--     <section class="content-header">
      <h1>
        Sidebar Collapsed
        <small>Layout with collapsed sidebar on load</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Layout</a></li>
        <li class="active">Collapsed Sidebar</li>
      </ol>
    </section> -->

    <!-- Main content -->
    <section class="content">
      <div class="col-lg-12 alerts">
        <!-- Tratamento de mensagens do sitema -->
        <?php if ($this->session->flashdata('error')) { ?>
          <div class="col-md-12">
            <div class="alert alert-danger alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h5><?php echo $this->session->flashdata('error'); ?></h5>
            </div>
          </div>
        <?php } else if ($this->session->flashdata('success')) { ?>
          <div class="col-md-12">
            <div class="alert alert-success alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h5></i> <?php echo $this->session->flashdata('success'); ?></h5>
            </div>
          </div>
        <?php } ?>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="info-box p-m">
              <div class="row">
                <div class="col-md-10">
                  <div class="form-group">
                    <label>Descrição do Produto:</label>
                    <select class="select2" style="width: 100%;" name="produto_desc" id="produto_desc" disabled>
                      <option value="">Selecione</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label>Estoque:</label>
                    <div class="info-box bg-aqua" style="color: #fff; font-weight: bold; font-size: 16px; text-align: right; padding: 6px; min-height: 100% !important;">
                      <span id="estoque">0</span>
                    </div>
                  </div>
                </div>
              </div>
              <hr />
              <div class="row">
                <div class="col-md-4">
                  <form>
                    <div class="form-group">
                      <label for="quantidade">Quantidade:</label>
                      <input type="text" class="text-right form-control" id="quantidade" placeholder="0,00" disabled>
                    </div>
                    <div class="form-group">
                      <label for="valor-unit">Valor unitário:</label>
                      <input type="text" class="text-right form-control money" id="valor_unit" value="" name="valor_unit" placeholder="R$0,00" disabled>
                      <div class="row">
                        <div class="col-sm-4">
                          <span>PVM: R$ <span id="pvm"></span></span>
                        </div>
                        <div class="col-sm-4">
                          <span>PCD: R$ <span id="pcd"></span></span>
                        </div>
                        <div class="col-sm-4">
                          <span>PCC: R$ <span id="pcc"></span></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="total-item">Total Item:</label>
                      <input type="text" class="text-right form-control" id="total-item" placeholder="R$0,00" disabled>
                    </div>
                    <div class="form-group">
                      <label for="total-venda">Total Venda:</label>
                      <input disabled type="text" class="custon-input-venda text-right form-control" id="total-venda" placeholder="R$0,00">
                    </div>
                  </form>
                </div>
                <div class="col-md-8 mt-s">
                  <div class="box-table">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th style="width: 10px">N</th>
                          <th>Grupo</th>
                          <th>Código</th>
                          <th width="40%">Descrição</th>
                          <th>QTD</th>
                          <th>Valor</th>
                          <th>Total Item</th>
                        </tr>
                      </thead>
                      <tbody>
                        <!--	<tr>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
											</tr> -->
                      </tbody>
                    </table>
                  </div>
                  <div class="box-qtd">
                    <span class="text-qtd">QTDE ITENS: 0</span>
                  </div>
                </div>
              </div>
              <hr />
              <div class="row">
                <div class="col-md-12">
                  <div class="box-btns">
                    <button onclick="iniciarVenda()" id="btn-iniciar-venda" class="btn-custon btn btn-primary btn-flat">
                      <span>F2 - Iniciar venda &nbsp; <i class="fa fa-plus-square"></i></span>
                    </button>
                    <button disabled id="btn-excluir-item" class="btn-custon btn btn-danger btn-flat">
                      <span>F3 - Excluir Item &nbsp; <i class="fa fa-trash-o"></i></span>
                    </button>
                    <button disabled id="btn-cancelar-venda" class="btn-custon btn btn-info btn-flat">
                      <span>F4 - Cancelar venda &nbsp; <i class="fa fa-undo"></i></span>
                    </button>
                    <button disabled id="btn-finalizar-venda" class="btn-custon btn btn-warning btn-flat">
                      <span>F5 - Finalizar venda &nbsp; <i class="fa fa-save"></i></span>
                    </button>
                    <button disabled id="btn-sair" class="btn-custon btn btn-danger btn-flat">
                      <span>F10 - Sair &nbsp; <i class="fa fa-sign-out"></i></span>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>

  <!-- /.content-wrapper -->



  <!--  MODAL 	-->
  <div class="modal fade" id="modal-default">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Finalizar Venda</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-7">
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Data:</label>
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right" id="datepicker">
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Número Venda:</label>
                    <input type="text" class="form-control">
                  </div>
                </div>
                <div class="col-md-5">
                  <div class="form-group">
                    <label>Cliente:</label>
                    <select class="form-control">
                      <option>Cliente 1</option>
                      <option>Cliente 2</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="box-field">
                    <span class="box-field-title">Vendedor:</span>
                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Código:</label>
                          <select class="form-control">
                            <option selected></option>
                            <option>Código 1</option>
                            <option>Código 2</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-9">
                        <div class="form-group">
                          <label>Nome:</label>
                          <input type="text" class="form-control" />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="box-field">
                    <span class="box-field-title">Forma de recebimento:</span>
                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Código:</label>
                          <select class="form-control">
                            <option selected></option>
                            <option>Código 1</option>
                            <option>Código 2</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Desconto:</label>
                    <input type="text" class="form-control" placeholder="% 0,00" />
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Total Venda:</label>
                    <input type="text" class="form-control" placeholder="R$ 0,00" />
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Valor Pago:</label>
                    <input type="text" class="form-control" placeholder="R$ 0,00" />
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-8">

                  <div class="box-actions-venda">
                    <button class="btn btn-success btn-flat">Confirmar venda &nbsp; <i class="fa fa-money"></i></button>
                    &nbsp;
                    <button class="btn btn-danger btn-flat">Cancelar &nbsp; <i class="fa fa-refresh"></i></button>
                  </div>

                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Troco:</label>
                    <input type="text" class="form-control" placeholder="R$ 0,00" />
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-5">
              <div>
                <table class="printer-ticket">
                  <thead>
                    <tr>
                      <th class="title" colspan="3">WDM Tecnologia</th>
                    </tr>
                    <tr>
                      <th colspan="3">07/10/2019 - 22:01:52</th>
                    </tr>
                    <tr>
                      <th colspan="3">
                        Nome do cliente <br />
                        000.000.000-00
                      </th>
                    </tr>
                    <tr>
                      <th class="ttu" colspan="3">
                        <b>Cupom não fiscal</b>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr class="top">
                      <td colspan="3">Doce de brigadeiro</td>
                    </tr>
                    <tr>
                      <td>R$7,99</td>
                      <td>2.0</td>
                      <td>R$15,98</td>
                    </tr>
                    <tr>
                      <td colspan="3">Opcional Adicicional: grande</td>
                    </tr>
                    <tr>
                      <td>R$0,33</td>
                      <td>2.0</td>
                      <td>R$0,66</td>
                    </tr>
                  </tbody>
                  <tfoot>
                    <tr class="sup ttu p--0">
                      <td colspan="3">
                        <b>Totais</b>
                      </td>
                    </tr>
                    <tr class="ttu">
                      <td colspan="2">Sub-total</td>
                      <td align="right">R$43,60</td>
                    </tr>
                    <tr class="ttu">
                      <td colspan="2">Taxa de serviço</td>
                      <td align="right">R$4,60</td>
                    </tr>
                    <tr class="ttu">
                      <td colspan="2">Desconto</td>
                      <td align="right">5,00%</td>
                    </tr>
                    <tr class="ttu">
                      <td colspan="2">Total</td>
                      <td align="right">R$45,56</td>
                    </tr>
                    <tr class="sup ttu p--0">
                      <td colspan="3">
                        <b>Pagamentos</b>
                      </td>
                    </tr>
                    <tr class="ttu">
                      <td colspan="2">Voucher</td>
                      <td align="right">R$10,00</td>
                    </tr>
                    <tr class="ttu">
                      <td colspan="2">Dinheiro</td>
                      <td align="right">R$10,00</td>
                    </tr>
                    <tr class="ttu">
                      <td colspan="2">Total pago</td>
                      <td align="right">R$10,00</td>
                    </tr>
                    <tr class="ttu">
                      <td colspan="2">Troco</td>
                      <td align="right">R$0,44</td>
                    </tr>
                    <tr class="sup">
                      <td colspan="3" align="center">
                        <b>Pedido:</b>
                      </td>
                    </tr>
                    <tr class="sup">
                      <td colspan="3" align="center">
                        www.site.com
                      </td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>


  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2019- <?php echo date('Y'); ?> <a href="www.wdmtecnologia.com.br">WDM-Tecnologia</a>.</strong> All rights
    reserved.
  </footer>

  <!-- VERIFICAR CONEXAO  -->
  <div id="taja-cnx-geral" class="taja-cnx-geral">
    <div id="taxa-cnx-off" class="taja-cnx-geral">
      <svg viewBox="0 0 24 24" id="svg-cnx-off" height="24" width="24">
        <path d="M23.7805.2195c.2925.293.2925.768 0 1.061l-22.5 22.5C1.134 23.927.942 24 .75 24c-.192 0-.384-.073-.5305-.2195-.2925-.293-.2925-.768 0-1.061l12.1967947-12.1971509c-2.3773758-.1150468-4.79162015.7286756-6.6038447 2.5401009-.3905.3905-1.0235.3905-1.414 0-.3905-.3905-.3905-1.024 0-1.414 2.65728981-2.65728981 6.3696459-3.62280965 9.8005664-2.90852144l2.430738-2.43186193C11.7816792 4.58111626 6.15198938 5.65271062 2.27735 9.52735c-.3905.3905-1.0235.3905-1.414 0-.3905-.3905-.3905-1.0235 0-1.414 4.6714534-4.67191574 11.571522-5.78608437 17.3096457-3.34843552L22.7195.2195c.293-.2925.768-.2925 1.061 0zM12 18.5c.6905 0 1.25.5595 1.25 1.25S12.6905 21 12 21s-1.25-.5595-1.25-1.25.5595-1.25 1.25-1.25zm1.4175-4.81495c.9705.2455 1.8905.741 2.6485 1.499.3905.3905.3905 1.0235 0 1.414-.1955.1955-.451.293-.707.293-.256 0-.512-.0975-.707-.293-.7835333-.7835333-1.8301422-1.1445778-2.8581093-1.0880116L11.574 15.52855l1.8435-1.8435zm3.8154-3.8154c.848.4725 1.649 1.059 2.3685 1.779.391.39.391 1.023 0 1.414-.195.195-.451.293-.707.293-.2555 0-.5115-.098-.707-.293-.7285-.728-1.5575-1.291-2.439-1.7085zm2.1908-2.1908l1.4425-1.4425c.8.545 1.5615 1.168 2.2705 1.877.3905.3905.3905 1.0235 0 1.414-.1955.1955-.451.293-.707.293-.256 0-.5115-.0975-.707-.293-.5933333-.59333333-1.2283333-1.11861111-1.895162-1.57959491L19.4237 7.67885l1.4425-1.4425z">
        </path>
      </svg>
      <span>Você está offline no momento. </span> <a href="" id="btn-atualizar-cnx">Atualizar</a>
    </div>
    <div id="taxa-cnx-on" class="taja-cnx-geral">
      <svg width="24" id="svg-cnx-on" height="24" viewBox="0 0 24 24">
        <path d="M8.213 16.984c.97-1.028 2.308-1.664 3.787-1.664s2.817.636 3.787 1.664l-3.787 4.016-3.787-4.016zm-1.747-1.854c1.417-1.502 3.373-2.431 5.534-2.431s4.118.929 5.534 2.431l2.33-2.472c-2.012-2.134-4.793-3.454-7.864-3.454s-5.852 1.32-7.864 3.455l2.33 2.471zm-4.078-4.325c2.46-2.609 5.859-4.222 9.612-4.222s7.152 1.613 9.612 4.222l2.388-2.533c-3.071-3.257-7.313-5.272-12-5.272s-8.929 2.015-12 5.272l2.388 2.533z" />
      </svg> <span>Sua conexão com a internet foi restaurada.</span>
    </div>
  </div>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-theme-demo-options-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <!-- <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li> -->
      <!-- <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li> -->
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <div id="control-sidebar-theme-demo-options-tab" class="tab-pane active">
        <div>
          <h3 class="control-sidebar-heading">Filias</h3>
          <div class="tab-pane active" id="control-sidebar-home-tab">
            <ul class="control-sidebar-menu">

              <?php foreach ($filias as $fili) {

                $f = $fili->schema_name;
                $filias = explode("_" . GRUPOLOJA . "_", $f);

                echo '<li>
                      <a href="#" class="filias' . ($fili->schema_name == BDCAMINHO ? ' active' : '') . '" id="' . $fili->schema_name . '" datacor="' . $fili->cor . '"><i class="menu-icon fa fa-folder-open bg-' . $fili->cor . '"></i>
                        <div class="menu-info">
                          <h4 class="control-sidebar-subheading">' . strtoupper($filias[1]) . '</h4>
                        </div>
                      </a>
                    </li>';
              }
              ?>
            </ul>
          </div>
        </div>
      </div>
      <!--       <div class="tab-pane" id="control-sidebar-settings-tab">
          <h3 class="control-sidebar-heading">Categorias</h3>
          <div class="tab-pane active" id="control-sidebar-home-tab">
          <ul class="control-sidebar-menu" id='categorias_prod_filias'>
            <?php
            foreach ($categorias as $category) {
              $filias = explode("_" . GRUPOLOJA . "_", BDCAMINHO);
              echo '<li><a href="#" class="category' . ($category->categoria_prod_id == '1' ? ' active' : '') . '" id="' . $category->categoria_prod_id . '">';
              if ($filias[1] == 'matriz') {
                echo '<div class="menu-icon"><img src="' . base_url('assets/img/matriz.png') . '" alt="" class="img-thumbnail img-circle img-responsive"></div>';
              } elseif ($filias[1] == 'game') {
                echo '<div class="menu-icon"><img src="' . base_url('assets/img/game.png') . '" alt="" class="img-thumbnail img-circle img-responsive"></div>';
              } elseif ($filias[1] == 'carlos') {
                echo '<div class="menu-icon"><img src="' . base_url('assets/img/carlos.png') . '" alt="" class="img-thumbnail img-circle img-responsive"></div>';
              } elseif ($filias[1] == 'mega') {
                echo '<div class="menu-icon"><img src="' . base_url('assets/img/mega.png') . '" alt="" class="img-thumbnail img-circle img-responsive"></div>';
              } elseif ($filias[1] == 'paloma') {
                echo '<div class="menu-icon"><img src="' . base_url('assets/img/paloma.png') . '" alt="" class="img-thumbnail img-circle img-responsive"></div>';
              } else {
                echo '<i class="menu-icon fa fa-folder-open bg-red"></i>';
              }
              echo '<div class="menu-info"><h4 class="control-sidebar-subheading">' . $category->categoria_prod_id . '</h4><p><strong>' . $category->categoria_prod_descricao . '</strong></p></div>
            </a></li>';
            }
            ?>
        </ul>
      </div>
      </div> -->
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

  <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title" id="mySmallModalLabel">Alterar Senha</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal">
            <div class="box-body">
              <div class="form-group">
                <label for="inputEmail3" class="">Senha Atual</label>
                <input class="form-control" id="senha_atual" type="password">
              </div>
              <div class="form-group">
                <label for="inputPassword3" class="">Nova Senha</label>
                <input class="form-control" id="nova_senha" type="password">
              </div>
              <div class="form-group">
                <label for="inputPassword3" class="">Confirma Senha</label>
                <input class="form-control" id="confirma_senha" type="password">
              </div>
            </div>
            <span class="msg-error text-danger"></span>
            <span class="msg-success text-success"></span>
            <!-- /.box-body -->
            <div class="box-footer">
              <span id="alterarSenha" class="btn btn-primary pull-right">Alterar Senha</span>
            </div>
            <!-- /.box-footer -->
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade historico" data-easein="flipYIn" tabindex="-1" role="dialog" aria-labelledby="cModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Histórico de Venda (<?php echo $this->session->userdata('usuario_nome'); ?>) </h4>
        </div>
        <div class="modal-body">
          <table class="stable" width="100%">
            <tbody>
              <?php $total = 0;
              foreach ($historico as $h) {
                $total += $h['total'] ?>

                <tr>
                  <td style="border-bottom: 1px solid #EEE;">
                    <h4><?php echo $h['forma_pgto'];  ?>:</h4>
                  </td>
                  <td style="text-align:right; border-bottom: 1px solid #EEE;">
                    <h4>
                      <span><?php echo 'R$ ' . $h['total'];  ?></span>
                    </h4>
                  </td>
                </tr>
              <?php  }   ?>

              <tr>
                <td style="font-weight:bold;" width="300px;">
                  <h4><strong>Total em Vendas</strong>:</h4>
                </td>
                <td style="text-align:right;">
                  <h4>
                    <span><strong><?php echo 'R$ ' . number_format($total, 2, ',', '.'); ?></strong></span>
                  </h4>
                </td>
              </tr>
            </tbody>
          </table>

        </div>
      </div>
    </div>
  </div>
  <div class="modal fade rank" data-easein="flipYIn" tabindex="-1" role="dialog" aria-labelledby="cModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Ranck do Vendedores</h4>
        </div>
        <div class="modal-body">
          <table class="stable" width="100%">
            <tbody>
              <tr>
                <th>Vendendor</th>
                <th>Total</th>
              </tr>
              <?php $contador = 1;
              foreach ($vendedores as $v) { ?>

                <tr>
                  <td style="border-bottom: 1px solid #EEE;">
                    <h4><?php echo '#' . $contador . ' - ' . $v['vendedor'];  ?>:</h4>
                  </td>
                  <td style="text-align:left; border-bottom: 1px solid #EEE;">
                    <h4>
                      <span><?php echo 'R$ ' . number_format($v['total'], 2, ',', '.');   ?></span>
                    </h4>
                  </td>
                </tr>
              <?php $contador++;
              }   ?>

            </tbody>
          </table>

        </div>
      </div>
    </div>
  </div>

  <form id="form_emitr_nf_msg" action="<?= base_url('Pdvtablet2') ?>" method="POST">
    <input type="hidden" id="emitr_nf_msg" name="emitr_nf_msg">
  </form>

  <script type="text/javascript">
    var base_url = "<?php echo base_url(); ?>";
  </script>

  <!-- jQuery 3 -->
  <script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- SlimScroll -->
  <script src="<?php echo base_url(); ?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
  <!-- FastClick -->
  <script src="<?php echo base_url(); ?>assets/bower_components/fastclick/lib/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="<?php echo base_url(); ?>assets/dist/js/adminlte.min.js"></script>
  <!--Date picker-->
  <script src="<?php echo base_url(); ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/bower_components/moment/min/moment.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
  <script src="<?php echo base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>
  <!-- <script src="<?php echo base_url(); ?>assets/dist/js/actions-pdv2.js"></script> -->
  <script src="<?php echo base_url(); ?>assets/dist/js/maskmoney.js"></script>

  <script src="<?php echo base_url(); ?>assets/dist/js/verificar-conexao.js"></script>


  <script type="text/javascript">
    $('#datepicker').datepicker({
      autoclose: true
    });

    $(function() {
      //Initialize Select2 Elements
      $('.select2').select2()
    });

    $(document).ready(function() {

      $(".money").maskMoney();
    });

    $(document).ready(function() {

      window.onhelp = function() {
        return false;
      };
      window.onkeydown = evt => {
        switch (evt.keyCode) {
          //ESC
          case 113:
            iniciarVenda();
            break;
          default:
            return true;
        }
        //Returning false overrides default browser event
        return false;
      };


      //Date picker

    });

    function iniciarVenda() {
      incluirVenda();
    }


    function incluirVenda() {

      var clie = 1;

      $.ajax({
          method: "POST",
          url: base_url + "pdvtablet2/adicionarVenda/",
          dataType: "JSON",
          data: {
            clie: clie
          }
        })
        .done(function(response) {
          console.log(response);
          //$("#idVenda").text(response);
          window.location.assign(base_url + "Pdvtablet2/editarVenda/" + response);
        });

    }
  </script>


  <script language="JavaScript">
    function fullScreen() {

      var el = document.documentElement,
        rfs = // for newer Webkit and Firefox
        el.requestFullScreen ||
        el.webkitRequestFullScreen ||
        el.mozRequestFullScreen ||
        el.msRequestFullScreen;
      if (typeof rfs != "undefined" && rfs) {
        rfs.call(el);
      } else if (typeof window.ActiveXObject != "undefined") {
        // for Internet Explorer
        var wscript = new ActiveXObject("WScript.Shell");
        if (wscript != null) {
          wscript.SendKeys("{F11}");
        }
      }

    }
  </script>


</body>

</html>