<section class="content">
  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="<?php echo current_url(); ?>" method="post">
        <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $dados[0]->perfil_id; ?>" placeholder="id">
          <div class="box-body">

            <div class="form-group">
              <label for="perfil_descricao" class="col-sm-2 control-label">Perfil</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" name="perfil_descricao" id="perfil_descricao" value="<?php echo $dados[0]->perfil_descricao; ?>" placeholder="Perfil">
              </div>
            </div>            
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>" class="btn btn-default">Voltar</a>
            <button type="submit" class="btn btn-primary pull-right"><?php echo ucfirst($this->uri->segment(2)); ?></button>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
  </div>
</section>