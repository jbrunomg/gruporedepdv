<?php 

$permissoes = unserialize($dados[0]->perfil_permissoes);

?>
<section class="content">
  <div class="row">
    <div class="col-md-12">

           <div class="box box-primary">
      
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <form  action="<?php echo current_url(); ?>" method="post">
                <input type="hidden" name="id" value="<?php echo $dados[0]->perfil_id; ?>">
                <table class="table no-margin">

                  <thead>
                    <tr><th colspan="4" class=""><h4 class="text-center">Definir Permissões para: <?php echo $dados[0]->perfil_descricao ?></h4></th></tr>
                  </thead>
                  <thead>
                    <tr><th colspan="4" class="label-default"><h4 class="text-center">Clientes</h4></th></tr>
                  </thead>
                  <tbody>
                      <?php $campo = "Cliente"; ?>
                      <tr>
                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red" name="v<?php echo $campo;?>" <?php echo (isset($permissoes["v{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Visualizar
                            </label>                          
                          </div>
                        </td>

                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue"  style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red" name="a<?php echo $campo;?>" <?php echo (isset($permissoes["a{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Adicionar
                            </label>                          
                          </div>
                        </td>                                     
      
                      <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red"  name="e<?php echo $campo;?>" <?php echo (isset($permissoes["e{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Editar
                            </label>                          
                          </div>
                        </td>

                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red"  name="d<?php echo $campo;?>" <?php echo (isset($permissoes["d{$campo}"]))?'checked':''; ?>   style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Excluir
                            </label>                          
                          </div>
                        </td>                                     
                      </tr> 

                  <thead>
                    <tr><th colspan="4" class="label-default"><h4 class="text-center">Funcionários</h4></th></tr>
                  </thead>
                  <tbody>
                      <?php $campo = "Funcionario"; ?>
                      <tr>
                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red" name="v<?php echo $campo;?>" <?php echo (isset($permissoes["v{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Visualizar
                            </label>                          
                          </div>
                        </td>

                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue"  style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red" name="a<?php echo $campo;?>" <?php echo (isset($permissoes["a{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Adicionar
                            </label>                          
                          </div>
                        </td>                                     
      
                      <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red"  name="e<?php echo $campo;?>" <?php echo (isset($permissoes["e{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Editar
                            </label>                          
                          </div>
                        </td>

                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red"  name="d<?php echo $campo;?>" <?php echo (isset($permissoes["d{$campo}"]))?'checked':''; ?>   style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Excluir
                            </label>                          
                          </div>
                        </td>                                     
                      </tr> 
                      
                  <thead>
                    <tr><th colspan="4" class="label-default"><h4 class="text-center">Fornecedores</h4></th></tr>
                  </thead>
                  <tbody>
                      <?php $campo = "Fornecedor"; ?>
                      <tr>
                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red" name="v<?php echo $campo;?>" <?php echo (isset($permissoes["v{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Visualizar
                            </label>                          
                          </div>
                        </td>

                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue"  style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red" name="a<?php echo $campo;?>" <?php echo (isset($permissoes["a{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Adicionar
                            </label>                          
                          </div>
                        </td>                                     
      
                      <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red"  name="e<?php echo $campo;?>" <?php echo (isset($permissoes["e{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Editar
                            </label>                          
                          </div>
                        </td>

                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red"  name="d<?php echo $campo;?>" <?php echo (isset($permissoes["d{$campo}"]))?'checked':''; ?>   style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Excluir
                            </label>                          
                          </div>
                        </td>                                     
                      </tr>  
              

                  <thead>
                    <tr><th colspan="4" class="label-default"><h4 class="text-center">Categoria</h4></th></tr>
                  </thead>
                  <tbody>
                      <?php $campo = "Categoria"; ?>
                      <tr>
                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red" name="v<?php echo $campo;?>" <?php echo (isset($permissoes["v{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Visualizar
                            </label>                          
                          </div>
                        </td>

                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue"  style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red" name="a<?php echo $campo;?>" <?php echo (isset($permissoes["a{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Adicionar
                            </label>                          
                          </div>
                        </td>                                     
      
                      <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red"  name="e<?php echo $campo;?>" <?php echo (isset($permissoes["e{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Editar
                            </label>                          
                          </div>
                        </td>

                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red"  name="d<?php echo $campo;?>" <?php echo (isset($permissoes["d{$campo}"]))?'checked':''; ?>   style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Excluir
                            </label>                          
                          </div>
                        </td>                                     
                      </tr>  


                  <thead>
                    <tr><th colspan="4" class="label-default"><h4 class="text-center">Almoxarifado</h4></th></tr>
                  </thead>
                  <tbody>
                      <?php $campo = "Almoxarifado"; ?>
                      <tr>
                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red" name="v<?php echo $campo;?>" <?php echo (isset($permissoes["v{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Visualizar
                            </label>                          
                          </div>
                        </td>

                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue"  style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red" name="a<?php echo $campo;?>" <?php echo (isset($permissoes["a{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Adicionar
                            </label>                          
                          </div>
                        </td>                                     
      
                      <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red"  name="e<?php echo $campo;?>" <?php echo (isset($permissoes["e{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Editar
                            </label>                          
                          </div>
                        </td>

                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red"  name="d<?php echo $campo;?>" <?php echo (isset($permissoes["d{$campo}"]))?'checked':''; ?>   style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Excluir
                            </label>                          
                          </div>
                        </td>                                     
                      </tr>       

                  <thead>
                    <tr><th colspan="4" class="label-default"><h4 class="text-center">Financeiro</h4></th></tr>
                  </thead>
                  <tbody>
                      <?php $campo = "Financeiro"; ?>
                      <tr>
                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red" name="v<?php echo $campo;?>" <?php echo (isset($permissoes["v{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Visualizar
                            </label>                          
                          </div>
                        </td>

                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue"  style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red" name="a<?php echo $campo;?>" <?php echo (isset($permissoes["a{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Adicionar
                            </label>                          
                          </div>
                        </td>                                     
      
                      <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red"  name="e<?php echo $campo;?>" <?php echo (isset($permissoes["e{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Editar
                            </label>                          
                          </div>
                        </td>

                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red"  name="d<?php echo $campo;?>" <?php echo (isset($permissoes["d{$campo}"]))?'checked':''; ?>   style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Excluir
                            </label>                          
                          </div>
                        </td>                                     
                      </tr>   


                  <thead>
                    <tr><th colspan="4" class="label-default"><h4 class="text-center">Fiscal</h4></th></tr>
                  </thead>
                  <tbody>
                      <?php $campo = "Fiscal"; ?>
                      <tr>
                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red" name="v<?php echo $campo;?>" <?php echo (isset($permissoes["v{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Visualizar
                            </label>                          
                          </div>
                        </td>

                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue"  style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red" name="a<?php echo $campo;?>" <?php echo (isset($permissoes["a{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Adicionar
                            </label>                          
                          </div>
                        </td>                                     
      
                      <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red"  name="e<?php echo $campo;?>" <?php echo (isset($permissoes["e{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Editar
                            </label>                          
                          </div>
                        </td>

                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red"  name="d<?php echo $campo;?>" <?php echo (isset($permissoes["d{$campo}"]))?'checked':''; ?>   style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Excluir
                            </label>                          
                          </div>
                        </td>                                     
                      </tr>                
           

                  <thead>
                    <tr><th colspan="4" class="label-default"><h4 class="text-center">Usuários</h4></th></tr>
                  </thead>
                  <tbody>
                      <?php $campo = "Usuario"; ?>
                      <tr>
                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red" name="v<?php echo $campo;?>" <?php echo (isset($permissoes["v{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Visualizar
                            </label>                          
                          </div>
                        </td>

                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue"  style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red" name="a<?php echo $campo;?>" <?php echo (isset($permissoes["a{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Adicionar
                            </label>                          
                          </div>
                        </td>                                     
      
                      <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red"  name="e<?php echo $campo;?>" <?php echo (isset($permissoes["e{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Editar
                            </label>                          
                          </div>
                        </td>

                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red"  name="d<?php echo $campo;?>" <?php echo (isset($permissoes["d{$campo}"]))?'checked':''; ?>   style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Excluir
                            </label>                          
                          </div>
                        </td>                                     
                      </tr>  




                  <thead>
                    <tr><th colspan="4" class="label-default"><h4 class="text-center">Pedido</h4></th></tr>
                  </thead>
                  <tbody>
                      <?php $campo = "Pedido"; ?>
                      <tr>
                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red" name="v<?php echo $campo;?>" <?php echo (isset($permissoes["v{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Visualizar
                            </label>                          
                          </div>
                        </td>

                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue"  style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red" name="a<?php echo $campo;?>" <?php echo (isset($permissoes["a{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Adicionar
                            </label>                          
                          </div>
                        </td>                                     
      
                      <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red"  name="e<?php echo $campo;?>" <?php echo (isset($permissoes["e{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Editar
                            </label>                          
                          </div>
                        </td>

                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red"  name="d<?php echo $campo;?>" <?php echo (isset($permissoes["d{$campo}"]))?'checked':''; ?>   style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Excluir
                            </label>                          
                          </div>
                        </td>                                     
                      </tr>  

                  <thead>
                    <tr><th colspan="4" class="label-default"><h4 class="text-center">Vendas</h4></th></tr>
                  </thead>
                  <tbody>
                      <?php $campo = "Venda"; ?>
                      <tr>
                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red" name="v<?php echo $campo;?>" <?php echo (isset($permissoes["v{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Visualizar
                            </label>                          
                          </div>
                        </td>

                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue"  style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red" name="a<?php echo $campo;?>" <?php echo (isset($permissoes["a{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Adicionar
                            </label>                          
                          </div>
                        </td>                                     
      
                      <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red"  name="e<?php echo $campo;?>" <?php echo (isset($permissoes["e{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Editar
                            </label>                          
                          </div>
                        </td>

                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red"  name="d<?php echo $campo;?>" <?php echo (isset($permissoes["d{$campo}"]))?'checked':''; ?>   style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Excluir
                            </label>                          
                          </div>
                        </td>                                     
                      </tr>   


                  <thead>
                    <tr><th colspan="4" class="label-default"><h4 class="text-center">Ordem de Serviço</h4></th></tr>
                  </thead>
                  <tbody>
                      <?php $campo = "OrdemServico"; ?>
                      <tr>
                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red" name="v<?php echo $campo;?>" <?php echo (isset($permissoes["v{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Visualizar
                            </label>                          
                          </div>
                        </td>

                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue"  style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red" name="a<?php echo $campo;?>" <?php echo (isset($permissoes["a{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Adicionar
                            </label>                          
                          </div>
                        </td>                                     
      
                      <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red"  name="e<?php echo $campo;?>" <?php echo (isset($permissoes["e{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Editar
                            </label>                          
                          </div>
                        </td>

                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red"  name="d<?php echo $campo;?>" <?php echo (isset($permissoes["d{$campo}"]))?'checked':''; ?>   style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Excluir
                            </label>                          
                          </div>
                        </td>                                     
                      </tr>  
                   

                  <thead>
                    <tr><th colspan="4" class="label-default"><h4 class="text-center">Serviço</h4></th></tr>
                  </thead>
                  <tbody>
                      <?php $campo = "Servico"; ?>
                      <tr>
                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red" name="v<?php echo $campo;?>" <?php echo (isset($permissoes["v{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Visualizar
                            </label>                          
                          </div>
                        </td>

                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue"  style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red" name="a<?php echo $campo;?>" <?php echo (isset($permissoes["a{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Adicionar
                            </label>                          
                          </div>
                        </td>                                     
      
                      <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red"  name="e<?php echo $campo;?>" <?php echo (isset($permissoes["e{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Editar
                            </label>                          
                          </div>
                        </td>

                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red"  name="d<?php echo $campo;?>" <?php echo (isset($permissoes["d{$campo}"]))?'checked':''; ?>   style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Excluir
                            </label>                          
                          </div>
                        </td>                                     
                      </tr>             
                  
                  <thead>
                    <tr><th colspan="4" class="label-default"><h4 class="text-center">Perfis</h4></th></tr>
                  </thead>
                  <tbody>
                      <?php $campo = "Perfil"; ?>
                      <tr>
                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red" name="v<?php echo $campo;?>" <?php echo (isset($permissoes["v{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Visualizar
                            </label>                          
                          </div>
                        </td>

                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue"  style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red" name="a<?php echo $campo;?>" <?php echo (isset($permissoes["a{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Adicionar
                            </label>                          
                          </div>
                        </td>                                     
      
                      <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red"  name="e<?php echo $campo;?>" <?php echo (isset($permissoes["e{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Editar
                            </label>                          
                          </div>
                        </td>

                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red"  name="d<?php echo $campo;?>" <?php echo (isset($permissoes["d{$campo}"]))?'checked':''; ?>   style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Excluir
                            </label>                          
                          </div>
                        </td>                                     
                      </tr> 
                      <tr>
                        <td colspan="4">
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red"  name="p<?php echo $campo;?>"  <?php echo (isset($permissoes["p{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Permissões
                            </label>                          
                          </div>
                        </td>  
                        
                      </tr>                   
                  
                 
                  <thead>
                      <tr><th colspan="4" class="label-default"><h4 class="text-center">Relatórios</h4></th></tr>
                  </thead>
                  <tbody>
                      <?php $campo = "Relatorio"; ?>
                      <tr>
                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red" name="cliente<?php echo $campo;?>" <?php echo (isset($permissoes["cliente{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Clientes
                            </label>                          
                          </div>
                        </td> 

                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red" name="fornecedor<?php echo $campo;?>" <?php echo (isset($permissoes["fornecedor{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Fornecedor
                            </label>                          
                          </div>
                        </td>   

                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red" name="chat<?php echo $campo;?>" <?php echo (isset($permissoes["chat{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              chat
                            </label>                          
                          </div>
                        </td> 

                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red" name="produto<?php echo $campo;?>" <?php echo (isset($permissoes["produto{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Produtos
                            </label>                          
                          </div>
                        </td>
                      
                      </tr>
                      <tr> 
                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red" name="simulacao<?php echo $campo;?>" <?php echo (isset($permissoes["simulacao{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Simulação de Compra
                            </label>                          
                          </div>
                        </td> 
                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red" name="vendas<?php echo $campo;?>" <?php echo (isset($permissoes["vendas{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Vendas
                            </label>                          
                          </div>
                        </td> 
                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red" name="vendedores<?php echo $campo;?>" <?php echo (isset($permissoes["vendedores{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Vendedores
                            </label>                          
                          </div>
                        </td> 
                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red" name="compras<?php echo $campo;?>" <?php echo (isset($permissoes["compras{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Compras
                            </label>                          
                          </div>                         
                        </td> 

    

                      </tr>
                      <tr>
                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red" name="financeiro<?php echo $campo;?>" <?php echo (isset($permissoes["financeiro{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Financeiro
                            </label>                          
                          </div>                          
                        </td> 

                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red" name="financeiro<?php echo $campo;?>" <?php echo (isset($permissoes["financeiro{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Ordem de Serviço
                            </label>                          
                          </div>                          
                        </td> 

                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red" name="vendedorExt<?php echo $campo;?>" <?php echo (isset($permissoes["vendedorExt{$campo}"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Vendedor externo
                            </label>                          
                          </div>                          
                        </td>

                        
                      </tr>


                  <thead>
                    <tr><th colspan="4" class="label-default"><h4 class="text-center">Outros</h4></th></tr>
                  </thead>
                  <tbody>                      
                      <tr>
                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red" name="vBackup" <?php echo (isset($permissoes["vBackup"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Backup
                            </label>                          
                          </div>
                        </td>

                        <td>
                          <div class="form-group">
                            <label class="">
                              <div class="icheckbox_flat-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input class="flat-red" name="vEmitente" <?php echo (isset($permissoes["vEmitente"]))?'checked':''; ?>  style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                              </div>
                              Emitente
                            </label>                          
                          </div>
                        </td>                      
                      <thead>



                  </tbody>
                  <tfoot>
                    <tr><td colspan="4">
                       <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>" class="btn btn-default">Voltar</a>
                       <button type="submit" class="btn btn-primary pull-right">Salvar</button>
                    </td></tr>
                  </tfoot>
                </table>
                </form>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->

           
            
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->

    
    </div>
  </div>
</section>
                  