<section class="content">
  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="<?php echo current_url(); ?>" method="post">
          <div class="box-body">

            <div class="form-group">
              <label for="fornecedor_tipo" class="col-sm-2 control-label">Fornecedor</label>
              <div class="col-sm-5">
                            
                <select required class="form-control" name="fornecedor_tipo" id="tipo">
                    <option value='1'>Pessoa Física</option>
                    <option value='2'>Pessoa Jurídica</option>
                </select>
              </div>
            </div>
          
            <div class="form-group">
              <label for="fornecedor_cnpj_cpf" class="col-sm-2 control-label">CPF/CNPJ</label>
              <div class="col-sm-5">              
                <input required type="text" class="form-control" name="fornecedor_cnpj_cpf" id="cpfCnpj" value="<?php echo set_value('fornecedor_cnpj_cpf'); ?>" placeholder="CPF">
              </div>
            </div>

            <div class="form-group">
              <label for="fornecedor_nome" class="col-sm-2 control-label">Nome</label>
              <div class="col-sm-5">              
                <input required type="text" class="form-control" id="nome" name="fornecedor_nome" value="<?php echo set_value('fornecedor_nome'); ?>" placeholder="Nome Completo">
              </div>
            </div> 

            <div class="form-group">
              <label for="fornecedor_cep" class="col-sm-2 control-label">CEP</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" id="cep" name="fornecedor_cep" value="<?php echo set_value('fornecedor_cep'); ?>" placeholder="Cep">
              </div>
            </div>  

            <div class="form-group">
              <label for="fornecedor_endereco" class="col-sm-2 control-label">Endereço</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" id="endereco" name="fornecedor_endereco" value="<?php echo set_value('fornecedor_endereco'); ?>" placeholder="Endereço">
              </div>
            </div> 
            
            <div class="form-group">
              <label for="fornecedor_bairro" class="col-sm-2 control-label">Bairro</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" id="bairro" name="fornecedor_bairro" value="<?php echo set_value('fornecedor_bairro'); ?>" placeholder="Bairro">
              </div>
            </div>

            <div class="form-group">
              <label for="fornecedor_estado" class="col-sm-2 control-label">Estado</label>
              <div class="col-sm-5">              
                
                <select class="form-control" name="fornecedor_estado" id="estado">
                  <option value="">Selecione</option>
                  <?php foreach ($estados as $valor) { ?>
                    <option value='<?php echo $valor->sigla; ?>'><?php echo $valor->nome; ?> </option>
                  <?php } ?>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="fornecedor_cidade" class="col-sm-2 control-label">Cidade</label>
              <div class="col-sm-5">              
                <select class="form-control" name="fornecedor_cidade" id="cidade">
                  <option value="">Escolha um estado</option>                                      
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="fornecedor_numero" class="col-sm-2 control-label">Número</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" id="fornecedor_numero" name="fornecedor_numero" value="<?php echo set_value('fornecedor_numero'); ?>" placeholder="Número">
              </div>
            </div>       

            <div class="form-group">
              <label for="fornecedor_complemento" class="col-sm-2 control-label">Complemento</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" id="fornecedor_complemento" name="fornecedor_complemento" value="<?php echo set_value('fornecedor_complemento'); ?>" placeholder="Complemento">
              </div>
            </div>

            <div class="form-group">
              <label for="fornecedor_telefone" class="col-sm-2 control-label">Telefone</label>
              <div class="col-sm-5">              
                <input required type="text" class="form-control" id="telefone"  name="fornecedor_telefone" value="<?php echo set_value('fornecedor_telefone'); ?>" placeholder="Telefone">
              </div>
            </div>

            <div class="form-group">
              <label for="fornecedor_celular" class="col-sm-2 control-label">Celular</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="celular" name="fornecedor_celular" value="<?php echo set_value('fornecedor_celular'); ?>" placeholder="Celular" required="required">
              </div>
            </div>

            <div class="form-group">
              <label for="fornecedor_email" class="col-sm-2 control-label">E-mail</label>
              <div class="col-sm-5">                
                <input type="text" class="form-control" id="e-mail" name="fornecedor_email" value="<?php echo set_value('fornecedor_email'); ?>" placeholder="E-mail">
              </div>
            </div>

             <div class="form-group">
              <label for="fornecedor_banco" class="col-sm-2 control-label">Banco</label>
              <div class="col-sm-5">              
                
                <select class="form-control" name="fornecedor_banco" id="banco">
                  <option value="">Selecione</option>
                  <?php foreach ($bancos as $valor) { ?>
                    <option value='<?php echo $valor->banco_id; ?>'><?php echo $valor->banco_descricao; ?> </option>
                  <?php } ?>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="fornecedor_agencia" class="col-sm-2 control-label">Agência</label>
              <div class="col-sm-5">                
                <input type="text" class="form-control" id="agencia" name="fornecedor_agencia" value="<?php echo set_value('fornecedor_agencia'); ?>" placeholder="Agência">
              </div>
            </div>

            <div class="form-group">
              <label for="fornecedor_conta" class="col-sm-2 control-label">Conta com Dígito</label>
              <div class="col-sm-5">                
                <input type="text" class="form-control" id="conta" name="fornecedor_conta" value="<?php echo set_value('fornecedor_conta'); ?>" placeholder="Conta">
              </div>
            </div>

            <div class="form-group">
              <label for="fornecedor_titular" class="col-sm-2 control-label">Titular da Conta</label>
              <div class="col-sm-5">                
                <input type="text" class="form-control" id="titular" name="fornecedor_titular" value="<?php echo set_value('fornecedor_titular'); ?>" placeholder="Titular">
              </div>
            </div>

            <div class="form-group">
              <label for="fornecedor_grupoprod_id" class="col-sm-2 control-label">Grupo Produto</label>
              <div class="col-sm-5">                
                <!-- <input type="text" class="form-control" id="titular" name="grupo_prod" value="<?php echo set_value('grupo_prod'); ?>" placeholder="Grupo Produto"> -->
                <select id="fornecedor_grupoprod_id" class="form-control select2" multiple="multiple" data-placeholder="Selecione o grupo de produtos" name="fornecedor_grupoprod_id[]">
                  <?php foreach ($grupo as $g) { ?>
                  <option value='<?php echo $g->categoria_prod_id; ?>'><?php echo $g->categoria_prod_descricao; ?> </option>
                  <?php } ?>                       
                </select>  
              </div>
            </div>

          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>" class="btn btn-default">Voltar</a>
            <button type="submit" class="btn btn-primary pull-right"><?php echo ucfirst($this->uri->segment(2)); ?></button>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
  </div>
</section>



