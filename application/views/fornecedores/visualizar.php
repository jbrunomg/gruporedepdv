<section class="content">
  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>
        </div>
        <!-- /.box-header -->
        
        <div class="col-md-12">
          <!-- Custom Tabs (Pulled to the right) -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs pull-right">
              <li><a href="#tab_1" data-toggle="tab">Compras</a></li>
              <li><a href="#tab_2" data-toggle="tab">Pagamentos</a></li>
              <li class="active"><a href="#tab_3" data-toggle="tab">Dados</a></li>      
              <li class="pull-left header"><i class="fa fa-th"></i> Historico Fornecedor</li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane" id="tab_1">  

                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Lista de todas as compras solicitada pela MEGACELL</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                      <thead>
                      <tr>
                        <th>#</th>
                        <th>Data Pedido</th>
                        <th>Observação</th>
                        <th>Itens/Volume</th>
                        <th>Faturado</th>
                        <th style="width: 40px">Ações</th>
                      </tr>
                      </thead>
                      <tbody>
                      <?php
                        $seq = 1;
                        foreach ($pedidos as $p) {
                      ?>
                        <tr>
                          <td><?php echo $seq ++ ?></td>
                          <td><?php echo $p->dataPedido ?></td>
                          <td><?php echo $p->observacao ?></td>
                          <td><?php echo $p->itens.'/'.$p->volume ?></td>
                          <td>Yes ou NO</td>
                          <td>
                          <?php if(verificarPermissao('vCliente')){ ?>
                          <a href="<?php echo base_url(); ?>pedidofornecedor/visualizar/<?php echo $p->idPedidos; ?>" data-toggle="tooltip" title="Visualizar"><i class="fa fa-search  text-success"></i> </a>
                          <?php } ?>
                          <?php if(verificarPermissao('eCliente')){ ?>
                          <a href="<?php echo base_url(); ?>pedidofornecedor/editar/<?php echo $p->idPedidos ?>" data-toggle="tooltip" title="Editar"><i class="fa fa-edit"></i> </a>
                          <?php } ?>
                          <?php if(verificarPermissao('dCliente')){ ?>
                          <a style="cursor:pointer;" title="Deletar" data-toggle="modal" data-target="#excluir" onclick="excluir('<?php echo base_url()."pedidofornecedor/excluir/".$p->idPedidos.'/'.$dados[0]->fornecedor_id
                           ?>');" ><i class="fa fa-trash  text-danger"></i></a>
                          </td>
                          <?php } ?>  
                        </tr>
                      <?php } ?>
                     
                      </tbody>
                      <tfoot>
                      <tr>
                        <th>#</th>
                        <th>Data Pedido</th>
                        <th>Observação</th>
                        <th>Itens/Volume</th>
                        <th>Faturado</th>
                        <th style="width: 40px">Ações</th>
                      </tr>
                      </tfoot>
                    </table>
                  </div>
                  <!-- /.box-body -->
                </div>
                <!-- /.box -->

     
                
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                <b>Lista de todas as faturas pagas e em aberto comprado pelo fornecedor.</b>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane active" id="tab_3">

                <div class="row">
                  <div class="col-md-6">
                    <label for="fornecedor_tipo">Tipo</label>                        
                    <select disabled required class="form-control" name="fornecedor_tipo" id="tipo">
                        <option value='1' <?php echo ($dados[0]->fornecedor_tipo == 1)?'Selected':'';?>>Pessoa Física</option>
                        <option value='2' <?php echo ($dados[0]->fornecedor_tipo == 2)?'Selected':'';?>>Pessoa Jurídica</option>
                    </select>
                  </div>

                  <div class="col-md-6 form-group">
                    <label for="descricao">CEP</label>
                   <input disabled type="text" class="form-control" id="cep" name="fornecedor_cep"  value="<?php echo $dados[0]->fornecedor_cep; ?>" placeholder="Cep">
                  </div>

                  <div class="col-md-6">
                    <label for="fornecedor_cnpj_cpf">CPF/CNPJ</label>
                    <input disabled type="text" class="form-control" name="fornecedor_cnpj_cpf" id="fornecedor_cnpj_cpf"  value="<?php echo $dados[0]->fornecedor_cnpj_cpf; ?>" placeholder="CPF/CNPJ">    
                  </div>

                  <div class="col-md-6 form-group">
                    <label for="descricao">Endereço</label>
                    <input disabled type="text" class="form-control" id="endereco" name="fornecedor_endereco"  value="<?php echo $dados[0]->fornecedor_endereco; ?>" placeholder="Endereço">
                  </div>
                   
                  <div class="col-md-6">
                    <label for="descricao">Nome</label>
                    <input disabled type="text" class="form-control" id="fornecedor_nome" name="fornecedor_nome"  value="<?php echo $dados[0]->fornecedor_nome; ?>" placeholder="Nome Completo">
                  </div>

                  <div class="col-md-6 form-group">
                    <label for="descricao">Bairro</label>
                    <input disabled type="text" class="form-control" id="bairro" name="fornecedor_bairro"  value="<?php echo $dados[0]->fornecedor_bairro; ?>" placeholder="Bairro">
                  </div>

                  <div class="col-md-6">
                    <label for="descricao">Telefone</label>
                    <input disabled type="text" class="form-control" id="telefone"  name="fornecedor_telefone"  value="<?php echo $dados[0]->fornecedor_telefone; ?>" placeholder="Telefone">
                  </div>

                  <div class="col-md-6 form-group">
                    <label for="descricao">Número</label>
                      <input disabled type="text" class="form-control" id="numero" name="fornecedor_numero"  value="<?php echo $dados[0]->fornecedor_numero; ?>" placeholder="Número">                            
                  </div>

                  <div class="col-md-6">
                    <label for="descricao">Celular</label>
                     <input disabled type="text" class="form-control" id="celular" name="fornecedor_celular"  value="<?php echo $dados[0]->fornecedor_celular; ?>" placeholder="Celular">
                  </div>

                  <div class="col-md-6">
                    <label for="descricao">Complemento</label>
                      <input disabled type="text" class="form-control" id="completo" name="fornecedor_complemento"  value="<?php echo $dados[0]->fornecedor_complemento; ?>" placeholder="Complemento">
                  </div>   

                  <div class="col-md-6">
                    <label for="descricao">E-mail</label>
                    <input disabled type="text" class="form-control" id="e-mail" name="fornecedor_email"  value="<?php echo $dados[0]->fornecedor_email; ?>" placeholder="E-mail">
                  </div>

                  <div class="col-md-6">
                    <label for="descricao">Cidade</label>
                      <select disabled class="form-control" name="fornecedor_cidade" id="cidade">
                        <option value="">Selecione</option>
                        <?php foreach ($cidades as $valor) { ?>
                        <?php $selected = ($valor->nome == strtoupper($dados[0]->fornecedor_cidade))?'SELECTED':''; ?>
                          <option value='<?php echo $valor->nome; ?>'  <?php echo $selected; ?>><?php echo $valor->nome; ?> </option>
                        <?php } ?>                                    
                      </select>
                  </div>  

                  <div class="col-md-6">                    
                    <label for="descricao">Grupo Produto</label>
                    <div>    
                      <select disabled name="fornecedor_grupoprod_id[]" id="fornecedor_grupoprod_id" class="form-control select2" multiple="multiple" required>          
                        <?php 
                          foreach($grupo as $key => $g) { $select = "";                    
                            foreach($grupoforn as $forn){                                
                                if($g->categoria_prod_id == $forn){
                                    $select = "selected";}
                              }
                              echo "<option value='".$g->categoria_prod_id."'".$select.">".$g->categoria_prod_descricao."</option>";
                        }?>                        
                      </select>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <label for="descricao">Estado</label>
                      <select disabled class="form-control" name="fornecedor_estado" id="estado">
                        <option value="">Selecione</option>
                        <?php foreach ($estados as $valor) { ?>
                        <?php $selected = ($valor->sigla == $dados[0]->fornecedor_estado)?'SELECTED':''; ?>
                          <option value='<?php echo $valor->sigla; ?>' <?php echo $selected; ?>><?php echo $valor->nome; ?> </option>
                        <?php } ?>
                      </select>
                  </div>        
                </div>  
             

              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->



        <!-- /.box-body -->
        <div class="box-footer">
          <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>" class="btn btn-default">Voltar</a> 
          <a href="<?php echo base_url(); ?>pedidofornecedor/adicionar/<?php echo $this->uri->segment(3);?>" class="btn btn-primary pull-right">Adicionar Compra</a>         
        </div>
        <!-- /.box-footer -->        
      </div>
    </div>
  </div>
</section>

<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>