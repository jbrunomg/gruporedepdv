<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <div class="col-md-2">
      <?php if(verificarPermissao('aCliente')){ ?>
        <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/adicionar" type="button" class="btn btn-block btn-default btn-flat">Adicionar</a>
        <?php } ?>
      </div>          
    </div>
    <div class="box-body">
      <div class="container-fluid">
        <table id="example" class="table" width="100%">
          <thead>
            <tr>                                            
              <th>Nome</th>
              <th>Celular</th>
              <th>Cpf/cnpj</th>
              <th>Endereço</th>
              <th>Tipo</th>
              <th>Ações</th>
            </tr>
          </thead>
          <tbody>

            <?php foreach ($dados as $d){ 
                $tipo = ($d->fornecedor_tipo == 1)? 'Física':'Jurídica';
            ?>
            <tr>                      
              <td> <?php echo $d->fornecedor_nome;?></td> 
              <td> <?php echo $d->fornecedor_celular;?></td> 
              <td> <?php echo $d->fornecedor_cnpj_cpf;?></td>
              <td> <?php echo $d->fornecedor_endereco;?></td>
              <td> <?php echo $tipo;?></td>
              <td>
                <?php if(verificarPermissao('vCliente')){ ?>
                <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/visualizar/<?php echo $d->fornecedor_id; ?>" data-toggle="tooltip" title="Visualizar"><i class="fa fa-search  text-success"></i> </a>
                <?php } ?>
                <?php if(verificarPermissao('eCliente')){ ?>
                <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/editar/<?php echo $d->fornecedor_id; ?>" data-toggle="tooltip" title="Editar"><i class="fa fa-edit"></i> </a>
                <?php } ?>
                <?php if(verificarPermissao('dCliente')){ ?>
                <a style="cursor:pointer;" title="Deletar" data-toggle="modal" data-target="#excluir" onclick="excluir('<?php echo base_url().$this->uri->segment(1)."/excluir/".$d->fornecedor_id; ?>');" ><i class="fa fa-trash  text-danger"></i></a>
              </td>
              <?php } ?>                
            </tr>
            <?php } ?>

          </tbody>
          <tfoot>
            <tr>                     
              <th>Nome</th>
              <th>Celular</th>
              <th>Cpf</th>
              <th>Endereço</th>
              <th>Tipo</th>
              <th>Ações</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</section>