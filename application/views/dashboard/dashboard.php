      <!-- Main content -->
      <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"></h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <div class="col-md-12">
              <?php if (verificarPermissao('vVenda')) { ?>
                <div class="col-lg-3 col-xs-6">
                  <!-- small box -->
                  <div class="small-box bg-aqua">
                    <div class="inner">
                      <h3><?php echo $vendas; ?></h3>

                      <p>Vendas</p>
                    </div>
                    <div class="icon">
                      <i class="fa fa-shopping-cart"></i>
                    </div>
                    <a href="<?php echo base_url() ?>vendas" class="small-box-footer">Listar <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
                </div>
              <?php } ?>
              <?php if (verificarPermissao('vUsuario')) { ?>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                  <!-- small box -->
                  <div class="small-box bg-yellow">
                    <div class="inner">
                      <h3><?php echo $usuarios; ?></h3>

                      <p>Usuários</p>
                    </div>
                    <div class="icon">
                      <i class="ion ion-person-add"></i>
                    </div>
                    <a href="<?php echo base_url() ?>usuarios" class="small-box-footer">Listar <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
                </div>
              <?php } ?>
              <?php if (verificarPermissao('vCliente')) { ?>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                  <!-- small box -->
                  <div class="small-box bg-green">
                    <div class="inner">
                      <h3><?php echo $clientes; ?><sup style="font-size: 20px"></sup></h3>

                      <p>Clientes</p>
                    </div>
                    <div class="icon">
                      <i class="fa fa-users"></i>
                    </div>
                    <a href="<?php echo base_url() ?>clientes" class="small-box-footer">Listar <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
                </div>
              <?php } ?>
              <?php if (verificarPermissao('vPedido')) { ?>

                <div class="col-lg-3 col-xs-6">
                  <!-- small box -->
                  <div class="small-box bg-red">
                    <div class="inner">
                      <h3><?php echo $pedidos; ?></h3>

                      <p>Pedido em aberto</p>
                    </div>
                    <div class="icon">
                      <i class="fa fa-barcode"></i>
                    </div>
                    <a href="<?php echo base_url() ?>pedido" class="small-box-footer">Listar <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
                </div>

              <?php } ?>
            </div>
          </div>
          <!-- /.box-body -->
        </div>

        <!--        <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">...</h3>
          <div class="box-tools pull-right">            
          </div> -->
        <!-- 
            <?php // foreach ($noticias as $not) { 
            ?>
            <div class="col-lg-4">
              <h4><?php // echo $not['title']; 
                  ?></h4>
              <span class="label label-warning">Publicado em:  <?php  // echo $not['pubDate']; 
                                                                ?></span>
              
              <p><?php // echo substr($not['description'],0,400);
                  ?></p>
              <p><a class="btn btn-primary" href="<?php // echo $not['link'] 
                                                  ?>" role="button" target="_blank">Ler Notícia</a></p>          

            </div>
            <?php // } 
            ?>
        </div>
      </div>
       -->
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <div class="col-md-8">


            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title"></h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>

              <?php if (verificarPermissao('vFinanceiro')) { ?>
                <div class="panel-body">

                  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                  <script type="text/javascript">
                    google.charts.load('current', {
                      'packages': ['bar']
                    });
                    google.charts.setOnLoadCallback(drawChart);

                    function drawChart() {
                      var data = google.visualization.arrayToDataTable([
                        ['Mês', '<?php echo date('Y') ?>', '<?php echo date('Y') - 1 ?>', '<?php echo date('Y') - 2 ?>'],

                        <?php foreach ($evoFaturamento as $valor) { ?>

                          [<?php echo "'" . $valor->Ano . "'"; ?>, <?php echo $valor->atual; ?>, <?php echo $valor->meio; ?>, <?php echo $valor->fim; ?>],


                        <?php } ?>

                      ]);

                      var options = {
                        chart: {
                          title: 'Evolução Faturamento',
                          //  subtitle: 'Sales, Expenses, and Profit: 2014-2017',
                        },
                        bars: 'vertical', // Required for Material Bar Charts.
                        vAxis: {
                          format: 'decimal'
                        }
                      };

                      var chart = new google.charts.Bar(document.getElementById('barchart_faturamento_rec'));

                      chart.draw(data, google.charts.Bar.convertOptions(options));
                    }
                  </script>

                  <div id="barchart_faturamento_rec" style="width: 100%; height: 500px;"></div>

                </div>
              <?php  } ?>

            </div>


            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title"></h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>

              <?php if (verificarPermissao('vFinanceiro')) { ?>
                <div class="panel-body">

                  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                  <script type="text/javascript">
                    google.charts.load('current', {
                      'packages': ['bar']
                    });
                    google.charts.setOnLoadCallback(drawChart);

                    function drawChart() {
                      var data = google.visualization.arrayToDataTable([
                        ['Mês', '<?php echo date('Y') ?>', '<?php echo date('Y') - 1 ?>', '<?php echo date('Y') - 2 ?>'],

                        <?php foreach ($evoDespesa as $valor) { ?>

                          [<?php echo "'" . $valor->Ano . "'"; ?>, <?php echo $valor->atual; ?>, <?php echo $valor->meio; ?>, <?php echo $valor->fim; ?>],


                        <?php } ?>

                      ]);

                      var options = {
                        chart: {
                          title: 'Evolução Despesa',
                          //  subtitle: 'Sales, Expenses, and Profit: 2014-2017',
                        },
                        bars: 'vertical', // Required for Material Bar Charts.
                        vAxis: {
                          format: 'decimal'
                        }
                      };

                      var chart = new google.charts.Bar(document.getElementById('barchart_faturamento_des'));

                      chart.draw(data, google.charts.Bar.convertOptions(options));
                    }
                  </script>

                  <div id="barchart_faturamento_des" style="width: 100%; height: 500px;"></div>

                </div>
              <?php  } ?>

            </div>


            <div class="row">
              <div class="col-md-12">
                <div class="box">
                  <div class="box-header with-border">
                    <h3 class="box-title">Comparativo Vendas (Data do Dia x Data da Semana Anterior)</h3>

                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                      <!--                 <div class="btn-group">
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                  </ul>
                </div> -->
                      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-8">
                        <!--            <p class="text-center">
                    <strong>Sales: 1 Jan, 2014 - 30 Jul, 2014</strong>
                  </p>
 -->
                        <div class="chart">
                          <!-- Sales Chart Canvas -->

                          <canvas id="salesChart" style="height: 180px;"></canvas>
                        </div>
                        <!-- /.chart-responsive -->
                      </div>
                      <!-- /.col -->
                      <div class="col-md-4">
                        <p class="text-center">
                          <strong>Comparativo Itens Grupo (Data Atual x Data Semana Passada)</strong>
                        </p>

                        <?php $cores = array("aqua", "red", "green", "yellow");
                        foreach ($dadosGraficosAux as $key => $value) {

                          if (intval($value['total_passado']) > intval($value['total_hoje'])) {

                            $porcentagem = ((intval($value['total_hoje']) / intval($value['total_passado'])) * 100);
                          } else {

                            $porcentagem = (((intval($value['total_hoje']) / intval($value['total_passado'])) - 1) * 100);
                          } ?>
                          <div class="progress-group">
                            <span class="progress-text"><?php echo $value['tb1_desc'] ?></span>
                            <span class="progress-number"><b><?php echo $value['total_hoje'] ?></b>/<?php echo $value['total_passado'] ?></span>

                            <div class="progress sm">
                              <div class="progress-bar progress-bar-<?php echo $cores[$key]; ?>" style="width: <?php echo $porcentagem ?>%"></div>
                            </div>
                          </div>
                        <?php } ?>

                      </div>
                      <!-- /.col -->
                    </div>
                    <!-- /.row -->
                  </div>
                  <!-- ./box-body -->
                </div>
                <!-- /.box -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

          </div>

          <div class="col-md-4">


            <!-- USERS LIST -->
            <div class="box box-danger">
              <div class="box-header with-border">
                <h3 class="box-title">Aniversariante do Mês</h3>

                <div class="box-tools pull-right">
                  <span class="label label-danger" id="qtdAniver"></span>
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                  </button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body no-padding">
                <ul class="users-list clearfix" id="aniversariantesdomes">
                
                </ul>
                <!-- /.users-list -->
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-center">
                <a href="javascript:void(0)" class="uppercase">View All Users</a>
              </div>
              <!-- /.box-footer -->
            </div>
            <!--/.box -->
          </div>
          <!-- /.col -->

          <?php
          // REGRA PARA VALIDAÇÃO C | V | E | I
          $cVal = ($entrada[0]->Total) ? $entrada[0]->Total : 0;
          $vVal = ($venda[0]->Total) ? $venda[0]->Total : 0;
          $eVal = ($estoque[0]->Total) ? $estoque[0]->Total : 0;
          $iVal = ($imei[0]->Total) ? $imei[0]->Total : 0;
          $cor  = 'bg-green';
          $texto = '100% Estável';


          if ($eVal > $iVal) {
            // Estoque maior que entrada de IMEI
            $cor = 'bg-red';
            $texto = ($eVal - $iVal) . ' Imei Faltando cadatrar';
          } elseif ($eVal < $iVal) {
            // Estoque menor que entrada de IMEI
            $cor = 'bg-red';
            $texto = ($iVal - $eVal) . ' Estoque diferente da quantidade IMEI';
          } elseif (($eVal == $iVal) and (($eVal + $vVal) == ($cVal))) {
            // Vendas sem remover estoque nem IMEI
            $cor = 'bg-red';
            $texto = ($eVal - $iVal) . ' Imei Faltando cadatrar';
          }

          // Compra maior que entrada de IMEI 



          ?>
          <div class="col-md-4">
            <!-- Info Boxes Style 2 -->
            <!-- /.info-box -->
            <div class="info-box <?php echo $cor ?> ">
              <span class="info-box-icon"><i class="ion ion-ios-heart-outline"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Analise Compra x Venda</span>
                <?php //var_dump($entrada[0]->Total);die(); 
                ?>
                <span class="info-box-number">
                  Com: <?php echo  $cVal; ?>
                  | Ven: <?php echo  $vVal; ?>
                  | Est: <?php echo  $eVal; ?>
                  | Ime: <?php echo  $iVal ?> </span>

                <div class="progress">
                  <div class="progress-bar" style="width: 80%"></div>
                </div>
                <span class="progress-description">
                  <?php echo $texto; ?>
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->

            <div class="info-box bg-yellow">
              <span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">INVENTORY</span>
                <span class="info-box-number">5,200</span>

                <div class="progress">
                  <div class="progress-bar" style="width: 50%"></div>
                </div>
                <span class="progress-description">
                  50% Increase in 30 Days
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <div class="info-box bg-red">
              <span class="info-box-icon"><i class="ion ion-ios-cloud-download-outline"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Downloads</span>
                <span class="info-box-number">114,381</span>

                <div class="progress">
                  <div class="progress-bar" style="width: 70%"></div>
                </div>
                <span class="progress-description">
                  70% Increase in 30 Days
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
            <div class="info-box bg-aqua">
              <span class="info-box-icon"><i class="ion-ios-chatbubble-outline"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Direct Messages</span>
                <span class="info-box-number">163,921</span>

                <div class="progress">
                  <div class="progress-bar" style="width: 40%"></div>
                </div>
                <span class="progress-description">
                  40% Increase in 30 Days
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->

            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Vendas (Vendedor x Intes)</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-md-8">
                    <div class="chart-responsive">
                      <canvas id="pieChart" height="150"></canvas>
                    </div>
                    <!-- ./chart-responsive -->
                  </div>
                  <!-- /.col -->
                  <div class="col-md-4">
                    <ul class="chart-legend clearfix">
                      <?php foreach ($vendedores as $key => $value) {                          
                      ?>
                      <li><i class="fa fa-circle-o text-gray"></i> <?php echo $value['vendedor'].'/'.$value['qtdItens']  ?></li>

                    <?php } ?>
                     <!--  <li><i class="fa fa-circle-o text-green"></i> Iphone Vitrine</li>
                      <li><i class="fa fa-circle-o text-yellow"></i> Xioami Lacrado</li>
                     <li><i class="fa fa-circle-o text-aqua"></i> IMEI</li>
                    <li><i class="fa fa-circle-o text-light-blue"></i> Opera</li>
                    <li><i class="fa fa-circle-o text-gray"></i> Navigator</li> -->
                    </ul>
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- /.box-body -->
              <div class="box-footer no-padding">
                <ul class="nav nav-pills nav-stacked">
                  <?php foreach ($vendedores as $key => $value) { 
                        $porcentagem = number_format( (($value['total'] / $value['custo'])*100)-100  ,2,",","."); // Solicitado por Srº Carlos  
                  ?>

                  <li><a href="#"><?php echo $value['vendedor'].'/'.$value['qtdItens'] ?><span class="pull-right text-green"><i class="fa fa-angle-down"></i> <?php  echo $porcentagem.'%'  ?></span></a></li>
                   <?php } ?>

          <!--         <li><a href="#">Iphone Lacrado
                      <span class="pull-right text-red"><i class="fa fa-angle-down"></i> 12%</span></a></li>
                  <li><a href="#">Iphone Vitrine <span class="pull-right text-green"><i class="fa fa-angle-up"></i> 4%</span></a>
                  </li>
                  <li><a href="#">Xioami Lacrado
                      <span class="pull-right text-yellow"><i class="fa fa-angle-left"></i> 0%</span></a></li> -->
                </ul>
              </div>
              <!-- /.footer -->
            </div>
            <!-- /.box -->

            <!-- PRODUCT LIST -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Recently Added Products</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <ul class="products-list product-list-in-box">
                  <li class="item">
                    <div class="product-img">
                      <img src="dist/img/default-50x50.gif" alt="Product Image">
                    </div>
                    <div class="product-info">
                      <a href="javascript:void(0)" class="product-title">Samsung TV
                        <span class="label label-warning pull-right">$1800</span></a>
                      <span class="product-description">
                        Samsung 32" 1080p 60Hz LED Smart HDTV.
                      </span>
                    </div>
                  </li>
                  <!-- /.item -->
                  <li class="item">
                    <div class="product-img">
                      <img src="dist/img/default-50x50.gif" alt="Product Image">
                    </div>
                    <div class="product-info">
                      <a href="javascript:void(0)" class="product-title">Bicycle
                        <span class="label label-info pull-right">$700</span></a>
                      <span class="product-description">
                        26" Mongoose Dolomite Men's 7-speed, Navy Blue.
                      </span>
                    </div>
                  </li>
                  <!-- /.item -->
                  <li class="item">
                    <div class="product-img">
                      <img src="dist/img/default-50x50.gif" alt="Product Image">
                    </div>
                    <div class="product-info">
                      <a href="javascript:void(0)" class="product-title">Xbox One <span class="label label-danger pull-right">$350</span></a>
                      <span class="product-description">
                        Xbox One Console Bundle with Halo Master Chief Collection.
                      </span>
                    </div>
                  </li>
                  <!-- /.item -->
                  <li class="item">
                    <div class="product-img">
                      <img src="dist/img/default-50x50.gif" alt="Product Image">
                    </div>
                    <div class="product-info">
                      <a href="javascript:void(0)" class="product-title">PlayStation 4
                        <span class="label label-success pull-right">$399</span></a>
                      <span class="product-description">
                        PlayStation 4 500GB Console (PS4)
                      </span>
                    </div>
                  </li>
                  <!-- /.item -->
                </ul>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-center">
                <a href="javascript:void(0)" class="uppercase">View All Products</a>
              </div>
              <!-- /.box-footer -->
            </div>
            <!-- /.box -->
          </div>
          <!-- /.col -->
        </div>


      </section>





      <script type="text/javascript">

        $(function() {
          preencherAniversarios();
          'use strict';
          <?php if (!empty($dadosGraficos)) { ?>
          /* ChartJS
           * -------
           * Here we will create a few charts using ChartJS
           */

          // -----------------------
          // - MONTHLY SALES CHART -
          // -----------------------

          // Get context with jQuery - using jQuery's .get() method.
          var salesChartCanvas = $('#salesChart').get(0).getContext('2d');
          // This will get the first returned node in the jQuery collection.
          var salesChart = new Chart(salesChartCanvas);

          var salesChartData = {
            labels: [<?php foreach ($dadosGraficos[0] as $key => $value) { ?> '<?php echo $key ?>', <?php } ?>],
            datasets: [{
                label: 'Electronics',
                fillColor: 'rgb(210, 214, 222)',
                strokeColor: 'rgb(210, 214, 222)',
                pointColor: 'rgb(210, 214, 222)',
                pointStrokeColor: '#c1c7d1',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgb(220,220,220)',
                data: [<?php foreach ($dadosGraficos[0] as $key => $value) { ?><?php echo $value ?>, <?php } ?>]
              },
              {
                label: 'Digital Goods',
                fillColor: 'rgba(60,141,188,0.9)',
                strokeColor: 'rgba(60,141,188,0.8)',
                pointColor: '#3b8bba',
                pointStrokeColor: 'rgba(60,141,188,1)',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(60,141,188,1)',
                data: [<?php foreach ($dadosGraficos[0] as $key => $value) { ?> <?php echo $value ?>, <?php } ?> ]
              }
            ]
          };

          var salesChartOptions = {
            // Boolean - If we should show the scale at all
            showScale: true,
            // Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines: false,
            // String - Colour of the grid lines
            scaleGridLineColor: 'rgba(0,0,0,.05)',
            // Number - Width of the grid lines
            scaleGridLineWidth: 1,
            // Boolean - Whether to show horizontal lines (except X axis)
            scaleShowHorizontalLines: true,
            // Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines: true,
            // Boolean - Whether the line is curved between points
            bezierCurve: true,
            // Number - Tension of the bezier curve between points
            bezierCurveTension: 0.3,
            // Boolean - Whether to show a dot for each point
            pointDot: false,
            // Number - Radius of each point dot in pixels
            pointDotRadius: 4,
            // Number - Pixel width of point dot stroke
            pointDotStrokeWidth: 1,
            // Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius: 20,
            // Boolean - Whether to show a stroke for datasets
            datasetStroke: true,
            // Number - Pixel width of dataset stroke
            datasetStrokeWidth: 2,
            // Boolean - Whether to fill the dataset with a color
            datasetFill: true,
            // String - A legend template
            legendTemplate: '<ul class=\'<%=name.toLowerCase()%>-legend\'><% for (var i=0; i<datasets.length; i++){%><li><span style=\'background-color:<%=datasets[i].lineColor%>\'></span><%=datasets[i].label%></li><%}%></ul>',
            // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio: true,
            // Boolean - whether to make the chart responsive to window resizing
            responsive: true
          };

          // Create the line chart
          salesChart.Line(salesChartData, salesChartOptions);

         <?php } ?>


          function preencherAniversarios() {

            $.ajax({
              url: "<?php echo base_url(); ?>notas/pegarAniversariantes",
              type: "GET",
              dataType: "json",
              success: function(data) {
                $('#qtdAniver').text(data.qtd + ' Aniversariantes');
                data.dados.forEach(element => {
                  let linha = '<li><img src="' + element.usuario_imagem + '" style="width:125px;" alt="User Image"> <a class="users-list-name" href="#">' + element.usuario_nome + '</a><span class="users-list-date">' + element.usuario_data_nascimento + '</span></li>';

                  $('#aniversariantesdomes').append(linha);
                });
              }
            });
          }

        });


      </script>