<section class="content">
	<div class="row">
		<div class="col-md-6">

		  <div class="box">
            <div class="box-header">
              <i class="fa fa-users"></i><h3 class="box-title">Clientes</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table id="example" class="table" width="100%">
                <tbody><tr>
                  <th style="width: 10px">#</th>
                  <th>Nome</th>
                  <th>CNPJ/CPF</th>
                  <th style="width: 40px">Label</th>
                </tr>
                <?php foreach ($clientes as $value) { ?>
                <tr>
                  <td><?php echo $value->cliente_id; ?></td>
                  <td><?php echo $value->cliente_nome; ?></td>
                  <td><?php echo $value->cliente_cpf_cnpj; ?></td>
                  <td><a href="<?php echo base_url(); ?>clientes/visualizar/<?php echo $value->cliente_id; ?>" data-toggle="tooltip" title="Visualizar"><i class="fa fa-search text-success"></i> </a> </td>
                </tr>
                <?php } ?>
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>

		</div>

		<div class="col-md-6">

		 <div class="box">
            <div class="box-header">
              <i class="fa fa-users"></i><h3 class="box-title">Fornecedores</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
               <table id="example" class="table" width="100%">
                <tbody><tr>
                  <th style="width: 10px">#</th>
                  <th>Nome</th>
                  <th>CNPJ/CPF</th>
                  <th style="width: 40px">Label</th>
                </tr>
                <?php foreach ($fornecedores as $value) { ?>
                <tr>
        				  <td><?php echo $value->fornecedor_id; ?></td>
        				  <td><?php echo $value->fornecedor_nome; ?></td>
        				  <td><?php echo $value->fornecedor_cnpj_cpf; ?></td>
                  <td><a href="<?php echo base_url(); ?>fornecedores/visualizar/<?php echo $value->fornecedor_id; ?>" data-toggle="tooltip" title="Visualizar"><i class="fa fa-search  text-success"></i> </a></i> </td>
                </tr>
                <?php } ?>
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
		</div>
		

	</div>
	<div class="row">

		<div class="col-md-6">
		  <div class="box">
            <div class="box-header">
              <i class="fa fa-shopping-cart"></i><h3 class="box-title">Vendas</h3>
            </div>
            <!-- /.box-header -->
	        <div class="box-body no-padding">
	            <table id="example" class="table" width="100%">
	                <tbody><tr>
	                  <th>N° da Nota</th>
	                  <th>Cliente</th>
	                  <th>Vendedor</th>
	                  <th style="width: 10px">Label</th>
	                </tr>
	                <?php foreach ($vendas as $value) { 
                    $value->venda_visivel == 0 ? $label = 'danger' : $label = 'success';
                     ?>
	                <tr>          				  
                    <td><span class="label label-<?php echo $label  ?>"><?php echo ucfirst($value->idVendas) ?></span></td>
          				  <td><?php echo $value->cliente_nome; ?></td>
          				  <td><?php echo $value->usuario_nome; ?></td>
	                  <td><a href="#" onClick="(function(){window.open('<?php echo base_url(); ?>vendas/visualizarNota/<?php echo $value->idVendas.'/'.$label ?>','MyWindow','toolbar=no,location=no,directories=no,status=no,menubar=yes,scrollbars=yes,resizable=yes,width=350,height=600');return false;})();return false;"data-toggle="tooltip" title="Visualizar"><i class="fa fa-search  text-success"></i> </a></td>

	                </tr>
	                <?php } ?>
	              </tbody></table>
	          </div>
	            <!-- /.box-body -->
	        </div>
       </div>

      <div class="col-md-6">
		 <div class="box">
            <div class="box-header">
              <i class="fa fa-money"></i><h3 class="box-title">Financeiro</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table id="example" class="table" width="100%">
                <tbody><tr>
                  <th>#</th>
                  <th>Cliente/Fornecedor</th>
                  <th>Tipo</th>
                  <th>Status</th>
                  <th style="width: 10px">Label</th>
                </tr>
                <?php foreach ($financeiro as $value) {
                if($value->financeiro_baixado == 0){$status = 'Pendente';}else{ $status = 'Pago';}; 
                if($value->financeiro_tipo == 'receita'){ $label = 'success'; } else{$label = 'danger';  } ?>
                <tr>
        				  <td><?php echo $value->idFinanceiro; ?></td>
        				  <td><?php echo $value->financeiro_forn_clie; ?></td>
        				  <td><span class="label label-<?php echo $label  ?>"><?php echo ucfirst($value->financeiro_tipo) ?></span></td>
        				  <td><?php echo $status; ?></td>
                  <td><a href="<?php echo base_url(); ?>financeiro/index/1/0/0/<?php echo $value->clientes_id; ?>/0/<?php echo $value->vendas_id; ?>" data-toggle="tooltip" title="Vizualizar"><i class="fa fa-search text-success"></i></a> </td>
                </tr>
                <?php } ?>
              </tbody></table>
            </div>
            <!-- /.box-body -->
	      </div>
	    </div>


	 </div>
   <div class="row">

    <div class="col-md-12">
      <div class="box">
        <div class="box-header">
          <i class="fa fa-users"></i><h3 class="box-title">Produtos</h3>
        </div>
          <!-- /.box-header -->
        <div class="box-body no-padding">
            <table id="example" class="table" width="100%">
                <tbody><tr>
                  <th>Código</th>
                  <th>Descrição</th>

                  <?php foreach ($lojas as $value) {  ?>  
                  <th>E. <?php echo $value->loja; ?></th>  
                  <?php  } ?>
             
                  <th>P M Venda</th>
                  <th>P. Venda</th>
                  <th style="width: 10px">Label</th>
                </tr>
                

               
                <?php foreach ($produtos as $key => $p) { ?>
                
                <tr>
                  <td><?php echo $p->produto_codigo; ?></td>
                  <td><?php echo $p->produto_descricao; ?></td>

                  <?php foreach ($lojas as $l) { 
                   $valor = 'estoque_'.$l->loja ?> 
                
                  <td><?php echo $p->$valor ?></td>
                  <?php  } ?>

                  <td><?php echo $p->produto_preco_minimo_venda; ?></td>
                  <td><?php echo $p->produto_preco_venda; ?></td>
                  <td><a href="<?php echo base_url(); ?>produto/editar/<?php echo $p->produto_id; ?>" data-toggle="tooltip" title="Visualizar"><i class="fa fa-search text-success"></i> </a> </td>

                </tr>
                <?php } ?>
              </tbody></table>
          </div>
          <!-- /.box-body -->
      </div>
    </div>



   </div>
   
   
   
   
   <div class="row">

    <div class="col-md-6">
      <div class="box">
        <div class="box-header">
          <i class="fa fa-users"></i><h3 class="box-title">Movimentação Mercadoria - Produto</h3>
        </div>
          <!-- /.box-header -->
        <div class="box-body no-padding">
            <table id="example" class="table" width="100%">
                <tbody><tr>
                  <th>Código</th>
                  <th>Tipo</th>
                  <th>Data</th>
                  <th>Descrição</th>
                  <th>Quantidade</th>                
                  <th style="width: 10px">Label</th>
                </tr>
				
                <?php foreach ($prodMercadoria as $value) { ?>
                <tr>
                  <td><?php echo $value->movimentacao_produto_id; ?></td>
                  <td><?php echo $value->movimentacao_produto_tipo; ?></td>
                  <td><?php echo $value->movimentacao_produto_data_cadastro; ?></td>
                  <td><?php echo $value->produto_descricao; ?></td>
                  <td><?php echo $value->item_movimentacao_produto_quantidade; ?></td>
                  <td><a href="<?php echo base_url(); ?>movimentacao/visualizar/<?php echo $value->movimentacao_produto_id; ?>" data-toggle="tooltip" title="Visualizar"><i class="fa fa-search text-success"></i> </a> </td>

                </tr>
                <?php } ?>
              </tbody></table>
          </div>
          <!-- /.box-body -->
      </div>
    </div>

      <div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <i class="fa fa-users"></i><h3 class="box-title">Ajuste de Estoque - Produto</h3>
            </div>
            <!-- /.box-header -->
          <div class="box-body no-padding">
              <table id="example" class="table" width="100%">
                  <tbody><tr>
                    <th>#</th>
                    <th>Data</th>
                    <th>Produto</th>
                    <th>Qtd</th>     
                    <th>Qtd Atual</th>               
                    <th style="width: 10px">Label</th>
                  </tr>
                  <?php foreach ($prodAjusteEstoque as $value) { ?>
                  <tr>
                    <td><?php echo $value->estoque_ajuste_id; ?></td>
                    <td><?php echo $value->estoque_ajuste_data; ?></td>
                    <td><?php echo $value->produto_descricao; ?></td>
                    <td><?php echo $value->estoque; ?></td>
                    <td><?php echo $value->quantidade_atual; ?></td> 
                    


                    <td><a href="<?php echo base_url(); ?>estoque/visualizar/<?php echo $value->estoque_ajuste_id; ?>" data-toggle="tooltip" title="Visualizar"><i class="fa fa-search text-success"></i> </a> </td>

                  </tr>
                  <?php } ?>
                </tbody></table>
            </div>
              <!-- /.box-body -->
          </div>
       </div>

   </div>

  <div class="row">

    <div class="col-md-6">
      <div class="box">
        <div class="box-header">
          <i class="fa fa-users"></i><h3 class="box-title">Estorno - Produto</h3>
        </div>
          <!-- /.box-header -->
        <div class="box-body no-padding">
            <table id="example" class="table" width="100%">
                <tbody><tr>
                  <th>Código</th>
                  <th>Data</th>                  
                  <th>Descrição</th>
                  <th>Quantidade</th>    
                  <th>Motivo</th> 
                  <th style="width: 10px">Label</th>
                </tr>
                <?php foreach ($prodEstornado as $value) { ?>
                <tr>
                  <td><?php echo $value->produto_avaria_id; ?></td>
                  <td><?php echo $value->produto_avaria_cadastro; ?></td>
                  <td><?php echo $value->produto_descricao; ?></td>
                  <td><?php echo $value->produto_avarias_quantidade; ?></td>
                  <td><?php echo $value->produto_avaria_motivo; ?></td>
                  <td><a href="<?php echo base_url(); ?>produtoavaria/visualizar/<?php echo $value->produto_avaria_id; ?>" data-toggle="tooltip" title="Visualizar"><i class="fa fa-search text-success"></i> </a> </td>

                </tr>
                <?php } ?>
              </tbody></table>
          </div>
          <!-- /.box-body -->
      </div>
    </div>

    <div class="col-md-6">
      <div class="box">
        <div class="box-header">
          <i class="fa fa-users"></i><h3 class="box-title">Produtos na ultimas vendas</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
          <table id="example" class="table" width="100%">
              <tbody><tr>
                <th>Nº</th>
                <th>Data</th>
                <th>Produto</th>
                <th>Qtd</th>                   
                <th style="width: 10px">Label</th>
              </tr>
              <?php foreach ($produtoVenda as $value) { ?>
              <tr>
                <td><?php echo $value->idVendas; ?></td>
                <td><?php echo $value->dataVenda; ?></td>
                <td><?php echo ($value->imei_valor <> '' ) ? $value->produto_descricao.' - '.$value->imei_valor :  $value->produto_descricao ?></td>
                <td><?php echo $value->quantidade; ?></td>                   
                <td><a href="<?php echo base_url(); ?>vendas/editar/<?php echo $value->idVendas; ?>" data-toggle="tooltip" title="Visualizar"><i class="fa fa-search text-success"></i> </a> </td>

              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
          <!-- /.box-body -->
      </div>          
    </div>

  </div>


  <div class="row">

    <div class="col-md-6">
      <div class="box">
        <div class="box-header">
          <i class="fa fa-users"></i><h3 class="box-title">OS</h3>
        </div>
          <!-- /.box-header -->
        <div class="box-body no-padding">
            <table id="example" class="table" width="100%">
                <tbody><tr>
                  <th>Nº</th>
                  <th>Marca</th>                  
                  <th>Modelo</th>
                  <th>Serie</th>    
                  <th>Técnico</th> 
                  <th style="width: 10px">Label</th>
                </tr>
                <?php foreach ($os as $value) { ?>
                <tr>
                  <td><?php echo $value->os_id; ?></td>
                  <td><?php echo $value->os_marca; ?></td>
                  <td><?php echo $value->os_modelo; ?></td>
                  <td><?php echo $value->os_serie; ?></td>
                  <td><?php echo $value->usuario_nome; ?></td>
                  <td><a href="<?php echo base_url(); ?>ordemservico/visualizar/<?php echo $value->os_id; ?>" data-toggle="tooltip" title="Visualizar"><i class="fa fa-search text-success"></i> </a> </td>

                </tr>
                <?php } ?>
              </tbody></table>
          </div>
          <!-- /.box-body -->
      </div>
    </div>

    <div class="col-md-6">
      <div class="box">
        <div class="box-header">
          <i class="fa fa-users"></i><h3 class="box-title">Serviço</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
          <table id="example" class="table" width="100%">
              <tbody><tr>
                <th>Nº</th>
                <th>Data</th>
                <th>Produto</th>
                <th>Qtd</th>                   
                <th style="width: 10px">Label</th>
              </tr>
              <?php foreach ($produtoVenda as $value) { ?>
              <tr>
                <td><?php echo $value->idVendas; ?></td>
                <td><?php echo $value->dataVenda; ?></td>
                <td><?php echo ($value->imei_valor <> '' ) ? $value->produto_descricao.' - '.$value->imei_valor :  $value->produto_descricao ?></td>
                <td><?php echo $value->quantidade; ?></td>                   
                <td><a href="<?php echo base_url(); ?>vendas/editar/<?php echo $value->idVendas; ?>" data-toggle="tooltip" title="Visualizar"><i class="fa fa-search text-success"></i> </a> </td>

              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
          <!-- /.box-body -->
      </div>          
    </div>

  </div>
   
</section>