<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <div class="col-md-2">
        <?php if(verificarPermissao('aAlmoxarifado')){ ?>
        <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/adicionar" type="button" class="btn btn-block btn-default btn-flat">Cotação de Compra</a>
        <?php } ?>
      </div>          
    </div>
    <div class="box-body">
      <div class="container-fluid">
        <table id="exampleC" class="table" width="100%">
          <thead>
            <tr>                                            
              <th>Código</th>
              <th>Categoria</th>
              <th>Período</th>
              <th>Situação</th>              
              <th>Ações</th>
            </tr>
          </thead>
          <tbody>

            <?php foreach ($dados as $dado) { ?>
            <tr>                      
              <td><?php echo $dado->id; ?></td>
              <td><?php echo $dado->categoria; ?></td> 
              <td>De: <?php echo $dado->data_inicio.' a '.$dado->data_final; ?></td> 
              <td><?php echo $dado->situacao == 'Preparada'? $dado->situacao: $dado->produtos_adicionados.'/'.$dado->produtos_totais; ?></td> 
              <td>

                <?php if(verificarPermissao('eAlmoxarifado') && $dado->situacao == 'Preparada'){ ?>
                  <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/situacaocotacao/<?php echo $dado->id; ?>" data-toggle="tooltip" title="Cotação"><i class="fa fa-cart-plus text-info"></i> </a>
                <?php } ?>

                <?php if(verificarPermissao('vAlmoxarifado')){ ?>
                  <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/visualizar/<?php echo $dado->id; ?>" data-toggle="tooltip" title="Visualizar"><i class="fa fa-search  text-success"></i> </a>
                <?php } ?>

                <?php if(verificarPermissao('eAlmoxarifado') && $dado->situacao != 'Preparada'){ ?>
                  <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/editar/<?php echo $dado->id; ?>" data-toggle="tooltip" title="Editar"><i class="fa fa-edit"></i> </a>
                <?php } ?>

                <?php if(verificarPermissao('dAlmoxarifado')){ ?>
                 <a style="cursor:pointer;" title="Deletar" data-toggle="modal" data-target="#excluir" onclick="excluir('<?php echo base_url().$this->uri->segment(1)."/excluir/".$dado->id; ?>');" ><i class="fa fa-trash  text-danger"></i></a>
                <?php } ?>

              </td>
            </tr>
            <?php } ?>
          </tbody>
          <tfoot>
            <tr>                     
              <th>Código</th>
              <th>Categoria</th>
              <th>Período</th>
              <th>Situação</th>              
              <th>Ações</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</section>


<script type="text/javascript">
  // Basic example
$(document).ready(function () {
$('#exampleC').DataTable({
"ordering": false // false to disable sorting (or any other option)
});

});


</script>