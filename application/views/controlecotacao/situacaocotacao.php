<?php 
  $valorTotal = 0;
?>
<style type="text/css">
  .wrapper1, .wrapper2 { width: 100%; overflow-x: scroll; overflow-y: hidden; }
  .wrapper1 { height: 20px; }
  .wrapper2 {}
  .scrol1 { height: 20px; }
  .scroll2 { overflow: none; }
</style>
<section class="content">
  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="" method="POST">                
          <div class="box-body">

            <div class="row">
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <!-- <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 17%</span> -->
                    <h5 class="description-header"><?php echo $produtos[0]->categoria; ?></h5>
                    <span class="description-text">GRUPO/CATEGORIA</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <!-- <span class="description-percentage text-yellow"><i class="fa fa-caret-left"></i> 0%</span> -->
                    <h5 class="description-header">De: <?php echo $produtos[0]->dataInicio.' a '.$produtos[0]->dataFim ?></h5>
                    <span class="description-text">DATA-GERAÇÃO</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <!-- <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 20%</span> -->
                    <h5 class="description-header"><?php echo $produtos[0]->cotacao_compra_observacao; ?></h5>
                    <span class="description-text">OBSERVAÇÃO</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block">
                    <!-- <span class="description-percentage text-red"><i class="fa fa-caret-down"></i> 18%</span> -->
                    <h5 class="description-header"><?php echo $produtos[0]->funcionario ?></h5>
                    <span class="description-text">FUNCIONÁRIO</span>
                  </div>
                  <!-- /.description-block -->
                </div>
              </div>
          </div>

        </form>

        <fieldset class="col-md-12">
          <legend>Listagem fornecedores</legend>
          <div class="col-md-12">

          </div>
        </fieldset>
        

      <div class="row">

        <?php 
        $idCotacao = $this->uri->segment(3);
        foreach ($fornecedor as $f) {  ?>
        <!-- /.col -->
        <div class="col-md-4">
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-aqua-active">
              <h3 class="widget-user-username"><?php echo $f->fornecedor_nome ?></h3>
              <h5 class="widget-user-desc"><?php echo $f->fornecedor_email ?></h5>
            </div>
            <div class="widget-user-image">
              <img class="img-circle" src="<?php echo base_url(); ?>assets/dist/img/avatar5.png" alt="User Avatar">
            </div>
            <div class="box-footer">
              <div class="row">
                <div class="col-sm-4 border-right">
                  <div class="description-block">
                    <h5 class="description-header"><a href="<?php ?>" title="URL Cotação" data-toggle="modal" data-target="#modal-default" fornecedor="<?php echo $f->cotacao_fornecedor_forn_id.'/'.$idCotacao  ?>"><i class="fa fa-share-alt text-info"></i> </a></h5>
                    <span class="description-text">LINK</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-4 border-right">
                  <div class="description-block">
                    <h5 class="description-header"><?php echo number_format(($f->total ), 2,'.','') ?></h5>
                    <span class="description-text">TOTAL</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-4">
                  <div class="description-block">
                    <h5 class="description-header"><?php echo $f->feito.'/'.$f->qtd ?></h5>
                    <span class="description-text">PRODUTOS</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
          </div>
          <!-- /.widget-user -->
        </div>
        <!-- /.col -->
      <?php } ?>
      </div>


        <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Listagem GERAL - COTAÇÃO</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="wrapper1" sytle='width: 100%; overflow-x: scroll; overflow-y: hidden;'>
                <div class="scrol1"></div>
              </div>
              <div class="wrapper2">
                <div class="scroll2">
                  <table class="table table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                      <tr>                 
                        <?php

                        $colunas = array();
                        $novoArray = reset($listagemCotacaoGeral);
                        foreach($novoArray as $key => $value){
                        $colunas[] = $key; ?>
                           <th><?php echo "$key" ?></th>
                        <?php } ?>
                      </tr>

                      <?php foreach ($listagemCotacaoGeral as $l) { ?>
                        <tr>                    
                          <?php for ($i=0; $i < count($colunas) ; $i++) { 
                            $arr = get_object_vars($l);
                            if($arr[$colunas[$i]] == '9999.00'){
                              $valor = '';
                            }else{
                              $valor = $arr[$colunas[$i]];
                            }
                            
                            if ($valor == $l->minimo) {
                              echo '<td><span class="badge bg-green"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">'.$valor.'</font></font></span></td>';
                            }else{
                            echo '<td>'.$valor.'</td>';
                            }

                          } ?>
                        </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
           <div class="box-footer">                
                <!-- <iframe name="iframe_download" class="hidden"></iframe>
                <a href="<?php echo base_url()?>/assets/arquivos/planilhas/cotacao_compra.xlsx"  class="btn btn-success pull-right" target="iframe_download">Importar Gridr</a> -->
                <a href="<?php echo base_url().'controlecotacao/situacaoCotacaoExcel/'.$this->uri->segment(3); ?>" class="btn btn-success pull-right">Exportação Grid</a>  
              </div>
          </div>
          <!-- /.box -->



      </div>
    </div>
  </div>
</section>



  <div class="modal fade" id="modal-default">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Gerador de link - Cotação preço</h4>
        </div>

        <div class="modal-body">
          <div class="alert alert-info alert-styled-left text-blue-800 content-group">
                    <span class="text-semibold">Instruções!</span> Siga o exemplo abaixo.
                    <button type="button" class="close" data-dismiss="alert">×</button>
                </div>              
                   
          <h6 class="text-semibold"><i class="icon-embed2 position-left"></i> Copie o cód abaixo e compartilhe com seu vendedor </h6>
          
          <pre class="language-markup code-toolbar">
            <code class=" language-markup" id="iframe">                           
              <!-- Cód javaScript -->
            </code>       
          </pre>
          <hr>

          <hr>

          <p><i class="icon-mention position-left"></i>Obs: Maximo de atenção para não liberar o link para vendedor diferente.</p>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>          
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->



<script type="text/javascript">
$(document).ready(function(){


   $(document).on('click', 'a', function(event) {
        
      var fornecedor = $(this).attr('fornecedor');

      var span = '<span> <?php echo base_url(); ?>cotacaofornecedor/carregarcotacao/'+fornecedor+'</span>';

        
    document.getElementById("iframe").innerHTML = span;


    });

});

</script>

<script type="text/javascript">
  $(function () {
    $('.wrapper1').on('scroll', function (e) {
        $('.wrapper2').scrollLeft($('.wrapper1').scrollLeft());
    }); 
    $('.wrapper2').on('scroll', function (e) {
        $('.wrapper1').scrollLeft($('.wrapper2').scrollLeft());
    });
  });
  $(window).on('load', function (e) {
      $('.scrol1').width($('table').width());
      $('.scroll2').width($('table').width());
  });
</script>
