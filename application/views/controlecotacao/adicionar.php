<section class="content">
  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="<?php echo current_url(); ?>" method="post">
          <div class="box-body">

                <div class="form-group">
                  <label for="cotacao_compra_categoria_id" class="col-sm-2 control-label">Grupo Produto</label>
                  <div class="col-sm-5">                            
                    <select id="cotacao_compra_categoria_id" class="form-control" data-placeholder="Selecione o grupo de produtos" name="cotacao_compra_categoria_id">
                      <!-- <option value="">Selecione</option> -->
                      <option value="0">TODOS</option>
                      <?php foreach ($grupo as $g) { ?>
                      <option value='<?php echo $g->categoria_prod_id; ?>'><?php echo $g->categoria_prod_descricao; ?> </option>
                      <?php } ?>                       
                    </select>  
                  </div>
                </div>


          <div class="form-group">
            <label for="loja" class="col-sm-2 control-label">Fornecedor </label>
            <div class="col-sm-5">              
            <select id="cotacao_compra_fornecedor" class="form-control select2" multiple="multiple" data-placeholder="Selecione os fornecedores" name="cotacao_compra_fornecedor[]">                      
             </select>
             </div>
          </div> 


       
                <div class="form-group">
                  <label for="cotacao_compra_observacao" class="col-sm-2 control-label">Observação </label>
                  <div class="col-sm-5">              
                    <input type="text" class="form-control" name="cotacao_compra_observacao" id="cotacao_compra_observacao" value="<?php echo set_value('cotacao_compra_observacao'); ?>" placeholder="Observacao">
                  </div>
                </div> 

                <div class="form-group">
                  <label for="cotacao_compra_simulacao_data_inicio" class="col-sm-2 control-label">Data Inicial </label>
                  <div class="col-sm-2">              
                    <input type="date" class="form-control" name="cotacao_compra_simulacao_data_inicio"  value="<?php echo set_value('cotacao_compra_simulacao_data_inicio'); ?>" >
                  </div>

                  <label for="cotacao_compra_simulacao_data_final" class="col-sm-1 control-label">Data Final </label>
                  <div class="col-sm-2">              
                    <input type="date" class="form-control" name="cotacao_compra_simulacao_data_final" value="<?php echo set_value('cotacao_compra_simulacao_data_final'); ?>">
                  </div>
                </div> 
  
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>" class="btn btn-default">Voltar</a>
            <button type="submit" class="btn btn-primary pull-right"><?php echo ucfirst($this->uri->segment(2)); ?></button>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
  </div>
</section>


<script type="text/javascript">

  $(document).ready(function(){

    $("#cotacao_compra_fornecedor").select2({
        maximumSelectionLength: 10,
        language: {
            maximumSelected: function (e) {
                return "Você atingiu o limite de " + e.maximum + " itens";
            }
        }
    });

    var grupo = $('#cotacao_compra_categoria_id').val();

       $.ajax({
          method: "POST",
          url: base_url+"controlecotacao/selecionarFornecedor/",
          dataType: "html",
          data: { grupo: grupo}
          })
          .done(function( response ) {

          $('#cotacao_compra_fonecedor').html(response);
        
       });
   
  });


  $('#cotacao_compra_categoria_id').change(function(){
   
      var grupo = $('#cotacao_compra_categoria_id').val();

       $.ajax({
          method: "POST",
          url: base_url+"controlecotacao/selecionarFornecedor/",
          dataType: "html",
          data: { grupo: grupo}
          })
          .done(function( response ) {

          $('#cotacao_compra_fornecedor').html(response);
        
       });
   
  });

</script>


<script type="text/javascript">  

  $(function () {


    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })

  })


</script>