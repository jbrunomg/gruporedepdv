<?php 
  $valorTotal = 0;
?>

<section class="content" style="height:100%">
  <input type="hidden" id="idCompra" value="<?php echo $this->uri->segment(3); ?>">

  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="" method="POST">                
          <div class="box-body">

            <div class="row">
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <!-- <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 17%</span> -->
                    <h5 class="description-header"><?php echo $produtos[0]->categoria; ?></h5>
                    <span class="description-text">GRUPO/CATEGORIA</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <!-- <span class="description-percentage text-yellow"><i class="fa fa-caret-left"></i> 0%</span> -->
                    <h5 class="description-header">De: <?php echo $produtos[0]->dataInicio.' a '.$produtos[0]->dataFim ?></h5>
                    <span class="description-text">DATA-GERAÇÃO</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <!-- <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 20%</span> -->
                    <h5 class="description-header"><?php echo $produtos[0]->cotacao_compra_observacao; ?></h5>
                    <span class="description-text">OBSERVAÇÃO</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block">
                    <!-- <span class="description-percentage text-red"><i class="fa fa-caret-down"></i> 18%</span>
                    <h5 class="description-header">1200</h5>
                    <span class="description-text">ATUALIZADO</span> -->
                  </div>
                  <!-- /.description-block -->
                </div>
              </div>
          </div>

        </form>

  

        <fieldset class="col-md-12">
            <legend>Dados dos Produtos</legend>
        </fieldset>     

          <div class="box-footer">
                        
              <!-- /.box-header -->
              <div id="divProdutos" class="box-body no-padding" style="overflow-y:scroll; height:500px; width:100%;">
                <table class="table table-bordered table-striped" id="tbProduto">
                  <thead>
                    <tr>                      
                      <th>Produto</th>                     
                      <th>Preço Custo</th>              
                      <th>Quantidade</th>
                      <th>Total</th>              
                    </tr>
                  </thead>  
                  <tbody>
                    <div>

                    <?php foreach ($produtos as $produto) { ?>
                      <tr>                        
                        <td><?php echo $produto->produto_descricao; ?></td>
                        <td><?php echo $produto->produto_preco_custo; ?></td>
                        <td><?php echo $produto->cotacao_compra_quantidade; ?></td>                   
                      
                        <td>
                          <?php 
                            $soma = number_format(($produto->produto_preco_custo * $produto->cotacao_compra_quantidade), 2,'.','');
                            $valorTotal += $soma; 
                          ?>
                          <p id="total_<?php echo $produto->produto_id?>"><?php echo $soma; ?></p>
                        </td>
                      <tr>
                    <?php } ?>

                    </div>
                  </tbody>
                </table>                
              </div>
              <div class=""></div>
     
              <!-- /.box-body -->
              <div class="box-footer">                
                <h1 class="pull-right">R$ <b id="qtdtotal"><?php echo number_format($valorTotal, 2, '.', ''); ?></b></h1>
                <a href="<?php echo base_url().'controlecotacao/prepararCotacaovendedor/'.$this->uri->segment(3); ?>" class="btn btn-primary">Preparar Cotação</a>
              </div>
              <!-- /.box-footer -->      

          </div>

         

      </div>
    </div>
  </div>
</section>


<script type="text/javascript">

  $(document).ready(function () {
    let total = $('#qtdtotal').text();

    if(total <= 0){ 
      $("#btnPrepararCompra").hide();
    } else {
      $("#btnPrepararCompra").show();
    }
  });

</script>