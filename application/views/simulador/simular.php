
<!-- Table with togglable columns -->
<section class="content">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h5 class="box-title">Simular Compra</h5>
				<div class="box-tools pull-right">
					<!-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
					</button>
					<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
				</div>
		</div>
        <div class="box-body">
            <form action="<?php echo base_url(); ?>simulador/simular" method="post">

                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />

                <div class="panel-body" >

                    <div class="row">
                        <div class="form-group col-md-3">
                            <label for="vlprod">Digitar Valor da Compra:*</label>
                            <input type="text"  name="vlprod" autocomplete="off" id="vlprod"  class="form-control form-control-sm money" placeholder="0,00" maxlength="12">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="vlentr">Digitar Valor da Entrada em Espécie:</label>
                            <input type="text" name="vlentr" autocomplete="off" id="vlentr" class="form-control form-control-sm money"  placeholder="0,00"  maxlength="12">
                        </div>
                        <div class="from-group col-md-3">
                            <label for="data_nascimento">Limite Máximo de Parcelas:</label>
                            <input type="number" id="qtparc" name="qtparc"  value="18" placeholder="" class="form-control form-control-sm"  maxlength="2" readonly="" />
                        </div>
                    </div>
                </div>

                <div class="form-actions">
                    <div class="span12 text-right">
                        <input type="reset" class="btn" value="Limpar" />
                        <button type="submit" class="btn btn-success"><i class="icon-credit-card icon-white"></i> Calcular</button>
                    </div>
                </div>

            </form>
        </div>
    </div>

    <div class="alert alert-info bg-white alert-styled-left alert-arrow-left alert-dismissible">
        <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
        <h6 class="alert-heading font-weight-semibold mb-1">Static top navbar</h6>
        As taxas e condições informadas podem sofrer alteração a qualquer tempo sem prévio aviso..
    </div>
</section>

    <!-- <div align=center>
        <strong>SIMULADOR DE PARCELAS</strong>
    </div> -->

<?php if(isset($resultado) ){ ?>
    <section class="content">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h5 class="box-title">Simulação</h5>
            </div>

            <div class="box-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="highlighted-justified-tab1" >

                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="vlprod">Valor da Compra:*</label>
                                    <input type="text"  id="vlprod" name="vlprod" autocomplete="off"  class="form-control form-control-sm money" value="<?php echo number_format($simulado['valorCompra'],2,',','.'); ?>" disabled="">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="vlentr">Valor da Entrada em Espécie:</label>
                                    <input type="text" id="vlentr" name="vlentr" autocomplete="off"  class="form-control form-control-sm money"  value="<?php echo number_format($simulado['valorEntrada'],2,',','.'); ?>" disabled="">
                                </div>
                                <div class="from-group col-md-4">
                                    <label for="data_nascimento">Pagamento Restante em Espécie:*</label>
                                    <input type="text" id="qtparc" name="qtparc" autocomplete="off"  class="form-control form-control-sm"  value="<?php echo number_format($simulado['valorSimular'],2,',','.') ?>" disabled=""  />
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="vlprod">Pagamento Restante em Débito:*</label>
                                    <input type="text"  name="vlprod" autocomplete="off" id="vlprod"  class="form-control form-control-sm money" value="<?php echo (number_format($resultado[0]->valor_parcela + $simulado['valorSimular'],2,',','.') ) ?>" disabled="" >
                                </div>
                                <div class="from-group col-md-4">
                                    <label for="data_nascimento">Quantidade Máxima de Parcelas:</label>
                                    <input type="number" id="qtparc" name="qtparc"  value="18" placeholder="" class="form-control form-control-sm"  maxlength="2" disabled="" />
                                </div>
                            </div>


                            <h2 class="text-center fontbold">Opções de Parcelamento</h2>

                            <table class="table table-bordered">
                                <thead>
                                    <tr style="background-color: #2D335B; color: white;">
                                        <th colspan="1" class="columns01 col_default sorting_asc text-center">Parcelas</th>
                                        <th colspan="4" class="columns01 col_default sorting_asc text-center">Valor da Parcela</th>
                                        <th colspan="4" class="columns01 col_default sorting_asc text-center">Total da Compra</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (empty($resultado)) { ?>
                                        <tr>
                                            <td colspan="9" class="text-center">Nenhuma simulação cadastrada</td>
                                        </tr>
                                    <?php } else {
                                        foreach ($resultado as $v) {
                                            if ($v->tipo_parcela == 'debito') {
                                                continue;
                                            }

                                            $vlParcela = ($simulado['valorSimular'] + $v->valor_parcela) / $v->numero_parcela;

                                            if ($v->tipo_parcela == 'pix') {
                                                $vlParcela = $simulado['valorSimular'] + $v->valor_parcela;
                                            }
                                    ?>
                                        <tr>
                                            <td colspan="1" class="text-center"><?php echo htmlspecialchars($v->tipo_parcela); ?></td>
                                            <td colspan="4" class="text-center"><?php echo number_format($vlParcela, 2, ',', '.'); ?></td>
                                            <td colspan="4" class="text-center"><?php echo number_format($simulado['valorSimular'] + $v->valor_parcela, 2, ',', '.'); ?></td>
                                        </tr>
                                    <?php
                                        }
                                    } ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="text-right">
                            <a onclick="#" href="<?php echo base_url(); ?>simulador" class="btn btn-warning ">Retornar</a>
                        </div>
                        <br>

                    </div>
                <!-- </div> -->
            </div>
        </div>
    </section>
<?php  } ?>


<!--
<script src="<?php // echo base_url()?>js/jquery.validate.js"></script>
<script src="<?php // echo base_url();?>public/assets/js/maskmoney2.js"></script>
<script src="<?php // echo base_url();?>assets/js/jquery.mask.js"></script>
<script type="text/javascript">

    $(document).ready(function(){
        $(".money").maskMoney();

    });
      // $(document).ready(function(){

      //  $(".money").maskMoney();

      //  $('#formSimulador').validate({
      //   rules : {
      //       vlprod:{ required: true},

      //   },
      //   messages: {
      //       vlprod :{ required: 'Campo Requerido.'}
      //   },

      //   errorClass: "help-inline",
      //   errorElement: "span",
      //   highlight:function(element, errorClass, validClass) {
      //       $(element).parents('.control-group').addClass('error');
      //   },
      //   unhighlight: function(element, errorClass, validClass) {
      //       $(element).parents('.control-group').removeClass('error');
      //       $(element).parents('.control-group').addClass('success');
      //   }
      //  });


      // });
</script> -->




