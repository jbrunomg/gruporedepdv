
<!-- Table with togglable columns -->
<section class="content">
	<div class="box box-primary">
		<div class="box-header">
			<h5 class="box-title">Configurar Taxas do Cartão (Débito e Crédito)</h5>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
					</button>
					<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
				</div>
		</div>

		<div class="box-body">
			<form action="<?php echo base_url(); ?>simulador/editarTaxasExe" method="post">
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>"
					value="<?php echo $this->security->get_csrf_hash(); ?>" />

				<table class="table table-hover">
					<thead>
						<tr>
							<th class="columns01 col_default sorting_asc">#</th>
							<th class="columns01 col_default sorting_asc text-center">Tipo</th>
							<th class="columns01 col_default sorting_asc">
								<div style="display: flex;justify-content: end;align-items: center;padding-right: 38px;">
									Taxa %
								</div>
							</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($taxas as $taxa) { ?>
							<tr >
								<td><?php echo $taxa->numero_parcela; ?></td>
								<td style="text-align: center;"><?php echo $taxa->tipo_parcela; ?></td>
								<td >
								<div style="display: flex;justify-content: end;align-items: center;">
									<input type="text" class="money" style="width: 100px;" placeholder="0,00"
										value="<?php echo $taxa->percentual_parcela; ?>" name="<?php echo $taxa->idSimulador; ?>">&nbsp;%
								</div>
								</td>
							</tr>
						<?php } ?>

						<tr>
							<td colspan="3">
								<div class="text-right">
									<button type="submit" class="btn bg-teal">Configurar<i
											class="icon-arrow-right14 position-right"></i></button>
								</div>
							</td>
						</tr>

					</tbody>
				</table>
			</form>
		</div>
	</div>
</section>