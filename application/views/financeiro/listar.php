  <style>
    .ui-front{
      z-index: 1051;
    }
  </style>
<br><br><section class="content">
  <div class="box">
  <div class="box-header with-border">
    <div class="col-md-12 row">
        <?php if(verificarPermissao('eFinanceiro')){ ?>
        <a data-toggle="modal" data-target="#modalReceita"  role="button" class="btn btn-success tip-bottom" title="Cadastrar nova receita"><i class="fa fa-plus icon-white"></i> Nova Receita</a>  
        <a href="#modalDespesa" data-toggle="modal" role="button" class="btn btn-danger tip-bottom" title="Cadastrar nova despesa"><i class="fa fa-plus icon-white"></i> Nova Despesa</a>
        <?php } ?>
     </div>

     <div> &nbsp </div>
     <div> &nbsp </div>

     <div class="col-md-12 row">
       <form action="<?php echo base_url(); ?>financeiro/index/">
         <div class="box-body">
              <div class="row">

                <div class="col-xs-3">
                  <div class="form-group">
                    <label for="cliente">Cliente / Fornecedor</label>
                    <input type="text" class="form-control" name="cliente" id="clie_forn" value="<?php echo set_value('cliente'); ?>" >
                    <input type="hidden" id="clie_forn_id" name="clie_forn_id" value="<?php echo set_value('clie_forn_id'); ?>">
                  </div>
                </div>
                
                <div class="col-xs-3">
                  <div class="form-group">
                  <label>Período <i class="icon-info-sign tip-top" data-original-title="Lançamentos com vencimento no período."></i></label>
                  <select name="periodo" class="form-control">
                    <option value="dia">Dia</option>
                    <option value="semana" <?php if($periodo == 'semana'){ echo 'selected';} ?>>Semana</option>
                    <option value="mes" <?php if($periodo == 'mes'){ echo 'selected';} ?>>Mês</option>
                    <option value="ano" <?php if($periodo == 'ano'){ echo 'selected';} ?>>Ano</option>
                    <option value="todos" <?php if($periodo == 'todos'){ echo 'selected';} ?>>Todos</option>
                  </select>
                 </div>
                </div> 

                <div class="col-xs-3">
                  <div class="form-group">
                  <label>Situação <i class="icon-info-sign tip-top" data-original-title="Lançamentos com situação específica ou todos."></i></label>
                  <select name="situacao" class="form-control">
                    <option value="todos">Todos</option>
                    <option value="previsto" <?php if($situacao == 'previsto'){ echo 'selected';} ?>>À Vencer</option>
                    <option value="atrasado" <?php if($situacao == 'atrasado'){ echo 'selected';} ?>>Vencidos</option>
                    <option value="realizado" <?php if($situacao == 'realizado'){ echo 'selected';} ?>>Realizado</option>
                    <option value="pendente" <?php if($situacao == 'pendente'){ echo 'selected';} ?>>Pendencias</option>
                  </select>
                 </div>
                </div>


                <div class="col-xs-3">
                  <div class="form-group">
                  <label>Tipo <i class="icon-info-sign tip-top" data-original-title="Tipo receita ou despesa"></i></label>
                  <select name="tipo" class="form-control">

                  <option value="">Ambos</option>
                  <option value="receita" <?php if($tipo == 'receita'){ echo 'selected';} ?>>Receita</option>
                  <option value="despesa" <?php if($tipo == 'despesa'){ echo 'selected';} ?>>Despesa</option>                     
                  </select>
                 </div>
                </div>

                <div class="col-xs-3"><br>
                  
                    <button type="submit" class="btn btn-block btn-primary btn-flat">Filtrar</button>
                  
                </div>

              </div>
            </div>
        </form>
      </div>
<br>
<div class="box-body">
 <table  class="table table-bordered table-striped">
        <thead>
          <tr>
            <th class="columns01 col_default">#</th>
            <th class="columns02 col_default">Tipo</th>
            <th class="columns03 col_default">Cliente / Fornecedor</th>
            <th class="columns04 col_default">Vencimento</th>
            <th class="columns05 col_default">Status</th>
            <th class="columns05 col_default">Valor</th>
            <th>Ações</th>           
          </tr>          
          </tr>
          </thead>
          <tbody>
           <?php 

          $totalReceita = 0;
          $totalDespesa = 0;
          $saldo = 0;

           foreach ($dados as $f){ 
              $f->idFinanceiro; 
              $vencimento  = date(('d/m/Y'),strtotime($f->data_vencimento));
              if($f->financeiro_baixado == 0){$status = 'Pendente';}else{ $status = 'Pago';}; 
              if($f->financeiro_tipo == 'receita'){ $label = 'success'; $totalReceita += $f->financeiro_valor;} else{$label = 'danger'; $totalDespesa += $f->financeiro_valor;
            }
            ?>
              
              <tr>
                  <td class="text-left"><?php echo $f->idFinanceiro; ?></td>
                  <td><span class="label label-<?php echo $label  ?>"><?php echo ucfirst($f->financeiro_tipo) ?></span></td>
                  
                  <td class="text-left"><?php echo $f->financeiro_forn_clie.'<br>'. $f->financeiro_descricao; ?></td>

                  <td class="text-left"><?php echo $vencimento; ?></td>
                  <td class="text-left"><?php echo $status; ?></td> 
                  <td class="text-left"><?php echo $f->financeiro_valor; ?></td>      
                  <td>
              <!--       <?php if(verificarPermissao('vFinanceiro')){ ?>
                    <a href="#" onClick="(function(){window.open('<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/visualizar/<?php echo $f->idFinanceiro; ?>','MyWindow','toolbar=no,location=no,directories=no,status=no,menubar=yes,scrollbars=yes,resizable=yes,width=350,height=600');return false;})();return false;"data-toggle="tooltip" title="Visualizar"><i class="fa fa-search  text-success"></i> </a>
                    <?php } ?> -->
                    <?php if(verificarPermissao('eFinanceiro')){ 
                     echo '<a href="#modalEditar" style="margin-right: 1%" data-toggle="modal" role="button" idLancamento="'.$f->idFinanceiro.'" descricao="'.$f->financeiro_descricao.'" valor="'.$f->financeiro_valor.'" vencimento="'.date('d/m/Y',strtotime($f->data_vencimento)).'" pagamento="'.date('d/m/Y', strtotime($f->data_pagamento)).'" baixado="'.$f->financeiro_baixado.'" cliente="'.$f->financeiro_forn_clie.'" formaPgto="'.$f->financeiro_forma_pgto.'" tipo="'.$f->financeiro_tipo.'" autorizacao_nsu="'.$f->financeiro_autorizacao_NSU.'" bandeira_cart="'.$f->financeiro_bandeira_cart.'" venda="'.$f->vendas_id.'" class="editar" title="Editar"><i class="fa fa-edit"></i></a>'; 
                      } ?>
                    <?php if(verificarPermissao('vFinanceiro')){ echo '<a href="#modalExcluir" data-toggle="modal" role="button" idLancamento="'.$f->idFinanceiro.'" class="excluir" title="Excluir"><i class="fa fa-trash  text-danger"></i></a>';  } ?>             
                  </td>




              </tr>
              
          <?php }  ?>    
          <tfoot>
                  <tr>
                    <td colspan="5" style="text-align: right; color: green"> <strong>Total Receitas:</strong></td>
                    <td colspan="2" style="text-align: left; color: green"><strong>R$ <?php echo number_format($totalReceita,2,',','.') ?></strong></td>
                  </tr>
                  <tr>
                    <td colspan="5" style="text-align: right; color: red"> <strong>Total Despesas:</strong></td>
                    <td colspan="2" style="text-align: left; color: red"><strong>R$ <?php echo number_format($totalDespesa,2,',','.') ?></strong></td>
                  </tr>
                  <tr>
                    <td colspan="5" style="text-align: right"> <strong>Saldo:</strong></td>
                    <td colspan="2" style="text-align: left;"><strong>R$ <?php echo number_format($totalReceita - $totalDespesa,2,',','.') ?></strong></td>
                  </tr>
            </tfoot>

      </table>
        </div>

     <div class="box-footer text-center clearfix">
              <ul class="pagination pagination-sm no-margin pull-center">
                <?php
                if ($total_financeiro <> 0){

                        for($p=1, $i=0; $i < $total_financeiro; $p++, $i += $itens_per_page):
                            if($page == $p):
                                $tmp[] = "<li class='active'><a href='javascript:;'>".$p."</a></li>";
                            else:
                                $tmp[] = "<li><a href='".base_url()."financeiro/index/".$p."/".$situacao."/".$periodo."/".$cliente."/".$cliente_fornecedor."'>".$p."</a></li>";
                            endif;
                        endfor;

                        for($i = count($tmp) - 3; $i > 1; $i--):
                            if(abs($page - $i - 1) > 2):
                                unset($tmp[$i]);
                            endif;
                        endfor;

                        if(count($tmp) > 1):
                            if($page > 1):
                                echo "<li ><a href='".base_url()."financeiro/index/" .($page - 1)."/".$situacao."/".$periodo."/".$cliente."/".$cliente_fornecedor."'>«</a></li>";
                            endif;

                            $lastlink = 0;
                            foreach($tmp as $i => $link):
                                if($i > $lastlink + 1):
                                    echo "<li><a href='javascript:;'>...</a></li>";
                                endif;
                                echo $link;
                                $lastlink = $i;
                            endforeach;

                            if($page <= $lastlink):
                                echo "<li ><a href='".base_url()."financeiro/index/" .($page + 1)."/".$situacao."/".$periodo."/".$cliente."/".$cliente_fornecedor."'>«</a></li>";
                            endif;

                        endif;

                }else{

                  echo "Financeiro não foi encontrado!";
                }

                ?>

              </ul>
            </div>

            </div>
      </div>

    </section>

<!-- Modal nova receita -->
<!-- Modal -->
<div class="example-modal">
  <div class="modal" id="modalReceita">
    <div class="modal-dialog">     
        <!-- Modal content-->
        <div class="modal-content">
           <form id="formReceita" action="<?php echo base_url() ?>index.php/financeiro/adicionarReceita" method="post">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Adicionar Receita</h3>
              </div>
              <div class="modal-body">      

              <div class="col-xs-12 alert alert-info" style="margin-left: 0"> Obrigatório o preenchimento dos campos com asterisco.</div>

              <div class="col-xs-12"> 
                <label for="cliente">Cliente*</label>
<!--                 <input class="form-control" id="clie_receita" type="text" name="cliente" />
                <input id="clie_receita_id" class="span12" type="hidden" name="clie_receita_id" value=""  /> -->
                <input type="text" class="form-control" name="clie_receita" id="clie_receita" value="<?php echo set_value('cliente'); ?>" >
                <input type="hidden" id="clie_receita_id" name="clie_receita_id" value="<?php echo set_value('clie_receita_id'); ?>">
              </div>       

              <div class="col-xs-12">
                <label class="control-label" for="descricao">Descrição</label>
                <div class="form-group">
                  <input class="form-control" id="descricao" type="text" name="descricao"  />
                  <input id="urlAtual" type="hidden" name="urlAtual" value="<?php echo current_url() ?>"  />
                </div>
              </div>

              <div class="col-xs-12">
                <label class="control-label" for="">Categoria / Grupo:</label>
                  <select name="categoria" id="categoria" class="form-control"   >
                    <option value="0">Selecione</option>
                    <?php foreach ($categoriaFin as $c) { ?>
                        <option value="<?php echo $c->categoria_fina_id; ?>"><?php echo $c->categoria_fina_descricao; ?></option>
                    <?php } ?>
                  </select>
              </div>
      
              <div class="col-xs-12"><br>
                <span class="btn btn-primary col-xs-12" id="novaFormaPagamento" >Adicionar mais formas de pagamento</span>
                <br><br>
              </div>        



              <div class="col-xs-12" style="margin-left: 0" id="formasPagamentoNova"></div>
              <div class="col-xs-12" style="margin-left: 0" id="formasPagamento"> 
                <div class="col-xs-12" style="margin-left: 0"> 
                  <div class="col-xs-3" style="margin-left: 0"> 
                    <label for="valor">Valor*</label>
                    <input type="hidden"  name="tipo" value="receita" />            
                    <input type="hidden" id="qtdFormaPagamento" name="qtdFormaPagamento" value="receita" /> 
                    <input class="form-control money"  type="text" name="valor"  />                       
                  </div>

                  <div class="col-xs-4" >
                    <label for="vencimento">Data Vencimento*</label>
                    <input class="form-control datepicker"  type="text" name="vencimento"  />
                  </div>
                  <div class="col-xs-4">
                    <label for="formaPgto">Forma Pgto</label>
                    <select name="formaPgto"  class="form-control">
                      <option value="Dinheiro">Dinheiro</option>
                      <option value="Pagamento Instantâneo (PIX)">Pagamento Instantâneo (PIX)</option>
                      <option value="Cheque">Cheque</option>
                      <option value="Cartão de Crédito">Cartão de Crédito</option>
                      <option value="Cartão de Débito">Cartão de Débito</option>
                      <option value="Crédito Loja">Crédito Loja</option>
                      <option value="Vale Alimentação">Vale Alimentação</option>
                      <option value="Vale Refeição">Vale Refeição</option>
                      <option value="Vale Presente">Vale Presente</option>
                      <option value="Vale Combustível">Vale Combustível</option>
                      <option value="Duplicata Mercantil">Duplicata Mercantil</option>
                      <option value="Boleto Bancário">Boleto Bancário</option>
                      <option value="Transferência Bancária">Transferência Bancária</option>
                      <option value="Sem pagamento">Sem pagamento</option>
                      <option value="Outros">Outros</option> 
                    </select>
                  </div>
                </div>
              </div> 

              <div class="col-xs-12" style="margin-left: 0"> 
                <div class="col-xs-4" style="margin-left: 0">
                  <label for="recebido">Recebido?</label><br>
                  <input  id="recebido" type="checkbox" name="recebido" value="1" /> 
                </div>
                <div id="divRecebimento" class="span8" style=" display: none">
                  <div class="col-xs-6">
                    <label for="pagamento">Data Pagamento</label>
                    <input class="form-control datepicker" id="recebimento" type="text" name="recebimento" /> 
                  </div>
                </div> 
              </div>
            </div>

          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-success">Adicionar Receita</button>
          </div>
          </form>
          </div>
          </div>
      </div>
  </div>

  <!-- Modal nova despesa -->
    <!-- Modal -->
<div class="example-modal">
  <div class="modal" id="modalDespesa">
    <div class="modal-dialog">     
        
        <!-- Modal content-->
   <div class="modal-content">
    <form id="formDespesa" action="<?php echo base_url() ?>index.php/financeiro/adicionarDespesa" method="post">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Adicionar Despesa</h3>
      </div>
      <div class="modal-body">
      <div class="col-xs-12 alert alert-info" style="margin-left: 0"> Obrigatório o preenchimento dos campos com asterisco.</div>
        <div class="col-xs-12" style="margin-left: 0"> 
          <label for="fornecedor">Fornecedor / Empresa*</label>
          <input class="form-control" id="clie_forn_despesa" type="text" name="fornecedor"  />
          <input id="clie_forn_despesa_id" class="form-control" type="hidden" name="clie_forn_despesa_id" value=""  />
        </div> 

        <div class="col-xs-12" style="margin-left: 0"> 
          <label for="descricao">Descrição</label>
          <input class="form-control" id="descricao" type="text" name="descricao"  />
          <input id="urlAtual" type="hidden" name="urlAtual" value="<?php echo current_url() ?>"  />
        </div> 

        <div class="col-xs-12" style="margin-left: 0"> 
          <label for="">Categoria / Grupo:</label>
            <select name="categoria" id="categoria" class="form-control"  >
              <option value="0">Selecione</option>
              <?php foreach ($categoriaFin as $c) { ?>
                  <option value="<?php echo $c->categoria_fina_id; ?>"><?php echo $c->categoria_fina_descricao; ?></option>
              <?php } ?>
            </select>
        </div> 

        <div class="col-xs-12" style="margin-left: 0"><br>
          <span class="btn btn-primary form-control" id="novaFormaPagamentoDes" >Adicionar mais formas de pagamento</span>
        </div>        

      <br>
      <br>

      
      <div class="col-xs-12" style="margin-left: 0" id="formasPagamentoNovaDes"></div>

      <div class="col-xs-12" style="margin-left: 0" id="formasPagamentoDes"> 
        <div class="col-xs-12" style="margin-left: 0"> 
          <div class="col-xs-3" style="margin-left: 0"> 
            <label for="valor">Valor*</label>
            <input type="hidden"  name="tipo" value="despesa" />            
            <input type="hidden" id="qtdFormaPagamentoDes" name="qtdFormaPagamentoDes" value="despesa" /> 
            <input class="form-control money"  type="text" name="valor"  />                       
          </div>

          <div class="col-xs-4" >
            <label for="vencimento">Data Vencimento*</label>
            <input class="form-control datepicker"  type="text" name="vencimento"  />
          </div>
          <div class="col-xs-4">
            <label for="formaPgto">Forma Pgto</label>
            <select name="formaPgto"  class="form-control">
              <!-- REGRA DA SEFAZ -->
              <!--
              01 - Dinheiro
              02 - Cheque
              03 - Cartão de Crédito
              04 - Cartão de Débito
              05 - Crédito Loja
              10 - Vale Alimentação
              11 - Vale Refeição
              12 - Vale Presente
              13 - Vale Combustível
              14 - Duplicata Mercantil
              15 - Boleto Bancário
              90 - Sem pagamento
              99 - Outros
              -->

            <option value="Dinheiro">Dinheiro</option>
            <option value="Pagamento Instantâneo (PIX)">Pagamento Instantâneo (PIX)</option>
            <option value="Cheque">Cheque</option>
            <option value="Cartão de Crédito">Cartão de Crédito</option>
            <option value="Cartão de Débito">Cartão de Débito</option>
            <option value="Crédito Loja">Crédito Loja</option>
            <option value="Vale Alimentação">Vale Alimentação</option>
            <option value="Vale Refeição">Vale Refeição</option>
            <option value="Vale Presente">Vale Presente</option>
            <option value="Vale Combustível">Vale Combustível</option>
            <option value="Duplicata Mercantil">Duplicata Mercantil</option>
            <option value="Boleto Bancário">Boleto Bancário</option>
            <option value="Transferência Bancária">Transferência Bancária</option>
            <option value="Sem pagamento">Sem pagamento</option>
            <option value="Outros">Outros</option>  
            </select>
          </div>
        </div>
      </div>  

      <div class="col-xs-12" style="margin-left: 0"> 
        <div class="col-xs-4" style="margin-left: 0">
          <label for="pago">Foi Pago?</label><br>
          <input  id="pago" type="checkbox" name="pago" value="1" /> 
        </div>
        <div id="divPagamento" class="span8" style=" display: none">
          <div class="col-xs-6">
            <label for="pagamento">Data Pagamento</label>
            <input class="form-control datepicker" id="pagamento" type="text" name="pagamento" /> 
          </div>
        </div> 
      </div>

  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
    <button class="btn btn-danger">Adicionar Despesa</button>
  </div>
  </form>
    </div>
    </div>
</div>
</div>

<!-- Modal editar lançamento -->
    <!-- Modal -->
    <div class="modal fade" id="modalEditar" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
    <div class="modal-content">
        <form id="formEditar" action="<?php echo base_url() ?>index.php/financeiro/editar" method="post">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Editar Lançamento</h3>
  </div>
  <div class="modal-body">
      <div class="col-xs-12 alert alert-info" style="margin-left: 0"> Obrigatório o preenchimento dos campos com asterisco.</div>    
        <div class="col-xs-12" style="margin-left: 0"> 
          <label for="fornecedor">Fornecedor / Empresa*</label>
          <input class="form-control" id="fornecedorEditar" type="text" name="fornecedor"  />
        </div>  
      <div class="col-xs-12" style="margin-left: 0"> 
        <label for="descricao">Descrição</label>
        <input class="form-control" id="descricaoEditar" type="text" name="descricao"  />
        <input id="urlAtualEditar" type="hidden" name="urlAtual" value=""  />
      </div> 
    
        <div class="col-xs-4" style="margin-left: 0">  
          <label for="valor">Valor*</label>
          <input type="hidden"  name="tipo" value="despesa" />  
          <input type="hidden"  id="idEditar" name="id" value="" /> 
          <input type="hidden"  id="idVenda" name="idVenda" value="" /> 

          <input class="form-control money"  type="text" name="valor" id="valorEditar" />
        </div>
        <div class="col-xs-4" >
          <label for="vencimento">Data Vencimento*</label>
          <input class="form-control datepicker"  type="text" name="vencimento" id="vencimentoEditar" />
        </div>
        <div class="col-xs-4">
          <label for="vencimento">Tipo*</label>
          <select class="form-control" name="tipo" id="tipoEditar">
            <option value="receita">Receita</option>
            <option value="despesa">Despesa</option>
          </select>
        </div>      
        <div class="col-xs-2" style="margin-left: 0">
          <label for="pago">Foi Pago?</label><BR>
          <input  id="pagoEditar" type="checkbox" name="pago" value="1" /> 
        </div>
        <div id="divPagamentoEditar" class="span8" style=" display: none">
          <div class="col-xs-5">
            <label for="pagamento">Data Pagamento</label>
            <input class="form-control datepicker" id="pagamentoEditar" type="text" name="pagamento"  />  
          </div>
          <div class="col-xs-5">
            <label for="formaPgto">Forma Pgto</label>
            <select name="formaPgto" id="formaPgtoEditar"  class="form-control">
                <option value="Dinheiro">Dinheiro</option>
                <option value="Pagamento Instantâneo (PIX)">Pagamento Instantâneo (PIX)</option>
                <option value="Cheque">Cheque</option>
                <option value="Cartão de Crédito">Cartão de Crédito</option>
                <option value="Cartão de Débito">Cartão de Débito</option>
                <option value="Crédito Loja">Crédito Loja</option>
                <option value="Vale Alimentação">Vale Alimentação</option>
                <option value="Vale Refeição">Vale Refeição</option>
                <option value="Vale Presente">Vale Presente</option>
                <option value="Vale Combustível">Vale Combustível</option>
                <option value="Duplicata Mercantil">Duplicata Mercantil</option>
                <option value="Boleto Bancário">Boleto Bancário</option>
                <option value="Transferência Bancária">Transferência Bancária</option>
                <option value="Sem pagamento">Sem pagamento</option>
                <option value="Outros">Outros</option> 
            </select>
          </div>
        </div>

        <div class="control-group" id="divDadosCartao" style=" display: none">
                <!-- 
                01 - Visa / Visa Electron
                02 - Mastercard / Maestro
                03 - American Express
                04 - Sorocred
                05 - Diners Club
                06 - Elo
                07 - Hipercard
                08 - Aura
                09 - Cabal
                99 - Outros
                -->
            <div class="col-xs-12" style="margin-left: 0">              
                <div class="col-xs-6">
                    <label for="bandeiraCart">Bandeira Cartão</label>
                        <select class="form-control" name="bandeiraCart" id="bandeira_cart">                             
                            <option value="">Selecione Bandeira</option>                       
                            
                            <?php $cartao_id =  "<input id='bandeira_cart'/>"; ?>
                            <?php foreach ($cartao as $c) { ?>
                              <?php $selected = ''; if($c->categoria_cart_id == $cartao_id ) { $selected = 'selected';}?>
                              <option value="<?php echo $c->categoria_cart_id; ?>"<?php echo $selected ?>><?php echo $c->categoria_cart_descricao.' - '.$c->categoria_cart_nome_credenciadora; ?></option>
                            <?php } ?> 
                        </select>                      

                </div>
                 <div class="col-xs-6">
                    <label for="n_autorizacao_cartao">Nº Autorização(NSU)</label>
                    <input id="Id_cartao" class="form-control" type="text" name="autorizacao_nsu"   />                                         
                </div>                              
            </div>
        </div>


  </div><div class="modal-footer"></div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true" id="btnCancelarEditar">Cancelar</button>
    <button class="btn btn-primary">Salvar Alterações</button>
  </div>
  </form>  
    </div>  
    </div>
</div>



<!-- Modal Excluir lançamento-->
    <!-- Modal -->
    <div class="modal fade" id="modalExcluir" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Excluir Lançamento</h3>
        </div>
        <div class="modal-body">
          <h5 style="text-align: center">Deseja realmente excluir esse lançamento?</h5>
          <input name="id" id="idExcluir" type="hidden" value="" />
        </div>
        <div class="modal-footer">
          <button class="btn" data-dismiss="modal" aria-hidden="true" id="btnCancelExcluir">Cancelar</button>
          <button class="btn btn-danger" id="btnExcluir">Excluir Lançamento</button>
        </div>
      </div>
      
    </div>
  </div>


<script src="<?php echo base_url()?>assets/dist/js/jquery.validate.js"></script>
<!-- <script src="<?php echo base_url();?>assets/dist/js/jquery.maskMoney.min.js"></script> -->
<script type="text/javascript">

  jQuery(document).ready(function($) {


    // Busca Cliente/Fornecedor (Filtro)
    $("#clie_forn").autocomplete({
      source: "<?php echo base_url(); ?>index.php/financeiro/autoCompleteClienteFornecedor",
      minLength: 2,
      select: function( event, ui ) {
           $("#clie_forn_id").val(ui.item.id); 
      }
    });

    // Modal Receita
    $("#clie_receita").autocomplete({
      source: "<?php echo base_url(); ?>index.php/financeiro/autoCompleteCliente",
      minLength: 1,
      select: function( event, ui ) {

           $("#clie_receita_id").val(ui.item.id);            

      }
    }); 

    // Modal Despesa
    $("#clie_forn_despesa").autocomplete({
      source: "<?php echo base_url(); ?>index.php/financeiro/autoCompleteClienteFornecedor",
      minLength: 1,
      select: function( event, ui ) {

           $("#clie_forn_despesa_id").val(ui.item.id); 
      }
    }); 

    $('#pago').click(function(event) {
      var flag = $(this).is(':checked');
      if(flag == true){
        $('#divPagamento').show();
      }
      else{
        $('#divPagamento').hide();
      }
    });


    $('#recebido').click(function(event) {
      var flag = $(this).is(':checked');
      if(flag == true){
        $('#divRecebimento').show();
      }
      else{
        $('#divRecebimento').hide();
      }
    });


    $('#pagoEditar').click(function(event) {
      var flag = $(this).is(':checked');
      if(flag == true){
        $('#divPagamentoEditar').show();
      }
      else{
        $('#divPagamentoEditar').hide();
      }
    });

    $("#formaPgtoEditar").change(function(){ 

        
        var flag = null;
        var flag = document.getElementById('formaPgtoEditar').value;

        if((flag == 'Cartão de Crédito') || (flag == 'Cartão de Débito') ){ 
            $('#divDadosCartao').show(); 
        } else{        
          $('#divDadosCartao').hide();
        }

    });


    $("#formReceita").validate({
          rules:{
             descricao: {required:true},
             cliente: {required:true},
             valor: {required:true},
             vencimento: {required:true}      

          },

          messages:{
             descricao: {required: 'Campo Requerido.'},
             cliente: {required: 'Campo Requerido.'},
             valor: {required: 'Campo Requerido.'},
             vencimento: {required: 'Campo Requerido.'}
          }

    });







    $("#formDespesa").validate({

          rules:{

             descricao: {required:true},
             fornecedor: {required:true},
             valor: {required:true},
             vencimento: {required:true}      

          },

          messages:{

             descricao: {required: 'Campo Requerido.'},
             fornecedor: {required: 'Campo Requerido.'},
             valor: {required: 'Campo Requerido.'},
             vencimento: {required: 'Campo Requerido.'}

          }

        });

    



    $(document).on('click', '.excluir', function(event) {

      $("#idExcluir").val($(this).attr('idLancamento'));

    });


    $(document).on('click', '.editar', function(event) {
      $("#idEditar").val($(this).attr('idLancamento'));
      $("#idVenda").val($(this).attr('venda'));

      $("#descricaoEditar").val($(this).attr('descricao'));
      $("#fornecedorEditar").val($(this).attr('cliente'));
      $("#valorEditar").val($(this).attr('valor'));
      $("#vencimentoEditar").val($(this).attr('vencimento'));
      $("#pagamentoEditar").val($(this).attr('pagamento'));
      $("#formaPgtoEditar").val($(this).attr('formaPgto'));
      $("#tipoEditar").val($(this).attr('tipo'));

      $("#Id_cartao").val($(this).attr('autorizacao_nsu'));
      $("#bandeira_cart").val($(this).attr('bandeira_cart'));
      

      $("#urlAtualEditar").val($(location).attr('href'));
      var baixado = $(this).attr('baixado');
      if(baixado == 1){
        $("#pagoEditar").attr('checked', true);
        $("#divPagamentoEditar").show();
      }
      else{
        $("#pagoEditar").attr('checked', false); 
        $("#divPagamentoEditar").hide();
      }

      var cartao = $(this).attr('formaPgto');
      if((cartao == 'Cartão de Crédito') || (cartao == 'Cartão de Débito') ){ 
          $('#divDadosCartao').show(); 
      } else{        
        $('#divDadosCartao').hide();
      }

      // if(baixado == 1){
      //   $("#pagoEditar").attr('checked', true);
      //   $("#divPagamentoEditar").show();
      // }
      // else{
      //   $("#pagoEditar").attr('checked', false); 
      //   $("#divPagamentoEditar").hide();
      // }

    });



    $(document).on('click', '#btnExcluir', function(event) {

        var id = $("#idExcluir").val();

    

        $.ajax({

          type: "POST",

          url: "<?php echo base_url();?>index.php/financeiro/excluirLancamento",

          data: "id="+id,

          dataType: 'json',

          success: function(data)

          {

            if(data.result == true){

                $("#btnCancelExcluir").trigger('click');

                $("#divLancamentos").html('<div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div>');

                window.location.href="";

                

            }

            else{

                $("#btnCancelExcluir").trigger('click');

                alert('Ocorreu um erro ao tentar excluir produto.');

            }

          }

        });

        return false;

    });

 




  });


  $(document).ready(function(){
    $(".datepicker" ).datepicker({

    format: 'dd/mm/yyyy',                
    language: 'pt-BR'

    });

      var qtdFormaPagamento = 1;        

      $('#qtdFormaPagamento').val(qtdFormaPagamento);

      $('#novaFormaPagamento').on('click',function(event) {     
       
      qtdFormaPagamento++;

      $('#qtdFormaPagamento').val(qtdFormaPagamento);

      $('<div class="col-xs-12" style="margin-left: 0" id="formasPagamento">'
          +'<div class="col-xs-3" style="margin-left: 0">' 
            +'<label for="valor">Valor*</label>'
            +'<input class="form-control money"  type="text" name="valor'+qtdFormaPagamento+'" id="valorEditar'+qtdFormaPagamento+'" />'
          +'</div>'
          +'<div class="col-xs-4" >'
            +'<label for="vencimento">Data Vencimento*</label>'
            +'<input class="form-control datepicker" type="text" name="vencimento'+qtdFormaPagamento+'" id="vencimentoEditar'+qtdFormaPagamento+'" value="<?php echo date('d/m/Y');?>" />'
          +'</div>'
          +'<div class="col-xs-4">'
            +'<label for="formaPgto">Forma Pgto</label>'
              +'<select name="formaPgto'+qtdFormaPagamento+'" id="formaPgtoEditar'+qtdFormaPagamento+'"  class="form-control">'
                +'<option value="Dinheiro">Dinheiro</option>'
                +'<option value="Cheque">Cheque</option>'
                +'<option value="Cartão de Crédito">Cartão de Crédito</option>'
                +'<option value="Cartão de Débito">Cartão de Débito</option>'
                +'<option value="Crédito Loja">Crédito Loja</option>'
                +'<option value="Vale Alimentação">Vale Alimentação</option>'
                +'<option value="Vale Refeição">Vale Refeição</option>'
                +'<option value="Vale Presente">Vale Presente</option>'
                +'<option value="Vale Combustível">Vale Combustível</option>'
                +'<option value="Duplicata Mercantil">Duplicata Mercantil</option>'
                +'<option value="Boleto Bancário">Boleto Bancário</option>'
                +'<option value="Transferência Bancária">Transferência Bancária</option>'
                +'<option value="Sem pagamento">Sem pagamento</option>'
                +'<option value="Outros">Outros</option>'      
            +'</select>'
          +'</div>'        
        +'</div>'
        +'</div>').prependTo('#formasPagamentoNova');

      });

       
      // $(".money").maskMoney();

      /* DESPESA */
      var qtdFormaPagamentoDes = 1; 

      $('#qtdFormaPagamentoDes').val(qtdFormaPagamentoDes);

      $('#novaFormaPagamentoDes').on('click',function(event) {
      
       
      qtdFormaPagamentoDes++;

      $('#qtdFormaPagamentoDes').val(qtdFormaPagamentoDes);

      $('<div class="col-xs-12" style="margin-left: 0" id="formasPagamentoDes">'
          +'<div class="col-xs-3" style="margin-left: 0">' 
            +'<label for="valor">Valor*</label>'
            +'<input class="form-control money"  type="text" name="valor'+qtdFormaPagamentoDes+'" id="valorEditar'+qtdFormaPagamentoDes+'" />'
          +'</div>'
          +'<div class="col-xs-4" >'
            +'<label for="vencimento">Data Vencimento*</label>'
            +'<input class="form-control datepicker" type="text" name="vencimento'+qtdFormaPagamentoDes+'" id="vencimentoEditar'+qtdFormaPagamentoDes+'" value="<?php echo date('d/m/Y');?>" />'
          +'</div>'
          +'<div class="col-xs-4">'
            +'<label for="formaPgto">Forma Pgto</label>'
              +'<select name="formaPgto'+qtdFormaPagamentoDes+'" id="formaPgtoEditar'+qtdFormaPagamentoDes+'"  class="form-control">'
                +'<option value="Dinheiro">Dinheiro</option>'
                +'<option value="Cheque">Cheque</option>'
                +'<option value="Cartão de Crédito">Cartão de Crédito</option>'
                +'<option value="Cartão de Débito">Cartão de Débito</option>'
                +'<option value="Crédito Loja">Crédito Loja</option>'
                +'<option value="Vale Alimentação">Vale Alimentação</option>'
                +'<option value="Vale Refeição">Vale Refeição</option>'
                +'<option value="Vale Presente">Vale Presente</option>'
                +'<option value="Vale Combustível">Vale Combustível</option>'
                +'<option value="Duplicata Mercantil">Duplicata Mercantil</option>'
                +'<option value="Boleto Bancário">Boleto Bancário</option>'
                +'<option value="Sem pagamento">Sem pagamento</option>'
                +'<option value="Transferência Bancária">Transferência Bancária</option>'
                +'<option value="Outros">Outros</option>'       
            +'</select>'
          +'</div>'        
        +'</div>'
        +'</div>').prependTo('#formasPagamentoNovaDes');

      });


  });



</script>
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/jquery-ui-1.9.2.custom.css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/dist/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/3.1.0/jquery-migrate.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    $('.js-example-basic-multiple').select2();
});
</script>