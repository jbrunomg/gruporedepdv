<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo strtoupper(SUBDOMINIO) ?>  | Controler</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <!-- Google recaptcha -->
  <script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>

<?php if ($emitente[0]['emitente_botao_panico'] == '0'){ ?>
<body class="hold-transition" background="<?php echo base_url(); ?>assets/img/trafego_<?php echo GRUPOLOJA ?>.jpg"> 
<div class="login-box">
  <div class="login-logo">
    <!-- <a href="<?php echo base_url(); ?>assets/index2.html"><b>Admin</b>LTE</a> -->
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <img src="<?php echo base_url(); ?>assets/img/logo_atual.png" class="img-responsive" style="margin: 0 auto;">
    <p class="login-box-msg"></p>

    <?php if($this->session->flashdata('erro')){ ?>
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-ban"></i> <?php echo $this->session->flashdata('erro'); ?></h4>                
              </div>
    <?php }else{ ?>
          <p class="login-box-msg">Digite seus dados de acesso</p>
    <?php } ?>

    <form action="<?php echo base_url(); ?>sistema/processarlogin" method="post">
      <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="Usuário" id="usuario" name="usuario" required="" value="<?php echo set_value('usuario') ?>">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" required="" placeholder="Senha" name="senha" value="">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <!-- <label>
              <input type="checkbox"> Remember Me
            </label> -->
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" onclick="return valida()"  class="btn btn-primary btn-block btn-flat">Acessar</button>
        </div>
        <!-- /.col -->        
      </div>
    </form>

    <!-- <div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
        Facebook</a>
      <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
        Google+</a>
    </div> -->
    <!-- /.social-auth-links -->

    <a href="#">Esqueceu sua senha?</a><br>
    <!-- <a href="register.html" class="text-center">Register a new membership</a> -->

    <br>
    <!-- <div class="g-recaptcha" data-sitekey="6Lcn1fEpAAAAACJ3VrRgOoAT9pSP4T60u6SU5Twz"></div> -->
     <!-- GRUPOCELL/ GRUPOBYTE / GRUPOREDE -->

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
<?php } else { ?>

<body class="hold-transition" background="<?php echo base_url(); ?>assets/img/ERRO.jpg">

    <section class="content">

      <div class="error-page">
        <h2 class="headline text-red">500</h2>

        <div class="error-content">
          <h3><i class="fa fa-warning text-red"></i> Oops! Algo deu errado.</h3>

          <p>
            Vamos trabalhar para corrigir isso imediatamente.
            Enquanto isso, você pode <a href="#">retornar ao painel</a> ou tente usar o formulário de pesquisa.
          </p>

          <form class="search-form">
            <div class="input-group">
              <input type="text" name="search" class="form-control" placeholder="Search">

              <div class="input-group-btn">
                <button type="submit" name="submit" class="btn btn-danger btn-flat"><i class="fa fa-search"></i>
                </button>
              </div>
            </div>
            <!-- /.input-group -->
          </form>
        </div>
      </div>
      <!-- /.error-page -->
    </section>

<?php } ?>  


<!-- jQuery 3 -->
<script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url(); ?>assets/plugins/iCheck/icheck.min.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url(); ?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script>
  /*$(function () {
    $('#usuario').inputmask('999.999.999-99');

    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });*/


    function valida(){
      if (grecaptcha.getResponse() == "") {
        alert("Você precisa marcar a validação");
        return false;
      };
    }

</script>
</body>
</html>