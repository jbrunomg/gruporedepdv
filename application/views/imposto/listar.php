<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <div class="col-md-2">
            <?php if (verificarPermissao('aFiscal')) : ?>
              <a href="<?= base_url('imposto/adicionar'); ?>" type="button" class="btn btn-block btn-default btn-flat">Adicionar Impostos</a>
            <?php endif ?>
          </div>
        </div>
        <!-- <?= var_dump($impostos) ?> -->
        <div class="box-body">
          <div class="container-fluid">
            <table id="example" class="table" width="100%">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Descrição</th>
                  <th>Tipo de Tributação</th>
                  <th>Código CFOP</th>
                  <th>Situação tributária</th>
                  <th>Ações</th>
                </tr>
              </thead>
              <tbody>

                <?php foreach ($impostos as $imposto) : ?>
                  <tr>
                    <td> <?= $imposto->imposto_id; ?></td>
                    <td> <?= $imposto->imposto_descricao; ?></td>
                    <td> <?= $imposto->imposto_tipo_tributacao ?></td>
                    <td> <?= $imposto->imposto_icms_codigo_CFOP; ?></td>
                    <td> <?= $imposto->imposto_icms_situacao_tributaria ?></td>
                    <td>
                      <?php if (verificarPermissao('eFiscal')) : ?>
                        <a href="<?= base_url() . $this->uri->segment(1) ?>/editar/<?= $imposto->imposto_id ?>" data-toggle="tooltip" title="Editar">
                          <i class="fa fa-edit"></i>
                        </a>
                      <?php endif ?>

                      <?php if (verificarPermissao('dFiscal')) : ?>
                        <a style="cursor:pointer;" title="Deletar" data-toggle="modal" data-target="#excluir" onclick="excluir('<?= base_url() . $this->uri->segment(1) . "/excluir/" . $imposto->imposto_id; ?>');"><i class="fa fa-trash text-danger"></i>
                        </a>
                      <?php endif ?>
                    </td>
                  </tr>
                <?php endforeach ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>