<section class="content">
     <div class="row">
          <div class="col-md-12">
               <div class="box box-primary">
                    <form class="form-horizontal" action="<?= base_url('imposto/add') ?>" method="POST">
                         <div class="box-body">
                              <div class="form-group">
                                   <label for="imposto_descricao" class="col-sm-2 control-label">Descrição<span style="color: red;">*</span></label>
                                   <div class="col-sm-5">
                                        <input required type="text" class="form-control" name="imposto_descricao" id="imposto_descricao">
                                   </div>
                              </div>

                              <div class="form-group">
                                   <label for="imposto_tipo_tributacao" class="col-sm-2 control-label">Tipo de Tributação<span style="color: red;">*</span></label>
                                   <div class="col-sm-5">
                                        <select required class="form-control" name="imposto_tipo_tributacao" id="imposto_tipo_tributacao">
                                             <option value="Simples">Simples Nacional</option>
                                             <option value="Normal">Tributação Normal</option>
                                        </select>
                                   </div>
                              </div>

                              <div class="form-group">
                                   <label for="imposto_icms_codigo_CFOP" class="col-sm-2 control-label">Código CFOP<span style="color: red;">*</span></label>
                                   <div class="col-sm-5">
                                        <input required type="text" class="form-control" name="imposto_icms_codigo_CFOP" id="imposto_icms_codigo_CFOP">
                                   </div>
                              </div>

                              <div class="form-group">
                                   <label for="imposto_icms_situacao_tributaria" class="col-sm-2 control-label">Situação Tributaria<span style="color: red;">*</span></label>
                                   <div class="col-sm-5">
                                        <select required class="form-control" name="imposto_icms_situacao_tributaria" id="imposto_icms_situacao_tributaria">
                                             <option value="101">101 - Tributada com permissão de crédito</option>
                                             <option value="102" selected="selected">102 - Tributada sem permissão de crédito</option>
                                             <option value="103">103 - Isenção do ICMS para faixa de receita bruta</option>
                                             <option value="201">201 - Tributada com permissão de crédito e com cobrança do ICMS por Substituição Tributária</option>
                                             <option value="202">202 - Tributada sem permissão de crédito e com cobrança do ICMS por substituição tributária</option>
                                             <option value="203">203 - Isenção do ICMS para faixa de receita bruta e com cobrança do ICMS por substituição tributária</option>
                                             <option value="300">300 - Imune</option>
                                             <option value="400">400 - Não tributada</option>
                                             <option value="500">500 - ICMS cobrado anteriormente por substituição tributária (substituído) ou por antecipação</option>
                                             <option value="900">900 - Outros</option>
                                        </select>
                                   </div>
                              </div>

                              <hr>
                              <div>
                                   <h5>IPI</h5>
                              </div>

                              <div class="form-group">
                                   <label for="imposto_ipi_situacao_tributaria" class="col-sm-2 control-label">Situação Tributaria<span style="color: red;">*</span></label>
                                   <div class="col-sm-5">
                                        <select required class="form-control" name="imposto_ipi_situacao_tributaria" id="imposto_ipi_situacao_tributaria">
                                             <option value="00">00 - Entrada com recuperação de crédito</option>
                                             <option value="01">01 - Entrada tributada com alíquota zero</option>
                                             <option value="02">02 - Entrada isenta</option>
                                             <option value="03">03 - Entrada não-tributada</option>
                                             <option value="04">04 - Entrada imune</option>
                                             <option value="05">05 - Entrada com suspensão</option>
                                             <option value="49">49 - Outras entradas</option>
                                             <option value="50">50 - Saída tributada</option>
                                             <option value="51">51 - Saída tributada com alíquota zero</option>
                                             <option value="52">52 - Saída isenta</option>
                                             <option value="53">53 - Saída não-tributada</option>
                                             <option value="54">54 - Saída imune</option>
                                             <option value="55">55 - Saída com suspensão</option>
                                             <option value="99" selected="selected">99 - Outras saídas</option>
                                        </select>
                                   </div>
                              </div>

                              <div class="form-group">
                                   <label for="imposto_ipi_codigo_enquadramento" class="col-sm-2 control-label">Código de enquadramento<span style="color: red;">*</span></label>
                                   <div class="col-sm-5">
                                        <input required type="text" class="form-control" name="imposto_ipi_codigo_enquadramento" id="imposto_ipi_codigo_enquadramento">
                                   </div>
                              </div>

                              <div class="form-group">
                                   <label for="imposto_ipi_aliquota" class="col-sm-2 control-label">Alíquota<span style="color: red;">*</span></label>
                                   <div class="col-sm-5">
                                        <div class="input-group">
                                             <span class="input-group-addon" style="background-color: #ccc;">%</span>
                                             <input required type="text" class="form-control number" name="imposto_ipi_aliquota" id="imposto_ipi_aliquota">
                                        </div>
                                   </div>
                              </div>

                              <hr>
                              <div>
                                   <h5>PIS</h5>
                              </div>

                              <div class="form-group">
                                   <label for="imposto_pis_situacao_tributaria" class="col-sm-2 control-label">Situação Tributaria<span style="color: red;">*</span></label>
                                   <div class="col-sm-5">
                                        <select required class="form-control" name="imposto_pis_situacao_tributaria" id="imposto_pis_situacao_tributaria">
                                             <option value="01">01 - Operação Tributável - Base de Cálculo = Valor da Operação Alíquota Normal (Cumulativo/Não cumulativo)</option>
                                             <option value="02">02 - Operação Tributável - Base de Cálculo = Valor da Operação (Alíquota diferenciada)</option>
                                             <option value="03">03 - Operação Tributável - Base de Cálculo = Quantidade Vendida X Alíquota por Unidade de Produto</option>
                                             <option value="04">04 - Operação Tributável - Tributação Monofásica - (Alíquota Zero)</option>
                                             <option value="06">06 - Operação Tributável - Alíquota Zero</option>
                                             <option value="07">07 - Operação Isenta de Contribuição</option>
                                             <option value="08">08 - Operação sem Incidência da Contribuição</option>
                                             <option value="09">09 - Operação com Suspensão da Contribuição</option>
                                             <option value="49">49 - Outras Operações de Saída</option>
                                             <option value="50">50 - Operação com Direito a Crédito - Vinculada Exclusivamente a Receita Tributada no Mercado Interno</option>
                                             <option value="51">51 - Operação com Direito a Crédito - Vinculada Exclusivamente a Receita Não Tributada no Mercado Interno</option>
                                             <option value="52">52 - Operação com Direito a Crédito - Vinculada Exclusivamente a Receita de Exportação</option>
                                             <option value="53">53 - Operação com Direito a Crédito - Vinculado a Receitas Tributadas e Não-Tributadas no Mercado Interno</option>
                                             <option value="54">54 - Operação com Direito a Crédito - Vinculada a Receitas Tributadas no Mercado Interno e de Exportação</option>
                                             <option value="55">55 - Operação com Direito a Crédito - Vinculada a Receitas Não-Tributadas no Mercado Interno e de Exportação</option>
                                             <option value="56">56 - Operação com Direito a Crédito - Vinculado a Receitas Tributadas e Não-Tributadas no Mercado Interno, e de Exportação</option>
                                             <option value="60">60 - Crédito Presumido - Operação de Aquisição Vinculada Exclusivamente a Receita Tributada no Mercado Interno</option>
                                             <option value="61">61 - Crédito Presumido - Operação de Aquisição Vinculada Exclusivamente a Receita Não-Tributada no Mercado Interno</option>
                                             <option value="62">62 - Crédito Presumido - Operação de Aquisição Vinculada Exclusivamente a Receita de Exportação</option>
                                             <option value="63">63 - Crédito Presumido - Operação de Aquisição Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno</option>
                                             <option value="64">64 - Crédito Presumido - Operação de Aquisição Vinculada a Receitas Tributadas no Mercado Interno e de Exportação</option>
                                             <option value="65">65 - Crédito Presumido - Operação de Aquisição Vinculada a Receitas Não-Tributadas no Mercado Interno e de Exportação</option>
                                             <option value="66">66 - Crédito Presumido - Operação de Aquisição Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno, e de Exportação</option>
                                             <option value="67">67 - Crédito Presumido - Outras Operações</option>
                                             <option value="70">70 - Operação de Aquisição sem Direito a Crédito</option>
                                             <option value="71">71 - Operação de Aquisição com Isenção</option>
                                             <option value="72">72 - Operação de Aquisição com Suspensão</option>
                                             <option value="73">73 - Operação de Aquisição a Alíquota Zero</option>
                                             <option value="74">74 - Operação de Aquisição sem Incidência da Contribuição</option>
                                             <option value="75">75 - Operação de Aquisição por Substituição Tributária</option>
                                             <option value="98">98 - Outras Operações de Entrada</option>
                                             <option value="99" selected="selected">99 - Outras Operações</option>
                                        </select>
                                   </div>
                              </div>

                              <div class="form-group">
                                   <label for="imposto_pis_aliquota" class="col-sm-2 control-label">Alíquota<span style="color: red;">*</span></label>
                                   <div class="col-sm-5">
                                        <div class="input-group">
                                             <span class="input-group-addon" style="background-color: #ccc;">%</span>
                                             <input required type="text" class="form-control number" name="imposto_pis_aliquota" id="imposto_pis_aliquota">
                                        </div>
                                   </div>
                              </div>

                              <hr>
                              <div>
                                   <h5>CONFIS</h5>
                              </div>

                              <div class="form-group">
                                   <label for="imposto_cofins_situacao_tributaria" class="col-sm-2 control-label">Situação Tributaria<span style="color: red;">*</span></label>
                                   <div class="col-sm-5">
                                        <select required class="form-control" name="imposto_cofins_situacao_tributaria" id="imposto_cofins_situacao_tributaria">
                                             <option value="01">01 - Operação Tributável - Base de Cálculo = Valor da Operação Alíquota Normal (Cumulativo/Não cumulativo)</option>
                                             <option value="02">02 - Operação Tributável - Base de Cálculo = Valor da Operação (Alíquota diferenciada)</option>
                                             <option value="03">03 - Operação Tributável - Base de Cálculo = Quantidade Vendida X Alíquota por Unidade de Produto</option>
                                             <option value="04">04 - Operação Tributável - Tributação Monofásica - (Alíquota Zero)</option>
                                             <option value="06">06 - Operação Tributável - Alíquota Zero</option>
                                             <option value="07">07 - Operação Isenta de Contribuição</option>
                                             <option value="08">08 - Operação sem Incidência da Contribuição</option>
                                             <option value="09">09 - Operação com Suspensão da Contribuição</option>
                                             <option value="49">49 - Outras Operações de Saída</option>
                                             <option value="50">50 - Operação com Direito a Crédito - Vinculada Exclusivamente a Receita Tributada no Mercado Interno</option>
                                             <option value="51">51 - Operação com Direito a Crédito - Vinculada Exclusivamente a Receita Não Tributada no Mercado Interno</option>
                                             <option value="52">52 - Operação com Direito a Crédito - Vinculada Exclusivamente a Receita de Exportação</option>
                                             <option value="53">53 - Operação com Direito a Crédito - Vinculado a Receitas Tributadas e Não-Tributadas no Mercado Interno</option>
                                             <option value="54">54 - Operação com Direito a Crédito - Vinculada a Receitas Tributadas no Mercado Interno e de Exportação</option>
                                             <option value="55">55 - Operação com Direito a Crédito - Vinculada a Receitas Não-Tributadas no Mercado Interno e de Exportação</option>
                                             <option value="56">56 - Operação com Direito a Crédito - Vinculado a Receitas Tributadas e Não-Tributadas no Mercado Interno, e de Exportação</option>
                                             <option value="60">60 - Crédito Presumido - Operação de Aquisição Vinculada Exclusivamente a Receita Tributada no Mercado Interno</option>
                                             <option value="61">61 - Crédito Presumido - Operação de Aquisição Vinculada Exclusivamente a Receita Não-Tributada no Mercado Interno</option>
                                             <option value="62">62 - Crédito Presumido - Operação de Aquisição Vinculada Exclusivamente a Receita de Exportação</option>
                                             <option value="63">63 - Crédito Presumido - Operação de Aquisição Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno</option>
                                             <option value="64">64 - Crédito Presumido - Operação de Aquisição Vinculada a Receitas Tributadas no Mercado Interno e de Exportação</option>
                                             <option value="65">65 - Crédito Presumido - Operação de Aquisição Vinculada a Receitas Não-Tributadas no Mercado Interno e de Exportação</option>
                                             <option value="66">66 - Crédito Presumido - Operação de Aquisição Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno, e de Exportação</option>
                                             <option value="67">67 - Crédito Presumido - Outras Operações</option>
                                             <option value="70">70 - Operação de Aquisição sem Direito a Crédito</option>
                                             <option value="71">71 - Operação de Aquisição com Isenção</option>
                                             <option value="72">72 - Operação de Aquisição com Suspensão</option>
                                             <option value="73">73 - Operação de Aquisição a Alíquota Zero</option>
                                             <option value="74">74 - Operação de Aquisição sem Incidência da Contribuição</option>
                                             <option value="75">75 - Operação de Aquisição por Substituição Tributária</option>
                                             <option value="98">98 - Outras Operações de Entrada</option>
                                             <option value="99" selected="selected">99 - Outras Operações</option>
                                        </select>
                                   </div>
                              </div>

                              <div class="form-group">
                                   <label for="imposto_cofins_aliquota" class="col-sm-2 control-label">Alíquota<span style="color: red;">*</span></label>
                                   <div class="col-sm-5">
                                        <div class="input-group">
                                             <span class="input-group-addon" style="background-color: #ccc;">%</span>
                                             <input required type="text" class="form-control number" name="imposto_cofins_aliquota" id="imposto_cofins_aliquota">
                                        </div>
                                   </div>
                              </div>

                              <hr>
                              <div>
                                   <h5>Estimativa de tributos</h5>
                              </div>

                              <div class="form-group">
                                   <label for="imposto_tributos_federais" class="col-sm-2 control-label">Tributos Federais<span style="color: red;">*</span></label>
                                   <div class="col-sm-5">
                                        <div class="input-group">
                                             <span class="input-group-addon" style="background-color: #ccc;">%</span>
                                             <input required type="text" class="form-control number" name="imposto_tributos_federais" id="imposto_tributos_federais">
                                        </div>
                                   </div>
                              </div>

                              <div class="form-group">
                                   <label for="imposto_tributos_estaduais" class="col-sm-2 control-label">Tributos Estaduais<span style="color: red;">*</span></label>
                                   <div class="col-sm-5">
                                        <div class="input-group">
                                             <span class="input-group-addon" style="background-color: #ccc;">%</span>
                                             <input required type="text" class="form-control number" name="imposto_tributos_estaduais" id="imposto_tributos_estaduais">
                                        </div>
                                   </div>
                              </div>
                         </div>

                         <div class="box-footer">
                              <a href="<?= base_url('imposto') ?>" class="btn btn-default">Voltar</a>
                              <button type="submit" class="btn btn-primary pull-right">Adicionar</button>
                         </div>
                    </form>
               </div>
          </div>
     </div>
</section>

<script>
     $(document).ready(function() {
          $('.number').on('input', function() {
               $(this).val($(this).val().replace(/[^\d.,]/g, ''));
          });
     });
</script>