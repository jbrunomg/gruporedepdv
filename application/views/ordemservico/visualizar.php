<section class="content">
  <div class="row">
    <div class="col-md-12">
      <!-- Horizontal Form -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="<?php echo current_url(); ?>" method="post">
          <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $dados[0]->os_id; ?>"
            placeholder="id">
          <div class="box-body">


            <div class="col-md-12">
              <!-- Custom Tabs -->
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab">Dados gerais</a></li>
                  <li><a href="#tab_2" data-toggle="tab">Histórico</a></li>
                  <li><a href="#tab_3" data-toggle="tab">Laudo Téc</a></li>
                  <li><a href="#tab_4" data-toggle="tab">Serviços/Mão de Obra</a></li>
                  <li><a href="#tab_5" data-toggle="tab">Produtos/Peças</a></li>

                  <li class="pull-right dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                      <i class="fa  fa-align-justify"></i> <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                      <li role="presentation"><a href="#tab_6" data-toggle="tab" role="menuitem"
                          tabindex="-1">Pagamento</a></li>
                    </ul>
                  </li>
                  <!-- <li class="pull-right dropdown"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li> -->
                </ul>
                <style>
                    .assinatura_pad {
                        border: 1px solid #ddd;
                        box-sizing: border-box;
                        width: 100%;
                        max-height: 100px;
                        max-width: 500px;
                        aspect-ratio: 5 / 1;
                      }
                      .title {
                        padding: 0;
                        font-size: 21px;
                        line-height: inherit;
                        color: #333;
                      }
                </style>
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">

                    <div class="box-body no-padding">
                      <div class="tab-pane active" id="dados-gerais">
                        <div class="col-sm-12 col-lg-12 col-md-12">
                          <div class="table-responsive">
                            <table cellpadding="0" cellspacing="0" class="table table-bordered">
                              <tbody>
                                <tr>
                                  <th>Nº da OS</th>
                                  <td>
                                    <?php echo $dados[0]->os_id; ?>
                                  </td>
                                </tr>
                                <tr>
                                  <th>Cliente</th>
                                  <td><a
                                      href="<?php echo base_url(); ?>clientes/visualizar/<?php echo $dados[0]->clientes_id; ?>">
                                      <?php echo $dados[0]->cliente_nome; ?>
                                    </a></td>
                                </tr>
                                <tr>
                                  <th>Entrada</th>
                                  <td>
                                    <?php echo date(('d/m/Y H:i:s'), strtotime($dados[0]->os_dataInicial . $dados[0]->os_horaInicial)); ?>
                                  </td>
                                </tr>
                                <tr>
                                  <th>Saída</th>
                                  <td>
                                    <?php echo date(('d/m/Y H:i:s'), strtotime($dados[0]->os_dataFinal . $dados[0]->os_horaFinal)); ?>
                                  </td>
                                </tr>
                                <tr>
                                  <th>Vendedor</th>
                                  <td><a
                                      href="<?php echo base_url(); ?>usuarios/visualizar/<?php echo $dados[0]->usuarios_id; ?>">
                                      <?php echo strtoupper($dados[0]->usuario_nome); ?>
                                    </a></td>
                                </tr>
                                <tr>
                                  <th>Técnico</th>
                                  <td><a
                                      href="<?php echo base_url(); ?>usuarios/visualizar/<?php echo $dados[0]->tec_id; ?>">
                                      <?php echo strtoupper($dados[0]->tec_nome); ?>
                                    </a></td>
                                </tr>
                                <tr>
                                  <th>Previsão de entrega</th>
                                  <td></td>
                                </tr>
                                <tr>
                                  <th>Observações</th>
                                  <td></td>
                                </tr>
                                <tr>
                                  <th>Observações interna</th>
                                  <td></td>
                                </tr>
                                <tr>
                                  <th>Canal de venda</th>
                                  <td>Presencial</td>
                                </tr>
                                <tr>
                                  <th>Cadastrado por</th>
                                  <td>
                                    <?php echo strtoupper($dados[0]->usuario_nome); ?>
                                  </td>
                                </tr>
                                <tr>
                                  <th>Cadastrado em</th>
                                  <td>
                                    <?php echo date(('d/m/Y'), strtotime($dados[0]->os_data_cadastro)); ?>
                                  </td>
                                </tr>
                                <tr>
                                  <th>Modificado em</th>
                                  <td>
                                    <?php echo date(('d/m/Y H:i:s'), strtotime($dados[0]->os_data_atualizacao)); ?>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                          <div class="row">
                            <div class="col-12">
                              <h2 class="text-center title">ASSINATURA DIGITAL</h4>
                            </div>
                          </div>
                          <div class="row justify-content-center">
                            <div class="col-12 text-center" >
                                <?php if(!empty($dados[0]->os_assinatura)) {?>
                                  <img class="assinatura_pad" src="<?= isset($dados[0]->os_assinatura) ? $dados[0]->os_assinatura : '' ;?>" alt="Assinatura Digital">
                                <?php } else{?>
                                  <div class="assinatura_pad" style="margin: 0 auto;display: flex;justify-content: center;align-items: center" >Sem Assinatura Digital</div>
                                <?php } ?>
                              </div>
                          </div>
                      </div>
                    </div>


                  </div>

                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_2">

                    <div class="box-body no-padding">
                      <div class="tab-pane" id="historico">
                        <div class="col-sm-12 col-lg-12 col-md-12">
                          <table cellpadding="0" cellspacing="0" class="table table-bordered">
                            <tbody>
                              <tr>
                                <th width="15%">Data</th>
                                <th width="50%">Observação</th>
                                <th width="15%" class="actions">Situação</th>
                                <th width="20%">Funcionário</th>
                              </tr>
                              <?php foreach ($hist_status as $dado) { ?>
                                <tr>
                                  <td>
                                    <?php echo date('d/m/Y h:i:s', strtotime($dado->situacao_data_atualizacao)) ?>
                                  </td>
                                  <td>
                                    <?php echo $dado->situacao_obs; ?>
                                  </td>
                                  <td>
                                    <?php echo $dado->situacao_status; ?>
                                  </td>
                                  <!--          <td class="actions">
                                      <span class="label" style="background-color:#F4CCCC">2- EM ANÁLISE TÉCNICA</span>
                                  </td> -->
                                  <td>
                                    <?php echo $dado->usuario_nome; ?>
                                  </td>
                                </tr>
                              <?php } ?>

                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>

                  </div>

                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_3">

                    <div class="box-body no-padding">
                      <div class="tab-pane" id="equipamentos">
                        <div class="col-sm-12 col-lg-12 col-md-12">
                          <div class="table-responsive">
                            <table cellpadding="0" cellspacing="0" class="table table-bordered">
                              <tbody>
                                <tr>
                                  <th>Equipamento</th>
                                  <td>
                                    <?php echo $dados[0]->os_equipamento; ?>
                                  </td>
                                </tr>
                                <tr>
                                  <th>Marca</th>
                                  <td>
                                    <?php echo $dados[0]->os_marca; ?>
                                  </td>
                                </tr>
                                <tr>
                                  <th>Modelo</th>
                                  <td>
                                    <?php echo $dados[0]->os_modelo; ?>
                                  </td>
                                </tr>
                                <tr>
                                  <th>Série</th>
                                  <td>
                                    <?php echo $dados[0]->os_serie; ?>
                                  </td>
                                </tr>
                                <tr>
                                  <td colspan="2">
                                    <div><b>Condições</b></div>
                                    <div>
                                      <?php echo $dados[0]->os_condicoes; ?>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td colspan="2">
                                    <div><b>Defeitos</b></div>
                                    <div>
                                      <?php echo $dados[0]->os_defeito; ?>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td colspan="2">
                                    <div><b>Acessórios</b></div>
                                    <div>
                                      <?php echo $dados[0]->os_acessorio; ?>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td colspan="2">
                                    <div><b>Solução</b></div>
                                    <div>
                                      <?php echo $dados[0]->os_solucao; ?>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td colspan="2">
                                    <div><b>Laudo técnico</b></div>
                                    <div>
                                      <?php echo $dados[0]->os_laudotecnico; ?>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td colspan="2">
                                    <div><b>Termos de garantia</b></div>
                                    <div>
                                      <?php echo $dados[0]->os_termogarantia; ?>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_4">

                    <div class="box-body no-padding">
                      <?php $totalGeralS = 0;
                      if ($servicos != null) { ?>
                        <table class="table table-condensed">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th>Serviço</th>
                              <th>Quantidade</th>
                              <th>SubTotal</th>
                              <th style="width: 40px">Ações</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php
                            $total = 0;
                            $qtd = 0;
                            $seqServico = 1;
                            // $totalGeralS = 0;
                            foreach ($servicos as $p) {
                              $qtd = $qtd + $p->servico_os_quantidade;
                              echo '<tr>';
                              echo '<td>' . $seqServico++ . '</td>';
                              echo '<td>' . $p->servico_nome . '</td>';
                              echo '<td>' . $p->servico_os_quantidade . '</td>';
                              echo '<td>' . number_format($p->servico_os_subTotal, 2, ".", ""), '</td>';
                              $totalGeralS += $p->servico_os_subTotal;

                              echo '</tr>';
                            } ?>


                          </tbody>
                          <tfoot>
                            <tr>
                              <th><span id="itensServico">ITENS:
                                  <?php echo ' ' . count($servicos); ?>
                                </span></th>
                              <th></th>
                              <th><span id="volumeServico">VOLUME:
                                  <?php echo ' ' . $qtd; ?>
                                </span></th>
                              <th colspan="2"><span id="totalGeralServico">TOTAL GERAL:
                                  <?php echo ' ' . number_format($totalGeralS, 2, ".", ""); ?><span id="volume"></th>
                            </tr>
                          </tfoot>
                        </table>
                        <input type="hidden" id='seqServico' value="<?php echo $seqServico ?>">
                        <input type="hidden" id='volServico' value="<?php echo $qtd ?>">
                        <input type="hidden" name="totalServico" id="totalServico"
                          value="<?php echo number_format($totalGeralS, 2, ".", ""); ?>">

                      <?php } else { ?>
                        <div class="tab-pane" id="servicos">
                          <div class="col-sm-12 col-lg-12 col-md-12">
                            <div class="table-responsive">
                              <div class="text-center">Nenhum serviço foi cadastrado</div>
                            </div>
                          </div>
                        </div>
                      <?php } ?>

                    </div>

                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_5">

                    <div class="box-body no-padding">
                      <?php $totalGeralP = 0;
                      if ($produtos != null) { ?>
                        <table class="table table-condensed">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th>Produto</th>
                              <th>Quantidade</th>
                              <th>Venda</th>
                              <th>SubTotal</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php
                            $total = 0;
                            $qtd = 0;
                            $seq = 1;
                            // $totalGeralP = 0;
                            foreach ($produtos as $p) {
                              $qtd = $qtd + $p->produtos_os_quantidade;
                              echo '<tr>';
                              echo '<td>' . $seq++ . '</td>';
                              echo '<td>' . $p->produto_descricao . '</td>';
                              echo '<td>' . $p->produtos_os_quantidade . '</td>';
                              echo '<td>' . $p->produto_preco_venda . '</td>';
                              echo '<td>' . number_format($p->produtos_os_subTotal, 2, ".", ""), '</td>';
                              $totalGeralP += $p->produtos_os_subTotal;
                              echo '</tr>';
                            } ?>

                          </tbody>
                          <tfoot>
                            <tr>
                              <th><span id="itens">ITENS:
                                  <?php echo ' ' . count($produtos); ?>
                                </span></th>
                              <th></th>
                              <th><span id="volume">VOLUME:
                                  <?php echo ' ' . $qtd; ?>
                                </span></th>
                              <th></th>
                              <th colspan="2"><span id="totalGeral">TOTAL GERAL:
                                  <?php echo ' ' . number_format($totalGeralP, 2, ".", ""); ?><span id="volume"></th>
                            </tr>
                          </tfoot>
                        </table>
                        <input type="hidden" id='seq' value="<?php echo $seq ?>">
                        <input type="hidden" id='vol' value="<?php echo $qtd ?>">
                        <input type="hidden" name="total" id="total"
                          value="<?php echo number_format($totalGeralP, 2, ".", ""); ?>">

                      <?php } else { ?>

                        <div class="tab-pane" id="produtos">
                          <div class="col-sm-12 col-lg-12 col-md-12">
                            <div class="table-responsive">
                              <div class="text-center">Nenhum produto foi cadastrado</div>
                            </div>
                          </div>
                        </div>
                      <?php } ?>


                    </div>

                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_6">

                    <div class="box-body no-padding">
                      <div class="tab-pane" id="faturamento">
                        <div class="col-sm-12 col-lg-12 col-md-12">
                          <div class="table-responsive">
                            <h3 class="text-primary">Total da OS</h3>
                            <table cellpadding="0" cellspacing="0" class="table table-bordered">
                              <tbody>
                                <tr>
                                  <th>Mão de obra</th>
                                  <th>Peças</th>
                                  <th>Frete</th>
                                  <th>Outros</th>
                                  <th>Desconto</th>
                                  <th>Total</th>
                                </tr>
                                <tr>
                                  <td>
                                    <?php echo number_format($totalGeralS, 2, ".", ""); ?>
                                  </td>
                                  <td>
                                    <?php echo number_format($totalGeralP, 2, ".", ""); ?>
                                  </td>
                                  <td>0,00 </td>
                                  <td>0,00 </td>
                                  <td>0,00 </td>
                                  <td>
                                    <?php echo number_format($dados[0]->os_valorTotal, 2, ',', '.'); ?>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div>
              <!-- nav-tabs-custom -->
            </div>
            <!-- /.col -->




          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>" class="btn btn-default">Voltar</a>

          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
  </div>
</section>