<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo strtoupper(SUBDOMINIO) ?>  | Controler</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/verificar-conexao.css">
  
  <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>


<body class="hold-transition skin-green  sidebar-mini">
<div >






<section class="content">

 
  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form  action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/editarSituacao" method="post">

         <?php //  var_dump($dados);die(); ?> 
        <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $dados[0]->situacao_os_id; ?>">
        <input type="hidden" class="form-control" name="usuario_id" id="usuario_id" value="<?php echo $this->session->userdata('usuario_id'); ?>">

        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">

              <div class="form-group col-md-4">
                
                <label>Situação*</label>
                <select class="form-control" name="situacao_status" id="situacao_status">
                  <option value="1- AGUARDANDO ORÇAMENTO" <?php if($dados[0]->situacao_status == '1- AGUARDANDO ORÇAMENTO'){ echo "SELECTED";}?> >1- AGUARDANDO ORÇAMENTO</option>
                  <option value="2- EM ANÁLISE TÉCNICA" <?php if($dados[0]->situacao_status == '2- EM ANÁLISE TÉCNICA'){ echo "SELECTED";}?> >2- EM ANÁLISE TÉCNICA</option>
                  <option value="3- AGUARDANDO APROVAÇÃO DO CLIENTE" <?php if($dados[0]->situacao_status == '3- AGUARDANDO APROVAÇÃO DO CLIENTE'){ echo "SELECTED";}?> >3- AGUARDANDO APROVAÇÃO DO CLIENTE</option>
                  <option value="4- ORÇAMENTO APROVADO" <?php if($dados[0]->situacao_status == '4- ORÇAMENTO APROVADO'){ echo "SELECTED";}?> >4- ORÇAMENTO APROVADO</option>
                  <option value="5- ORÇAMENTO NÃO APROVADO" <?php if($dados[0]->situacao_status == '5- ORÇAMENTO NÃO APROVADO'){ echo "SELECTED";}?> >5- ORÇAMENTO NÃO APROVADO</option>
                  <option value="6- EXECUTANDO" <?php if($dados[0]->situacao_status == '6- EXECUTANDO'){ echo "SELECTED";}?> >6- EXECUTANDO</option>

                  <option value="7- CONCLUÍDA COM ÊXITO (LIBERADO PARA FATURAMENTO)" <?php if($dados[0]->situacao_status == '7- CONCLUÍDA COM ÊXITO (LIBERADO PARA FATURAMENTO)'){ echo "SELECTED";}?> >7- CONCLUÍDA COM ÊXITO (LIBERADO PARA FATURAMENTO) </option>

                  <option value="8- CONCLUÍDA COM ÊXITO(SEM FATURAMENTO)" <?php if($dados[0]->situacao_status == '8- CONCLUÍDA COM ÊXITO(SEM FATURAMENTO)'){ echo "SELECTED";}?> >8- CONCLUÍDA COM ÊXITO(SEM FATURAMENTO)</option>
                  <option value="9- CONCLUÍDA SEM ÊXITO" <?php if($dados[0]->situacao_status == '9- CONCLUÍDA SEM ÊXITO'){ echo "SELECTED";}?> >9- CONCLUÍDA SEM ÊXITO</option>
                  <option value="10- O.S RETIRADA (COM FATURAMENTO)" <?php if($dados[0]->situacao_status == '10- O.S RETIRADA (COM FATURAMENTO)'){ echo "SELECTED";}?> >10- O.S RETIRADA (COM FATURAMENTO)</option>
                  <option value="10.1 FATURADO E NÃO RETIRADO" <?php if($dados[0]->situacao_status == '10.1 FATURADO E NÃO RETIRADO'){ echo "SELECTED";}?>>10.1 FATURADO E NÃO RETIRADO</option>

                  <option value="11- O.S RETIRADA (SEM FATURAMENTO)" <?php if($dados[0]->situacao_status == '11- O.S RETIRADA (SEM FATURAMENTO)'){ echo "SELECTED";}?> >11- O.S RETIRADA (SEM FATURAMENTO)</option>
                  <option value="12- VERIFICAR GARANTIA" <?php if($dados[0]->situacao_status == '12- VERIFICAR GARANTIA'){ echo "SELECTED";}?> >12- VERIFICAR GARANTIA</option>
                  <option value="13- GARANTIA VERIFICADA OK (EXECUTANDO)" <?php if($dados[0]->situacao_status == '13- GARANTIA VERIFICADA OK (EXECUTANDO)'){ echo "SELECTED";}?> >13- GARANTIA VERIFICADA OK (EXECUTANDO)</option>
                  
                  <option value="14- GARANTIA NÅO APROVADA" <?php if($dados[0]->situacao_status == '14- GARANTIA NÅO APROVADA'){ echo "SELECTED";}?> >14- GARANTIA NÅO APROVADA</option>

                  <option value="15- AGUARDANDO PECA" <?php if($dados[0]->situacao_status == '15- AGUARDANDO PECA'){ echo "SELECTED";}?> >15- AGUARDANDO PEÇA</option>
                   
                  <option value="16- AGUARDANDO FATURAMENTO" <?php if($dados[0]->situacao_status == '16- AGUARDANDO FATURAMENTO'){ echo "SELECTED";} ?> >16- AGUARDANDO FATURAMENTO</option>

                  <option value="17- O.S VERIFICADA E ARQUIVADA" <?php if($dados[0]->situacao_status == '17- O.S VERIFICADA E ARQUIVADA'){ echo "SELECTED";}?> >17- O.S VERIFICADA E ARQUIVADA</option>                          
                </select>
              </div>

              <div class="form-group col-md-8">
                <label>Observação</label>
                  <input type="text" class="form-control" name="situacao_obs" value="<?php echo $dados[0]->situacao_obs; ?>" placeholder="Observação">
              </div>

              <!-- <br><br><br> -->
              <div class="form-group col-md-12">
                <button type="submit" class="btn btn-primary pull-left">Alterar Situação</button>
              </div>
           

            </div>
            <!-- /.col -->
           
              
             
            <div class="form-group col-md-12">
            <h3 class="text-primary both col-sm-12 col-lg-12 col-md-12 margin-top-10px">Histórico de situações</h3> 
            </div>

            <div class="col-md-12">       

              <!-- <div class="table-responsive"> -->
                <div class="">
                    <div class="col-sm-12 col-lg-12 col-md-12">
                        <table cellpadding="0" cellspacing="0" class="table table-bordered">
                          <tbody>
                            <tr>
                              <th width="15%" style="border: 1px solid;">Data</th>
                              <th width="50%" style="border: 1px solid;">Observação</th>
                              <th width="15%" style="border: 1px solid;">Situação</th>
                              <!-- <th width="15%" class="actions">Situação</th> -->
                              <th width="20%" style="border: 1px solid;">Funcionário</th>
                            </tr>
                            
                            <?php foreach ($dados as $dado){ ?>
                            <tr>
                              <td style="border: 1px solid;"><?php echo date('d/m/Y h:i:s',  strtotime($dado->situacao_data_atualizacao)) ?></td>
                              <td style="border: 1px solid;" ><?php echo $dado->situacao_obs;    ?></td>
                              <td style="border: 1px solid;" ><?php echo $dado->situacao_status; ?></td>

                     <!--          <td class="actions">
                                  <span class="label" style="background-color:#F4CCCC">2- EM ANÁLISE TÉCNICA</span>
                              </td> -->
                              <td style="border: 1px solid;" ><?php echo $dado->usuario_nome;  ?></td>
                            </tr>
                             <?php } ?>
                            
   
                          </tbody>
                        </table>
                    </div>
                </div>
              <!-- </div> -->


            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->


          <!-- /.box-body -->
          <!-- <div class="box-footer"> -->
            <!-- <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>" class="btn btn-default">Voltar</a> -->
            <!-- <button type="submit" class="btn btn-primary pull-right"><?php echo ucfirst($this->uri->segment(2)); ?></button> -->
          <!-- </div> -->
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
  </div>
  
</section>


    </div>
  </body>
</html>