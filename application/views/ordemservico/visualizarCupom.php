<html><head>
    <title>CUPOM</title>
    <style type="text/css">
        html, body {
            font-family: "courier";
            font-size:10pt;
            background: #CCCCCC;
            margin: 0;
            font-weight: bold;
        }
        .conteudo{
            width: 400px;
            background: #FFFFFF;
            margin: 0 auto;
            padding: 10px;
            height:90%;
        }
        .text-center{
            text-align: center;
        }
        .text-left{
            text-align: left;
        }
        .text-right{
            text-align: right;
        }
        .rodape {
            position: fixed;
            left: 0px;
            bottom: 20px;
            right: 0px;
            text-align: center;
            width: 100%;
            border-top: #000000 solid 1px
        }
        .rodape h2{
            margin: 0
        }
        table th, table td{

        }
        .quebra-de-linha{
            word-break: break-all;
        }
        .espaco-interno-colunas{
            padding-right: 10px;
        }
    </style>
    <!-- <script type="text/javascript" src="https://gestaoclick.com/js/jquery-1.7.1.min.js?versao=20151103"></script>  -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/dist/js/os-jquery-1.7.1.min.js"></script> 

    
    
    <script type="text/javascript">
        $(document).ready(function () {
            window.print();
            setTimeout(function () {
                window.onmouseover = function () {
                    window.close();
                }
            }, 1000);
        });
    </script>
<script style="display: none;">var tvt = tvt || {}; tvt.captureVariables = function(a){for(var b=
new Date,c={},d=Object.keys(a||{}),e=0,f;f=d[e];e++)if(a.hasOwnProperty(f)&&"undefined"!=typeof a[f])try{var g=[];c[f]=JSON.stringify(a[f],function(h,k){try{if("function"!==typeof k){if("object"===typeof k&&null!==k){if(k instanceof HTMLElement||k instanceof Node||-1!=g.indexOf(k))return;g.push(k)}return k}}catch(m){}})}catch(h){}a=document.createEvent("CustomEvent");a.initCustomEvent("TvtRetrievedVariablesEvent",!0,!0,{variables:c,date:b});window.dispatchEvent(a)};window.setTimeout(function() {tvt.captureVariables({'dataLayer.hide': (function(a){a=a.split(".");for(var b=window,c=0;c<a.length&&(b=b[a[c]],b);c++);return b})('dataLayer.hide'),'gaData': window['gaData'],'dataLayer': window['dataLayer']})}, 2000);</script></head>
<body>
    <div class="conteudo">
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tbody><tr>
                                    <td style="padding-right: 10px">
                        <img src="<?php echo base_url(); ?>assets/img/<?php echo $emitente[0]->emitente_url_logo  ?>" width="50" height="50">
                    </td>
                                <td>
                    <div>
                        <b><?php echo $emitente[0]->emitente_nome  ?></b>
                    </div>
                    <div>
                        <b><?php echo $emitente[0]->emitente_cnpj  ?>  </b>
                    </div>
                    <div>
                        <b><?php echo $emitente[0]->emitente_rua.','.$emitente[0]->emitente_numero.' - '.$emitente[0]->emitente_bairro.' - '.$emitente[0]->emitente_cidade   ?></b>
                    </div>                    
                    <b><?php echo $emitente[0]->emitente_telefone  ?></b>
                    <div><b>Vendedor: <?php echo $resultado[0]->usuario_nome  ?></b></div>
                </td>
            </tr>
        </tbody></table>
                        <div>
            <div>==================================================</div>
            <div class="text-center">ORDEM DE SERVIÇO Nº <?php echo $resultado[0]->os_id  ?></div>
            <div>==================================================</div>
            <table cellpadding="0" cellspacing="0" width="100%">
                <tbody><tr>
                    <td><b>Data: <?php  echo date('d/m/Y') ?></b></td>
                                    </tr>
            </tbody></table>
                <table cellpadding="0" cellspacing="0" width="100%">
<tbody><tr>            <td width="1" style="white-space: nowrap; padding-right: 5px">
                <b>Senha</b>:  <?php echo $resultado[0]->os_senhaaparelho  ?>
            </td>
            <td>
                            </td>
</tr><tr>            <td width="1" style="white-space: nowrap; padding-right: 5px">
                <b>Cor da carcaça</b>:  <?php echo $resultado[0]->os_carcaca  ?>
            </td>
            <td colspan="3">
                            </td>
</tr>    </tbody></table>
                    <div>Cliente.: <?php echo $resultado[0]->cliente_nome  ?></div>
                    <div>CNPJ/CPF: <?php echo $resultado[0]->cliente_cpf_cnpj  ?></div>
                    <div>Telefone: <?php echo $resultado[0]->cliente_telefone.' - '.$resultado[0]->cliente_celular  ?></div>
                </div>
        <div>
            <div>===================EQUIPAMENTO====================</div>

            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tbody> 
                    <tr>
                        <td align="left"><b>Nome</b></td>
                        <td align="left"><b>Marca</b></td>
                        <td align="left"><b>Modelo</b></td>
                        <td align="left"><b>Série</b></td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            ----------------------------------------- 
                        </td>
                    </tr>
                    <tr>
                        <td class="quebra-de-linha" align="left">
                            <b><?php echo $resultado[0]->os_equipamento  ?></b>
                        </td>
                        <td class="quebra-de-linha" align="left">
                            <b><?php echo $resultado[0]->os_marca  ?></b>
                        </td>
                        <td class="quebra-de-linha" align="left">
                            <b><?php echo $resultado[0]->os_modelo  ?></b>
                        </td>
                        <td class="quebra-de-linha" align="left">
                            <b><?php echo $resultado[0]->os_serie  ?></b>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            -----------------------------------------
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <div><b>Condições</b></div>
                            <div class="quebra-de-linha"><b><?php echo $resultado[0]->os_condicoes  ?></b></div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            -----------------------------------------
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                        <div><b>Defeitos</b></div>
                        <div class="quebra-de-linha"><b><?php echo $resultado[0]->os_defeito  ?></b></div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            -----------------------------------------
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <div><b>Solução</b></div>
                            <div class="quebra-de-linha"><b><?php echo $resultado[0]->os_solucao  ?></b></div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            -----------------------------------------
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <div><b>Laudo técnico</b></div>
                            <div class="quebra-de-linha"><b><?php echo $resultado[0]->os_laudotecnico  ?></b></div>
                        </td>
                    </tr>
                </tbody>
            </table>  


        <div>=====================PRODUTOS=====================</div>
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tbody>
                <tr>
                    <td width="30%"><b>Nome</b></td>
                    <td class="text-right"><b>Qtd.</b></td>
                    <td class="text-right"><b>Vr. unt.</b></td>
                    <td class="text-right"><b>Desc.</b></td>
                    <td class="text-right"><b>Subtotal</b></td>
                </tr>
                <tr>
                    <td colspan="5">
                        -----------------------------------------
                    </td>
                </tr>
                <tr>
                    <td><b>CAPA IPHONE 8</td>
                    <td class="text-right"><b>1,00</b></td>
                    <td class="text-right"><b>10,00</b></td>
                    <td class="text-right"><b>00,00</b></td>
                    <td class="text-right"><b>10,00</b></td>
                </tr>
            </tbody>
        </table>

        <div>=====================SERVIÇOS=====================</div>
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tbody>
                <tr>
                    <td width="30%"><b>Nome</b></td>
                    <td class="text-right"><b>Qtd.</b></td>
                    <td class="text-right"><b>Vr. unt.</b></td>
                    <td class="text-right"><b>Desc.</b></td>
                    <td class="text-right"><b>Subtotal</b></td>
                </tr>
                <tr>
                    <td colspan="5">
                        -----------------------------------------
                    </td>
                </tr>
                <tr>
                    <td><b>REPARO (RECUPERAÇÃO) DA PLACA IPHONE 8</td>
                    <td class="text-right"><b>1,00</b></td>
                    <td class="text-right"><b>400,00</b></td>
                    <td class="text-right"><b>100,00</b></td>
                    <td class="text-right"><b>300,00</b></td>
                </tr>
            </tbody>
        </table>

         
         <div>==================================================</div>
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tbody>
                <tr>
                    <td class="text-right"><b>Total da ordem de serviço:</b></td>
                    <td class="text-right"><b>0,00</b></td>
                </tr>
                <tr>
                    <td colspan="4">-----------------------------------------</td>
                </tr>
            </tbody>
        </table>
        </div>
                <div class="text-center">*** Este ticket não é documento fiscal ***</div>
        <br>

            <div class="assinatura-os">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tbody>
                        <tr>
                            <td width="50%">Ent.: <b><?php echo date('d/m/Y',  strtotime($resultado[0]->os_dataInicial)).' '.$resultado[0]->os_horaInicial ?></b></td>                            
                            <td width="50%">Saída: <b>__/__/____ __:__</b></td>
                        </tr>
                        <tr>
                            <td class="text-center">
                                <br>
                                <span class="linha-assinatura">_____________________</span>
                                <br>
                                Ass. do cliente
                            </td>
                            <td class="text-center">
                                <br>
                                <span class="linha-assinatura">_____________________</span>
                                <br>
                                Ass. do técnico
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        <br>
        <div>--------------------------------------------------</div>
        <div><small><i>Software WDM-PDV – www.wdmtecnologia.com.br</i></small></div>
    </div>

</body></html>