<br><br><section class="content">
  <div class="box">
    <div class="box-header with-border">
      <div class="col-md-2">
        <?php if(verificarPermissao('aOrdemServico')){ ?>
        <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/adicionar" type="button" class="btn btn-block btn-default btn-flat">Adicionar</a>
        <?php } ?>
      </div>          
    </div>
    <div class="box-body">
      <div class="container-fluid">
        <table id="tabOrdemServico" class="table" width="100%">
        <thead>
          <tr>    
              <th class="columns01 col_default">#</th>
              <th class="columns02 col_default">Cliente</th>
              <th class="columns01 col_default">Entrada</th>
              <th class="columns01 col_default">Saida</th>
              <th class="columns01 col_default">Equipamentos</th>
              <th class="columns01 col_default">Situação</th>
              <th class="columns02 col_default">Valor</th> 
              <th>Ações</th>           
          </tr>
          </thead>
          <tbody>
  
          </tbody>

      </table>
        </div>

     </div>
  </div>

</section>


<!-- Modal ZAP-->
<div class="example-modal">
  <div class="modal" id="modalwhatsapp">
    <div class="modal-dialog">     
        <!-- Modal content-->
        <div class="modal-content">
           <!-- <form id="formReceita" action="<?php echo base_url() ?>index.php/ordemservico/enviarzap" method="post"> -->
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Enviar por WhatsApp</h3>
              </div>
              <div class="modal-body">      

              <div class="col-xs-12 alert alert-info" style="margin-left: 0"> Obrigatório o preenchimento dos campos com asterisco.</div>

              <div class="col-xs-12"> 
                <label for="cliente">Celular*</label>
                <input type="text" class="form-control celular" name="celular" id="celular" value="">
                <input type="hidden" class="form-control os_id" id="os_id" name="os_id" value="">
              </div>       

              <div class="col-xs-12">
                <label class="control-label" for="descricao">Descrição</label>
                <div class="form-group">
                  <textarea class="form-control" rows="3" id="descricao" name="descricao" value="" placeholder="Enter ..."></textarea>
                </div>
              </div>       


            </div>

          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-success" onclick="clickyClick()" >Enviar</button>
          </div>
          <!-- </form> -->
          </div>
          </div>
      </div>
  </div>


<!-- Modal EMAIL-->
<div class="example-modal">
  <div class="modal" id="modalemail">
    <div class="modal-dialog">     
        <!-- Modal content-->
        <div class="modal-content">
           <!-- <form id="formReceita" action="<?php echo base_url() ?>index.php/ordemservico/enviarzap" method="post"> -->
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Enviar por E-mail</h3>
              </div>
              <div class="modal-body">      

              <div class="col-xs-12 alert alert-info" style="margin-left: 0"> Obrigatório o preenchimento dos campos com asterisco.</div>

              <div class="col-xs-12"> 
                <label for="cliente">Email*</label>
                <input type="email" class="form-control email" name="email" id="email" value="">
                <input type="hidden" class="form-control os_id_email" id="os_id_email" name="os_id_email" value="">
              </div>       

              <div class="col-xs-12">
                <label class="control-label" for="descricao">Descrição</label>
                <div class="form-group">
                  <textarea class="form-control" rows="3" id="descricaoemail" name="descricaoemail" value="" placeholder="Enter ..."></textarea>
                </div>
              </div>       


            </div>

          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-success"  id="enviarsituacao" >Enviar</button>
          </div>
          <!-- </form> -->
          </div>
          </div>
      </div>
  </div>


<!-- Modal SITUACAO-->
<div class="modal fade bd-example-modal-lg" id="modalsituacao" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
       <!-- <form id="formReceita" action="<?php echo base_url() ?>index.php/ordemservico/enviarzap" method="post"> -->
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <!-- <h3 id="myModalLabel">Alterar situação</h3> -->
          </div>

          <div class="modal-body" style="background-color: #ecf0f5;">      
            <iframe width="100%" height="500px" frameborder="none"></iframe>     
          </div>
          <input type="hidden" id="idSituacaoOsModal"/>

       <div class="modal-footer">
      <!--   <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
        <button class="btn btn-success" onclick="clickyClick()" >Enviar</button> -->
      </div> 
      <!-- </form> -->
    </div>
  </div>
</div>

 <!--  MODAL  -->
 <div class="modal fade" id="modalSenha" data-backdrop="static" data-keyboard="false" style="z-index: 99999" tabindex="-1" role="dialog">
    <div class="modal-dialog" style="width: 350px; margin: 100px auto">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" onclick="closeModal()" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title text-center">Informar senha</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <p>Informe a senha do gerente para liberação desta ação</p>
              <div class="form-group">
                <input type="password" onkeyup="habilitarBTN(this.value)" id="input-liberar-senha" class="form-control" placeholder="Senha do gerente" />
                <input type="hidden" id="idOsModal"/>
              </div>
              <button id="btn-confirm" disabled class="btn btn-primary btn-block">CONFIRMAR</button>
            </div>
          </div>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>


<script type="text/javascript">

    function idSituacaoOsModal(idVendas) {      
      $('#idSituacaoOsModal').val(idVendas);
      // let id = $('#idSituacaoOsModal').val();      
    }


 
$(document).ready(function(){

   // let id = $('#modalsituatao').attr('datasituacao');


    $('#modalsituacao').on('show.bs.modal', function () {
      let id = $('#idSituacaoOsModal').val();
    $('#modalsituacao iframe').attr('src','<?php echo base_url(); ?>ordemservico/situacaoOs/'+id);  
    });

    $('#modalsituacao').on('hide.bs.modal', function () {
    $('#modalsituacao iframe').removeAttr('src');

    });
 

    $(document).on('click', 'a', function(event) {
        
        var celular = $(this).attr('celular');
        $('.celular').val(celular);

    });

    $(document).on('click', 'a', function(event) {     

        var os_id = $(this).attr('os_id');
        $('.os_id').val(os_id);

    });

    $(document).on('click', 'a', function(event) {
        
        var email = $(this).attr('email');
        $('.email').val(email);

    });

    $(document).on('click', 'a', function(event) {     

        var os_id_email = $(this).attr('os_id_email');
        $('.os_id_email').val(os_id_email);

    });

    $(document).on('click', 'a', function(event) {     

        var os_id_situacao = $(this).attr('os_id_situacao');
        $('.os_id_situacao').val(os_id_situacao);


    });

});


  function clickyClick() {
    numeroZap = document.getElementById("celular").value.replace(/[^0-9]+/g,'');
    url = 'https://wa.me/55' + numeroZap + '?text=Obrigado pela preferência!%0aSua O.S encontra-se: '+ document.getElementById("descricao").value ;

    window.open(url, '_blank');
    location.reload();
  }



  $('#enviaremail').click(function(event) { 

    descricaoemail = $('#descricaoemail').val();
    email      = $('#email').val();
    mensagem   = 'Obrigado pela preferência! <br> '+ descricaoemail+'';

    
    
      $.ajax({
          url: '<?php echo base_url(); ?>ordemservico/sendmail',
          type: 'POST',
          dataType: 'json',
          data: {email: email,mensagem: mensagem},
      })
      .done(function(result) {                     

          if(result.status){
            location.reload();
          }else{
             
          }           

      });         

  });

  
  function habilitarBTN(valor) {
      const el = document.querySelector('#btn-confirm')
      if(valor.length > 0) {
        el.disabled = false
      } else {
          el.disabled = true
      }
    }

    function closeModal() {
        $('#modalSenha').modal('hide');
        document.querySelector('#input-liberar-senha').value = null;
        document.querySelector('#idOsModal').value = null;
        const el = document.querySelector('#btn-confirm')
        el.disabled = true
    }

    function idOsModal(idOs) {
      $('#idOsModal').val(idOs);
    }

    $('#btn-confirm').click(function(event) {

        let senha = $('#input-liberar-senha').val();
        let id = $('#idOsModal').val();

        $.ajax({
          method: "POST",
          url: base_url+"pdv2/validacaoGerente/",
          dataType: "JSON",
          data: { senha },
          success: function(data)
          {
            if(data.result == true){
              window.location.href = base_url+'ordemservico/editar/'+id;
              closeModal();
            }else{
              alert('Senha Invalida.');
              location.reload();
            }
          }
          
        });
    })


</script>

