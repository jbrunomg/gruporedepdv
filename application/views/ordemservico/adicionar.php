<section class="content">
  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->

        <form  action="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/adicionar" method="post">
        <div class="row">  
        <div class="col-md-12">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Detalhes da OS - Celular</a></li>
               
   
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">

                <div class="row">
                  <div class="col-md-12">

      

                    <div class="form-group col-md-6">
                      <label>Cliente*</label>
                      <input type="text" class="form-control" name="cliente_nome" id="cliente_nome" value="<?php echo set_value('cliente_nome'); ?>" placeholder="Nome do Cliente">
                      <input type="hidden" id="cliente_id" name="cliente_id" value="" >
                    </div>

  
                    <div class="form-group col-md-6">
                      <label>Situação*</label>
                        <select class="form-control" name="os_status" id="os_status">                          
                          <option value="1- AGUARDANDO ORÇAMENTO">1- AGUARDANDO ORÇAMENTO</option>
                          <option value="2- EM ANÁLISE TÉCNICA">2- EM ANÁLISE TÉCNICA</option>
                          <option value="3- AGUARDANDO APROVAÇÃO DO CLIENTE">3- AGUARDANDO APROVAÇÃO DO CLIENTE</option>
                          <option value="4- ORÇAMENTO APROVADO">4- ORÇAMENTO APROVADO</option>
                          <option value="5- ORÇAMENTO NÃO APROVADO">5- ORÇAMENTO NÃO APROVADO</option>
                          <option value="6- EXECUTANDO ">6- EXECUTANDO</option>
                          <option value="7- CONCLUÍDA COM ÊXITO (LIBERADO PARA FATURAMENTO)">7- CONCLUÍDA COM ÊXITO (LIBERADO PARA FATURAMENTO) </option>
                          <option value="8- CONCLUÍDA COM ÊXITO(SEM FATURAMENTO)">8- CONCLUÍDA COM ÊXITO(SEM FATURAMENTO)</option>
                          <option value="9- CONCLUÍDA SEM ÊXITO ">9- CONCLUÍDA SEM ÊXITO</option>
                          <option value="10- O.S RETIRADA (COM FATURAMENTO)">10- O.S RETIRADA (COM FATURAMENTO)</option>      
                          <option value="10.1 FATURADO E NÃO RETIRADO ">10.1 FATURADO E NÃO RETIRADO</option>

                          <option value="11- O.S RETIRADA (SEM FATURAMENTO)">11- O.S RETIRADA (SEM FATURAMENTO)</option>
                          <option value="12- VERIFICAR GARANTIA">12- VERIFICAR GARANTIA</option>
                          <option value="13- GARANTIA VERIFICADA OK (EXECUTANDO)">13- GARANTIA VERIFICADA OK (EXECUTANDO)</option>
                          <option value="14- GARANTIA NÅO APROVADA">14- GARANTIA NÅO APROVADA</option>

                          <option value="15- AGUARDANDO PEÇA">15- AGUARDANDO PEÇA</option>
                          <option value="16- AGUARDANDO FATURAMENTO">16- AGUARDANDO FATURAMENTO</option>

                          <option value="17- O.S VERIFICADA E ARQUIVADA">17- O.S VERIFICADA E ARQUIVADA</option>     

                          <option value="18- ENVIADO P/ASSISTENCIA">18- ENVIADO P/ASSISTENCIA</option>  
                          <option value="19- RECEBIDO DA ASSISTENCIA">19- RECEBIDO DA ASSISTENCIA</option>  
                          <option value="20- ENTREGUE AO CLIENTE">20- ENTREGUE AO CLIENTE</option>                       
                        </select>
                    </div>


                    <div class="form-group col-md-3">
                      <label>Data entrada</label>
                        <input type="date" class="form-control money" name="os_dataInicial" id="os_dataInicial" value="<?php echo set_value('os_dataInicial'); ?>" >
                    </div>

                    <div class="form-group col-md-3">
                      <label>Hora entrada</label>
                        <input type="time" class="form-control money" name="os_horaInicial" id="os_horaInicial" value="<?php echo set_value('os_horaInicial'); ?>" >
                    </div>

                    <div class="form-group col-md-3">
                      <label>Data saída</label>
                        <input type="date" class="form-control money" name="os_dataFinal" id="os_dataFinal" value="<?php echo set_value('os_dataFinal'); ?>" >
                    </div>

                    <div class="form-group col-md-3">
                      <label>Hora saída</label>
                        <input type="time" class="form-control money" name="os_horaFinal" id="os_horaFinal" value="<?php echo set_value('os_horaFinal'); ?>" >
                    </div>

                    <div class="form-group col-md-6">
                      <label>Equipamento</label>
                      <input type="text" class="form-control" name="os_equipamento" id="os_equipamento" value="<?php echo set_value('os_equipamento'); ?>" placeholder="Equipamento">
                    </div>

                    <div class="form-group col-md-2">
                      <label>Marca</label>
                      <input type="text" class="form-control" name="os_marca" id="os_marca" value="<?php echo set_value('os_marca'); ?>" placeholder="Marca">
                    </div>
                    
                    <div class="form-group col-md-2">
                      <label>Modelo</label>
                      <input type="text" class="form-control" name="os_modelo" id="os_modelo" value="<?php echo set_value('os_modelo'); ?>" placeholder="Modelo">
                    </div>

                    <div class="form-group col-md-2">
                      <label>Série</label>
                      <input type="text" class="form-control" name="os_serie" id="os_serie" value="<?php echo set_value('os_serie'); ?>" placeholder="Serie">
                    </div>

                  </div>
                  <!-- /.col -->
                  <div class="col-md-12">

                    <div class="form-group col-md-4">
                      <label>Usuário cadastro</label>
                      <input type="text" class="form-control" name="nome" id="nome" value="<?php echo $this->session->userdata('usuario_nome'); ?>"  readonly>
                      <input type="hidden" name="usuario_id" value="<?php echo $this->session->userdata('usuario_id'); ?>">
                    </div>

                    <div class="form-group col-md-2">
                      <label>Data cadastro</label>
                      <input type="text" class="form-control" name="os_data_cadastro" id="os_data_cadastro" value="<?php echo date("d/m/Y"); ?>"  readonly>
                    </div>

                    <div class="form-group col-md-6">
                      <label>Técnico / Responsável*</label>
                      <input type="text" class="form-control" name="tec_nome" id="usuario_nome" value="<?php echo set_value('tec_codigo'); ?>" placeholder="Nome do Tecnico">
                      <input type="hidden" id="usuario_id" name="tec_id" value="">
                    </div>


                  </div>
                  <!-- /.col -->
                </div>

              </div>
              <!-- /.tab-pane -->
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        </div>
        <!-- /.col -->
          <!-- /.box-body -->
          <div class="box-footer">
            <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>" class="btn btn-default">Voltar</a>
            <button type="submit" class="btn btn-primary pull-right">Continuar</button>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
  </div>
</section>


<script type="text/javascript">

  $(function () {


    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })

  })


</script>