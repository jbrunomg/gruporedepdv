<section class="content">
  <div class="row">
    <div class="col-md-12">
      <!-- Horizontal Form -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

        <!-- <form  action="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>/adicionar" method="post"> -->
        <div class="row">
          <div class="col-md-12">
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" id='ostab' data-toggle="tab">OS</a></li>
                <li><a href="#tab_2" data-toggle="tab">Produtos</a></li>
                <li><a href="#tab_3" data-toggle="tab">Serviços</a></li>
                <li><a href="#tab_4" data-toggle="tab">Anexos</a></li>
                <li><a href="#tab_5" data-toggle="tab">Assinatura</a></li>
              </ul>

              <div class="tab-content">
                <div class="tab-pane active" id="tab_1">

                  <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab1" data-toggle="tab">Detalhe OS</a></li>
                    <li><a href="#tab2" data-toggle="tab">Laudo Técnico</a></li>
                    <!--  <li><a href="#tab_3" data-toggle="tab">Serviços</a></li>
                  <li><a href="#tab_4" data-toggle="tab">Anexos</a></li> -->
                  </ul>

                  <form action="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>/editarExe" method="post">

                    <div class="tab-content">
                      <div class="tab-pane active" id="tab1">



                        <input type="hidden" id="id" name="id" value="<?php echo $dados[0]->os_id; ?>">
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group col-md-12">
                              <h3>#O.S: <?php echo $dados[0]->os_id; ?></h3>
                            </div>

                            <div class="form-group col-md-6">
                              <label>Cliente*</label>
                              <input type="text" class="form-control" name="cliente_nome" id="cliente_nome" value="<?php echo $dados[0]->cliente_nome; ?>" placeholder="Nome do Cliente">
                              <input type="hidden" id="cliente_id" name="cliente_id" value="<?php echo $dados[0]->cliente_id; ?>">
                            </div>


                            <?php if ($dados[0]->os_status == 'FATURADO') { ?>

                              <div class="form-group col-md-6">
                                <label>Situação*</label>
                                <select class="form-control" name="os_status" id="os_status" readonly="">
                                  <option value="FATURADO" SELECTED>FATURADO</option>
                                </select>
                              </div>

                            <?php  } else {   ?>

                              <div class="form-group col-md-6">
                                <label>Situação*</label>
                                <input type="hidden" id="os_status_anterior" name="os_status_anterior" value="<?php echo $dados[0]->os_status; ?>">
                                <select class="form-control" name="os_status" id="os_status">

                                  <option value="1- AGUARDANDO ORÇAMENTO" <?php if ($dados[0]->os_status == '1- AGUARDANDO ORÇAMENTO') {
                                                                            echo "SELECTED";
                                                                          } ?>>1- AGUARDANDO ORÇAMENTO</option>
                                  <option value="2- EM ANÁLISE TÉCNICA" <?php if ($dados[0]->os_status == '2- EM ANÁLISE TÉCNICA') {
                                                                          echo "SELECTED";
                                                                        } ?>>2- EM ANÁLISE TÉCNICA</option>
                                  <option value="3- AGUARDANDO APROVAÇÃO DO CLIENTE" <?php if ($dados[0]->os_status == '3- AGUARDANDO APROVAÇÃO DO CLIENTE') {
                                                                                        echo "SELECTED";
                                                                                      } ?>>3- AGUARDANDO APROVAÇÃO DO CLIENTE</option>
                                  <option value="4- ORÇAMENTO APROVADO" <?php if ($dados[0]->os_status == '4- ORÇAMENTO APROVADO') {
                                                                          echo "SELECTED";
                                                                        } ?>>4- ORÇAMENTO APROVADO</option>
                                  <option value="5- ORÇAMENTO NÃO APROVADO" <?php if ($dados[0]->os_status == '5- ORÇAMENTO NÃO APROVADO') {
                                                                              echo "SELECTED";
                                                                            } ?>>5- ORÇAMENTO NÃO APROVADO</option>
                                  <option value="6- EXECUTANDO" <?php if ($dados[0]->os_status == '6- EXECUTANDO') {
                                                                  echo "SELECTED";
                                                                } ?>>6- EXECUTANDO</option>

                                  <option value="7- CONCLUÍDA COM ÊXITO (LIBERADO PARA FATURAMENTO)" <?php if ($dados[0]->os_status == '7- CONCLUÍDA COM ÊXITO (LIBERADO PARA FATURAMENTO)') {
                                                                                                        echo "SELECTED";
                                                                                                      } ?>>7- CONCLUÍDA COM ÊXITO (LIBERADO PARA FATURAMENTO) </option>

                                  <option value="8- CONCLUÍDA COM ÊXITO(SEM FATURAMENTO)" <?php if ($dados[0]->os_status == '8- CONCLUÍDA COM ÊXITO(SEM FATURAMENTO)') {
                                                                                            echo "SELECTED";
                                                                                          } ?>>8- CONCLUÍDA COM ÊXITO(SEM FATURAMENTO)</option>
                                  <option value="9- CONCLUÍDA SEM ÊXITO" <?php if ($dados[0]->os_status == '9- CONCLUÍDA SEM ÊXITO') {
                                                                            echo "SELECTED";
                                                                          } ?>>9- CONCLUÍDA SEM ÊXITO</option>
                                  <option value="10- O.S RETIRADA (COM FATURAMENTO)" <?php if ($dados[0]->os_status == '10- O.S RETIRADA (COM FATURAMENTO)') {
                                                                                        echo "SELECTED";
                                                                                      } ?>>10- O.S RETIRADA (COM FATURAMENTO)</option>
                                  <option value="10.1 FATURADO E NÃO RETIRADO" <?php if ($dados[0]->os_status == '10.1 FATURADO E NÃO RETIRADO') {
                                                                                  echo "SELECTED";
                                                                                } ?>>10.1 FATURADO E NÃO RETIRADO</option>

                                  <option value="11- O.S RETIRADA (SEM FATURAMENTO)" <?php if ($dados[0]->os_status == '11- O.S RETIRADA (SEM FATURAMENTO)') {
                                                                                        echo "SELECTED";
                                                                                      } ?>>11- O.S RETIRADA (SEM FATURAMENTO)</option>
                                  <option value="12- VERIFICAR GARANTIA" <?php if ($dados[0]->os_status == '12- VERIFICAR GARANTIA') {
                                                                            echo "SELECTED";
                                                                          } ?>>12- VERIFICAR GARANTIA</option>
                                  <option value="13- GARANTIA VERIFICADA OK (EXECUTANDO)" <?php if ($dados[0]->os_status == '13- GARANTIA VERIFICADA OK (EXECUTANDO)') {
                                                                                            echo "SELECTED";
                                                                                          } ?>>13- GARANTIA VERIFICADA OK (EXECUTANDO)</option>
                                  <option value="14- GARANTIA NÅO APROVADA" <?php if ($dados[0]->os_status == '14- GARANTIA NÅO APROVADA') {
                                                                              echo "SELECTED";
                                                                            } ?>>14- GARANTIA NÅO APROVADA</option>

                                  <option value="15- AGUARDANDO PECA" <?php if ($dados[0]->os_status == '15- AGUARDANDO PECA') {
                                                                        echo "SELECTED";
                                                                      } ?>>15- AGUARDANDO PEÇA</option>

                                  <option value="16- AGUARDANDO FATURAMENTO" <?php if ($dados[0]->os_status == '16- AGUARDANDO FATURAMENTO') {
                                                                                echo "SELECTED";
                                                                              } ?>>16- AGUARDANDO FATURAMENTO</option>

                                  <option value="17- O.S VERIFICADA E ARQUIVADA" <?php if ($dados[0]->os_status == '17- O.S VERIFICADA E ARQUIVADA') {
                                                                                    echo "SELECTED";
                                                                                  } ?>>17- O.S VERIFICADA E ARQUIVADA</option>

                                  <option value="18- ENVIADO P/ASSISTENCIA" <?php if ($dados[0]->os_status == '18- ENVIADO P/ASSISTENCIA') {
                                                                              echo "SELECTED";
                                                                            } ?>>18- ENVIADO P/ASSISTENCIA</option>
                                  <option value="19- RECEBIDO DA ASSISTENCIA" <?php if ($dados[0]->os_status == '19- RECEBIDO DA ASSISTENCIA') {
                                                                                echo "SELECTED";
                                                                              } ?>>19- RECEBIDO DA ASSISTENCIA</option>
                                  <option value="20- ENTREGUE AO CLIENTE" <?php if ($dados[0]->os_status == '20- ENTREGUE AO CLIENTE') {
                                                                            echo "SELECTED";
                                                                          } ?>>20- ENTREGUE AO CLIENTE</option>
                                </select>
                              </div>


                            <?php } ?>


                            <div class="form-group col-md-3">
                              <label>Data entrada</label>
                              <input type="date" class="form-control" name="os_dataInicial" id="os_dataInicial" value="<?php echo $dados[0]->os_dataInicial; ?>">
                            </div>

                            <div class="form-group col-md-3">
                              <label>Hora entrada</label>
                              <input type="time" class="form-control" name="os_horaInicial" id="os_horaInicial" value="<?php echo $dados[0]->os_horaInicial; ?>">
                            </div>

                            <div class="form-group col-md-3">
                              <label>Data saída</label>
                              <input type="date" class="form-control" name="os_dataFinal" id="os_dataFinal" value="<?php echo $dados[0]->os_dataFinal; ?>">
                            </div>

                            <div class="form-group col-md-3">
                              <label>Hora saída</label>
                              <input type="time" class="form-control" name="os_horaFinal" id="os_horaFinal" value="<?php echo $dados[0]->os_horaFinal; ?>">
                            </div>

                            <div class="form-group col-md-6">
                              <label>Equipamento</label>
                              <input type="text" class="form-control" name="os_equipamento" id="os_equipamento" value="<?php echo $dados[0]->os_equipamento; ?>" placeholder="Equipamento">
                            </div>

                            <div class="form-group col-md-2">
                              <label>Marca</label>
                              <input type="text" class="form-control" name="os_marca" id="os_marca" value="<?php echo $dados[0]->os_marca; ?>" placeholder="Marca">
                            </div>

                            <div class="form-group col-md-2">
                              <label>Modelo</label>
                              <input type="text" class="form-control" name="os_modelo" id="os_modelo" value="<?php echo $dados[0]->os_modelo; ?>" placeholder="Modelo">
                            </div>

                            <div class="form-group col-md-2">
                              <label>Série</label>
                              <input type="text" class="form-control" name="os_serie" id="os_serie" value="<?php echo $dados[0]->os_serie; ?>" placeholder="Serie">
                            </div>

                          </div>
                          <!-- /.col -->
                          <div class="col-md-12">

                            <div class="form-group col-md-4">
                              <label>Usuário cadastro</label>
                              <input type="text" class="form-control" name="nome" id="nome" value="<?php echo $this->session->userdata('usuario_nome'); ?>" readonly>
                              <input type="hidden" name="usuario_id" value="<?php echo $this->session->userdata('usuario_id'); ?>">
                            </div>

                            <div class="form-group col-md-2">
                              <label>Data cadastro</label>
                              <input type="date" class="form-control" name="os_data_cadastro" id="os_data_cadastro" value="<?php echo $dados[0]->os_data_cadastro; ?>" readonly>
                            </div>

                            <div class="form-group col-md-6">
                              <label>Técnico / Responsável*</label>
                              <input type="text" class="form-control" name="tec_nome" id="usuario_nome" value="<?php echo $dados[0]->tec_nome; ?>" placeholder="Nome do Tecnico">
                              <input type="hidden" id="usuario_id" name="tec_id" value="<?php echo $dados[0]->tec_id; ?>">
                            </div>


                          </div>
                          <!-- /.col -->
                        </div>


                      </div>

                      <div class="tab-pane" id="tab2">

                        <div class="row">
                          <div class="col-md-12">

                            <div class="form-group col-md-3">
                              <label>Cor da carcaça</label>
                              <input type="text" class="form-control" name="os_carcaca" id="os_carcaca" value="<?php echo $dados[0]->os_carcaca; ?>" placeholder="Cor carcaça">

                            </div>

                            <div class="form-group col-md-3">
                              <label>Senha</label>
                              <input type="text" class="form-control" name="os_senhaaparelho" id="os_senhaaparelho" value="<?php echo $dados[0]->os_senhaaparelho; ?>" placeholder="Senha Aparelho">

                            </div>

                            <div class="form-group col-md-6">
                              <br>
                              <br>
                              <br>
                            </div>

                            <div class="form-group col-md-6">
                              <label>Condições</label>
                              <textarea class="form-control" rows="3" name="os_condicoes" value="" placeholder="Enter ..."><?php echo $dados[0]->os_condicoes; ?></textarea>
                            </div>

                            <div class="form-group col-md-6">
                              <label>Defeitos</label>
                              <textarea class="form-control" rows="3" name="os_defeito" value="" placeholder="Enter ..."><?php echo $dados[0]->os_defeito; ?></textarea>
                            </div>


                            <div class="form-group col-md-6">
                              <label>Acessórios</label>
                              <textarea class="form-control" rows="3" name="os_acessorio" value="" placeholder="Enter ..."><?php echo $dados[0]->os_acessorio; ?></textarea>
                            </div>

                            <div class="form-group col-md-6">
                              <label>Solução</label>
                              <textarea class="form-control" rows="3" name="os_solucao" value="" placeholder="Enter ..."><?php echo $dados[0]->os_solucao; ?></textarea>
                            </div>

                            <div class="form-group col-md-6">
                              <label>Laudo técnico</label>
                              <textarea class="form-control" rows="3" name="os_laudotecnico" value="" placeholder="Enter ..."><?php echo $dados[0]->os_laudotecnico; ?></textarea>
                            </div>

                            <div class="form-group col-md-6">
                              <label>Termos de garantia</label>
                              <textarea class="form-control" rows="3" name="os_termogarantia" value="" placeholder="Enter ..."><?php echo $dados[0]->os_termogarantia; ?></textarea>
                            </div>

                            <div class="form-group col-md-12">
                              <label>Observação Interna <h6><i>Esta observação é de uso interno, portanto não será impressa no pedido.</i></h6> </label>
                              <textarea class="form-control" rows="3" name="os_obsinterna" value="" placeholder="obs ..."><?php echo $dados[0]->os_obsinterna; ?></textarea>
                            </div>


                          </div>
                        </div>


                      </div>

                    </div>


                    <div class="span12" style="padding: 1%; margin-left: 0">
                      <div class="span6 offset3" style="text-align: center">
                        <?php if ($dados[0]->os_status != 'FATURADO') { ?>
                          <a data-toggle="modal" id="btn-faturar" data-target="#modalFaturar" role="button" class="btn btn-success"><i class="fa fa-file-o"> Faturar</i></a>
                        <?php  } ?>
                        <button class="btn btn-primary" id="btnContinuar"><i class="fa fa-edit">Alterar</i> </button>
                        <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>/visualizar/<?php echo $dados[0]->os_id; ?>" target="_blank" class="btn bg-navy"><i class="fa fa-search">Visualizar OS</i></a>
                        <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>" class="btn btn-default"><i class="fa fa-arrow-left">Voltar</i> </a>
                      </div>
                    </div>

                  </form>

                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_2">


                  <fieldset class="col-md-12">
                    <legend>Dados dos Produtos</legend>
                    <div class="col-md-12">
                      <form id="formProdutos" method="post">

                        <div class="form-group">

                          <input type="hidden" name="os_id" id="os_id" value="<?php echo $dados[0]->os_id; ?>" />
                          <input type="hidden" name="estoque" id="estoque" value="" />


                          <div class="col-md-2">
                            <label for="quantidade" class="control-label">Grupo </label>
                            <div class="input-group margin col-xs-12">
                              <select class="form-control" name="categoria_prod_id" id="grupo_prod_ordemservico">
                                <option value="">Selecione</option>
                                <?php foreach ($categoria_prod as $valor) { ?>
                                  <option value='<?php echo $valor->categoria_prod_id; ?>'><?php echo $valor->categoria_prod_descricao; ?> </option>
                                <?php } ?>
                              </select>
                            </div>
                          </div>

                          <div class="col-md-4">
                            <label for="quantidade" class="control-label">Produto </label>
                            <div class="input-group margin">
                              <div class="input-group-btn">
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-default">P</button>
                              </div>
                              <select class="select2 form-control" style="width: 100%;" name="produto_desc" id="produto_desc_ordemservico">
                                <option value="">Selecione um grupo</option>
                              </select>
                            </div>
                          </div>

                          <div class="col-md-2">
                            <label for="subTotal" class="control-label">Valor Unitário</label>
                            <div class="input-group margin" id="valord">
                              <input type="text" class="form-control money">
                            </div>
                          </div>


                          <div id="valord">
                            <input type="hidden">
                          </div>


                          <div class="col-md-2">
                            <label for="quantidade" class="control-label">Quantidade </label>
                            <div class="input-group margin">
                              <input type="text" class="form-control" name="quantidade" id="quantidade" placeholder="Quantidade" required="required">
                            </div>
                          </div>


                          <div class="col-md-2">
                            <div class="form-group">
                              <label for="btnAdicionarProdutoOs" class="control-label">&nbsp; </label>
                              <div class="input-group margin">
                                <span class="btn btn-success" id="btnAdicionarProdutoOs"><i class="icon-white icon-plus"></i> Adicionar</span>
                              </div>
                            </div>
                          </div>


                        </div>



                      </form>
                    </div>
                  </fieldset>



                  <div class="box-footer">

                    <!-- /.box-header -->
                    <div id="divProdutos" class="box-body no-padding">
                      <table class="table table-condensed">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Produto</th>
                            <th>Quantidade</th>
                            <th>Venda</th>
                            <th>SubTotal</th>
                            <th style="width: 40px">Ações</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          $total = 0;
                          $qtd = 0;
                          $seq = 1;
                          $totalGeral = 0;
                          foreach ($produtos as $p) {
                            $qtd = $qtd + $p->produtos_os_quantidade;
                            echo '<tr>';
                            echo '<td>' . $seq++ . '</td>';
                            echo '<td>' . $p->produto_descricao . '</td>';
                            echo '<td>' . $p->produtos_os_quantidade . '</td>';
                            echo '<td>' . $p->produto_preco_venda . '</td>';
                            echo '<td>' . number_format($p->produtos_os_subTotal, 2, ".", ""), '</td>';
                            $totalGeral += $p->produtos_os_subTotal;
                            echo '<td><a  href="' . base_url() . 'index.php/ordemservico/excluirProduto/' . $dados[0]->os_id . '/' . $p->idItens . '/' . $p->produtos_id . '/' . $p->produtos_os_quantidade . '/' . $p->produtos_os_subTotal . '" title="Excluir Produto" ><i class="fa fa-trash text-danger"></i></a></td>';
                            echo '</tr>';
                          } ?>


                        </tbody>
                        <tfoot>
                          <tr>
                            <th><span id="itens">ITENS:<?php echo ' ' . count($produtos); ?></span></th>
                            <th></th>
                            <th><span id="volume">VOLUME:<?php echo ' ' . $qtd; ?></span></th>
                            <th></th>
                            <th colspan="2"><span id="totalGeral">TOTAL GERAL: <?php echo ' ' . number_format($totalGeral, 2, ".", ""); ?><span id="volume"></th>
                          </tr>
                        </tfoot>
                      </table>
                      <input type="hidden" id='seq' value="<?php echo $seq ?>">
                      <input type="hidden" id='vol' value="<?php echo $qtd ?>">
                      <input type="hidden" name="total" id="total" value="<?php echo number_format($totalGeral, 2, ".", ""); ?>">
                    </div>

                  </div>


                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_3">


                  <fieldset class="col-md-12">
                    <legend>Dados dos Serviços</legend>
                    <div class="col-md-12">
                      <form id="formServicos" method="post">

                        <div class="form-group">

                          <input type="hidden" name="os_id" id="os_id" value="<?php echo $dados[0]->os_id; ?>" />

                          <div class="col-md-6">
                            <label for="quantidade" class="control-label">Serviços </label>
                            <div class="input-group margin">
                              <div class="input-group-btn">
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-default">S</button>
                              </div>
                              <select class="select2 form-control" style="width: 100%;" name="servico_id" id="servico_id">
                                <option value="">Selecione</option>
                                <?php foreach ($servicos as $valor) { ?>
                                  <option value='<?php echo $valor->servico_id; ?>'><?php echo $valor->servico_nome; ?> </option>
                                <?php } ?>
                              </select>
                            </div>
                          </div>

                          <div class="col-md-2">
                            <label for="subTotal" class="control-label">Valor Unitário</label>
                            <div class="input-group margin" id="valors">
                              <input type="text" class="form-control money">
                            </div>
                          </div>


                          <div class="col-md-2">
                            <label for="quantidadeServico" class="control-label">Quantidade </label>
                            <div class="input-group margin">
                              <input type="text" class="form-control" name="quantidadeServico" id="quantidadeServico" placeholder="Quantidade" required="required">
                            </div>
                          </div>


                          <div class="col-md-2">
                            <div class="form-group">
                              <label for="btnAdicionarServicoOs" class="control-label">&nbsp; </label>
                              <div class="input-group margin">
                                <span class="btn btn-success" id="btnAdicionarServicoOs"><i class="icon-white icon-plus"></i> Adicionar</span>
                              </div>
                            </div>
                          </div>


                        </div>



                      </form>
                    </div>
                  </fieldset>

                  <div class="box-footer">
                    <div id="divServicos" class="box-body no-padding">

                      <table class="table table-condensed">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Serviço</th>
                            <th>Quantidade</th>
                            <th>SubTotal</th>
                            <th style="width: 40px">Ações</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          $total = 0;
                          $qtd = 0;
                          $seqServico = 1;
                          $totalGeral = 0;
                          foreach ($servicosItens as $p) {
                            $qtd = $qtd + $p->servico_os_quantidade;
                            echo '<tr>';
                            echo '<td>' . $seqServico++ . '</td>';
                            echo '<td>' . $p->servico_nome . '</td>';
                            echo '<td>' . $p->servico_os_quantidade . '</td>';
                            echo '<td>' . number_format($p->servico_os_subTotal, 2, ".", ""), '</td>';
                            $totalGeral += $p->servico_os_subTotal;
                            echo '<td><a  href="' . base_url() . 'index.php/ordemservico/excluirServico/' . $dados[0]->os_id . '/' . $p->idItens . '/' . $p->servico_id . '/' . $p->servico_os_quantidade . '/' . $p->servico_os_subTotal . '" title="Excluir Serviço" ><i class="fa fa-trash text-danger"></i></a></td>';
                            echo '</tr>';
                          } ?>


                        </tbody>
                        <tfoot>
                          <tr>
                            <th><span id="itensServico">ITENS:<?php echo ' ' . count($servicosItens); ?></span></th>
                            <th></th>
                            <th><span id="volumeServico">VOLUME:<?php echo ' ' . $qtd; ?></span></th>
                            <th colspan="2"><span id="totalGeralServico">TOTAL GERAL: <?php echo ' ' . number_format($totalGeral, 2, ".", ""); ?><span id="volume"></th>
                          </tr>
                        </tfoot>
                      </table>
                      <input type="hidden" id='seqServico' value="<?php echo $seqServico ?>">
                      <input type="hidden" id='volServico' value="<?php echo $qtd ?>">
                      <input type="hidden" name="totalServico" id="totalServico" value="<?php echo number_format($totalGeral, 2, ".", ""); ?>">
                    </div>
                  </div>


                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_4">
                  <fieldset class="col-md-12">
                    <legend>Dados dos Serviços</legend>
                    <div class="col-md-12">
                      <form id="formAnexo" class="form-horizontal" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                          <input type="hidden" name="os_id" id="os_id" value="<?php echo $dados[0]->os_id; ?>" />
                          <div class="col-xs-12">
                            <label for="produto_imagem" class="control-label">Imagem </label>
                            <div class="input-group">
                              <div class="btn-group" style="display: flex;max-width: 50rem;">
                                <input id="arquivo" type="file" name="arquivo" accept="image/png, image/jpg, image/jpeg" class="form-control">
                                <!-- <label for="btEnviarAnexo" class="control-label">&nbsp </label> </br> -->
                                <span class="btn btn-success" id="btEnviarAnexo"><i class="icon-white icon-plus"></i> Adicionar</span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </form>
                  </fieldset>
                </div>

                <div class="tab-pane" id="tab_5">
                  <fieldset class="col-md-12">
                    <legend>Dados dos Serviços</legend>
                    <div class="col-md-12">
                      <form id="formAnexo" class="form-horizontal" method="post" enctype="multipart/form-data">

                        <style type="text/css">
                          .assinatura_pad {
                            border: 1px solid #ddd;
                            width: 99%;
                            height: 80px;
                            max-width: 400px;
                          }
                        </style>

                        <div class="row">
                          <div class="col-12 ">
                            <div class="text-center">
                              <h4>Assine ou altere a assinatura abaixo</h4>
                            </div>
                          </div>

                          <div class="col-12 ">
                            <div class="text-center">
                              <canvas id="assinatura-pad" class="assinatura_pad"></canvas>
                            </div>
                          </div>
                          <div class="col-12 text-center">
                            <div style="display: flex;gap: .5rem;flex-wrap: wrap;justify-content: center;padding: 1rem;">
                              <button type="button" class="btn btn-danger limpar">Limpar Assinatura</button>
                              <button type="button" class="btn btn-success salvar" disabled>Salvar Assinatura</button>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </fieldset>
                </div>

                <div class="box-body">
                  <div style="display: flex;flex-wrap: wrap;gap: 1rem;justify-content: center;padding-top: 1rem;" class="col-md-12" id='divAnexos'>

                    <?php foreach ($anexosItens as $p) { ?>
                      <!-- <div class="col-lg-3 col-xs-6">
                        <div class="form-group"> -->
                          <div class="media-left media-middle">
                            <img src="<?php echo base_url() . $p->anexo_url . $p->anexo_thumb ?>" width="150" height="150" alt="">
                          <!-- </div>
                        </div> -->
                      </div>

                    <?php } ?>

                  </div>
                </div>

              </div>
              <!-- /.tab-pane -->
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
      </div>
      <!-- /.col -->
      <!-- /.box-body -->
      <!--          <div class="box-footer">
            <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>" class="btn btn-default">Voltar</a>
            <button type="submit" class="btn btn-primary pull-right">Continuar</button>
          </div> -->


      <!-- /.box-footer -->
      <!-- </form> -->
    </div>
  </div>
  </div>
</section>


<div class="example-modal">
  <div class="modal" id="modalFaturar">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <form id="formFaturar" action="<?php echo base_url() ?>index.php/ordemservico/faturar" method="post">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Faturar Venda</h3>
          </div>
          <div class="modal-body">

            <div class="col-xs-12 alert alert-info" style="margin-left: 0"> Obrigatório o preenchimento dos campos com asterisco.</div>

            <div class="col-xs-12">
              <label for="cliente">Cliente*</label>
              <input type="text" class="form-control" name="clie_receita" id="clie_receita" value="<?php echo $dados[0]->cliente_nome; ?>">
              <input type="hidden" id="clie_receita_id" name="clie_receita_id" value="<?php echo $dados[0]->cliente_id; ?>">
            </div>

            <div class="col-xs-12">
              <label class="control-label" for="descricao">Descrição</label>
              <div class="form-group">
                <input class="form-control" id="descricao" type="text" name="descricao" value="Fatura de Ordem Serviço #<?php echo $dados[0]->os_id; ?>" />
                <input id="urlAtual" type="hidden" name="urlAtual" value="<?php echo current_url() ?>" />
                <input id="osID" type="hidden" name="osID" value="<?php echo $dados[0]->os_id ?>" />
              </div>
            </div>

            <div class="col-xs-3">
              <label for="valor">Valor*</label>
              <div class="form-group">
                <input type="hidden" name="tipo" value="receita" />
                <input type="hidden" id="qtdFormaPagamento" name="qtdFormaPagamento" value="receita" />
                <input class="form-control money" type="text" name="valor" value="<?php echo number_format($dados[0]->os_valorTotal, 2, ".", "") ?>" />
              </div>
            </div>

            <div class="col-xs-3">
              <label for="vencimento">Data Vencimento*</label>
              <div class="form-group">
                <input class="form-control datepicker" type="text" name="vencimentoOdem" id="vencimentoOdem" />
              </div>
            </div>
            <div class="col-xs-6">
              <div class="form-group">
                <label for="formaPgto">Forma Pgto</label>
                <select name="formaPgto" id="formaPgto" class="form-control">->
                  <option value="Dinheiro">Dinheiro</option>
                  <option value="Cheque">Cheque</option>
                  <option value="Cartão de Crédito">Cartão de Crédito</option>
                  <option value="Cartão de Débito">Cartão de Débito</option>
                  <option value="Crédito Loja">Crédito Loja</option>
                  <option value="Vale Alimentação">Vale Alimentação</option>
                  <option value="Vale Refeição">Vale Refeição</option>
                  <option value="Vale Presente">Vale Presente</option>
                  <option value="Vale Combustível">Vale Combustível</option>
                  <option value="Duplicata Mercantil">Duplicata Mercantil</option>
                  <option value="Boleto Bancário">Boleto Bancário</option>
                  <option value="Transferência Bancária">Transferência Bancária</option>
                  <option value="Sem pagamento" Selected>Sem pagamento</option>
                  <option value="Outros">Outros</option>
                </select>
              </div>
            </div>
            <div class="col-xs-12" style="margin-left: 0">
              <div class="col-xs-4" style="margin-left: 0">
                <label for="recebido">Recebido?</label><br>
                <input id="recebido" type="checkbox" name="recebido" value="1" />
              </div>
              <div id="divRecebimento" class="span8" style=" display: none">
                <div class="col-xs-6">
                  <label for="pagamento">Data Pagamento</label>
                  <input class="form-control datepicker" id="recebimentoOdem" type="text" name="recebimentoOdem" />
                </div>
              </div>
            </div>
          </div>

          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-success">Faturar</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script src="<?php echo base_url() ?>assets/dist/js/notification.js"></script>
<script src="<?php echo base_url() ?>assets/dist/js/assinatura.js"></script>

<script src="<?php echo base_url() ?>assets/dist/js/jquery.validate.js"></script>
<script type="text/javascript">
  $(document).ready(function() {

    $(function() {
      //iCheck for checkbox and radio inputs
      $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
      })

    });

    // Canvas da assinatura.
    const assinatura = document.querySelector("#assinatura-pad");
    const ctx = assinatura.getContext('2d');

    // Novo pad da assinatura
    const assinaturaPad = new SignaturePad(assinatura);

    // Evento para redimensionar o canvas da assinatura.
    function redimensionarAssinatura() {
      const ratio = Math.max(window.devicePixelRatio || 1, 1);
      assinatura.width = assinatura.offsetWidth * ratio;
      assinatura.height = assinatura.offsetHeight * ratio;
      assinatura.getContext("2d").scale(ratio, ratio);
      assinaturaPad.clear();
    }


    assinaturaPad.addEventListener('beginStroke', function(e) {
      $ImageAssinatura = assinaturaPad.toDataURL("image/png");

      if (assinaturaPad.isEmpty() && $ImageAssinatura == 'data:,') {
        redimensionarAssinatura();
      }
      if (assinaturaPad.isEmpty()) {
        liberarBotaoAssinatura()
      }
      ctx.fillRect(0, 0, 0, 0);
    }, false);


    $(".salvar").on('click', function(e) {
      if (assinaturaPad.isEmpty()) {
        alert('Por favor, assine antes de salvar.');
        return;
      }

      $ImageAssinatura = assinaturaPad.toDataURL("image/png");

      if ($ImageAssinatura == 'data:,') {
        alert('Por favor, assine antes de salvar.');
        redimensionarAssinatura();
        return;
      }

      $.ajax({
        url: "<?= base_url() ?>index.php/ordemservico/editarAssinatura/<?= $this->uri->segment(3); ?>",
        method: 'post',
        dataType: 'json',
        data: {
          assinatura: $ImageAssinatura,
        },
        success: function(resp) {
          const message = resp?.message ?? 'Assinatura adicionada com sucesso.';
          notification(message, 'success')
          console.log(resp);
        },
        error: function(err) {
          const message = err?.responseJSON?.message ?? 'Ocorreu um erro, por favor tente novamente.';
          notification(message, 'danger', 3000)
          console.log(err);
        }
      })
    });

    // Botão de limpar a assinatura
    $(".limpar").on('click', function(e) {
      $(".salvar").attr('disabled', 'disabled');
      assinaturaPad.clear();
      redimensionarAssinatura();
    })

    // Função para habilitar/desabilitar o botão de salvar assinatura caso esteja vazia.
    function liberarBotaoAssinatura() {
      if (assinaturaPad.isEmpty()) {
        $(".salvar").attr('disabled', 'disabled');
      } else {
        $(".salvar").removeAttr('disabled');
      }
    }

    // Evento para detectar quando assinar via computador para liberar o botão.
    assinatura.onmousemove = liberarBotaoAssinatura;

    // Evento para detectar quando assinar via mobile para liberar o botão.
    assinatura.ontouchmove = liberarBotaoAssinatura;

    // // Forçar primeiro redimensionamento
    redimensionarAssinatura();

  });


  $('#recebido').click(function(event) {
    var flag = $(this).is(':checked');
    if (flag == true) {
      $('#divRecebimento').show();
      $('#formaPgto').val('Dinheiro');
    } else {
      $('#divRecebimento').hide();
      $('#formaPgto').val('Sem pagamento');
    }
  });

  $('#ostab').click(function(event) {
    location.reload('#divProdutos');
  });


  $("#formFaturar").validate({
    rules: {
      descricao: {
        required: true
      },
      cliente: {
        required: true
      },
      valor: {
        required: true
      },
      vencimentoOdem: {
        required: true
      }

    },

    messages: {
      descricao: {
        required: 'Campo Requerido.'
      },
      cliente: {
        required: 'Campo Requerido.'
      },
      valor: {
        required: 'Campo Requerido.'
      },
      vencimentoOdem: {
        required: 'Campo Requerido.'
      }
    }

  });
</script>
