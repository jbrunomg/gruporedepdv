<section class="content">
  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="<?php echo current_url(); ?>" method="post">
        <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $dados[0]->categoria_prod_id; ?>" placeholder="id">
          <div class="box-body">

            <div class="form-group">
              <label for="categoria_prod_descricao" class="col-sm-2 control-label">Descrição</label>
              <div class="col-sm-5">              
                <input disabled type="text" class="form-control" name="categoria_prod_descricao" id="categoria_prod_descricao" value="<?php echo $dados[0]->categoria_prod_descricao; ?>" placeholder="Descrição">
              </div>
            </div>
            <div class="form-group">
              <label for="categoria_prod_margem" class="col-sm-2 control-label">Margem %</label>
              <div class="col-sm-5">              
                <input disabled type="text" class="form-control" name="categoria_prod_margem" id="categoria_prod_margem" value="<?php echo $dados[0]->categoria_prod_margem; ?>" placeholder="Margem">
              </div>
            </div> 
            <?php if ($dados[0]->categoria_prod_link == '1') { ?>
              <div class="form-group">
                <label for="categoria_prod_link" class="col-sm-2 control-label">Link</label>
                <div class="col-sm-5">              
                  <input disabled type="text" class="form-control" name="categoria_prod_link" id="categoria_prod_link" value="<?php echo $link; ?>">
                </div>
              </div> 
            <?php } ?>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>" class="btn btn-default">Voltar</a>
            
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
  </div>
</section>