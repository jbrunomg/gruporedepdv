<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <div class="col-md-2">
        <?php if(verificarPermissao('aCategoria')){ ?>
        <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/adicionar" type="button" class="btn btn-block btn-default btn-flat">Adicionar</a>
        <?php } ?>
      </div>          
    </div>
    <div class="box-body">
      <div class="container-fluid">
        <table id="example" class="table" width="100%">
          <thead>
            <tr>       
              <th>#</th>                                     
              <th>Descrição</th>
              <th>Margem</th>
              <th>Data Cadastro</th>
              <th>Ações</th>                            
            </tr>
          </thead>
          <tbody>

            <?php foreach ($dados as $d){ ?>
            <tr>                      
              <td> <?php echo $d->categoria_prod_id;?></td> 
              <td> <?php echo $d->categoria_prod_descricao;?></td> 
              <td> <?php echo $d->categoria_prod_margem.' %'?></td> 
              <td> <?php echo $d->categoria_prod_data_cadastro;?></td>              
              <td>                
              <?php if(verificarPermissao('vCategoria')){ ?>
                <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/visualizar/<?php echo $d->categoria_prod_id; ?>" data-toggle="tooltip" title="Visualizar"><i class="fa fa-search text-success"></i> </a>
                <?php } ?>
                <?php if(verificarPermissao('eCategoria')){ ?>
                <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/editar/<?php echo $d->categoria_prod_id; ?>" data-toggle="tooltip" title="Editar"><i class="fa fa-edit"></i> </a>
                <?php } ?>
                <?php if(verificarPermissao('dCategoria')){ ?>
                <a style="cursor:pointer;" title="Deletar" data-toggle="modal" data-target="#excluir" onclick="excluir('<?php echo base_url().$this->uri->segment(1)."/excluir/".$d->categoria_prod_id; ?>');" ><i class="fa fa-trash text-danger"></i></a>
                <?php } ?>
              </td>
            </tr>
            <?php } ?>

          </tbody>
          <tfoot>
            <tr>                     
              <th>#</th>                                     
              <th>Descrição</th>
              <th>Margem</th>
              <th>Data Cadastro</th>
              <th>Ações</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</section>


<a href="javascript:void(0);" data-toggle="tooltip" data-target="#modal-visualizarImei"  title="Visualizar"><i class="fa fa-edit text-success"></i> </a>


<!-- Modal -->
<div class="modal fade" id="modal-visualizarImei" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel">Visualizar Produto</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Conteúdo do Produto será carregado aqui -->
                <div id="produtoInfo">
                    <!-- Informações do produto serão carregadas aqui -->
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-primary">Salvar alterações</button>
            </div>
        </div>
    </div>
</div>