<section class="content">
  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="<?php echo current_url(); ?>" method="post">
          <div class="box-body">

            <div class="form-group">
              <label for="loja" class="col-sm-2 control-label">Lojas </label>
              <div class="col-sm-5">              
                <select id="lojas" class="form-control select2" multiple="multiple" data-placeholder="Selecione as lojas" name="lojas[]" disabled="disabled">
                  <?php 
                  foreach ($lojas as $s) {
                    echo "<option value='".strtoupper($s['lojas'])."' selected>".strtoupper($s['lojas'])."</option>";
                  }
                  ?>                        
                </select>
              </div>
            </div> 

            <div class="form-group">
              <label for="categoria_prod_descricao" class="col-sm-2 control-label">Descrição*</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" name="categoria_prod_descricao" id="categoria_prod_descricao" value="<?php echo set_value('categoria_prod_descricao'); ?>" placeholder="categoria descricao">
              </div>
            </div>      

            <div class="form-group">
              <label for="categoria_prod_margem" class="col-sm-2 control-label">Margem % *</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" name="categoria_prod_margem" id="categoria_prod_margem" value="<?php echo set_value('categoria_prod_margem'); ?>" placeholder="Margem">
              </div>
            </div>     
            
            <div class="form-group row">
                <label for="categoria_prod_link" class="col-sm-2 control-label">Link Externo</label>  
                <div class="col-lg-5">
                    <div class="form-check">
                      <label class="form-check-label">
                        <input type="checkbox" class="form-check-input" name="categoria_prod_link" id="categoria_prod_link">
                      </label>
                    </div>
                </div>
            </div>

          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>" class="btn btn-default">Voltar</a>
            <button type="submit" class="btn btn-primary pull-right"><?php echo ucfirst($this->uri->segment(2)); ?></button>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
  </div>
</section>