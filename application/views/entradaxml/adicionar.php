<style>
  .input-file-placeholder {
    font-weight: 500;
    color: hsl(0, 0%, 33%);
  }

  .input-file-placeholder-init-color {
    color: hsl(0, 0%, 60%);
  }

  .ml-1 {
    margin-left: 4px;
  }
</style>

<section class="content">
  <div class="row">
    <div class="col-md-12">
      <!-- Horizontal Form -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal"
          action="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>/adicionarExe" method="post"
          enctype="multipart/form-data">
          <div class="box-body">

            <div class="form-group">
              <label for="inputFile" class="col-sm-2 control-label">Arquivo XML</label>
              <div class="col-sm-5">
                <label for="inputFile" class="input-group ">
                  <span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-upload"></i></span>
                  <div id="inputFilePlaceholder"
                    class="form-control input-file-placeholder input-file-placeholder-init-color">Enviar Xml </div>
                  <input type="file" name="xmlFileEnvio" id="inputFile" class="form-control" style="display: none;">
                </label>
                <div class="text-danger ml-1" id="fileErro"></div>
              </div>
            </div>

            <div class="form-group">
              <label for="dataEnvio" class="col-sm-2 control-label">Data da Entrada <!-- Data da Entrada/Saída  --></label>
              <div class="col-sm-5">
                <input type="date" class="form-control" name="dataEnvio"
                  value="<?php echo set_value('dataEnvio', date('Y-m-d')); ?>" placeholder="" id="dataEnvio">
                <div class="text-danger ml-1" id="dataEnvioErro"></div>
              </div>
            </div>


            <!-- <div class="form-group">
              <label for="movimentacao_produto_tipo" class="col-sm-2 control-label">Tipo </label>
              <div class="col-sm-5">
                <select class="form-control" name="movimentacao_produto_tipo" id="movimentacao_produto_tipo">
                  <option value="Entrada">Entrada</option>
                  <option value="Saída">Saída</option>
                </select>
              </div>
            </div> -->

            <!-- <div style="display: none" id="saida">
              <div class="form-group">
                <label class="col-sm-2 control-label">Tipo Saida</label>
                <div class="col-sm-5">
                  <select class="form-control" name="tipo_saida" id="tipo_saida">
                    <option value="fornecedor">Fornecedor</option>
                    <option value="filial">Filial</option>
                  </select>
                </div>
              </div> -->

            <!-- <div class="form-group" id="div_filial" style="display: none">
                <label class="col-sm-2 control-label">Filial</label>
                <div class="col-sm-5">
                  <select class="form-control" name="filial" id="filial">
                    <option value="">Selecione</option>
                    <?php //foreach ($filias as $filial) {
                      // $arr = explode('_', $filial->Nome);
                      // $i = count($arr) - 1;
                      ?>
                      <option value"<?php //echo $filial->Nome ?>"><?php //echo $arr[$i]; ?></option>
                    <?php //} ?>
                  </select>
                </div>
              </div>
            </div> -->

            <!-- <div class="form-group" id="fornecedor">
              <label for="movimentacao_fornecedor_id" class="col-sm-2 control-label">Forncedor</label>
              <div class="col-sm-5">

                <select class="form-control" name="movimentacao_fornecedor_id" id="movimentacao_fornecedor_id">
                  <option value="">Selecione</option>
                  <?php //foreach ($fornecedor as $valor) { ?>
                    <option value='<?php //echo $valor->fornecedor_id; ?>'><?php //echo $valor->fornecedor_nome; ?> </option>
                  <?php //} ?>
                </select>
              </div>
            </div> -->


            <!-- <div class="form-group" id="div_tipo_moeda">
              <label for="tipo_moeda" class="col-sm-2 control-label">Tipo moeda </label>
              <div class="col-sm-5">
                <select class="form-control" name="tipo_moeda" id="tipo_moeda">
                  <option value="">Selecione</option>
                  <option value="real">Real</option>
                  <option value="dolar">Dolar</option>
                </select>
              </div>
            </div>

            <div class="form-group" id="div_cotacao_moeda" style="display: none;">
              <label for="cotacao_moeda" class="col-sm-2 control-label">Cotação Dolar</label>
              <div class="col-sm-5">
                <input type="text" class="form-control dinheiro" name="cotacao_moeda" id="cotacao_moeda"
                  value="<?php echo set_value('cotacao_moeda'); ?>" placeholder="Cotação">
              </div>
            </div> -->


          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>" class="btn btn-default">Voltar</a>
            <button type="submit" class="btn btn-primary pull-right">
              <?php echo ucfirst($this->uri->segment(2)); ?>
            </button>
          </div>

          <!--  -->
          <?php
          if (isset($produtos) && $produtos) {
            ?>
            <div class="span12" id="divProdutos" style="margin-left: 0">
              <table class="table table-condensed no-padding" id="tblProdutos">
                <thead>
                  <tr>
                    <th class="text-center">Situação</th>
                    <th class="text-center">Produto</th>
                    <th class="text-center">Cod.</th>
                    <th class="text-center">Quantidade</th>
                    <th class="text-center">Preço Custo</th>
                  </tr>
                </thead>
                <tbody>
                  <?php

                  $total = 0;
                  $qtd = 0;

                  foreach ($produtos as $p) {

                    $total = $total + ($p['valorUnitario'] * $p['quantidade']);
                    $qtd = $qtd + $p['quantidade'];

                    $situacao = $p['existe'] == true ? '<i class="icon-thumbs-up label label-success" title="'. $p['existeTitulo'] .'">'. $p['existeNome'] .'</i>' : '<i class="icon-thumbs-down label label-warning" title="'. $p['existeTitulo'] .'">'. $p['existeNome'] .'</i>';

                    echo '<tr>';
                    echo '<td class="text-center">' . $situacao . '</td>';
                    echo '<td class="text-center">' . $p['nomeProduto'] . '</td>';
                    echo '<td class="text-center">' . $p['produtoCodigo'] . '</td>';
                    echo '<td class="text-center">' . $p['quantidade'] . '</td>';
                    echo '<td class="text-center">' . number_format($p['valorUnitario'], 2, ',', '.') . '</td>';
                    echo '</tr>';
                  } ?>

                  <tr>
                    <th>Situação</th>
                    <th>ITENS:
                      <?php echo count($produtos); ?>
                    </th>
                    <th>Cód Barra</th>
                    <th>T.QTD:
                      <?php echo $qtd; ?>
                    </th>
                    <th>Total:
                      <?php echo number_format($total, 2, ',', '.'); ?>
                    </th>
                  </tr>
                </tbody>
              </table>
            </div>
          <?php } ?>
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
  </div>
</section>


<script type="text/javascript">

  $(function () {


    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    })

  })

  $('#inputFile').change(function (val) {
    $("#inputFilePlaceholder").text(val.target.files[0].name)
    $("#inputFilePlaceholder").removeClass('input-file-placeholder-init-color')
  })

  $(document).ready(function () {
    $('#myForm').submit(function () {

      // const erroArray = []

      // // FileXml
      // const inputFile = $('#inputFile');
      // const fileErro = $('#fileErro');

      // if (inputFile[0].files.length === 0) {
      //   fileErro.text('Por favor, selecione um arquivo.');
      //   erroArray.push(false)
      // } else {
      //   fileErro.text('');
      // }

      // // Fornecedor
      // const fornecedorId = $('#fornecedor_id');
      // const fornecedorErro = $('#fornecedorErro');

      // if (fornecedorId.val()) {
      //   fornecedorErro.text('Por favor, selecione um fornecedor valido');
      //   erroArray.push(false)
      // } else {
      //   fornecedorErro.text('');
      // }

      // data Envio
      const dataEnvio = $('#dataEnvio');
      const dataEnvioErro = $('#dataEnvioErro');

      if (dataEnvio.val()) {
        dataEnvioErro.text('Por favor, selecione uma data valida');
        erroArray.push(false)
      } else {
        dataEnvioErro.text('');
      }

      console.log(!erroArray.length);

      return !erroArray.length
    });
  });


</script>
