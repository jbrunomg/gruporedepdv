
<input id="movId" type="hidden" value="<?php echo $dados[0]->movimentacao_produto_id ?>">
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <!-- Horizontal Form -->
      <div class="box box-primary collapsed-box">
        <div class="box-header with-border">
          <h3>#<?php echo $dados[0]->movimentacao_produto_tipo ?>:<?php echo $dados[0]->movimentacao_produto_id ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
            </button>
          </div>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="" method="post">
          <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $dados[0]->movimentacao_produto_id; ?>" placeholder="id">
          <div class="box-body">
            <div class="form-group">
              <label for="movimentacao_produto_data_cadastro" class="col-sm-2 control-label">Data da Entrada/Saída </label>
              <div class="col-sm-2">
                <input type="text" class="form-control" name="movimentacao_produto_data_cadastro" id="data" value="<?php echo date('d/m/Y', strtotime($dados[0]->movimentacao_produto_data_cadastro)); ?>" disabled>
              </div>

              <?php if ($dados[0]->movimentacao_produto_saida_tipo == 'fornecedor') { ?>
                <label for="fornecedor_nome" class="col-sm-2 control-label">Fonecedor</label>
                <div class="col-sm-2">
                  <input type="text" class="form-control" name="fornecedor_nome" id="fornecedor_nome" value="<?php echo $dados[0]->fornecedor_nome ?>" disabled>
                </div>
              <?php } else { ?>
                <label for="movimentacao_produto_saida_filial" class="col-sm-2 control-label">Filial</label>
                <div class="col-sm-2">
                  <input type="text" class="form-control" name="movimentacao_produto_saida_filial" id="movimentacao_produto_saida_filial" value="<?php echo $dados[0]->movimentacao_produto_saida_filial ?>" disabled>
                </div>
              <?php } ?>
            </div>

            <div class="form-group">
              <label for="movimentacao_produto_documento" class="col-sm-2 control-label">Documento </label>
              <div class="col-sm-2">
                <input type="text" class="form-control" name="movimentacao_produto_documento" id="movimentacao_produto_documento" value="<?php echo $dados[0]->movimentacao_produto_documento ?>" disabled>
              </div>
              <label for="movimentacao_tipo_moeda" class="col-sm-2 control-label">Tipo Moeda </label>
              <div class="col-sm-2">
                <input type="text" class="form-control" name="movimentacao_tipo_moeda" id="movimentacao_tipo_moeda" value="<?php echo $dados[0]->movimentacao_tipo_moeda ?>" disabled>
              </div>
            </div>


          </div>



        </form>
      </div>



      <div class="box-footer">

        <div class="box-header with-border">
          <h3>Dados dos Produtos</h3>
          <div class="box-tools pull-right">
            <a title="Imprimir" target="_blank" class="btn btn-mini btn-default" href="<?php echo base_url() . 'entradaxml/imprimirEntradaSaida/' . $this->uri->segment(3); ?>"><i class="glyphicon glyphicon-print icon-white"></i> Imprimir</a>
          </div>
        </div>

        <!-- /.box-header -->
        <div id="divProdutos" class="box-body">
          <form id="formProdutos" action="<?php echo base_url() . 'entradaxml/entradaProdutos/' . $this->uri->segment(3); ?>" method="post">
            <table class="table table-condensed no-padding" id="tableProdutos" style="width: 100%;">
              <thead>
                <tr>
                  <th class="text-center">Cód.</th>
                  <th class="text-center">Imei</th>
                  <th class="text-center" width="20%">Produto</th>
                  <th class="text-center">Quantidade</th>
                  <th class="text-center">Custo</th>
                  <th class="text-center">Total</th>
                  <th class="text-center">Conf.</th>

                </tr>
              </thead>
              <tbody>
                <?php
                $total = 0;
                $qtd = 0;
                $seq = 1;
                $totalDocumento = 0;

                foreach ($produtos as $p) {

                  if ($dados[0]->movimentacao_tipo_moeda == 'dolar') {

                    $total_item = number_format(($p->quantidadeProduto * $p->dolarProduto * $dados[0]->movimentacao_cotacao_moeda), 2, '.', '');
                  } else {

                    $total_item = number_format(($p->quantidadeProduto * $p->custoProduto), 2, '.', '');
                  }

                  $totalDocumento = $totalDocumento + $total_item;

                  $qtd = $qtd + $p->quantidadeProduto;
                ?>
                  <tr>
                    <td class="text-center"> <?php echo $p->codigoProduto; ?> </td>
                    <td class="text-center" width="12%"><?php echo $p->Imei; ?></td>
                    <td class="text-center"> <?php echo $p->descricaoProduto; ?> </td>
                    <td class="text-center"> <?php echo $p->quantidadeProduto; ?> </td>
                    <td class="text-center"> <?php echo $p->custoProduto; ?> </td>
                    <td class="text-center"> <?php echo $total_item; ?> </td>
                    <td class="text-center">
                      <?php if (verificarPermissao('eAlmoxarifado') && 1 != 0) { ?>
                        <a onclick="modalEdit('<?= $p->codigoProduto; ?>', '<?= $p->Imei; ?>', '<?= $p->descricaoProduto; ?>', <?= $p->idMovimentacaoProduto ?>, <?= $p->idProduto ?>, <?= $p->quantidadeProduto ?>)" data-toggle="modal" data-target="#exampleModalCenter" title="Editar"><i class="fa fa-edit"></i> </a>
                      <?php } ?>
                    </td>
                  </tr>

                <?php } ?>
              </tbody>

              <tfoot>
                <tr>
                  <th>ITENS: <?php echo ' ' . count($produtos); ?></th>
                  <th></th>
                  <th>VOLUME: <?php echo ' ' . $qtd; ?></th>
                  <th></th>
                  <th>VALOR TOTAL: <?php echo number_format($totalDocumento, 2, '.', ''); ?></th>
                  <th></th>
                </tr>
              </tfoot>
            </table>
          </form>
          <div class="span12" style="padding: 1%; margin-left: 0">
            <a href="<?php echo base_url() ?>index.php/entradaxml" class="btn btn-default"><i class="icon-arrow-left"></i> Voltar</a>
          </div>
        </div>
      </div>
    </div>

    <div id="containerModalEditItem"></div>

  </div>
  </div>
</section>

<script src="<?php echo base_url() ?>assets/dist/js/notification.js"></script>
<script>
  function modalEdit(codigoProduto, imei, descricaoProduto, itemMovimentacaoProdutoId, produtoId, quantidadeProduto) {
    $('#containerModalEditItem').html(`
      <div class="modal fade bd-example-modal-lg align-content-center " id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content" style="border-radius: 4px;">
            <div class="modal-header">
              <div style="display: grid; grid-template-rows: min-content; grid-template-columns: 1fr min-content; align-items: center; justify-content: center;">
                <h5 class="modal-title" id="exampleModalLongTitle">Mudar Dados do Produto</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true" class="h1">&times;</span>
                </button>
              </div>
            </div>
            <form id="formSubmitEdit" class="modal-body">
              <input type="hidden" name="imeiInicial" value="${imei}">
              <input type="hidden" name="itemMovimentacaoProdutoId" value="${itemMovimentacaoProdutoId}">
              <input type="hidden" name="produtoIdInicial" value="${produtoId}">
              <input type="hidden" name="produtoIdNovo" id="produtoIdNovo" value="${produtoId}">
              <input type="hidden" name="quantidadeItemMovimentacaoProduto" value="${quantidadeProduto}">
              <div class="form-group">
                <label for="inputModalDescricaoProduto">Produto</label>
                <input type="text" name="descricaoProduto" class="form-control" id="inputModalDescricaoProduto" value="${descricaoProduto}">
              </div>
              <div class="row">
                <div class="form-group col-sm-6">
                  <label for="inputModalCodigoProduto">Codigo do Produto</label>
                  <input type="text" name="codigoProduto" class="form-control" id="inputModalCodigoProduto" value="${codigoProduto}">
                </div>
                <div class="form-group col-sm-6">
                  <label for="inputModalImeiIdNovo">Imei</label>
                  <input type="text" name="imeiNovo" class="form-control" id="inputModalImeiIdNovo" value="${imei}">
                </div>
                </div>

                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
          </div>
        </div>
      </div>
     `)

    $("#inputModalDescricaoProduto, #inputModalCodigoProduto").autocomplete({
      source: function(request, response) {
        $.ajax({
          url: '<?php echo base_url(); ?>entradaxml/autocompleteProduto',
          dataType: "json",
          data: {
            lorem: $(this.element).val()
          },
          success: function(data) {
            response(data)
          }
        });
      },
      response: function() {
        $(".ui-autocomplete").css({
          "z-index": 2000,
          "max-height": "200px",
          "overflow-x": "hidden",
          "overflow-y": "auto"
        });
      },
      minLength: 2,
      select: function(event, ui) {
        setTimeout(() => {
          $("#produtoIdNovo").val(ui.item.id)
          $("#inputModalCodigoProduto").val(ui.item.cod)
          $("#inputModalDescricaoProduto").val(ui.item.descricao)
        }, 1)
      }
    });

    const formSubmitEdit = document.querySelector('#formSubmitEdit');

    formSubmitEdit.addEventListener('submit', submitForm)
  }


  function submitForm(event) {
    event.preventDefault();

    const dadosEntries = new FormData(event.target).entries()
    const dadosForm = Object.fromEntries(dadosEntries)
    const tempoDaleyMenssagens = 4000;

    $.ajax({
      url: '<?php echo base_url(); ?>entradaxml/atualizarMovimentacaoDeProdutosRotaVisializar',
      dataType: 'json',
      method: "POST",
      data: dadosForm,
      success: function(data) {
        notification(data?.message ?? 'Edição realizada com sucesso', 'success', tempoDaleyMenssagens)

        setTimeout(function() {
          location.reload();
        }, 2500);
      },
      error: function(jqXHR) {
        let messageValue = ''
        const message = jqXHR.responseJSON?.message
        if (typeof message == 'object') {
          const Entries = Object.entries(message)
          messageValue = Entries?.[0]?.[1]
        } else {
          messageValue = message
        }

        notification(messageValue || 'Ocorreu um erro, por favor tente novamente.', 'danger', tempoDaleyMenssagens)
      },
    })
  }

</script>
