<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary ">
        <div class="box-header with-border">
          <div class="col-md-2">
            <?php if(verificarPermissao('aAlmoxarifado')){ ?>
            <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/adicionar" type="button" class="btn btn-block btn-default btn-flat">Adicionar</a>
            <?php } ?>
          </div>
        </div>
        <div class="box-body">
          <div class="container-fluid">
            <table id="examplee" class="table" width="100%">
              <thead>
                <tr>
                  <th>status</th>
                  <th>Código</th>
                  <th>Fornecedor</th>
                  <th>Tipo</th>
                  <th>Data</th>
                  <th>Descrição</th>
                  <th>Total</th>
                  <th>Ações</th>
                </tr>
              </thead>
              <tbody>

                <?php foreach ($dados as $d){ ?>
                <tr>

                  <td> <?php echo $d->movimentacao_produto_status == 0 ? '<i class="icon-thumbs-up label label-success"> Finalidado </i>' : '<i class="icon-thumbs-down label label-warning"> Em Analize </i>';?></td>
                  <td> <?php echo $d->movimentacao_produto_id;?></td>
                  <td> <?php echo $d->fornecedor_nome == NULL ? ' -------' : $d->fornecedor_nome;?></td>
                  <td> <?php echo $d->movimentacao_produto_tipo;?></td>
                  <td> <?php echo $d->movimentacao_produto_data_cadastro;?></td>
                  <td> <?php echo $d->movimentacao_produto_documento;?></td>
                  <td> <?php echo $d->total;?></td>

                  <td>
                    <?php if(verificarPermissao('vAlmoxarifado')){ ?>
                    <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/visualizar/<?php echo $d->movimentacao_produto_id; ?>" data-toggle="tooltip" title="Visualizar"><i class="fa fa-search  text-success"></i> </a>
                    <?php } ?>
                    <?php if(verificarPermissao('eAlmoxarifado') && $d->movimentacao_produto_status != 0){ ?>
                    <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/editar/<?php echo $d->movimentacao_produto_id; ?>" data-toggle="tooltip" title="Editar"><i class="fa fa-edit"></i> </a>
                    <?php } ?>
                    <?php if(verificarPermissao('dAlmoxarifado') && $d->movimentacao_produto_status != 0){ ?>
                    <a style="cursor:pointer;" title="Deletar" data-toggle="modal" data-target="#excluir" onclick="excluir('<?php echo base_url() . 'entradaxml/excluir/' . $d->movimentacao_produto_id; ?>')" ><i class="fa fa-trash  text-danger"></i></a>
                    <?php } ?>

                  <?php if( $d->movimentacao_produto_status != 0 && $d->movimentacao_saida_origem_id != '' ){ ?>
                    <a style="cursor:pointer;" title="Rejeitar Entrada" data-toggle="modal" data-target="#rejeitar" onclick="rejeitar('<?php echo base_url(). $this->uri->segment(1) . "/rejeitar/" . $d->movimentacao_produto_id; ?>');" ><i class="fa fa-retweet  text-yellow"></i></a>
                    <?php } ?>
                  </td>
                </tr>
                <?php } ?>

              </tbody>
              <tfoot>
                <tr>
                  <th>Código</th>
                  <th>Tipo</th>
                  <th>Data</th>
                  <th>Descrição</th>
                  <th>Ações</th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">
  // Basic example
$(document).ready(function () {
$('#examplee').DataTable({
"ordering": false // false to disable sorting (or any other option)
});

});


</script>
