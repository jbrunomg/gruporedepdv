<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo strtoupper(SUBDOMINIO) ?>  | Controler</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css">
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition  skin-<?php echo CLIENTE ?>  layout-top-nav">
<div class="wrapper">

<header class="main-header">
    <nav class="navbar navbar-static-top">
        <div class="container text-center">
            <div class="navbar-header w-100">
                <a href="#" class="navbar-brand d-inline-block mx-auto">
                    <b><?php echo $dados[0]->categoria_prod_descricao; ?></b>
                </a>
            </div>
        </div>
        <!-- /.container-fluid -->
    </nav>
</header>

  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content">
        <div class="box">
          <div class="box-header with-border ">
            <div class="col-md-2">
            </div>          
          </div>
          <div class="box-body">
            <div class="container-fluid">
              <table id="example" class="table" width="100%">
                <thead>
                  <tr>                                            
                    <th>#</th>
                    <th>Produto</th>
                    <th>Quantidade</th>
                    <th>Preço</th>
                    <th style="text-align: center;">Ações</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($dados as $dado) { //v($dado);?>
                    <tr>                      
                      <td><?php echo $dado->produto_id; ?></td> 
                      <td><?php echo $dado->produto_descricao; ?></td> 
                      <td><?php echo $dado->produto_estoque; ?></td> 
                      <td><?php echo $dado->produto_preco_venda; ?></td> 
                      <td style="text-align: center;">
                        <a href="javascript:void(0);" data-toggle="tooltip" data-target="#modal-visualizarImei" onclick="visualizarImei('<?php echo $dado->produto_id;?>');" title="Visualizar"><i class="fa fa-tablet text-success"></i> </a>
                        <a href="javascript:void(0);" data-toggle="tooltip" data-target="#modal-simulador" onclick="simulador('<?php echo $dado->produto_preco_venda;?>', '<?php echo $dado->produto_descricao;?>');" title="Simulador"><i class="fa fa-credit-card text-primary"></i></a>
                      </td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.18
    </div>
    <strong>Copyright &copy; 2019- <?php echo date('Y');?> <a href="www.wdmtecnologia.com.br">WDM-Tecnologia</a>.</strong> All rights
    reserved.
  </footer>
</div>
<!-- ./wrapper -->


<!-- Modal -->
<div class="modal fade" id="modal-visualizarImei" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">IMEI's Disponíveis</h4>
            </div>
        <div class="modal-body">
          <table id="visualizarImei" class="table" width="100%">
            <thead>
              <tr>                                            
                <th>#</th>
                <th>Imei</th>
                <th>Bateria</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal-simulador" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Simular Compra</h4>
            </div>
            <div class="box-body">
            <form id="form-simulador" method="post">
                <div class="panel-body">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="descprod">Produto:</label>
                            <input type="text" name="descprod" autocomplete="off" id="descprod" class="form-control form-control-sm money" 
                                   placeholder="0,00" maxlength="12" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label for="vlprod">Digitar Valor da Compra:*</label>
                            <input type="text" name="vlprod" autocomplete="off" id="vlprod" class="form-control form-control-sm money" 
                                   placeholder="0,00" maxlength="12" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="vlentr">Digitar Valor da Entrada em Espécie:</label>
                            <input type="text" name="vlentr" autocomplete="off" id="vlentr" class="form-control form-control-sm money" 
                                   placeholder="0,00" maxlength="12">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="qtparc">Limite Máximo de Parcelas:</label>
                            <input type="number" id="qtparc" name="qtparc" value="18" class="form-control form-control-sm" readonly />
                        </div>
                    </div>
                    <table id="table-simulador" class="table" width="100%"></table>
                </div>

                <div class="form-actions">
                    <div class="span12 text-right">
                        <button type="submit" class="btn btn-success"><i class="icon-credit-card icon-white"></i> Calcular</button>
                        <button type="button" class="btn btn-primary" id="btn-whatsapp"><i class="icon-whatsapp"></i> Copiar para WhatsApp</button>
                    </div>
                </div>

            </form>
            <div id="resultado-simulador" style="margin-top: 15px;"></div>
        </div>
      </div>
    </div>
</div>


<!-- jQuery 2.2.0 -->
<script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- DataTables -->
<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- Maskmoney -->
<script src="<?php echo base_url(); ?>assets/dist/js/maskmoney.js"></script>

<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>

<script>
    $(document).ready(function() {
      $(".money").maskMoney();
        $('#example').DataTable({
            "pageLength": 20, // Define o número mínimo de registros por página
            "lengthChange": false,    // Remove o seletor "Show entries"
            "language": {
                "sProcessing":     "Processando...",
                "sLengthMenu":     "Mostrar _MENU_ registros por página",
                "sZeroRecords":    "Nenhum registro encontrado",
                "sInfo":           "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered":   "(filtrado de _MAX_ registros no total)",
                "sSearch":         "Pesquisar:",
                "oPaginate": {
                    "sFirst":    "Primeiro",
                    "sPrevious": "Anterior",
                    "sNext":     "Próximo",
                    "sLast":     "Último"
                }
            }
        }); 

        $('#visualizarImei').DataTable({
            "pageLength": 20, // Define o número mínimo de registros por página
            "lengthChange": false,    // Remove o seletor "Show entries"
            "language": {
                "sProcessing":     "Processando...",
                "sLengthMenu":     "Mostrar _MENU_ registros por página",
                "sZeroRecords":    "Nenhum registro encontrado",
                "sInfo":           "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered":   "(filtrado de _MAX_ registros)",
                "sSearch":         "Pesquisar:",
                "oPaginate": {
                    "sFirst":    "Primeiro",
                    "sPrevious": "Anterior",
                    "sNext":     "Próximo",
                    "sLast":     "Último"
                }
            }
        });

        $('#form-simulador').on('submit', function (e) {
            e.preventDefault(); // Evita o envio padrão do formulário

            const formData = $(this).serialize();

            $.ajax({
                url: '<?php echo base_url(); ?>categoriaprodutopublico/simular',
                method: 'POST',
                data: formData,
                dataType: 'json',
                success: function (response) {
                    if (response.simulado) {
                        $('#vlprod').val(response.simulado.valorCompra);
                        $('#vlentr').val(response.simulado.valorEntrada);
                        $('#qtparc').val(response.simulado.qtdParcela);
                    }

                    let tabela = '';
                    if (response.resultado && response.resultado.length > 0) {
                        response.resultado.forEach((v) => {
                            tabela += `
                                <tr>
                                    <td colspan="1" class="text-center">${v.tipo_parcela}</td>
                                    <td colspan="4" class="text-center">${v.valor_parcela_formatado}</td>
                                    <td colspan="4" class="text-center">${v.valor_total_formatado}</td>
                                </tr>`;
                        });
                    } else {
                        tabela = '<tr><td colspan="9" class="text-center">Nenhuma simulação cadastrada</td></tr>';
                    }

                    // Atualizar tabela no modal
                    $('#table-simulador').html(tabela);
                },
                error: function () {
                    $('#resultado-simulador').html('<p>Erro ao realizar a simulação.</p>');
                }
            });
        });
        
        $('#btn-whatsapp').on('click', function () {
            const produto = $('#descprod').val() || "Não informado";
            const valorCompra = $('#vlprod').val() || "0,00";
            const valorEntrada = $('#vlentr').val() || "0,00";
            const parcelas = $('#qtparc').val() || "18";

            let mensagem = `*Simulação de Compra:*\n\n`;
            mensagem += `*Produto:* ${produto}\n`;
            mensagem += `*Valor da Compra:* R$ ${valorCompra}\n`;
            mensagem += `*Entrada:* R$ ${valorEntrada}\n`;
            mensagem += `*Parcelas:* ${parcelas}\n\n`;
            mensagem += `*Tabela de Parcelamento:*\n`;

            $('#table-simulador tr').each(function () {
                const cols = $(this).find('td');
                if (cols.length > 0) {
                    const tipoParcela = cols.eq(0).text().trim();
                    const valorParcela = cols.eq(1).text().trim();
                    const totalCompra = cols.eq(2).text().trim();
                    mensagem += `- ${tipoParcela}: ${valorParcela} (Total: ${totalCompra})\n`;
                }
            });

            // Copiar a mensagem formatada para a área de transferência
            navigator.clipboard.writeText(mensagem).then(() => {
                // alert('Informações copiadas para a área de transferência!');
            }).catch(() => {
                alert('Erro ao copiar informações. Por favor, tente novamente.');
            });
        });

    });

    function visualizarImei(id) {
        $('#modal-visualizarImei').modal('show'); 

        $.ajax({
            url: '<?php echo base_url() ?>categoriaprodutopublico/visualizarImei',
            type: 'POST',
            dataType: 'json',
            data: { id: id },
        }).done(function(response) {
            // Obtém a instância do DataTable
            var table = $('#visualizarImei').DataTable();

            table.clear();

            if (response && response.length > 0) {
                $.each(response, function(index, item) {
                    var bateria = item.imei_bateria ? item.imei_bateria + '%' : 'Sem Informação';  // Se bateria for null ou 0, deixa vazio
                    table.row.add([
                        item.imei_id,         // Primeiro coluna (imei_id)
                        item.imei_valor,      // Segunda coluna (imei_valor)
                        bateria              // Terceira coluna (imei_bateria)
                    ]);
                });
            } else {
                table.row.add([
                    'Nenhum dado encontrado.', 
                    '', 
                    ''
                ]);
            }

            table.draw();
        }).fail(function() {
            alert('Erro ao carregar os dados.');
        });
    }

    function simulador(preco, descricao) {
      $('#modal-simulador').modal('show'); 
      $('#vlprod').val(preco); 
      $('#descprod').val(descricao); 
    }


</script>
</body>
</html>
