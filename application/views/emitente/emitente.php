<section class="content">
  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="<?php echo current_url(); ?>" method="post" enctype="multipart/form-data" >
          <input type="hidden" class="form-control" name="id" id="id"  value="<?php echo $dados[0]->emitente_id; ?>">
          <div class="box-body">

            <div class="form-group">
              <label for="emitente_cnpj" class="col-sm-2 control-label">CPF/CNPJ</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" name="emitente_cnpj" id="emitente_cnpj"  value="<?php echo $dados[0]->emitente_cnpj; ?>" placeholder="CNPJ"">
              </div>
            </div>

            <div class="form-group">
              <label for="emitente_nome" class="col-sm-2 control-label">Nome Completo</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" id="emitente_nome" name="emitente_nome"  value="<?php echo $dados[0]->emitente_nome; ?>" placeholder="Nome Completo">
              </div>
            </div> 

            <div class="form-group">
              <label for="emitente_cep" class="col-sm-2 control-label">CEP</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" id="emitente_cep" name="emitente_cep"  value="<?php echo $dados[0]->emitente_cep; ?>" placeholder="Cep">
              </div>
            </div>  

            <div class="form-group">
              <label for="emitente_rua" class="col-sm-2 control-label">Endereço</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" id="emitente_rua" name="emitente_rua"  value="<?php echo $dados[0]->emitente_rua; ?>" placeholder="Endereço">
              </div>
            </div> 
            
            <div class="form-group">
              <label for="emitente_bairro" class="col-sm-2 control-label">Bairro</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" id="emitente_bairro" name="emitente_bairro"  value="<?php echo $dados[0]->emitente_bairro; ?>" placeholder="Bairro">
              </div>
            </div>

            <div class="form-group">
              <label for="emitente_uf" class="col-sm-2 control-label">Estado</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" id="emitente_uf" name="emitente_uf"  value="<?php echo $dados[0]->emitente_uf; ?>" placeholder="Estado">
              </div>
            </div>

            <div class="form-group">
              <label for="emitente_cidade" class="col-sm-2 control-label">Cidade</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" id="emitente_cidade" name="emitente_cidade"  value="<?php echo $dados[0]->emitente_cidade; ?>" placeholder="Cidade">
              </div>
            </div>

            
            <div class="form-group">
              <label for="emitente_numero" class="col-sm-2 control-label">Número</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" id="emitente_numero" name="emitente_numero"  value="<?php echo $dados[0]->emitente_numero; ?>" placeholder="Número">
              </div>
            </div>       

            <div class="form-group">
              <label for="emitente_complemento" class="col-sm-2 control-label">Complemento</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" id="emitente_complemento" name="emitente_complemento"  value="<?php echo $dados[0]->emitente_complemento; ?>" placeholder="Complemento">
              </div>
            </div>

            <div class="form-group">
              <label for="emitente_telefone" class="col-sm-2 control-label">Telefone</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" id="emitente_telefone"  name="emitente_telefone"  value="<?php echo $dados[0]->emitente_telefone; ?>" placeholder="Telefone">
              </div>
            </div>

            <div class="form-group">
              <label for="emitente_celular" class="col-sm-2 control-label">Celular</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="emitente_celular" name="emitente_celular"  value="<?php echo $dados[0]->emitente_celular; ?>" placeholder="Celular">
              </div>
            </div>

            <div class="form-group">
              <label for="emitente_email" class="col-sm-2 control-label">E-mail</label>
              <div class="col-sm-5">                
                <input type="text" class="form-control" id="emitente_email" name="emitente_email"  value="<?php echo $dados[0]->emitente_email; ?>" placeholder="E-mail">
              </div>
            </div>

            <div class="form-group">
              <label for="emitente_email" class="col-sm-2 control-label">E-mail</label>
              <div class="col-sm-5">                
                <input type="text" class="form-control" id="emitente_email" name="emitente_email"  value="<?php echo $dados[0]->emitente_email; ?>" placeholder="E-mail">
              </div>
            </div>

            <div class="form-group">
              <label for="emitente_tipo_empresa" class="col-sm-2 control-label">Tipo</label>
              <div class="col-sm-5">
                <select required class="form-control" name="emitente_tipo_empresa" id="tipo">
                    <option value='1' <?php echo ($dados[0]->emitente_tipo_empresa == 1)?'Selected':'';?>>Matriz</option>
                    <option value='2' <?php echo ($dados[0]->emitente_tipo_empresa == 2)?'Selected':'';?>>Filial</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="emitente_cor_empresa" class="col-sm-2 control-label">Cor</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="emitente_cor_empresa" name="emitente_cor_empresa"  value="<?php echo $dados[0]->emitente_cor_empresa; ?>" placeholder="Cor">
              </div>
            </div>

            <div class="form-group">                
                <label for="produto_imagem" class="col-sm-2 control-label">Imagem </label>
                <div class="controls">
                    <input id="arquivo" type="file" name="userfile" /> (png|jpg|jpeg)
                </div>
            </div>


          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>" class="btn btn-default">Voltar</a>
            <button type="submit" class="btn btn-primary pull-right">Alterar</button>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
  </div>
</section>
