<section class="content">
  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/adicionar" method="post">
          <div class="box-body">         

            <div class="form-group">
              <label for="cliente" class="col-sm-2 control-label">Cliente </label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" name="cliente_nome" id="cliente_nome" value="<?php echo set_value('cliente_nome'); ?>" placeholder="Cliente">
                <input type="hidden" id="cliente_id" name="cliente_id" value="<?php echo set_value('cliente_id'); ?>">
              </div>
            </div>
            <div class="form-group">
              <label for="dataPedido" class="col-sm-2 control-label">Data do Pedido </label>
              <div class="col-sm-5">              
                <input type="date" class="form-control" name="dataPedido"  value="<?php echo set_value('dataPedido'); ?>" placeholder="">
              </div>
            </div>
            
            <div class="form-group">
              <label for="observacao" class="col-sm-2 control-label">Obsevação </label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" name="observacao" id="observacao" value="<?php echo set_value('observacao'); ?>" placeholder="Obsevação">
              </div>
            </div> 
            <div class="form-group">
              <label for="nota" class="col-sm-2 control-label">Emitir Nota </label>
              <div class="col-sm-5">              
                <input  type="checkbox" class="form-check-input" id="nota" name="nota" value="1">
              </div>
            </div>  



          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>" class="btn btn-default">Voltar</a>
            <button type="submit" class="btn btn-primary pull-right"><?php echo ucfirst($this->uri->segment(2)); ?></button>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
  </div>
</section>


<script type="text/javascript">

  $(function () {


    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })

  })


</script>