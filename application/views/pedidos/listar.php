<br><br><section class="content">
  <div class="box">
    <div class="box-header with-border">
      <div class="col-md-2">
        <?php if(verificarPermissao('aAlmoxarifado')){ ?>
        <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/adicionar" type="button" class="btn btn-block btn-default btn-flat">Adicionar</a>
        <?php } ?>
      </div>          
    </div>
    <div class="box-body">
      <div class="container-fluid">
        <table id="list-pedido" class="table" width="100%">
        <thead>
          <tr>
              <th class="columns01 col_default">#</th>
              <th class="columns01 col_default">Data</th>
              <th class="columns02 col_default">Cliente</th>
              <th class="columns02 col_default">Total</th>
              <th class="columns02 col_default">Desconto</th>
              <th class="columns02 col_default">Status</th>
              <th>Ações</th>           
          </tr>
          </thead>
          <tbody>
           <?php foreach ($dados as $dado){ 
              $dado->idPedidos; 
            ?>
              
              <tr>
                   <td class="text-left"><?php echo $dado->idPedidos; ?></td>
                   <td class="text-left"><?php echo date('d/m/Y',  strtotime($dado->dataPedido)); ?></td>
                   <td class="text-left"><?php echo $dado->cliente_nome; ?></td>
                   <td class="text-left"><?php echo $dado->valorTotal; ?></td>
                   <td class="text-left"></td>
                   <td class="text-left"><?php echo $dado->desconto; ?></td>
                   <td>
                      <?php if(verificarPermissao('vPedido')){ ?>
                      <a href="#" onClick="(function(){window.open('<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/visualizar/<?php echo $dado->idPedidos; ?>','MyWindow','toolbar=no,location=no,directories=no,status=no,menubar=yes,scrollbars=yes,resizable=yes,width=350,height=600');return false;})();return false;"data-toggle="tooltip" title="Visualizar"><i class="fa fa-search  text-success"></i> </a>
                      <?php } ?>
                      <?php if(verificarPermissao('ePedido')){ ?>
                        <?php if($dado->finalizado == '0'){ ?>
                      <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/editar/<?php echo $dado->idPedidos; ?>" data-toggle="tooltip" title="Editar"><i class="fa fa-edit"></i> </a>
                         <?php } ?>
                      <?php } ?>
                      <?php if(verificarPermissao('vPedido')){ ?>
                      <a style="cursor:pointer;" title="Deletar" data-toggle="modal" data-target="#excluir" onclick="excluir('<?php echo base_url().$this->uri->segment(1)."/excluir/".$dado->idPedidos; ?>');" ><i class="fa fa-trash  text-danger"></i></a>
                      <?php } ?>             
                   </td>

              </tr>
              
          <?php }  ?>    
          </tbody>

      </table>
        </div>

     </div>
  </div>

</section>

<script type="text/javascript">
    $(document).ready(function () {
        $('#list-pedido').DataTable({
            "order": [
                [0, 'desc']
            ],
            "bDestroy": true
        });
    });
</script>

