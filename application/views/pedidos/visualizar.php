<style type="text/css" media="all">
    body { color: #000; }
    #wrapper { max-width: 480px; margin: 0 auto; padding-top: 20px; }
    .btn { border-radius: 0; margin-bottom: 5px; }
    .table { border-radius: 3px; }
    .table th { background: #f5f5f5; }
    .table th, .table td { vertical-align: middle !important; }
    h3 { margin: 5px 0; }

    @media print {
        .no-print { display: none; }
        #wrapper { max-width: 480px; width: 100%; min-width: 250px; margin: 0 auto; }
    }
</style>

<div id="wrapper">
    <div id="receiptData">
    <div class="no-print">
            </div>
    <div id="receipt-data">
        <div class="text-center">
                <h2><?php echo $emitente[0]->emitente_nome ?> - PDV</h2>
                <?php echo $emitente[0]->emitente_rua ?>,<br>
                <?php echo $emitente[0]->emitente_cep .' - '. $emitente[0]->emitente_cidade.' - '.$emitente[0]->emitente_uf?><br><p>
                Pedido No: <?php echo $pedido ?><br>
                Cliente: <?php echo $cliente ?> <br>
                Data: <?php echo $dataPedido ?> </p>
            <div style="clear:both;"></div>
            <table class="table table-striped table-condensed">
                <thead>
                    <tr>
                        <th class="text-center col-xs-6">Descrição</th>
                        <th class="text-center col-xs-1">Qtd</th>
                        <th class="text-center col-xs-2">Preço</th>
                        <th class="text-center col-xs-3">Subtotal</th>
                    </tr>
                </thead>
                <tbody>                       
                <?php foreach ($dados as $d){ ?>
                <tr>
                    <td class="text-left"><?php echo $d->produto_descricao ?></td>
                    <td class="text-center"><?php echo $d->quantidade ?></td>
                    <td class="text-right"><?php echo $d->valorPago ?></td>
                    <td class="text-right"><?php echo $d->subTotal ?></td>
                </tr>
                <?php } ?>             
                </tbody>
                <tfoot>
<!--                 <tr>
                    <th colspan="2">Total</th>
                    <th colspan="2" class="text-right"><?php echo $total ?></th>
                </tr> -->
                <tr>
                    <th colspan="2">Total Geral</th>
                    <th colspan="2" class="text-right"><?php echo $total ?></th>
                </tr>
                </tfoot>
            </table>
<!--             <table class="table table-striped table-condensed">
                <tbody>
                    <tr>
                        <td>Pagamento via: Dinheiro</td>
                        <td>Valor Pago: <?php echo $ValorPago ?></td>
                        <td>Troco: <?php echo $troco ?></td>
                    </tr>
                </tbody>
            </table>
            <img src=https://chart.googleapis.com/chart?cht=qr&chs=300x300&chl=http%3A%2F%2Fwww.nfe.fazenda.gov.br%2Fportal%2Fconsulta.aspx%3FtipoConsulta%3Dcompleta%26tipoConteudo%3DXbSeqxE8pl8%3DMobLanche_PDVPARATODOS.COM.BR&choe=UTF-8&chld=L|4>
            <div class="well well-sm">
                Volte Sempre!
            <br></div> -->
        </div>
        <div style="clear:both;"></div>
    </div>
<div id="buttons" style="padding-top:10px; text-transform:uppercase;" class="no-print">
    <hr>
    
    <span class="pull-right col-xs-12">
        <a href="javascript:window.print()" id="web_print" class="btn btn-block btn-primary" onClick="window.print();return false;">Impressão Web</a>
    </span>
<!--     <span class="pull-left col-xs-12">
        <a class="btn btn-block btn-success" href="#" id="email">Email</a>
    </span> -->
    <span class="col-xs-12">
        <a class="btn btn-block btn-warning" href="javascript:window.close()">Fechar</a>
    </span>
    <div style="clear:both;"></div>
        <div class="col-xs-12" style="background:#F5F5F5; padding:10px;">
            <font size="-2">
            <p style="font-weight:bold;">Favor alterar as configurações de impressão de seu browser</p>
            <p style="text-transform: capitalize;"><strong>FireFox:</strong> Arquivo &gt; Configurar impressora &gt; Margem &amp;Cabeçalho/Rodapé --Nenhum--</p>
            <p style="text-transform: capitalize;"><strong>Chrome:</strong> Menu &gt; Impressora &gt; Disabilitar Cabeçalho/Rodapé Opções &amp; Setar margem em branco</p></div>
            <font>
    <div style="clear:both;"></div>

</div>

</div>
<canvas id="hidden_screenshot" style="display:none;">

</canvas>
<div class="canvas_con" style="display:none;"></div>
<!-- <script src="<?php echo base_url(); ?>/assets/plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
            <script type="text/javascript">
                $(document).ready(function () {
                    $('#email').click(function () {
                        var email = prompt("Endereço de e-mail", "<?php echo $email ?>");
                        if (email != null) {
                            $.ajax({
                                type: "post",
                                url: "http://localhost/sistemapdv/pos/email_receipt",
                                data: {email: email, id: <?= $venda; ?>},
                                dataType: "json",
                                success: function (data) {
                                    alert(data.msg);
                                },
                                error: function () {
                                    alert('Falha de requisição!');
                                    return false;
                                }
                            });
                        }
                        return false;
                    });
                });
</script> -->