<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <div class="col-md-2">
        <?php if(verificarPermissao('aCategoria')){ ?>
        <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/adicionar" type="button" class="btn btn-block btn-default btn-flat">Adicionar</a>
        <?php } ?>
      </div>          
    </div>
    <div class="box-body">
      <div class="container-fluid">
        <table id="example" class="table" width="100%">
          <thead>
            <tr>       
              <th>#</th>                                     
              <th>Descrição</th>
              <th>Data Cadastro</th>
              <th>Ações</th>                            
            </tr>
          </thead>
          <tbody>

            <?php foreach ($dados as $d){ ?>
            <tr>                      
              <td> <?php echo $d->categoria_fina_id;?></td> 
              <td> <?php echo $d->categoria_fina_descricao;?></td> 
              <td> <?php echo $d->categoria_fina_data_cadastro;?></td>              
              <td>                
              <?php if(verificarPermissao('vCategoria')){ ?>
                <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/visualizar/<?php echo $d->categoria_fina_id; ?>" data-toggle="tooltip" title="Visualizar"><i class="fa fa-search text-success"></i> </a>
                <?php } ?>
                <?php if(verificarPermissao('eCategoria')){ ?>
                <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/editar/<?php echo $d->categoria_fina_id; ?>" data-toggle="tooltip" title="Editar"><i class="fa fa-edit"></i> </a>
                <?php } ?>
                <?php if(verificarPermissao('dCategoria')){ ?>
                <a style="cursor:pointer;" title="Deletar" data-toggle="modal" data-target="#excluir" onclick="excluir('<?php echo base_url().$this->uri->segment(1)."/excluir/".$d->categoria_fina_id; ?>');" ><i class="fa fa-trash text-danger"></i></a>
                <?php } ?>
              </td>
            </tr>
            <?php } ?>

          </tbody>
          <tfoot>
            <tr>                     
              <th>#</th>                                     
              <th>Descrição</th>
              <th>Data Cadastro</th>
              <th>Ações</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</section>