<section class="content">
  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="<?php echo current_url(); ?>" method="post">
        <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $dados[0]->categoria_fina_id; ?>" placeholder="id">
          <div class="box-body">

            <div class="form-group">
              <label for="categoria_fina_descricao" class="col-sm-2 control-label">Descrição*</label>
              <div class="col-sm-5">              
                <input disabled type="text" class="form-control" name="categoria_fina_descricao" id="categoria_fina_descricao" value="<?php echo $dados[0]->categoria_fina_descricao; ?>" placeholder="Descrição">
              </div>
            </div>            
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>" class="btn btn-default">Voltar</a>
            
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
  </div>
</section>