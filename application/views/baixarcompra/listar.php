<br><br><br>
 <section class="content">
  <div class="box">
    <div class="box-header with-border">
      <div class="col-md-2">
        <a type="button" data-toggle="modal" data-target="#modal-baixar" class="btn btn-block btn-default btn-flat">Adicionar</a>
      </div>          
    </div>
    <div class="box-body">
      <div class="container-fluid">       
        <table id="tabBaixar" class="table" width="100%">  
          <thead>
            <tr> 
              <th>#</th>                                           
              <th>Fornecedor</th>
              <th>Valor Total</th>
              <th>Valor Baixa</th>
              <th>Valor Debito</th>
              <th>Data</th>
            </tr>
          </thead>
          <tbody>

            <?php foreach ($dados as $d){ ?>
            <tr> 
              <td> <?php echo $d->id_baixa;?></td>                     
              <td> <?php echo $d->fornecedor_nome;?></td> 
              <td> <?php echo $d->valor_total;?></td> 
              <td> <?php echo $d->valor_baixa;?></td>
              <td> <?php echo $d->valor_debito;?></td>
              <td> <?php echo date('d-m-Y', strtotime($d->data_baixa));?></td>

            </tr>
            <?php } ?>

          </tbody>
          <tfoot>
            <tr>                     
              <th>Fornecedor</th>
              <th>Valor</th>
              <th>Usuario</th>
              <th>Data</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</section>

<div class="modal fade" id="modal-baixar">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalBaixarLoja">Recebimento</h4>
      </div>
      <div class="modal-body" id="modalBodyBaixar">
        <div class="row">
          <div class="col-xs-12">
            <div class="form-group">
              <label class="control-label" for="fornecedor_nomeModal">Fornecedor</label>
              <select required class="form-control input-sm kb-pad" name="fornecedor_nomeModal" id="fornecedor_nomeModal">
                 <option value="">Selecione</option>
                  <?php foreach ($fornecedorPendentes as $p) { ?>
      
                    <option data-valor="<?php echo $p->valor_total_produtos; ?>" data-valor-vendas="<?php echo $p->valor_total_produtos; ?>"data-valor-pago="<?php echo $p->valor_pago; ?>" value='<?php echo $p->fornecedor_id; ?>'><?php echo $p->fornecedor; ?> </option>
                  <?php } ?>
                 </select>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-3">
            <div class="form-group">
              <label class="control-label" for="datepickerCadastroIncio">Data</label>
              <input type="text" name="datepickerBaixar" id="datepickerBaixar" placeholder="dd/mm/aaaa"  class="form-control input-sm kb-text" >
            </div>
          </div>
          <div class="col-xs-3">
            <div class="form-group">
              <label class="control-label" for="total_vendas"><span style="color: red;">Total Compra</span></label>
              <input type="text" style="background-color:yellow; color: red;" name="total_vendas" id="total_vendas" disabled  value="" class="form-control input-sm kb-pad" >
            </div>
          </div>
          <div class="col-xs-3">
            <div class="form-group">
              <label class="control-label" for="total_pago"><span style="color: red;">Total Pago</span></label>
              <input type="text" style="background-color:yellow; color: red;" name="total_pago" id="total_pago" disabled  value="" class="form-control input-sm kb-pad" >
            </div>
          </div>
          <div class="col-xs-3">
            <div class="form-group">
              <label class="control-label" for="total_debito"><span style="color: red;">Total de Debito</span></label>
              <input type="text" style="background-color:yellow; color: red;" name="total_debito" id="total_debito" disabled  value="" class="form-control input-sm kb-pad" >
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-6">
            <div class="form-group">
              <label class="control-label" for="tipo">Recebimento</label>
              <select required class="form-control input-sm kb-pad" name="cliente_tipo" id="tipo">
                <option value="Dinheiro">Dinheiro</option>
                <option value="Cheque">Cheque</option>
                <option value="Cartão de Crédito">Cartão de Crédito</option>
                <option value="Cartão de Débito">Cartão de Débito</option>
                <option value="Crédito Loja">Crédito Loja</option>
                <option value="Vale Alimentação">Vale Alimentação</option>
                <option value="Vale Refeição">Vale Refeição</option>
                <option value="Vale Presente">Vale Presente</option>
                <option value="Vale Combustível">Vale Combustível</option>
                <option value="Duplicata Mercantil">Duplicata Mercantil</option>
                <option value="Boleto Bancário">Boleto Bancário</option>
                <option value="Transferência Bancária">Transferência Bancária</option>
                <option value="Sem pagamento">Sem pagamento</option>
                <option value="Outros">Outros</option>
              </select>
            </div>
          </div>
          <div class="col-xs-3">
            <div class="form-group">
              <label class="control-label" for="valor_pago">Valor Pago</span></label>
              <input type="text" style="color: green;"  name="valor_pago" id="valor_pago" value="" class="form-control input-sm kb-text" >
            </div>
          </div>
          <div class="col-xs-3">
            <div class="form-group">
              <label class="control-label" for="debito"><span style="color: red;">Total em Aberto</span></label>
              <input type="text" style="background-color:yellow; color: red;" name="debito" id="debito" disabled value="" class="form-control input-sm kb-text" >
            </div>
          </div>         
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
        <button type="button" onclick="this.disabled = true;" id='btn-confirm' class="btn btn-success">Confirmar Recebimento</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- Modal alerta Null -->
<div class="example-modal">
  <div class="modal" id="modalNull">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
        </div>
        <div class="modal-body">
          <p id="msg"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success" data-dismiss="modal">ok</button>
        </div>
      </div>      
    </div>          
  </div>        
</div>  
<!-- Fim modal alerta Null -->


<!-- Modal alerta Null -->
<div class="example-modal">
  <div class="modal" id="modalNull">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
        </div>
        <div class="modal-body">
          <p id="msg"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success" data-dismiss="modal">ok</button>
        </div>
      </div>      
    </div>          
  </div>        
</div>  
<!-- Fim modal alerta Null -->

<!-- Modal alerta -->
<div class="example-modal">
  <div class="modal" id="modalAlerta">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
        </div>
        <div class="modal-body">
          <p id="msgAlert"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger pull-left" data-dismiss="modal" id="modalAlertaNao">Não</button>
          <button type="button" onclick="this.disabled = true;" class="btn btn-success" id="modalAlertaSim">Sim</button>
        </div>
      </div>      
    </div>          
  </div>        
</div>  
<!-- Fim modal alerta -->


<script type="text/javascript">

  let validation = true;
  function abreModalNull(msg) {
    $("#modalNull").modal({
      show: true
    });
    $('#msg').text(msg);
    validation = false;
  }

  function abreModalAlert(msg) {
    $("#modalAlerta").modal({
      show: true
    });
    $('#msgAlert').text(msg);
  }
    
  $('#fornecedor_nomeModal').change(function(){
    
     
     $('#total_vendas').val($('#fornecedor_nomeModal').find(':selected').attr('data-valor-vendas'));
     $('#total_pago').val($('#fornecedor_nomeModal').find(':selected').attr('data-valor-pago'));
     valor = $('#total_vendas').val();
     valor_pago = $('#total_pago').val();
     $('#total_debito').val(valor - valor_pago);
  });


  $('#valor_pago').change(function(){

      valor = $('#total_debito').val();
      valor_pago = $('#valor_pago').val();
      $('#debito').val(valor - valor_pago);
     
  });


  $('#btn-confirm').click(function(event) {
    var valor_total  = $('#fornecedor_nomeModal').find(':selected').attr('data-valor');
    var valor_pago   = $('#valor_pago').val();
    var valor_debito = $('#debito').val();
    var data_baixa = $('#datepickerBaixar').val();
    var fornecedor = $('#fornecedor_nomeModal').find(':selected').val();
    var data_baixa = $('#datepickerBaixar').val();
    var validacao  = false;

      if (!valor_pago) {abreModalNull('Valor Pago, em branco preencher por gentileza!!!');}  
      $("#btn-confirm").attr("disabled", false);
      validation = true;

      $.ajax({
          method: "POST",
          url: base_url+"movimentacao/fornecedorBaixarAdd/",
          dataType: "JSON",
          data: { valor_total:valor_total, valor_pago:valor_pago, valor_debito:valor_debito,fornecedor:fornecedor,data_baixa:data_baixa},
          success: function(data)
          {
            location.reload();
          }
          
      });
        
  });

  // function verificarBaixar(cliente, valor_debito) {

  //   var data = $.ajax({
  //         method: "POST",
  //         url: base_url+"financeiro/verificarBaixar/",
  //         dataType: "JSON",
  //         async: false,
  //         data: { valor_pago:valor_pago, cliente: cliente},
  //         success: function(data)
  //         {
  //           if(data == true){
  //             abreModalAlert('Já existe baixa com esse valor no dia de hoje para esse cliente, Deseja Continua ??');
             
  //           }
  //           data = JSON.parse(data);
  //       }
  //   });

  //   return JSON.parse(data.responseText);

  // }

  $('#modalAlertaNao').click(function(event) {
    location.reload();
  });

  $('#modalAlertaSim').click(function(event) {

    var valor_total = $('#fornecedor_nomeModal').find(':selected').attr('data-valor');
    var valor_pago = $('#valor_pago').val();
    var valor_debito = $('#debito').val();
    var fornecedor = $('#fornecedor_nomeModal').find(':selected').val();
      $.ajax({
          method: "POST",
          url: base_url+"movimentacao/fornecedorBaixarAdd/",
          dataType: "JSON",          
          data: { valor_total:valor_total, valor_pago:valor_pago, valor_debito:valor_debito,fornecedor:fornecedor,data_baixa:data_baixa},
          success: function(data)
          {
            location.reload();
          }
          
      });
  });


</script>