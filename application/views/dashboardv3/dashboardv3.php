<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Collapsed Sidebar Layout</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
    href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!-- ADD THE CLASS sidebar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->

<body class="hold-transition skin-blue sidebar-collapse sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">

    <header class="main-header">
      <!-- Logo -->
      <a href="../../index2.html" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>A</b>LT</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Admin</b>LTE</span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- Messages: style can be found in dropdown.less-->
            <li class="dropdown messages-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-envelope-o"></i>
                <span class="label label-success">4</span>
              </a>
              <ul class="dropdown-menu">
                <li class="header">You have 4 messages</li>
                <li>
                  <!-- inner menu: contains the actual data -->
                  <ul class="menu">
                    <li><!-- start message -->
                      <a href="#">
                        <div class="pull-left">
                          <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                        </div>
                        <h4>
                          Support Team
                          <small><i class="fa fa-clock-o"></i> 5 mins</small>
                        </h4>
                        <p>Why not buy a new awesome theme?</p>
                      </a>
                    </li>
                    <!-- end message -->
                  </ul>
                </li>
                <li class="footer"><a href="#">See All Messages</a></li>
              </ul>
            </li>
            <!-- Notifications: style can be found in dropdown.less -->
            <li class="dropdown notifications-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bell-o"></i>
                <span class="label label-warning">10</span>
              </a>
              <ul class="dropdown-menu">
                <li class="header">You have 10 notifications</li>
                <li>
                  <!-- inner menu: contains the actual data -->
                  <ul class="menu">
                    <li>
                      <a href="#">
                        <i class="fa fa-users text-aqua"></i> 5 new members joined today
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="footer"><a href="#">View all</a></li>
              </ul>
            </li>
            <!-- Tasks: style can be found in dropdown.less -->
            <li class="dropdown tasks-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-flag-o"></i>
                <span class="label label-danger">9</span>
              </a>
              <ul class="dropdown-menu">
                <li class="header">You have 9 tasks</li>
                <li>
                  <!-- inner menu: contains the actual data -->
                  <ul class="menu">
                    <li><!-- Task item -->
                      <a href="#">
                        <h3>
                          Design some buttons
                          <small class="pull-right">20%</small>
                        </h3>
                        <div class="progress xs">
                          <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar"
                            aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                            <span class="sr-only">20% Complete</span>
                          </div>
                        </div>
                      </a>
                    </li>
                    <!-- end task item -->
                  </ul>
                </li>
                <li class="footer">
                  <a href="#">View all tasks</a>
                </li>
              </ul>
            </li>
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
            </li>
            <!-- Control Sidebar Toggle Button -->
            <li>
              <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
            </li>
          </ul>
        </div>
      </nav>
    </header>

    <!-- =============================================== -->

    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
            <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
          </div>
          <div class="pull-left info">
            <p>Alexander Pierce</p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
          <div class="input-group">
            <input type="text" name="q" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
          </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header">MAIN NAVIGATION</li>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-dashboard"></i> <span>Dashboard</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="../../index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
              <li><a href="../../index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
            </ul>
          </li>
          <li class="treeview active">
            <a href="#">
              <i class="fa fa-files-o"></i>
              <span>Layout Options</span>
              <span class="pull-right-container">
                <span class="label label-primary pull-right">4</span>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="top-nav.html"><i class="fa fa-circle-o"></i> Top Navigation</a></li>
              <li><a href="boxed.html"><i class="fa fa-circle-o"></i> Boxed</a></li>
              <li><a href="fixed.html"><i class="fa fa-circle-o"></i> Fixed</a></li>
              <li class="active"><a href="collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a>
              </li>
            </ul>
          </li>
          <li>
            <a href="../widgets.html">
              <i class="fa fa-th"></i> <span>Widgets</span>
              <span class="pull-right-container">
                <small class="label pull-right bg-green">new</small>
              </span>
            </a>
          </li>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-pie-chart"></i>
              <span>Charts</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="../charts/chartjs.html"><i class="fa fa-circle-o"></i> ChartJS</a></li>
              <li><a href="../charts/morris.html"><i class="fa fa-circle-o"></i> Morris</a></li>
              <li><a href="../charts/flot.html"><i class="fa fa-circle-o"></i> Flot</a></li>
              <li><a href="../charts/inline.html"><i class="fa fa-circle-o"></i> Inline charts</a></li>
            </ul>
          </li>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-laptop"></i>
              <span>UI Elements</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="../UI/general.html"><i class="fa fa-circle-o"></i> General</a></li>
              <li><a href="../UI/icons.html"><i class="fa fa-circle-o"></i> Icons</a></li>
              <li><a href="../UI/buttons.html"><i class="fa fa-circle-o"></i> Buttons</a></li>
              <li><a href="../UI/sliders.html"><i class="fa fa-circle-o"></i> Sliders</a></li>
              <li><a href="../UI/timeline.html"><i class="fa fa-circle-o"></i> Timeline</a></li>
              <li><a href="../UI/modals.html"><i class="fa fa-circle-o"></i> Modals</a></li>
            </ul>
          </li>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-edit"></i> <span>Forms</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="../forms/general.html"><i class="fa fa-circle-o"></i> General Elements</a></li>
              <li><a href="../forms/advanced.html"><i class="fa fa-circle-o"></i> Advanced Elements</a></li>
              <li><a href="../forms/editors.html"><i class="fa fa-circle-o"></i> Editors</a></li>
            </ul>
          </li>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-table"></i> <span>Tables</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="../tables/simple.html"><i class="fa fa-circle-o"></i> Simple tables</a></li>
              <li><a href="../tables/data.html"><i class="fa fa-circle-o"></i> Data tables</a></li>
            </ul>
          </li>
          <li>
            <a href="../calendar.html">
              <i class="fa fa-calendar"></i> <span>Calendar</span>
              <span class="pull-right-container">
                <small class="label pull-right bg-red">3</small>
                <small class="label pull-right bg-blue">17</small>
              </span>
            </a>
          </li>
          <li>
            <a href="../mailbox/mailbox.html">
              <i class="fa fa-envelope"></i> <span>Mailbox</span>
              <span class="pull-right-container">
                <small class="label pull-right bg-yellow">12</small>
                <small class="label pull-right bg-green">16</small>
                <small class="label pull-right bg-red">5</small>
              </span>
            </a>
          </li>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-folder"></i> <span>Examples</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="../examples/invoice.html"><i class="fa fa-circle-o"></i> Invoice</a></li>
              <li><a href="../examples/profile.html"><i class="fa fa-circle-o"></i> Profile</a></li>
              <li><a href="../examples/login.html"><i class="fa fa-circle-o"></i> Login</a></li>
              <li><a href="../examples/register.html"><i class="fa fa-circle-o"></i> Register</a></li>
              <li><a href="../examples/lockscreen.html"><i class="fa fa-circle-o"></i> Lockscreen</a></li>
              <li><a href="../examples/404.html"><i class="fa fa-circle-o"></i> 404 Error</a></li>
              <li><a href="../examples/500.html"><i class="fa fa-circle-o"></i> 500 Error</a></li>
              <li><a href="../examples/blank.html"><i class="fa fa-circle-o"></i> Blank Page</a></li>
              <li><a href="../examples/pace.html"><i class="fa fa-circle-o"></i> Pace Page</a></li>
            </ul>
          </li>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-share"></i> <span>Multilevel</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
              <li class="treeview">
                <a href="#"><i class="fa fa-circle-o"></i> Level One
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                  <li class="treeview">
                    <a href="#"><i class="fa fa-circle-o"></i> Level Two
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                    </a>
                    <ul class="treeview-menu">
                      <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                      <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                    </ul>
                  </li>
                </ul>
              </li>
              <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
            </ul>
          </li>
          <li><a href="https://adminlte.io/docs"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
          <li class="header">LABELS</li>
          <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
          <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
          <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>

    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          GRUPO CELL
          <small>Dashboard</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">Layout</a></li>
          <li class="active">Collapsed Sidebar</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">

        <!-- Default box -->

        <!-- Main row -->
        <div class="row">

          <!--COLUNA 01 -->
          <div class="col-md-4">


            <!-- USERS LIST -->
            <div class="box box-danger">
              <div class="box-header with-border">
                <h3 class="box-title">Aniversariante do Mês</h3>

                <div class="box-tools pull-right">
                  <span class="label label-danger" id="qtdAniver"></span>
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                  </button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body no-padding">
                <ul class="users-list clearfix" id="aniversariantesdomes">

                </ul>
                <!-- /.users-list -->
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-center">
                <a href="javascript:void(0)" class="uppercase">View All Users</a>
              </div>
              <!-- /.box-footer -->
            </div>
            <!--/.box -->







            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Vendas do Mês por Vendedor e Vendas dia Anterior / Atual</h3>
                <!-- + Vendas dia anterior / atual -->

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                      class="fa fa-times"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="chart-responsive">
                  <script src="https://www.gstatic.com/charts/loader.js"></script>
                  <script>
                    google.charts.load('current', { 'packages': ['corechart'] });
                    google.charts.setOnLoadCallback(drawChart);

                    function drawChart() {
                      $.ajax({
                        url: "<?php echo base_url(); ?>notas/pegarVendasPorVendedor",
                        type: "POST",
                        dataType: "json",
                        success: function (response) {
                          if (response.dados && response.dados.length > 0) {
                            var chartData = [['usuarios_id', 'totalProduto']];

                            $.each(response.dados, function (index, value) {
                              chartData.push([value.usuarios_id, parseInt(value.totalProduto)]);
                            });

                            var data = google.visualization.arrayToDataTable(chartData);
                            var options = {
                              title: 'VENDAS DO MÊS POR VENDEDOR',
                              is3D: true,
                              sliceVisibilityThreshold: 0, // mostra todos os slices, mesmo os menores
                              pieSliceTextStyle: {
                                fontSize: 18 // ajusta o tamanho da fonte
                              },
                              textStyle: {
                                fontSize: 18 // ajusta o tamanho da fonte da legenda referente ao slice
                              },
                              chartArea: {
                              left: 50, // ajusta a area do grafico
                              top: 50, 
                              width: '80%', 
                              height: '80%' 
                            },
                            legend: {
                              textStyle: {
                                fontSize: 14 // ajusta o tamanho da fonte da legenda
                              }
                            }
                            };
                            var chart = new google.visualization.PieChart(document.getElementById('myChart2'));
                            chart.draw(data, options);
                          }

                          else {
                            document.getElementById('myChart2').innerHTML = '<center><img src="https://i.pinimg.com/564x/66/ed/b1/66edb18baed87748cfe51b22a2c81fcf.jpg" alt="No Data Image" style="max-width: 350px; height: 350px;"></center>';
                            // document.getElementById('myChart2').innerHTML = '<h1><center>Erro, nenhuma venda encontrada</center></h1>';
                          }
                        },
                        error: function () {
                          document.getElementById('myChart2').innerHTML = '<h1><center>Algo deu errado ao carregar os dados</center></h1>';
                        }


                      });

                    }
                  </script>
                  <div id="myChart2" style="width:100%; max-width:900px; height: 350px;"></div>
                </div>
                <!-- ./chart-responsive -->
                <div class="row">
                  <div class="col-md-8">
                  </div>

                </div>
                <!-- /.row -->
              </div>

              <!-- /.box-body -->
              <div class="box-footer no-padding">

                <ul class="nav nav-pills nav-stacked" id="ContainerVendedor">
                  <!-- aqui onde vai gerar os vendedores de forma dinamica -->
                </ul>

              </div>

              <script>

                function dadosVendedores() {
                  $.ajax({
                    url: "<?php echo base_url(); ?>notas/pegarVendedoresDinamicamente",
                    type: "POST",
                    dataType: "json",
                    success: function (response) {
                      var container = document.getElementById('ContainerVendedor');
                      container.innerHTML = ''; // limpar conteúdo se existir

                      var vendedoresProcessados = new Set(); // grava os vendedores que já foram processados pra usar depois, confia

                      if (response.dadosDA && response.dadosDA.length > 0) {
                        response.dadosDA.forEach(function (item) { // item -> dados do dia anterior
                          response.dadosDiaAtual.forEach(function (item2) { // item2 -> dados do dia atual

                            if (item.usuarios_id == item2.usuarios_id) {
                              vendedoresProcessados.add(item2.usuarios_id);
                              var li = document.createElement('li');

                              // Determinar qual valor é maior
                              var valorDiaAnterior = parseInt(item.totalProduto, 10);
                              var valorDiaAtual = parseInt(item2.totalProduto, 10);

                              if (valorDiaAtual > valorDiaAnterior) {
                                li.innerHTML = '<a href="#"> Vendedor ' + item.usuarios_id +
                                  ' <span class="pull-right text-green" style="margin-left: 10px;"><i class="fa fa-angle-up"></i> ' +
                                  valorDiaAtual + ' </span>' + 
                                  ' <span class="pull-right" style="margin-left: 10px;"> / </span>' +
                                  '<span class="pull-right text-red"><i class="fa fa-angle-down"></i> ' +
                                  valorDiaAnterior + ' </span></a>';
                              } else if (valorDiaAtual < valorDiaAnterior) {
                                li.innerHTML = '<a href="#"> Vendedor ' + item.usuarios_id +
                                  ' <span class="pull-right text-red" style="margin-left: 10px;"><i class="fa fa-angle-down"></i> ' +
                                  valorDiaAtual + ' </span>' + 
                                  ' <span class="pull-right" style="margin-left: 10px;"> / </span>' +
                                  '<span class="pull-right text-green"><i class="fa fa-angle-up"></i> ' +
                                  valorDiaAnterior + '  </span></a>';
                              } else {
                                li.innerHTML = '<a href="#"> Vendedor ' + item.usuarios_id +
                                  ' <span class="pull-right text-yellow" style="margin-left: 10px;"><i class="fa fa-angle-right"></i> ' +
                                  valorDiaAtual + ' </span>' + 
                                  ' <span class="pull-right" style="margin-left: 10px;"> / </span>' +
                                  ' <span class="pull-right text-yellow"><i class="fa fa-angle-right"></i> ' +
                                  valorDiaAnterior + '  </span></a>';
                              }
                              container.appendChild(li);
                            }
                          });
                        });
                      }

                      // Vê se o caba vendeu hoje e não ontem
                      response.dadosDiaAtual.forEach(function (item2) {
                        if (!vendedoresProcessados.has(item2.usuarios_id)) {
                          var li = document.createElement('li');
                          var li = document.createElement('li');
                          li.innerHTML = '<a href="#"> Vendedor ' + item2.usuarios_id +
                            ' <span class="pull-right text-green"><i class="fa fa-angle-up"></i> ' +
                            item2.totalProduto + ' (Dia Atual)</span></a>';
                          container.appendChild(li);
                        }
                      });
                    },

                    error: function (xhr, status, error) {
                      console.error('Erro na requisição:', error);

                    }
                  });
                }

                document.addEventListener('DOMContentLoaded', function () {
                  dadosVendedores();
                });


              </script>
              <!-- /.footer -->

            </div>
            <!-- /.box -->

            <!-- PRODUCT LIST -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Recently Added Products</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                      class="fa fa-times"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <ul class="products-list product-list-in-box">
                  <li class="item">
                    <div class="product-img">
                      <img src="dist/img/default-50x50.gif" alt="Product Image">
                    </div>
                    <div class="product-info">
                      <a href="javascript:void(0)" class="product-title">Samsung TV
                        <span class="label label-warning pull-right">$1800</span></a>
                      <span class="product-description">
                        Samsung 32" 1080p 60Hz LED Smart HDTV.
                      </span>
                    </div>
                  </li>
                  <!-- /.item -->
                  <li class="item">
                    <div class="product-img">
                      <img src="dist/img/default-50x50.gif" alt="Product Image">
                    </div>
                    <div class="product-info">
                      <a href="javascript:void(0)" class="product-title">Bicycle
                        <span class="label label-info pull-right">$700</span></a>
                      <span class="product-description">
                        26" Mongoose Dolomite Men's 7-speed, Navy Blue.
                      </span>
                    </div>
                  </li>
                  <!-- /.item -->
                  <li class="item">
                    <div class="product-img">
                      <img src="dist/img/default-50x50.gif" alt="Product Image">
                    </div>
                    <div class="product-info">
                      <a href="javascript:void(0)" class="product-title">Xbox One <span
                          class="label label-danger pull-right">$350</span></a>
                      <span class="product-description">
                        Xbox One Console Bundle with Halo Master Chief Collection.
                      </span>
                    </div>
                  </li>
                  <!-- /.item -->
                  <li class="item">
                    <div class="product-img">
                      <img src="dist/img/default-50x50.gif" alt="Product Image">
                    </div>
                    <div class="product-info">
                      <a href="javascript:void(0)" class="product-title">PlayStation 4
                        <span class="label label-success pull-right">$399</span></a>
                      <span class="product-description">
                        PlayStation 4 500GB Console (PS4)
                      </span>
                    </div>
                  </li>
                  <!-- /.item -->
                </ul>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-center">
                <a href="javascript:void(0)" class="uppercase">View All Products</a>
              </div>
              <!-- /.box-footer -->
            </div>
            <!-- /.box -->

          </div>
          <!-- /.col -->


          <!-- COLUNA 02 -->
          <div class="col-md-4">
            <div class="box box-default">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-8" style="text-align: center;">
                    <h1 style="font-size:50px !important;">Dólar hoje</h1>
                    <h1 id="usd-bid"><i id="usd-icon"></i> <span id="usd-bid-value"></span></h1>
                  </div>
                  <div class="col-md-4">
                    <ul class="chart-legend clearfix" style="font-size:20px;">
                      <li><i class="fa fa-circle-o text-yellow"></i> Dólar/Real <span id="usd-legend-bid"></span></li>
                      <li><i class="fa fa-circle-o text-yellow"></i> Euro/Real <span id="eur-legend-bid"></span></li>
                      <li><i class="fa fa-circle-o text-yellow"></i> Bitcoin/Real <span id="btc-legend-bid"></span></li>
                      <li><i class="fa fa-circle-o text-yellow"></i> USDT/Real <span id="usdt-legend-bid"></span></li>
                    </ul>
                  </div>
                </div>
                <!-- /.row -->
              </div>
              <!-- /.box-body -->
              <div class="box-footer no-padding" style="font-size:20px;">
                <ul class="nav nav-pills nav-stacked">
                  <li id="usd-brl"><a href="#">Dólar Americano/Real
                      <span class="pull-right text-red"><i class="fa fa-angle-down"></i> <span
                          id="usd-value">-0.024</span></span></a></li>
                  <li id="eur-brl"><a href="#">Euro/Real <span class="pull-right text-red"><i
                          class="fa fa-angle-down"></i> <span id="eur-value">-0.0277</span></span></a>
                  </li>
                  <li id="btc-brl"><a href="#">Bitcoin/Real<span class="pull-right text-red"><i
                          class="fa fa-angle-down"></i> <span id="btc-value">5326</span></span></a></li>
                </ul>
              </div>
              <!-- /.footer -->
            </div>
            <!-- /.box -->
            <div class="box box-default">
              <div class="box-body" style="text-align: center;">
                <h1 id="clock" style="white-space: break-spaces;"></h1>
                <!-- /.row -->
              </div>
            </div>

            <div class="box box-default">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-8" style="text-align: center;">
                    <h1 style="font-size:50px !important;">Clima/Tempo</h1>
                    <h1 id="usd-bid"><i id="usd-icon"></i> <span id="usd-bid-value"></span></h1>
                  </div>
                </div>
                <!-- /.row -->
              </div>
              <!-- /.box-body -->
              <div class="box-footer no-padding" style="font-size:20px;">
                <ul class="nav nav-pills nav-stacked">
                  <li id="usd-brl"><a href="#"><span id="olindaNome">Olinda</span> <span
                        class="pull-right text-yellow"><i class="fa fa-angle-left"></i>
                        <span id="olinda-prev">0°</span></span></a></li>
                  <li id="eur-brl"><a href="#"><span id="recifeNome">Recife</span> <span
                        class="pull-right text-yellow"><i class="fa fa-angle-left"></i>
                        <span id="recife-prev">0°</span></span></a>
                  </li>
                  <li id="btc-brl"><a href="#"><span id="jaboataoNome">Jaboatão dos Guararapes</span><span
                        class="pull-right text-yellow"><i class="fa fa-angle-left"></i>
                        <span id="jaboatao-prev">0°</span></span></a></li>
                </ul>
              </div>
              <!-- /.footer -->
            </div>
          </div>
          <!-- /.col -->
          <!-- COLUNA 03 -->
          <div class="col-md-4">

            <!-- USERS LIST -->
            <div class="box box-danger">
              <div class="box-header with-border">
                <h3 class="box-title">Venda - Mês</h3>

                <div class="box-tools pull-right">
                  <span class="label label-danger">Detalhe</span>
                  <!-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
            </button> -->
                </div>
              </div>

              <!-- google chart graphic -->
              <script src="https://www.gstatic.com/charts/loader.js"></script>
              <div id="myChart" style="width:100%; max-width:600px; height:350px; text-overflow: clip;">
              </div>

              <script type="text/javascript">
                google.charts.load('current', { 'packages': ['corechart'] });
                google.charts.setOnLoadCallback(drawChart);

                function drawChart() {
                  $.ajax({
                    url: "<?php echo base_url(); ?>notas/pegarVendasMes",
                    type: "POST",
                    dataType: "json",
                    success: function (response) {
                      if (response.detalhes && response.detalhes.length > 0) {
                        // console.log(response.detalhes);
                        var chartData = [['Grupo', 'Porcentagem']];
                        var totalVendas = response.detalhes.reduce(function (acc, curr) {
                          return acc + parseFloat(curr.total);
                        }, 0);

                        response.detalhes.forEach(function (item) {
                          var porcentagem = (parseFloat(item.total) / totalVendas) * 100;
                          chartData.push([item.grupo, porcentagem]);
                        });

                        var data = google.visualization.arrayToDataTable(chartData);
                        var options = {
                          title: 'VENDAS POR GRUPO',
                          is3D: true,
                          sliceVisibilityThreshold: 0, // mostra todos os slices, mesmo os menores
                          pieSliceTextStyle: {
                            fontSize: 18 // ajusta o tamanho da fonte de dentro do slice
                          },
                          textStyle: {
                            fontSize: 18 // ajusta o tamanho da fonte da legenda referente ao slice
                          },
                          chartArea: {
                          left: 50, // ajusta a area do grafico
                          top: 50, 
                          width: '80%', 
                          height: '80%' 
                        },
                        legend: {
                          textStyle: {
                            fontSize: 14 // ajuste o tamanho da fonte da legenda do grafico
                          }
                        }
                        
                        };
                        var chart = new google.visualization.PieChart(document.getElementById('myChart'));
                        chart.draw(data, options);
                      }

                      else {
                        document.getElementById('myChart').innerHTML = '<center><img src="https://p16-capcut-sign-va.ibyteimg.com/tos-maliva-v-be9c48-us/owMvwynQhuN5BfJDJAEEAogOW1AdBVzE0IGt6s~tplv-nhvfeczskr-1:250:0.webp?lk3s=44acef4b&x-expires=1743775810&x-signature=htyfR8VQpymSoAP0SOIpblULYwY%3D" alt="No Data Image" style="max-width: 350px; height: 350px;"></center>';
                        // document.getElementById('myChart2').innerHTML = '<h1><center>Erro, nenhuma venda encontrada</center></h1>';
                      }

                    },

                    error: function (response) {
                      document.getElementById('myChart').innerHTML = '<h1><center>Algo deu errado</center></h1>';
                    }

                  });
                }
              </script>

              <!-- /google chart graphic -->




              <!-- /.box-header -->
              <div class="box-body">
                <div class="row" id="vendacategoriadia">




                </div>
                <!-- /.row -->
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <div class="row">
                  <div class="col-sm-4 col-xs-6">
                    <div class="description-block border-right">
                      <span class="description-text">TOTAL</span>
                      <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 0%</span>
                      <h5 class="description-header">$0</h5>

                    </div>
                    <!-- /.description-block -->
                  </div>
                  <!-- /.col -->
                  <div class="col-sm-4 col-xs-6">
                    <div class="description-block border-right">
                      <span class="description-text">CUSTO</span>
                      <span class="description-percentage text-yellow"><i class="fa fa-caret-left"></i> 0%</span>
                      <h5 class="description-header">$0</h5>
                    </div>
                    <!-- /.description-block -->
                  </div>
                  <!-- /.col -->
                  <div class="col-sm-4 col-xs-6">
                    <div class="description-block border-right">
                      <span class="description-text">LUCRO</span>
                      <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 0%</span>
                      <h5 class="description-header">$0</h5>

                    </div>
                    <!-- /.description-block -->
                  </div>
                  <!-- /.col -->
                  <!--
            <div class="col-sm-3 col-xs-6">
              <div class="description-block">
              <span class="description-percentage text-red"><i class="fa fa-caret-down"></i> 18%</span>
              <h5 class="description-header">1200</h5>
              <span class="description-text">GOAL COMPLETIONS</span>
              </div>                 
            </div> -->
                </div>
                <!-- /.row -->
              </div>
              <!-- /.box-footer -->


            </div>
            <!--/.box -->


            <!-- Info Boxes Style 2 -->
            <div class="info-box bg-yellow">
              <span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Inventory</span>
                <span class="info-box-number">5,200</span>

                <div class="progress">
                  <div class="progress-bar" style="width: 50%"></div>
                </div>
                <span class="progress-description">
                  50% Increase in 30 Days
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
            <div class="info-box bg-green">
              <span class="info-box-icon"><i class="ion ion-ios-heart-outline"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Mentions</span>
                <span class="info-box-number">92,050</span>

                <div class="progress">
                  <div class="progress-bar" style="width: 20%"></div>
                </div>
                <span class="progress-description">
                  20% Increase in 30 Days
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
            <div class="info-box bg-red">
              <span class="info-box-icon"><i class="ion ion-ios-cloud-download-outline"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Downloads</span>
                <span class="info-box-number">114,381</span>

                <div class="progress">
                  <div class="progress-bar" style="width: 70%"></div>
                </div>
                <span class="progress-description">
                  70% Increase in 30 Days
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
            <div class="info-box bg-aqua">
              <span class="info-box-icon"><i class="ion-ios-chatbubble-outline"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Direct Messages</span>
                <span class="info-box-number">163,921</span>

                <div class="progress">
                  <div class="progress-bar" style="width: 40%"></div>
                </div>
                <span class="progress-description">
                  40% Increase in 30 Days
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->







            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Browser Usage</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                      class="fa fa-times"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-md-8">
                    <div class="chart-responsive">
                      <canvas id="pieChart" height="150"></canvas>
                    </div>
                    <!-- ./chart-responsive -->
                  </div>
                  <!-- /.col -->
                  <div class="col-md-4">
                    <ul class="chart-legend clearfix">
                      <li><i class="fa fa-circle-o text-red"></i> Chrome</li>
                      <li><i class="fa fa-circle-o text-green"></i> IE</li>
                      <li><i class="fa fa-circle-o text-yellow"></i> FireFox</li>
                      <li><i class="fa fa-circle-o text-aqua"></i> Safari</li>
                      <li><i class="fa fa-circle-o text-light-blue"></i> Opera</li>
                      <li><i class="fa fa-circle-o text-gray"></i> Navigator</li>
                    </ul>
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- /.box-body -->
              <div class="box-footer no-padding">
                <ul class="nav nav-pills nav-stacked">
                  <li><a href="#">United States of America
                      <span class="pull-right text-red"><i class="fa fa-angle-down"></i> 12%</span></a></li>
                  <li><a href="#">India <span class="pull-right text-green"><i class="fa fa-angle-up"></i> 4%</span></a>
                  </li>
                  <li><a href="#">China
                      <span class="pull-right text-yellow"><i class="fa fa-angle-left"></i> 0%</span></a></li>
                </ul>
              </div>
              <!-- /.footer -->
            </div>
            <!-- /.box -->



          </div>
          <!-- /.col -->



        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <b>Version</b> 1.0.0
      </div>
      <strong>Copyright &copy; 2024- <?php echo date('Y'); ?> <a
          href="www.wdmtecnologia.com.br">WDM-Tecnologia</a>.</strong> All rights
      reserved.
    </footer>












    <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
  </div>

  <script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>

  <script>
    $(document).ready(function () {
      previsaoTempo('olinda');
      previsaoTempo('recife');
      previsaoTempo('jaboatao');
      preencherAniversarios();
      preencherVendaCategoriaDia();
      function updateClock() {
        var now = new Date();
        var hours = now.getHours();
        var minutes = now.getMinutes();
        var seconds = now.getSeconds();
        var timeString = formatTime(hours) + ':' + formatTime(minutes) + ':' + formatTime(seconds);
        $('#clock').text(atualizarDataHoje() + ' ' + ' ' + timeString);
      }

      function formatTime(time) {
        if (time < 10) {
          return '0' + time;
        }
        return time;
      }

      function atualizarDataHoje() {
        let dataHoje = new Date();

        let dia = dataHoje.getDate();
        let mes = dataHoje.getMonth() + 1;
        let ano = dataHoje.getFullYear();

        if (dia < 10) {
          dia = '0' + dia;
        }
        if (mes < 10) {
          mes = '0' + mes;
        }
        return dia + '/' + mes + '/' + ano;
      }


      function fetchData() {
        $.get('https://economia.awesomeapi.com.br/last/USD-BRL,EUR-BRL,BTC-BRL', function (data) {
          const usdBrl = data.USDBRL;
          const eurBrl = data.EURBRL;
          const btcBrl = data.BTCBRL;

          $('#usd-value').text(usdBrl.varBid);
          $('#eur-value').text(eurBrl.varBid);
          $('#btc-value').text(btcBrl.varBid);

          updateClass('#usd-value', usdBrl.varBid);
          updateClass('#eur-value', eurBrl.varBid);
          updateClass('#btc-value', btcBrl.varBid);

          $('#usd-bid-value').text(`R$ ${usdBrl.bid}`);
          $('#usd-icon').attr('class', parseFloat(usdBrl.varBid) >= 0 ? 'fa fa-angle-up text-green' : 'fa fa-angle-down text-red');

          $('#usd-legend-bid').text(`R$ ${usdBrl.bid}`);
          $('#eur-legend-bid').text(`R$ ${eurBrl.bid}`);
          $('#btc-legend-bid').text(`R$ ${btcBrl.bid}`);
        }).fail(function (error) {
          console.error('Error fetching data:', error);
        });
      }


      function pegarUsdt() {
        $.get('https://api.coingecko.com/api/v3/simple/price?ids=tether&vs_currencies=brl', function (data) {
          console.log(data)
          $('#usdt-legend-bid').text(`R$ ${data.tether.brl}`);
        }).fail(function (error) {
          console.error('Error fetching data:', error);
        });
      }

      function updateClass(elementId, variation) {
        const $element = $(elementId);
        if (parseFloat(variation) >= 0) {
          $element.parent().removeClass('text-red').addClass('text-green');
          $element.prev().attr('class', 'fa fa-angle-up');
        } else {
          $element.parent().removeClass('text-green').addClass('text-red');
          $element.prev().attr('class', 'fa fa-angle-down');
        }
      }

      pegarUsdt()
      setInterval(pegarUsdt, 300000);
      fetchData();
      setInterval(fetchData, 300000);
      updateClock();
      setInterval(updateClock, 1000);

      setInterval(function () {
        previsaoTempo('olinda');
        previsaoTempo('recife');
        previsaoTempo('jaboatao');
      }, 20 * 60 * 1000);

      function previsaoTempo(cidade) {
        $.ajax({
          url: 'https://api.tomorrow.io/v4/weather/realtime?location=' + cidade + '&apikey=sVYM1JijsLNmtIUCjlgzBEmxj1XENYrL',
          method: 'GET',
          success: function (dados) {
            let periodo = 0;

            if (cidade == 'olinda') {
              $('#olinda-prev').text(Math.round(dados.data.values.temperature) + '°');

              if (isDayOrNight() == 'noite') {
                periodo = 1;
              }

              $("#olindaNome").html('Olinda ' + '<img src="<?php echo base_url(); ?>assets/tempo/' + dados.data.values.weatherCode + periodo + '.png" alt="User Image">')
            }
            if (cidade == 'recife') {
              $('#recife-prev').text(Math.round(dados.data.values.temperature) + '°');

              if (isDayOrNight() == 'noite') {
                periodo = 1;
              }

              $("#recifeNome").html('Recife ' + '<img src="<?php echo base_url(); ?>assets/tempo/' + dados.data.values.weatherCode + periodo + '.png" alt="User Image">')
            }
            if (cidade == 'jaboatao') {
              $('#jaboatao-prev').text(Math.round(dados.data.values.temperature) + '°');

              if (isDayOrNight() == 'noite') {
                periodo = 1;
              }

              $("#jaboataoNome").html('Jaboatão dos Guararapes   ' + '<img src="<?php echo base_url(); ?>assets/tempo/' + dados.data.values.weatherCode + periodo + '.png" alt="User Image">')
            }
          },
          error: function (xhr, status, error) {
            // A função error é chamada se houver um erro durante a solicitação
            console.error('Erro ao fazer a solicitação:', error);
          }
        });

      }

      function isDayOrNight() {
        var currentHour = new Date().getHours();

        if (currentHour >= 18 || currentHour < 4) {
          return "noite";
        } else {
          return "dia";
        }
      }

      function preencherAniversarios() {

        $.ajax({
          url: "<?php echo base_url(); ?>notas/pegarAniversariantes",
          type: "GET",
          dataType: "json",
          success: function (data) {
            $('#qtdAniver').text(data.qtd + ' Aniversariantes');
            data.dados.forEach(element => {
              let linha = '<li><img src="' + element.usuario_imagem + '" style="width:125px;" alt="User Image"> <a class="users-list-name" href="#">' + element.usuario_nome + '</a><span class="users-list-date">' + element.usuario_data_nascimento + '</span></li>';

              $('#aniversariantesdomes').append(linha);
            });
          }
        });
      }


      function preencherGrafico() {

        $.ajax({
          url: "<?php echo base_url(); ?>notas/pegarVendasMes",
          type: "GET",
          dataType: "json",
          success: function (data) {
            data.dados.forEach(element => {
              let linha = '<div class="col-xs-6 col-md-3 text-center"><div class="knob-label">' + element.grupo + '</div><input type="text" class="knob" value="30" data-width="60" data-height="60" data-fgColor="#3c8dbc"><div class="knob-label">Qtd. Produtos: 1</div></div>';

              $('#vendamesgrafico').append(linha);
            });
          }
        });
      }


      function preencherVendaCategoriaDia() {


        $.ajax({
          url: "<?php echo base_url(); ?>notas/pegarVendaCategoriaDia",
          type: "GET",
          dataType: "json",
          success: function (data) {
            data.dados.forEach(element => {
              let linha = '<div class="col-xs-6 col-md-3 text-center"><div class="knob-label">' + element.grupo + '</div><input type="text" class="knob" value="30" data-width="60" data-height="60" data-fgColor="#3c8dbc"><div class="knob-label">Qtd. Produtos: 1</div></div>';

              $('#vendacategoriadia').append(linha);
            });
          }
        });
      }







      $(".knob").knob({
        /*change : function (value) {
         //console.log("change : " + value);
         },
         release : function (value) {
         console.log("release : " + value);
         },
         cancel : function () {
         console.log("cancel : " + this.value);
         },*/
        draw: function () {

          // "tron" case
          if (this.$.data('skin') == 'tron') {

            var a = this.angle(this.cv)  // Angle
              , sa = this.startAngle          // Previous start angle
              , sat = this.startAngle         // Start angle
              , ea                            // Previous end angle
              , eat = sat + a                 // End angle
              , r = true;

            this.g.lineWidth = this.lineWidth;

            this.o.cursor
              && (sat = eat - 0.3)
              && (eat = eat + 0.3);

            if (this.o.displayPrevious) {
              ea = this.startAngle + this.angle(this.value);
              this.o.cursor
                && (sa = ea - 0.3)
                && (ea = ea + 0.3);
              this.g.beginPath();
              this.g.strokeStyle = this.previousColor;
              this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);
              this.g.stroke();
            }

            this.g.beginPath();
            this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
            this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);
            this.g.stroke();

            this.g.lineWidth = 2;
            this.g.beginPath();
            this.g.strokeStyle = this.o.fgColor;
            this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
            this.g.stroke();

            return false;
          }
        }
      });
      /* END JQUERY KNOB */



    });




  </script>



  <script type="text/javascript">
    var base_url = "<?php echo base_url(); ?>";
  </script>
  <!-- ./wrapper -->


  <!-- jQuery 3 -->

  <!-- Bootstrap 3.3.7 -->
  <script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- SlimScroll -->
  <script src="<?php echo base_url(); ?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
  <!-- FastClick -->
  <script src="<?php echo base_url(); ?>assets/bower_components/fastclick/lib/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="<?php echo base_url(); ?>assets/dist/js/adminlte.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>

  <!-- jQuery Knob -->
  <script src="<?php echo base_url(); ?>assets/bower_components/jquery-knob/js/jquery.knob.js"></script>



</body>

</html>