<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <div class="col-md-2">
        <?php if(verificarPermissao('aCliente')){ ?>
        <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/adicionar" type="button" class="btn btn-block btn-default btn-flat">Adicionar</a>
        <?php } ?>
      </div>          
    </div>
    <div class="box-body">
      <div class="container-fluid">
        <table id="tableCliente" class="table" width="100%">
          <thead>
            <tr>                                            
              <th>Nome</th>
              <th>Celular</th>
              <th>Cpf</th>
              <th>E-mail</th>
              <th>Ações</th>
            </tr>
          </thead>
          <tbody>

          </tbody>
          <tfoot>
            <tr>                     
              <th>Nome</th>
              <th>Celular</th>
              <th>Cpf</th>
              <th>Local</th>
              <th>Ações</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</section>
