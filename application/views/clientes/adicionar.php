<section class="content">
  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="<?php echo current_url(); ?>" method="post">
          <div class="box-body">

            <div class="form-group">
              <label for="loja" class="col-sm-2 control-label">Lojas </label>
              <div class="col-sm-5">              
                <select id="lojas" class="form-control select2" multiple="multiple" data-placeholder="Selecione as lojas" name="lojas[]" disabled="disabled">
                  <?php 
                  foreach ($lojas as $s) {
                    echo "<option value='".strtoupper($s['lojas'])."' selected>".strtoupper($s['lojas'])."</option>";
                  }
                  ?>                        
                </select>
              </div>
            </div> 

            <div class="form-group">
              <label for="cliente_tipo" class="col-sm-2 control-label">Tipo</label>
              <div class="col-sm-5">
                            
                <select required class="form-control" name="cliente_tipo" id="tipo">
                    <option value='1'>Pessoa Física</option>
                    <option value='2'>Pessoa Jurídica</option>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="cliente_cpf_cnpj" class="col-sm-2 control-label">CPF/CNPJ</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" name="cliente_cpf_cnpj" id="cpfCnpj" value="<?php echo set_value('cliente_cpf_cnpj'); ?>" placeholder="CPF/CNPJ">
              </div>
            </div>

            <div class="form-group">
              <label for="cliente_rg_ie" class="col-sm-2 control-label">RG/IE</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" name="cliente_rg_ie" id="rgIe" value="<?php echo set_value('cliente_rg_ie'); ?>" placeholder="RG/IE">
              </div>
            </div>

            <div class="form-group">
              <label for="cliente_nome" class="col-sm-2 control-label">Nome</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" id="nome" name="cliente_nome" value="<?php echo set_value('cliente_nome'); ?>" placeholder="Nome">
              </div>
            </div> 

            <div class="form-group">
              <label for="cliente_data_nasc" class="col-sm-2 control-label">Data Nascimento</label>
              <div class="col-sm-5">              
                <input type="date" class="form-control" id="nome" name="cliente_data_nasc" value="<?php echo set_value('cliente_data_nasc'); ?>" placeholder="Data Nascimento">
              </div>
            </div> 

            <div class="form-group">
              <label for="cliente_cep" class="col-sm-2 control-label">CEP</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" id="cep" name="cliente_cep" value="<?php echo set_value('cliente_cep'); ?>" placeholder="Cep">
              </div>
            </div>  

            <div class="form-group">
              <label for="cliente_endereco" class="col-sm-2 control-label">Endereço</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" id="endereco" name="cliente_endereco" value="<?php echo set_value('cliente_endereco'); ?>" placeholder="Endereço">
              </div>
            </div> 
            
            <div class="form-group">
              <label for="cliente_bairro" class="col-sm-2 control-label">Bairro</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" id="bairro" name="cliente_bairro" value="<?php echo set_value('cliente_bairro'); ?>" placeholder="Bairro">
              </div>
            </div>

            <div class="form-group">
              <label for="cliente_estado" class="col-sm-2 control-label">Estado</label>
              <div class="col-sm-5">              
                
                <select class="form-control" name="cliente_estado" id="estado">
                  <option value="">Selecione</option>
                  <?php foreach ($estados as $estado) { ?>
                    <option value="<?php echo $estado->sigla; ?>"><?php echo $estado->nome; ?> </option>
                  <?php } ?>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="cliente_cidade" class="col-sm-2 control-label">Cidade</label>
              <div class="col-sm-5">              
                <select class="form-control" name="cliente_cidade" id="cidade">
                  <option value="">Escolha um estado</option>                                      
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="cliente_numero" class="col-sm-2 control-label">Número</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" id="cliente_numero" name="cliente_numero" value="<?php echo set_value('cliente_numero'); ?>" placeholder="Número">
              </div>
            </div>       

            <div class="form-group">
              <label for="cliente_complemento" class="col-sm-2 control-label">Complemento</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" id="cliente_complemento" name="cliente_complemento" value="<?php echo set_value('cliente_complemento'); ?>" placeholder="Complemento">
              </div>
            </div>

            <div class="form-group">
              <label for="cliente_telefone" class="col-sm-2 control-label">Telefone</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" id="telefone"  name="cliente_telefone" value="<?php echo set_value('cliente_telefone'); ?>" placeholder="Telefone">
              </div>
            </div>

            <div class="form-group">
              <label for="cliente_celular" class="col-sm-2 control-label">Celular</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="celular" name="cliente_celular" value="<?php echo set_value('cliente_celular'); ?>" placeholder="Celular">
              </div>
            </div>

            <div class="form-group">
              <label for="cliente_email" class="col-sm-2 control-label">E-mail</label>
              <div class="col-sm-5">                
                <input type="text" class="form-control" id="email" name="cliente_email" value="<?php echo set_value('cliente_email'); ?>" placeholder="E-mail">
              </div>
            </div>

            <div class="form-group">
              <label for="cliente_face" class="col-sm-2 control-label">Facebook</label>
              <div class="col-sm-5">                
                <input type="text" class="form-control" id="facebook" name="cliente_face" value="<?php echo set_value('cliente_face'); ?>" placeholder="Facebook">
              </div>
            </div>

            <div class="form-group">
              <label for="cliente_insta" class="col-sm-2 control-label">Instagram</label>
              <div class="col-sm-5">                
                <input type="text" class="form-control" id="instagram" name="cliente_insta" value="<?php echo set_value('cliente_insta'); ?>" placeholder="Instagram">
              </div>
            </div>

            <div class="form-group">
              <label for="cliente_tipo" class="col-sm-2 control-label">Situação</label>
              <div class="col-sm-5">                            
                <select  class="form-control" name="cliente_ativo" id="cliente_ativo">
                  <option value='1'>Ativo</option>
                  <option value='0'>Inativo</option>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="cliente_app" class="col-sm-2 control-label">Acesso Aplicativo</label>
              <div class="col-sm-5">                            
                <select  class="form-control" name="cliente_app" id="cliente_app">
                  <option value='0'>Não</option>
                  <option value='1'>Sim</option>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="cliente_categoria" class="col-sm-2 control-label">Categoria</label>
              <div class="col-sm-5">              
                
                <select class="form-control" name="cliente_categoria" id="cliente_categoria">
                  <option value="">Selecione</option>
                  <?php foreach ($categoria as $c) { ?>
                    <option value="<?php echo $c->categoria_clie_id; ?>"><?php echo $c->categoria_clie_descricao; ?> </option>
                  <?php } ?>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="cliente_limite_cred_cliente" class="col-sm-2 control-label">R$ limite credito*</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control money" name="cliente_limite_cred_cliente" id="cliente_limite_cred_cliente" value="<?php echo set_value('cliente_limite_cred_cliente'); ?>" placeholder="limite crédito">
              </div>
            </div> 

            <div class="form-group">
              <label for="cliente_limite_dias_cliente" class="col-sm-2 control-label">limite de dias*</label>
              <div class="col-sm-5">                            
                <select  class="form-control" name="cliente_limite_dias_cliente" id="cliente_limite_dias_cliente">
                <option value=''>Selecione</option>
                  <option value='1'>1</option>
                  <option value='5'>5</option>
                  <option value='7'>7</option>
                  <option value='10'>10</option>
                  <option value='15'>15</option>
                  <option value='20'>20</option>
                </select>
              </div>
            </div>
            
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>" class="btn btn-default">Voltar</a>
            <button type="submit" class="btn btn-primary pull-right"><?php echo ucfirst($this->uri->segment(2)); ?></button>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
  </div>
</section>

