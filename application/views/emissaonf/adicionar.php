<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <form class="form-horizontal" action="<?= base_url('emissao/add') ?>" method="POST">
                    <div class="box-body">

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="cliente_nome" class="col-sm-4 control-label">Cliente<span style="color: red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="cliente_nome" id="cliente_nome" placeholder="Cliente" required>
                                    <input type="hidden" id="cliente_id" name="cliente_id">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="usuario_nome" class="col-sm-4 control-label">Vendedor<span style="color: red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="usuario_nome" id="usuario_nome" placeholder="Vendedor" required value="<?= $this->session->userdata('usuario_nome') ?>">
                                    <input type="hidden" id="usuario_id" name="usuario_id" value="<?= $this->session->userdata('usuario_id') ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="operacao" class="col-sm-4 control-label">Tipo da Nota Fiscal<span style="color: red;">*</span></label>
                                <div class="col-sm-8">
                                    <select required class="form-control" name="operacao" id="operacao">
                                        <option value="">Selecione</option>
                                        <option value="0">Entrada</option>
                                        <option value="1">Saída</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="modelo" class="col-sm-4 control-label">Modelo<span style="color: red;">*</span></label>
                                <div class="col-sm-8">
                                    <select required class="form-control" name="modelo" id="modelo">
                                        <option value="">Selecione</option>
                                        <option value="1">NF-e</option>
                                        <option value="2">NFC-e Cupom</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="consumidor_final" class="col-sm-4 control-label">Consumidor Final &lpar;Opcional&rpar;</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="consumidor_final" id="consumidor_final">
                                        <option value="">Selecione</option>
                                        <option value="0">Normal</option>
                                        <option value="1">Consumidor final</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="natureza_operacao" class="col-sm-4 control-label">Natureza Operacão<span style="color: red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="natureza_operacao" id="natureza_operacao" placeholder="Natureza Operacão" required>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="finalidade" class="col-sm-4 control-label">Forma de Emissão<span style="color: red;">*</span></label>
                                <div class="col-sm-8">
                                    <select required class="form-control" name="finalidade" id="finalidade">
                                        <option value="">Selecione</option>
                                        <option value="1">NF-e Normal</option>
                                        <option value="4">Devolução Mercadoria</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="pagamento" class="col-sm-4 control-label">Forma de pagamento<span style="color: red;">*</span></label>
                                <div class="col-sm-8">
                                    <select required class="form-control" name="pagamento" id="pagamento">
                                        <option value="">Selecione</option>
                                        <option value="0">Pagamento à vista</option>
                                        <option value="1">Pagamento a prazo</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="presenca" class="col-sm-4 control-label">Presença do comprador<span style="color: red;">*</span></label>
                                <div class="col-sm-8">
                                    <select required class="form-control" name="presenca" id="presenca">
                                        <option value="">Selecione</option>
                                        <option value="0">Não se aplica (por exemplo, Nota Fiscal complementar ou de ajuste)</option>
                                        <option value="1">Operação presencial</option>
                                        <option value="2">Operação não presencial, pela Internet</option>
                                        <option value="3">Operação não presencial, pela Internet</option>
                                        <option value="4">NFC-e em operação com entrega a domicílio</option>
                                        <option value="9">Operação não presencial, outros</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="modalidade_frete" class="col-sm-4 control-label">Modalidade do Frete<span style="color: red;">*</span></label>
                                <div class="col-sm-8">
                                    <select required class="form-control" name="modalidade_frete" id="modalidade_frete">
                                        <option value="">Selecione</option>
                                        <option value="0">Por conta do emitente</option>
                                        <option value="1">Por conta do destinatário/remetente</option>
                                        <option value="2">Por conta de terceiros</option>
                                        <option value="9">Sem frete</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="nfe_referenciada" class="col-sm-4 control-label">Nº Chave de acesso da NF-e emitida anteriormente</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="nfe_referenciada" id="nfe_referenciada" placeholder="Nº Chave de acesso da NF-e">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="fiscal_cefop_devolucao" class="col-md-4 control-label">Nº CEFOP</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="fiscal_cefop_devolucao" id="fiscal_cefop_devolucao" placeholder="Nº CEFOP">
                                </div>
                            </div>
                        </div>

                        &nbsp;

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="volume" class="col-sm-4 control-label">Quantidade</label>
                                <div class="col-sm-8">
                                    <!-- <i class="fa fa-truck"></i> -->
                                    <input type="text" class="form-control" name="volume" id="volume" placeholder="Quantidade">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="especie" class="col-sm-4 control-label">Espécie</label>
                                <div class="col-sm-8">
                                    <!-- <i class="fa fa-truck"></i> -->
                                    <input type="text" class="form-control" name="especie" id="especie" placeholder="Espécie">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="peso_bruto" class="col-sm-4 control-label">Peso Bruto</label>
                                <div class="col-sm-8">
                                    <!-- <i class="fa fa-truck"></i> -->
                                    <input type="text" class="form-control" name="peso_bruto" id="peso_bruto" placeholder="Peso Bruto">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="peso_liquido" class="col-sm-4 control-label">Peso Líquido</label>
                                <div class="col-sm-8">
                                    <!-- <i class="fa fa-truck"></i> -->
                                    <input type="text" class="form-control" name="peso_liquido" id="peso_liquido" placeholder="Peso Líquido">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="frete" class="col-sm-4 control-label">Frete</label>
                                <div class="col-sm-8">
                                    <!-- <i class="fa fa-truck"></i> -->
                                    <input type="text" class="form-control" name="frete" id="frete" placeholder="Frete">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="desconto" class="col-sm-4 control-label">Desconto</label>
                                <div class="col-sm-8">
                                    <!-- <i class="fa fa-truck"></i> -->
                                    <input type="text" class="form-control" name="desconto" id="desconto" placeholder="Desconto">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="total" class="col-sm-4 control-label">Total</label>
                                <div class="col-sm-8">
                                    <!-- <i class="fa fa-truck"></i> -->
                                    <input type="text" class="form-control" name="total" id="total" placeholder="Total">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="despesas_acessorias" class="col-sm-4 control-label">Despesas acessorias</label>
                                <div class="col-sm-8">
                                    <!-- <i class="fa fa-truck"></i> -->
                                    <input type="text" class="form-control" name="despesas_acessorias" id="despesas_acessorias" placeholder="Despesas acessorias">
                                </div>
                            </div>
                        </div>


                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="informacoes_complementares" class="col-sm-2 control-label">Informacões Complementares</label>
                                <div class="col-sm-10">
                                    <!-- <i class="fa fa-truck"></i> -->
                                    <input type="text" class="form-control" name="informacoes_complementares" id="informacoes_complementares" placeholder="Informacões Complementares">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="<?= base_url('emissao') ?>" class="btn btn-default">Voltar</a>
                        <button type="submit" class="btn btn-primary pull-right">Adicionar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>