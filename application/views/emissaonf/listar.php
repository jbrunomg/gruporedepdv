<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <div class="col-md-2">
            <?php if (verificarPermissao('aFiscal')) : ?>
              <a href="<?= base_url('emissao/adicionar'); ?>" type="button" class="btn btn-block btn-default btn-flat">Emitir Nota Fiscal</a>
            <?php endif ?>
          </div>
        </div>
        <!-- <?= var_dump($dados) ?> -->
        <div class="box-body">
          <div class="container-fluid">
            <table id="example" class="table" width="100%">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Data da Emissão</th>
                  <th>Cliente</th>
                  <th>Modelo</th>
                  <th>Nº</th>
                  <th>Tipo</th>
                  <th>Total</th>
                  <th>Ações</th>
                </tr>
              </thead>
              <tbody>
                <!-- <?= var_dump($dados) ?> -->
                <?php foreach ($dados as $nota) : ?>
                  <tr>
                    <td> <?= $nota->fiscal_nf_id; ?></td>
                    <td> <?= $nota->fiscal_nf_data_Fiscal; ?></td>
                    <td> <?= $nota->cliente_nome; ?></td>
                    <td> <?= $nota->fiscal_nf_finalidade == 1 ? 'Normal' : ($nota->fiscal_nf_finalidade == 4 ? 'Devolução/Retorno' : 'Cancelado') ?></td>
                    <td> <?= $nota->fiscal_nf_fiscal_nfe; ?></td>
                    <td> <?= $nota->fiscal_nf_modelo == 1 ? 'NF-e' : 'NFC-e' ?></td>
                    <td> <?= $nota->fiscal_nf_total ?></td>
                    <td>
                      <?php if (verificarPermissao('vFiscal')) : ?>
                        <?php if ($nota->fiscal_nf_status_nota == 1 || $nota->fiscal_nf_status_nota == 2) : ?>
                          <a href="<?= $nota->fiscal_nf_fiscal_danfe ?>" target="_blank" style="color:black" title="Visualizar DANFE">
                            <i class="fa fa-barcode" aria-hidden="true"></i>
                          </a>
                          <a href="<?= base_url($this->uri->segment(1) .  '/downloadXml/' . $nota->fiscal_nf_id) ?>" title="Download XML" style="color:black">
                            <i class="fa fa-download" aria-hidden="true"></i>
                          </a>
                          <?php if ($nota->fiscal_nf_status_nota != 2) : ?>
                            <a class="a-cancelar" href="" role="button" data-toggle="modal" data-target="#modal-cancelar" fiscal="<?= $nota->fiscal_nf_id ?>" style="color: #a94442;" title="Cancelar Nota Fiscal">
                              <i class="fa fa-ban" aria-hidden="true"></i>
                            </a>
                          <?php endif ?>

                        <?php else : ?>
                          <!-- <a href="<?= base_url('emissao/visualizar/' . $nota->fiscal_nf_id) ?>" style="color:black" title="Detalhes">
                          <i class="fa fa-eye" aria-hidden="true"></i>
                        </a> -->
                        <?php endif ?>
                      <?php endif ?>

                      <?php if (verificarPermissao('eFiscal') and $nota->fiscal_nf_status_nota == 0) : ?>
                        <a href="<?= base_url() . $this->uri->segment(1) ?>/editar/<?= $nota->fiscal_nf_id ?>" data-toggle="tooltip" title="Editar">
                          <i class="fa fa-edit"></i>
                        </a>
                      <?php endif ?>

                      <?php if (verificarPermissao('dFiscal') and $nota->fiscal_nf_status_nota == 0) : ?>
                        <a style="cursor:pointer;" title="Deletar" data-toggle="modal" data-target="#excluir" onclick="excluir('<?= base_url() . $this->uri->segment(1) . "/excluir/" .  $nota->fiscal_nf_id ?>');"><i class="fa fa-trash text-danger"></i>
                        </a>
                      <?php endif ?>

                      <?php if ($nota->fiscal_nf_status_nota == 1 || $nota->fiscal_nf_status_nota == 2) : ?>
                        <a class="enviar" href="" data-toggle="modal" data-target="#enviar" title="Enviar" urlNota="<?= $nota->fiscal_nf_fiscal_danfe ?>"> <i class="fa fa-paper-plane-o" aria-hidden="true"></i></a>
                      <?php endif ?>
                    </td>
                  </tr>
                <?php endforeach ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<div class="modal fade" id="enviar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header text-white">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Enviar</h4>
      </div>
      <div class="modal-body bg-light">
        <div>
          <form class="form-horizontal" action="">
            <div class="col-xs-12">
              <div class="form-group">
                <label for="input-celular" class="col-sm-4 control-label">Número de whatsapp<span style="color: red;">*</span></label>
                <div class="col-sm-5">
                  <!-- <i class="fa fa-truck"></i> -->
                  <input type="text" class="form-control input_numero" name="input-celular" id="input-celular" placeholder="Digite DDD + Nº WhatsApp">
                  <input type="hidden" class="urlNota" name="id" id="urlNotaWhatsapp" />
                </div>
              </div>
            </div>

            <div class="col-xs-12">
              <div class="form-group" style="display: flex; justify-content: center;">
                <button class="btn btn-success" onclick="clickyClick()">Enivar por whatsapp</button>
              </div>
            </div>
          </form>
        </div>

        <!-- <div>
          <form class="form-horizontal" action="<?= base_url('emissao/enviarEmail') ?>" method="POST">
            <div class="col-xs-12">
              <div class="form-group">
                <label for="email" class="col-sm-4 control-label">Endereço de E-mail<span style="color: red;">*</span></label>
                <div class="col-sm-5">
                  <input type="text" class="form-control" name="email" id="email" placeholder="E-mail">
                  <input type="hidden" class="urlNota" name="id" />
                </div>
              </div>
            </div>

            <div class="col-xs-12">
              <div class="form-group" style="display: flex; justify-content: center;">
                <button type="submit" class="btn btn-info">Enivar por E-mail</button>
              </div>
            </div>
          </form>
        </div> -->

      </div>
      <div class="modal-footer">
        <button type="button" class="btn  btn-default pull-left" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modal-cancelar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header text-white">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Cancelar nota fiscal</h4>
      </div>
      <div class="modal-body bg-light">
        <h4 style="text-align: center">Deseja realmente cancelar esta nota fiscal?</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn  btn-default pull-left" data-dismiss="modal">Cancelar</button>
        <form action="<?= base_url('emissao/cancelar') ?>" method="POST">
          <input type="hidden" name="id_cancelar" id="id_cancelar">
          <button type="submit" class="btn btn-danger pull-rigth">Confirmar</button>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {
    $('#example').DataTable({
      "order": [
        [0, 'desc']
      ],
      "bDestroy": true
    });
  });
  $('.a-cancelar').click(function() {
    let id = $(this).attr('fiscal');
    $('#id_cancelar').val(id);
  });

  $('.enviar').click(function() {
    let url = $(this).attr('urlNota');
    $('.urlNota').val(url);
  });

  $('.input_numero').on('input', function() {
    $(this).val(function(index, value) {
      return value
        .replace(/[^0-9.]/g, '')
    });
  });

  function clickyClick() {
    let numero = $('#input-celular').val();
    let url = 'https://wa.me/55' + numero + '?text=Obrigado pela preferência!%0aSegue link de acesso para a nota fiscal de compra: ' + $('#urlNotaWhatsapp').val();
    window.open(url, '_blank');
    location.reload();
  }
</script>