<?= var_dump($dados) ?>
<?= var_dump($vendas) ?>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <div style="display: flex; justify-content: flex-end; gap: 10px;">
                        <?php if (verificarPermissao('aFiscal')) : ?>
                            <a href="<?= base_url('emissao/editar/' . $dados->fiscal_nf_id); ?>" type="button" class="btn btn-primary">Editar</a>
                            <a href="<?= base_url('emissao/imprimir'); ?>" type="button" class="btn btn-warning">Imprimir</a>
                        <?php endif ?>
                    </div>
                </div>
                <div class="box-body">

                </div>
            </div>
        </div>
    </div>
</section>