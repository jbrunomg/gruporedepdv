<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <form class="form-horizontal" action="<?= base_url('emissao/edt') ?>" method="POST">
                    <div class="box-body">
                        <!-- <?= var_dump($dados) ?> -->
                        <input type="hidden" name="fiscal_nf_id" value="<?= $dados->fiscal_nf_id ?>">

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="cliente_nome" class="col-sm-4 control-label">Cliente<span style="color: red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="cliente_nome" id="cliente_nome" placeholder="Cliente" required value="<?= $dados->cliente_nome ?>">
                                    <input type="hidden" id="cliente_id" name="cliente_id" value="<?= $dados->fiscal_nf_clientes_id ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="usuario_nome" class="col-sm-4 control-label">Vendedor<span style="color: red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="usuario_nome" id="usuario_nome" placeholder="Vendedor" required value="<?= $dados->usuario_nome ?>">
                                    <input type="hidden" id="usuario_id" name="usuario_id" value="<?= $dados->fiscal_nf_usuarios_id ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="operacao" class="col-sm-4 control-label">Tipo da Nota Fiscal<span style="color: red;">*</span></label>
                                <div class="col-sm-8">
                                    <select required class="form-control" name="operacao" id="operacao">
                                        <option value="" <?= $dados->fiscal_nf_operacao == '' ? 'selected' : '' ?>>Selecione</option>
                                        <option value="0" <?= $dados->fiscal_nf_operacao == 0 ? 'selected' : '' ?>>Entrada</option>
                                        <option value="1" <?= $dados->fiscal_nf_operacao == 1 ? 'selected' : '' ?>>Saída</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="modelo" class="col-sm-4 control-label">Modelo<span style="color: red;">*</span></label>
                                <div class="col-sm-8">
                                    <select required class="form-control" name="modelo" id="modelo">
                                        <option value="" <?= $dados->fiscal_nf_modelo == '' ? 'selected' : '' ?>>Selecione</option>
                                        <option value="1" <?= $dados->fiscal_nf_modelo == 1 ? 'selected' : '' ?>>NF-e</option>
                                        <option value="2" <?= $dados->fiscal_nf_modelo == 2 ? 'selected' : '' ?>>NFC-e Cupom</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="consumidor_final" class="col-sm-4 control-label">Consumidor Final &lpar;Opcional&rpar;</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="consumidor_final" id="consumidor_final">
                                        <option value="" <?= $dados->fiscal_nf_consumidor_final == '' ?  'selected' : '' ?>>Selecione</option>
                                        <option value="0" <?= $dados->fiscal_nf_consumidor_final == 0 ?  'selected' : '' ?>>Normal</option>
                                        <option value="1" <?= $dados->fiscal_nf_consumidor_final == 1 ?  'selected' : '' ?>>Consumidor final</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="natureza_operacao" class="col-sm-4 control-label">Natureza Operacão<span style="color: red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="natureza_operacao" id="natureza_operacao" placeholder="Natureza Operacão" required value="<?= $dados->fiscal_nf_natureza_operacao ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="finalidade" class="col-sm-4 control-label">Forma de Emissão<span style="color: red;">*</span></label>
                                <div class="col-sm-8">
                                    <select required class="form-control" name="finalidade" id="finalidade">
                                        <option value="" <?= $dados->fiscal_nf_finalidade == '' ? 'selected' : '' ?>>Selecione</option>
                                        <option value="1" <?= $dados->fiscal_nf_finalidade == 1 ? 'selected' : '' ?>>NF-e Normal</option>
                                        <option value="4" <?= $dados->fiscal_nf_finalidade == 4 ? 'selected' : '' ?>>Devolução Mercadoria</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="pagamento" class="col-sm-4 control-label">Forma de pagamento<span style="color: red;">*</span></label>
                                <div class="col-sm-8">
                                    <select required class="form-control" name="pagamento" id="pagamento">
                                        <option value="" <?= $dados->fiscal_nf_forma_pgto == '' ? 'selected' : '' ?>>Selecione</option>
                                        <option value="0" <?= $dados->fiscal_nf_forma_pgto == 0 ? 'selected' : '' ?>>Pagamento à vista</option>
                                        <option value="1" <?= $dados->fiscal_nf_forma_pgto == 1 ? 'selected' : '' ?>>Pagamento a prazo</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="presenca" class="col-sm-4 control-label">Presença do comprador<span style="color: red;">*</span></label>
                                <div class="col-sm-8">
                                    <select required class="form-control" name="presenca" id="presenca">
                                        <option value="" <?= $dados->fiscal_nf_presenca == '' ? 'selected' : '' ?>>Selecione</option>
                                        <option value="0" <?= $dados->fiscal_nf_presenca == 0 ? 'selected' : '' ?>>Não se aplica (por exemplo, Nota Fiscal complementar ou de ajuste)</option>
                                        <option value="1" <?= $dados->fiscal_nf_presenca == 1 ? 'selected' : '' ?>>Operação presencial</option>
                                        <option value="2" <?= $dados->fiscal_nf_presenca == 2 ? 'selected' : '' ?>>Operação não presencial, pela Internet</option>
                                        <option value="3" <?= $dados->fiscal_nf_presenca == 3 ? 'selected' : '' ?>>Operação não presencial, pela Internet</option>
                                        <option value="4" <?= $dados->fiscal_nf_presenca == 4 ? 'selected' : '' ?>>NFC-e em operação com entrega a domicílio</option>
                                        <option value="9" <?= $dados->fiscal_nf_presenca == 9 ? 'selected' : '' ?>>Operação não presencial, outros</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="modalidade_frete" class="col-sm-4 control-label">Modalidade do Frete<span style="color: red;">*</span></label>
                                <div class="col-sm-8">
                                    <select required class="form-control" name="modalidade_frete" id="modalidade_frete">
                                        <option value="" <?= $dados->fiscal_nf_modalidade_frete == '' ? 'selected' : '' ?>>Selecione</option>
                                        <option value="0" <?= $dados->fiscal_nf_modalidade_frete == 0 ? 'selected' : '' ?>>Por conta do emitente</option>
                                        <option value="1" <?= $dados->fiscal_nf_modalidade_frete == 1 ? 'selected' : '' ?>>Por conta do destinatário/remetente</option>
                                        <option value="2" <?= $dados->fiscal_nf_modalidade_frete == 2 ? 'selected' : '' ?>>Por conta de terceiros</option>
                                        <option value="9" <?= $dados->fiscal_nf_modalidade_frete == 9 ? 'selected' : '' ?>>Sem frete</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <!-- <div class="col-xs-6">
                            <div class="form-group">
                                <label for="nfe_referenciada" class="col-sm-4 control-label">Nº Chave de acesso da NF-e emitida anteriormente<span style="color: red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="nfe_referenciada" id="nfe_referenciada" placeholder="Nº Chave de acesso da NF-e" required value="<?= $dados->fiscal_nf_nfe_referenciada ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="fiscal_cefop_devolucao" class="col-sm-4 control-label">Nº CEFOP</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="fiscal_cefop_devolucao" id="fiscal_cefop_devolucao" placeholder="Nº CEFOP" value="<?= $dados->fiscal_nf_fiscal_cefop_devolucao ?>">
                                </div>
                            </div>
                        </div> -->


                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="nfe_referenciada" class="col-sm-4 control-label">Nº Chave de acesso da NF-e emitida anteriormente</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="nfe_referenciada" id="nfe_referenciada" placeholder="Nº Chave de acesso da NF-e" value="<?= $dados->fiscal_nf_nfe_referenciada ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="fiscal_cefop_devolucao" class="col-sm-4 control-label">Nº CEFOP</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="fiscal_cefop_devolucao" id="fiscal_cefop_devolucao" placeholder="Nº CEFOP" value="<?= $dados->fiscal_nf_fiscal_cefop_devolucao ?>">
                                </div>
                            </div>
                        </div>

                        &nbsp;

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="volume" class="col-sm-4 control-label">Quantidade</label>
                                <div class="col-sm-8">
                                    <!-- <i class="fa fa-truck"></i> -->
                                    <input type="text" class="form-control input_numero" name="volume" id="volume" placeholder="Quantidade" value="<?= $dados->fiscal_nf_volume ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="especie" class="col-sm-4 control-label">Espécie</label>
                                <div class="col-sm-8">
                                    <!-- <i class="fa fa-truck"></i> -->
                                    <input type="text" class="form-control" name="especie" id="especie" placeholder="Espécie" value="<?= $dados->fiscal_nf_especie ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="peso_bruto" class="col-sm-4 control-label">Peso Bruto</label>
                                <div class="col-sm-8">
                                    <!-- <i class="fa fa-truck"></i> -->
                                    <input type="text" class="form-control input_numero" name="peso_bruto" id="peso_bruto" placeholder="Peso Bruto" value="<?= $dados->fiscal_nf_peso_bruto ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="peso_liquido" class="col-sm-4 control-label">Peso Líquido</label>
                                <div class="col-sm-8">
                                    <!-- <i class="fa fa-truck"></i> -->
                                    <input type="text" class="form-control input_numero" name="peso_liquido" id="peso_liquido" placeholder="Peso Líquido" value="<?= $dados->fiscal_nf_peso_liquido ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="frete" class="col-sm-4 control-label">Frete</label>
                                <div class="col-sm-8">
                                    <!-- <i class="fa fa-truck"></i> -->
                                    <input type="text" class="form-control input_numero" name="frete" id="frete" placeholder="Frete" value="<?= $dados->fiscal_nf_peso_liquido ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="desconto" class="col-sm-4 control-label">Desconto</label>
                                <div class="col-sm-8">
                                    <!-- <i class="fa fa-truck"></i> -->
                                    <input type="text" class="form-control input_numero" name="desconto" id="desconto" placeholder="Desconto" value="<?= $dados->fiscal_nf_valor_desconto ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="total" class="col-sm-4 control-label">Total</label>
                                <div class="col-sm-8">
                                    <!-- <i class="fa fa-truck"></i> -->
                                    <input type="text" class="form-control input_numero" name="total" id="total" placeholder="Total" value="<?= $dados->fiscal_nf_total ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="despesas_acessorias" class="col-sm-4 control-label">Despesas acessorias</label>
                                <div class="col-sm-8">
                                    <!-- <i class="fa fa-truck"></i> -->
                                    <input type="text" class="form-control" name="despesas_acessorias" id="despesas_acessorias" placeholder="Despesas acessorias" value="<?= $dados->fiscal_nf_despesas_acessorias ?>">
                                </div>
                            </div>
                        </div>


                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="informacoes_complementares" class="col-sm-2 control-label">Informacões Complementares</label>
                                <div class="col-sm-10">
                                    <!-- <i class="fa fa-truck"></i> -->
                                    <input type="text" class="form-control" name="informacoes_complementares" id="informacoes_complementares" placeholder="Informacões Complementares" value="<?= $dados->fiscal_nf_informacoes_complementares ?>">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="box-footer" style="border-top: none; display: flex; justify-content: center;">
                        <button type="submit" class="btn btn-primary">Alterar dados de emissão</button>
                    </div>
                </form>

                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Adicionar Vendas</h3>
                    </div>
                    <div class="box-body">
                        <form class="form-horizontal" action="<?= base_url('emissao/adicionarVenda') ?>" method="POST">
                            <input type="hidden" name="fiscal_nf_id" id="fiscal_nf_id" value="<?= $dados->fiscal_nf_id ?>">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label for="venda_id" class="col-sm-4 control-label">Número da Venda<span style="color: red;">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="venda_id" id="venda_id" placeholder="Número da Venda" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Adicionar venda</button>

                                    <!-- <a href="<?= base_url('emissao/visualizar/' . $dados->fiscal_nf_id)?>" type="button" id="btn_visualizar_nfe" class="btn" style="color: white; background-color: black;">Visualizar NF-e</a> -->

                                    <button data-toggle="modal" data-target="#modal_nf" type="button" id="btn_emitir_nfe" class="btn btn-success">Emitir NF-e</button>
                                </div>
                            </div>
                        </form>
                        <table class="table" width="100%">
                            <thead>
                                <tr>
                                    <th>Número da venda</th>
                                    <th>Valor da venda</th>
                                    <th>Valor do desconto</th>
                                    <th>Ações</th>
                                    <th>Sub-total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- <?= var_dump($vendas) ?> -->
                                <input type="hidden" id="qtd_vendas" value="<?= sizeof($vendas)?>">
                                <?php $valor_total = 0;
                                foreach ($vendas as $venda) : $valor_total += $venda->valorTotal - $venda->desconto ?>
                                    <tr>
                                        <td><?= $venda->idVendas ?></td>
                                        <td>R&#36; <?= number_format($venda->valorTotal, 2, ',', '.') ?></td>
                                        <td>R&#36; <?= number_format($venda->desconto, 2, ',', '.') ?></td>
                                        <td>
                                            <?php if (verificarPermissao('dFiscal')) : ?>
                                                <a style="cursor:pointer;" title="Desvincular" data-toggle="modal" data-target="#excluir" onclick="excluir('<?php echo base_url() . $this->uri->segment(1) . "/excluirVenda/" . $venda->idVendas . "/" . $dados->fiscal_nf_id; ?>');">
                                                    <i class="fa fa-trash text-danger"></i>
                                                </a>
                                            <?php endif ?>
                                        </td>
                                        <td>R&#36; <?= number_format($venda->valorTotal - $venda->desconto, 2, ',', '.') ?></td>
                                    </tr>
                                <?php endforeach ?>
                                <td colspan="4" style="text-align: right"><strong>Total:</strong></td>
                                <td><strong>R&#36; <?= number_format($valor_total, 2, ',', '.'); ?></strong>
                            </tbody>
                        </table>

                    </div>

                    <div class="box-footer">
                        <a href="<?= base_url('emissao') ?>" class="btn btn-default">Voltar</a>
                    </div>
                </div>
            </div>
        </div>
</section>

<div class="modal fade" id="modal_nf" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header text-white">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Emitir Nota Fiscal</h4>
            </div>
            <div class="modal-body bg-light">
                <p>Deseja realmente emitir esta nota?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn  btn-default pull-left" data-dismiss="modal">Cancelar</button>
                <form action="<?= base_url('emissao/emitirNotaFiscal') ?>" method="post">
                    <input type="hidden" name="nf_id" value="<?= $dados->fiscal_nf_id ?>">
                    <button type="submit" class="btn btn-success pull-right">Emitir</button>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $('#qtd_vendas').val() < 1 ? $('#btn_emitir_nfe').hide() : $('#btn_emitir_nfe').show();
    // $('#finalidade').change(function() {
    //     let valor = $(this).val();

    //     if (valor == 4) {
    //         $('#fiscal_chave_devolucao').show();
    //     } else {
    //         $('#fiscal_chave_devolucao').hide();
    //     }
    // });

    $('.input_numero').on('input', function() {
        $(this).val(function(index, value) {
            return value
                .replace(/[^0-9.]/g, '')
                .replace(/(\..*)\./g, '$1');
        });
    });
</script>