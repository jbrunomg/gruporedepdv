<section class="content">
  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="<?php echo current_url(); ?>" method="post">
        <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $dados[0]->codigo_prod_id; ?>" placeholder="id">
          <div class="box-body">

          <div class="form-group">
              <label for="codigo_prod_categoria_id" class="col-sm-2 control-label">Categoria*</label>
              <div class="col-sm-5">
                <select disabled id="codigo_prod_categoria_id" class="form-control" data-placeholder="Selecione o grupo de produtos" name="codigo_prod_categoria_id" required="required">
                    <?php foreach ($grupo as $g) { ?>
                  <option value='<?php echo $g->categoria_prod_id; ?>' <?php echo ($g->categoria_prod_id == $dados[0]->codigo_prod_categoria_id) ? 'selected' : '';?>><?php echo $g->categoria_prod_descricao; ?> </option>
                    <?php } ?>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="codigo_prod_codigo" class="col-sm-2 control-label">Código do Produto</label>
              <div class="col-sm-5">              
                <input disabled type="text" class="form-control" name="codigo_prod_codigo" id="codigo_prod_codigo" value="<?php echo $dados[0]->codigo_prod_codigo; ?>">
              </div>

            </div>
            <div class="form-group">
              <label for="codigo_prod_descricao" class="col-sm-2 control-label">Descrição</label>
              <div class="col-sm-5">
                <input disabled type="text" class="form-control" name="codigo_prod_descricao" id="codigo_prod_descricao" value="<?php echo $dados[0]->codigo_prod_descricao; ?>">
              </div>
            </div>

          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>" class="btn btn-default">Voltar</a>
            
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
  </div>
</section>