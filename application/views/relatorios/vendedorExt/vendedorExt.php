
<section class="content">
	<div class="row">
		<div class="col-md-3">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios Rápidos</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					
				

				</div>
			</div>
		</div>

		<div class="col-md-9">


			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios Customizáveis - Produtos/Estoque</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-12">
						<div class="row">
							<form action="<?php echo base_url()?>index.php/relatorios/produtosCustom" method="post" target="_blank">
							
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label>Grupo</label>		
										<select id="grupo_prod_pedido" class="form-control" data-placeholder="Selecione o grupo de produtos" name="produto_categoria_id"  required="required">
						                  <option value="">Selecione</option>
						                  <option value="todos">TODOS</option>
						                  <?php foreach ($grupo as $g) { ?>
						                  <option value='<?php echo $g->categoria_prod_id; ?>'><?php echo $g->categoria_prod_descricao; ?> </option>
						                  <?php } ?>                       
						                </select>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Produto</label>		
					                      <select class="select2" style="width: 100%;" name="produto_desc" id="produto_desc" >
					                        <option value="">Selecionar um produto</option>                       
					                      </select>  
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Ordenar</label>		
				                      	<select class="form-control" name="tipo_ordenacao">
											<option value="0">Nome Produto</option>								
											<option value="1">Cód Produto</option>
											<option value="2">Estoque A-Z</option>
											<option value="3">Estoque Z-A</option>
										</select>  
									</div>
								</div>
							</div>
							<div class="row">
		

								<div class="col-md-4">
									<div class="form-group">
										<label>Exibir estoque zerado</label><br>		
						                  <label>
						                     <div class="iradio_flat-green" style="position: relative;"><input type="radio" name="estoqueProduto" value="0" class="flat-red" checked="" ></ins></div> NÂO
						                  </label>
						                  						                  <label>
						                     <div class="iradio_flat-green" style="position: relative;"><input type="radio" name="estoqueProduto" value="1" class="flat-red" ></ins></div> SIM
						                  </label>
									</div>
				     			</div>
			
				

							</div>

						
							<div class="row no-print text-center" style="margin-top: 40px">
								<div class="col-md-12">
									<button type="submit" class="btn btn-primary" style="margin-right: 50px;  width: 30%;">
										<i class="fa fa-print"></i> Imprimir
									</button>
								</div>
							</div>
						    </form>
						</div>
					</div>
				</div>
			</div>



			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios Customizáveis - Catalogo Produtos</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-12">
						<div class="row">
							<form action="<?php echo base_url()?>index.php/relatorios/produtosCatalogoCustom" method="post" target="_blank">
							
							<div class="row">
								<div class="col-md-3">
									<div class="form-group">
										<label>Grupo</label>		
										<select id="grupo_prod_produto" class="form-control" data-placeholder="Selecione o grupo de produtos" name="produto_categoria_id"  required="required">
						                  <option value="">Selecione</option>
						                  <?php foreach ($grupo as $g) { ?>
						                  <option value='<?php echo $g->categoria_prod_id; ?>'><?php echo $g->categoria_prod_descricao; ?> </option>
						                  <?php } ?>                       
						                </select>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label>Produto</label>		
					                      <select class="select2" style="width: 100%;" name="produto_desc" id="rel_produto_desc" >
					                        <option value="">Selecionar um produto</option>                       
					                      </select>  
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label>Com Estoque</label>
										<select class="form-control" name="estoque">
											<option value="">Ambos</option>
											<option value="N">Não</option>									
											<option value="S">Sim</option>																						
										</select>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label>Ordenar</label>
										<select class="form-control" name="tipo_ordenacao">
											<option value="0">Nome Produto</option>								
											<option value="1">Cód Produto</option>
											<option value="2">Cód Produto (agrupado)</option>
											<option value="3">Estoque A-Z</option>
											<option value="4">Estoque Z-A</option>
										</select>  
									</div>
								</div>
							</div>

							<div class="col-xs-12" style="margin-left: 0"> 

							<!-- 	<div class="col-xs-2" style="margin-left: 0">
								  <label for="pc">Preço Custo?</label><br>
								  <input  id="pc" type="checkbox" name="pc" value="1" /> 
								</div> 
								<div class="col-xs-2" style="margin-left: 0">
								  <label for="pa">Preço Atacado?</label><br>
								  <input  id="pa" type="checkbox" name="pa" value="1" /> 
								</div>
								<div class="col-xs-2" style="margin-left: 0">
								  <label for="pv">Preço Varejo?</label><br>
								  <input  id="pv" type="checkbox" name="pv" value="1" /> 
								</div>
								<div class="col-xs-3" style="margin-left: 0">
								  <label for="pr">Preço Revenda?</label><br>
								  <input  id="pr" type="checkbox" name="pr" value="1" /> 
								</div>		-->					
								<div class="col-xs-2" style="margin-left: 0">
								  <label for="qtd">Quantidade?</label><br>
								  <input  id="qtd" type="checkbox" name="qtd" value="1" /> 
								</div>
						
							</div>

						
							<div class="row no-print text-center" style="margin-top: 40px">
								<div class="col-md-12">
									<button type="submit" class="btn btn-primary" style="margin-right: 50px;  width: 30%;">
										<i class="fa fa-print"></i> Imprimir
									</button>
								</div>
							</div>
						    </form>
						</div>
					</div>
				</div>
			</div>


	



		</div>

	</div>
</section>

