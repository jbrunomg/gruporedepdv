

<hr style="margin-top:-10px">
<h3 class="text-center" style="margin-top:-10px">Relatório de Compras Mês - <?php  echo  $data ?></h3>


<!--  <?php echo "<pre>"; var_dump($dados);  ?>  -->
<table>
	<thead>
		<tr>			
			<th style="font-size: 12px;" >Fornecedor</th>			
			<th style="font-size: 12px;" >Valor Total</th>
			<th style="font-size: 12px;" >Valor Pago</th>			
		</tr>
	</thead>
	<tbody>
		<?php $somaTotal = 0; ?>
		<?php foreach ($dados as $valor){
		$somaTotal +=  $valor->total; ?>
			 				
			<tr class="even">				
				<td><?php echo $valor->fornecedor_nome; ?></td>
				<td style="color: blue; font-weight: bold;"><?php echo 'R$ '.number_format($valor->total,2,',','.').'' ; ?></td>

				<?php 
				if (empty($pagamento)) {

				echo '<td style="color: blue; font-weight: bold;"> R$ 0,00 </td>'; 
				$somaTotalP = '0.00';
					
				} else {

					$somaTotalP = 0;
				    foreach ($pagamento as $valorp){
				    $somaTotalP +=  $valorp->total;	
				    if ($valorp->fornecedor_id == $valor->fornecedor_id ) { ?>

				<td style="color: blue; font-weight: bold;"><?php echo 'R$ '.number_format($valorp->total,2,',','.').'' ; ?></td> 
				    		
				<?php  } else { ?>	
					
				<td style="color: blue; font-weight: bold;"><?php echo 'R$ 0,00' ?></td> 

				<?php  } } } ?>	

			</tr>
		
		<?php } ?> 
	</tbody>




</table>

<br class="clear">
<br class="clear">

<table>
    <tfoot>
        <tr class="even">
        	<th colspan="3" style="font-size: 12px; text-align: right; color: blue; font-weight: bold;"></th>

        	<th  colspan="3" style="font-size: 12px; text-align: right; color: blue; font-weight: bold;">
        	 <strong>TOTAL : R$ <?php echo number_format($somaTotal,2,",","."); ?></strong>
        	</th>
        	<th colspan="3" style="font-size: 12px; text-align: right; color: blue; font-weight: bold;">
        	 <strong>TOTAL : R$ <?php echo number_format($somaTotalP,2,",","."); ?></strong>
        	</th>
        </tr>             
    </tfoot>
</table>
