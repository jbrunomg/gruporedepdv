
<section class="content">
	<div class="row">
		<div class="col-md-3">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios Rápidos</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					
					<div class="small-box bg-green">
						<a href="<?php echo base_url() ?>relatorios/compras/comprasMensal/atual" class="small-box-footer" target="_blank">
						<div class="inner">
							<h3><?php echo 'R$ '.number_format($totalMesAtual[0]->total,2,',','.').'' ; ?></h3>

							<p>Compra Mês Atual</p>
						</div>
						<div class="icon">
							<i class="ion ion-bag"></i>
						</div>
						</a>
					</div>

					<div class="small-box bg-blue">
						<a href="<?php echo base_url() ?>relatorios/compras/comprasMensal/anterior" class="small-box-footer" target="_blank">
						<div class="inner">
							<h3><?php echo 'R$ '.number_format($totalMesAnterior[0]->total,2,',','.').'' ;  ?></h3>

							<p>Compra Mês Anterior</p>
						</div>
						<div class="icon">
							<i class="ion ion-bag"></i>
						</div>
						</a>
					</div>

				</div>
			</div>
		</div>

		<div class="col-md-9">
			<form action="<?php echo base_url() ?>relatorios/comprasPagamentoCustomizadas" method='POST' target="_blank" >
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Relatórios Compras - Pagamento</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="col-md-12">
							<div class="row">

								<div class="col-md-12">
									<div class="form-group">
									  <label>Fornecedores</label>
										<select id="fornecedor_id" class="form-control" data-placeholder="Selecione o vendedor" name="fornecedor_id" >					                 
						                  <option value=''>TODOS FORNECEDORES</option>
						                  <?php foreach ($fornecedor as $v) { ?>
						                  <option value='<?php echo $v->fornecedor_id; ?>'><?php echo $v->fornecedor_nome; ?> </option>
						                  <?php } ?>                       
						                </select>
									</div>
								</div>
								
							</div>
<!-- 							<div class="row">										
								<div class="col-md-6">
									<div class="form-group">
										<label>Tipo Relatorio</label>
										<select class="form-control" name="tipo">
											<option value="1">Resumido</option>
											<option value="2">Detalhado</option>
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Posição Impresso</label>
										<select class="form-control" name="formato">
											<option value="A4-P">Retrato</option>
											<option value="A4-L">Paissagem</option>
										</select>
									</div>
							    </div>						
							</div> -->
				
						</div>
						<div class="row no-print text-center" style="margin-top: 40px">
							<div class="col-md-12">
								<button type="button" class="btn btn-default">
									<i class="fa fa-eraser"></i> Limpar
								</button>
								<button type="submit" class="btn btn-primary" style="margin-right: 5px;">
									<i class="fa fa-print"></i> Imprimir
								</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>

		<div class="col-md-9">
			<form action="<?php echo base_url() ?>relatorios/comprasCustomizadas" method='POST' >
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Relatórios Customizáveis</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Compra de</label>
										<div class="input-group date">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" class="form-control pull-right" id="datepickerVendaIncio" placeholder="dd/mm/aaaa" name="dataInicio">
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>até</label>
										<div class="input-group date">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" class="form-control pull-right" id="datepickerVendaFim" placeholder="dd/mm/aaaa" name="dataFim">
										</div>
									</div>
								</div>
							</div>
							<div class="row">										
								<div class="col-md-6">
									<div class="form-group">
										<label>Tipo Relatorio</label>
										<select class="form-control" name="tipo">
											<option value="1">Resumido</option>
											<option value="2">Detalhado</option>
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Posição Impresso</label>
										<select class="form-control" name="formato">
											<option value="A4-P">Retrato</option>
											<option value="A4-L">Paissagem</option>
										</select>
									</div>
							    </div>						
							</div>
				
						</div>
						<div class="row no-print text-center" style="margin-top: 40px">
							<div class="col-md-12">
								<button type="button" class="btn btn-default">
									<i class="fa fa-eraser"></i> Limpar
								</button>
								<button type="submit" class="btn btn-primary" style="margin-right: 5px;">
									<i class="fa fa-print"></i> Imprimir
								</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>

	</div>
</section>

