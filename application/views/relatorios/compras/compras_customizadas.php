

<hr style="margin-top:-10px"> 
<?php
$dataInicio = isset($infoRelatorio['dataInicio']) ? $infoRelatorio['dataInicio'] : null;
$dataFim = isset($infoRelatorio['dataFim']) ? $infoRelatorio['dataFim'] : null;

if ($dataInicio && $dataFim) {
    $titulo = 'De ' . $dataInicio . ' Até ' . $dataFim;
} else {
    $titulo = 'Relatório de Compras';
}
?></h3>

<h3 class="text-center" style="margin-top:-10px"><?php echo $titulo; ?></h3>
<!--  <?php echo "<pre>"; var_dump($dados);  ?>  -->
<table>
    <thead>
        <tr>            
            <th style="font-size: 12px;">Fornecedor</th>            
            <th style="font-size: 12px;">Valor Total</th>
            <th style="font-size: 12px;">Valor Pago</th>
            <th style="font-size: 12px;">Dívida</th>
            <th style="font-size: 12px;">Estoque</th>
            <th style="font-size: 12px;">Valor Estoque</th> 
        </tr>
    </thead>
    <tbody>
        <?php 
        $somaTotal = 0; 
        $somaPago = 0; 
        $estoqueTotal = 0;
        $valorEstoqueTotal = 0;
        ?>
        <?php foreach ($dados as $valor) :
            $somaTotal +=  $valor->valor_total_produtos;
            $somaPago += $valor->valor_pago; 
            $divida = $valor->valor_total_produtos - $valor->valor_pago;
            $estoqueTotal += $valor->quantidade_aparelhos_estoque;
            $valorEstoqueTotal += $valor->valor_total_estoque;
        ?>
        <tr class="even">                
            <td style="color: black; font-weight: bold;"><?php echo $valor->fornecedor; ?></td>
            <td style="color: black; font-weight: bold;"><?php echo 'R$ '.number_format($valor->valor_total_produtos, 2, ',', '.'); ?></td>
            <td style="color: black; font-weight: bold;"><?php echo 'R$ '.number_format($valor->valor_pago, 2, ',', '.'); ?></td>     
            <td style="color: black; font-weight: bold;"><?php echo 'R$ '.number_format($divida, 2, ',', '.'); ?></td>
            <td style="color: black; font-weight: bold;"><?php echo $valor->quantidade_aparelhos_estoque; ?></td>
            <td style="color: black; font-weight: bold;"><?php echo 'R$ '.number_format($valor->valor_total_estoque, 2, ',', '.'); ?></td>       
        </tr>
        <?php endforeach; ?>
        <!-- Espaço em branco -->
        <tr>
            <td colspan="6" style="height: 20px;"></td>
        </tr>
        <!-- Total -->
        <tr>               
            <td style="color: black; font-weight: bold;" colspan="1"><strong>TOTAL</strong></td>
            <td style="color: black; font-weight: bold;"><strong>R$ </strong><?php echo number_format($somaTotal, 2, ',', '.'); ?></td>
            <td style="color: black; font-weight: bold;"><strong>R$ </strong><?php echo number_format($somaPago, 2, ',', '.'); ?></td>
            <td style="color: black; font-weight: bold;"><strong>R$ </strong><?php echo number_format($somaTotal - $somaPago, 2, ',', '.'); ?></td>
            <td style="color: black; font-weight: bold;"><?php echo $estoqueTotal; ?></td>
            <td style="color: black; font-weight: bold;"><?php echo 'R$ '.number_format($valorEstoqueTotal, 2, ',', '.'); ?></td>                                                
        </tr>
    </tbody>
</table>




<br class="clear">
<br class="clear">


<table>


		<?php if($infoRelatorio['tipo'] == '2'){ ?>

		<tr>	
		    <?php

              $colunas = array();
              $novoArray = reset($detalhe);
              foreach($novoArray as $key => $value){
              $colunas[] = $key; ?>
                 <th><?php echo "$key" ?></th>
                 <!-- <th style="font-size: 12px;"><?php echo"$key" ?></td> -->
            <?php } ?>		
            		   	
		</tr>


        <?php foreach ($detalhe as $l) { ?>
          <tr>                    
            <?php for ($i=0; $i < count($colunas) ; $i++) { 
              $arr = get_object_vars($l);
              echo '<td>'.$arr[$colunas[$i]].'</td>';
            } ?>
          </tr>
        <?php } ?>







			
		<?php } ?>    

</table>




<!-- <table>
    <tfoot>
        <tr class="even">
        	<th colspan="5" style="font-size: 12px; text-align: right; color: #000"> <strong>TOTAL : R$ <?php echo number_format($somaTotal,2,",","."); ?></strong></th>
        </tr>             
    </tfoot>
</table> -->