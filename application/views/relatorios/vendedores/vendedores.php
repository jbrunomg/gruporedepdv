
<section class="content">
	<div class="row">
	  <div class="col-md-3">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios Rápidos</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					
					<a href="<?php echo base_url() ?>relatorios/vendedores/vendedoresDia" class="small-box-footer" target="_blank">
					<div class="small-box bg-green">						
						<div class="inner">
							<h3><?php echo $totalVendasDia; ?></h3>

							<p>Venda do dia P/Vendedores</p>
						</div>
						<div class="icon">
							<i class="fa fa-dropbox"></i>
						</div>						
					</div>
					</a>

					<a href="<?php echo base_url() ?>relatorios/vendedores/vendedoresDiaOutraloja" class="small-box-footer" target="_blank">
					<div class="small-box bg-blue">						
						<div class="inner">
							<h3><?php echo $totalVendasDiaOutraLoja; ?></h3>

							<p>Venda do dia P/Vendedores - Outras lojas</p>
						</div>
						<div class="icon">
							<i class="fa fa-dropbox"></i>
						</div>						
					</div>
					</a>
<!-- 
					<a href="<?php echo base_url() ?>relatorios/produtos/valorestoque" class="small-box-footer" target="_blank">
					<div class="small-box bg-blue">
						
						<div class="inner">
							<h3><?php echo '$ '.number_format($totalValorEstoque[0]->totalValor,2,',','.'); ?></h3>

							<p>Valor em estoque</p>
						</div>
						<div class="icon">
							<i class="fa fa-dropbox"></i>
						</div>
						
					</div>
					</a> -->

				</div>
			</div>
		</div>
		<div class="col-md-9">
          <form action="<?php echo base_url() ?>relatorios/vendedoresCustomizadas" method='POST' target="_blank" >
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios Customizáveis</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Venda de</label>
									<div class="input-group date">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="text" class="form-control pull-right" id="datepickerVendaIncio" placeholder="dd/mm/aaaa" name="dataInicio">
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>até</label>
									<div class="input-group date">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="text" class="form-control pull-right" id="datepickerVendaFim" placeholder="dd/mm/aaaa" name="dataFim">
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Vendendor</label>
									   <input type="text" class="form-control" name="usuario_nome" id="usuario_nome" value="<?php set_value('usuario_nome'); ?>">
									   <input type="hidden" id="usuario_id" name="usuario_id" value="<?php echo set_value('usuario_id'); ?>">
								</div>
							</div>
<!-- 							<div class="col-md-6">
								<div class="form-group">
									<label>Cliente</label>
									 <input type="text" class="form-control" name="cliente_nome" id="cliente_nomeR" value="<?php echo set_value('cliente_nome'); ?>" placeholder="Cliente">
                                     <input type="hidden" id="cliente_id" name="cliente_id" value="<?php echo set_value('cliente_id'); ?>">
								</div>
							</div> -->
						    <div class="col-md-6">
								<div class="form-group">
									<label>Ordenar por</label>
									<select class="form-control" name="ordenacao">
										<option value="1">Nome Vendedores A - Z</option>
										<option value="2">Nome Vendedores Z - A</option>
									</select>
								</div>
							</div>
						</div>

<!-- 						<div class="col-md-6">
								<div class="form-group">
									<label>Ordenar por</label>
									<select class="form-control" name="ordenacao">
										<option value="1">Nome cliente A - Z</option>
										<option value="2">Nome cliente Z - A</option>
									</select>
								</div>
							</div>
					 -->
						
			
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Lojas</label>
						                <select id="lojas" class="form-control" data-placeholder="Selecione as lojas" name="lojas">
						                <option value="todas">Todas Lojas</option>
						                  <?php  foreach ($lojas as $s) {
						                    echo "<option value='".$s['lojas']."'>".strtoupper($s['lojas'])."</option>";
						                  } ?>                        
						                </select>
								</div>
						    </div> 
							<div class="col-md-6">
								<div class="form-group">
									<label>Posição Impresso</label>
									<select class="form-control" name="formato">
										<option value="A4-P">Retrato</option>
										<option value="A4-L">Paissagem</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="row no-print text-center" style="margin-top: 40px">
						<div class="col-md-12">
							<button type="reset" class="btn btn-default">
								<i class="fa fa-eraser"></i> Limpar
							</button>
							<button type="submit" class="btn btn-primary" style="margin-right: 5px;">
								<i class="fa fa-print"></i> Imprimir
							</button>
						</div>
					</div>
				</div>
			</div>
		  </form>
		</div>



		<div class="col-md-9">
          <form action="<?php echo base_url() ?>relatorios/vendedoresCelularCustomizadas" method='POST' target="_blank" >
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios vendas - Celular</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Venda de</label>
									<div class="input-group date">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="text" class="form-control pull-right" id="dateVendaIncio" placeholder="dd/mm/aaaa" name="dataInicio">
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>até</label>
									<div class="input-group date">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="text" class="form-control pull-right" id="dateVendaFim" placeholder="dd/mm/aaaa" name="dataFim">
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Vendendor</label>
									   <input type="text" class="form-control" id="usuario_nome_vendedor"  name="usuario_nome" value="<?php set_value('usuario_nome'); ?>">
									   <input type="hidden" id="usuario_id_vendedor" name="usuario_id" value="<?php echo set_value('usuario_id'); ?>">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Lojas</label>
						                <select id="lojas" class="form-control" data-placeholder="Selecione as lojas" name="lojas">
						                <option value="todas">Todas Lojas</option>
						                  <?php  foreach ($lojas as $s) {
						                    echo "<option value='".$s['lojas']."'>".strtoupper($s['lojas'])."</option>";
						                  } ?>                        
						                </select>
								</div>
						    </div> 

						</div>
<!-- 			
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Lojas</label>
						                <select id="lojas" class="form-control" data-placeholder="Selecione as lojas" name="lojas">
						                <option value="todas">Todas Lojas</option>
						                  <?php  foreach ($lojas as $s) {
						                    echo "<option value='".$s['lojas']."'>".strtoupper($s['lojas'])."</option>";
						                  } ?>                        
						                </select>
								</div>
						    </div> 
							<div class="col-md-6">
								<div class="form-group">
									<label>Posição Impresso</label>
									<select class="form-control" name="formato">
										<option value="A4-P">Retrato</option>
										<option value="A4-L">Paissagem</option>
									</select>
								</div>
							</div>
						</div> -->
					</div>
					<div class="row no-print text-center" style="margin-top: 40px">
						<div class="col-md-12">
							<button type="reset" class="btn btn-default">
								<i class="fa fa-eraser"></i> Limpar
							</button>
							<button type="submit" class="btn btn-primary" style="margin-right: 5px;">
								<i class="fa fa-print"></i> Imprimir
							</button>
						</div>
					</div>
				</div>
			</div>
		  </form>
		</div>

	</div>
</section>
