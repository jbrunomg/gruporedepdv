

<hr style="margin-top:-10px">
<h3 class="text-center" style="margin-top:-10px">Relatório de Vendas</h3>


<!-- <?php var_dump($dados);  ?> -->
<table>
  <tr>
    <th style="font-size: 12px;" >Vendedor / Qtd. Venda</th>
    <th style="font-size: 12px;" >Valor Total</th>
  </tr>

  <?php 
  $total = 0;
  $totalVenda = 0;
  foreach ($dados as $d) {   
  ?>

    <tr class="even">
      <td><?php echo $d['vendedor'].' / '.$d['qtdVendas'] ?></td>
      <td style="color: blue;  font-weight: bold; font-size: 13px;"><?php echo number_format($d['total'],2,",",".") ?></td>     
    </tr>
    
    <?php
         
        $total      += $d['total']; 
        $totalVenda += $d['qtdVendas'];  
     } 

     ?>

</table>
<br class="clear">
<br class="clear">


<table>

          <tfoot>
              <tr class="even">
                <th style="font-size: 12px; text-align: right; color: black"> <strong>TOTAL VENDA <?php echo $totalVenda;?></strong></th>
                <th style="font-size: 12px; text-align: right; color: blue"> <strong>TOTAL GERAL R$ <?php echo number_format($total,2,",",".");?></strong></th>
              </tr>

        </tfoot>
</table>


