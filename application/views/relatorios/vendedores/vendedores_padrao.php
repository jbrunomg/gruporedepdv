

<hr style="margin-top:-10px">
<h3 class="text-center" style="margin-top:-10px">Relatório de Vendedores</h3>


<!-- <?php var_dump($dados);  ?> -->
<?php $total = 0 ; ?>
<table>
  <tr>
    <th style="font-size: 12px;" >Vendedor / Qtd. Venda</th>
    <th style="font-size: 12px;" >custo</th>

    <th style="font-size: 12px;" >Valor Total</th>
    <th style="font-size: 12px;" >lucro</th>

    <th style="font-size: 14px;" >Percentual</th>
  </tr>

  <?php  foreach ($dados as $d) {   
  $porcentagem = number_format( (($d['total'] / $d['custo'])*100)-100  ,2,",","."); // Solicitado por Srº Carlos  
  ?>

    <tr class="even">
      <td><?php echo $d['vendedor'].' / '.$d['qtdVendas'] ?></td>
      <td style="color: red;  font-weight: bold; font-size: 13px;"><?php echo number_format($d['custo'],2,",",".") ?></td>
      <td style="color: blue;  font-weight: bold; font-size: 13px;"><?php echo number_format($d['total'],2,",",".") ?></td>
      
      <td style="color: green; font-weight: bold; font-size: 13px;"><?php echo number_format($d['total'] - $d['custo'],2,",",".") ?></td>         
      <td style="color: black;  font-weight: bold; font-size: 13px;"><?php echo $porcentagem ?>%</td>
    </tr>
    
    <?php         
      $total += $d['total'];         
    }?>
</table>
<br class="clear">
<br class="clear">


<table>
  <tfoot>
        <tr class="even">
          <th colspan="2" style="font-size: 12px; text-align: right;font-weight: bold"><strong>TOTAL GERAL <strong style="color: green">R$ <?php echo number_format(($total),2,",",".");?></strong></strong></th>
        </tr>
  </tfoot>
</table>
<br class="clear">
<br class="clear">

<table>
  
  <tr>
    <th style="font-size: 14px;" >Grupo</th>
    <th style="font-size: 14px;" >&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </th>    
    <th style="font-size: 14px;" >Custo</th>
    <th style="font-size: 14px;" >Total</th>
    <th style="font-size: 14px;" >Lucro</th>
    <th style="font-size: 14px;" >Percentual</th>
  </tr>



  <?php foreach ($detalhes as $d) { 
    $custo += $d['custo'];
    $total += $d['total'];
    
    // =SOMA(TOTAL/CUSTO*100)-100
    $porcentagem = number_format( (($d['total'] / $d['custo'])*100)-100  ,2,",","."); // Solicitado por Srº Carlos

   ?>

    <tr class="even">
      <td style="color: black; font-weight: bold; font-size: 13px;"><?php echo $d['grupo']?></td>
      <td style="color: blue; font-weight: bold; font-size: 13px;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</td>
      
      <td style="color: red;  font-weight: bold; font-size: 13px;"><?php echo $d['custo']?></td>
      <td style="color: blue; font-weight: bold; font-size: 13px;"><?php echo $d['total']?></td>
      <td style="color: green; font-weight: bold; font-size: 13px;"><?php echo $d['total'] - $d['custo']?></td>
      
      <?php if ($porcentagem > 0) { ?>
      <td style="color: black;  font-weight: bold; font-size: 13px;"><?php echo $porcentagem ?>%</td>
        <?php }else{ ?>
        <td style="color: red;  font-weight: bold; font-size: 13px;"><?php echo $porcentagem ?>%</td>
        <?php } ?>
    </tr>
  <?php } ?>





</table>

