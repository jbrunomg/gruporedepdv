

<hr style="margin-top:-10px">
<h3 class="text-center" style="margin-top:-10px">Relatório Financeiro - <?php echo $tipo ?></h3>



<table>
  </thead>
  	<tr>
  		<th style="font-size: 17px;" >&nbsp; Data Lançamento</th>  		
  		<th style="font-size: 17px;" >&nbsp; Receita</th>
  		<th style="font-size: 17px;" >&nbsp; Despesa</th>     
  	</tr>
  <thead>
  <tbody>

	<?php 
	$totalReceita = 0;
  $totalDespesa = 0;
  $comissao = floatval(str_replace(",",".",$comissao));


	foreach ($dados as $d) { 
    $vencimento = date('d/m/Y', strtotime($d->dia)); 

    $totalReceita = $totalReceita + $d->receita;
    $totalDespesa = $totalDespesa + $d->despesa;
    $tatalbruto =  $d->total - $d->custo;
  ?>

		<tr class="even">

      <td style="font-size: 13px;"><?php echo $vencimento ?></td>		
      
			<td style="color: green;font-size: 13px;"><?php echo number_format($d->receita,2,",",".");?></td>
      <td style="color: red;font-size: 13px;"><?php echo number_format($d->despesa,2,",",".");?></td>
		
		</tr> 

  <?php } ?> 

  <tr style="height: 20px;">
    <td colspan="3" style="background: grey">&nbsp; &nbsp; </td>
  </tr>

  <tr>
    <td style="font-weight: bold;font-size: 13px;">&nbsp; LAÇAMENTOS:<?php echo ' '.count($dados). ' DIAS';  ?></td>                            
    <td style="color: green; font-weight: bold;font-size: 13px;">&nbsp; RECEITAS:<?php echo ' '.number_format($totalReceita,2,",","."); ?></td>
    <td style="color: red; font-weight: bold;font-size: 13px;">&nbsp; DESPESAS:<?php echo ' '.number_format($totalDespesa,2,",","."); ?></th>   
  </tr> 

  <tr style="height: 20px;">
    <td colspan="3" style="background: grey">&nbsp; &nbsp; </td>
  </tr>

  <tr>
    <td style="font-weight: bold;font-size: 13px;">COMISSÂO <?php echo $comissao ?>%</td>
    <td style="color: red; font-weight: bold;font-size: 13px;"><?php echo ' '.number_format($comissao*$totalReceita/100,2,",","."); ?></td>
    <td ></td>  
  </tr>

    </tbody> 


</table>

<br><br><br>

<table style="height: 300px; width: 300px">
  
  <tr>
    <td style="font-weight: bold;font-size: 13px; color: green">&nbsp; RECEITAS:</td>
    <td style="font-weight: bold;font-size: 13px; color: green"><?php echo number_format($totalReceita,2,",","."); ?></td> 
  </tr>

  <tr>
    <td style="font-weight: bold;font-size: 13px;">&nbsp; LAÇAMENTOS:</td>   
    <td style="font-weight: bold;font-size: 13px;"><?php echo ' '.count($dados). ' DIAS';  ?></td>
  </tr>

  <tr>
    <td style="font-weight: bold;font-size: 13px;">&nbsp; MEDIA:</td>   
    <td style="font-weight: bold;font-size: 13px;"><?php echo number_format($totalReceita/count($dados),2,",","."); $media = $totalReceita/count($dados); ?></td>
  </tr>

  <tr>
    <td style="font-weight: bold;font-size: 13px;">&nbsp; DIAS UTEIS:</td>   
    <td style="font-weight: bold;font-size: 13px;"><?php echo $dias_uteis ?></td>
  </tr>

  <tr>
    <td style="font-weight: bold;font-size: 13px;">&nbsp; PROJEÇÃO:</td>   
    <td style="font-weight: bold;font-size: 13px;"><?php echo number_format($media*$dias_uteis,2,",","."); ?></td>
  </tr>

  <tr>
    <td style="color: red; font-weight: bold;font-size: 13px;">&nbsp; DESPESAS:</th>   
    <td style="color: red; font-weight: bold;font-size: 13px;"><?php echo number_format($totalDespesa,2,",","."); ?></th> 
  </tr>

  <tr>
    <td style="color: red; font-weight: bold;font-size: 13px;">COMISSÂO</td>
    <td style="color: red; font-weight: bold;font-size: 13px;"><?php echo number_format($comissao*$totalReceita/100,2,",","."); ?></td>
  </tr>

  <tr>
    <td style="color: green;font-weight: bold;font-size: 13px;">LUCRO BRUTO</td>
    <td style="color: green;font-weight: bold;font-size: 13px;"><?php echo number_format($tatalbruto,2,",","."); ?></td>
  </tr>

  <tr>
    <td style="color: green;font-weight: bold;font-size: 13px;">LUCRO LIQUIDO</td>
    <!-- <td style="font-weight: bold;font-size: 13px;"><?php echo number_format($tatalbruto - ($totalDespesa + ($comissao*$totalReceita/100)),2,",","."); ?></td> -->
    <td style="color: green;font-weight: bold;font-size: 13px;"><?php echo number_format($tatalbruto - $totalDespesa ,2,",","."); ?></td>
  </tr>


</table>
