
<section class="content">
	<div class="row">
	  <div class="col-md-3">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios Rápidos</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					
					<a href="<?php echo base_url() ?>relatorios/financeiro/dia" class="small-box-footer" target="_blank">
					<div class="small-box bg-green">						
						<div class="inner">
							<h3><?php echo '$' ?></h3>

							<p>Fluxo caixa do dia</p>
						</div>
						<div class="icon">
							<i class="fa fa-dropbox"></i>
						</div>						
					</div>
					</a>

 					<a href="<?php echo base_url() ?>relatorios/financeiro/mes" class="small-box-footer" target="_blank">
					<div class="small-box bg-aqua">						
						<div class="inner">
							<h3><?php echo '$' ?></h3>

							<p>Fluxo caixa do mês</p>
						</div>
						<div class="icon">
							<i class="fa fa-dropbox"></i>
						</div>						
					</div>
					</a>

					<a href="<?php echo base_url() ?>relatorios/financeiro/mesPassado" class="small-box-footer" target="_blank">
					<div class="small-box bg-blue">						
						<div class="inner">
							<h3><?php echo '$' ?></h3>

							<p>Fluxo caixa Mês Anterior</p>
						</div>
						<div class="icon">
							<i class="fa fa-dropbox"></i>
						</div>						
					</div>
					</a>


				</div>
			</div>
		</div>


		<div class="col-md-9">
			<form action="<?php echo base_url() ?>relatorios/financeiroCustom" method='GET' target="_blank">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios Customizáveis New</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Vencimento de:</label>
									<div class="input-group date">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="text" class="form-control pull-right" id="datepickerFinaVecIncio" placeholder="dd/mm/aaaa" name="dataInicio">
									</div>
								</div>
							</div>
 							<div class="col-md-6">
								<div class="form-group">
									<label>até</label>
									<div class="input-group date">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="text" class="form-control pull-right" id="datepickerFinaVecFim" placeholder="dd/mm/aaaa" name="dataFim">
									</div>
								</div>
							</div> 
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Pagamento de:</label>
									<div class="input-group date">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="text" class="form-control pull-right" id="datepickerFinaPagIncio" placeholder="dd/mm/aaaa" name="pdataInicio">
									</div>
								</div>
							</div>
 							<div class="col-md-6">
								<div class="form-group">
									<label>até</label>
									<div class="input-group date">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="text" class="form-control pull-right" id="datepickerFinaPagFim" placeholder="dd/mm/aaaa" name="pdataFim">
									</div>
								</div>
							</div> 
						</div>

						<div class="row">
						   <div class="col-md-12">
								<div class="form-group">
									<label>Centro de Custo</label>
						               <input type="text" class="form-control" name="categoria_fina_descricao" id="categoria_fina_descricao" value="<?php set_value('categoria_fina_descricao'); ?>">
									   <input type="hidden" id="categoria_fina_id" name="categoria_fina_id" value="">
								</div>
						   </div>
		
						</div>

						<div class="row">				
							<div class="col-md-6">
								<div class="form-group">
									<label>Tipo</label>
									<select class="form-control" name="tipo">
										<option value="1">Ambos</option>
										<option value="receita">Receita</option>
										<option value="despesa">Despesa</option>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Situação</label>
									<select class="form-control" name="situacao">
										<option value="1">Ambos</option>
										<option value="pago">Pago</option>
										<option value="pendente">Pendente</option>
									</select>
								</div>
							</div>
						</div>

						<div class="row">				
							<div class="col-md-6">
								<div class="form-group">
									<label>Cliente</label>
									 <input type="text" class="form-control" name="cliente_nome" id="cliente_nomeR" value="<?php echo set_value('cliente_nome'); ?>" placeholder="Cliente">
                                     <input type="hidden" id="cliente_id" name="cliente_id" value="">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Fornecedor</label>
									<input type="text" class="form-control" name="fornecedor_nome" id="fornecedor_nome" value="<?php set_value('fornecedor_nome'); ?>" placeholder="Fornecedor">
									<input type="hidden" id="fornecedor_id" name="fornecedor_id" value="">
								</div>
							</div>
						</div>


						<div class="row">
			
						   <div class="col-md-6">
								<div class="form-group">
									<label>Ordenar por</label>
									<select class="form-control" name="ordenacao">
										<option value="1">Nome cliente A - Z</option>
										<option value="2">Nome cliente Z - A</option>
									</select>
								</div>
							</div>
						</div>



					</div>
					<div class="row no-print text-center" style="margin-top: 40px">
						<div class="col-md-12">
							<button type="reset" class="btn btn-default">
								<i class="fa fa-eraser"></i> Limpar
							</button>
							<button type="submit" class="btn btn-primary" style="margin-right: 5px;">
								<i class="fa fa-print"></i> Imprimir
							</button>
						</div>
					</div>
				</div>
			</div>
		  </form>
		  

          <form action="<?php echo base_url() ?>relatorios/financeiroCustomizadas" method='POST' target="_blank">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios Customizáveis</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Vencimento de:</label>
									<div class="input-group date">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="text" class="form-control pull-right" id="datepickerVendaIncio" placeholder="dd/mm/aaaa" name="dataInicio">
									</div>
								</div>
							</div>
 							<div class="col-md-6">
								<div class="form-group">
									<label>até</label>
									<div class="input-group date">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="text" class="form-control pull-right" id="datepickerVendaFim" placeholder="dd/mm/aaaa" name="dataFim">
									</div>
								</div>
							</div> 
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Vendendor</label>
									   <input type="text" class="form-control" name="usuario_nome" id="usuario_nome" value="<?php set_value('usuario_nome'); ?>">
									   <input type="hidden" id="usuario_id" name="usuario_id" value="">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Cliente</label>
									 <input type="text" class="form-control" name="cliente_nome" id="cliente_nomeFin" value="<?php echo set_value('cliente_nome'); ?>" placeholder="Cliente">
                                     <input type="hidden" id="cliente_idFin" name="cliente_id" value="">
								</div>
							</div>
						</div>
						<div class="row">
						   <div class="col-md-6">
								<div class="form-group">
									<label>Centro de Custo</label>
						               <input type="text" class="form-control" name="categoria_fina_descricao" id="categoria_fina_descricao" value="<?php set_value('categoria_fina_descricao'); ?>">
									   <input type="hidden" id="categoria_fina_id" name="categoria_fina_id" value="">
								</div>
						   </div>
						   <div class="col-md-6">
								<div class="form-group">
									<label>Ordenar por</label>
									<select class="form-control" name="ordenacao">
										<option value="1">Nome cliente A - Z</option>
										<option value="2">Nome cliente Z - A</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Tipo Relatorio</label>
									<select class="form-control" name="tipo">
										<option value="1">Resumido</option>
										<option value="2">Detalhado</option>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Tipo</label>
									<select class="form-control" name="tipoRelatorio">
										<option value="1">Ambos</option>
										<option value="2">Receita</option>
										<option value="3">Despesa</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="row no-print text-center" style="margin-top: 40px">
						<div class="col-md-12">
							<button type="reset" class="btn btn-default">
								<i class="fa fa-eraser"></i> Limpar
							</button>
							<button type="submit" class="btn btn-primary" style="margin-right: 5px;">
								<i class="fa fa-print"></i> Imprimir
							</button>
						</div>
					</div>
				</div>
			</div>
		  </form>

		  <div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Financeiro em Aberto</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-12">
						<div class="row">
							<form action="<?php echo base_url() ?>relatorios/financeiroEmAberto" method='POST' target="_blank">
							
							<div class="row">

								<div class="col-md-2">
									<div class="form-group">
										<label>Ano</label>
										<div class="input-group date">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" class="form-control pull-right" id="datepickerAbertoAno" placeholder="aaaa" name="anoRelatorio" required="required">
										</div>
									</div>
								</div>
		
							    <div class="col-md-2">
									<div class="form-group">
										<label>Mês</label>
										<div class="input-group date">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" class="form-control pull-right" id="datepickerAbertoMes" placeholder="mm" name="mesRelatorio" required="required">
										</div>
									</div>
								</div>

								<div class="col-md-4">
									<div class="form-group">
										<label>Cliente</label>
										 <input type="text" class="form-control" name="cliente_nome" id="cliente_nomeFinAbr" value="<?php echo set_value('cliente_nome'); ?>" placeholder="Cliente">
		                                 <input type="hidden" id="cliente_idFinAbr" name="cliente_id" value="">
									</div>
								</div>

								<div class="col-md-4">
									<div class="form-group">
										<label>Tipo</label>
										<select class="form-control" name="tipoRelatorio">
											<option value="1">Resumido</option>
											<option value="2">Detalhado</option>
										</select>
									</div>
								</div>		    
						
							</div>
						
							<div class="row no-print text-center" style="margin-top: 40px">
								<div class="col-md-12">
									<button type="submit" class="btn btn-primary" style="margin-right: 50px;  width: 30%;">
										<i class="fa fa-print"></i> Imprimir
									</button>
								</div>
							</div>
						    </form>
						</div>
					</div>
				</div>
		  </div>

		  <div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Previsão Faturamento</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-12">
						<div class="row">
							<form action="<?php echo base_url() ?>relatorios/financeiroPrevisaoFaturamento" method='POST' target="_blank">
							
							<div class="row">
		
							    <div class="col-md-4">
									<div class="form-group">
										<label>Mês</label>
										<div class="input-group date">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" class="form-control pull-right" id="datepickerGrupo" placeholder="mm/aaaa" name="mesInicio" required="required">
										</div>
									</div>
								</div>

								<div class="col-md-4">
									<div class="form-group">
										<label>Dias uteis</label>
										<input type="text" class="form-control" name="dias_uteis" id="dias_uteis" value="">
										
									</div>
								</div>


								<div class="col-md-4">
									<div class="form-group">
										<label>Comissão %</label>
									    <input type="text" class="form-control" name="comissao" id="comissao" value="">
									    
									</div>
								</div>
						
							</div>
						
							<div class="row no-print text-center" style="margin-top: 40px">
								<div class="col-md-12">
									<button type="reset" class="btn btn-default">
										<i class="fa fa-eraser"></i> Limpar
									</button>
									<button type="submit" class="btn btn-primary" style="margin-right: 5px;">
										<i class="fa fa-print"></i> Imprimir
									</button>
								</div>
							</div>
						    </form>
						</div>
					</div>
				</div>
			</div>


		    <div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Fluxo de Caixa</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-12">
						<div class="row">
							<form action="<?php echo base_url() ?>relatorios/financeiroFluxoCaixa" method='POST' target="_blank">
							
							<div class="row">

								<div class="col-md-4">
									<div class="form-group">
										<label>Ano</label>
										<div class="input-group date">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" class="form-control pull-right" id="datepickerGrupoAno" placeholder="aaaa" name="anoRelatorio" required="required">
										</div>
									</div>
								</div>
		
							    <div class="col-md-4">
									<div class="form-group">
										<label>Periodo</label>
										<select class="form-control" name="periodoRelatorio">
											<option value="1">Semestre 1</option>
											<option value="2">Semestre 2</option>
										</select>
									</div>
								</div>

								<div class="col-md-4">
									<div class="form-group">
										<label>Tipo</label>
										<select class="form-control" name="tipoRelatorio">
											<option value="1">Ambos</option>
											<option value="2">Receita</option>
											<option value="3">Despesa</option>
										</select>
									</div>
								</div>
						
							</div>
						
							<div class="row no-print text-center" style="margin-top: 40px">
								<div class="col-md-12">
									<button type="submit" class="btn btn-primary" style="margin-right: 50px;  width: 30%;">
										<i class="fa fa-print"></i> Imprimir
									</button>
								</div>
							</div>
						    </form>
						</div>
					</div>
				</div>
			</div>


			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Baixa Parcial</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-12">
						<div class="row">
							<form action="<?php echo base_url() ?>relatorios/financeiroBaixaParcial" method='POST' target="_blank">
							
							<div class="row">

							    <div class="col-md-4">
									<div class="form-group">
										<label>Baixa de</label>
										<div class="input-group date">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" class="form-control pull-right" id="datepickerBaixaIncio" placeholder="dd/mm/aaaa" name="dataInicio" required="required">
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>até</label>
										<div class="input-group date">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" class="form-control pull-right" id="datepickerBaixaFim" placeholder="dd/mm/aaaa" name="dataFim" required="required">
										</div>
									</div>
								</div>

								<div class="col-md-4">
									<div class="form-group">
										<label>Tipo Relatorio</label>
										<select class="form-control" name="tipo">
											<option value="1">Resumido</option>
											<option value="2">Detalhado</option>
										</select>
									</div>
								</div>

								<div class="col-md-12">
									<div class="form-group">
										<label>Cliente</label>
										<input type="text" class="form-control" name="cliente_nome" id="cliente_nomeBai" value="<?php echo set_value('cliente_nome'); ?>" placeholder="Cliente">
										<input type="hidden" id="cliente_idBai" name="cliente_id" value="">
									</div>
								</div>
						
							</div>
						
							<div class="row no-print text-center" style="margin-top: 40px">
								<div class="col-md-12">
									<button type="submit" class="btn btn-primary" style="margin-right: 50px;  width: 30%;">
										<i class="fa fa-print"></i> Imprimir
									</button>
								</div>
							</div>
						    </form>
						</div>
					</div>
				</div>
			</div>

 

		</div>

	</div>
</section>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
var base_url = "<?php echo base_url(); ?>";

$("#cliente_nomeFinAbr").autocomplete({
  source: base_url+"relatorios/autoCompleteClientes",
  minLength: 2,
  select: function( event, ui ) {    
    $("#cliente_idFinAbr").val(ui.item.id);   
  }
});

$("#cliente_nomeBai").autocomplete({
  source: base_url+"relatorios/autoCompleteClientes",
  minLength: 2,
  select: function( event, ui ) {    
    $("#cliente_idBai").val(ui.item.id);   
  }
});

</script>
