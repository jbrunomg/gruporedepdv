

<hr style="margin-top:-10px">
<h3 class="text-center" style="margin-top:-10px">Relatório Financeiro - </h3>



<table>
  </thead>
  	<tr>
  		<th style="font-size: 12px;" >&nbsp; Data Lançamento</th>  		
  		<th style="font-size: 12px;" >&nbsp; Cliente/Fornecedor</th>
  		<th style="font-size: 12px;" >&nbsp; Valor</th>
      <th style="font-size: 12px;" >&nbsp; Data Pagamento</th>
  		<th style="font-size: 12px;" >&nbsp; Forma de Pagamento</th> 
      <th style="font-size: 12px;" >&nbsp; Tipo/Situação</th>      
  	</tr>
  <thead>
  <tbody>

 <?php
     $totalCartao = 0;
     $totalCartaoDebito = 0;
     $totalCheque = 0;
     $totalDinheiro = 0;
     $totalPix      = 0;
     $totalOutros = 0;
     $totalReceita = 0;
     $totalPagOnline = 0;
     $totalTransfBancaria = 0;

     $totalDesSucata = 0;
     $totalDesDinheiro = 0;
     $totalDesPix = 0;                          
     $totalDespesa = 0;
     $vencimento = "";
     $situacao = "";
     $totalBoleto = 0;
     $saldo = 0;

    foreach ($lancamentos as $l) {

        $vencimento = date('d/m/Y', strtotime($l->data_vencimento));
        
        if($l->data_pagamento == '0000-00-00'){
          $pagamento = 'A definir'; 
        } else{
          $pagamento = date(('d/m/Y'),strtotime($l->data_pagamento));
        }

        if($l->financeiro_baixado == 1){$situacao = 'Pago';}else{ $situacao = 'Pendente';}  

        if($l->financeiro_tipo == 'receita' and $l->financeiro_forma_pgto == 'Cartão de Crédito'){ $totalCartao += $l->financeiro_valor;}
        if($l->financeiro_tipo == 'receita' and $l->financeiro_forma_pgto == 'Cartão de Débito'){ $totalCartaoDebito += $l->financeiro_valor;}
        if($l->financeiro_tipo == 'receita' and $l->financeiro_forma_pgto == 'Cheque'){ $totalCheque += $l->financeiro_valor;}
        if($l->financeiro_tipo == 'receita' and $l->financeiro_forma_pgto == 'Dinheiro'){ $totalDinheiro += $l->financeiro_valor;}
        if($l->financeiro_tipo == 'receita' and $l->financeiro_forma_pgto == 'Pagamento Instantâneo (PIX)'){ $totalPix += $l->financeiro_valor;}
        
        if($l->financeiro_tipo == 'receita' and ($l->financeiro_forma_pgto == 'Outros' or $l->financeiro_forma_pgto == 'Sem pagamento')){ $totalOutros += $l->financeiro_valor;}
        if($l->financeiro_tipo == 'receita' and $l->financeiro_forma_pgto == 'Boleto Bancário'){ $totalBoleto += $l->financeiro_valor;}

        if ($l->financeiro_tipo == 'receita' and $l->financeiro_forma_pgto == 'Pagamento Online') {$totalPagOnline += $l->financeiro_valor;}
        if ($l->financeiro_tipo == 'receita' and $l->financeiro_forma_pgto == 'Transferência Bancária') {$totalTransfBancaria += $l->financeiro_valor;}
                                    

        if($l->financeiro_tipo == 'receita'){ $totalReceita += $l->financeiro_valor;} else{ $totalDespesa += $l->financeiro_valor;}
                              
        echo '<tr>';                                      
        echo '<td>' . $vencimento. '</td>'; 
        echo '<td>' . trim($l->financeiro_forn_clie).'<br />' .trim($l->financeiro_descricao) . '</td>';                                     
        if(is_numeric($l->financeiro_valor)) { echo '<td>' . 'R$ '. number_format($l->financeiro_valor,2,',','.') . '</td>';}                              
        echo '<td>' . $pagamento. '</td>';
        echo '<td>' . trim($l->financeiro_forma_pgto) . '</td>';  
        echo '<td>' . $l->financeiro_tipo.' / '.$situacao. '</td>';
        echo '</tr>';
        
    }

    ?>



    </tbody> 


</table>


      <h5 colspan="5" style="text-align: right; color: grey"> <strong>Cartão Crédito: R$ <?php echo number_format($totalCartao,2,',','.') ?></strong></h5>
      <h5 colspan="5" style="text-align: right; color: grey"> <strong>Cartão Débito: R$ <?php echo number_format($totalCartaoDebito,2,',','.') ?></strong></h5>
      <h5 colspan="5" style="text-align: right; color: grey"> <strong>Cheque: R$ <?php echo number_format($totalCheque,2,',','.') ?></strong></h5>
      <h5 colspan="5" style="text-align: right; color: grey"> <strong>Dinheiro: R$ <?php echo number_format($totalDinheiro,2,',','.') ?></strong></h5>
      <h5 colspan="5" style="text-align: right; color: grey"> <strong>Pagamento Instantâneo (PIX) : R$ <?php echo number_format($totalPix,2,',','.') ?></strong></h5>
      <h5 colspan="5" style="text-align: right; color: grey"> <strong>Boleto: R$ <?php echo number_format($totalBoleto,2,',','.') ?></strong></h5>
      <h5 colspan="5" style="text-align: right; color: grey"> <strong>Pagamento Online: R$ <?php echo number_format($totalPagOnline,2,',','.') ?></strong></h5>

       <h5 colspan="5" style="text-align: right; color: grey"> <strong>Tranferencia Bancaria: R$ <?php echo number_format($totalTransfBancaria,2,',','.') ?></strong></h5>

      <h5 colspan="5" style="text-align: right; color: grey"> <strong>Outros: R$ <?php echo number_format($totalOutros,2,',','.') ?></strong></h5>
      <h5 colspan="5" style="text-align: right; color: green"> <strong>Total Receitas: R$ <?php echo number_format($totalReceita,2,',','.') ?></strong></h5>
     
      <!-- 
      <h5 colspan="5" style="text-align: right; color: grey"> <strong>Dinheiro: R$ <?php echo number_format($totalDesDinheiro,2,',','.') ?></strong></h5>
      <h5 colspan="5" style="text-align: right; color: grey"> <strong>Pix: R$ <?php echo number_format($totalDesPix,2,',','.') ?></strong></h5>
      -->

      <h5 colspan="2" style="text-align: right; color: red"><strong>Total Despesas: R$ <?php echo number_format($totalDespesa,2,',','.') ?></strong></h5>
      
      <h5 colspan="2" style="text-align: right;"><strong>Saldo: R$ <?php echo number_format($totalReceita - $totalDespesa,2,',','.') ?></strong></h5>

      <h5 style="text-align: right">Data do Relatório: <?php echo date('d/m/Y');?></h5>