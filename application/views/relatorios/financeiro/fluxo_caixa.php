<?php 
    @set_time_limit( 300 );
    ini_set('max_execution_time', 300);
    ini_set('max_input_time', 300);
    ini_set('memory_limit', '512M');
?>

<hr style="margin-top:-10px">
<h3 class="text-center" style="margin-top:-10px">Relatório Financeiro - Fluxo De Caixa <br><center><?php echo $periodoRelatorio == '1' ? "Semestre 1 - " : "Semestre 2 - "; echo $ano;?></center></h3>



<table>
  </thead>
  	<tr>
      <th style="font-size: 12px;" >&nbsp;</th>
  		<th style="font-size: 12px;" >&nbsp; Previsão</th>  		
  		<th style="font-size: 12px;" >&nbsp; Realizado</th>
      <th style="font-size: 12px;" >&nbsp; Previsão</th>      
      <th style="font-size: 12px;" >&nbsp; Realizado</th>  
      <th style="font-size: 12px;" >&nbsp; Previsão</th>      
      <th style="font-size: 12px;" >&nbsp; Realizado</th>  
      <th style="font-size: 12px;" >&nbsp; Previsão</th>      
      <th style="font-size: 12px;" >&nbsp; Realizado</th>  
      <th style="font-size: 12px;" >&nbsp; Previsão</th>      
      <th style="font-size: 12px;" >&nbsp; Realizado</th>  
      <th style="font-size: 12px;" >&nbsp; Previsão</th>      
      <th style="font-size: 12px;" >&nbsp; Realizado</th>      
  	</tr>

  <?php if ($periodoRelatorio == '1') { ?>

    <tr>
      <th  class="text-center" style="font-size: 12px;" >&nbsp;</th>
      <th  class="text-center" style="font-size: 12px;" >&nbsp; 1</th>      
      <th  class="text-center" style="font-size: 12px;" >&nbsp; 1</th>
      <th  class="text-center" style="font-size: 12px;" >&nbsp; 2</th>      
      <th  class="text-center" style="font-size: 12px;" >&nbsp; 2</th>  
      <th  class="text-center" style="font-size: 12px;" >&nbsp; 3</th>      
      <th  class="text-center" style="font-size: 12px;" >&nbsp; 3</th>  
      <th  class="text-center" style="font-size: 12px;" >&nbsp; 4</th>      
      <th  class="text-center" style="font-size: 12px;" >&nbsp; 4</th>  
      <th  class="text-center" style="font-size: 12px;" >&nbsp; 5</th>      
      <th  class="text-center" style="font-size: 12px;" >&nbsp; 5</th>  
      <th  class="text-center" style="font-size: 12px;" >&nbsp; 6</th>      
      <th  class="text-center" style="font-size: 12px;" >&nbsp; 6</th>      
    </tr>

  <?php } else { ?>

    <tr>
      <th  class="text-center" style="font-size: 12px;" >&nbsp;</th>
      <th  class="text-center" style="font-size: 12px;" >&nbsp; 7</th>      
      <th  class="text-center" style="font-size: 12px;" >&nbsp; 7</th>
      <th  class="text-center" style="font-size: 12px;" >&nbsp; 8</th>      
      <th  class="text-center" style="font-size: 12px;" >&nbsp; 8</th>  
      <th  class="text-center" style="font-size: 12px;" >&nbsp; 9</th>      
      <th  class="text-center" style="font-size: 12px;" >&nbsp; 9</th>  
      <th  class="text-center" style="font-size: 12px;" >&nbsp; 10</th>      
      <th  class="text-center" style="font-size: 12px;" >&nbsp; 10</th>  
      <th  class="text-center" style="font-size: 12px;" >&nbsp; 11</th>      
      <th  class="text-center" style="font-size: 12px;" >&nbsp; 11</th>  
      <th  class="text-center" style="font-size: 12px;" >&nbsp; 12</th>      
      <th  class="text-center" style="font-size: 12px;" >&nbsp; 12</th>      
    </tr>


  <?php } ?> 

  <?php if ($tipoRelatorio == '2' or $tipoRelatorio == '1') { ?>
    <tr>
      <th  bgcolor="#D3D3D3" style="font-size: 12px; width:250px;" >ENTRADAS</th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px;" ></th>   
      <th  bgcolor="#D3D3D3" style="font-size: 12px;" ></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px;" ></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px;" ></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px;" ></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px;" ></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px;" ></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px;" ></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px;" ></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px;" ></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px;" ></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px;" ></th>        
    </tr>

  <?php 

      $totalprevistoreceita1 = 0;
      $totalrealizadoreceita1 = 0;
      $totalprevistoreceita2 = 0;
      $totalrealizadoreceita2 = 0;
      $totalprevistoreceita3 = 0;
      $totalrealizadoreceita3 = 0;
      $totalprevistoreceita4 = 0;
      $totalrealizadoreceita4 = 0;
      $totalprevistoreceita5 = 0;
      $totalrealizadoreceita5 = 0;
      $totalprevistoreceita6 = 0;
      $totalrealizadoreceita6 = 0;
      $totalprevistoreceita7 = 0;
      $totalrealizadoreceita7 = 0;
      $totalprevistoreceita8 = 0;
      $totalrealizadoreceita8 = 0;
      $totalprevistoreceita9 = 0;
      $totalrealizadoreceita9 = 0;
      $totalprevistoreceita10 = 0;
      $totalrealizadoreceita10 = 0;
      $totalprevistoreceita11 = 0;
      $totalrealizadoreceita11 = 0;
      $totalprevistoreceita12 = 0;
      $totalrealizadoreceita12 = 0;


  foreach ($dadosreceita as $r) { ?>

    <?php if ($periodoRelatorio == '1') { 

      $totalprevistoreceita1  = $totalprevistoreceita1  + str_replace(",",".",$r->previstoreceita1);
      $totalrealizadoreceita1 = $totalrealizadoreceita1 + str_replace(",",".",$r->realizadoreceita1);
      $totalprevistoreceita2  = $totalprevistoreceita2  + str_replace(",",".",$r->previstoreceita2);
      $totalrealizadoreceita2 = $totalrealizadoreceita2 + str_replace(",",".",$r->realizadoreceita2);
      $totalprevistoreceita3  = $totalprevistoreceita3  + str_replace(",",".",$r->previstoreceita3);
      $totalrealizadoreceita3 = $totalrealizadoreceita3 + str_replace(",",".",$r->realizadoreceita3);
      $totalprevistoreceita4  = $totalprevistoreceita4  + str_replace(",",".",$r->previstoreceita4);
      $totalrealizadoreceita4 = $totalrealizadoreceita4 + str_replace(",",".",$r->realizadoreceita4);
      $totalprevistoreceita5  = $totalprevistoreceita5  + str_replace(",",".",$r->previstoreceita5);
      $totalrealizadoreceita5 = $totalrealizadoreceita5 + str_replace(",",".",$r->realizadoreceita5);
      $totalprevistoreceita6  = $totalprevistoreceita6  + str_replace(",",".",$r->previstoreceita6);
      $totalrealizadoreceita6 = $totalrealizadoreceita6 + str_replace(",",".",$r->realizadoreceita6);

    ?>
    <tr>
      <td style="font-size: 12px; text-align: left;" ><?php echo $r->categoria ?></td>  
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->previstoreceita1,2,",",".") ?></td>
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->realizadoreceita1,2,",",".") ?></td>
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->previstoreceita2,2,",",".") ?></td>
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->realizadoreceita2,2,",",".") ?></td> 
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->previstoreceita3,2,",",".") ?></td>
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->realizadoreceita3,2,",",".") ?></td> 
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->previstoreceita4,2,",",".") ?></td>
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->realizadoreceita4,2,",",".") ?></td> 
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->previstoreceita5,2,",",".") ?></td>
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->realizadoreceita5,2,",",".") ?></td> 
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->previstoreceita6,2,",",".") ?></td>
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->realizadoreceita6,2,",",".") ?></td> 
    </tr> 

    <?php } else { 

      $totalprevistoreceita7   = $totalprevistoreceita7   + str_replace(",",".",$r->previstoreceita7);
      $totalrealizadoreceita7  = $totalrealizadoreceita7  + str_replace(",",".",$r->realizadoreceita7);
      $totalprevistoreceita8   = $totalprevistoreceita8   + str_replace(",",".",$r->previstoreceita8);
      $totalrealizadoreceita8  = $totalrealizadoreceita8  + str_replace(",",".",$r->realizadoreceita8);
      $totalprevistoreceita9   = $totalprevistoreceita9   + str_replace(",",".",$r->previstoreceita9);
      $totalrealizadoreceita9  = $totalrealizadoreceita9  + str_replace(",",".",$r->realizadoreceita9);
      $totalprevistoreceita10  = $totalprevistoreceita10  + str_replace(",",".",$r->previstoreceita10);
      $totalrealizadoreceita10 = $totalrealizadoreceita10 + str_replace(",",".",$r->realizadoreceita10);
      $totalprevistoreceita11  = $totalprevistoreceita11  + str_replace(",",".",$r->previstoreceita11);
      $totalrealizadoreceita11 = $totalrealizadoreceita11 + str_replace(",",".",$r->realizadoreceita11);
      $totalprevistoreceita12  = $totalprevistoreceita12  + str_replace(",",".",$r->previstoreceita12);
      $totalrealizadoreceita12 = $totalrealizadoreceita12 + str_replace(",",".",$r->realizadoreceita12);

    ?>

     <tr>
      <td style="font-size: 12px; text-align: left;" ><?php echo $r->categoria ?></td>  
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->previstoreceita7,2,",",".") ?></td>
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->realizadoreceita7,2,",",".") ?></td>
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->previstoreceita8,2,",",".") ?></td>
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->realizadoreceita8,2,",",".") ?></td> 
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->previstoreceita9,2,",",".") ?></td>
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->realizadoreceita9,2,",",".") ?></td> 
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->previstoreceita10,2,",",".") ?></td>
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->realizadoreceita10,2,",",".") ?></td> 
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->previstoreceita11,2,",",".") ?></td>
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->realizadoreceita11,2,",",".") ?></td> 
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->previstoreceita12,2,",",".") ?></td>
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->realizadoreceita12,2,",",".") ?></td> 
     </tr>  

    <?php } ?>

  <?php } ?> 

  <?php if ($periodoRelatorio == '1') { ?>
    <tr>
      <th  bgcolor="#D3D3D3" style="font-size: 12px; " >TOTAL DAS ENTRADAS</th>
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalprevistoreceita1,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalrealizadoreceita1,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalprevistoreceita2,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalrealizadoreceita2,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalprevistoreceita3,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalrealizadoreceita3,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalprevistoreceita4,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalrealizadoreceita4,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalprevistoreceita5,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalrealizadoreceita5,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalprevistoreceita6,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalrealizadoreceita6,2,",",".")?></th>   
    </tr> 

    <?php } else { ?>

     <tr>
      <th  bgcolor="#D3D3D3" style="font-size: 12px; " >TOTAL DAS ENTRADAS</th>
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalprevistoreceita7,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalrealizadoreceita7,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalprevistoreceita8,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalrealizadoreceita8,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalprevistoreceita9,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalrealizadoreceita9,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalprevistoreceita10,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalrealizadoreceita10,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalprevistoreceita11,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalrealizadoreceita11,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalprevistoreceita12,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalrealizadoreceita12,2,",",".")?></th>     
     </tr>  

    <?php } ?>
     <?php } ?> 

    <?php if ($tipoRelatorio == '3' or $tipoRelatorio == '1') { ?>

    <tr>
      <th  bgcolor="#D3D3D3" style="font-size: 12px; " >SAÍDAS</th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px;" ></th>   
      <th  bgcolor="#D3D3D3" style="font-size: 12px;" ></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px;" ></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px;" ></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px;" ></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px;" ></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px;" ></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px;" ></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px;" ></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px;" ></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px;" ></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px;" ></th>        
    </tr>

    <?php 

      $totalprevistodespesa1 = 0;
      $totalrealizadodespesa1 = 0;
      $totalprevistodespesa2 = 0;
      $totalrealizadodespesa2 = 0;
      $totalprevistodespesa3 = 0;
      $totalrealizadodespesa3 = 0;
      $totalprevistodespesa4 = 0;
      $totalrealizadodespesa4 = 0;
      $totalprevistodespesa5 = 0;
      $totalrealizadodespesa5 = 0;
      $totalprevistodespesa6 = 0;
      $totalrealizadodespesa6 = 0;
      $totalprevistodespesa7 = 0;
      $totalrealizadodespesa7 = 0;
      $totalprevistodespesa8 = 0;
      $totalrealizadodespesa8 = 0;
      $totalprevistodespesa9 = 0;
      $totalrealizadodespesa9 = 0;
      $totalprevistodespesa10 = 0;
      $totalrealizadodespesa10 = 0;
      $totalprevistodespesa11 = 0;
      $totalrealizadodespesa11 = 0;
      $totalprevistodespesa12 = 0;
      $totalrealizadodespesa12 = 0;


  foreach ($dadosdespesa as $r) { ?>

    <?php if ($periodoRelatorio == '1') { 

      $totalprevistodespesa1  = $totalprevistodespesa1  + str_replace(",",".",$r->previstodespesa1);
      $totalrealizadodespesa1 = $totalrealizadodespesa1 + str_replace(",",".",$r->realizadodespesa1);
      $totalprevistodespesa2  = $totalprevistodespesa2  + str_replace(",",".",$r->previstodespesa2);
      $totalrealizadodespesa2 = $totalrealizadodespesa2 + str_replace(",",".",$r->realizadodespesa2);
      $totalprevistodespesa3  = $totalprevistodespesa3  + str_replace(",",".",$r->previstodespesa3);
      $totalrealizadodespesa3 = $totalrealizadodespesa3 + str_replace(",",".",$r->realizadodespesa3);
      $totalprevistodespesa4  = $totalprevistodespesa4  + str_replace(",",".",$r->previstodespesa4);
      $totalrealizadodespesa4 = $totalrealizadodespesa4 + str_replace(",",".",$r->realizadodespesa4);
      $totalprevistodespesa5  = $totalprevistodespesa5  + str_replace(",",".",$r->previstodespesa5);
      $totalrealizadodespesa5 = $totalrealizadodespesa5 + str_replace(",",".",$r->realizadodespesa5);
      $totalprevistodespesa6  = $totalprevistodespesa6  + str_replace(",",".",$r->previstodespesa6);
      $totalrealizadodespesa6 = $totalrealizadodespesa6 + str_replace(",",".",$r->realizadodespesa6);

    ?>
    <tr>
      <td style="font-size: 12px; text-align: left;" ><?php echo $r->categoria ?></td>  
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->previstodespesa1,2,",",".") ?></td>
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->realizadodespesa1,2,",",".") ?></td>
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->previstodespesa2,2,",",".") ?></td>
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->realizadodespesa2,2,",",".") ?></td> 
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->previstodespesa3,2,",",".") ?></td>
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->realizadodespesa3,2,",",".") ?></td> 
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->previstodespesa4,2,",",".") ?></td>
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->realizadodespesa4,2,",",".") ?></td> 
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->previstodespesa5,2,",",".") ?></td>
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->realizadodespesa5,2,",",".") ?></td> 
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->previstodespesa6,2,",",".") ?></td>
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->realizadodespesa6,2,",",".") ?></td> 
    </tr> 

    <?php } else { 

      $totalprevistodespesa7   = $totalprevistodespesa7   + str_replace(",",".",$r->previstodespesa7);
      $totalrealizadodespesa7  = $totalrealizadodespesa7  + str_replace(",",".",$r->realizadodespesa7);
      $totalprevistodespesa8   = $totalprevistodespesa8   + str_replace(",",".",$r->previstodespesa8);
      $totalrealizadodespesa8  = $totalrealizadodespesa8  + str_replace(",",".",$r->realizadodespesa8);
      $totalprevistodespesa9   = $totalprevistodespesa9   + str_replace(",",".",$r->previstodespesa9);
      $totalrealizadodespesa9  = $totalrealizadodespesa9  + str_replace(",",".",$r->realizadodespesa9);
      $totalprevistodespesa10  = $totalprevistodespesa10  + str_replace(",",".",$r->previstodespesa10);
      $totalrealizadodespesa10 = $totalrealizadodespesa10 + str_replace(",",".",$r->realizadodespesa10);
      $totalprevistodespesa11  = $totalprevistodespesa11  + str_replace(",",".",$r->previstodespesa11);
      $totalrealizadodespesa11 = $totalrealizadodespesa11 + str_replace(",",".",$r->realizadodespesa11);
      $totalprevistodespesa12  = $totalprevistodespesa12  + str_replace(",",".",$r->previstodespesa12);
      $totalrealizadodespesa12 = $totalrealizadodespesa12 + str_replace(",",".",$r->realizadodespesa12);

    ?>

     <tr>
      <td style="font-size: 12px; text-align: left;" ><?php echo $r->categoria ?></td>  
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->previstodespesa7,2,",",".") ?></td>
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->realizadodespesa7,2,",",".") ?></td>
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->previstodespesa8,2,",",".") ?></td>
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->realizadodespesa8,2,",",".") ?></td> 
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->previstodespesa9,2,",",".") ?></td>
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->realizadodespesa9,2,",",".") ?></td> 
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->previstodespesa10,2,",",".") ?></td>
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->realizadodespesa10,2,",",".") ?></td> 
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->previstodespesa11,2,",",".") ?></td>
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->realizadodespesa11,2,",",".") ?></td> 
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->previstodespesa12,2,",",".") ?></td>
      <td style="font-size: 12px; text-align: right;" ><?php echo number_format($r->realizadodespesa12,2,",",".") ?></td> 
     </tr>  

    <?php } ?>

  <?php } ?> 

  <?php if ($periodoRelatorio == '1') { ?>
    <tr>
      <th  bgcolor="#D3D3D3" style="font-size: 12px; " >TOTAL DAS SAÍDAS</th>
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalprevistodespesa1,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalrealizadodespesa1,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalprevistodespesa2,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalrealizadodespesa2,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalprevistodespesa3,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalrealizadodespesa3,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalprevistodespesa4,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalrealizadodespesa4,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalprevistodespesa5,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalrealizadodespesa5,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalprevistodespesa6,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalrealizadodespesa6,2,",",".")?></th>   
    </tr>

    <?php } else { ?>

     <tr>
      <th  bgcolor="#D3D3D3" style="font-size: 12px; " >TOTAL DAS SAÍDAS</th>
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalprevistodespesa7,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalrealizadodespesa7,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalprevistodespesa8,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalrealizadodespesa8,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalprevistodespesa9,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalrealizadodespesa9,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalprevistodespesa10,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalrealizadodespesa10,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalprevistodespesa11,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalrealizadodespesa11,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalprevistodespesa12,2,",",".")?></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalrealizadodespesa12,2,",",".")?></th>     
     </tr>  

    <?php } ?>
    <?php } ?>


    <?php if ($tipoRelatorio == '1') { ?>

    <?php if ($periodoRelatorio == '1') { ?>

        <tr>
          <th  bgcolor="#D3D3D3" style="font-size: 12px; " >1 (ENTRADAS - SAÍDAS)</th>
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalprevistoreceita1 - $totalprevistodespesa1,2,",",".") ?></th> 
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalrealizadoreceita1 - $totalrealizadodespesa1,2,",",".")?></th> 
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalprevistoreceita2 - $totalprevistodespesa2,2,",",".") ?></th> 
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalrealizadoreceita2 - $totalrealizadodespesa2,2,",",".")?></th> 
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalprevistoreceita3 - $totalprevistodespesa3,2,",",".") ?></th> 
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalrealizadoreceita3 - $totalrealizadodespesa3,2,",",".")?></th> 
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalprevistoreceita4 - $totalprevistodespesa4,2,",",".") ?></th> 
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalrealizadoreceita4 - $totalrealizadodespesa4,2,",",".")?></th> 
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalprevistoreceita5 - $totalprevistodespesa5,2,",",".") ?></th> 
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalrealizadoreceita5 - $totalrealizadodespesa5,2,",",".")?></th> 
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalprevistoreceita6 - $totalprevistodespesa6,2,",",".") ?></th> 
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalrealizadoreceita6 - $totalrealizadodespesa6,2,",",".")?></th>   
        </tr> 

        <tr>
          <th  bgcolor="#D3D3D3" style="font-size: 12px; " >2 SALDO ANTERIOR</th> 
          <th  style="font-size: 12px;" ></th>   
          <th  style="font-size: 12px;" ></th> 
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format($totalprevistoreceita1 - $totalprevistodespesa1,2,",",".") ?></th> 
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format($totalrealizadoreceita1 - $totalrealizadodespesa1,2,",",".")?></th> 
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($totalprevistoreceita2 - $totalprevistodespesa2) + ($totalprevistoreceita1 - $totalprevistodespesa1),2,",",".") ?></th> 
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($totalrealizadoreceita2 - $totalrealizadodespesa2) + ($totalrealizadoreceita1 - $totalrealizadodespesa1),2,",",".") ?></th> 
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($totalprevistoreceita3 - $totalprevistodespesa3) + ($totalprevistoreceita2 - $totalprevistodespesa2) + ($totalprevistoreceita1 - $totalprevistodespesa1),2,",",".") ?></th> 
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($totalrealizadoreceita3 - $totalrealizadodespesa3) + ($totalrealizadoreceita2 - $totalrealizadodespesa2) + ($totalrealizadoreceita1 - $totalrealizadodespesa1),2,",",".") ?></th>
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($totalprevistoreceita4 - $totalprevistodespesa4) + ($totalprevistoreceita3 - $totalprevistodespesa3) + ($totalprevistoreceita2 - $totalprevistodespesa2) + ($totalprevistoreceita1 - $totalprevistodespesa1),2,",",".") ?></th>
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($totalrealizadoreceita4 - $totalrealizadodespesa4) + ($totalrealizadoreceita3 - $totalrealizadodespesa3) + ($totalrealizadoreceita2 - $totalrealizadodespesa2) + ($totalrealizadoreceita1 - $totalrealizadodespesa1),2,",",".") ?></th> 
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($totalprevistoreceita5 - $totalprevistodespesa5) + ($totalprevistoreceita4 - $totalprevistodespesa4) + ($totalprevistoreceita3 - $totalprevistodespesa3) + ($totalprevistoreceita2 - $totalprevistodespesa2) + ($totalprevistoreceita1 - $totalprevistodespesa1),2,",",".") ?></th>
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($totalrealizadoreceita5 - $totalrealizadodespesa5) + ($totalrealizadoreceita4 - $totalrealizadodespesa4) + ($totalrealizadoreceita3 - $totalrealizadodespesa3) + ($totalrealizadoreceita2 - $totalrealizadodespesa2) + ($totalrealizadoreceita1 - $totalrealizadodespesa1),2,",",".") ?></th>
        </tr>

        <tr>
          <th  bgcolor="#D3D3D3" style="font-size: 12px; " >3 SALDO FINAL</th> 
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format($totalprevistoreceita1 - $totalprevistodespesa1,2,",",".") ?></th> 
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format($totalrealizadoreceita1 - $totalrealizadodespesa1,2,",",".")?></th>  
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($totalprevistoreceita2 - $totalprevistodespesa2) + ($totalprevistoreceita1 - $totalprevistodespesa1),2,",",".") ?></th> 
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($totalrealizadoreceita2 - $totalrealizadodespesa2) + ($totalrealizadoreceita1 - $totalrealizadodespesa1),2,",",".") ?></th> 
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($totalprevistoreceita3 - $totalprevistodespesa3) + ($totalprevistoreceita2 - $totalprevistodespesa2) + ($totalprevistoreceita1 - $totalprevistodespesa1),2,",",".") ?></th> 
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($totalrealizadoreceita3 - $totalrealizadodespesa3) + ($totalrealizadoreceita2 - $totalrealizadodespesa2) + ($totalrealizadoreceita1 - $totalrealizadodespesa1),2,",",".") ?></th>
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($totalprevistoreceita4 - $totalprevistodespesa4) + ($totalprevistoreceita3 - $totalprevistodespesa3) + ($totalprevistoreceita2 - $totalprevistodespesa2) + ($totalprevistoreceita1 - $totalprevistodespesa1),2,",",".") ?></th>
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($totalrealizadoreceita4 - $totalrealizadodespesa4) + ($totalrealizadoreceita3 - $totalrealizadodespesa3) + ($totalrealizadoreceita2 - $totalrealizadodespesa2) + ($totalrealizadoreceita1 - $totalrealizadodespesa1),2,",",".") ?></th>
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($totalprevistoreceita5 - $totalprevistodespesa5) + ($totalprevistoreceita4 - $totalprevistodespesa4) + ($totalprevistoreceita3 - $totalprevistodespesa3) + ($totalprevistoreceita2 - $totalprevistodespesa2) + ($totalprevistoreceita1 - $totalprevistodespesa1),2,",",".") ?></th>
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($totalrealizadoreceita5 - $totalrealizadodespesa5) + ($totalrealizadoreceita4 - $totalrealizadodespesa4) + ($totalrealizadoreceita3 - $totalrealizadodespesa3) + ($totalrealizadoreceita2 - $totalrealizadodespesa2) + ($totalrealizadoreceita1 - $totalrealizadodespesa1),2,",",".") ?></th>
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($totalprevistoreceita6 - $totalprevistodespesa6) + ($totalprevistoreceita5 - $totalprevistodespesa5) + ($totalprevistoreceita4 - $totalprevistodespesa4) + ($totalprevistoreceita3 - $totalprevistodespesa3) + ($totalprevistoreceita2 - $totalprevistodespesa2) + ($totalprevistoreceita1 - $totalprevistodespesa1),2,",",".") ?></th>
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($totalrealizadoreceita6 - $totalrealizadodespesa6) + ($totalrealizadoreceita5 - $totalrealizadodespesa5) + ($totalrealizadoreceita4 - $totalrealizadodespesa4) + ($totalrealizadoreceita3 - $totalrealizadodespesa3) + ($totalrealizadoreceita2 - $totalrealizadodespesa2) + ($totalrealizadoreceita1 - $totalrealizadodespesa1),2,",",".") ?></th>  
        </tr>

    <?php } else { ?>

        <tr>
          <th  bgcolor="#D3D3D3" style="font-size: 12px; " >1 (ENTRADAS - SAÍDAS)</th>
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalprevistoreceita7 - $totalprevistodespesa7, 2,",",".")  ?></th> 
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalrealizadoreceita7 - $totalrealizadodespesa7,2,",",".") ?></th> 
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalprevistoreceita8 - $totalprevistodespesa8 ,2,",",".")  ?></th> 
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalrealizadoreceita8 - $totalrealizadodespesa8,2,",",".") ?></th> 
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalprevistoreceita9 - $totalprevistodespesa9,2,",",".")  ?></th> 
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalrealizadoreceita9 - $totalrealizadodespesa9,2,",",".") ?></th> 
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalprevistoreceita10 - $totalprevistodespesa10,2,",",".") ?></th> 
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalrealizadoreceita10 - $totalrealizadodespesa10,2,",",".")?></th> 
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalprevistoreceita11 - $totalprevistodespesa11,2,",",".") ?></th> 
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalrealizadoreceita11 - $totalrealizadodespesa11,2,",",".")?></th> 
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalprevistoreceita12 - $totalprevistodespesa12,2,",",".") ?></th> 
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right; " ><?php echo number_format($totalrealizadoreceita12 - $totalrealizadodespesa12,2,",",".")?></th>     
        </tr>  

        <tr>
          <th  bgcolor="#D3D3D3" style="font-size: 12px;" >2 SALDO ANTERIOR</th> 
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format($previstoAntetior,2,",",".");?></th>
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format($realizadoAntetior,2,",",".");?></th>
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($previstoAntetior) + ($totalprevistoreceita7 - $totalprevistodespesa7),2,",",".") ?></th>
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($realizadoAntetior) +  ($totalrealizadoreceita7 - $totalrealizadodespesa7),2,",",".") ?></th>
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($previstoAntetior) + ($totalprevistoreceita7 - $totalprevistodespesa7) + ($totalprevistoreceita8 - $totalprevistodespesa8),2,",",".") ?></th>
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($realizadoAntetior) +  ($totalrealizadoreceita7 - $totalrealizadodespesa7) + ($totalrealizadoreceita8 - $totalrealizadodespesa8),2,",",".") ?></th>
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($previstoAntetior) + ($totalprevistoreceita7 - $totalprevistodespesa7) + ($totalprevistoreceita8 - $totalprevistodespesa8) + ($totalprevistoreceita9 - $totalprevistodespesa9),2,",",".") ?></th>
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($realizadoAntetior) +  ($totalrealizadoreceita7 - $totalrealizadodespesa7) + ($totalrealizadoreceita8 - $totalrealizadodespesa8) + ($totalrealizadoreceita9 - $totalrealizadodespesa9),2,",",".") ?></th>
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($previstoAntetior) + ($totalprevistoreceita7 - $totalprevistodespesa7) + ($totalprevistoreceita8 - $totalprevistodespesa8) + ($totalprevistoreceita9 - $totalprevistodespesa9) + ($totalprevistoreceita10 - $totalprevistodespesa10),2,",",".")?></th>
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($realizadoAntetior) +  ($totalrealizadoreceita7 - $totalrealizadodespesa7) + ($totalrealizadoreceita8 - $totalrealizadodespesa8) + ($totalrealizadoreceita9 - $totalrealizadodespesa9) + ($totalrealizadoreceita10 - $totalrealizadodespesa10),2,",",".") ?></th>
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($previstoAntetior) + ($totalprevistoreceita7 - $totalprevistodespesa7) + ($totalprevistoreceita8 - $totalprevistodespesa8) + ($totalprevistoreceita9 - $totalprevistodespesa9) + ($totalprevistoreceita10 - $totalprevistodespesa10) + ($totalprevistoreceita11 - $totalprevistodespesa11),2,",",".")?></th>
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($realizadoAntetior) +  ($totalrealizadoreceita7 - $totalrealizadodespesa7) + ($totalrealizadoreceita8 - $totalrealizadodespesa8) + ($totalrealizadoreceita9 - $totalrealizadodespesa9) + ($totalrealizadoreceita10 - $totalrealizadodespesa10) + ($totalrealizadoreceita11 - $totalrealizadodespesa11),2,",",".") ?></th>
        </tr>


        <tr>
          <th  bgcolor="#D3D3D3" style="font-size: 12px; " >3 SALDO FINAL</th> 
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($previstoAntetior) + ($totalprevistoreceita7 - $totalprevistodespesa7),2,",",".") ?></th>
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($realizadoAntetior) +  ($totalrealizadoreceita7 - $totalrealizadodespesa7),2,",",".") ?></th>
           <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($previstoAntetior) + ($totalprevistoreceita7 - $totalprevistodespesa7) + ($totalprevistoreceita8 - $totalprevistodespesa8),2,",",".") ?></th>
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($realizadoAntetior) +  ($totalrealizadoreceita7 - $totalrealizadodespesa7) + ($totalrealizadoreceita8 - $totalrealizadodespesa8),2,",",".") ?></th>
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($previstoAntetior) + ($totalprevistoreceita7 - $totalprevistodespesa7) + ($totalprevistoreceita8 - $totalprevistodespesa8) + ($totalprevistoreceita9 - $totalprevistodespesa9),2,",",".") ?></th>
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($realizadoAntetior) +  ($totalrealizadoreceita7 - $totalrealizadodespesa7) + ($totalrealizadoreceita8 - $totalrealizadodespesa8) + ($totalrealizadoreceita9 - $totalrealizadodespesa9),2,",",".") ?></th>
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($previstoAntetior) + ($totalprevistoreceita7 - $totalprevistodespesa7) + ($totalprevistoreceita8 - $totalprevistodespesa8) + ($totalprevistoreceita9 - $totalprevistodespesa9) + ($totalprevistoreceita10 - $totalprevistodespesa10),2,",",".")?></th>
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($realizadoAntetior) +  ($totalrealizadoreceita7 - $totalrealizadodespesa7) + ($totalrealizadoreceita8 - $totalrealizadodespesa8) + ($totalrealizadoreceita9 - $totalrealizadodespesa9) + ($totalrealizadoreceita10 - $totalrealizadodespesa10),2,",",".") ?></th>
           <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($previstoAntetior) + ($totalprevistoreceita7 - $totalprevistodespesa7) + ($totalprevistoreceita8 - $totalprevistodespesa8) + ($totalprevistoreceita9 - $totalprevistodespesa9) + ($totalprevistoreceita10 - $totalprevistodespesa10) + ($totalprevistoreceita11 - $totalprevistodespesa11),2,",",".")?></th>
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($realizadoAntetior) +  ($totalrealizadoreceita7 - $totalrealizadodespesa7) + ($totalrealizadoreceita8 - $totalrealizadodespesa8) + ($totalrealizadoreceita9 - $totalrealizadodespesa9) + ($totalrealizadoreceita10 - $totalrealizadodespesa10) + ($totalrealizadoreceita11 - $totalrealizadodespesa11),2,",",".") ?></th>
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($previstoAntetior) + ($totalprevistoreceita7 - $totalprevistodespesa7) + ($totalprevistoreceita8 - $totalprevistodespesa8) + ($totalprevistoreceita9 - $totalprevistodespesa9) + ($totalprevistoreceita10 - $totalprevistodespesa10) + ($totalprevistoreceita11 - $totalprevistodespesa11) + ($totalprevistoreceita12 - $totalprevistodespesa12),2,",",".")?></th>
          <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format(($realizadoAntetior) +  ($totalrealizadoreceita7 - $totalrealizadodespesa7) + ($totalrealizadoreceita8 - $totalrealizadodespesa8) + ($totalrealizadoreceita9 - $totalrealizadodespesa9) + ($totalrealizadoreceita10 - $totalrealizadodespesa10) + ($totalrealizadoreceita11 - $totalrealizadodespesa11) + ($totalrealizadoreceita12 - $totalrealizadodespesa12),2,",",".") ?></th>
        </tr>

    <?php } ?>
    <?php } ?>


   
  <thead>
  <tbody>



</table>
