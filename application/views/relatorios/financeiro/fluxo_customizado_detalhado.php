

<hr style="margin-top:-10px">
<h3 class="text-center" style="margin-top:-10px">Relatório Financeiro Detalhado</h3>


<table>
  </thead>
<!--     <tr>
  		<th style="font-size: 12px;" >&nbsp; Data Lançamento</th>
      <th style="font-size: 12px;" >&nbsp; Cliente / Fornecedor</th>
      <th style="font-size: 12px;" >&nbsp; Valor </th>
      <th style="font-size: 12px;" >&nbsp; Data Pagamento</th>
      <th style="font-size: 12px;" >&nbsp; Forma de Pagamento</th>
      <th style="font-size: 12px;" >&nbsp; Tipo</th>
      <th style="font-size: 12px;" >&nbsp; Situação</th>
  	</tr>  -->
  <thead>

      <?php
        $totalReceita = 0;
        $totalDespesa = 0;
        $totalReembolso = 0;

        for ($i=0; $i < count($dados) ; $i++){
          $vencimento = date('d/m/Y', strtotime($dados[$i]['dia'])); 

          $totalReceita = $totalReceita + $dados[$i]['receita'];
          $totalDespesa = $totalDespesa + $dados[$i]['despesa'];
          $totalReembolso = $totalReembolso + $dados[$i]['reembolso'];
      ?>


  <tbody>

      <tr class="table-primary">                                                        
        <td style="font-size: 12px;" >Data Lançamento</td>
        <td style="font-size: 12px;" >Receita</td> 
        <td style="font-size: 12px;" >Despesa</td> 
        <td style="font-size: 12px;" >Reembolso</td> 
        <td style="font-size: 12px;" >Saldo</td>                   
      </tr>

      <tr class="black">
        <td class="black"><?php echo $vencimento ?></td>
        <td class="black" style="color: green; font-weight: bold;"><?php echo $dados[$i]['receita'] ?></td>
        <td class="black" style="color: red; font-weight: bold;"><?php echo $dados[$i]['despesa']  ?></td>
        <td class="black" style="color: blue; font-weight: bold;"><?php echo $dados[$i]['reembolso'] ?></td>
        <td class="black" style="color: blue; font-weight: bold;"><?php echo $dados[$i]['saldo'] ?></td>       
      </tr>

      <tr class="table-primary">                                                        
        <td style="font-size: 12px;" >&nbsp;</td>
        <td style="font-size: 12px;" >&nbsp;</td> 
        <td style="font-size: 12px;" >&nbsp;</td> 
        <td style="font-size: 12px;" >&nbsp;</td> 
        <td style="font-size: 12px;" >&nbsp;</td>                   
      </tr>

      <tr class="table-primary">                                                        
        <td style="font-size: 12px;" >Data Lançamento</td>
        <td style="font-size: 12px;" >Cliente / Fornecedor</td> 
        <td style="font-size: 12px;" >Valor</td> 
        <td style="font-size: 12px;" >Tipo</td> 
        <td style="font-size: 12px;" >Situação</td>                   
      </tr>

      <?php            
        
          for($j=0; $j < count($itens) ; $j++){ 
            $vencimentoItens = date('d/m/Y', strtotime($itens[$j]['dia'])); 
           
            if ($vencimento ==  $vencimentoItens) {
             // $TOTAL += $itensvenda[$j]->quantidade * $itensvenda[$j]->precoCompra;
              if($itens[$j]['financeiro_baixado'] == 0){$status = 'Pendente';}else{ $status = 'Pago';};
          ?>

          <tr class="black">
            <td class="black"><?php echo $itens[$j]['dia'] ?></td>
            <td class="black"><?php echo $itens[$j]['financeiro_forn_clie'].'<br>'.$itens[$j]['financeiro_descricao']; ?></td>
            <td class="black"><?php echo $itens[$j]['financeiro_valor']  ?></td>
            <td class="black"><?php echo $itens[$j]['financeiro_tipo'] ?></td>
            <td class="black"><?php echo $status ?></td>       
          </tr>


      <?php  } }  ?>

  </tbody> 

  <?php  }  ?>

</table>

<br class="clear">
<br class="clear">

<table>
  <tr>
    <th colspan="2">&nbsp; LAÇAMENTOS:<?php echo ' '.count($dados). ' DIAS';  ?></th>
    <?php  if ($tipoRelatorio == '1' or $tipoRelatorio == '2') { ?>                              
    <th style="color: green; font-weight: bold;">&nbsp; RECEITAS:<?php echo ' '.number_format($totalReceita,2,",","."); ?></th>
    <?php } ?> 
    <?php  if ($tipoRelatorio == '1' or $tipoRelatorio == '3') { ?>  
    <th style="color: red; font-weight: bold;">&nbsp; DESPESAS:<?php echo ' '.number_format($totalDespesa,2,",","."); ?></th>
    <?php } ?> 
    <th style="color: blue; font-weight: bold;" colspan="2">&nbsp; SALDO:<?php echo ' '.number_format($totalReceita - ($totalDespesa + $totalReembolso),2,",","."); ; ?></th>      
  </tr> 
</table>
