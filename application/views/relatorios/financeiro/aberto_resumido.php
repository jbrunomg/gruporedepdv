<?php 
    @set_time_limit( 300 );
    ini_set('max_execution_time', 300);
    ini_set('max_input_time', 300);
    ini_set('memory_limit', '512M');
?>

<hr style="margin-top:-10px">
<h4 class="text-center" style="margin-top:-10px">Relatório Financeiro - <?php echo $tipo ?><center><?php echo $dataMes.'/'.$dataAno;?></center></h4>



<table>
  <thead>
  	<tr>
      <th style="font-size: 12px;" >&nbsp;</th>
  		<th style="font-size: 12px;" >&nbsp; VALOR</th>  		      
  	</tr>

    <tr>
      <th  bgcolor="#D3D3D3" style="font-size: 12px; width:250px;" >CLIENTES</th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px;" ></th>   >        
    </tr>

    <?php $totalGeral = 0; foreach ($dados as $d) {  $totalGeral = $totalGeral + $d->valor; ?>
      <tr>
        <td style="font-size: 12px; text-align: left;" ><?php echo $d->cliente ?></td>  
        <td style="font-size: 12px; text-align: right;" ><?php echo number_format($d->valor,2,",",".") ?></td>
      </tr> 
    <?php } ?>

    <tr>
      <th  bgcolor="#D3D3D3" style="font-size: 12px; width:250px;" >TOTAL</th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px; text-align: right;" ><?php echo number_format($totalGeral,2,",",".") ?></th>   >        
    </tr>
   
  </thead>
</table>
