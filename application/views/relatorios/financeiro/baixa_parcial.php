

<hr style="margin-top:-10px">
<h4 class="text-center" style="margin-top:-10px">Relatório Financeiro - Baixa Parcial<br>
<center><?php echo $tipo; echo $date;?></center></h4>



<table>
  </thead>
  	<tr class="even">
      <th style="font-size: 12px;" >&nbsp; Data</th>  
  		<th style="font-size: 12px;" >&nbsp; Usuario</th>  		
  		<th style="font-size: 12px;" >&nbsp; Cliente</th>
  		<th style="font-size: 12px;" >&nbsp; Total de Debito</th>
        <th style="font-size: 12px;" >&nbsp; Valor da Baixa</th>
  		<th style="font-size: 12px;" >&nbsp; Debito</th>      
  	</tr>
  <thead>
  <tbody>

	<?php
    $totalDebito = 0;
    $valorBaixa  = 0;
    $debito  = 0;

    foreach ($dados as $d) { 

      $totalDebito = $totalDebito + $d->valor_total;
      $valorBaixa  = $valorBaixa + $d->valor_baixa;
      $debito  = $debito + $d->valor_debito;  

  ?>
		<tr class="even">            
      <td><?php echo date('d-m-Y', strtotime($d->data_baixa));?></td>
      <td><?php echo $d->usuario_nome ?></td>		
      <td><?php echo $d->cliente_nome ?></td>	
      <td style="color: red; font-weight: bold;"><?php echo number_format($d->valor_total,2,",",".");?></td>
      <td style="color: green; font-weight: bold;"><?php echo number_format($d->valor_baixa,2,",",".");?></td>
      <td style="color: blue; font-weight: bold;"><?php echo number_format($d->valor_debito,2,",",".");?></td>	
		</tr> 
    <?php } ?>
    <tr class="even">            
      <td></td>
      <td></td>   
      <td></td> 
      <td></td>
      <td></td>
      <td></td> 
    </tr> 
    <tr class="even">            
      <td></td>
      <td></td>   
      <td></td> 
      <td style="color: black; font-weight: bold;"><?php echo number_format($totalDebito,2,",",".");?></td>
      <td style="color: black; font-weight: bold;"><?php echo number_format($valorBaixa,2,",",".");?></td>
      <td style="color: black; font-weight: bold;"><?php echo number_format($debito,2,",",".");?></td>  
    </tr> 

   </tbody> 


</table>
