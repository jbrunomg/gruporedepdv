

<hr style="margin-top:-10px">
<h3 class="text-center" style="margin-top:-10px">Relatório Financeiro Resumido</h3>



<table>
  </thead>
  	<tr>
  		<th style="font-size: 12px;" >&nbsp; Data Lançamento</th>
      <?php  if ($tipoRelatorio == '1' or $tipoRelatorio == '2') { ?>  
        <th style="font-size: 12px;" >&nbsp; Receita</th>
      <?php } ?> 
      <?php  if ($tipoRelatorio == '1' or $tipoRelatorio == '3') { ?>  			
  		<th style="font-size: 12px;" >&nbsp; Despesa</th>
      <?php } ?> 
      <th style="font-size: 12px;" >&nbsp; Reembolso</th>
  		<th style="font-size: 12px;" >&nbsp; Saldo</th>      
  	</tr>
  <thead>
  <tbody>

	<?php 
	$totalReceita = 0;
  $totalDespesa = 0;
  $totalReembolso = 0;

	foreach ($dados as $d) { 
    $vencimento = date('d/m/Y', strtotime($d['dia'])); 

    $totalReceita = $totalReceita + $d['receita'];
    $totalDespesa = $totalDespesa + $d['despesa'];
    $totalReembolso = $totalReembolso + $d['reembolso'];
  ?>

		<tr class="even">

      <td><?php echo $vencimento ?></td>		
      <?php  if ($tipoRelatorio == '1' or $tipoRelatorio == '2') { ?>  
			<td style="color: green; font-weight: bold;"><?php echo number_format($d['receita'],2,",",".");?></td>
      <?php } ?> 
      <?php  if ($tipoRelatorio == '1' or $tipoRelatorio == '3') { ?>  
      <td style="color: red; font-weight: bold;"><?php echo number_format($d['despesa'],2,",",".");?></td>
      <?php } ?> 
       <td style="color: blue; font-weight: bold;"><?php echo number_format($d['reembolso'],2,",",".");?></td>
      <td style="color: blue; font-weight: bold;"><?php echo number_format($d['saldo'],2,",",".");?></td>
		
		</tr>

  <?php } ?> 

  <tr>
    <th s>&nbsp; LAÇAMENTOS:<?php echo ' '.count($dados). ' DIAS';  ?></th>
    <?php  if ($tipoRelatorio == '1' or $tipoRelatorio == '2') { ?>                              
    <th style="color: green; font-weight: bold;">&nbsp; RECEITAS:<?php echo ' '.number_format($totalReceita,2,",","."); ?></th>
    <?php } ?> 
    <?php  if ($tipoRelatorio == '1' or $tipoRelatorio == '3') { ?>  
    <th style="color: red; font-weight: bold;">&nbsp; DESPESAS:<?php echo ' '.number_format($totalDespesa,2,",","."); ?></th>
    <?php } ?> 
    <th style="color: blue; font-weight: bold;">&nbsp; REEMBOLSO:<?php echo ' '.number_format($totalReembolso,2,",","."); ?></th>
    <th style="color: blue; font-weight: bold;">&nbsp; SALDO:<?php echo ' '.number_format($totalReceita - ($totalDespesa + $totalReembolso),2,",","."); ; ?></th>      
  </tr> 

    </tbody> 


</table>
