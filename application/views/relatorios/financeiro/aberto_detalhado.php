<?php 
    @set_time_limit( 300 );
    ini_set('max_execution_time', 300);
    ini_set('max_input_time', 300);
    ini_set('memory_limit', '512M');
?>

<hr style="margin-top:-10px">
<h4 class="text-center" style="margin-top:-10px">Relatório Financeiro - <?php echo $tipo ?><center><?php echo $dataAno;?></center></h4>



<table>
  <thead>
  	<tr>
      <th style="font-size: 12px;" >MES</th> 
      <th style="font-size: 12px;" ><center>1</center></th>
  		<th style="font-size: 12px;" ><center>2</center></th>
      <th style="font-size: 12px;" ><center>3</center></th>
      <th style="font-size: 12px;" ><center>4</center></th> 
      <th style="font-size: 12px;" ><center>5</center></th>
      <th style="font-size: 12px;" ><center>6</center></th> 
      <th style="font-size: 12px;" ><center>7</center></th>
      <th style="font-size: 12px;" ><center>8</center></th> 
      <th style="font-size: 12px;" ><center>9</center></th>
      <th style="font-size: 12px;" ><center>10</center></th> 
      <th style="font-size: 12px;" ><center>11</center></th>
      <th style="font-size: 12px;" ><center>12</center></th>  
  	</tr>

    <tr>
      <th  bgcolor="#D3D3D3" style="font-size: 12px; width:50px;" >CLIENTES</th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px;  text-align: right;" ></th>
      <th  bgcolor="#D3D3D3" style="font-size: 12px;  text-align: right;" ></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px;  text-align: right;" ></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px;  text-align: right;" ></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px;  text-align: right;" ></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px;  text-align: right;" ></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px;  text-align: right;" ></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px;  text-align: right;" ></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px;  text-align: right;" ></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px;  text-align: right;" ></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px;  text-align: right;" ></th> 
      <th  bgcolor="#D3D3D3" style="font-size: 12px;  text-align: right;" ></th>      
    </tr>

    <?php 
        $totalGeral = 0;
        $totalGeral01  = 0;
        $totalGeral02  = 0;
        $totalGeral03  = 0;
        $totalGeral04  = 0;
        $totalGeral05  = 0;
        $totalGeral06  = 0;
        $totalGeral07  = 0;
        $totalGeral08  = 0;
        $totalGeral09  = 0;
        $totalGeral10  = 0;
        $totalGeral11  = 0;
        $totalGeral12  = 0;
       
     foreach ($dados as $d) { 

        $totalGeral = $totalGeral + $d->valor; 
        $totalGeral01  = $totalGeral01 + $d->pendente1; 
        $totalGeral02  = $totalGeral02 + $d->pendente2;  
        $totalGeral03  = $totalGeral03 + $d->pendente3;  
        $totalGeral04  = $totalGeral04 + $d->pendente4;   
        $totalGeral05  = $totalGeral05 + $d->pendente5; 
        $totalGeral06  = $totalGeral06 + $d->pendente6;  
        $totalGeral07  = $totalGeral07 + $d->pendente7; 
        $totalGeral08  = $totalGeral08 + $d->pendente8; 
        $totalGeral09  = $totalGeral09 + $d->pendente9;   
        $totalGeral10  = $totalGeral10 + $d->pendente10; 
        $totalGeral11  = $totalGeral11 + $d->pendente11;
        $totalGeral12  = $totalGeral12 + $d->pendente12; 

      ?>
      <tr>
        <td style="font-size: 12px; text-align: left;" ><?php echo $d->cliente ?></td>
        <td style="font-size: 12px;" ><?php echo $d->mes == 1 ? number_format($d->valor,2,",",".") : '0,00' ?></center></td>
        <td style="font-size: 12px;" ><?php echo $d->mes == 2 ? number_format($d->valor,2,",",".") : '0,00' ?></center></td>
        <td style="font-size: 12px;" ><?php echo $d->mes == 3 ? number_format($d->valor,2,",",".") : '0,00' ?></center></td>
        <td style="font-size: 12px;" ><?php echo $d->mes == 4 ? number_format($d->valor,2,",",".") : '0,00' ?></center></td> 
        <td style="font-size: 12px;" ><?php echo $d->mes == 5 ? number_format($d->valor,2,",",".") : '0,00' ?></center></td>
        <td style="font-size: 12px;" ><?php echo $d->mes == 6 ? number_format($d->valor,2,",",".") : '0,00' ?></center></td> 
        <td style="font-size: 12px;" ><?php echo $d->mes == 7 ? number_format($d->valor,2,",",".") : '0,00' ?></center></td>
        <td style="font-size: 12px;" ><?php echo $d->mes == 8 ? number_format($d->valor,2,",",".") : '0,00' ?></center></td> 
        <td style="font-size: 12px;" ><?php echo $d->mes == 9 ? number_format($d->valor,2,",",".") : '0,00' ?></center></td>
        <td style="font-size: 12px;" ><?php echo $d->mes == 10 ? number_format($d->valor,2,",",".") : '0,00' ?></center></td> 
        <td style="font-size: 12px;" ><?php echo $d->mes == 11 ? number_format($d->valor,2,",",".") : '0,00' ?></center></td>
        <td style="font-size: 12px;" ><?php echo $d->mes == 12 ? number_format($d->valor,2,",",".") : '0,00' ?></center></td>  
      </tr> 
    <?php } ?>

    <tr>
      <th  bgcolor="#D3D3D3" style="font-size: 12px; width:250px;" >TOTAL</th>
      <th style="font-size: 12px;" ><?php echo number_format($totalGeral01,2,",",".") ?></center></th>
      <th style="font-size: 12px;" ><?php echo number_format($totalGeral02,2,",",".") ?></center></th>
      <th style="font-size: 12px;" ><?php echo number_format($totalGeral03,2,",",".") ?></center></th>
      <th style="font-size: 12px;" ><?php echo number_format($totalGeral04,2,",",".") ?></center></th> 
      <th style="font-size: 12px;" ><?php echo number_format($totalGeral05,2,",",".") ?></center></th>
      <th style="font-size: 12px;" ><?php echo number_format($totalGeral06,2,",",".") ?></center></th> 
      <th style="font-size: 12px;" ><?php echo number_format($totalGeral07,2,",",".") ?></center></th>
      <th style="font-size: 12px;" ><?php echo number_format($totalGeral08,2,",",".") ?></center></th> 
      <th style="font-size: 12px;" ><?php echo number_format($totalGeral09,2,",",".") ?></center></th>
      <th style="font-size: 12px;" ><?php echo number_format($totalGeral10,2,",",".") ?></center></th> 
      <th style="font-size: 12px;" ><?php echo number_format($totalGeral11,2,",",".") ?></center></th>
      <th style="font-size: 12px;" ><?php echo number_format($totalGeral12,2,",",".") ?></center></th>  
    </tr>

   
  </thead>
</table>
