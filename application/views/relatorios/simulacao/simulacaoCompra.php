
<section class="content">
	<div class="row">
		<div class="col-md-12">
          <form action="<?php echo base_url() ?>relatorios/simulacaoCompra/simulacaoCompra_padrao" method='POST' >
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios Customizáveis</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Simulação de</label>
									<div class="input-group date">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="text" class="form-control pull-right" id="datepickerVendaIncio" placeholder="dd/mm/aaaa" name="dataInicio">
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>até</label>
									<div class="input-group date">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="text" class="form-control pull-right" id="datepickerVendaFim" placeholder="dd/mm/aaaa" name="dataFim">
									</div>
								</div>
							</div>
						</div>
						<div class="row">
						   <div class="col-md-6">
								<div class="form-group">
									<label>Lojas</label>
						                <select id="lojas" class="form-control" data-placeholder="Selecione as lojas" name="lojas" required="required">
						                 <option value="">Selecione</option>
						                  <?php 
						                  foreach ($lojas as $s) {
						                    echo "<option value='".$s['lojas']."'>".strtoupper($s['lojas'])."</option>";
						                  }
						                  ?>                        
						                </select>
								</div>
						   </div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Grupo</label>		
									<select id="produto_categoria_id" class="form-control" data-placeholder="Selecione o grupo de produtos" name="produto_categoria_id" required="required">
					                  <option value="">Selecione</option>
					                  <?php foreach ($grupo as $g) { ?>
					                  <option value='<?php echo $g->categoria_prod_id; ?>'><?php echo $g->categoria_prod_descricao; ?> </option>
					                  <?php } ?>                       
					                </select>
								</div>
							</div>
						</div>
					</div>
					<div class="row no-print text-center" style="margin-top: 40px">
						<div class="col-md-12">
							<button type="reset" class="btn btn-default">
								<i class="fa fa-eraser"></i> Limpar
							</button>
							<button type="submit" class="btn btn-primary" style="margin-right: 5px;">
								<i class="fa fa-print"></i> Imprimir
							</button>
						</div>
					</div>
				</div>
			</div>
		  </form>
		</div>

	</div>
</section>
