<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <!-- <title>Relatórios</title> -->

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>  


    <div id="head" style="padding-top: 9px; display: table;">

        <h1 style="width:200px; float:left;">
          <img style=" width:150px; display: table-cell; vertical-align: middle; margin: 0 auto;" src="<?php echo base_url(); ?>assets/img/<?php echo $emitente[0]->emitente_url_logo  ?>"  width="100" height="100">
        </h1>
        <p style="font-size: 12px; display: block; margin: 4px; text-align: right;"><?php echo $emitente[0]->emitente_nome  ?> </p>
        <p style="font-size: 12px; display: block; margin: 4px; text-align: right;"><?php echo $emitente[0]->emitente_telefone  ?> </p>
        <p style="font-size: 12px; display: block; margin: 4px; text-align: right;"><?php echo $emitente[0]->emitente_email
          ?> </p>
        <p style="font-size: 12px; display: block; margin: 4px; text-align: right;">Data: <?php echo date('d/m/Y');?></p>
        <p style="font-size: 12px; display: block; margin: 4px; text-align: right;">Hora: <?php echo date('H:i:s');?></p>
        <br class="clear">
    </div>

    <?php $this->load->view($meio); ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  </body>
</html>

<?php 
    // @set_time_limit( 300 );
    // ini_set('max_execution_time', 300);
    // ini_set('max_input_time', 300);
    // ini_set('memory_limit', '1024M');

    // PHP >= 7
    ini_set('max_execution_time', '600');
    ini_set("pcre.backtrack_limit", "5000000");
    ini_set('memory_limit', '1024M');

?>