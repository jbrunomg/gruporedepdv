
<section class="content">
	<div class="row">
	  <div class="col-md-3">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios Rápidos</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					
					<a href="<?php echo base_url() ?>relatorios/vendas/vendasDia" class="small-box-footer" target="_blank">
					<div class="small-box bg-green">						
						<div class="inner">
							<h3><?php echo $totalVendasDia; ?></h3>

							<p>Venda do dia</p>
						</div>
						<div class="icon">
							<i class="fa fa-dropbox"></i>
						</div>						
					</div>
					</a>

 					<a href="<?php echo base_url() ?>relatorios/vendas/vendaMensal/atual" class="small-box-footer" target="_blank">
					<div class="small-box bg-blue">						
						<div class="inner">
							<h3><?php echo $totalVendasMesAtual; ?></h3>

							<p>Venda Mensal Atual</p>
						</div>
						<div class="icon">
							<i class="fa fa-dropbox"></i>
						</div>						
					</div>
					</a>


 					<a href="<?php echo base_url() ?>relatorios/vendas/vendaMensal/anterior" class="small-box-footer" target="_blank">
					<div class="small-box bg-aqua">						
						<div class="inner">
							<h3><?php echo $totalVendasMesAnterior; ?></h3>

							<p>Venda Mensal Anterior</p>
						</div>
						<div class="icon">
							<i class="fa fa-dropbox"></i>
						</div>						
					</div>
					</a>

					<a href="<?php echo base_url() ?>relatorios/vendas/vendaAnual" class="small-box-footer" target="_blank">
					<div class="small-box bg-red">
						
						<div class="inner">
							<h3><?php echo $totalVendasAnual; ?></h3>

							<p>Venda Anual (GERAL)</p>
						</div>
						<div class="icon">
							<i class="fa fa-dropbox"></i>
						</div>
						
					</div>
					</a> 

					<a href="<?php echo base_url() ?>relatorios/vendas/vendaAnualLoja" class="small-box-footer" target="_blank">
					<div class="small-box bg-orange">
						
						<div class="inner">
							<h3><?php echo $totalVendasAnualLoja; ?></h3>

							<p>Venda Anual (LOJA)</p>
						</div>
						<div class="icon">
							<i class="fa fa-dropbox"></i>
						</div>
						
					</div>
					</a> 

				</div>
			</div>
		</div>
		<div class="col-md-9">
          <form action="<?php echo base_url() ?>relatorios/vendasCustomizadas" method='POST' target="_blank">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios Customizáveis</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label>Venda de</label>
									<div class="input-group date">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="text" class="form-control pull-right" id="datepickerVendaIncio" placeholder="dd/mm/aaaa" name="dataInicio">
									</div>
								</div>
							</div>
<!-- 							<div class="col-md-6">
								<div class="form-group">
									<label>até</label>
									<div class="input-group date">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="text" class="form-control pull-right" id="datepickerVendaFim" placeholder="dd/mm/aaaa" name="dataFim">
									</div>
								</div>
							</div> -->
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Vendedor</label>
									   <input type="text" class="form-control" name="usuario_nome" id="usuario_nome" value="<?php set_value('usuario_nome'); ?>">
									   <input type="hidden" id="usuario_id" name="usuario_id" value="<?php echo set_value('usuario_id'); ?>">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Cliente</label>
									 <input type="text" class="form-control" name="cliente_nome" id="cliente_nomeR" value="<?php echo set_value('cliente_nome'); ?>" placeholder="Cliente">
                                     <input type="hidden" id="cliente_id" name="cliente_id" value="<?php echo set_value('cliente_id'); ?>">
								</div>
							</div>
						</div>
						<div class="row">
						   <div class="col-md-6">
								<div class="form-group">
									<label>Lojas</label>
						                <select id="lojas" class="form-control" data-placeholder="Selecione as lojas" name="lojas">
						                <!--  <option value="">Selecione</option> -->
						                  <?php 
						                  foreach ($lojas as $s) { ?>

						                  	 <option value="<?php echo $s['lojas']; ?>"<?php echo $s['lojas'] == $base ? 'selected':'';?>><?php echo strtoupper($s['lojas'])?></option> 
						                 <?php  } ?>                        
						                </select>
								</div>
						   </div>
						   <div class="col-md-6">
								<div class="form-group">
									<label>Ordenar por</label>
									<select class="form-control" name="ordenacao">
										<option value="1">Nome cliente A - Z</option>
										<option value="2">Nome cliente Z - A</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Tipo Relatorio</label>
									<select class="form-control" name="tipo">										
										<option value="1">Resumido</option>
										<option value="2">detalhado</option>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Faturado</label>
									<select class="form-control" name="faturado">
										<option value="">Ambos</option>
										<option value="1">Faturado</option>
										<option value="0">Não Faturado</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="row no-print text-center" style="margin-top: 40px">
						<div class="col-md-12">
							<button type="reset" class="btn btn-default">
								<i class="fa fa-eraser"></i> Limpar
							</button>
							<button type="submit" class="btn btn-primary" style="margin-right: 5px;">
								<i class="fa fa-print"></i> Imprimir
							</button>
						</div>
					</div>
				</div>
			</div>
		  </form>

			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios Customizáveis - Venda Mensal Grupo</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-12">
						<div class="row">
							<form action="<?php echo base_url()?>index.php/relatorios/vendasAnualGrupoCustomizado" method="post" target="_blank">
							
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Venda de</label>
										<div class="input-group date">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" class="form-control pull-right" id="datepickerGrupo" placeholder="mm/aaaa" name="dataInicio" required="required">
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Grupo</label>		
										<select id="produto_categoria_id" class="form-control" data-placeholder="Selecione o grupo de produtos" name="produto_categoria_id" >
						                  <option value="">TODAS CATEGORIAS</option>
						                  <?php foreach ($grupo as $g) { ?>
						                  <option value='<?php echo $g->categoria_prod_id; ?>'><?php echo $g->categoria_prod_descricao; ?> </option>
						                  <?php } ?>                       
						                </select>
									</div>
								</div>
							</div>
						
							<div class="row no-print text-center" style="margin-top: 40px">
								<div class="col-md-12">
									<button type="submit" class="btn btn-primary" style="margin-right: 50px;  width: 30%;">
										<i class="fa fa-print"></i> Imprimir
									</button>
								</div>
							</div>
						    </form>
						</div>
					</div>
				</div>
			</div>



			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios Customizáveis - Vendas Excluídas</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-12">
						<div class="row">
							<form action="<?php echo base_url()?>index.php/relatorios/vendasVendasExcluidasCustomizado" method="post" target="_blank">
							
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Venda de</label>
										<div class="input-group date">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" class="form-control pull-right" id="datepickerVendasExcluidas" placeholder="mm/aaaa" name="dataInicio" required="required">
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
									  <label>Vendedor</label>
										<select id="usuario_id" class="form-control" data-placeholder="Selecione o vendedor" name="usuario_id" >
						                  <option value="">TODOS VENDEDORES</option>
						                  <?php foreach ($vendedor as $v) { ?>
						                  <option value='<?php echo $v->usuario_id; ?>'><?php echo $v->usuario_nome; ?> </option>
						                  <?php } ?>                       
						                </select>
									</div>
								</div>
							</div>
						
							<div class="row no-print text-center" style="margin-top: 40px">
								<div class="col-md-12">
									<button type="submit" class="btn btn-primary" style="margin-right: 50px;  width: 30%;">
										<i class="fa fa-print"></i> Imprimir
									</button>
								</div>
							</div>
						    </form>
						</div>
					</div>
				</div>
			</div>

			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios Customizáveis - Vendas Anual</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-12">
						<div class="row">
							<form action="<?php echo base_url()?>index.php/relatorios/vendasAnualCustomizado" method="post" target="_blank">
							
							<div class="row">

								<div class="col-md-6">
									<div class="form-group">
										<label>Ano de</label>
										<div class="input-group date">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" class="form-control pull-right" id="datepickerVendasAnual" placeholder="aaaa" name="anoInicio" required="required">
										</div>
									</div>
								</div>
								
								<div class="col-md-6">
									<div class="form-group">
										<label>Tipo</label>
										<select class="form-control" name="tipo">
											<option value="vendaAnual">Todas as lojas</option>
											<option value="vendaAnualLoja">Loja atual</option>
										</select>
									</div>
								</div>
								

							</div>
						
							<div class="row no-print text-center" style="margin-top: 40px">
								<div class="col-md-12">
									<button type="submit" class="btn btn-primary" style="margin-right: 50px;  width: 30%;">
										<i class="fa fa-print"></i> Imprimir
									</button>
								</div>
							</div>
						    </form>
						</div>
					</div>
				</div>
			</div>


			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios Customizáveis - Vendas x Fornecedor</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-12">
						<div class="row">
							<form action="<?php echo base_url()?>index.php/relatorios/vendasFornecedorCustomizado" method="post" target="_blank">
							
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Venda de</label>
									<div class="input-group date">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="date" class="form-control pull-right"  placeholder="dd/mm/aaaa" name="dataInicio" required>
									</div>
								</div>
							</div>
 							<div class="col-md-6">
								<div class="form-group">
									<label>até</label>
									<div class="input-group date">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="date" class="form-control pull-right"  placeholder="dd/mm/aaaa" name="dataFim" required>
									</div>
								</div>
							</div> 
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
								  <label>Fornecedores</label>
									<select id="fornecedor_id" class="form-control" data-placeholder="Selecione o vendedor" name="fornecedor_id" >					                 
					                  <option value=''>TODOS FORNECEDORES</option>
					                  <?php foreach ($fornecedor as $v) { ?>
					                  <option value='<?php echo $v->fornecedor_id; ?>'><?php echo $v->fornecedor_nome; ?> </option>
					                  <?php } ?>                       
					                </select>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
								  <label>Vendedor</label>
									<select id="usuario_id" class="form-control" data-placeholder="Selecione o vendedor" name="usuario_id" >					                 
					                  <option value=''>TODOS VENDEDORES</option>
					                  <?php foreach ($vendedor as $v) { ?>
					                  <option value='<?php echo $v->usuario_id; ?>'><?php echo $v->usuario_nome; ?> </option>
					                  <?php } ?>                       
					                </select>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								    <div class="form-group">
										<label>Grupo/Categoria</label>		
										<select id="produto_categoria_id" class="form-control" data-placeholder="Selecione o grupo de produtos" name="produto_categoria_id" >
						                  <option value="">TODAS CATEGORIAS</option>
						                  <?php foreach ($grupo as $g) { ?>
						                  <option value='<?php echo $g->categoria_prod_id; ?>'><?php echo $g->categoria_prod_descricao; ?> </option>
						                  <?php } ?>                       
						                </select>
									</div>

							</div>
						</div>
						
							<div class="row no-print text-center" style="margin-top: 40px">
								<div class="col-md-12">
									<button type="submit" class="btn btn-primary" style="margin-right: 50px;  width: 30%;">
										<i class="fa fa-print"></i> Imprimir
									</button>
								</div>
							</div>
						    </form>
						</div>
					</div>
				</div>
			</div>


			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios - Vendas x Forma Pagamento</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-12">
						<div class="row">
							<form action="<?php echo base_url()?>index.php/relatorios/vendasPagamentoCustomizado" method="post" target="_blank">
							
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Venda de</label>
									<div class="input-group date">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="date" class="form-control pull-right"  placeholder="dd/mm/aaaa" name="dataInicio" required>
									</div>
								</div>
							</div>
 							<div class="col-md-6">
								<div class="form-group">
									<label>até</label>
									<div class="input-group date">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="date" class="form-control pull-right"  placeholder="dd/mm/aaaa" name="dataFim" required>
									</div>
								</div>
							</div> 
						</div>

						<div class="row">
			<!-- 				<div class="col-md-6">
								<div class="form-group">
								  <label>Fornecedores</label>
									<select id="fornecedor_id" class="form-control" data-placeholder="Selecione o vendedor" name="fornecedor_id" >					                 
					                  <option value=''>TODOS FORNECEDORES</option>
					                  <?php foreach ($fornecedor as $v) { ?>
					                  <option value='<?php echo $v->fornecedor_id; ?>'><?php echo $v->fornecedor_nome; ?> </option>
					                  <?php } ?>                       
					                </select>
								</div>
							</div> -->

							<div class="col-md-12">
								<div class="form-group">
								  <label>Vendedor</label>
									<select id="usuario_id" class="form-control" data-placeholder="Selecione o vendedor" name="usuario_id" >					                 
					                  <option value=''>TODOS VENDEDORES</option>
					                  <?php foreach ($vendedor as $v) { ?>
					                  <option value='<?php echo $v->usuario_id; ?>'><?php echo $v->usuario_nome; ?> </option>
					                  <?php } ?>                       
					                </select>
								</div>
							</div>
							
						</div>
<!-- 
						<div class="row">
							<div class="col-md-12">
								    <div class="form-group">
										<label>Grupo/Categoria</label>		
										<select id="produto_categoria_id" class="form-control" data-placeholder="Selecione o grupo de produtos" name="produto_categoria_id" >
						                  <option value="">TODAS CATEGORIAS</option>
						                  <?php foreach ($grupo as $g) { ?>
						                  <option value='<?php echo $g->categoria_prod_id; ?>'><?php echo $g->categoria_prod_descricao; ?> </option>
						                  <?php } ?>                       
						                </select>
									</div>

							</div>
						</div> -->
						
							<div class="row no-print text-center" style="margin-top: 40px">
								<div class="col-md-12">
									<button type="submit" class="btn btn-primary" style="margin-right: 50px;  width: 30%;">
										<i class="fa fa-print"></i> Imprimir
									</button>
								</div>
							</div>
						    </form>
						</div>
					</div>
				</div>
			</div>




						<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios - Vendas x Pagamento Cartão(Banco)</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-12">
						<div class="row">
							<form action="<?php echo base_url()?>index.php/relatorios/vendasPagamentoCartao" method="post" target="_blank">
							
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Venda de</label>
									<div class="input-group date">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="date" class="form-control pull-right"  placeholder="dd/mm/aaaa" name="dataInicio" required>
									</div>
								</div>
							</div>
 							<div class="col-md-6">
								<div class="form-group">
									<label>até</label>
									<div class="input-group date">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="date" class="form-control pull-right"  placeholder="dd/mm/aaaa" name="dataFim" required>
									</div>
								</div>
							</div> 
						</div>

					    <div class="row">

							<div class="col-md-12">
								<div class="form-group">
								  <label>Maquina Cartão(banco)</label>
									<select id="nome_credenciadora" class="form-control" data-placeholder="Selecione o vendedor" name="nome_credenciadora" >					                 
					                  <option value=''>TODAS MAQUINAS</option>
					                  <?php foreach ($grupoCartao as $g) { ?>
					                  <option value='<?php echo $g->categoria_cart_nome_credenciadora; ?>'><?php echo $g->categoria_cart_nome_credenciadora; ?> </option>
					                  <?php } ?>                       
					                </select>
								</div>
							</div>
							
						</div>
						
							<div class="row no-print text-center" style="margin-top: 40px">
								<div class="col-md-12">
									<button type="submit" class="btn btn-primary" style="margin-right: 50px;  width: 30%;">
										<i class="fa fa-print"></i> Imprimir
									</button>
								</div>
							</div>
						    </form>
						</div>
					</div>
				</div>
			</div>




		</div>

	</div>
</section>
