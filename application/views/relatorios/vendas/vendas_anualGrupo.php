

<hr style="margin-top:-10px">
<h3 class="text-center" style="margin-top:-10px">Relatório de Venda Mensal Grupo</h3>


<?php $total = 0 ;  $custo = 0; ?>
<table>
	<tr>
		<th style="font-size: 14px;" >Mês</th>
		<th style="font-size: 14px;" >Ano</th>
		<th style="font-size: 14px;" >Qt. Vendas</th>
		<th style="font-size: 14px;" >Custo</th>
		<th style="font-size: 14px;" >Total</th>
		<th style="font-size: 14px;" >Lucro</th>
		<th style="font-size: 14px;" >Percentual</th>
	</tr>

	<?php foreach ($dados as $d) { 
		$custo += $d['custo'];
		$total += $d['total'];
		// $porcentagem = number_format( (($d['total'] - $d['custo']) / $d['total']) * 100,2,",",".");

		// =SOMA(TOTAL/CUSTO*100)-100
		$porcentagem = number_format( (($d['total'] / $d['custo'])*100)-100  ,2,",","."); // Solicitado por Srº Carlos

	 ?>

		<tr class="even">
			<td style="color: blue; font-weight: bold; font-size: 13px;"><?php echo $d['mese']?></td>
			<td style="color: blue; font-weight: bold; font-size: 13px;"><?php echo $d['ano']?></td>
			<td style="color: blue; font-weight: bold; font-size: 13px;"><?php echo $d['qtVendas']?></td>
			<td style="color: red;  font-weight: bold; font-size: 13px;"><?php echo number_format($d['custo'],2,',','.')?></td>
			<td style="color: green; font-weight: bold; font-size: 13px;"><?php echo number_format($d['total'],2,',','.')?></td>
			<td style="color: blue; font-weight: bold; font-size: 13px;"><?php echo number_format($d['total'] - $d['custo'],2,',','.')?></td>
			
			<?php if ($porcentagem > 0) { ?>
			<td style="color: blue;  font-weight: bold; font-size: 13px;"><?php echo $porcentagem ?>%</td>
		    <?php }else{ ?>
		    <td style="color: red;  font-weight: bold; font-size: 13px;"><?php echo $porcentagem ?>%</td>
		    <?php } ?>
		</tr>
	<?php } ?>



</table>
<br class="clear">
<br class="clear">


<table>
	<tr>
		<th style="font-size: 14px;" >Grupo</th>
		<th style="font-size: 14px;" >Qtd Itens</th>		
		<th style="font-size: 14px;" >Custo</th>
		<th style="font-size: 14px;" >Total</th>
		<th style="font-size: 14px;" >Lucro</th>
		<th style="font-size: 14px;" >Percentual</th>
	</tr>



	<?php foreach ($detalhes as $d) { 
		$custo += $d['custo'];
		$total += $d['total'];

		// Solicitado por Srº Carlos
		$lucro = $d['total'] - $d['custo'];
          $porcentagem = ($lucro / $d['total']) * 100;	
	

	 ?>

		<tr class="even">
			<td style="color: blue; font-weight: bold; font-size: 13px;"><?php echo $d['grupo']?></td>
			<td style="color: blue; font-weight: bold; font-size: 13px;" ><?php echo $d['qtd_itens']?>   </td>
			
			<td  style="color: red;  font-weight: bold; font-size: 13px;"><?php echo number_format($d['custo'],2,',','.')?></td>
			<td style="color: blue; font-weight: bold; font-size: 13px;"><?php echo number_format($d['total'],2,',','.')?></td>
			<td style="color: green; font-weight: bold; font-size: 13px;"><?php echo number_format($d['total'] - $d['custo'],2,',','.')?></td>
			
			<?php if ($porcentagem > 0) { ?>
			<td style="color: blue;  font-weight: bold; font-size: 13px;"><?php echo $porcentagem ?>%</td>
		    <?php }else{ ?>
		    <td style="color: red;  font-weight: bold; font-size: 13px;"><?php echo $porcentagem ?>%</td>
		    <?php } ?>
		</tr>
	<?php } ?>





</table>


<!-- <table>
     <tfoot>
          <tr class="even">
            <th colspan="5" style="font-size: 13px; text-align: right;"> <strong>TOTAL GERAL <strong style="color: green">R$ <?php echo number_format(($total - $custo),2,",",".");?></strong></strong></th>
          </tr>

    </tfoot>
</table> -->
