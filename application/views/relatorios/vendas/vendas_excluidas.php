

<hr style="margin-top:-10px">
<h3 class="text-center" style="margin-top:-10px">Relatório de Vendas - Vendas Excluídas  <?php echo $data; ?></h3>




<table class="table">
	<thead>
		<tr>			
			<th class="text-center" style="font-size: 12px;" >Data das Vendas</th>			
			<th class="text-center" style="font-size: 12px;" >Vendedor</th>
			<th class="text-center" style="font-size: 12px;" >N° da Nota</th>
			<th class="text-center" style="font-size: 12px;" >Data da exclusão</th>
			<th class="text-center" style="font-size: 12px;" >Quem excluiu?</th>

		</tr>
	</thead>
	<tbody>
		<?php
			
       
        foreach ($dados as $d){ ?>			
			<tr class="even">				
				<td style="padding: 5px;font-size:10"><?php echo date("d/m/Y", strtotime($d->dataVenda)); ?></td>
				<td style="padding: 5px;font-size:10"><?php echo $d->usuario_nome; ?></td>
				<td style="padding: 5px;font-size:10"><?php echo $d->idVendas; ?></td>
				<td style="padding: 5px;font-size:10"><?php echo date("d/m/Y", strtotime($d->data_atualizacao)); ?></td>
				<td style="padding: 5px;font-size:10"><?php echo $d->usu_nome; ?></td>
			</tr>
		
		<?php } ?>
		
	</tbody>
</table>