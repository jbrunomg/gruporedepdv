

<hr style="margin-top:-10px">
<h3 class="text-center" style="margin-top:-10px">Relatório de Vendas x Fornecedores <?php echo date("d/m/y"); ?></h3>




<table class="table">
	<thead>
		<tr>			
			<th class="text-center" style="font-size: 12px;" >Data das Vendas</th>			
			<!-- <th class="text-center" style="font-size: 12px;" >Vendedor</th> -->
			<th class="text-center" style="font-size: 12px;" >Fornecedor</th>
			<th class="text-center" style="font-size: 12px;" >N° da Nota</th>
			<th class="text-center" style="font-size: 12px;" >Produto / Categoria</th>
			<th class="text-center" style="font-size: 12px;" >IMEI</th>
			<th class="text-center" style="font-size: 12px;" >Custo</th>
			<th class="text-center" style="font-size: 12px;" >Venda</th>
			<th class="text-center" style="font-size: 12px;" >Lucro</th>
			<th class="text-center" style="font-size: 12px;" >Lucro Liquido</th>
		</tr>
	</thead>
	<tbody>
		<?php $total = 0 ;  $custo = 0; $lucro = 0; 	$liquido = 0; 	
       
        foreach ($dados as $d){
          $custo += $d->item_movimentacao_produto_custo;
		$total += $d->preco_venda;
		$lucro += $d->lucro;

		$liquido += $d->preco_venda - $d->item_movimentacao_produto_custo - $d->desconto_cartao;
		?>			
			<tr class="even">				
				<td style="padding: 5px;font-size:10"><?php echo date("d/m/Y", strtotime($d->dataVenda)).'<br>'.$d->usuario_nome; ?></td>				
				<td style="padding: 5px;font-size:10"><?php echo $d->fornecedor_nome; ?></td>
				<td style="padding: 5px;font-size:10"><?php echo $d->idVendas; ?></td>
				<td style="padding: 5px;font-size:10"><?php echo $d->produto_descricao.'<br>'.$d->categoria_prod_descricao; ?></td>
				<td style="padding: 5px;font-size:10"><?php echo $d->imei_valor; ?></td>
				<td style="padding: 5px;font-size:10"><?php echo number_format($d->item_movimentacao_produto_custo,2,',','.'); ?></td>
				<td style="padding: 5px;font-size:10"><?php echo number_format($d->preco_venda,2,',','.'); ?></td>
				<td style="padding: 5px;font-size:10"><?php echo number_format($d->lucro,2,',','.'); ?></td>
				<td style="padding: 5px;font-size:10"><?php echo number_format($d->preco_venda - $d->item_movimentacao_produto_custo - $d->desconto_cartao,2,',','.'); ?></td>

 
			</tr>		
		<?php } ?>
		
	</tbody>
</table>

<br class="clear">
<br class="clear">


<table>
     <tfoot>
          <tr class="even">
            <th style="font-size: 13px; text-align: right;font-weight: bold">
             	<strong>T. CUSTO <strong style="color: red">R$ <?php echo number_format($custo,2,",",".");?></strong></strong> |
             	<strong>T. VENDA <strong style="color: blue">R$ <?php echo number_format($total,2,",",".");?></strong></strong> |
             	<strong>T. GERAL <strong style="color: green">R$ <?php echo number_format($lucro,2,",",".");?></strong></strong> |
             	<strong>T. LIQUIDO <strong style="color: green">R$ <?php echo number_format($liquido,2,",",".");?></strong></strong> 

         	</th>
          </tr>

    </tfoot>
</table>