

<hr style="margin-top:-10px">
<h3 class="text-center" style="margin-top:-10px">Relatório de Vendas/Pagamento - <?php echo date('d-m-Y') ?></h3>



<table>
	<tr>
		<th style="font-size: 12px;" >#</th>		
		<th style="font-size: 12px;" >Valor Bruto</th>
		<th style="font-size: 12px;" >Valor Pago</th>
		<th style="font-size: 12px;" >Desconto</th>
		<th style="font-size: 12px;" >Valor Liquido</th>
		<th style="font-size: 12px;" >Forma de Pagamento</th>
	</tr>

	<?php 
	$total = 0;
	$cartaoDB = 0;
 	$cartaoCD = 0;
	$dinheiro = 0;
	$pix      = 0;
	$outros   = 0;

	$desconto = 0;


	foreach ($dados as $d) {  ?>

		<tr class="even">
			<td><?php echo '#'.$d['idVendas'].' <br> '.$d['vendedor']  ?></td>
			<td><?php echo  number_format($d['vl_bruto'],2,",","."); ?></td>
			<td><?php echo $d['vl_pgto'] ?></td>
			<td><?php echo $d['desc_cartao'] ?></td>			
			<td><?php echo number_format($d['vl_pgto']-$d['desc_cartao'],2,",","."); ?></td>
			<td><?php echo $d['frm_pgto'] ?></td>
		</tr>
    
    <?php
         
        $total += $d['vl_pgto'];

        $desconto += $d['desc_cartao']; 


      
				if (strpos($d['frm_pgto'], 'Cartão de Crédito') !== false) {
				     $cartaoCD += $d['vl_pgto'];
				} 


			  if (strpos($d['frm_pgto'], 'Cartão de Débito') !== false) {
				     $cartaoDB += $d['vl_pgto'];
				} 


				if (strpos($d['frm_pgto'], 'Dinheiro') !== false) {
				     $dinheiro += $d['vl_pgto'];
				} 


				if (strpos($d['frm_pgto'], 'Pagamento Instantâneo (PIX)') !== false) {
				     $pix += $d['vl_pgto'];
				} 


				if (strpos($d['frm_pgto'], 'Crédito Loja') !== false) {
				     $outros += $d['vl_pgto'];
				} 


				if (strpos($d['frm_pgto'], 'Sem pagamento') !== false) {
				     $outros += $d['vl_pgto'];
				} 

				if (strpos($d['frm_pgto'], 'Outros') !== false) {
				     $outros += $d['vl_pgto'];
				} 

				  



         // if ($d['frm_pgto'] == 'Cartão de Crédito') { $cartaoCD += $d['vl_pgto']; }

         // if ($d['frm_pgto'] == 'Cartão de Débito') { $cartaoDB += $d['vl_pgto']; }

         // if ($d['frm_pgto'] != 'Dinheiro' and $d['frm_pgto'] != 'Cartão de Crédito' and $d['frm_pgto'] != 'Cartão de Débito') { $outros += $d['vl_pgto']; }

         // if ($d['frm_pgto'] == 'Dinheiro') { $dinheiro += $d['vl_pgto'];  }
         
         
     } 

     ?>

</table>

<br class="clear">
<br class="clear">

<table>

          <tfoot>
              <tr class="even">
                <th colspan="5" style="font-size: 12px; text-align: right; color: #000"> <strong>CARTÃO DE CREDITO : R$ <?php echo number_format($cartaoCD,2,",","."); ?></strong></th>
              </tr>
              <tr class="even">
                <th colspan="5" style="font-size: 12px; text-align: right; color: #000"> <strong>CARTÃO DE DEBITO : R$ <?php echo number_format($cartaoDB,2,",","."); ?></strong></th>
              </tr>
              <tr class="even">
                <th colspan="5" style="font-size: 12px; text-align: right; color: #000"> <strong>DINHEIRO R$ <?php echo number_format($dinheiro,2,",",".");  ?></strong></th>
              </tr>
              <tr class="even">
                <th colspan="5" style="font-size: 12px; text-align: right; color: #000"> <strong>PIX R$ <?php echo number_format($pix,2,",",".");  ?></strong></th>
              </tr>
              <tr class="even">
                <th colspan="5" style="font-size: 12px; text-align: right; color: #000"> <strong>OUTROS R$ <?php echo  number_format($outros,2,",",".");?></strong></th>
              </tr>

              <tr class="even">
				        <th colspan="5" style="font-size: 12px; text-align: right; color: #000"> <strong>DESCONTO CARTAO R$ - <?php echo  number_format($desconto,2,",",".");?></strong></th>
				      </tr>

              <tr class="even">
                <th colspan="5" style="font-size: 12px; text-align: right; color: #000"> <strong>TOTAL GERAL R$ <?php echo number_format($total  - $desconto,2,",",".");?></strong></th>
              </tr>

        </tfoot>
</table>
