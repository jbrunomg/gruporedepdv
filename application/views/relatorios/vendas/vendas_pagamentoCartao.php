

<hr style="margin-top:-10px">
<h3 class="text-center" style="margin-top:-10px">Relatório Pagamento Cartão(Banco) - <?php echo date('d-m-Y') ?></h3>



<table>
	<tr>			
		<th style="font-size: 12px;" >Banco</th>
		<th style="font-size: 12px;" >Bandeira</th>
		<th style="font-size: 12px;" >Parcela</th>
		<th style="font-size: 12px;" >% Desconto</th>
		<th style="font-size: 12px;" >Valor Bruto</th>
		<th style="font-size: 12px;" >Valor Liquido</th>
	</tr>

	<?php 
	$total    = 0;
	$totalLiq = 0;



	foreach ($dados as $d) { 

	$total += $d->financeiro_valor;
	
	$totalLiq += $d->liq;


 ?>

		<tr class="even">
			<td><?php echo $d->categoria_cart_nome_credenciadora ?></td>
			<td><?php echo $d->categoria_cart_descricao ?></td>

			<td><?php echo $d->financeiro_parcela_cart ?></td>
			<td><?php echo $d->financeiro_percentual_cart ?></td>


			<td><?php echo  number_format($d->financeiro_valor,2,",","."); ?></td>
			<td><?php echo  number_format($d->liq,2,",","."); ?></td>
		
		</tr>
    
    <?php  } ?>

</table>

<br class="clear">
<br class="clear">

<table>

          <tfoot>
              <tr class="even">
                <th colspan="5" style="font-size: 12px; text-align: right; color: #000"> <strong>TOTAL BRUTO : R$ <?php echo number_format($total,2,",","."); ?></strong></th>
              </tr>
              <tr class="even">
                <th colspan="5" style="font-size: 12px; text-align: right; color: #000"> <strong>TOTAL LIQUIDO : R$ <?php echo number_format($totalLiq,2,",","."); ?></strong></th>
              </tr>

    

        </tfoot>
</table>
