

<hr style="margin-top:-10px">
<h3 class="text-center" style="margin-top:-10px">Relatório de Vendas </h3>

<?php $total = 0;   $totalgeralBruto = 0;  $totalgeralLiq = 0; ?>

<table>
  <tr>
    <th style="font-size: 12px;" >Dia</th>
    <th style="font-size: 12px;" >Dinheiro/Pix</th>  
    <th style="font-size: 12px;" >Outros</th>  
    <th style="font-size: 12px;" >Cart. crédito bruto</th>
    <th style="font-size: 12px;" >Cart. crédito líquido</th>
    <th style="font-size: 12px;" >Cart. débito bruto</th>
    <th style="font-size: 12px;" >Cart. débito líquido</th>
    <th style="font-size: 12px;" >Total</th>
  </tr>

<?php  // echo "<pre>"; var_dump($dados);die(); ?>
  
  <?php 
 
  for ($x = 1; $x <= 31; $x++) {
     $status = false;

      foreach ($dados as $vl) {  

        if ($vl['dia'] == $x) { 

          $totalgeralLiq += $vl['dinheiro'] + $vl['outros'] + $vl['cart_cred_liquido'] + $vl['cart_deb_liquido']; 
          $totalgeralBruto += $vl['dinheiro'] + $vl['outros'] + $vl['cart_cred_bruto'] + $vl['cart_debt_bruto']; ?>

        <tr class="even">  
          <th style="font-size: 12px;" ><?php echo $x ?></th>
          <th style="font-size: 12px;" ><?php echo 'R$ '. $vl['dinheiro']  ?></th>
          <th style="font-size: 12px;" ><?php echo 'R$ '. $vl['outros']  ?></th>    
          <th style="font-size: 12px;" ><?php echo 'R$ '. $vl['cart_cred_bruto'] ?></th>
          <th style="font-size: 12px;" ><?php echo 'R$ '. $vl['cart_cred_liquido'] ?></th>
          <th style="font-size: 12px;" ><?php echo 'R$ '. $vl['cart_debt_bruto'] ?></th>
          <th style="font-size: 12px;" ><?php echo 'R$ '. $vl['cart_deb_liquido'] ?></th>
          <th style="font-size: 12px;" ><?php echo 'R$ '. number_format(($vl['dinheiro'] + $vl['outros'] + $vl['cart_cred_liquido'] + $vl['cart_deb_liquido']),2,".","") ?></th>
        </tr>

  <?php  $status = true; break; } elseif ($x <  $vl['dia'] ) { ?>


    <tr class="even">  
      <th style="font-size: 12px;" ><?php echo $x ?></th>
      <th style="font-size: 12px;" ><?php echo '' ?></th>
      <th style="font-size: 12px;" ><?php echo '' ?></th>    
      <th style="font-size: 12px;" ><?php echo '' ?></th>
      <th style="font-size: 12px;" ><?php echo '' ?></th>
      <th style="font-size: 12px;" ><?php echo '' ?></th>
      <th style="font-size: 12px;" ><?php echo '' ?></th>
      <th style="font-size: 12px;" ><?php echo '' ?></th>
    </tr>  

  <?php  $status = true; break; } }
  if (!$status){ ?>

    <tr class="even">  
      <th style="font-size: 12px;" ><?php echo $x ?></th>
      <th style="font-size: 12px;" ><?php echo '' ?></th>   
      <th style="font-size: 12px;" ><?php echo '' ?></th> 
      <th style="font-size: 12px;" ><?php echo '' ?></th>
      <th style="font-size: 12px;" ><?php echo '' ?></th>
      <th style="font-size: 12px;" ><?php echo '' ?></th>
      <th style="font-size: 12px;" ><?php echo '' ?></th>
      <th style="font-size: 12px;" ><?php echo '' ?></th>
    </tr> 

 <?php } } ?>   




</table>

<br class="clear">
<br class="clear">

<table>
     <tfoot>
          <tr class="even">
            <th colspan="5" style="font-size: 12px; text-align: right; color: #000"> <strong>TOTAL GERAL BRUTO R$ <?php echo number_format($totalgeralBruto,2,",",".");?></strong></th>
          </tr>

          <tr class="even">
            <th colspan="5" style="font-size: 12px; text-align: right; color: #000"> <strong>TOTAL GERAL LIQ. R$ <?php echo number_format($totalgeralLiq,2,",",".");?></strong></th>
          </tr>

    </tfoot>
</table>


