

<hr style="margin-top:-10px">
<h3 class="text-center" style="margin-top:-10px">Relatório de Vendas - <?php echo $data?></h3>


<!-- <?php var_dump($dados);  ?> -->
<table>
  </thead>
  	<tr>
      <th style="font-size: 12px;" >&nbsp;&nbsp; Venda &nbsp;</th>
  		<th style="font-size: 12px;" >&nbsp;&nbsp; Cliente</th>
  		<th style="font-size: 12px;" >&nbsp;&nbsp; Valor Total</th>
  		<th style="font-size: 12px;" >&nbsp;&nbsp; Valor Pago</th>
  		<th style="font-size: 12px;" >&nbsp;&nbsp; Vendendor</th>
  		<th style="font-size: 12px;" >&nbsp;&nbsp; Forma de Pagamento</th>
  	</tr>
  <thead>
  <tbody>

	<?php 
	
	foreach ($dados as $d) { ?>

		<tr class="even">
      <td><?php echo '#'.$d['idVendas']?></td>
			<td><?php echo $d['cliente_nome'] ?></td>
			<td><?php echo number_format($d['valorTotal'],2,",",".");?></td>
			<td><?php echo $d['valorRecebido']?></td>
			<td><?php echo $d['usuario_nome']?></td>
			<td><?php echo $d['financeiro_forma_pgto']?></td>
		</tr>    
    <?php } ?>
         


    <?php
      $cartaoDB = 0;
      $cartaoCD = 0;
      $dinheiro = 0;
      $pix      = 0;
      $credLoja = 0;
      $outros   = 0;

      $total    = 0;
      $percCartao = 0;

      foreach ($financeiro as $f) { 
        $total += $f['financeiro_valor'];

        if ($f['financeiro_forma_pgto'] == 'Dinheiro') { $dinheiro += $f['financeiro_valor'];  }
        if ($f['financeiro_forma_pgto'] == 'Cartão de Débito') { $cartaoDB += $f['financeiro_valor']; }

        if ($f['financeiro_forma_pgto'] == 'Cartão de Crédito') {
          $cartaoCD   += $f['financeiro_valor'];
          // $percCartao += $f['financeiro_percentual_cart'];

          $percCartao += $f['financeiro_valor'] * ($f['financeiro_percentual_cart']/100);
       
        }

        if ($f['financeiro_forma_pgto'] == 'Pagamento Instantâneo (PIX)') {
           $pix += $f['financeiro_valor'];
        }
 
        if ($f['financeiro_forma_pgto'] == 'Crédito Loja') {
           $credLoja += $f['financeiro_valor'];
        }

        if ($f['financeiro_forma_pgto'] == 'Sem pagamento') {
           $outros += $f['financeiro_valor'];
        }

        if ($f['financeiro_forma_pgto'] == 'Outros') {
           $outros += $f['financeiro_valor'];
        }

      

      } ?>  

    </tbody> 
    <tfoot>
      <tr class="even">
        <th colspan="6" style="font-size: 12px; text-align: right; color: #000"> <strong>CARTÃO CREDITO : R$ <?php echo number_format($cartaoCD,2,",","."); ?></strong></th>
      </tr>
      <tr class="even">
        <th colspan="6" style="font-size: 12px; text-align: right; color: #000"> <strong>CARTÃO DEBITO : R$ <?php echo number_format($cartaoDB,2,",","."); ?></strong></th>
      </tr>
      <tr class="even">
        <th colspan="6" style="font-size: 12px; text-align: right; color: #000"> <strong>DINHEIRO R$ <?php echo number_format($dinheiro,2,",",".");  ?></strong></th>
      </tr>
      <tr class="even">
        <th colspan="6" style="font-size: 12px; text-align: right; color: #000"> <strong>PIX R$ <?php echo number_format($pix,2,",",".");  ?></strong></th>
      </tr>
      <tr class="even">
        <th colspan="6" style="font-size: 12px; text-align: right; color: #000"> <strong>CREDITO LOJA R$ <?php echo number_format($credLoja,2,",",".");  ?></strong></th>
      </tr>
      <tr class="even">
        <th colspan="6" style="font-size: 12px; text-align: right; color: #000"> <strong>OUTROS R$ <?php echo  number_format($outros,2,",",".");?></strong></th>
      </tr>

      <tr class="even">
        <th colspan="6" style="font-size: 12px; text-align: right; color: #000"> <strong>DESCONTO CARTAO R$ - <?php echo  number_format( $percCartao,2,",",".");?></strong></th>
      </tr>

      <tr class="even">
        <th colspan="6" style="font-size: 12px; text-align: right; color: #000"> <strong>TOTAL GERAL R$ <?php echo number_format(($total - $percCartao),2,",",".");?></strong></th>
      </tr>
    </tfoot>

</table>
