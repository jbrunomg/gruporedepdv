

<hr style="margin-top:-10px">
<h3 class="text-center" style="margin-top:-10px">Relatório de Vendas - <?php echo $data?></h3>


<!--  <?php echo "<pre>"; var_dump($dados);  ?>  -->
<table>
	<tr>
		<th style="font-size: 12px;" >Cliente</th>
		<th style="font-size: 12px;" >Valor Total</th>
		<th style="font-size: 12px;" >Valor Pago</th>
		<th style="font-size: 12px;" >Vendendor</th>
		<th style="font-size: 12px;" >Forma de Pagamento</th>
	</tr>

	<?php 

	foreach ($dados as $d) { ?>

		<tr class="even">
			<td><?php echo '#'.$d['idVendas'].' - '.$d['cliente_nome']  ?></td>
			<td><?php echo  number_format($d['valorTotal'],2,",","."); ?></td>
			<td><?php echo $d['valorRecebido']?></td>
			<td><?php echo $d['usuario_nome']?></td>
			<td><?php echo $d['financeiro_forma_pgto']?></td>
		</tr>
	   <?php if (count($d[0]) > 0) { ?>
		<tr class="black">
			<td class="black" style="font-size: 10px";>Produto</td>
			<td class="black" style="font-size: 10px";>Quanditade</td>
			<td class="black" style="font-size: 10px";>Preço Unitário</td>
			<td class="black" style="font-size: 10px";>Sub-Total</td>
			<td class="black" style="font-size: 10px";>Filial</td>
		</tr>
	   <?php } ?>
       <?php foreach ($d[0] as $dItens) { ?>
		<tr class="black">
			<td class="black"><?php echo $dItens['produto_descricao'] ?></td>
			<td class="black"><?php echo $dItens['quantidade'] ?></td>
			<td class="black"><?php echo $dItens['valor_unitario'] ?></td>
			<td class="black"><?php echo $dItens['subTotal'] ?></td>		
			<td class="black"><?php echo $dItens['filial'] ?></td>
		</tr>
		<?php }  ?>
		<tr><td colspan="5" height="20" style="color: white;"></td></tr>
    
    <?php } ?>

    <?php

      $cartaoDB = 0;
      $cartaoCD = 0;
      $dinheiro = 0;
      $pix      = 0;
      $credLoja = 0;
      $outros   = 0;

      $total    = 0;
      $percCartao = 0;

      
      foreach ($financeiro as $f) { 
        $total += $f['financeiro_valor'];

        if ($f['financeiro_forma_pgto'] == 'Dinheiro') { $dinheiro += $f['financeiro_valor'];  }
        if ($f['financeiro_forma_pgto'] == 'Cartão de Débito') { $cartaoDB += $f['financeiro_valor']; }

        if ($f['financeiro_forma_pgto'] == 'Cartão de Crédito') {
          $cartaoCD   += $f['financeiro_valor'];
          $percCartao += $f['financeiro_percentual_cart'];
        }


     		if ($f['financeiro_forma_pgto'] == 'Pagamento Instantâneo (PIX)') {
           $pix += $f['financeiro_valor'];
        }

        if ($f['financeiro_forma_pgto'] == 'Crédito Loja') {
           $credLoja += $f['financeiro_valor'];
        }

        if ($f['financeiro_forma_pgto'] == 'Sem pagamento') {
           $outros += $f['financeiro_valor'];
        }

        if ($f['financeiro_forma_pgto'] == 'Outros') {
           $outros += $f['financeiro_valor'];
        }


      } ?> 

</table>

<br class="clear">
<br class="clear">

<table>

          <tfoot>
              <tr class="even">
                <th colspan="5" style="font-size: 12px; text-align: right; color: #000"> <strong>CARTÃO DE CREDITO : R$ <?php echo number_format($cartaoCD,2,",","."); ?></strong></th>
              </tr>
              <tr class="even">
                <th colspan="5" style="font-size: 12px; text-align: right; color: #000"> <strong>CARTÃO DE DEBITO : R$ <?php echo number_format($cartaoDB,2,",","."); ?></strong></th>
              </tr>
              <tr class="even">
                <th colspan="5" style="font-size: 12px; text-align: right; color: #000"> <strong>DINHEIRO R$ <?php echo number_format($dinheiro,2,",",".");  ?></strong></th>
              </tr>
              <tr class="even">
                <th colspan="5" style="font-size: 12px; text-align: right; color: #000"> <strong>PIX R$ <?php echo number_format($pix,2,",",".");  ?></strong></th>
              </tr>
              <tr class="even">
                <th colspan="5" style="font-size: 12px; text-align: right; color: #000"> <strong>CREDITO LOJA R$ <?php echo number_format($credLoja,2,",",".");  ?></strong></th>
              </tr>
              <tr class="even">
                <th colspan="5" style="font-size: 12px; text-align: right; color: #000"> <strong>OUTROS R$ <?php echo  number_format($outros,2,",",".");?></strong></th>
              </tr>

              <tr class="even">
				        <th colspan="5" style="font-size: 12px; text-align: right; color: #000"> <strong>DESCONTO CARTAO R$ - <?php echo  number_format($cartaoCD * ($percCartao/100),2,",",".");?></strong></th>
				      </tr>

              <tr class="even">
                <th colspan="5" style="font-size: 12px; text-align: right; color: #000"> <strong>TOTAL GERAL R$ <?php echo number_format($total  - ($cartaoCD * ($percCartao/100)),2,",",".");?></strong></th>
              </tr>

        </tfoot>
</table>
