<hr style="margin-top:-10px">
<h3 class="text-center" style="margin-top:-10px">Relatório Balanço Produto - Nº <?php echo $numBalanco ?></h3>
<h4 class="text-center" style="margin-top:-10px"><?php echo (empty($imeisProdutosCompativeis[0]->produto_descricao )) ? '' : $imeisProdutosCompativeis[0]->produto_descricao; ?></h4>

<hr style="margin-top:0px">
<h3 class="text-center" style="margin-top:-10px">Todos Imeis</h3>

<table class="table">
	<thead>
		<tr>
			<th class="text-center" style="font-size: 12px; width: 50%;">Imeis Enviados</th>
			<th class="text-center" style="font-size: 12px; width: 50%;">Imeis Cadastrados</th>
		</tr>
	</thead>
	<tbody>
		<?php
			$maximo = count($imeisProdutos) > count($imeisEnviados) ? count($imeisProdutos) : count($imeisEnviados);
			for ($i = 0; $i < $maximo; $i++) {
		?>
			<tr class="even">
				<td><?= isset($imeisEnviados[$i]->itens_de_balanco_imei) ? $imeisEnviados[$i]->itens_de_balanco_imei : ''; ?></td>
				<td><?= isset($imeisProdutos[$i]->itens_de_balanco_imei) ? $imeisProdutos[$i]->itens_de_balanco_imei : ''; ?> | 
					<?php echo (empty($imeisProdutosCompativeis[0]->produto_descricao )) ? '' : $imeisProdutosCompativeis[0]->produto_descricao; ?></td>
			</tr>
		<?php } ?>
	</tbody>
</table>

<!-- <hr style="margin-top:-10px">
<h3 class="text-center" style="margin-top:-10px">Imeis Compatíveis</h3>

<table class="table">
	<thead>
		<tr>
			<th class="text-center" style="font-size: 12px; width: 50%;">Imeis Enviados</th>
			<th class="text-center" style="font-size: 12px; width: 50%;">Imeis Cadastrados</th>
		</tr>
	</thead>
	<tbody>
		<?php
			$maximo = count($imeisProdutosCompativeis) > count($imeisEnviadosCompativeis) ? count($imeisProdutosCompativeis) : count($imeisEnviadosCompativeis);
			for ($i = 0; $i < $maximo; $i++) {
		?>
			<tr class="even">
				<td><?= isset($imeisEnviadosCompativeis[$i]->itens_de_balanco_imei) ? $imeisEnviadosCompativeis[$i]->itens_de_balanco_imei : ''; ?></td>
				<td><?= isset($imeisProdutosCompativeis[$i]->itens_de_balanco_imei) ? $imeisProdutosCompativeis[$i]->itens_de_balanco_imei : ''; ?> | <?= isset($imeisProdutosCompativeis[$i]->produto_descricao) ? $imeisProdutosCompativeis[$i]->produto_descricao : ''; ?></td>
			</tr>
		<?php } ?>
	</tbody>
</table> -->

<hr style="margin-top:-10px">
<h3 class="text-center" style="margin-top:-10px">Imeis Incompatíveis</h3>

<table class="table">
	<thead>
		<tr>
			<th class="text-center" style="font-size: 12px; width: 50%;">Imeis Enviados</th>
			<th class="text-center" style="font-size: 12px; width: 50%;">Imeis Cadastrados</th>
		</tr>
	</thead>
	<tbody>
		<?php
			$maximo = count($imeisProdutosIncompativeis) > count($imeisEnviadosIncompativeis) ? count($imeisProdutosIncompativeis) : count($imeisEnviadosIncompativeis);
			for ($i = 0; $i < $maximo; $i++) {
		?>
			<tr class="even">
				<td><?= isset($imeisEnviadosIncompativeis[$i]->itens_de_balanco_imei) ? $imeisEnviadosIncompativeis[$i]->itens_de_balanco_imei : ''; ?></td>
				<td><?= isset($imeisProdutosIncompativeis[$i]->itens_de_balanco_imei) ? $imeisProdutosIncompativeis[$i]->itens_de_balanco_imei : ''; ?> | <?= isset($imeisProdutosCompativeis[$i]->produto_descricao) ? $imeisProdutosCompativeis[$i]->produto_descricao : ''; ?></td>
			</tr>
		<?php } ?>
	</tbody>
</table> 

<hr style="margin-top:-10px">
<h3 class="text-center" style="margin-top:-10px">Relatório Geral</h3>

<ul>
	<li>
		Imeis bipado: <strong><?= count($imeisEnviados); ?></strong>
	</li>
	<li>
		Imeis estoque: <strong><?= count($imeisProdutos); ?></strong>
	</li>
<!-- 	<li>
		Imeis compativeis: <strong><?= count($imeisProdutosCompativeis) + count($imeisEnviadosCompativeis); ?></strong>
	</li> -->
	<li>
		Imeis incompativeis: <strong><?= count($imeisProdutosIncompativeis) + count($imeisEnviadosIncompativeis); ?></strong>
	</li>
</ul>

