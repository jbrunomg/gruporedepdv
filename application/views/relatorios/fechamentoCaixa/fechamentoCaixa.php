
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios Customizáveis</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-12">
						<div class="row">
                            <form id="formRelatorio" action="<?php echo base_url()?>index.php/relatorios/fechamentoCaixa" method="post" target="formTarget">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Venda de</label>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" id="datepickerVendaIncio" placeholder="dd/mm/aaaa" name="dataInicio" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row no-print text-center" style="margin-top: 40px">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary" style="margin-right: 50px;  width: 30%;">
                                            <i class="fa fa-print"></i> Imprimir
                                        </button>
                                    </div>
                                </div>
                            </form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<script>
    document.getElementById('formRelatorio').addEventListener('submit', function(event) {
        event.preventDefault(); // Impede o envio padrão do formulário
        var form = event.target;

        // Abre a nova janela com as especificações de tamanho
        var myWindow = window.open('', 'formTarget', 'toolbar=no,location=no,directories=no,status=no,menubar=yes,scrollbars=yes,resizable=yes,width=350,height=600');

        // Envia o formulário para a nova janela
        form.submit();
    });

    // Inicializa o datepicker
    $( function() {
        $( "#datepickerVendaIncio" ).datepicker({ dateFormat: 'dd/mm/yy' });
    });
</script>