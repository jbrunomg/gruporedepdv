

<hr style="margin-top:-10px">
<!-- <h3 class="text-center" style="margin-top:-10px">Relatório de Clientes</h3> -->
<table>
	<thead>
		<tr>			
			<th style="font-size: 12px;" >Cliente</th>			
			<th style="font-size: 12px;" >Telefone</th>
			<th style="font-size: 12px;" >Cidade - Estado</th>
			<th style="font-size: 12px;" >Cpf/Cnpj</th>
			<th style="font-size: 12px;" >Tipo</th>
		</tr>
	</thead>
	<tbody>
		<?php $cont = 0; ?>
		<?php foreach ($dados as $valor){ ?>
			<?php $cont++; $tipo = ($valor->cliente_tipo == 1)? 'Física':'Jurídica'; ?>			
			<tr class="even">				
				<td><?php echo $valor->cliente_nome; ?></td>
				<td><?php echo $valor->cliente_telefone; ?></td>
				<td><?php echo $valor->cliente_cidade."/".$valor->cliente_estado; ?></td>
				<td><?php echo $valor->cliente_cpf_cnpj; ?></td>
				<td><?php echo $tipo; ?></td>
			</tr>
		
		<?php } ?> 
	</tbody>
</table>