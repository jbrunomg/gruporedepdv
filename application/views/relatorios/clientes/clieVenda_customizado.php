

<hr style="margin-top:-10px">
<h3 class="text-center" style="margin-top:-10px">Relatório Clientes x Vendas - <?php echo $titulo; ?></h3>


<!--  <?php echo "<pre>"; var_dump($dados);  ?>  -->

  <table class="table table-bordered">
    <!-- <table class="table-responsive-md table-striped">       -->
      <thead> 
      <!--   <tr>
          <th></th>                       
        </tr>   -->
      </thead>                                   
      <?php

        $totalGeral = 0;
        $totalGeralAberto = 0;
        $totalGeralPago   = 0;
        $TotalPrecoCusto  = 0;

        for ($i=0; $i < count($clieVendas) ; $i++){

          $dataVenda = date('d/m/Y', strtotime($clieVendas[$i]['dataVenda']));
          if ($clieVendas[$i]['faturado'] == '1') {
            $totalGeralPago += $clieVendas[$i]['valorTotal'];
           }else{
            $totalGeralAberto += $clieVendas[$i]['valorTotal']; 
           } 
          
          $totalGeral += $clieVendas[$i]['valorTotal'];
        ?>                         
      <tbody>
             <tr>
              <td  colspan="3"><strong></strong></td>                       
            </tr>  
            <tr>
              <td  colspan="3"><strong>DADOS DO CLIENTE:</strong></td>                       
            </tr>                                                       
            <tr>
              <td><strong>NOME:</strong><?php echo mb_strtoupper($clieVendas[$i]['cliente_nome'])  ?></td>
              <td><strong>VENDA:</strong><?php echo ' '.$clieVendas[$i]['idVendas']   ?></td>
              <td><strong>DATA:</strong> <?php echo ' '.$dataVenda   ?></td>                    
            </tr> 
            <tr>
              <td><strong>ENDEREÇO:</strong><?php echo $clieVendas[$i]['cliente_endereco'].' '.$clieVendas[$i]['cliente_numero'].' '.$clieVendas[$i]['cliente_bairro'] ?></td>
              <!-- <td colspan="2"><strong>CONTATO:</strong><?php echo $clieVendas[$i]['cliente_celular'].''.$clieVendas[$i]['cliente_telefone'] ?></td>  -->
              <td><strong>CONTATO:</strong><?php echo $clieVendas[$i]['cliente_celular'].''.$clieVendas[$i]['cliente_telefone'] ?></td> 
              <td><strong>VALOR:</strong> <?php echo ' R$ '.$clieVendas[$i]['valorTotal']   ?></td>                    
            </tr>

        <?php
          if ($tipoRelatorio == "1") { ?>                        
          <tr class="table-primary">
              <td><STRONG>DESCRICAO</STRONG></td>
              <td><strong>QTD</strong></td> 
              <td><strong>SUB TOTAL</strong></td>                   
          </tr>
          
        <?php
        
        for ($j=0; $j < count($itensVendas) ; $j++){
          if ($clieVendas[$i]['idVendas'] == $itensVendas[$j]['idVendas']) {
            if ($itensVendas[$j]['quantidade'] > 0) {
              $TotalPrecoCusto += $itensVendas[$j]['prod_preco_custo']; 
            }
           
        ?>
          <tr>
            <td><?php echo mb_strtoupper($itensVendas[$j]['produto_descricao'].' '.$itensVendas[$j]['imei_valor'])  ?></td>
            <td><?php echo $itensVendas[$j]['quantidade']   ?></td>
            <td>R$ <?php echo number_format($itensVendas[$j]['subTotal'],2,',','.');    ?></td>                    
          </tr>
        <?php  } } ?>
          <tr>
            <td></td>
            <td><strong>TOTAL</strong></td>
            <td><strong>R$ </strong><?php echo number_format($clieVendas[$i]['valorTotal'],2,',','.');  ?></td>                                                
          </tr>
        <?php  }  ?>
          <tr>
            <td style="background-color: #DCDCDC;" colspan="3"></td>
          </tr>

                                                     
      </tbody>
        <?php } ?>                      
    </table>


    <table>
     <tfoot>
         

          <tr class="even">
          <?php if ($tipoRelatorio == "1") { ?>  
        <!--     <th colspan="5" style="font-size: 13px; text-align: right;"> <strong>TOTAL CUSTO <strong style="color: red">R$ <?php echo number_format($TotalPrecoCusto,2,",",".");?></strong></strong></th> -->

            <th colspan="5" style="font-size: 13px; text-align: right;"></th>
          <?php } ?>  
          

            <th colspan="5" style="font-size: 13px; text-align: right;"> <strong>TOTAL ABERTO <strong style="color: green">R$ <?php echo number_format($totalGeralAberto,2,",",".");?></strong></strong></th>
            <th colspan="5" style="font-size: 13px; text-align: right;"> <strong>TOTAL PAGO <strong style="color: blue">R$ <?php echo number_format($totalGeralPago,2,",",".");?></strong></strong></th>
            <th colspan="5" style="font-size: 13px; text-align: right;"> <strong>TOTAL GERAL <strong style="color: black">R$ <?php echo number_format($totalGeral,2,",",".");?></strong></strong></th>
          </tr>

    </tfoot>
</table>