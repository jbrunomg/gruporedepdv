

<hr style="margin-top:-10px">
<h3 class="text-center" style="margin-top:-10px"><?php echo $titulo; ?></h3>

<!-- <?php echo "<pre>"; var_dump($dados);  ?>  -->
  <table class="table table-bordered">
      <thead> 
      </thead>                                                     
      <tbody>
            <tr>
              <td  colspan="3"><strong></strong></td>                       
            </tr>  
            <tr>
              <td style="text-align: center; font-size: 12px;" colspan="5"><strong><?php echo mb_strtoupper($dados[0]->cliente)  ?></strong></td>                       
            </tr>                                                       
            <tr>
              <td style="text-align: center; font-size: 12px;"><strong>ANO</strong></td>
              <td style="text-align: center; font-size: 12px;"><strong>MÊS</strong></td>
              <td style="text-align: center; font-size: 12px;"><strong>ABERTO</strong></td>  
              <td style="text-align: center; font-size: 12px;"><strong>COMPROU</strong></td>  
              <td style="text-align: center; font-size: 12px;"><strong>PAGO</strong></td>                    
            </tr> 

            <?php 
             $geralAberto = 0;
             $geralCompra = 0;
             $geralPago = 0;
            
             for ($i=0; $i < count($dados) ; $i++){ 

               $geralAberto += $dados[$i]->totalAberto; 
               $geralCompra += $dados[$i]->totalCompra; 
               $geralPago += $dados[$i]->total; 
              
              ?>  
                  <tr>
                    <td style="text-align: center; font-size: 12px;"><strong><?php echo mb_strtoupper($dados[$i]->ano)  ?></td>
                    <td style="text-align: center; font-size: 12px;"><strong><?php echo mb_strtoupper($dados[$i]->mes)  ?></td>
                    <td style="text-align: center; font-size: 12px;"><strong><?php echo number_format($dados[$i]->totalAberto,2,',','.');  ?></td>  
                    <td style="text-align: center; font-size: 12px;"><strong><?php echo number_format($dados[$i]->totalCompra,2,',','.');  ?></td>  
                    <td style="text-align: center; font-size: 12px;"><strong><?php echo number_format($dados[$i]->total,2,',','.');  ?></td>                    
                  </tr> 
                  <tr>
                    <td style="background-color: #DCDCDC;" colspan="5"></td>
                  </tr>
            <?php } ?>  
            <tr>
              <td style="text-align: center; font-size: 12px;" colspan="2"><strong>TOTAL GERAL</td>
              <td style="text-align: center; font-size: 12px;"><strong><?php echo number_format($geralAberto,2,',','.');  ?></td>  
              <td style="text-align: center; font-size: 12px;"><strong><?php echo number_format($geralCompra,2,',','.');  ?></td>  
              <td style="text-align: center; font-size: 12px;"><strong><?php echo number_format($geralPago,2,',','.');  ?></td>                    
            </tr> 
            <tr>
              <td style="background-color: #DCDCDC;" colspan="5"></td>
            </tr>
      </tbody>                    
  </table>
