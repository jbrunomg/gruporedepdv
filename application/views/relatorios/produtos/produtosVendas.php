

<hr style="margin-top:-10px">
<h3 class="text-center" style="margin-top:-10px">Relatório de Produtos - Produto/Venda  <?php echo $data; ?></h3>




<table class="table">
	<thead>
		<tr>			
			<th class="text-center" style="font-size: 12px;" >Data das Vendas</th>			
			<th class="text-center" style="font-size: 12px;" >Quantidade</th>
			<th class="text-center" style="font-size: 12px;" >Produto</th>
		</tr>
	</thead>
	<tbody>
		<?php
			
       
        foreach ($dados as $d){ ?>			
			<tr class="even">				
				<td style="padding: 5px;font-size:10"><?php echo date("d/m/Y", strtotime($d->dataVenda)); ?></td>
				<td style="padding: 5px;font-size:10"><?php echo $d->quantidade; ?></td>
				<td style="padding: 5px;font-size:10"><?php echo $d->produto_descricao; ?></td>
			</tr>
		
		<?php } ?>
		
	</tbody>
</table>