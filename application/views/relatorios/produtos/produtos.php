
<section class="content">
	<div class="row">
		<div class="col-md-3">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios Rápidos</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					
					<a href="<?php echo base_url() ?>relatorios/produtos/todos" class="small-box-footer" target="_blank">
					<div class="small-box bg-green">						
						<div class="inner">
							<h3><?php echo $totalProduto; ?></h3>

							<p>Todos os produtos</p>
						</div>
						<div class="icon">
							<i class="fa fa-dropbox"></i>
						</div>						
					</div>
					</a>

					<a href="<?php echo base_url() ?>relatorios/produtos/minimo" class="small-box-footer" target="_blank">
					<div class="small-box bg-aqua">						
						<div class="inner">
							<h3><?php echo $totalEstoqueMinimo; ?></h3>

							<p>Com estoque minimo</p>
						</div>
						<div class="icon">
							<i class="fa fa-dropbox"></i>
						</div>						
					</div>
					</a>

					<a href="<?php echo base_url() ?>relatorios/produtos/valorestoque" class="small-box-footer" target="_blank">
					<div class="small-box bg-blue">
						
						<div class="inner">
							<h3><?php echo '$ '.number_format($totalValorEstoque[0]->totalValor,2,',','.'); ?></h3>

							<p>Valor em estoque</p>
						</div>
						<div class="icon">
							<i class="fa fa-dropbox"></i>
						</div>
						
					</div>
					</a>


					<a href="<?php echo base_url() ?>relatorios/produtos/valorestoquegrupo" class="small-box-footer" target="_blank">
					<div class="small-box bg-yellow">
						
						<div class="inner">
							<h3><?php echo '$ '.number_format($totalValorEstoque[0]->totalValor,2,',','.'); ?></h3>

							<p>Valor em estoque grupo</p>
						</div>
						<div class="icon">
							<i class="fa fa-dropbox"></i>
						</div>
						
					</div>
					</a>

				</div>
			</div>
		</div>

		<div class="col-md-9">


			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios Customizáveis - Produtos/Estoque</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-12">
						<div class="row">
							<form action="<?php echo base_url()?>index.php/relatorios/produtosCustom" method="post" target="_blank">
							
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label>Grupo</label>		
										<select id="grupo_prod_pedido" class="form-control" data-placeholder="Selecione o grupo de produtos" name="produto_categoria_id"  required="required">
						                  <option value="">Selecione</option>
						                  <option value="todos">TODOS</option>
						                  <?php foreach ($grupo as $g) { ?>
						                  <option value='<?php echo $g->categoria_prod_id; ?>'><?php echo $g->categoria_prod_descricao; ?> </option>
						                  <?php } ?>                       
						                </select>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Produto</label>		
					                      <select class="select2" style="width: 100%;" name="produto_desc" id="produto_desc" >
					                        <option value="">Selecionar um produto</option>                       
					                      </select>  
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Ordenar</label>		
				                      	<select class="form-control" name="tipo_ordenacao">
											<option value="0">Nome Produto</option>								
											<option value="1">Cód Produto</option>
											<option value="2">Estoque A-Z</option>
											<option value="3">Estoque Z-A</option>
										</select>  
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label>Preço</label><br>		
						                  <label>
						                     <div class="iradio_flat-green" style="position: relative;"><input type="radio" name="precoProduto" value="0" class="flat-red" checked="" ></ins></div> NÂO
						                  </label>
						                  						                  <label>
						                     <div class="iradio_flat-green" style="position: relative;"><input type="radio" name="precoProduto" value="1" class="flat-red" ></ins></div> SIM
						                  </label>
									</div>
								</div>

								<div class="col-md-4">
									<div class="form-group">
										<label>Exibir estoque zerado</label><br>		
						                  <label>
						                     <div class="iradio_flat-green" style="position: relative;"><input type="radio" name="estoqueProduto" value="0" class="flat-red" checked="" ></ins></div> NÂO
						                  </label>
						                  						                  <label>
						                     <div class="iradio_flat-green" style="position: relative;"><input type="radio" name="estoqueProduto" value="1" class="flat-red" ></ins></div> SIM
						                  </label>
									</div>
								</div>

							
																	
								<div class="col-md-4">
									<div class="form-group">
										<label>Exibir IMEI</label><br>		
						                  <label>
						                     <div class="iradio_flat-green" style="position: relative;"><input type="radio" name="exibirImei" value="0" class="flat-red" checked="" ></ins></div> NÂO
						                  </label>
						                  						                  <label>
						                     <div class="iradio_flat-green" style="position: relative;"><input type="radio" name="exibirImei" value="1" class="flat-red" ></ins></div> SIM
						                  </label>
									</div>
								</div>
								

							</div>

						
							<div class="row no-print text-center" style="margin-top: 40px">
								<div class="col-md-12">
									<button type="submit" class="btn btn-primary" style="margin-right: 50px;  width: 30%;">
										<i class="fa fa-print"></i> Imprimir
									</button>
								</div>
							</div>
						    </form>
						</div>
					</div>
				</div>
			</div>

			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios Customizáveis - Etiquetas</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-12">
						<div class="row">
							<form action="<?php echo base_url()?>index.php/relatorios/produtosEtiquetaCustom" method="post" target="_blank">
							
							<div class="col-md-4">
								<div class="form-group">
									<label>Tipo</label>
									<select class="form-control" name="tipo_etiqueta">
										<option value="">Selecione</option>									
										<option value="01">Cód Barra</option>
										<option value="02">Qr-Code</option>
										<option value="03">Só Nome</option>
									</select>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Grupo</label>		
									<select id="produto_categoria_id" class="form-control" data-placeholder="Selecione o grupo de produtos" name="produto_categoria_id" >
					                  <option value="">Selecione</option>
					                  <?php foreach ($grupo as $g) { ?>
					                  <option value='<?php echo $g->categoria_prod_id; ?>'><?php echo $g->categoria_prod_descricao; ?> </option>
					                  <?php } ?>                       
					                </select>
								</div>
							</div>
						
							<div class="col-md-4">
								<label>&nbsp;</label>
								<button type="submit" class="btn btn-primary btn-block">
									<i class="fa fa-print"></i> Imprimir
								</button>
							</div>
						    </form>
						</div>
					</div>
				</div>
			</div>

			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios Customizáveis - Estorno / Produto Avaria / Devolução / Troca IMEI</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-12">
						<div class="row">
							<form action="<?php echo base_url()?>index.php/relatorios/produtosAvaria" method="post" target="_blank">
							
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Venda de</label>
										<div class="input-group date">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" class="form-control pull-right" id="datepickerVendaIncio" placeholder="dd/mm/aaaa" name="dataInicio" required="required">
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>até</label>
										<div class="input-group date">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" class="form-control pull-right" id="datepickerVendaFim" placeholder="dd/mm/aaaa" name="dataFim" required="required">
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Motivo</label>
										<select class="form-control" name="motivo">												
					                        <option value='1'>Avaria do Produto</option>
					                        <option value='2'>Reembolso do Produto</option>
					                        <option value='3'>Avaria com Reembolso</option>
					                        <option value='4'>Troca de IMEI</option>
											<option value='5'>Troca de IMEI Gerente</option>
											<option value="">Todos</option>										
										</select>
									</div>
								</div>
							</div>
						
							<div class="row no-print text-center" style="margin-top: 40px">
								<div class="col-md-12">
									<button type="submit" class="btn btn-primary" style="margin-right: 50px;  width: 30%;">
										<i class="fa fa-print"></i> Imprimir
									</button>
								</div>
							</div>
						    </form>
						</div>
					</div>
				</div>
			</div>


			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios Customizáveis - Mais vendido</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-12">
						<div class="row">
							<form action="<?php echo base_url()?>index.php/relatorios/produtosVendidosCustom" method="post" target="_blank">
							
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Venda de</label>
										<div class="input-group date">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" class="form-control pull-right" id="datepickerprodutosVendidosIncio" placeholder="dd/mm/aaaa" name="dataInicio" required="required">
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>até</label>
										<div class="input-group date">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" class="form-control pull-right" id="datepickerprodutosVendidosFim" placeholder="dd/mm/aaaa" name="dataFim" required="required">
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label>Mais Vendido</label>
										<select class="form-control" name="limite">												
											<option value="10">Top 10</option>									
											<option value="20">Top 20</option>
											<option value="30">Top 30</option>	
											<option value="">Todos</option>										
										</select>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Grupo</label>		
										<select id="produto_categoria_id" class="form-control" data-placeholder="Selecione o grupo de produtos" name="produto_categoria_id" >
						                  <option value="">Selecione</option>
						                  <?php foreach ($grupo as $g) { ?>
						                  <option value='<?php echo $g->categoria_prod_id; ?>'><?php echo $g->categoria_prod_descricao; ?> </option>
						                  <?php } ?>                       
						                </select>
									</div>
								</div>

								<div class="col-md-4">
									<div class="form-group">
										<label>Unificar</label>
										<select class="form-control" name="unificar">
											<option value="0">Nome Produto</option>										
											<option value="1">Cód Produto (agrupado)</option>
										
										</select>  
									</div>
								</div>
							</div>


						
							<div class="row no-print text-center" style="margin-top: 40px">
								<div class="col-md-12">
									<button type="submit" class="btn btn-primary" style="margin-right: 50px;  width: 30%;">
										<i class="fa fa-print"></i> Imprimir
									</button>
								</div>
							</div>
						    </form>
						</div>
					</div>
				</div>
			</div>



			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios Customizáveis - Parado No Estoque (+20 DIAS)</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-12">
						<div class="row">
							<form action="<?php echo base_url()?>index.php/relatorios/produtosParadoCustom" method="post" target="_blank">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Mais Parado</label>
										<select class="form-control" name="limite">
											<option value="10">Top 10</option>									
											<option value="20">Top 20</option>
											<option value="30">Top 30</option>
											<option value="1">Todos</option>											
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Grupo</label>		
										<select id="produto_categoria_id" class="form-control" data-placeholder="Selecione o grupo de produtos" name="produto_categoria_id" >
						                  <option value="">Todas</option>
						                  <?php foreach ($grupo as $g) { ?>
						                  <option value='<?php echo $g->categoria_prod_id; ?>'><?php echo $g->categoria_prod_descricao; ?> </option>
						                  <?php } ?>                       
						                </select>
									</div>
								</div>
							</div>


						
							<div class="row no-print text-center" style="margin-top: 40px">
								<div class="col-md-12">
									<button type="submit" class="btn btn-primary" style="margin-right: 50px;  width: 30%;">
										<i class="fa fa-print"></i> Imprimir
									</button>
								</div>
							</div>
						    </form>
						</div>
					</div>
				</div>
			</div>



			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios Customizáveis - Produto/Venda</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-12">
						<div class="row">
							<form action="<?php echo base_url()?>index.php/relatorios/produtosVendasCustom" method="post" target="_blank">
							
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Venda de</label>
										<div class="input-group date">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" class="form-control pull-right" id="datepickerprodutosVendasIncio" placeholder="dd/mm/aaaa" name="dataInicio" required="required">
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>até</label>
										<div class="input-group date">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" class="form-control pull-right" id="datepickerprodutosVendasFim" placeholder="dd/mm/aaaa" name="dataFim" required="required">
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Grupo</label>		
										<select id="grupo_prod_venda" class="form-control" data-placeholder="Selecione o grupo de produtos" name="produto_categoria_id"  required="required">
						                  <option value="">Selecione</option>
						                  <?php foreach ($grupo as $g) { ?>
						                  <option value='<?php echo $g->categoria_prod_id; ?>'><?php echo $g->categoria_prod_descricao; ?> </option>
						                  <?php } ?>                       
						                </select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Produto</label>		
					                      <select class="select2" style="width: 100%;" name="produto_desc" id="rel_produto_venda_desc" required="required">
					                        <option value="">Selecionar um produto</option>                       
					                      </select>  
									</div>
								</div>
							</div>


						
							<div class="row no-print text-center" style="margin-top: 40px">
								<div class="col-md-12">
									<button type="submit" class="btn btn-primary" style="margin-right: 50px;  width: 30%;">
										<i class="fa fa-print"></i> Imprimir
									</button>
								</div>
							</div>
						    </form>
						</div>
					</div>
				</div>
			</div>

			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios Customizáveis - Catalogo Produtos</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-12">
						<div class="row">
							<form action="<?php echo base_url()?>index.php/relatorios/produtosCatalogoCustom" method="post" target="_blank">
							
							<div class="row">
								<div class="col-md-3">
									<div class="form-group">
										<label>Grupo</label>		
										<select id="grupo_prod_produto" class="form-control" data-placeholder="Selecione o grupo de produtos" name="produto_categoria_id"  required="required">
						                  <option value="">Selecione</option>
						                  <?php foreach ($grupo as $g) { ?>
						                  <option value='<?php echo $g->categoria_prod_id; ?>'><?php echo $g->categoria_prod_descricao; ?> </option>
						                  <?php } ?>                       
						                </select>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label>Produto</label>		
					                      <select class="select2" style="width: 100%;" name="produto_desc" id="rel_produto_desc" >
					                        <option value="">Selecionar um produto</option>                       
					                      </select>  
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label>Com Estoque</label>
										<select class="form-control" name="estoque">
											<option value="">Ambos</option>
											<option value="N">Não</option>									
											<option value="S">Sim</option>																						
										</select>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label>Ordenar</label>
										<select class="form-control" name="tipo_ordenacao">
											<option value="0">Nome Produto</option>								
											<option value="1">Cód Produto</option>
											<option value="2">Cód Produto (agrupado)</option>
											<option value="3">Estoque A-Z</option>
											<option value="4">Estoque Z-A</option>
										</select>  
									</div>
								</div>
							</div>

							<div class="col-xs-12" style="margin-left: 0"> 

								<div class="col-xs-2" style="margin-left: 0">
								  <label for="pc">Preço Custo?</label><br>
								  <input  id="pc" type="checkbox" name="pc" value="1" /> 
								</div>
								<div class="col-xs-2" style="margin-left: 0">
								  <label for="pa">Preço Atacado?</label><br>
								  <input  id="pa" type="checkbox" name="pa" value="1" /> 
								</div>
								<div class="col-xs-2" style="margin-left: 0">
								  <label for="pv">Preço Varejo?</label><br>
								  <input  id="pv" type="checkbox" name="pv" value="1" /> 
								</div>
								<div class="col-xs-3" style="margin-left: 0">
								  <label for="pr">Preço Revenda?</label><br>
								  <input  id="pr" type="checkbox" name="pr" value="1" /> 
								</div>							
								<div class="col-xs-2" style="margin-left: 0">
								  <label for="qtd">Quantidade?</label><br>
								  <input  id="qtd" type="checkbox" name="qtd" value="1" /> 
								</div>
						
							</div>

						
							<div class="row no-print text-center" style="margin-top: 40px">
								<div class="col-md-12">
									<button type="submit" class="btn btn-primary" style="margin-right: 50px;  width: 30%;">
										<i class="fa fa-print"></i> Imprimir
									</button>
								</div>
							</div>
						    </form>
						</div>
					</div>
				</div>
			</div>


			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios Customizáveis - Produto/Venda Menor que o PMV</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-12">
						<div class="row">
							<form action="<?php echo base_url()?>index.php/relatorios/produtosVendasPMVCustom" method="post" target="_blank">
							
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Venda de</label>
										<div class="input-group date">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" class="form-control pull-right" id="datepickerprodutosPMVIncio" placeholder="dd/mm/aaaa" name="dataInicio" required="required">
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>até</label>
										<div class="input-group date">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" class="form-control pull-right" id="datepickerprodutosPMVFim" placeholder="dd/mm/aaaa" name="dataFim" required="required">
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Grupo</label>		
										<select id="grupo_prod_venda" class="form-control" data-placeholder="Selecione o grupo de produtos" name="produto_categoria_id"  required="required">
						                  <option value="">Selecione</option>
						                  <?php foreach ($grupo as $g) { ?>
						                  <option value='<?php echo $g->categoria_prod_id; ?>'><?php echo $g->categoria_prod_descricao; ?> </option>
						                  <?php } ?>                       
						                </select>
									</div>
								</div>
					<!-- 			<div class="col-md-6">
									<div class="form-group">
										<label>Produto</label>		
					                      <select class="select2" style="width: 100%;" name="produto_desc" id="rel_produto_venda_desc" required="required">
					                        <option value="">Selecionar um produto</option>                       
					                      </select>  
									</div>
								</div> -->
							</div>


						
							<div class="row no-print text-center" style="margin-top: 40px">
								<div class="col-md-12">
									<button type="submit" class="btn btn-primary" style="margin-right: 50px;  width: 30%;">
										<i class="fa fa-print"></i> Imprimir
									</button>
								</div>
							</div>
						    </form>
						</div>
					</div>
				</div>
			</div>



		</div>

	</div>
</section>

