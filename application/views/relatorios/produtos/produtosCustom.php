

<hr style="margin-top:-10px">
<h3 class="text-center" style="margin-top:-10px">Relatório de Produtos </h3>




<table class="table">
	<thead>
		<tr>			
			<th class="text-center" style="font-size: 12px;" >Categoria</th>			
			<th class="text-center" style="font-size: 12px;" >Produto</th>
			 <?php if ($imei[0] == '1') { ?>
			    <th class="text-center" style="font-size: 12px;" >IMEI</th>
		    <?php } ?>
			<th class="text-center" style="font-size: 12px;" >Estoque</th>
		    <?php if ($preco[0] == '1') { ?>
			    <th class="text-center" style="font-size: 12px;" >Preço Custo</th>
				<th class="text-center" style="font-size: 12px;" >Preço Venda</th>
		    <?php } ?>
		</tr>
	</thead>
	<tbody>
		<?php
			$qtd = 0;
			$totalcusto = 0;
			$totalVendido = 0;

			$totalGeralcusto = 0;
			$totalGeralVendido = 0;
        

        foreach ($dados as $d){

        $totalGeralcusto   = $totalGeralcusto   + ($d->produto_preco_custo * $d->produto_estoque );
        $totalGeralVendido = $totalGeralVendido + ($d->produto_preco_venda * $d->produto_estoque );

         ?>	


			<tr class="even">				
				<td style="padding: 5px;font-size:10"><?php echo $d->categoria_prod_descricao  ?></td>
				<td style="padding: 5px;font-size:10"><?php echo $d->produto_descricao; ?></td>
				
				<?php if ($imei[0] == '1') { ?>
				<td style="padding: 5px;font-size:10"><?php echo $d->imei_valor; ?></td>

				<?php if (($d->imei_valor) and ($d->vendas_id)) { ?>
					<td style="padding: 5px;font-size:10"><?php $qtd = $qtd + '0'; echo '0'; ?></td>
				<?php } else { ?>
					<td style="padding: 5px;font-size:10"><?php $qtd = $qtd + '1'; echo '1'; ?></td>
							
				<?php } } else { ?>
				
				<td style="padding: 5px;font-size:10"><?php $qtd = $qtd + $d->produto_estoque; echo $d->produto_estoque; ?></td>

				<?php } ?>

                <?php if ($preco[0] == '1') { ?>
					<td style="padding: 5px;font-size:10;color: red; font-weight: bold;"><?php $totalcusto = $totalcusto + $d->produto_preco_custo; echo $d->produto_preco_custo; ?></td>
					<td style="padding: 5px;font-size:10;color: blue; font-weight: bold;"><?php $totalVendido = $totalVendido + $d->produto_preco_venda; echo $d->produto_preco_venda; ?></td>
				<?php } ?>
			</tr>
		
		<?php } ?>

 			<tr>          
                <th style="padding: 5px;font-size:10">ITENS:<?php echo ' '.count($dados); ?></th>
                <th style="padding: 5px;font-size:10"></th>                             
                <th style="padding: 5px;font-size:10">VOLUME:<?php echo ' '.$qtd; ?></th>
                <?php if ($preco[0] == '1') { ?>
	                <th style="padding: 5px;font-size:10;color: red; font-weight: bold; ">TOTAL : R$ <?php echo ' '.number_format($totalcusto,2,',','.');?></th> 
	                <th style="padding: 5px;font-size:10; color: blue; font-weight: bold;">TOTAL : R$ <?php echo ' '.number_format($totalVendido,2,',','.');?></th>
	            <?php } ?>
            </tr>
            <tr>          
                <th style="padding: 5px;font-size:10"></th>
                <th style="padding: 5px;font-size:10"></th>               
                <?php if ($preco[0] == '1') { ?>
                	<th style="padding: 5px;font-size:10">VOLUME + TOTAL:</th>
	                <th style="padding: 5px;font-size:10"> R$ <?php echo ' '.number_format($totalGeralcusto,2,',','.');?></th> 
	                <th style="padding: 5px;font-size:10"> R$ <?php echo ' '.number_format($totalGeralVendido,2,',','.');?></th>
	            <?php } else { ?>
	            	<th style="padding: 5px;font-size:10">VOLUME + TOTAL:</th>
	            <?php } ?>	
            </tr>  
		 
	</tbody>
</table>