

<hr style="margin-top:-10px">
<h3 class="text-center" style="margin-top:-10px">Relatório de Produtos - Catalogo </h3>




<table class="table">
	<thead>
		<tr>			
			<th class="text-center" style="font-size: 12px;" >Categoria</th>			
			<th class="text-center" style="font-size: 12px;" >Produto</th>
			<?php if ($qtd[0] == '1') { ?>
			<th class="text-center" style="font-size: 12px;" >Quantidade</th>
			<?php } ?>
			<?php if ($pc[0] == '1') { ?>
			<th class="text-center" style="font-size: 12px;" >Preço Custo</th>
			<?php } ?>
		    <?php if ($pa[0] == '1') { ?>
			<th class="text-center" style="font-size: 12px;" >Preço Atacado</th>
			<?php } ?>
		    <?php if ($pr[0] == '1') { ?>
			<th class="text-center" style="font-size: 12px;" >Preço Revenda</th>
			<?php } ?>

            <?php if ($pv[0] == '1') { ?>
			<th class="text-center" style="font-size: 12px;" >Preço Venda</th>
			<?php } ?>
					   
		</tr>
	</thead>
	<tbody>
		<?php
			//$qtd = 0;
			$totalcusto = 0;
			$totalVendido = 0;
        

        foreach ($dados as $d){ ?>			
			<tr class="even">				
				<td style="padding: 5px;font-size:10"><?php echo $d->categoria_prod_descricao  ?></td>
				<td style="padding: 5px;font-size:10"><?php echo $d->produto_descricao; ?></td>

				<?php if ($qtd[0] == '1') { ?>
				<td style="padding: 5px;font-size:10"><?php echo $d->produto_estoque ?></td>
				<?php } ?>

                <?php if ($pc[0] == '1') { ?>
					<td style="padding: 5px;font-size:10;color: red; font-weight: bold;"><?php echo $d->produto_preco_custo; ?></td>
				<?php } ?>
				<?php if ($pa[0] == '1') { ?>
					<td style="padding: 5px;font-size:10; color: blue; font-weight: bold;"><?php  echo $d->produto_preco_minimo_venda; ?></td>
				<?php } ?>
				<?php if ($pr[0] == '1') { ?>
					<td style="padding: 5px;font-size:10; color: blue; font-weight: bold;"><?php  echo $d->produto_preco_venda; ?></td>
				<?php } ?>
			    <?php if ($pv[0] == '1') { ?>
					<td style="padding: 5px;font-size:10; color: blue; font-weight: bold;"><?php echo $d->produto_preco_cart_debito; ?></td>
				<?php } ?>

			</tr>
		
		<?php } ?>

 			<tr>          
                <th style="padding: 5px;font-size:10">ITENS:<?php echo ' '.count($dados); ?></th>
                         
            </tr> 
		 
	</tbody>
</table>