

<hr style="margin-top:-10px">
<h3 class="text-center" style="margin-top:-10px">Relatório de Produtos c/ Avarias <?php echo $titulo; ?></h3>


	<table class="table">
	<thead>
		<tr>
		    <th class="text-center" style="font-size: 12px;" >Data</th>	
		    <th class="text-center" style="font-size: 12px;" >N° Venda/ Dias</th>				
			<th class="text-center" style="font-size: 12px;" >Produto</th>	
			<?php if ($dados[0]->produto_avaria_motivo == '4' OR $dados[0]->produto_avaria_motivo == '5') { ?>
			<th class="text-center" style="font-size: 12px;" >IMEI De</th>
			<th class="text-center" style="font-size: 12px;" >IMEI Para</th>				
			<?php  } ?>		
			<th class="text-center" style="font-size: 12px;" >Quantidade</th>
			<th class="text-center" style="font-size: 12px;" >Custo</th>
			<th class="text-center" style="font-size: 12px;" >Venda</th>
			<th class="text-center" style="font-size: 12px;" >Motivo</th>
			<th class="text-center" style="font-size: 12px;" >Fornecedor</th>
			<th class="text-center" style="font-size: 12px;" >Vendendor</th>			
		</tr>
	</thead>
	<tbody>
		<?php
		$totalcusto = 0;
		$totalVendido = 0;
		$qtd = 0;

		 foreach ($dados as $valor){ 
			$qtd = $qtd + $valor->produto_avarias_quantidade;
			$totalcusto = $totalcusto + $valor->produto_avaria_preco_custo;
			$totalVendido = $totalVendido + $valor->produto_avaria_preco_vendido;

			if ($valor->produto_avaria_motivo == '1') {
				$motivo = 'Avaria do Produto';
			}elseif ($valor->produto_avaria_motivo == '2') {
				$motivo = 'Reembolso do Produto';
			}elseif ($valor->produto_avaria_motivo == '3') {
				$motivo = 'Avaria com Reembolso';
			}elseif ($valor->produto_avaria_motivo == '4') {
				$motivo = 'Troca de IMEI';	
			}elseif ($valor->produto_avaria_motivo == '5') {
				$motivo = 'Troca de IMEI Gerente';	
			}else{
				$motivo = 'Sem Motivo';
			}
		?>	
			<tr class="even">
			    <td style="padding: 5px;font-size:10"><?php echo $valor->data; ?></td>
		    	<td style="padding: 5px;font-size:10"><?php echo $valor->venda_id.' > '. $valor->dia  ?></td>				
				<td style="padding: 5px;font-size:10"><?php echo $valor->produto_descricao; ?></td>
				<?php if ($dados[0]->produto_avaria_motivo == '4' OR $dados[0]->produto_avaria_motivo == '5') { ?>
				<td style="padding: 5px;font-size:10"><?php echo $valor->produto_avaria_antigo_imei; ?></td>
				<td style="padding: 5px;font-size:10"><?php echo $valor->produto_avaria_novo_imei; ?></td>
				<?php  } ?>		
				<td style="padding: 5px;font-size:10"><?php echo $valor->produto_avarias_quantidade; ?></td>
				<td style="padding: 5px;font-size:10; color: red; font-weight: bold;"><?php echo number_format($valor->produto_avaria_preco_custo,2,',','.');  ?></td>
				<td style="padding: 5px;font-size:10; color: blue; font-weight: bold;"><?php echo number_format($valor->produto_avaria_preco_vendido,2,',','.');  ?></td>
				<td style="padding: 5px;font-size:10"><?php echo $motivo; ?></td>
				<td style="padding: 5px;font-size:10"><?php echo $valor->fornecedor_nome; ?></td>
				<td style="padding: 5px;font-size:10"><?php echo $valor->usuario_nome; ?></td>			
			</tr>
		
		<?php } ?>
		    <tr>           
                <th>ITENS:<?php echo ' '.count($dados); ?></th>
                <th></th>        
                <th></th>                      
                <th>VOLUME:<?php echo ' '.$qtd; ?></th>
                <th style="color: red; font-weight: bold;">TOTAL : R$ <?php echo ' '.number_format($totalcusto,2,".","");?></th> 
                <th style="color: blue; font-weight: bold;">TOTAL : R$ <?php echo ' '.number_format($totalVendido,2,".","");?></th>
                <th></th>        
                <th></th> 
            </tr> 
	</tbody>
</table>
