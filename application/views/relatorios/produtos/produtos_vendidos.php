

<hr style="margin-top:-10px">
<h3 class="text-center" style="margin-top:-10px">Relatório de Produtos - Mais Vendidos</h3>




<table class="table">
	<thead>
		<tr>			
			<th style="font-size: 12px;" >Nome</th>			
			<th style="font-size: 12px;" >ESTOQUE</th>
			<th style="font-size: 12px;" >QTD</th>
            <th style="font-size: 12px;" >CUSTO MEDIO</th>
            <th style="font-size: 12px;" >VENDA MEDIA</th>
            <th style="font-size: 12px;" >LUCRO</th>
		</tr>
	</thead>
	<tbody>
		<?php

		$totalEsto   = 0;
		$totalProd   = 0;
	    $totalCompra = 0;
        $totalVenda  = 0;
        $totalLucro  = 0;			
       
        foreach ($dados as $valor){ 

        	$totalEsto   += $valor->produto_estoque;
        	$totalProd   += $valor->qtd;
        	$totalCompra += $valor->custo_medio;
            $totalVenda  += $valor->venda_media;
           
			$lucro =  (($valor->venda_media - $valor->custo_medio) * $valor->qtd) ;

			$totalLucro  += $lucro;           
     
        ?>			
			<tr class="even">				
				<td style="padding: 5px;font-size:10"><?php echo $valor->produto_descricao; ?></td>
				<td style="padding: 5px;font-size:10"><?php echo $valor->produto_estoque; ?></td>
				<td style="padding: 5px;font-size:10"><?php echo $valor->qtd; ?></td>
				<td style="padding: 5px;font-size:10; color: red; font-weight: bold;"><?php echo $valor->custo_medio; ?></td>
				<td style="padding: 5px;font-size:10; color: blue; font-weight: bold;"><?php echo $valor->venda_media; ?></td>
				<td style="padding: 5px;font-size:10; color: green; font-weight: bold;"><?php echo number_format($lucro , 2, '.', ','); ?></td>	
			</tr>
		
		<?php } ?>

			<tr>
			   <td colspan="1"></td>
			    <td colspan="1" style="text-align: center; color: red; font-weight: bold;font-size:13"><b><?php echo "Total Est: </br> ".$totalEsto; ?></b></td>	
			   <td colspan="1" style="text-align: center; color: red; font-weight: bold;font-size:13"><b><?php echo "Total Qtd: </br> ".$totalProd; ?></b></td>	
               <td colspan="1" style="text-align: center; color: red; font-weight: bold;font-size:13"><b><?php echo "Total Custo: </br> ".number_format($totalCompra, 2, ',', '.'); ?></b></td>
               <td colspan="1" style="text-align: center; color: blue; font-weight: bold;font-size:13"><b><?php echo "Total Venda: </br> ".number_format($totalVenda, 2, ',', '.'); ?></b></td>
               <td colspan="1" style="text-align: center; color: green; font-weight: bold;font-size:13"><b><?php echo "Total Lucro: </br> ".number_format($totalLucro, 2, ',', '.'); ?></b></td>              
            </tr> 
		
	</tbody>
</table>
