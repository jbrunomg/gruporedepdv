

<hr style="margin-top:-10px">
<h3 class="text-center" style="margin-top:-10px">Relatório de Produtos</h3>



<?php if (empty($dados[0]->lucro)) { ?>
	<table class="table">
	<thead>
		<tr>			
			<th style="font-size: 12px;" >Nome</th>			
			<th style="font-size: 12px;" >UN</th>
			<th style="font-size: 12px;" >Preço Custo</th>
            <th style="font-size: 12px;" >Preço Venda</th>
            <th style="font-size: 12px;" >Estoque</th>
		</tr>
	</thead>
	<tbody>
		<?php
			$totalProdutos = 0;
	        $totalEstoque = 0;   
       
        foreach ($dados as $valor){ 
			$totalProdutos += $valor->produto_preco_venda;
            $totalEstoque += $valor->produto_estoque;
     
        ?>			
			<tr class="even">				
				<td style="padding: 5px;font-size:10"><?php echo $valor->produto_descricao; ?></td>
				<td style="padding: 5px;font-size:10"><?php echo $valor->produto_unidade; ?></td>
				<td style="padding: 5px;font-size:10"><?php echo number_format($valor->produto_preco_custo, 2, '.', ','); ?></td>
				<td style="padding: 5px;font-size:10"><?php echo number_format($valor->produto_preco_venda, 2, '.', ','); ?></td>
				<td style="padding: 5px;font-size:10"><?php echo number_format($valor->produto_estoque, 2, '.', ','); ?></td>	
			</tr>
		
		<?php } ?>
		    <tr>
               <td colspan="2" style="text-align: right; color: white;"><b><?php echo "Valor Total: ".number_format($totalProdutos, 2, '.', ','); ?></b></td>
               <td colspan="2" style="text-align: right; color: white;"><b><?php echo "Quantidade Total: ".$totalEstoque ; ?></b></td>
            </tr> 
	</tbody>
</table>

	
<?php } else { ?>
<!-- RELATORIO PRODUTOS x LUCRO  -->
	<table class="table">
	<thead>
		<tr>			
			<th style="font-size: 12px;" >Nome</th>			
			<!-- <th style="font-size: 12px;" >UN</th> -->
			<th style="font-size: 12px;" >Estoque</th>
			<th style="font-size: 12px;" >Preço Custo</th>
            <th style="font-size: 12px;" >Preço Venda</th>           
            <th style="font-size: 12px;" >Lucro</th>

		</tr>
	</thead>
	<tbody>
		<?php
	        $totalCompra = 0;
	        $totalVenda  = 0;
	        $totalLucro  = 0;
        
        foreach ($dados as $valor){ 		

            $totalCompra += $valor->produto_preco_custo;
            $totalVenda  += $valor->produto_preco_venda;
            $totalLucro  += $valor->lucro; 
        ?>			
			<tr class="even">				
				<td style="padding: 5px;font-size:10"><?php echo $valor->produto_descricao; ?></td>				 
				<td style="padding: 5px;font-size:10"><?php echo $valor->produto_estoque; ?></td>
				<!-- <td style="padding: 5px;font-size:10"><?php echo $valor->produto_unidade; ?></td> -->
				<td style="padding: 5px;font-size:10;color: red; font-weight: bold;"><?php echo number_format($valor->produto_preco_custo, 2, ',', '.'); ?></td>
				<td style="padding: 5px;font-size:10;color: blue; font-weight: bold;"><?php echo number_format($valor->produto_preco_venda, 2, ',', '.'); ?></td>
				<td style="padding: 5px;font-size:10;color: green; font-weight: bold;"><?php echo number_format($valor->lucro, 2, ',', '.'); ?></td>		
			</tr>
		
		<?php } ?>
 		    <tr>
               <td colspan="2" style="text-align: center; color: red; font-weight: bold;"><b><?php echo "Total Custo: ".number_format($totalCompra, 2, ',', '.'); ?></b></td>
               <td colspan="2" style="text-align: center; color: blue; font-weight: bold;"><b><?php echo "Total Venda: ".number_format($totalVenda, 2, ',', '.'); ?></b></td>
               <td colspan="1" style="text-align: center; color: green; font-weight: bold;"><b><?php echo "Total Lucro: ".number_format($totalLucro, 2, ',', '.'); ?></b></td>              
            </tr> 
	</tbody>
</table>


<?php } ?>