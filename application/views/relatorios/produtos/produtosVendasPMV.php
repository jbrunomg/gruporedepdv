

<hr style="margin-top:-10px">
<h3 class="text-center" style="margin-top:-10px">Relatório de Produto/Venda menor que PMV :</h3>
<h3 class="text-center" style="margin-top:-10px"><?php echo $data; ?></h3>




<table class="table">
	<thead>
		<tr>			
			<th class="text-center" style="font-size: 12px;" >Data das Vendas</th>			
			<th class="text-center" style="font-size: 12px;" >Venda</th>
			<th class="text-center" style="font-size: 12px;" >Vendedor</th>
			<th class="text-center" style="font-size: 12px;" >Categoria</th>
			<th class="text-center" style="font-size: 12px;" >Produto</th>
			<th class="text-center" style="font-size: 12px;" >PMV</th>
			<th class="text-center" style="font-size: 12px;" >Preço Vendido</th>
		</tr>
	</thead>
	<tbody>
		<?php
			
       
        foreach ($dados as $d){ ?>			
			<tr class="even">				
				<td style="padding: 5px;font-size:10"><?php echo date("d/m/Y", strtotime($d->dataVenda)); ?></td>
				<td style="padding: 5px;font-size:10"><?php echo $d->idVendas; ?></td>
				<td style="padding: 5px;font-size:10"><?php echo $d->usuario_nome; ?></td>
				<td style="padding: 5px;font-size:10"><?php echo $d->categoria_prod_descricao; ?></td>
				<td style="padding: 5px;font-size:10"><?php echo $d->produto_descricao; ?></td>
				<td style="padding: 5px;font-size:10"><?php echo $d->prod_preco_minimo_venda; ?></td>
				<td style="padding: 5px;font-size:10"><?php echo number_format($d->preco_vendido, 2, '.', ',');  ?></td>


			</tr>
		
		<?php } ?>
		
	</tbody>
</table>