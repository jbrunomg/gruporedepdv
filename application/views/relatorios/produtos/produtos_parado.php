

<hr style="margin-top:-10px">
<h3 class="text-center" style="margin-top:-10px">Relatório de Produtos - Parado No Estoque (+20 DIAS)</h3>




<table class="table">
	<thead>
		<tr>			
			<th style="font-size: 12px;" >NOME</th>			
			<th style="font-size: 12px;" >ESTOQUE</th>
			<th style="font-size: 12px;" >P. CUSTO</th>
			<th style="font-size: 12px;" >P M VENDA</th>
			<th style="font-size: 12px;" >ULTIMA VENDA</th>
		</tr>
	</thead>
	<tbody>
		<?php
			
       
        foreach ($dados as $valor){ ?>			
			<tr class="even">				
				<td style="padding: 5px;font-size:10"><?php echo $valor->produto_descricao; ?></td>
				<td style="padding: 5px;font-size:10"><?php echo $valor->produto_estoque; ?></td>
				<td style="padding: 5px;font-size:10"><?php echo $valor->produto_preco_custo; ?></td>
				<td style="padding: 5px;font-size:10"><?php echo $valor->produto_preco_minimo_venda; ?></td>
				<td style="padding: 5px;font-size:10"><?php echo date("d/m/Y", strtotime($valor->produto_data_ultima_venda)); ?></td>
			</tr>
		
		<?php } ?>
		
	</tbody>
</table>
