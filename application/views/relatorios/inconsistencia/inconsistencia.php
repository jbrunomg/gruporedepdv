
<section class="content">
	<div class="row">
		<div class="col-md-3">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios Rápidos</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					
					<div class="small-box bg-green">
						<a href="<?php echo base_url() ?>relatorios/inconsistencia/sim" class="small-box-footer" target="_blank">
						<div class="inner">							
							<h3><?php echo $totalVendaSemFinanceiro[0]->total; ?></h3>

							<p>Vendas sem relacionamento <br> com financeiro</p>
						</div>
						<div class="icon">
							<i class="fa fa-wechat"></i>
						</div>
						</a>
					</div>

					<div class="small-box bg-red">
						<a href="<?php echo base_url() ?>relatorios/inconsistencia/nao" class="small-box-footer" target="_blank">
						<div class="inner">
							<h3><?php echo $totalVendaSemFinanceiro[0]->total; ?></h3>

							<p>Vendas sem relacionamento <br> com financeiro</p>
						</div>
						<div class="icon">
							<i class="fa fa-wechat"></i>
						</div>
						</a>
					</div>

				</div>
			</div>
		</div>
		<div class="col-md-9">

			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios Customizáveis</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-12">
						<div class="row">
							<form action="<?php echo base_url()?>index.php/relatorios/inconsistenciaCustom" method="post" target="_blank">
								
								<div class="row">
									<div class="col-md-6">
										<label>Data de</label>
										<div class="form-group">
											<div class="input-group date">
												<div class="input-group-addon">
													<i class="fa fa-calendar"></i>
												</div>
												<input type="text" class="form-control pull-right" id="datepickerCadastroIncio" placeholder="dd/mm/aaaa" name="dataInicio">
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<label>até</label>
										<div class="form-group">
											<div class="input-group date">
												<div class="input-group-addon">
													<i class="fa fa-calendar"></i>
												</div>
												<input type="text" class="form-control pull-right" id="datepickerCadastroFim" placeholder="dd/mm/aaaa" name="dataFim">
											</div>
										</div>
									</div>
								</div>
									
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label>Vendedor</label>
											   <input type="text" class="form-control" name="usuario_nome" id="usuario_nome" value="<?php set_value('usuario_nome'); ?>" placeholder="Vendedor">
											   <input type="hidden" id="usuario_id" name="usuario_id" value="<?php echo set_value('usuario_id'); ?>">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>Cliente</label>
											 <input type="text" class="form-control" name="cliente_nome" id="cliente_nomeR" value="<?php echo set_value('cliente_nome'); ?>" placeholder="Cliente">
		                                     <input type="hidden" id="cliente_id" name="cliente_id" value="<?php echo set_value('cliente_id'); ?>">
										</div>
									</div>
								</div>

								<div class="row no-print text-center" style="margin-top: 40px">
									<div class="col-md-12">
										<button type="reset" class="btn btn-default">
											<i class="fa fa-eraser"></i> Limpar
										</button>
										<button type="submit" class="btn btn-primary" style="margin-right: 5px;">
											<i class="fa fa-print"></i> Imprimir
										</button>
									</div>
								</div>

							<!-- 	<div class="col-md-4">
									<label>&nbsp;</label>
									<button type="button" class="btn btn-primary btn-block">
										<i class="fa fa-print"></i> Imprimir
									</button>
								</div> -->

						    </form>
						</div>
					</div>
				</div>
			</div>


			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios Produto Sem IMEI</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-12">
						<div class="row">
							<form action="<?php echo base_url()?>index.php/relatorios/inconsistenciaProdImei" method="post" target="_blank">
								
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label>Grupo</label>		
											<select id="grupo_prod_pedido" class="form-control" data-placeholder="Selecione o grupo de produtos" name="produto_categoria_id"  required="required">
							                  <option value="">Selecione</option>
							                  <option value="todos">TODOS</option>
							                  <?php foreach ($grupo as $g) { ?>
							                  <option value='<?php echo $g->categoria_prod_id; ?>'><?php echo $g->categoria_prod_descricao; ?> </option>
							                  <?php } ?>                       
							                </select>
										</div>
									</div>
								</div>									
		
								<div class="row no-print text-center" style="margin-top: 40px">
									<div class="col-md-12">
										<button type="reset" class="btn btn-default">
											<i class="fa fa-eraser"></i> Limpar
										</button>
										<button type="submit" class="btn btn-primary" style="margin-right: 5px;">
											<i class="fa fa-print"></i> Imprimir
										</button>
									</div>
								</div>				

						    </form>
						</div>
					</div>
				</div>
			</div>


			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios ENTRADA | VENDA | ESTOQUE | IMEI</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-12">
						<div class="row">
							<form action="<?php echo base_url()?>index.php/relatorios/inconsistenciaEVEI" method="post" target="_blank">
								
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label>Grupo</label>		
											<select id="grupo_prod_pedido" class="form-control" data-placeholder="Selecione o grupo de produtos" name="produto_categoria_id"  required="required">
							                  <option value="">Selecione</option>
							                  <option value="todos">TODOS</option>
							                  <?php foreach ($grupo as $g) { ?>
							                  <option value='<?php echo $g->categoria_prod_id; ?>'><?php echo $g->categoria_prod_descricao; ?> </option>
							                  <?php } ?>                       
							                </select>
										</div>
									</div>
								</div>									
		
								<div class="row no-print text-center" style="margin-top: 40px">
									<div class="col-md-12">
										<button type="reset" class="btn btn-default">
											<i class="fa fa-eraser"></i> Limpar
										</button>
										<button type="submit" class="btn btn-primary" style="margin-right: 5px;">
											<i class="fa fa-print"></i> Imprimir
										</button>
									</div>
								</div>				

						    </form>
						</div>
					</div>
				</div>
			</div>



		</div>

	</div>
</section>

