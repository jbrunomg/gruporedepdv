<hr style="margin-top:-10px">
<h3 class="text-center" style="margin-top:-10px">Relatório Inconsistência - Produto x IMEI  </h3>

<table>
	<thead>
		<tr>			
			<th style="font-size: 12px;" >Produto</th>		
			<th style="font-size: 12px;" >Estoque</th>		
			<th style="font-size: 12px;" >Estoque IMEI</th>					
		</tr>
	</thead>
	<tbody>
		
		<?php foreach ($dados as $valor){ 
			if ($valor['produto_estoque']  <> $valor['estoque_imei'] ) { ?>

			<tr class="even">	
				<td><?php echo $valor['produto_descricao'] ?></td>	
				<td><?php echo $valor['produto_estoque'] ?></td>
				<td><?php echo $valor['estoque_imei'] ?></td>						
			</tr>		
			
		<?php	} ?>	
		
		<?php } ?> 
	</tbody>
</table>