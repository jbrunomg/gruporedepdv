<hr style="margin-top:-10px">
<h3 class="text-center" style="margin-top:-10px">Relatório Inconsistência - Venda x Financeiro  <?php echo date("Y", strtotime($dados[0]->dataVenda)); ?></h3>

<table>
	<thead>
		<tr>			
			<th style="font-size: 12px;" >Nº Vendas</th>		
			<th style="font-size: 12px;" >Data</th>		
			<th style="font-size: 12px;" >Usuário</th>
			<th style="font-size: 12px;" >Cliente</th>	
			<th style="font-size: 12px;" >Valor</th>			
		</tr>
	</thead>
	<tbody>
		
		<?php foreach ($dados as $valor){ ?>
			

			<tr class="even">	
				<td><?php echo $valor->idVendas; ?></td>	
				<td><?php echo date("d/m/Y", strtotime($valor->dataVenda)); ?></td>		
				<td><?php echo $valor->usuario_nome; ?></td>
				<td><?php echo $valor->cliente_nome; ?></td>
				<td><?php echo number_format($valor->valorTotal,2,',','.'); ?></td>				
			</tr>
		
		<?php } ?> 
	</tbody>
</table>