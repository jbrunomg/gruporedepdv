<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <div class="col-md-2">
      
      </div>          
    </div>
    <div class="box-body">
      <div class="container-fluid">
        <table id="example" class="table" width="100%">
          <thead>
            <tr>                                            
              <th>Vendedor</th>
              <th>Cliente</th>
              <th>N° da Nota</th>
              <th>Data</th>
              <th>Total</th>
              <th>Ações</th>
            </tr>
          </thead>        
          <tbody>
           <?php foreach ($dados as $dado){ 
              $dado->idVendas; 
            ?>
              
              <tr>
                   <td><?php echo $dado->usuario_nome; ?></td>
                   <td><?php echo $dado->cliente_nome; ?></td>
                   <td><?php echo $dado->idVendas; ?></td>
                   <td><?php echo $dado->dataVenda; ?></td>
                   <td><?php echo $dado->valorTotal; ?></td>
                   <td>
                      <!-- NOVA VENDA -->
                      <?php if(verificarPermissao('vVenda')){ ?>
                        <?php if($dado->faturado == '1'){ ?>
                      <a href="#" onClick="(function(){window.open('<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/visualizarNota/<?php echo $dado->idVendas; ?>','MyWindow','toolbar=no,location=no,directories=no,status=no,menubar=yes,scrollbars=yes,resizable=yes,width=350,height=600');return false;})();return false;"data-toggle="tooltip" title="Visualizar"><i class="fa fa-search  text-success"></i> </a>
                        <?php } ?>
                      <?php } ?>

                      <?php if(verificarPermissao('eVenda')){ ?>
                      <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/editar/<?php echo $dado->idVendas; ?>" data-toggle="tooltip" title="Editar"><i class="fa fa-edit"></i> </a>
                      <?php } ?>
                      <?php if(verificarPermissao('dVenda')){ ?>                     
                      <a style="cursor:pointer;" title="Deletar" data-toggle="modal" data-target="#excluir" onclick="excluir('<?php echo base_url().$this->uri->segment(1)."/excluir/".$dado->idVendas."/A" ?>');" ><i class="fa fa-trash text-danger"></i></a>
                      <?php } ?>             
                   </td>

              </tr>
              
          <?php }  ?>    
          </tbody>

          <tfoot>
            <tr>                     
              <th>Vendedor</th>
              <th>Cliente</th>
              <th>N° da Nota</th>
              <th>Data</th>
              <th>Total</th>
              <th>Ações</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</section>