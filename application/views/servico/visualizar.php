<section class="content">
  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form  action="<?php echo current_url(); ?>" method="post">
        <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $dados[0]->servico_id; ?>" placeholder="id">



        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">

              <div class="form-group col-md-4">
                <label>Nome do Serviço</label>
                <input disabled type="text" class="form-control" name="servico_nome" id="servico_nome" value="<?php echo $dados[0]->servico_nome; ?>" placeholder="Nome do Serviço">
              </div>

              <div class="form-group col-md-4">
                <label>Código Interno</label>
                <input disabled type="text" class="form-control" name="servico_codigo" id="servico_codigo"value="<?php echo $dados[0]->servico_codigo; ?>" placeholder="Código Interno">
              </div>


              <div class="form-group col-md-4">
                <label>Valor Unitário</label>
                  <input disabled type="text" class="form-control money" name="servico_valor" id="produto_preco_venda" value="<?php echo $dados[0]->servico_valor; ?>" placeholder="Valor Unitário">
              </div>
            </div>

            <!-- /.col -->
            <div class="col-md-12">
              <div class="form-group col-md-12">
                <label>Descrição</label>
                <textarea class="form-control" rows="3" name="servico_descricao"  placeholder="Enter ..." disabled><?php echo $dados[0]->servico_descricao; ?></textarea>                  
              </div>
            </div>
            <!-- /.col -->

          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->

          <!-- /.box-body -->
          <div class="box-footer">
            <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>" class="btn btn-default">Voltar</a>
            
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
  </div>
</section>