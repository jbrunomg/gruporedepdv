<section class="content">
  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="<?php echo current_url(); ?>" method="post" enctype="multipart/form-data">
        <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $dados[0]->produto_id; ?>" placeholder="id">
         <?php echo form_hidden('produto_imagem',$dados[0]->produto_imagem) ?>
          <div class="box-body">   

          <div class="form-group">
            <label for="produto_categoria_id" class="col-sm-2 control-label">Grupo Produto</label>
            <div class="col-sm-5">         
              <select class="form-control" name="produto_categoria_id" id="produto_categoria_id">                
                <?php foreach ($grupo as $g) { ?>
                  <?php $selected = ($g->categoria_prod_id == $dados[0]->produto_categoria_id)?'SELECTED':''; ?>
                  <option value='<?php echo $g->categoria_prod_id; ?>' <?php echo $selected; ?>><?php echo $g->categoria_prod_descricao; ?> </option>
                <?php } ?>
              </select>
            </div>
          </div>  

          <div class="form-group">
            <label for="produto_codigo" class="col-sm-2 control-label">Código </label>
            <div class="col-sm-5">              
              <input type="text" class="form-control" name="produto_codigo" id="produto_codigo" value="<?php echo $dados[0]->produto_codigo; ?>" placeholder="código">
            </div>
          </div> 

          <div class="form-group">
              <label for="produto_descricao" class="col-sm-2 control-label">Descrição </label>
            <div class="col-sm-5">              
              <input type="text" class="form-control" name="produto_descricao" id="produto_descricao" value="<?php echo $dados[0]->produto_descricao; ?>" placeholder="produto descricao">
            </div>
          </div> 

          <div class="form-group">
            <label for="produto_unidade" class="col-sm-2 control-label">Unidade </label>
            <div class="col-sm-5">                 
              <select class="form-control" name="produto_unidade" id="produto_unidade">
                <option value="">Selecione</option>
                <option value="UND"  <?php echo ($dados[0]->produto_unidade == 'UND')?'SELECTED':''; ?>>UND</option>
                <option value="PCT"  <?php echo ($dados[0]->produto_unidade == 'PCT')?'SELECTED':''; ?>>PCT</option>
                <option value="KG"   <?php echo ($dados[0]->produto_unidade == 'KG')?'SELECTED':''; ?>>KG</option> 
                <option value="METRO" <?php echo ($dados[0]->produto_unidade == 'METRO')?'SELECTED':''; ?>>METRO</option>  
              </select>
            </div>
          </div>

          <div class="form-group">
            <label for="produto_codigo_barra" class="col-sm-2 control-label">Cód Barra </label>
            <div class="col-sm-5">              
              <input type="text" class="form-control" name="produto_codigo_barra" id="produto_codigo_barra" value="<?php echo $dados[0]->produto_codigo_barra; ?>" placeholder="Cód barra">
            </div>
          </div> 

          <div class="form-group">
            <label for="produto_gaveta" class="col-sm-2 control-label">Gaveta </label>
            <div class="col-sm-5">              
              <input type="text" class="form-control" name="produto_gaveta" id="produto_gaveta" value="<?php echo $dados[0]->produto_gaveta; ?>" placeholder="gaveta">
            </div>
          </div> 

          <div class="form-group">
            <label for="produto_preco_dolar" class="col-sm-2 control-label">Preço Dolar </label>
            <div class="col-sm-5">              
              <input type="text" class="form-control money" name="produto_preco_dolar" id="produto_preco_dolar" value="<?php echo $dados[0]->produto_preco_dolar; ?>" placeholder="preço dolar">
            </div>
          </div>   

          <div class="form-group">
            <label for="produto_preco_custo" class="col-sm-2 control-label">Preço Custo </label>
            <div class="col-sm-5">              
              <input type="text" class="form-control" name="produto_preco_custo" id="produto_preco_custo" value="<?php echo $dados[0]->produto_preco_custo; ?>" placeholder="preço custo">
            </div>
          </div> 

          <div class="form-group">
            <label for="produto_preco_minimo_venda" class="col-sm-2 control-label">Preço Minimo Venda</label>
            <div class="col-sm-5">              
              <input type="text" class="form-control money" name="produto_preco_minimo_venda" id="produto_preco_minimo_venda" value="<?php echo $dados[0]->produto_preco_minimo_venda; ?>" placeholder="preço minimo venda">
            </div>
          </div>

          <div class="form-group">
            <label for="produto_preco_venda" class="col-sm-2 control-label">Preço Venda </label>
            <div class="col-sm-5">              
              <input type="text" class="form-control money" name="produto_preco_venda" id="produto_preco_venda" value="<?php echo $dados[0]->produto_preco_venda; ?>" placeholder="preço venda">
            </div>
          </div>

          <div class="form-group">
            <label for="produto_preco_cart_debito" class="col-sm-2 control-label">Preço Cartão-débito</label>
            <div class="col-sm-5">              
              <input type="text" class="form-control money" name="produto_preco_cart_debito" id="produto_preco_cart_debito" value="<?php echo $dados[0]->produto_preco_cart_debito; ?>" placeholder="preço cartão débito">
            </div>
          </div>

          <div class="form-group">
            <label for="produto_preco_cart_credito" class="col-sm-2 control-label">Preço Cartão-crédito</label>
            <div class="col-sm-5">              
              <input type="text" class="form-control money" name="produto_preco_cart_credito" id="produto_preco_cart_credito" value="<?php echo $dados[0]->produto_preco_cart_credito; ?>" placeholder="preço cartão crédito">
            </div>
          </div>    

          <div class="form-group">
            <label for="produto_estoque_minimo" class="col-sm-2 control-label">Estoque Minimo </label>
            <div class="col-sm-5">              
              <input type="text" class="form-control" name="produto_estoque_minimo" id="produto_estoque_minimo" value="<?php echo $dados[0]->produto_estoque_minimo; ?>" placeholder="estoque minimo">
            </div>
          </div>  

          <div class="form-group">                
              <label for="produto_imagem" class="col-sm-2 control-label">Imagem </label>
              <div class="controls">
                  <input id="arquivo" type="file" name="userfile" /> (png|jpg|jpeg)
              </div>
          </div>  


          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>" class="btn btn-default">Voltar</a>
            <button type="submit" class="btn btn-primary pull-right"><?php echo ucfirst($this->uri->segment(2)); ?></button>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
  </div>
</section>