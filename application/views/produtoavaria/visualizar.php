<section class="content">
  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="<?php echo current_url(); ?>" method="post">
        <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $dados[0]->produto_id; ?>" placeholder="id">
          <div class="box-body">

            <div class="form-group">
              <label for="produto_codigo" class="col-sm-2 control-label">Código</label>
              <div class="col-sm-5">              
                <input disabled type="text" class="form-control" name="produto_codigo" id="produto_codigo" value="<?php echo $dados[0]->produto_codigo; ?>" placeholder="Descrição">
              </div>
            </div>
            <div class="form-group">
              <label for="produto_descricao" class="col-sm-2 control-label">Descrição</label>
              <div class="col-sm-5">              
                <input disabled type="text" class="form-control" name="produto_descricao" id="produto_descricao" value="<?php echo $dados[0]->produto_descricao; ?>" placeholder="descricao">
              </div>
            </div>

            <div class="form-group">
              <label for="produto_IMEI" class="col-sm-2 control-label">IMEI (A / N)</label>
              <div class="col-sm-5">              
                <input disabled type="text" class="form-control" name="produto_IMEI" id="produto_IMEI" value="<?php echo $dados[0]->produto_avaria_antigo_imei.' / '.$dados[0]->produto_avaria_novo_imei  ?>" placeholder="IMEI">
              </div>
            </div>

            <div class="form-group">
              <label for="produto_avarias_quantidade" class="col-sm-2 control-label">Quantidade</label>
              <div class="col-sm-5">              
                <input disabled type="text" class="form-control" name="produto_avarias_quantidade" id="produto_avarias_quantidade" value="<?php echo $dados[0]->produto_avarias_quantidade; ?>" placeholder="quantidade">
              </div>
            </div>

            <div class="form-group">
              <label for="produto_avaria_cadastro" class="col-sm-2 control-label">Data</label>
              <div class="col-sm-5">              
                <input disabled type="text" class="form-control" name="produto_avaria_cadastro" id="produto_avaria_cadastro" value="<?php echo date(('d/m/Y'),strtotime($dados[0]->produto_avaria_cadastro)) ; ?>" placeholder="cadastro">
              </div>
            </div>

  
            <div class="form-group">
              <label for="produto_avaria_cadastro" class="col-sm-2 control-label">Venda</label>

              <div class="input-group col-sm-4">
                <input disabled type="text" name="message" value="<?php echo $dados[0]->venda_id; ?>" class="form-control">
                <span class="input-group-btn">
                  <a href="#" onClick="(function(){window.open('<?php echo base_url(); ?>vendas/visualizarNota/<?php echo  $dados[0]->venda_id; ?>','MyWindow','toolbar=no,location=no,directories=no,status=no,menubar=yes,scrollbars=yes,resizable=yes,width=350,height=600');return false;})();return false;"data-toggle="tooltip" title="Visualizar">
                    <button class="btn btn-primary btn-flat"><i class="fa fa-search  text-success"></i></button>
                  </a>
                </span>
              </div>              
            </div>



            <div class="form-group">
              <label for="produto_avaria_observacao" class="col-sm-2 control-label">Observação</label>
              <div class="col-sm-5">              
                <input disabled type="text" class="form-control" name="produto_avaria_observacao" id="produto_avaria_observacao" value="<?php echo $dados[0]->produto_avaria_observacao; ?>" placeholder="observacao">
              </div>
            </div>



          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>" class="btn btn-default">Voltar</a>
            
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
  </div>
</section>