<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Relatórios Customizáveis</h3>
                </div>
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="row">
                            <form action="<?php echo base_url()?>balancoproduto/adicionar" method="post">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Grupo</label>
                                            <select id="grupo_produto" class="form-control" data-placeholder="Selecione o grupo de produtos" name="produto_categoria_id" required="required">
                                                <option value="">Selecione</option>
                                                <?php foreach ($grupo as $g) { ?>
                                                    <option value='<?php echo $g->categoria_prod_id; ?>'><?php echo $g->categoria_prod_descricao; ?> </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Código</label>
                                            <!-- Campo hidden para armazenar o código do produto -->
                                            <input type="hidden" name="codigo_produto" id="codigo_produto_real">  
                                            <select id="codigo_produto" class="form-control select2" data-placeholder="Selecione o código" required="required">
                                                <option value="">Selecione um grupo</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row no-print text-center" style="margin-top: 40px">
                                    <div class="col-md-12">
                                        <button type="reset" class="btn btn-default" id="reset" onclick="resetarSelectCodigos()">
                                            <i class="fa fa-eraser"></i> Limpar
                                        </button>
                                        <button type="submit" class="btn btn-primary" style="margin-right: 5px;">
                                            Avançar
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    function resetarSelectCodigos() {
        $("#codigo_produto").html('<option value="">Selecione um grupo</option>');
    }

    $(document).ready(function () {
        function preencherSelectCodigos() {
            var grupo = $("#grupo_produto").val();

            if (grupo) {
                $.ajax({
                    method: "POST",
                    url: "<?php echo base_url(); ?>balancoproduto/preencherSelectCodigos",
                    dataType: "json",
                    data: { grupo: grupo }
                }).done(function(response) {
                    var options = '<option value="">Selecione</option>';
                    response.forEach(function(item) {
                        options += '<option value="' + item.codigo_prod_codigo + '">' + item.codigo_prod_codigo + ' - ' + item.codigo_prod_descricao + '</option>';
                    });
                    $("#codigo_produto").html(options);
                });
            } else {
                resetarSelectCodigos();
            }
        }

        $("#grupo_produto").change(function () {
            preencherSelectCodigos();
        });

        $("#codigo_produto").change(function () {
            var selectedOption = $(this).find('option:selected').val();
            $("#codigo_produto_real").val(selectedOption);
        });
    });
</script>
