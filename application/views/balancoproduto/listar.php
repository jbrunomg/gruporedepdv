<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <div class="col-md-2">
                <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/adicionar" type="button" class="btn btn-block btn-default btn-flat">Adicionar</a>
            </div>
        </div>
        <div class="box-body">
            <div class="container-fluid">
                <table id="balanco-produto" class="table" width="100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Categoria Produto</th>
                            <th>Código Produto/produto</th>
                            <!-- <th>Produto</th> -->
                            <th>Data Cadastro</th>
                            <th>Data Finalização</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($dados as $d) { ?>
                            <tr>
                                <td><?php echo $d->balanco_produto_id; ?></td> 
                                <td><?php echo $d->categoria_prod_descricao; ?></td>
                                <td><?php echo $d->balanco_produto_codigo_produto;?> - <?php echo $d->codigo_prod_descricao; ?></td> 
                                <td><?php echo date('d/m/Y H:i:s', strtotime($d->balanco_produto_data_cadastro)); ?></td>
                                <td><?php echo $d->balanco_produto_data_finalizacao ? date('d/m/Y H:i:s', strtotime($d->balanco_produto_data_finalizacao)) : 'Em aberto'; ?></td>
                                <td>
                                    <?php if (!$d->balanco_produto_data_finalizacao) { ?>
                                        <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/editar/<?php echo $d->balanco_produto_id; ?>" data-toggle="tooltip" title="Editar"><i class="fa fa-edit"></i> </a>
                                    <?php } else { ?>
                                        <a href="<?php echo base_url(); ?>relatorios/<?php echo $this->uri->segment(1);?>/<?php echo $d->balanco_produto_id; ?>" data-toggle="tooltip" title="Relatório" target="_blank"><i class="fa fa-file-pdf-o"></i></a>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Categoria Produto</th>
                            <th>Código Produto</th>
                            <!-- <th>Produto</th> -->
                            <th>Data Cadastro</th>
                            <th>Data Finalização</th>
                            <th>Ações</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function () {
        $('#balanco-produto').DataTable({
            "order": [
                [0, 'desc']
            ],
            "bDestroy": true
        });
    });
</script>