<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios Customizáveis</h3>
				</div>
				<div class="box-body">
					<div class="col-md-12">
						<div class="row">
							<form method="post">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Categoria Produto</label>
                                            <input type="text" class="form-control" name="categoria_produto" value="<?= $dados->categoria_prod_descricao; ?>" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Código Produto</label>
                                            <input type="text" class="form-control" name="codigo_produto" value="<?= $dados->balanco_produto_codigo_produto; ?>" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Data Cadastro</label>
                                            <input type="text" class="form-control" name="data_cadastro" value="<?= date('d/m/Y H:i:s', strtotime($dados->balanco_produto_data_cadastro)); ?>" readonly>
                                        </div>
                                    </div>
								</div>
								<?php if (!$dados->balanco_produto_data_finalizacao) { ?>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<label>Imeis | <span id="contador">Total: 0</span></label>
												<textarea class="form-control" rows="20" name="imeis" id="imeis"><?= set_value('imeis'); ?></textarea>
											</div>
										</div>
									</div>
								<?php } ?>
								<div class="row no-print text-center" style="margin-top: 40px">
									<div class="col-md-12">
										<?php if (!$dados->balanco_produto_data_finalizacao) { ?>
											<button type="reset" class="btn btn-default" id="reset" onclick="resetarSelectCodigos()">
												<i class="fa fa-eraser"></i> Limpar
											</button>
											<button type="submit" class="btn btn-primary" style="margin-right: 5px;">
												Avançar
											</button>
										<?php } else { ?>
											<a href="<?= base_url() ?>balancoproduto" class="btn btn-default">
												Voltar
											</a>
											<a href="<?= base_url() ?>relatorios/balancoproduto/<?= $this->uri->segment(3); ?>" class="btn btn-primary" target="_blank">
												Relatório
											</a>
										<?php } ?>
									</div>
								</div>
						    </form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<script>
	function contarLinhas() {
		var texto = document.getElementById("imeis").value;
		var linhas = texto ? texto.split(/\r*\n/).length : 0;
		document.getElementById("contador").innerText = "Total: " + linhas;
	}

	contarLinhas();

	document.getElementById("imeis").addEventListener("input", contarLinhas);
</script>