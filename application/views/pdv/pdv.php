<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>MEGA | Controler</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link href="<?php echo base_url(); ?>assets/bootstrapPDV/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>assets/plugins/iCheck/square/green.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>/assets/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>/assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>/assets/plugins/redactor/redactor.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>/assets/dist/css/jquery-ui.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>assets/dist/css/custom.css" rel="stylesheet" type="text/css" />
	<script src="<?php echo base_url(); ?>assets/plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
</head>
<body class="skin-<?php echo CLIENTE ?> sidebar-collapse sidebar-mini pos">
	<div class="wrapper">
		<header class="main-header">
			<a href="<?php echo base_url(); ?>sistema/dashboard" class="logo"> <span class="logo-mini">PDV</span></a>
			<nav class="navbar navbar-static-top" role="navigation">
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<li><a href="#" class="filialAtual"><?php $loja = BDCAMINHO; $loja = explode("_", $loja); echo strtoupper($loja[2]); ?></a></li>
						<li><a href="#" class="clock"></a></li>
						<li><a href="http://localhost/sistemapdv/"><i class="fa fa-dashboard"></i></a></li>
					    <li><a href="http://localhost/sistemapdv/settings"><i class="fa fa-cogs"></i></a></li>
						<li><a href="http://localhost/sistemapdv/pos/view_bill" target="_blank"><i class="fa fa-file-text-o"></i></a></li>
						<li><a href="http://localhost/sistemapdv/pos/register_details" data-toggle="ajax">Histórico de Vendas</a></li>
						<li><a href="http://localhost/sistemapdv/pos/today_sale" data-toggle="ajax">Venda do dia</a></li>
						<li><a href="http://localhost/sistemapdv/pos/close_register" data-toggle="ajax">Fechar Caixa</a></li>
						<li class="dropdown notifications-menu">
						    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
						        <i class="fa fa-bell-o"></i>
						        <span class="label label-warning">1</span>
						    </a>
						    <ul class="dropdown-menu">
						        <li class="header">Vendas recentes em aberto</li>
						        <li>
						            <ul class="menu">
						                <li>
						                <a href="http://localhost/sistemapdv/pos/?hold=2" class="load_suspended">3030/0303/2016161616 08:11:01 PM (Cliente Padr?o)<br><strong>teste</strong></a>						                </li>
						            </ul>
						        </li>
						        <li class="footer"><a href="http://localhost/sistemapdv/sales/opened">Ver tudo</a></li>
						    </ul>
						</li>
					      <li class="dropdown user user-menu">
					          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
					            <img src="<?php echo base_url(); ?>assets/dist/img/user2-160x160.jpg"  class="user-image" alt="Avatar" />
					            <span class="hidden-xs"><?php echo $this->session->userdata('usuario_nome'); ?></span>
					          </a>
					          <ul class="dropdown-menu">
					            <li class="user-header">
					              <img src="<?php echo base_url(); ?>assets/dist/img/user2-160x160.jpg"  class="img-circle" alt="Avatar" />
					              <p>
					               <?php echo $this->session->userdata('usuario_nome'); ?> <br> <?php echo $this->session->userdata('usuario_perfil_nome'); ?>
					              </p>
					            </li>
					            <li class="user-footer">
					              <div class="pull-left">
					                <a href="#" class="btn btn-default btn-flat" data-toggle="modal" data-target=".bs-example-modal-sm">Alterar Senha</a>
					              </div>
					              <div class="pull-right">
					                <a href="<?php echo base_url(); ?>sistema/logout" class="btn btn-default btn-flat">Sair</a>
					              </div>
					            </li>
					          </ul>
					        </li>
<!-- 						<li>
							<a href="#" data-toggle="control-sidebar" class="sidebar-icon" id="categorias"><i class="fa fa-folder sidebar-icon"></i></a>
						</li> -->
			            <li>
							<a href="#" data-toggle="control-sidebar" class="sidebar-icon"><i class="fa fa-folder sidebar-icon"></i></a>
						</li>
					</ul>
				</div>
			</nav>
		</header>

		<aside class="main-sidebar">
			<section class="sidebar">
				<ul class="sidebar-menu">
					    <li class="treeview">
					      <a href="<?php echo base_url(); ?>sistema/dashboard">
					        <i class="fa fa-dashboard"></i> <span>Dashboard</span>           
					      </a>          
					    </li>	
                        <?php if(verificarPermissao('vVenda')){ ?>
					    <li class="treeview">
					      <a href="<?php echo base_url(); ?>PDV">
					        <i class="fa fa-shopping-cart"></i> <span>PDV</span>           
					      </a>          
					    </li>	
		                <?php } ?>				
				</ul>
			</section>
	    </aside>

		<div class="content-wrapper">
			<div class="col-lg-12 alerts">			
			  <!-- Tratamento de mensagens do sitema -->
			<?php if($this->session->flashdata('erro')){ ?>
			  <div class="col-md-12">
			    <div class="alert alert-danger alert-dismissible">
			      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			      <h5><?php echo $this->session->flashdata('erro'); ?></h5>                
			    </div>
			  </div>
			 <?php }else if($this->session->flashdata('success')){ ?>
			  <div class="col-md-12">
			    <div class="alert alert-success alert-dismissible">
			      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			      <h5></i> <?php echo $this->session->flashdata('success'); ?></h5>               
			    </div>
			  </div>
			 <?php } ?>

			</div>
			<table style="width:100%;" class="layout-table">
				<tr>
					<td style="width: 460px;">
						<div id="pos">
							<form action="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/venda" id="pos-sale-form" method="post" accept-charset="utf-8">
                              <input type="hidden" name="spos_token" value="f661c6c8f4ac92f2c469a3cc6bae7757" style="display:none;" />
							<div class="well well-sm" id="leftdiv">
								<div id="lefttop" style="margin-bottom:5px;">
									<div class="form-group" style="margin-bottom:5px;">
										<div class="input-group">
								            <select name="customer_id" id="spos_customer" data-placeholder="Selecione Cliente" required="required" class="form-control select2" style="width:100%;">
							                    <option value="">Selecione um Cliente</option>                       
							                 </select>    
											<div class="input-group-addon no-print" style="padding: 2px 5px;">
												<a href="#" id="add-customer" class="external" data-toggle="modal" data-target="#myModal"><i class="fa fa-2x fa-plus-circle" id="addIcon"></i></a>
											</div>
										</div>
										<div style="clear:both;"></div>
									</div>
									<div class="form-group" style="margin-bottom:5px;">
										<input type="text" name="code" id="add_item" class="form-control" placeholder="Procure o produto pelo código, nome ou código de barras" />
									</div>
								</div>
								<div id="printhead" class="print">
									<h2>CADPROJETOS - PDV</h2>Rua Afonso Marques, 441 - Centro,<br>
                                                                                      CEP 62.300-000 - Viçosa - CE<br>											<p>Date: 2121/0808/2019191919</p>
								</div>
								<div id="print">
									<div id="list-table-div">
										<table id="posTable" class="table table-striped table-condensed table-hover list-table" style="margin:0;">
											<thead>
												<tr class="success">
													<th>Produto</th>
													<th style="width: 15%;text-align:center;">Preço</th>
													<th style="width: 15%;text-align:center;">Qtd</th>
													<th style="width: 20%;text-align:center;">Subtotal</th>
													<th style="width: 20px;" class="satu"><i class="fa fa-trash-o"></i></th>
												</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
									<div style="clear:both;"></div>
									<div id="totaldiv">
										<table id="totaltbl" class="table table-condensed totals" style="margin-bottom:10px;">
											<tbody>
												<tr class="info">
													<td width="25%">Total de Itens</td>
													<td class="text-right" style="padding-right:10px;"><span id="count">0</span></td>
													<td width="25%">Total</td>
													<td class="text-right" colspan="2"><span id="total">0</span></td>
												</tr>
<!-- 												<tr class="info">
													<td width="25%"><a href="#" id="add_discount">Desconto</a></td>
													<td class="text-right" style="padding-right:10px;"><span id="ds_con">0</span></td>
													<td width="25%"><a href="#" id="add_tax">Taxa</a></td>
													<td class="text-right"><span id="ts_con">0</span></td>
												</tr> -->
												<tr class="success">
													<td colspan="2" style="font-weight:bold;">Total a Pagar</td>
													<td class="text-right" colspan="2" style="font-weight:bold;"><span id="total-payable">0</span></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<div id="botbuttons" class="col-xs-12 text-center">
									<div class="row">
										<div class="col-xs-4" style="padding: 0;">
											<div class="btn-group-vertical btn-block">
												<button type="button" class="btn btn-warning btn-block btn-flat"
												id="suspend">Aguardar</button>
												<button type="button" class="btn btn-danger btn-block btn-flat"
												id="reset">Cancelar</button>
											</div>

										</div>
										<div class="col-xs-4" style="padding: 0 5px;">
											<div class="btn-group-vertical btn-block">
												<button type="button" class="btn bg-purple btn-block btn-flat" id="print_order">Ver Itens</button>

												<button type="button" class="btn bg-navy btn-block btn-flat" id="print_bill">Ver Fatura</button>
											</div>
										</div>
										<div class="col-xs-4" style="padding: 0;">
											<button type="button" class="btn btn-success btn-block btn-flat" id="payment" style="height:67px;">Pagamento</button>
										</div>
									</div>

								</div>
								<div class="clearfix"></div>
								<span id="hidesuspend"></span>
								<input type="hidden" name="spos_note" value="" id="spos_note">

								<div id="payment-con">
									<input type="hidden" name="amount" id="amount_val" value=""/>
									<input type="hidden" name="balance_amount" id="balance_val" value=""/>
									<input type="hidden" name="paid_by" id="paid_by_val" value="cash"/>
									<input type="hidden" name="cc_no" id="cc_no_val" value=""/>
									<input type="hidden" name="paying_gift_card_no" id="paying_gift_card_no_val" value=""/>
									<input type="hidden" name="cc_holder" id="cc_holder_val" value=""/>
									<input type="hidden" name="cheque_no" id="cheque_no_val" value=""/>
									<input type="hidden" name="cc_month" id="cc_month_val" value=""/>
									<input type="hidden" name="cc_year" id="cc_year_val" value=""/>
									<input type="hidden" name="cc_type" id="cc_type_val" value=""/>
									<input type="hidden" name="cc_cvv2" id="cc_cvv2_val" value=""/>
									<input type="hidden" name="balance" id="balance_val" value=""/>
									<input type="hidden" name="payment_note" id="payment_note_val" value=""/>
								</div>
								<input type="hidden" name="customer" id="customer" value="1" />
								<input type="hidden" name="order_tax" id="tax_val" value="" />
								<input type="hidden" name="order_discount" id="discount_val" value="" />
								<input type="hidden" name="count" id="total_item" value="" />
								<input type="hidden" name="did" id="is_delete" value="0" />
								<input type="hidden" name="eid" id="is_delete" value="0" />
								<input type="hidden" name="hold_ref" id="hold_ref" value="" />
								<input type="hidden" name="total_items" id="total_items" value="0" />
								<input type="hidden" name="total_quantity" id="total_quantity" value="0" />
								<input type="submit" id="submit" value="Submit Sale" style="display: none;" />
							</div>
							</form>								</div>

					</td>
					<td>
						<div class="contents" id="right-col">
							<div id="item-list">
								<div class="items">
<!-- 									<div id='categoria_prod'>
										<?php foreach ($produtos as $p) { ?>
										<button type="button" id="product-0123" type="button" value='02' class="btn btn-both btn-flat product"><span class="bg-img">
											<img src="http://localhost/sistemapdv/uploads/thumbs/99ba15cb71eafc2bf6675c47eda93853.gif"  style="width: 100px; height: 100px;"></span>
											<span><span><?php echo $p->produto_descricao ?></span></span>
										</button>
								     	<?php } ?>
 -->

										<?php echo $produtos; ?> 
									
<!-- 										<button type="button" data-name="XEROX - COLORIDA" id="product-0124" type="button" value='03' class="btn btn-both btn-flat product"><span class="bg-img">
											<img src="http://localhost/sistemapdv/uploads/thumbs/1c485ac2a3a40d5065a8ff8736311008.gif" alt="XEROX - COLORIDA" style="width: 100px; height: 100px;"></span>
											<span><span>XEROX - COLORIDA</span></span>
										</button> -->
									<!-- </div>	 -->									
								</div>
							</div>
							<div class="product-nav">
								<div class="btn-group btn-group-justified">
									<div class="btn-group">
										<button style="z-index:10002;" class="btn btn-warning pos-tip btn-flat" type="button" id="previous"><i class="fa fa-chevron-left"></i></button>
									</div>
									<div class="btn-group">
										<button style="z-index:10003;" class="btn btn-success pos-tip btn-flat" type="button" ><i class="fa fa-credit-card" id="addIcon"></i> Lista de Produto da Loja</button>
									</div>
									<div class="btn-group">
										<button style="z-index:10004;" class="btn btn-warning pos-tip btn-flat" type="button" id="next"><i class="fa fa-chevron-right"></i></button>
									</div>
								</div>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</div>

<!-- 			<aside class="control-sidebar control-sidebar-dark" id="categories-list">
				<div class="tab-content">
					<div class="tab-pane active" id="control-sidebar-home-tab">
						<ul class="control-sidebar-menu" id="categorias_desc">					
					   </ul>
				</div>
			</div>
		</aside> -->

<aside class="control-sidebar control-sidebar-dark" id="categories-list">
	<ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-theme-demo-options-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <!-- <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li> -->
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
	<div class="tab-content">
      <div id="control-sidebar-theme-demo-options-tab" class="tab-pane active">
      	<div>
      		<h3 class="control-sidebar-heading">Filias</h3>
      		   <div class="tab-pane active" id="control-sidebar-home-tab">
					<ul class="control-sidebar-menu">
						<?php
						foreach($filias as $fili) {
							$f = $fili;
							$filias = explode("_".GRUPOLOJA."_", $f);

							echo '<li><a href="#" class="filias'.($fili == BDCAMINHO ? ' active' : '').'" id="'.$fili.'">';
							if($filias[1] == 'matriz') {
								echo '<div class="menu-icon"><img src="'.base_url('assets/img/matriz.png').'" alt="" class="img-thumbnail img-circle img-responsive"></div>';
							} elseif($filias[1] == 'game') {
								echo '<div class="menu-icon"><img src="'.base_url('assets/img/game.png').'" alt="" class="img-thumbnail img-circle img-responsive"></div>';
							} elseif($filias[1] == 'carlos') {
								echo '<div class="menu-icon"><img src="'.base_url('assets/img/carlos.png').'" alt="" class="img-thumbnail img-circle img-responsive"></div>';
							} elseif($filias[1] == 'mega') {
								echo '<div class="menu-icon"><img src="'.base_url('assets/img/mega.png').'" alt="" class="img-thumbnail img-circle img-responsive"></div>';
							} elseif($filias[1] == 'paloma') {
								echo '<div class="menu-icon"><img src="'.base_url('assets/img/paloma.png').'" alt="" class="img-thumbnail img-circle img-responsive"></div>';
							} else {
								echo '<i class="menu-icon fa fa-folder-open bg-red"></i>';
							}
							echo '<div class="menu-info"><h4 class="control-sidebar-subheading">'.$filias[1].'</h4></div>
						</a></li>';
					}
					?>
				</ul>
			</div>
      	</div>
      </div>
      <div class="tab-pane" id="control-sidebar-settings-tab">
          <h3 class="control-sidebar-heading">Categorias</h3>
          <div class="tab-pane active" id="control-sidebar-home-tab">
					<ul class="control-sidebar-menu" id='categorias_prod_filias'>
						<?php
						foreach($categorias as $category) {
							$filias = explode("_".GRUPOLOJA."_", BDCAMINHO);
							echo '<li><a href="#" class="category'.($category->categoria_prod_id == '1' ? ' active' : '').'" id="'.$category->categoria_prod_id.'">';
							if($filias[1] == 'matriz') {
								echo '<div class="menu-icon"><img src="'.base_url('assets/img/matriz.png').'" alt="" class="img-thumbnail img-circle img-responsive"></div>';
							} elseif($filias[1] == 'game') {
								echo '<div class="menu-icon"><img src="'.base_url('assets/img/game.png').'" alt="" class="img-thumbnail img-circle img-responsive"></div>';
							} elseif($filias[1] == 'carlos') {
								echo '<div class="menu-icon"><img src="'.base_url('assets/img/carlos.png').'" alt="" class="img-thumbnail img-circle img-responsive"></div>';
							} elseif($filias[1] == 'mega') {
								echo '<div class="menu-icon"><img src="'.base_url('assets/img/mega.png').'" alt="" class="img-thumbnail img-circle img-responsive"></div>';
							} elseif($filias[1] == 'paloma') {
								echo '<div class="menu-icon"><img src="'.base_url('assets/img/paloma.png').'" alt="" class="img-thumbnail img-circle img-responsive"></div>';
							} else {
								echo '<i class="menu-icon fa fa-folder-open bg-red"></i>';
							}
							echo '<div class="menu-info"><h4 class="control-sidebar-subheading">'.$category->categoria_prod_id.'</h4><p><strong>'.$category->categoria_prod_descricao.'</strong></p></div>
						</a></li>';
					}
					?>
				</ul>
			</div>
      </div>
      <!-- /.tab-pane -->
    </div>
</aside>


<div class="control-sidebar-bg"></div>
</div>
</div>
<div id="order_tbl" style="display:none;"><span id="order_span"></span>
	<table id="order-table" class="prT table table-striped table-condensed" style="width:100%;margin-bottom:0;"></table>
</div>
<div id="bill_tbl" style="display:none;"><span id="bill_span"></span>
	<table id="bill-table" width="100%" class="prT table table-striped table-condensed" style="width:100%;margin-bottom:0;"></table>
	<table id="bill-total-table" width="100%" class="prT table table-striped table-condensed" style="width:100%;margin-bottom:0;"></table>
</div>

<div class="modal" data-easein="flipYIn" id="posModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>
<div class="modal" data-easein="flipYIn" id="posModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true"></div>
<div id="ajaxCall"><i class="fa fa-spinner fa-pulse"></i></div>

<div class="modal" data-easein="flipYIn" id="gcModal" tabindex="-1" role="dialog" aria-labelledby="mModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
				<h4 class="modal-title" id="myModalLabel">Vender Cartão da Loja</h4>
			</div>
			<div class="modal-body">
				<p>Por favor, preencha as informações abaixo</p>

				<div class="alert alert-danger gcerror-con" style="display: none;">
					<button data-dismiss="alert" class="close" type="button">×</button>
					<span id="gcerror"></span>
				</div>
				<div class="form-group">
					<label for="gccard_no">Cartão No</label> *
					<div class="input-group">
						<input type="text" name="gccard_no" value=""  class="form-control" id="gccard_no" />
						<div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;"><a href="#" id="genNo"><i class="fa fa-cogs"></i></a></div>
					</div>
				</div>
				<input type="hidden" name="gcname" value="Cartão da Loja" id="gcname"/>
				<div class="form-group">
					<label for="gcvalue">Valor</label> *
					<input type="text" name="gcvalue" value=""  class="form-control" id="gcvalue" />
				</div>
				<div class="form-group">
					<label for="gcprice">Preço</label> *
					<input type="text" name="gcprice" value=""  class="form-control" id="gcprice" />
				</div>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Fechar</button>
				<button type="button" id="addGiftCard" class="btn btn-primary">Vender Cartão da Loja</button>
			</div>
		</div>
	</div>
</div>

<div class="modal" data-easein="flipYIn" id="dsModal" tabindex="-1" role="dialog" aria-labelledby="dsModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
				<h4 class="modal-title" id="dsModalLabel">Desconto Descrição</h4>
			</div>
			<div class="modal-body">
				<input type='text' class='form-control input-sm kb-pad' id='get_ds' onClick='this.select();' value=''>

				<label class="checkbox" for="apply_to_order">
					<input type="radio" name="apply_to" value="order" id="apply_to_order" checked="checked"/>
					Aplicar à Venda				</label>
				<label class="checkbox" for="apply_to_products">
					<input type="radio" name="apply_to" value="products" id="apply_to_products"/>
					Aplicar ao Produto				</label>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-sm pull-left" data-dismiss="modal">Fechar</button>
				<button type="button" id="updateDiscount" class="btn btn-primary btn-sm">Atualizar</button>
			</div>
		</div>
	</div>
</div>

<div class="modal" data-easein="flipYIn" id="tsModal" tabindex="-1" role="dialog" aria-labelledby="tsModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
				<h4 class="modal-title" id="tsModalLabel">Taxa Descrição</h4>
			</div>
			<div class="modal-body">
				<input type='text' class='form-control input-sm kb-pad' id='get_ts' onClick='this.select();' value=''>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-sm pull-left" data-dismiss="modal">Fechar</button>
				<button type="button" id="updateTax" class="btn btn-primary btn-sm">Atualizar</button>
			</div>
		</div>
	</div>
</div>

<div class="modal" data-easein="flipYIn" id="proModal" tabindex="-1" role="dialog" aria-labelledby="proModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header modal-primary">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
				<h4 class="modal-title" id="proModalLabel">
					Pagamento				</h4>
			</div>
			<div class="modal-body">
				<table class="table table-bordered table-striped">
					<tr>
						<th style="width:25%;">Preço de Internet</th>
						<th style="width:25%;"><span id="net_price"></span></th>
						<th style="width:25%;">Produto Taxa</th>
						<th style="width:25%;"><span id="pro_tax"></span> <span id="pro_tax_method"></span></th>
					</tr>
				</table>
				<input type="hidden" id="row_id" />
				<input type="hidden" id="item_id" />
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="nPrice">Valor unitário</label>							<input type="text" class="form-control input-sm kb-pad" id="nPrice" onClick="this.select();" placeholder="Novo Preço">
						</div>
						<div class="form-group">
							<label for="nDiscount">Desconto</label>							<input type="text" class="form-control input-sm kb-pad" id="nDiscount" onClick="this.select();" placeholder="Desconto">
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label for="nQuantity">Qtd</label>							<input type="text" class="form-control input-sm kb-pad" id="nQuantity" onClick="this.select();" placeholder="Quantidade Atual">
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Fechar</button>
				<button class="btn btn-success" id="editItem">Atualizar</button>
			</div>
		</div>
	</div>
</div>

<div class="modal" data-easein="flipYIn" id="susModal" tabindex="-1" role="dialog" aria-labelledby="susModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
				<h4 class="modal-title" id="susModalLabel">Suspender Venda</h4>
			</div>
			<div class="modal-body">
				<p>Tipo de referência Nota</p>

				<div class="form-group">
					<label for="reference_note">Nota de referência</label>					<input type="text" name="reference_note" value=""  class="form-control kb-text" id="reference_note" />
				</div>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal"> Fechar </button>
				<button type="button" id="suspend_sale" class="btn btn-primary">Enviar</button>
			</div>
		</div>
	</div>
</div>



<div class="modal" data-easein="flipYIn" id="saleModal" tabindex="-1" role="dialog" aria-labelledby="saleModalLabel" aria-hidden="true"></div>
<div class="modal" data-easein="flipYIn" id="opModal" tabindex="-1" role="dialog" aria-labelledby="opModalLabel" aria-hidden="true"></div>

<div class="modal" data-easein="flipYIn" id="payModal" tabindex="-1" role="dialog" aria-labelledby="payModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-success">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
				<h4 class="modal-title" id="payModalLabel">
					Pagamento				
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-9">
						<div class="font16">
							<table class="table table-bordered table-condensed" style="margin-bottom: 0;">
								<tbody>
									<tr>
										<td width="25%" style="border-right-color: #FFF !important;">Total de Itens</td>
										<td width="25%" class="text-right"><span id="item_count">0.00</span></td>
										<td width="25%" style="border-right-color: #FFF !important;">Total a Pagar</td>
										<td width="25%" class="text-right"><span id="twt">0.00</span></td>
									</tr>
									<tr>
										<td style="border-right-color: #FFF !important;">Total Pago</td>
										<td class="text-right"><span id="total_paying">0.00</span></td>
										<td style="border-right-color: #FFF !important;">Troco</td>
										<td class="text-right"><span id="balance">0.00</span></td>
									</tr>
								</tbody>
							</table>
							<div class="clearfix"></div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<div class="form-group">
									<label for="note">Informações</label>									
									<textarea name="note" id="note" class="pa form-control kb-text"></textarea>
								</div>
							</div>
							<div class="col-xs-6">
								<div class="form-group">
									<label for="amount">Valor Pago</label>									
									<input name="amount[]" type="text" id="amount"
									class="pa form-control kb-pad amount"/>
								</div>
							</div>
							<div class="col-xs-6">
								<div class="form-group">
									<label for="paid_by">Pagar em</label>									
									<select id="paid_by" class="form-control paid_by select2" style="width:100%;">
										<!-- <option value="Dinheiro">Dinheiro</option> -->
										<option value="CC">Cartão de Crédito</option>
										<option value="dinheiro">Dinheiro</option>
										<option value="Cheque">Cheque</option>
										<option value="gift_card">Cartão da Loja</option>
										<option value="stripe">Cartão de Débito</option>									
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<div class="form-group gc" style="display: none;">
									<label for="gift_card_no">Cartão Loja No</label>									
									<input type="text" id="gift_card_no" class="pa form-control kb-pad gift_card_no gift_card_input"/>
									<div id="gc_details"></div>
								</div>
								<div class="pcc" style="display:none;">
									<div class="form-group">
										<input type="text" id="swipe" class="form-control swipe swipe_input" placeholder="Cartão de furto aqui, em seguida, escrever código de segurança manualmente"/>
									</div>
									<div class="row">
										<div class="col-xs-6">
											<div class="form-group">
												<input type="text" id="pcc_no" class="form-control kb-pad" placeholder="Cartão de Crédito No"/>
											</div>
										</div>
										<div class="col-xs-6">
											<div class="form-group">
												<input type="text" id="pcc_holder" class="form-control kb-text" placeholder="Titular em Espera"/>
											</div>
										</div>
										<div class="col-xs-3">
											<div class="form-group">
												<select id="pcc_type" class="form-control pcc_type select2" placeholder="Tipo de Cartão">
												<option value="Visa">Visa</option>
												<option value="MasterCard">MasterCard</option>
												<option value="Amex">Amex</option>
												<option value="Discover">Discover</option>
											</select>
										</div>
									</div>
									<div class="col-xs-3">
										<div class="form-group">
											<input type="text" id="pcc_month" class="form-control kb-pad" placeholder="Mês"/>
										</div>
									</div>
									<div class="col-xs-3">
										<div class="form-group">
											<input type="text" id="pcc_year" class="form-control kb-pad" placeholder="Ano"/>
										</div>
									</div>
									<div class="col-xs-3">
										<div class="form-group">
											<input type="text" id="pcc_cvv2" class="form-control kb-pad" placeholder="CVV2"/>
										</div>
									</div>
								</div>
							</div>
							<div class="pcheque" style="display:none;">
								<div class="form-group"><label for="cheque_no">Cheque No</label>									
									<input type="text" id="cheque_no" class="form-control cheque_no  kb-text"/>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-3 text-center">
					<!-- <span style="font-size: 1.2em; font-weight: bold;">Venda Rápida</span> -->

					<div class="btn-group btn-group-vertical" style="width:100%;">
						<button type="button" class="btn btn-info btn-block quick-cash" id="quick-payable">0.00
						</button>
						<button type="button" class="btn btn-block btn-warning quick-cash">10</button>
						<button type="button" class="btn btn-block btn-warning quick-cash">20</button>
						<button type="button" class="btn btn-block btn-warning quick-cash">50</button>
						<button type="button" class="btn btn-block btn-warning quick-cash">100</button>
						<button type="button" class="btn btn-block btn-warning quick-cash">500</button>						
						<button type="button" class="btn btn-block btn-danger" id="clear-cash-notes">Limpar</button>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default pull-left" data-dismiss="modal"> Fechar </button>
			<button class="btn btn-primary" id="submit-sale">Enviar</button>
		</div>
	</div>
</div>
</div>

<div class="modal" data-easein="flipYIn" id="customerModal" tabindex="-1" role="dialog" aria-labelledby="cModalLabel" aria-hidden="true">
	<div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Adicionar Cliente </h4>
        </div>
        <div class="modal-body">
		<div id="c-alert" class="alert alert-danger" style="display:none;"></div>
		<div class="row">
			<div class="col-xs-12">
				<div class="form-group">
					<label class="control-label" for="cliente_nome">Nome</label>
					<input type="text" name="cliente_nome" id="cliente_nome" value="" class="form-control input-sm kb-text" >
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6">
				<div class="form-group">
					<label class="control-label" for="cliente_email">Endereço de e-mail</label>
					<input type="text" name="cliente_email" id="cliente_email" value="" class="form-control input-sm kb-text" >
				</div>
			</div>
			<div class="col-xs-6">
				<div class="form-group">
					<label class="control-label" for="cliente_numero">Telefone</label>
					<input type="text" name="cliente_numero" id="cliente_numero" value="" class="form-control input-sm kb-pad" >
				</div>
			</div>
		</div>
		<div class="row">
		    <div class="col-xs-6">
				<div class="form-group">
					<label class="control-label" for="tipo">Tipo</label>
				 <select required class="form-control input-sm kb-pad" name="cliente_tipo" id="tipo">
		                <option value='1'>Pessoa Física</option>
		                <option value='2'>Pessoa Jurídica</option>
		            </select>
				</div>
			</div>
			<div class="col-xs-6">
				<div class="form-group">
					<label class="control-label" for="cpfCnpj">CPF/CNPJ</label>
					<input type="text" name="cpfCnpj" id="cpfCnpj"value="" class="form-control input-sm kb-text" >
				</div>
			</div>

		</div>

	   </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Fechar</button>
          <button type="button" class="btn btn-success" id='addCliente' data-dismiss="modal">Adicionar</button>
        </div>
      </div>
	</div>
</div>
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button> 
        <h4 class="modal-title" id="mySmallModalLabel">Alterar Senha</h4> 
      </div> 
      <div class="modal-body">
          <form class="form-horizontal">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="">Senha Atual</label>                  
                    <input class="form-control" id="senha_atual"  type="password">
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="">Nova Senha</label>                  
                    <input class="form-control" id="nova_senha"  type="password">
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="">Confirma Senha</label>                  
                    <input class="form-control" id="confirma_senha"  type="password">
                </div>                
              </div>
              <span class="msg-error text-danger"></span>
              <span class="msg-success text-success"></span>
              <!-- /.box-body -->
              <div class="box-footer">
                <span id="alterarSenha" class="btn btn-primary pull-right">Alterar Senha</span>
              </div>
              <!-- /.box-footer -->
            </form>
      </div> 
    </div>
  </div>
</div>


<script type="text/javascript">
  
var base_url = "<?php echo base_url(); ?>";

</script>

<script src="<?php echo base_url(); ?>/assets/bootstrapPDV/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/plugins/redactor/redactor.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/plugins/select2/select2.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/plugins/formvalidation/js/formValidation.popular.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/plugins/formvalidation/js/framework/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/dist/js/common-libs.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/dist/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/dist/js/app.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/dist/js/pages/all.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/dist/js/custom.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/plugins/input-mask/jquery.inputmask.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/plugins/input-mask/jquery.inputmask.date.extensions.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/plugins/velocity/velocity.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/plugins/velocity/velocity.ui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/dist/js/parse-track-data.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/dist/js/pos.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/dist/js/action.js"></script>
<script type="text/javascript">
	var base_url = '<?php echo base_url(); ?>', assets = '<?php echo base_url(); ?>/assets/';
	var dateformat = 'dd/mm/Yyyy', timeformat = 'h:i:s A';
		var Settings = {
			"logo":"logo11.png",
			"site_name":"<?php echo $emitente[0]->emitente_nome ?> - PDV",
			"tel":"89 99430-2106",
			"dateformat":"dd\/mm\/Yyyy",
			"timeformat":"h:i:s A",
			"language":"portugues",
			"theme":"default",
			"mmode":"0",
			"captcha":"0",
			"currency_prefix":"R$ ",
			"default_customer":"1",
			"default_tax_rate":"0%",
			"rows_per_page":"25",
			"total_rows":"30",
			"header":"<h2>CADPROJETOS - PDV<\/h2>Rua Afonso Marques, 441 - Centro,<br>\r\n  CEP 62.300-000 - Vi\u00e7osa - CE<br>","footer":"Volte Sempre!\r\n<br>",
			"bsty":"3",
			"display_kb":"0",
			"default_category":"1",
			"default_discount":"0",
			"item_addition":"1",
			"barcode_symbology":"",
			"pro_limit":"100",
			"decimals":"2",
			"thousands_sep":",",
			"decimals_sep":".",
			"focus_add_item":"F7",
			"add_customer":"ALT+F2",
			"toggle_category_slider":"ALT+F10",
			"cancel_sale":"F3",
			"suspend_sale":"F2",
			"print_order":"F6",
			"print_bill":"F4",
			"finalize_sale":"F1",
			"today_sale":"Ctrl+F1",
			"open_hold_bills":"F5",
			"close_register":"ALT+F7",
			"java_applet":"0",
			"receipt_printer":"",
			"pos_printers":"",
			"cash_drawer_codes":"",
			"char_per_line":"42",
			"rounding":"0",
			"pin_code":"81dc9bdb52d04dc20036dbd8313ed055",
			"purchase_code":"ff2400d9-f3aa-4db5-9dc5-4eee236c6254",
			"envato_username":"patriciomelo"
		};

	var sid = false, username = '<?php echo $this->session->userdata('usuario_nome') ?>', spositems = {};
	$(window).load(function () {
		$('#mm_pos').addClass('active');
		$('#pos_index').addClass('active');
	});
	var pro_limit = 99, java_applet = 0, count = 1, total = 0, an = 1, p_page = 0, page = 0, cat_id = 1, tcp = 2;
	var gtotal = 0, order_discount = 0, order_tax = 0, protect_delete = 0;
	var order_data = '', bill_data = '';
	var lang = new Array();
	lang['code_error'] = 'Código de Erro';
	lang['r_u_sure'] = '<strong>Tem certeza que deseja cancelar?</strong>';
	lang['please_add_product'] = 'Favor adicionar produto';
	lang['paid_less_than_amount'] = 'Valor pago é menor do que pagar';
	lang['x_suspend'] = 'Venda não pode ser suspensa';
	lang['discount_title'] = 'Desconto Descrição';
	lang['update'] = 'Atualizar';
	lang['tax_title'] = 'Taxa Descrição';
	lang['leave_alert'] = 'Você vai a perda de dados, você tem certeza?';
	lang['close'] = 'Fechar';
	lang['delete'] = 'Deletar';
	lang['no_match_found'] = 'Nenhum produto correspondente encontrado';
	lang['wrong_pin'] = 'Pin errado';
	lang['file_required_fields'] = 'Por favor, preencha os campos obrigatórios';
	lang['enter_pin_code'] = 'Digite o código Pin';
	lang['incorrect_gift_card'] = 'Número do cartão Loja é errado ou cartão já é usado.';
	lang['card_no'] = 'Cartão No';
	lang['value'] = 'Valor';
	lang['balance'] = 'Troco';
	lang['unexpected_value'] = 'Valor inesperado fornecido!';
	lang['inclusive'] = 'Incluso';
	lang['exclusive'] = 'Não incluso';
	lang['total'] = 'Total';
	lang['total_items'] = 'Total de Itens';
	lang['order_tax'] = 'Taxa';
	lang['order_discount'] = 'Desconto';
	lang['total_payable'] = 'Total a Pagar';
	lang['rounding'] = 'Arredondamento';
	lang['grand_total'] = 'Total Geral';

	$(document).ready(function() {
		posScreen();
		
		if(get('rmspos')) {
			if (get('spositems')) { remove('spositems'); }
			if (get('spos_discount')) { remove('spos_discount'); }
			if (get('spos_tax')) { remove('spos_tax'); }
			if (get('spos_note')) { remove('spos_note'); }
			if (get('spos_customer')) { remove('spos_customer'); }
			if (get('amount')) { remove('amount'); }
			remove('rmspos');
		}
					if(! get('spos_discount')) {
				store('spos_discount', '0');
				$('#discount_val').val('0');
			}
			if(! get('spos_tax')) {
				store('spos_tax', '0%');
				$('#tax_val').val('0%');
			}
		
		if (ots = get('spos_tax')) {
		    $('#tax_val').val(ots);
		}
		if (ods = get('spos_discount')) {
		    $('#discount_val').val(ods);
		}
		if(Settings.display_kb == 1) { display_keyboards(); }
		nav_pointer();
		loadItems();
		read_card();
		bootbox.addLocale('bl',{OK:'OK',CANCEL:'No',CONFIRM:'Sim'});
		bootbox.setDefaults({closeButton:false,locale:"bl"});
			});


	$(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
    }); 


</script>
</body>
</html>