 <table style="width:97%;" class="layout-table">
		 <tbody>
		 	<tr>
			   <td style="width: 460px;">
                  <div id="pos">
					  <form action="http://localhost/sistemapdv/pos" id="pos-sale-form" method="post" accept-charset="utf-8">
                           <input type="hidden" name="spos_token" value="f661c6c8f4ac92f2c469a3cc6bae7757" style="display:none;">
							   <div class="well well-sm" id="leftdiv">
									<div id="lefttop" style="margin-bottom:5px;">
										<div class="form-group" style="margin-bottom:5px;">
										  <div class="input-group">
							                  <select class="select2" style="width: 100%;" name="cliente_desc" id="cliente_desc">
							                    <option value="">Selecione um Cliente</option>                       
							                  </select>    
											  <div class="input-group-addon no-print" style="padding: 2px 5px;">
													<a href="#" class="external" data-toggle="modal" data-target="#modal-default"><i class="fa fa-2x fa-plus-circle" id="addIcon"></i></a>
											   </div>
										   </div>
												<div style="clear:both;"></div>
										</div>
									</div>
<!-- 										<div id="printhead" class="print">
											<h2>CADPROJETOS - PDV</h2>Rua Afonso Marques, 441 - Centro,<br>
                                                                                              CEP 62.300-000 - Viçosa - CE<br>											<p>Date: 2121/0808/2019191919</p>
										</div> -->
										<div id="print">
											<div id="list-table-div" class="ps-container" data-ps-id="7527ee8e-166f-59fd-0d9c-999f9ec21eae" style="height: 236px;">
												<table id="posTable" class="table table-striped table-condensed table-hover list-table" style="margin:0;">
													<thead>
														<tr class="success">
															<th>Produto</th>
															<th style="width: 15%;text-align:center;">Preço</th>
															<th style="width: 15%;text-align:center;">Qtd</th>
															<th style="width: 20%;text-align:center;">Subtotal</th>
															<th style="width: 20px;" class="satu"><i class="fa fa-trash-o"></i></th>
														</tr>
													</thead>
													<tbody><tr id="1566395241006" class="24 danger" data-item-id="24"><td><input name="product_id[]" type="hidden" class="rid" value="24"><button type="button" class="btn btn-block btn-xs edit btn-warning" id="1566395241006" data-item="24"><span class="sname" id="name_1566395241006">XEROX - COLORIDA (03)</span></button></td><td class="text-right"><input class="realuprice" name="real_unit_price[]" type="hidden" value="2.00"><input class="rdiscount" name="product_discount[]" type="hidden" id="discount_1566395241006" value="0"><span class="text-right sprice" id="sprice_1566395241006">2.00</span></td><td><input class="form-control input-qty kb-pad text-center rquantity" name="quantity[]" type="text" value="1" data-id="1566395241006" data-item="24" id="quantity_1566395241006" onclick="this.select();"></td><td class="text-right"><span class="text-right ssubtotal" id="subtotal_1566395241006">2.00</span></td><td class="text-center"><i class="fa fa-trash-o tip pointer posdel" id="1566395241006" title="Remove"></i></td></tr><tr id="1566395241001" class="23 danger" data-item-id="23"><td><input name="product_id[]" type="hidden" class="rid" value="23"><button type="button" class="btn btn-block btn-xs edit btn-warning" id="1566395241001" data-item="23"><span class="sname" id="name_1566395241001">IMPRESSÃO - P/B (02)</span></button></td><td class="text-right"><input class="realuprice" name="real_unit_price[]" type="hidden" value="0.50"><input class="rdiscount" name="product_discount[]" type="hidden" id="discount_1566395241001" value="0"><span class="text-right sprice" id="sprice_1566395241001">0.50</span></td><td><input class="form-control input-qty kb-pad text-center rquantity" name="quantity[]" type="text" value="3" data-id="1566395241001" data-item="23" id="quantity_1566395241001" onclick="this.select();"></td><td class="text-right"><span class="text-right ssubtotal" id="subtotal_1566395241001">1.50</span></td><td class="text-center"><i class="fa fa-trash-o tip pointer posdel" id="1566395241001" title="Remove"></i></td></tr></tbody>
												</table>
											<div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px; display: block;"><div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px; display: block;"><div class="ps-scrollbar-y" style="top: 0px; height: 0px;"></div></div></div>
											<div style="clear:both;"></div>
											<div id="totaldiv">
												<table id="totaltbl" class="table table-condensed totals" style="margin-bottom:10px;">
													<tbody>
														<tr class="info">
															<td width="25%">Total de Itens</td>
															<td class="text-right" style="padding-right:10px;"><span id="count">2 (4.00)</span></td>
															<td width="25%">Total</td>
															<td class="text-right" colspan="2"><span id="total">3.50</span></td>
														</tr>
														<tr class="info">
															<td width="25%"><a href="#" id="add_discount">Desconto</a></td>
															<td class="text-right" style="padding-right:10px;"><span id="ds_con">(0.00) 0.00</span></td>
															<td width="25%"><a href="#" id="add_tax">Taxa</a></td>
															<td class="text-right"><span id="ts_con">0.00</span></td>
														</tr>
														<tr class="success">
															<td colspan="2" style="font-weight:bold;">Total a Pagar</td>
															<td class="text-right" colspan="2" style="font-weight:bold;"><span id="total-payable">3.50</span></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div id="botbuttons" class="col-xs-12 text-center">
											<div class="row">
												<div class="col-xs-4" style="padding: 0;">
													<div class="btn-group-vertical btn-block">
														<button type="button" class="btn btn-warning btn-block btn-flat" id="suspend">Aguardar</button>
														<button type="button" class="btn btn-danger btn-block btn-flat" id="reset">Cancelar</button>
													</div>

												</div>
												<div class="col-xs-4" style="padding: 0 5px;">
													<div class="btn-group-vertical btn-block">
														<button type="button" class="btn bg-purple btn-block btn-flat" id="print_order">Ver Itens</button>

														<button type="button" class="btn bg-navy btn-block btn-flat" id="print_bill">Ver Fatura</button>
													</div>
												</div>
												<div class="col-xs-4" style="padding: 0;">
													<button type="button" class="btn btn-success btn-block btn-flat" id="payment" style="height:67px;">Pagamento</button>
												</div>
											</div>

										</div>
										<div class="clearfix"></div>
										<span id="hidesuspend"></span>
										<input type="hidden" name="spos_note" value="" id="spos_note">

										<div id="payment-con">
											<input type="hidden" name="amount" id="amount_val" value="">
											<input type="hidden" name="balance_amount" id="balance_val" value="">
											<input type="hidden" name="paid_by" id="paid_by_val" value="cash">
											<input type="hidden" name="cc_no" id="cc_no_val" value="">
											<input type="hidden" name="paying_gift_card_no" id="paying_gift_card_no_val" value="">
											<input type="hidden" name="cc_holder" id="cc_holder_val" value="">
											<input type="hidden" name="cheque_no" id="cheque_no_val" value="">
											<input type="hidden" name="cc_month" id="cc_month_val" value="">
											<input type="hidden" name="cc_year" id="cc_year_val" value="">
											<input type="hidden" name="cc_type" id="cc_type_val" value="">
											<input type="hidden" name="cc_cvv2" id="cc_cvv2_val" value="">
											<input type="hidden" name="balance" id="balance_val" value="">
											<input type="hidden" name="payment_note" id="payment_note_val" value="">
										</div>
										<input type="hidden" name="customer" id="customer" value="1">
										<input type="hidden" name="order_tax" id="tax_val" value="0%">
										<input type="hidden" name="order_discount" id="discount_val" value="0">
										<input type="hidden" name="count" id="total_item" value="">
										<input type="hidden" name="did" id="is_delete" value="0">
										<input type="hidden" name="eid" id="is_delete" value="0">
										<input type="hidden" name="hold_ref" id="hold_ref" value="">
										<input type="hidden" name="total_items" id="total_items" value="0">
										<input type="hidden" name="total_quantity" id="total_quantity" value="0">
										<input type="submit" id="submit" value="Submit Sale" style="display: none;">
									</div>
									</form>								</div>

							</td>
							<td>
								<div class="contents" id="right-col" style="height: 517px;">
									<div id="item-list">
										<div class="items ps-container" data-ps-id="e92015b7-4f64-9e5b-28d8-21b64b760c85" style="height: 467px;">
											<div><button type="button" data-name="IMPRESSÃO - P/B" id="product-0123" value="02" class="btn btn-both btn-flat product"><span class="bg-img"><img src="http://localhost/sistemapdv/uploads/thumbs/99ba15cb71eafc2bf6675c47eda93853.gif" alt="IMPRESSÃO - P/B" style="width: 100px; height: 100px;"></span><span><span>IMPRESSÃO - P/B</span></span></button><button type="button" data-name="XEROX - COLORIDA" id="product-0124" value="03" class="btn btn-both btn-flat product"><span class="bg-img"><img src="http://localhost/sistemapdv/uploads/thumbs/1c485ac2a3a40d5065a8ff8736311008.gif" alt="XEROX - COLORIDA" style="width: 100px; height: 100px;"></span><span><span>XEROX - COLORIDA</span></span></button></div>										<div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px; display: block;"><div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px; display: block;"><div class="ps-scrollbar-y" style="top: 0px; height: 0px;"></div></div></div>
									</div>
									<div class="product-nav">
										<div class="btn-group btn-group-justified">
											<div class="btn-group">
												<button style="z-index:10002;" class="btn btn-warning pos-tip btn-flat" type="button" id="previous" disabled="disabled"><i class="fa fa-chevron-left"></i></button>
											</div>
											<div class="btn-group">
												<button style="z-index:10003;" class="btn btn-success pos-tip btn-flat" type="button" id="sellGiftCard"><i class="fa fa-credit-card" id="addIcon"></i> Vender Cartão da Loja</button>
											</div>
											<div class="btn-group">
												<button style="z-index:10004;" class="btn btn-warning pos-tip btn-flat" type="button" id="next" disabled="disabled"><i class="fa fa-chevron-right"></i></button>
											</div>
										</div>
									</div>
								</div>
							</td>
						</tr>
					</tbody></table>
	




<!-- Modal Cliente -->
<div class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <form action="<?php echo base_url() ?>index.php/produtos/excluir" method="post" >
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h5 id="myModalLabel">Excluir Produto</h5>
  </div>
  <div class="modal-body">
    <input type="hidden" id="idProduto" name="id" value="" />
    <h5 style="text-align: center">Deseja realmente excluir este produto?</h5>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
    <button class="btn btn-danger">Excluir</button>
  </div>
  </form>
</div>

  <div class="modal fade" id="modal-default">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Adicionar Cliente </h4>
        </div>
        <div class="modal-body">
				<div id="c-alert" class="alert alert-danger" style="display:none;"></div>
				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<label class="control-label" for="cliente_nome">Nome</label>
							<input type="text" name="cliente_nome" id="cliente_nome" value="" class="form-control input-sm kb-text" >
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-6">
						<div class="form-group">
							<label class="control-label" for="cliente_email">Endereço de e-mail</label>
							<input type="text" name="cliente_email" id="cliente_email" value="" class="form-control input-sm kb-text" >
						</div>
					</div>
					<div class="col-xs-6">
						<div class="form-group">
							<label class="control-label" for="cliente_numero">Telefone</label>
							<input type="text" name="cliente_numero" id="cliente_numero" value="" class="form-control input-sm kb-pad" >
						</div>
					</div>
				</div>
				<div class="row">
				    <div class="col-xs-6">
						<div class="form-group">
							<label class="control-label" for="tipo">Tipo</label>
						 <select required class="form-control input-sm kb-pad" name="cliente_tipo" id="tipo">
				                <option value='1'>Pessoa Física</option>
				                <option value='2'>Pessoa Jurídica</option>
				            </select>
						</div>
					</div>
					<div class="col-xs-6">
						<div class="form-group">
							<label class="control-label" for="cpfCnpj">CPF/CNPJ</label>
							<input type="text" name="cpfCnpj" id="cpfCnpj"value="" class="form-control input-sm kb-text" >
						</div>
					</div>

				</div>

			</div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Fechar</button>
          <button type="button" class="btn btn-success" id='addCliente' data-dismiss="modal">Adicionar</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->



