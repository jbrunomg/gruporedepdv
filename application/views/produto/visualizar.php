<section class="content">
  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>
        </div>
        <!-- /.box-header -->
        
        <div class="col-md-12">
          <!-- Custom Tabs (Pulled to the right) -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs pull-right">
              <li><a href="#tab_1" data-toggle="tab">Compras</a></li>
              <li class="active"><a href="#tab_2" data-toggle="tab">vendas</a></li>
              <li><a href="#tab_3" data-toggle="tab">Dados</a></li>      
              <li class="pull-left header"><i class="fa fa-th"></i> Historico produto</li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane" id="tab_1">
                <table id="example" class="table" width="100%">
                  <thead>
                    <tr> 
                      <th>Movimentação</th>                                                           
                      <th>Quatidade</th>
                      <th>Preço Custo</th>
                      <th>Data Compra</th>
                      <th>Ações</th>                            
                    </tr>
                  </thead>
                  <tbody>

                    <?php foreach ($movimentacao as $d){ ?>
                    <tr>                      
                      <td> <?php echo $d->item_movimentacao_produto_movimentacao_produto_id;?></td> 
                      <td> <?php echo $d->item_movimentacao_produto_quantidade;?></td> 
                      <td> <?php echo $d->item_movimentacao_produto_custo;?></td> 
                       <td> <?php echo $d->item_movimentacao_produto_atualizacao;?></td>              
                      <td>                
                      <?php if(verificarPermissao('vAlmoxarifado')){ ?>
                        <a href="<?php echo base_url(); ?>movimentacao/visualizar/<?php echo $d->item_movimentacao_produto_movimentacao_produto_id; ?>" data-toggle="tooltip" title="Visualizar"><i class="fa fa-search text-success"></i> </a>
                        <?php } ?>

                      </td>
                    </tr>
                    <?php } ?>

                  </tbody>
                  <tfoot>
                    <tr>                     
                      <th>Movimentação</th>                                                           
                      <th>Quatidade</th>
                      <th>Preço Custo</th>
                      <th>Data Compra</th>
                      <th>Ações</th> 
                    </tr>
                  </tfoot>
                </table>
                
              </div>


              <!-- /.tab-pane -->
              <div class="tab-pane active" id="tab_2">
                <b>Quantidade q o produto foi vendido no mês.</b>

                <div class="row">

                <div class="col-md-6">
                  <!-- LINE CHART -->
                  <div class="box box-info">
                    <div class="box-header with-border">
                      <h3 class="box-title">MÊS ATUAL VS MÊS PASSADO</h3>

                      <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                      </div>
                    </div>
                    <div class="box-body chart-responsive">
                      <div class="chart" id="line-chart" style="height: 300px;"></div>
                    </div>
                    <!-- /.box-body -->
                  </div>
                  <!-- /.box -->

                </div>

                <div class="col-md-6">
                  <!-- LINE CHART -->
                  <div class="box box-info">
                    <div class="box-header with-border">
                      <h3 class="box-title">ANO ATUAL VS ANO PASSADO</h3>

                      <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                      </div>
                    </div>
                    <div class="box-body chart-responsive">
                      <div class="chart" id="bar-chart" style="height: 300px;"></div>
                    </div>
                    <!-- /.box-body -->
                  </div>
                  <!-- /.box -->

                </div>



                </div>

              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane " id="tab_3">
                  <div class="row">
                        <div class="col-md-6">
                          <label for="produto_categoria_id">Grupo Produto</label>                        
                          <select disabled required class="form-control" name="produto_categoria_id" id="tipo">
                            <?php foreach ($grupo as $g) { ?>
                              <?php $selected = ($g->categoria_prod_id == $dados[0]->produto_categoria_id)?'SELECTED':''; ?>
                              <option value='<?php echo $g->categoria_prod_id; ?>' <?php echo $selected; ?>><?php echo $g->categoria_prod_descricao; ?> </option>
                            <?php } ?>
                          </select>
                        </div>

                        <div class="col-md-6 form-group">
                          <label for="produto_codigo">Código</label>
                         <input disabled type="text" class="form-control" name="produto_codigo" id="produto_codigo" value="<?php echo $dados[0]->produto_codigo; ?>" placeholder="código">

                        </div>

                        <div class="col-md-6">
                          <label for="produto_descricao">Descrição</label>
                          <input disabled type="text" class="form-control" name="produto_descricao" id="produto_descricao"  value="<?php echo $dados[0]->produto_descricao; ?>" placeholder="CPF/CNPJ">
                           
                        </div>

                        <div class="col-md-6 form-group">
                          <label for="descricao">Unidade</label>
                          <select disabled class="form-control" name="produto_unidade" id="produto_unidade">
                            <option value="">Selecione</option>
                            <option value="UND"  <?php echo ($dados[0]->produto_unidade == 'UND')?'SELECTED':''; ?>>UND</option>
                            <option value="PCT"  <?php echo ($dados[0]->produto_unidade == 'PCT')?'SELECTED':''; ?>>PCT</option>
                            <option value="KG"   <?php echo ($dados[0]->produto_unidade == 'KG')?'SELECTED':''; ?>>KG</option> 
                            <option value="METRO" <?php echo ($dados[0]->produto_unidade == 'METRO')?'SELECTED':''; ?>>METRO</option>  
                          </select>
                        </div>
                         
                        <div class="col-md-6">
                          <label for="produto_codigo_barra">Cód Barra</label>
                          <input disabled type="text" class="form-control" id="produto_codigo_barra" name="produto_codigo_barra"  value="<?php echo $dados[0]->produto_codigo_barra; ?>" placeholder="Nome Completo">
                        </div>

                        <div class="col-md-6 form-group">
                          <label for="produto_gaveta">Gaveta</label>
                          <input disabled type="text" class="form-control" id="produto_gaveta" name="produto_gaveta"  value="<?php echo $dados[0]->produto_gaveta; ?>" placeholder="Bairro">
                        </div>

                        <div class="col-md-6">
                          <label for="produto_preco_dolar">Preço Dolar</label>
                          <input disabled type="text" class="form-control money" name="produto_preco_dolar" id="produto_preco_dolar" value="<?php echo $dados[0]->produto_preco_dolar; ?>" placeholder="produto_preco_dolar">
                        </div>

                        <div class="col-md-6 form-group">
                          <label for="descricao">Preço Custo</label>
                            <input disabled class="form-control money" name="produto_preco_custo" id="produto_preco_custo" value="<?php echo $dados[0]->produto_preco_custo; ?>" placeholder="preço custo">    
                        </div>

                        <div class="col-md-6">
                          <label for="produto_preco_minimo_venda">Preço Minimo Venda</label>
                            <input disabled type="text" class="form-control money" name="produto_preco_minimo_venda" id="produto_preco_minimo_venda" value="<?php echo $dados[0]->produto_preco_minimo_venda; ?>" placeholder="preço minimo venda">                            
                        </div>

                          <div class="col-md-6  form-group">
                            <label for="descricao">Preço Venda</label>
                             <input disabled class="form-control money" name="produto_preco_venda" id="produto_preco_venda" value="<?php echo $dados[0]->produto_preco_venda; ?>" placeholder="preço venda">
                          </div>

                          <div class="col-md-6">
                            <label for="descricao">Preço Cartão-débito</label>
                              <input disabled class="form-control money" name="produto_preco_cart_debito" id="produto_preco_cart_debito" value="<?php echo $dados[0]->produto_preco_cart_debito; ?>" placeholder="preço cartão débito">
                          </div>   

                          <div class="col-md-6  form-group">
                            <label for="descricao">Preço Cartão-crédito</label>
                            <input disabled type="text" class="form-control money" name="produto_preco_cart_credito" id="produto_preco_cart_credito" value="<?php echo $dados[0]->produto_preco_cart_credito; ?>" placeholder="preço cartão crédito">
                          </div>                         

                          <div class="col-md-6">
                            <label for="cliente_limite_cred_cliente">Estoque Minimo</label>
                            <input disabled type="text" class="form-control" name="produto_estoque_minimo" id="produto_estoque_minimo" value="<?php echo $dados[0]->produto_estoque_minimo; ?>" placeholder="estoque minimo">
                          </div>
         

                    </div>
             

              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->



        <!-- /.box-body -->
        <div class="box-footer">
          <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>" class="btn btn-default">Voltar</a>          
        </div>
        <!-- /.box-footer -->        
      </div>
    </div>
  </div>
</section>



<!-- jQuery 3 -->
<script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Morris.js charts -->
<script src="<?php echo base_url(); ?>assets/bower_components/raphael/raphael.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/morris.js/morris.min.js"></script>



<script type="text/javascript">

  $(function () {
    "use strict";

    // LINE CHART
    var line = new Morris.Line({
      element: 'line-chart',
      resize: true,
      data: [

      <?php foreach ($graficoMes as $gm ) { ?>       

        {y: <?php echo "'".$gm->venda."'"; ?>, item1: <?php echo $gm->atual; ?>, item2: <?php echo $gm->passado; ?>},
     
      <?php } ?> 

        // {y: '2020-04-01', item1: 10, item2: 15},
        // {y: '2020-04-02', item1: 18, item2: 20},
        // {y: '2020-04-03', item1: 15, item2: 10},
        // {y: '2020-04-04', item1: 3767, item2: 3597},
        // {y: '2020-04-05', item1: 6810, item2: 1914},
        // {y: '2020-04-06', item1: 5670, item2: 4293},
        // {y: '2020-04-07', item1: 4820, item2: 3795},
        // {y: '2020-04-08', item1: 15073, item2: 5967},
        // {y: '2020-04-09', item1: 10687, item2: 0},
        // {y: '2020-04-10', item1: 8432, item2: 5713},

      ],
      xkey: 'y',
      ykeys: ['item1', 'item2'],
      labels: ['Atual', 'Passado'],
      lineColors: ['#a0d0e0', '#3c8dbc'],
      hideHover: 'auto'
    });


    //BAR CHART
    var bar = new Morris.Bar({
      element: 'bar-chart',
      resize: true,
      data: [

      <?php foreach ($graficoAno as $valor ) { ?>
      
        {y: <?php echo "'".$valor->Ano."'"; ?>, a: <?php echo $valor->atual; ?>, b: <?php echo $valor->passado; ?>},

      <?php } ?>  


        // {y: 'Fev', a: 75, b: 65},
        // {y: 'Mar', a: 50, b: 40},
        // {y: 'Abr', a: 75, b: 65},
        // {y: 'Mai', a: 50, b: 40},
        // {y: 'Jun', a: 75, b: 65},
        // {y: 'Jul', a: 100, b: 90},
        // {y: 'Ago', a: 100, b: 90},
        // {y: 'Set', a: 100, b: 90},
        // {y: 'Out', a: 100, b: 90},
        // {y: 'Nov', a: 100, b: 90},
        // {y: 'Dez', a: 100, b: 90}
      ],
      barColors: ['#00a65a', '#f56954'],
      xkey: 'y',
      ykeys: ['a', 'b'],
      labels: ['Atual', 'Passado'],
      hideHover: 'auto'
    });


  });

</script>