<section class="content">
  <div class="row">
    <div class="col-md-12">
      <!-- Horizontal Form -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="<?php echo current_url(); ?>" method="post" enctype="multipart/form-data">
          <div class="box-body">

            <div class="form-group">
              <label for="loja" class="col-sm-2 control-label">Lojas </label>
              <div class="col-sm-5">
                <select id="lojas" class="form-control select2" multiple="multiple" data-placeholder="Selecione as lojas" name="lojas[]" disabled="disabled">
                  <?php
                  foreach ($lojas as $s) {
                    echo "<option value='" . strtoupper($s['lojas']) . "' selected>" . strtoupper($s['lojas']) . "</option>";
                  }
                  ?>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="produto_categoria_id" class="col-sm-2 control-label">Grupo Produto</label>
              <div class="col-sm-5">
              <select id="grupo_produto" class="form-control" data-placeholder="Selecione o grupo de produtos" name="produto_categoria_id" required="required">
                <option value="">Selecione</option>
                  <?php foreach ($grupo as $g) { ?>
                <option value='<?php echo $g->categoria_prod_id; ?>'><?php echo $g->categoria_prod_descricao; ?> </option>
                  <?php } ?>
              </select>
              </div>
            </div>

            <div class="input-group margin" id="margem_cat">
              <input type="hidden" id="margem_categoria" class="form-control" value="">
            </div>

            <input type="hidden" id="cartao_debito" class="form-control" value="<?php echo $parametros[0]->parametro_cart_debito ?>">
            <input type="hidden" id="cartao_credto" class="form-control" value="<?php echo $parametros[0]->parametro_cart_credito ?>">

            <div class="form-group">
              <label for="codigo_produto" class="col-sm-2 control-label">Código </label>
              <div class="col-sm-5">
                        <!-- é aqui que vamos trabalhar -->    
                 <!-- Campo hidden para armazenar o código do produto -->
                <input type="hidden" name="codigo_produto" id="codigo_produto_real">
                <select id="codigo_produto" class="form-control select2" data-placeholder="Selecione o código" required="required">
                  <option value="">Selecione um grupo</option>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="produto_descricao" class="col-sm-2 control-label">Descrição </label>
              <div class="col-sm-5">
                <input type="text" class="form-control" name="produto_descricao" value="<?php echo set_value('produto_descricao'); ?>" placeholder="produto descricao">
              </div>
            </div>

            <div class="form-group">
              <label for="produto_unidade" class="col-sm-2 control-label">Unidade </label>
              <div class="col-sm-5">
                <select class="form-control" name="produto_unidade" id="produto_unidade">
                  <option value="">Selecione</option>
                  <option value="UND">UND</option>
                  <option value="PCT">PCT</option>
                  <option value="KG">KG</option>
                  <option value="MT">MT</option>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="produto_codigo_barra" class="col-sm-2 control-label">Cód Barra </label>
              <div class="col-sm-5">
                <input type="text" class="form-control" name="produto_codigo_barra" id="produto_codigo_barra" value="<?php echo set_value('produto_codigo_barra'); ?>" placeholder="Cód barra">
              </div>
            </div>

            <div class="form-group">
              <label for="produto_gaveta" class="col-sm-2 control-label">Gaveta </label>
              <div class="col-sm-5">
                <input type="text" class="form-control" name="produto_gaveta" id="produto_gaveta" value="<?php echo set_value('produto_gaveta'); ?>" placeholder="gaveta">
                <input type="color" class="form-control" name="produto_gaveta_cor" id="produto_gaveta_cor" value="" placeholder="gaveta cor">
              </div>
            </div>

            <div class="form-group">
              <label for="produto_estoque_minimo" class="col-sm-2 control-label">Alerta estoque Minimo </label>
              <div class="col-sm-5">
                <input type="text" class="form-control" name="produto_estoque_minimo" id="produto_estoque_minimo" value="<?php echo set_value('produto_estoque_minimo'); ?>" placeholder="estoque minimo">
              </div>
            </div>

            <div class="form-group">
              <label for="produto_preco_dolar" class="col-sm-2 control-label">Preço Dolar </label>
              <div class="col-sm-5">
                <input type="text" class="form-control money" name="produto_preco_dolar" id="produto_preco_dolar" value="<?php echo set_value('produto_preco_dolar'); ?>" placeholder="preço dolar">
              </div>
            </div>

            <div class="form-group">
              <label for="produto_preco_custo" class="col-sm-2 control-label">Preço Custo </label>
              <div class="col-sm-5">
                <input type="text" class="form-control money" name="produto_preco_custo" id="produto_preco_custo" value="<?php echo set_value('produto_preco_custo'); ?>" placeholder="preço custo">
              </div>
            </div>

            <div class="form-group">
              <label for="produto_preco_minimo_venda" class="col-sm-2 control-label">Preço Minimo Venda</label>
              <div class="col-sm-5">
                <input type="text" class="form-control money" name="produto_preco_minimo_venda" id="produto_preco_minimo_venda" value="<?php echo set_value('produto_preco_minimo_venda'); ?>" placeholder="preço minimo venda">
              </div>
            </div>

            <div class="form-group">
              <label for="produto_preco_venda" class="col-sm-2 control-label">Preço Venda </label>
              <div class="col-sm-5">
                <input type="text" class="form-control money" name="produto_preco_venda" id="produto_preco_venda" value="<?php echo set_value('produto_preco_venda'); ?>" placeholder="preço venda">
              </div>
            </div>

            <div class="form-group">
              <label for="produto_preco_cart_debito" class="col-sm-2 control-label">Preço Cartão-débito</label>
              <div class="col-sm-5">
                <input type="text" class="form-control money" name="produto_preco_cart_debito" id="produto_preco_cart_debito" value="<?php echo set_value('produto_preco_cart_debito'); ?>" placeholder="preço cartão débito">
              </div>
            </div>

            <div class="form-group">
              <label for="produto_preco_cart_credito" class="col-sm-2 control-label">Preço Cartão-crédito</label>
              <div class="col-sm-5">
                <input type="text" class="form-control money" name="produto_preco_cart_credito" id="produto_preco_cart_credito" value="<?php echo set_value('produto_preco_cart_credito'); ?>" placeholder="preço cartão crédito">
              </div>
            </div>

            <!-- <div class="form-group">
              <label for="produto_imposto_id" class="col-sm-2 control-label">Impostos </label>
              <div class="col-sm-5">
                <select class="form-control" name="produto_imposto_id" id="produto_imposto_id">
                  <option value="">Selecione</option>
                  <?php foreach ($impostos as $i) : ?>
                    <option value='<?= $i->imposto_id; ?>'><?= $i->imposto_descricao; ?> </option>
                  <?php endforeach ?>
                </select>
              </div>
            </div> -->

            <div class="form-group">
              <label for="produto_imagem" class="col-sm-2 control-label">Imagem </label>
              <div class="controls">
                <input id="arquivo" type="file" name="userfile" /> (png|jpg|jpeg)
              </div>
            </div>

          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>" class="btn btn-default">Voltar</a>
            <button type="submit" class="btn btn-primary pull-right"><?php echo ucfirst($this->uri->segment(2)); ?></button>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
  </div>
</section>

<?php

// print("<pre>");
// print_r($grupo);
// print("<pre>");
?>

<script type="text/javascript">
  $(function() {


    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    })

  })

  function resetarSelectCodigos() {
		$("#codigo_produto").html('<option value="">Selecione um grupo</option>');
	}

  $(document).ready(function () {
        function preencherSelectCodigos() {
            var grupo = $("#grupo_produto").val();

            if (grupo) {
                $.ajax({
                    method: "POST",
                    url: "<?php echo base_url(); ?>produto/preencherSelectCodigos",
                    dataType: "json",
                    data: { grupo: grupo }
                }).done(function(response) {
                    var options = '<option value="">Selecione</option>';
                    response.forEach(function(item) {
                        options += '<option value="' + item.codigo_prod_codigo + '">' + item.codigo_prod_codigo + ' - ' + item.codigo_prod_descricao + '</option>';
                    });
                    $("#codigo_produto").html(options);
                });
            } else {
                resetarSelectCodigos();
            }
        }

        $("#grupo_produto").change(function () {
            preencherSelectCodigos();
        });

        $("#codigo_produto").change(function () {
            var selectedOption = $(this).find('option:selected').val();
            $("#codigo_produto_real").val(selectedOption);
        });
    });
</script>