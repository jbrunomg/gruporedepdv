<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <div class="col-md-2">
        <?php if(verificarPermissao('aAlmoxarifado')){ ?>
        <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/adicionar" type="button" class="btn btn-block btn-default btn-flat">Adicionar</a>
        <?php } ?>
      </div>          
    </div>
    <div class="box-body">
      <div class="container-fluid">
        <table id="tableProduto" class="table table-bordered table-striped" width="100%">
          <thead>
            <tr>                                            
              <th>Código/Categoria</th>             
              <th>Cód Barra</th>
              <th>Descrição</th>
              <th>Estoque</th>              
              <th>Ações</th>
            </tr>
          </thead>
          <tbody>

          </tbody>
          <tfoot>
            <tr>                     
              <th>Código/Categoria</th>
              <th>Cód Barra</th>
              <th>Descrição</th>
              <th>Estoque</th>              
              <th>Ações</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</section>

 <!--  MODAL  -->
 <div class="modal fade" id="modalSenhaEstoque" data-backdrop="static" data-keyboard="false" style="z-index: 99999" tabindex="-1" role="dialog">
    <div class="modal-dialog" style="width: 350px; margin: 100px auto">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" onclick="closeModalEstoque()" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title text-center">Informar senha</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <p>Informe a senha do gerente para liberação desta ação</p>
              <div class="form-group">
                <input type="password" onkeyup="habilitarBTN(this.value)" id="input-liberar-senha" class="form-control" placeholder="Senha do gerente" />
              </div>
              <button id="btn-confirm" disabled class="btn btn-primary btn-block">CONFIRMAR</button>
            </div>
          </div>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>

   <!--  MODAL  -->
 <div class="modal fade" id="modalEstoque" data-backdrop="static" data-keyboard="false" style="z-index: 99999" tabindex="-1" role="dialog">
    <div class="modal-dialog" style="width: 350px; margin: 100px auto">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" onclick="closeModalEstoque()" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title text-center">Informar Valor Estoque</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <p>Informe a Nova Quantidade do Estoque do Produto</p>
              <div class="form-group">
                <input type="text"  id="descricaoProdutoModal" class="form-control" placeholder=""  disabled="disabled"/>
                <input type="number" onkeyup="habilitarBTNEstoque(this.value)" id="input-liberar-valor" class="form-control" />
                <input type="hidden" id="idProdutoModal"/>
                <input type="hidden" id="idCategoriaModal"/>
                <input type="hidden" id="estoqueProdutoModal"/>
              </div>
              <button id="btn-confirm-estoque" disabled class="btn btn-primary btn-block">CONFIRMAR</button>
            </div>
          </div>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>


<script>

  function habilitarBTN(valor) {
    const el = document.querySelector('#btn-confirm')
    if(valor.length > 0) {
      el.disabled = false
    } else {
        el.disabled = true
    }
  }

  function habilitarBTNEstoque(valor) {
    const el = document.querySelector('#btn-confirm-estoque')
    if(valor.length > 0) {
      el.disabled = false
    } else {
        el.disabled = true
    }
  }

  function idProdutoEstoqueModal(idProduto, idCategoria, descricao, estoque) {
    $('#idProdutoModal').val(idProduto);
    $('#idCategoriaModal').val(idCategoria);
    $('#descricaoProdutoModal').val(descricao);
    $('#estoqueProdutoModal').val(estoque);
  }

  function closeModalEstoque() {
      $('#modalSenhaEstoque').modal('hide');
      $('#modalEstoque').modal('hide');
      document.querySelector('#input-liberar-senha').value = null;
      document.querySelector('#input-liberar-valor').value = null;
      document.querySelector('#idProdutoModal').value = null;
      document.querySelector('#idCategoriaModal').value = null;
      document.querySelector('#descricaoProdutoModal').value = null;
      document.querySelector('#estoqueProdutoModal').value = null;
      const el = document.querySelector('#btn-confirm')
      el.disabled = true
  }

  $('#btn-confirm').click(function(event) {

      let senha = $('#input-liberar-senha').val();
      let id = $('#idProdutoModal').val();

      $.ajax({
        method: "POST",
        url: base_url+"pdv2/validacaoGerente/",
        dataType: "JSON",
        data: { senha },
        success: function(data)
        {
          if(data.result == true){
            $('#modalEstoque').modal('show');    
          }else{
            alert('Senha Invalida.');
            closeModalEstoque();
          }
        }
        
      });
  });
  
  $('#btn-confirm-estoque').click(function(event) {

    let id = $('#idProdutoModal').val();
    let categoria = $('#idCategoriaModal').val();
    let descricao = $('#descricaoProdutoModal').val();
    let estoque = $('#estoqueProdutoModal').val();
    let quantidade =  $('#input-liberar-valor').val();
    
    $.ajax({
      method: "POST",
      url: base_url+"produto/alterarEstoque/",
      dataType: "JSON",
      data: { id: id, categoria: categoria, descricao: descricao, estoque: estoque, quantidade: quantidade },
      success: function(data)
      {
        if(data.result == true){
          alert('Dados alterados com sucesso!');
          location.reload();
        }else{
          alert('Algo deu errado. Tente Novamente!');
          location.reload();
        }
      }
      
    });
}); 

</script>