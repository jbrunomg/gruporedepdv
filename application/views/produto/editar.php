<section class="content">
  <div class="row">
    <div class="col-md-12">
      <!-- Horizontal Form -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>
        </div>
        <!-- <?= var_dump($dados) ?> -->
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="<?php echo current_url(); ?>" method="post" enctype="multipart/form-data">
          <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $dados[0]->produto_id; ?>" placeholder="id">
          <?php echo form_hidden('produto_imagem', $dados[0]->produto_imagem) ?>
          <div class="box-body">

            <div class="form-group">
              <label for="loja" class="col-sm-2 control-label">Lojas </label>
              <div class="col-sm-5">
                <select id="lojas" class="form-control select2" multiple="multiple" data-placeholder="Selecione as lojas" name="lojas[]" disabled="disabled">
                  <?php
                  foreach ($lojas as $s) {
                    echo "<option value='" . strtoupper($s['lojas']) . "' selected>" . strtoupper($s['lojas']) . "</option>";
                  }
                  ?>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="produto_categoria_id" class="col-sm-2 control-label">Grupo Produto</label>
              <div class="col-sm-5">
                <select class="form-control" name="produto_categoria_id" id="produto_categoria_id">
                  <?php foreach ($grupo as $g) { ?>
                    <?php $selected = ($g->categoria_prod_id == $dados[0]->produto_categoria_id) ? 'SELECTED' : ''; ?>
                    <option value='<?php echo $g->categoria_prod_id; ?>' <?php echo $selected; ?>><?php echo $g->categoria_prod_descricao; ?> </option>
                  <?php } ?>
                </select>
              </div>
            </div>

            <div class="input-group margin" id="margem_cat">
              <input type="hidden" id="margem_categoria" class="form-control" value="<?php echo $margem[0]->categoria_prod_margem ?>">
            </div>

            <input type="hidden" id="cartao_debito" class="form-control" value="<?php echo $parametros[0]->parametro_cart_debito ?>">
            <input type="hidden" id="cartao_credto" class="form-control" value="<?php echo $parametros[0]->parametro_cart_credito ?>">

            <div class="form-group">
              <label for="codigo_produto" class="col-sm-2 control-label">Código </label>
              <div class="col-sm-5">    
                 <!-- Campo hidden para armazenar o código do produto -->
                <input type="hidden" name="codigo_produto" id="codigo_produto_real">
                <select id="codigo_produto" name="codigo_produto" class="form-control select2" data-placeholder="Selecione o código" >
                  <option value="">Selecione</option>
                  <?php foreach ($codigos as $codigo) { ?>
                      <option value="<?php echo $codigo->codigo_prod_codigo; ?>"
                          <?php echo ($dados[0]->produto_codigo == $codigo->codigo_prod_codigo) ? 'selected' : ''; ?>>
                          <?php echo $codigo->codigo_prod_codigo;?> - <?php echo $codigo->codigo_prod_descricao; ?>
                      </option>
                  <?php } ?>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="produto_descricao" class="col-sm-2 control-label">Descrição </label>
              <div class="col-sm-5">
                <input type="text" class="form-control" name="produto_descricao" value="<?php echo $dados[0]->produto_descricao; ?>" placeholder="Produto descrição">
              </div>
            </div>

            <div class="form-group">
              <label for="produto_unidade" class="col-sm-2 control-label">Unidade </label>
              <div class="col-sm-5">
                <select class="form-control" name="produto_unidade" id="produto_unidade">
                  <option value="">Selecione</option>
                  <option value="UND" <?php echo ($dados[0]->produto_unidade == 'UND') ? 'SELECTED' : ''; ?>>UND</option>
                  <option value="PCT" <?php echo ($dados[0]->produto_unidade == 'PCT') ? 'SELECTED' : ''; ?>>PCT</option>
                  <option value="KG" <?php echo ($dados[0]->produto_unidade == 'KG') ? 'SELECTED' : ''; ?>>KG</option>
                  <option value="METRO" <?php echo ($dados[0]->produto_unidade == 'METRO') ? 'SELECTED' : ''; ?>>METRO</option>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="produto_codigo_barra" class="col-sm-2 control-label">Cód Barra </label>
              <div class="col-sm-5">
                <input type="text" class="form-control" name="produto_codigo_barra" id="produto_codigo_barra" value="<?php echo $dados[0]->produto_codigo_barra; ?>" placeholder="Cód barra">
              </div>
            </div>

            <div class="form-group">
              <label for="produto_gaveta" class="col-sm-2 control-label">Gaveta </label>
              <div class="col-sm-5">
                <input type="text" class="form-control" name="produto_gaveta" id="produto_gaveta" value="<?php echo $dados[0]->produto_gaveta; ?>" placeholder="Gaveta">
                <input type="color" class="form-control" name="produto_gaveta_cor" id="produto_gaveta_cor" value="<?php echo $dados[0]->produto_gaveta_cor; ?>" placeholder="Gaveta cor">
              </div>
            </div>

            <div class="form-group">
              <label for="produto_preco_dolar" class="col-sm-2 control-label">Preço Dolar </label>
              <div class="col-sm-5">
                <input type="text" class="form-control money" name="produto_preco_dolar" id="produto_preco_dolar" value="<?php echo $dados[0]->produto_preco_dolar; ?>" placeholder="Preço dolar">
              </div>
            </div>

            <div class="form-group">
              <label for="produto_preco_custo" class="col-sm-2 control-label">Preço Custo </label>
              <div class="col-sm-5">
                <input type="text" class="form-control money" name="produto_preco_custo" id="produto_preco_custo" value="<?php echo $dados[0]->produto_preco_custo; ?>" placeholder="Preço custo">
              </div>
            </div>

            <div class="form-group">
              <label for="produto_preco_minimo_venda" class="col-sm-2 control-label">Preço Minimo Venda</label>
              <div class="col-sm-5">
                <input type="text" class="form-control money" name="produto_preco_minimo_venda" id="produto_preco_minimo_venda" value="<?php echo $dados[0]->produto_preco_minimo_venda; ?>" placeholder="Preço mínimo venda">
              </div>
            </div>

            <div class="form-group">
              <label for="produto_preco_venda" class="col-sm-2 control-label">Preço Venda </label>
              <div class="col-sm-5">
                <input type="text" class="form-control money" name="produto_preco_venda" id="produto_preco_venda" value="<?php echo $dados[0]->produto_preco_venda; ?>" placeholder="Preço venda">
              </div>
            </div>

            <div class="form-group">
              <label for="produto_preco_cart_debito" class="col-sm-2 control-label">Preço Cartão-débito</label>
              <div class="col-sm-5">
                <input type="text" class="form-control money" name="produto_preco_cart_debito" id="produto_preco_cart_debito" value="<?php echo $dados[0]->produto_preco_cart_debito; ?>" placeholder="Preço cartão débito">
              </div>
            </div>

            <div class="form-group">
              <label for="produto_preco_cart_credito" class="col-sm-2 control-label">Preço Cartão-crédito</label>
              <div class="col-sm-5">
                <input type="text" class="form-control money" name="produto_preco_cart_credito" id="produto_preco_cart_credito" value="<?php echo $dados[0]->produto_preco_cart_credito; ?>" placeholder="Preço cartão crédito">
              </div>
            </div>

            <div class="form-group">
              <label for="produto_estoque_minimo" class="col-sm-2 control-label">Estoque Minimo </label>
              <div class="col-sm-5">
                <input type="text" class="form-control" name="produto_estoque_minimo" id="produto_estoque_minimo" value="<?php echo $dados[0]->produto_estoque_minimo; ?>" placeholder="Estoque mínimo">
              </div>
            </div>

            <div class="form-group">
              <label for="produto_imagem" class="col-sm-2 control-label">Imagem </label>
              <div class="controls">
                <input id="arquivo" type="file" name="userfile" /> (png|jpg|jpeg)
              </div>
            </div>
            <hr>

            <div class="form-group">
              <label for="produto_imposto_id" class="col-sm-2 control-label">Impostos </label>
              <div class="col-sm-5">
                <select class="form-control" name="produto_imposto_id" id="produto_imposto_id">
                  <option value="">Selecione</option>
                  <?php foreach ($impostos as $i) : ?>
                    <option value='<?= $i->imposto_id; ?>' <?= $dados[0]->produto_imposto_id == $i->imposto_id ? 'selected' : '' ?>>
                      <?= $i->imposto_descricao; ?>
                    </option>
                  <?php endforeach ?>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="produto_peso" class="col-sm-2 control-label">Peso em KG </label>
              <div class="col-sm-5">
                <input type="text" class="form-control" name="produto_peso" id="produto_peso" value="<?= $dados[0]->produto_peso; ?>" placeholder="Peso em KG">
              </div>
            </div>

            <div class="form-group">
              <label for="produto_ncm" class="col-sm-2 control-label">NCM </label>
              <div class="col-sm-5">
                <input type="text" class="form-control" name="produto_ncm" id="produto_ncm" value="<?= $dados[0]->produto_ncm; ?>" placeholder="NCM">
              </div>
            </div>

            <div class="form-group">
              <label for="produto_cest" class="col-sm-2 control-label">CEST </label>
              <div class="col-sm-5">
                <input type="text" class="form-control" name="produto_cest" id="produto_cest" value="<?= $dados[0]->produto_cest; ?>" placeholder="CEST">
              </div>
            </div>

            <div class="form-group">
              <label for="produto_origem" class="col-sm-2 control-label">Origem </label>
              <div class="col-sm-5">
                <div class="input-group">
                  <span class="input-group-addon" style="background-color: #ccc; cursor: pointer;" data-toggle="modal" data-target="#myModal">
                    <i class="fa fa-fw fa-question-circle"></i>
                  </span>
                  <select class="form-control" name="produto_origem" id="produto_origem">
                    <option value="0" <?= ($dados[0]->produto_origem == 0) ? 'SELECTED' : ''; ?>>0 - Nacional</option>
                    <option value="1" <?= ($dados[0]->produto_origem == 1) ? 'SELECTED' : ''; ?>>1 - Estrangeira</option>
                    <option value="2" <?= ($dados[0]->produto_origem == 2) ? 'SELECTED' : ''; ?>>2 - Estrangeira</option>
                    <option value="4" <?= ($dados[0]->produto_origem == 4) ? 'SELECTED' : ''; ?>>4 - Nacional</option>
                    <option value="5" <?= ($dados[0]->produto_origem == 5) ? 'SELECTED' : ''; ?>>5 - Nacional</option>
                    <option value="6" <?= ($dados[0]->produto_origem == 6) ? 'SELECTED' : ''; ?>>6 - Estrangeira</option>
                    <option value="7" <?= ($dados[0]->produto_origem == 7) ? 'SELECTED' : ''; ?>>7 - Estrangeira</option>
                    <option value="8" <?= ($dados[0]->produto_origem == 8) ? 'SELECTED' : ''; ?>>8 - Nacional</option>
                  </select>
                </div>
              </div>
            </div>

            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header text-white" style="background-color: #ccc;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 class="modal-title" id="myModalLabel">Origem do Produto</h5>
                  </div>
                  <div class="modal-body bg-light">
                    <p class="text-dark">
                      0 - Nacional, exceto as indicadas nos códigos 3, 4, 5 e 8
                      <br>
                      1 - Estrangeira - Importação direta, exceto a indicada no código 6
                      <br>
                      2 - Estrangeira - Adquirida no mercado interno, exceto a indicada no código 7
                      <br>
                      3 - Nacional, mercadoria ou bem com Conteúdo de Importação superior a 40% e inferior ou igual a 70%
                      <br>
                      4 - Nacional, cuja produção tenha sido feita em conformidade com os processos produtivos básicos de que tratam as legislações citadas nos Ajustes
                      <br>
                      5 - Nacional, mercadoria ou bem com Conteúdo de Importação inferior ou igual a 40%
                      <br>
                      6 - Estrangeira - Importação direta, sem similar nacional, constante em lista da CAMEX e gás natural
                      <br>
                      7 - Estrangeira - Adquirida no mercado interno, sem similar nacional, constante lista CAMEX e gás natural
                      <br>
                      8 - Nacional, mercadoria ou bem com Conteúdo de Importação superior a 70%.
                    </p>

                  </div>
                </div>
              </div>
            </div>

          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>" class="btn btn-default">Voltar</a>
            <button type="submit" class="btn btn-primary pull-right"><?php echo ucfirst($this->uri->segment(2)); ?></button>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
  </div>
</section>



<script>
  function resetarSelectCodigos() {
		$("#codigo_produto").html('<option value="">Selecione um grupo</option>');
	}

  $(document).ready(function () {
        function preencherSelectCodigos() {
            var grupo = $("#grupo_produto").val();

            if (grupo) {
                $.ajax({
                    method: "POST",
                    url: "<?php echo base_url(); ?>produto/preencherSelectCodigos",
                    dataType: "json",
                    data: { grupo: grupo }
                }).done(function(response) {
                    var options = '<option value="">Selecione</option>';
                    response.forEach(function(item) {
                        options += '<option value="' + item.codigo_prod_codigo + '">' + item.codigo_prod_codigo + ' - ' + item.codigo_prod_descricao + '</option>';
                    });
                    $("#codigo_produto").html(options);
                });
            } else {
                resetarSelectCodigos();
            }
        }

        $("#grupo_produto").change(function () {
            preencherSelectCodigos();
        });

        $("#codigo_produto").change(function () {
            var selectedOption = $(this).find('option:selected').val();
            $("#codigo_produto_real").val(selectedOption);
        });
    });
</script>