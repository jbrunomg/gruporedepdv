<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <div class="col-md-2">
        <?php if(verificarPermissao('aAlmoxarifado')){ ?>
        <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/adicionar" type="button" class="btn btn-block btn-default btn-flat">Adicionar</a>
        <?php } ?>
      </div>          
    </div>
    <div class="box-body">
      <div class="container-fluid">
        <table id="examplee" class="table table-bordered table-striped" width="100%">
          <thead>
            <tr>                                            
              <th>Código</th>
              <th>Categoria</th>
              <th>Data</th>
              <th>Feito / Falta</th>              
              <th>Ações</th>
            </tr>
          </thead>
          <tbody>

            <?php foreach ($dados as $d){ 
             //  var_dump($dados);die(); ?>
            <tr>                      
              <td> <?php echo $d->estoque_ajuste_id;?></td> 
              <td> <?php echo $d->categoria_prod_descricao;?></td> 
              <td> <?php echo $d->estoque_ajuste_data;?></td> 
              <td> <?php echo $d->feito.'/'.$d->falta;?></td>
                             
              <td>
                <?php if(verificarPermissao('vAlmoxarifado')){ ?>
                <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/visualizar/<?php echo $d->estoque_ajuste_id; ?>" data-toggle="tooltip" title="Visualizar"><i class="fa fa-search  text-success"></i> </a>
                <?php } ?>

                <?php if(verificarPermissao('eAlmoxarifado')){ 
                  if ($d->falta <> 0) { ?>
                <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/editar/<?php echo $d->estoque_ajuste_id; ?>" data-toggle="tooltip" title="Editar"><i class="fa fa-edit"></i> </a>
                <?php } }  ?>

                <?php if(verificarPermissao('dAlmoxarifado')){ ?>
                <a style="cursor:pointer;" title="Deletar" data-toggle="modal" data-target="#excluir" onclick="excluir('<?php echo base_url().$this->uri->segment(1)."/excluir/".$d->estoque_ajuste_id; ?>');" ><i class="fa fa-trash  text-danger"></i></a>
                <?php } ?>             
              </td>
            </tr>
            <?php } ?>

          </tbody>
          <tfoot>
            <tr>                     
              <th>Código</th>
              <th>Categoria</th>
              <th>Data</th>
              <th>Situação</th>              
              <th>Ações</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</section>


<script type="text/javascript">
  // Basic example
$(document).ready(function () {
$('#examplee').DataTable({
"ordering": false // false to disable sorting (or any other option)
});

});


</script>