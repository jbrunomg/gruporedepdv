<section class="content">
  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="<?php echo current_url(); ?>" method="post">
        <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $dados[0]->id; ?>" placeholder="id">
           <div class="box-body">

            <div class="form-group">
              <label for="nome_digitado" class="col-sm-2 control-label">Nome Digitado*</label>
              <div class="col-sm-5">              
                <input disabled type="text" class="form-control" name="nome_digitado" id="nome_digitado" value="<?php echo $dados[0]->nome_digitado; ?>" placeholder="Descrição">
              </div>
            </div> 

            <div class="form-group">
              <label for="loja" class="col-sm-2 control-label">Grupo </label>
              <div class="col-sm-5">  

                <select disabled class="form-control" name="produto_categoria_id" id="produto_categoria_id">
                  <option value="">Selecione</option>
                  <?php foreach ($grupo as $g) { ?>
                  <?php $selected = ($g->categoria_prod_id == $dados[0]->produto_categoria_id)?'SELECTED':''; ?>
                    <option   value='<?php echo $g->categoria_prod_id; ?>' <?php echo $selected; ?>><?php echo $g->categoria_prod_descricao; ?> </option>
                  <?php } ?>
                </select>        

              </div>
            </div> 

            <div class="form-group">
              <label for="operador" class="col-sm-2 control-label">Tipo</label>
              <div class="col-sm-5">
                            
                <select disabled required class="form-control" name="operador" id="tipo">
                    <option value='1' <?php echo ($dados[0]->operador == 1)?'Selected':'';?>> = &nbsp;&nbsp;&nbsp;  Igualdade </option>
                    <option value='2' <?php echo ($dados[0]->operador == 2)?'Selected':'';?>> % &nbsp;&nbsp;&nbsp;  Contem </option>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="nome_sistema" class="col-sm-2 control-label">Nome no Sistema*</label>
              <div class="col-sm-5">              
                <input disabled type="text" class="form-control" name="nome_sistema" id="nome_sistema" value="<?php echo $dados[0]->nome_sistema; ?>" placeholder="Margem">
              </div>
            </div>

          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>" class="btn btn-default">Voltar</a>
            
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
  </div>
</section>