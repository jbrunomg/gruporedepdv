<section class="content">
  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="<?php echo current_url(); ?>" method="post">
          <div class="box-body">

            <div class="form-group">
              <label for="nome_digitado" class="col-sm-2 control-label">Nome Digitado*</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" name="nome_digitado" id="nome_digitado" value="<?php echo set_value('nome_digitado'); ?>" placeholder="Nome Digitado">
              </div>
            </div> 

            <div class="form-group">
              <label for="loja" class="col-sm-2 control-label">Grupo </label>
              <div class="col-sm-5">

                <select id="produto_categoria_id" class="form-control" data-placeholder="Selecione o grupo de produtos" name="produto_categoria_id" >
                    <option value="">Selecione</option>
                    <?php foreach ($grupo as $g) { ?>
                    <option value='<?php echo $g->categoria_prod_id; ?>'><?php echo $g->categoria_prod_descricao; ?> </option>
                    <?php } ?>                       
                </select>   

              </div>
            </div>   

            <div class="form-group">
              <label for="operador" class="col-sm-2 control-label">Operado</label>
              <div class="col-sm-5">
                            
                <select required class="form-control" name="operador" id="operador">
                    <option value='1'> = &nbsp;&nbsp;&nbsp;  Igualdade</option>
                    <option value='2'> % &nbsp;&nbsp;&nbsp;  Contem</option>
                </select>
              </div>
            </div>   

            <div class="form-group">
              <label for="nome_sistema" class="col-sm-2 control-label">Nome no Sistema</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" name="nome_sistema" id="nome_sistema" value="<?php echo set_value('nome_sistema'); ?>" placeholder="Nome no Sistema">
              </div>
            </div>      

          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>" class="btn btn-default">Voltar</a>
            <button type="submit" class="btn btn-primary pull-right"><?php echo ucfirst($this->uri->segment(2)); ?></button>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
  </div>
</section>