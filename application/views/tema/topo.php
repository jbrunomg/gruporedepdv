<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo strtoupper(SUBDOMINIO) ?>  | Controler</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/verificar-conexao.css">
  
  <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<?php 

$this->CI = get_instance();

switch ($this->CI->uri->segment(1).'/'.$this->CI->uri->segment(2)) { 

  case 'movimentacao/editar':
    $layout = 'sidebar-collapse';
    break;

  case 'estoque/editar':
    $layout = 'sidebar-collapse';
    break; 

  case 'estoque/visualizar':
    $layout = 'sidebar-collapse';
    break;  

  case 'precoajuste/editar':
    $layout = 'sidebar-collapse';
    break; 

  case 'precoajuste/visualizar':
    $layout = 'sidebar-collapse';
    break;    
   
  case 'sistema/dashboardv2':
    $layout = 'sidebar-collapse';
    break;   

  
  default:
    $layout = '';
    break;
}
?>
<body class="hold-transition skin-<?php echo CLIENTE ?> <?php echo $layout ?> sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url(); ?>sistema" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>M</b>-Controler</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>M</b>-Controler</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

     

          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning" id="somaNotificacao"></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">Você tem <span id="somaNotificacao2"></span> notificação</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li>
                    <a href="<?php echo base_url(); ?>movimentacao/mercadoriaLiberada">
                      <i class="fa fa-truck text-aqua"></i> <span id="apiNotificacao"></span> Lote liberado pela matriz
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-warning text-yellow"></i> Você teve atualização de preço(s)
                    </a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>relatorios/produtos">
                      <i class="fa fa-users text-red"></i><span id="apiProd20"></span> Produtos a mais de 20 dias parado em estoque.
                    </a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>relatorios/vendas/VendasDiaDashbord" target="_blank">
                      <i class="fa fa-shopping-cart text-green"></i> <span id="apiProdLjParc"></span> Intes vendidos hoje da(s) loja(s) parceira.
                    </a>
                  </li>           
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>

          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger" id="somaTarefa"></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">Você tem <span id="somaTarefa2"></span> tarefa</li>              
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- Task item -->
                    <a href="<?php echo base_url(); ?>vendas/listarAberto">
                      <h3>
                        Venda(s) em aberto
                        <small class="pull-right"><span id="apiVendas"></span></small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">20% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <!-- Task item -->
            <!--  <li>
                    <a href="#">
                      <h3>
                        Create a nice theme
                        <small class="pull-right">40%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">40% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>  -->
                  <!-- end task item -->
                  <!-- Task item -->
                 <li>
                    <a href="<?php echo base_url(); ?>vendas/listarZerada">
                      <h3>
                        Venda(s) zerada(s)
                        <small class="pull-right"><span id="apiVendaZerada"></span></small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">60% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li> 
                  <!-- end task item -->
                  <!-- Task item -->
                  <li>
                    <a href="<?php echo base_url(); ?>vendas/listarVendaIncompleta">
                      <h3>
                        Venda(s) Celular incompleta
                        <small class="pull-right"><span id="apiVendaIncompleta"></span></small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">80% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li> 
                  <!-- end task item -->
                </ul>
              </li>
              <li class="footer">
                <a href="#">View all tasks</a>
              </li>
            </ul>
          </li>
        
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="<?php echo base_url(); ?>assets/usuarios/<?php echo $this->session->userdata('usuario_imagem')  ?>"  class="user-image" alt="Avatar" />
                <span class="hidden-xs"><?php echo $this->session->userdata('usuario_nome'); ?></span>
              </a>
              <ul class="dropdown-menu">
                <li class="user-header">
                  <img src="<?php echo base_url(); ?>assets/usuarios/<?php echo $this->session->userdata('usuario_imagem')  ?>"  class="img-circle" alt="Avatar" />
                  <p>
                   <?php echo $this->session->userdata('usuario_nome'); ?> <br> <?php echo $this->session->userdata('usuario_perfil_nome'); ?>
                  </p>
                </li>
                <li class="user-footer">
                  <div class="pull-left">
                    <a href="#" class="btn btn-default btn-flat" data-toggle="modal" data-target=".bs-example-modal-sm">Alterar Senha</a>
                  </div>
                  <div class="pull-right">
                    <a href="<?php echo base_url(); ?>sistema/logout" class="btn btn-default btn-flat">Sair</a>
                  </div>
                </li>
              </ul>
            </li>
          <!-- Control Sidebar Toggle Button -->
          <!-- <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li> -->
        </ul>
      </div>
    </nav>
  </header>