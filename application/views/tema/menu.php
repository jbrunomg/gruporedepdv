<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url(); ?>assets/usuarios/<?php echo $this->session->userdata('usuario_imagem')  ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $this->session->userdata('usuario_nome'); ?> </p>
          <a href="#"><i class="fa fa-circle text-success"></i> <?php echo $this->session->userdata('usuario_perfil_nome'); ?></a>
        </div>
      </div>
      <!-- search form -->
      <form action="<?php echo base_url(); ?>sistema/pesquisa" method="post" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="pesquisa" class="form-control" placeholder="Pesquisar...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <!-- <li class="header">MAIN NAVIGATION</li> -->
        <li class="treeview">
          <a href="<?php echo base_url(); ?>sistema/dashboard">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        	<?php if($this->session->userdata('empresa_tipo') == '1'){ ?>
        	<li class="treeview">
        	  <a href="<?php // echo base_url(); ?>sistema/dashboardv2">
              <i class="fa fa-dashboard"></i> <span>Dashboard v2</span>
            </a>
         	</li>
          <?php } ?>

        <ul class="sidebar-menu" data-widget="tree">
          <li class="treeview">
            <a href="#">
              <i class="fa fa-users"></i>
              <span>Pessoas</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <?php if(verificarPermissao('vCliente')){ ?>
              <li><a href="<?php echo base_url(); ?>clientes"><i class="fa fa-circle-o"></i> Cliente</a></li>
              <?php } ?>
              <?php if(verificarPermissao('vFornecedor')){ ?>
              <li><a href="<?php echo base_url(); ?>fornecedores"><i class="fa fa-circle-o"></i> Fornecedor</a></li>
              <?php } ?>
              <?php if(verificarPermissao('vFuncionario')){ ?>
              <li><a href="<?php echo base_url(); ?>funcionarios"><i class="fa fa-circle-o"></i> Funcionário</a></li>
              <?php } ?>
            </ul>

          </li>
        </ul>

        <ul class="sidebar-menu" data-widget="tree">
          <li class="treeview">
            <a href="#">
              <i class="fa fa-list"></i>
              <span>Categorias/Grupos</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <?php if(verificarPermissao('vCategoria')){ ?>
              <li><a href="<?php echo base_url(); ?>categoriacartao"><i class="fa fa-circle-o"></i> Cartão </a></li>
              <li><a href="<?php echo base_url(); ?>categoriafinanceiro"><i class="fa fa-circle-o"></i> Centro de custo </a></li>
              <li><a href="<?php echo base_url(); ?>categoriacliente"><i class="fa fa-circle-o"></i> Cliente </a></li>
              <li><a href="<?php echo base_url(); ?>categoriaproduto"><i class="fa fa-circle-o"></i> Produto </a></li>
              <?php } ?>
            </ul>

          </li>
        </ul>

        <ul class="sidebar-menu" data-widget="tree">
          <li class="treeview">
            <a href="#">
              <i class="fa fa-wrench"></i>
              <span>Almoxarifado</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <?php if(verificarPermissao('vAlmoxarifado')){ ?>
              <li><a href="<?php echo base_url(); ?>produto"><i class="fa fa-circle-o"></i> Produto</a></li>
              <?php } ?>
              <?php if(verificarPermissao('vAlmoxarifado')){ ?>
              <li><a href="<?php echo base_url(); ?>produtoavaria"><i class="fa fa-circle-o"></i> Produto Avaria</a></li>
              <?php } ?>
              <?php if(verificarPermissao('vAlmoxarifado')  and ($this->session->userdata('empresa_tipo') == '1')) { ?>
              <li><a href="<?php echo base_url(); ?>precoajuste"><i class="fa fa-circle-o"></i> Ajuste de preço</a></li>
              <?php } ?>
              <?php if(verificarPermissao('vAlmoxarifado')){ ?>
              <li><a href="<?php echo base_url(); ?>estoque"><i class="fa fa-circle-o"></i> Ajuste de estoque</a></li>
              <?php } ?>
            </ul>

          </li>
        </ul>

        <ul class="sidebar-menu" data-widget="tree">
          <?php if(verificarPermissao('vFinanceiro')){ ?>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-money"></i>
              <span>Financeiro</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="<?php echo base_url(); ?>financeiro"><i class="fa fa-circle-o"></i> Lançamentos</a></li>
              <li><a href="<?php echo base_url(); ?>financeiro/financeiroBaixarListar"><i class="fa fa-circle-o"></i> Baixa Parcial</a></li>
            </ul>
          </li>
          <?php } ?>
        </ul>

        <ul class="sidebar-menu" data-widget="tree">
          <?php if(verificarPermissao('vFiscal')){ ?>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-money"></i>
              <span>Fiscal</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="<?php echo base_url(); ?>emissao"><i class="fa fa-circle-o"></i> Emissão</a></li>
              <li><a href="<?php echo base_url(); ?>financeiro/financeiroBaixarListar"><i class="fa fa-circle-o"></i> Carta de Correção(CC-e)</a></li>
              <li><a href="<?php echo base_url(); ?>imposto"><i class="fa fa-circle-o"></i> Impostos</a></li>
              <li><a href="<?php echo base_url(); ?>financeiro/financeiroBaixarListar"><i class="fa fa-circle-o"></i> Nota Fiscal de Serviço</a></li>
            </ul>
          </li>
          <?php } ?>
        </ul>

        <ul class="sidebar-menu" data-widget="tree">
          <li class="treeview">
            <a href="#">
              <i class="fa fa-tags"></i>
              <span>Ordem de Serviço</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <?php if(verificarPermissao('vOrdemServico')){ ?>
              <li><a href="<?php echo base_url(); ?>ordemservico"><i class="fa fa-circle-o"></i> O.S</a></li>
              <?php } ?>
              <?php if(verificarPermissao('vServico')){ ?>
              <li><a href="<?php echo base_url(); ?>servico"><i class="fa fa-circle-o"></i> Serviço</a></li>
              <?php } ?>
            </ul>

          </li>
        </ul>

        <ul class="sidebar-menu" data-widget="tree">
          <li class="treeview">
            <a href="#">
              <i class="fa fa-shopping-cart"></i>
              <span>Vendas</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <?php if(verificarPermissao('vVenda')){ ?>
              <li><a href="<?php echo base_url(); ?>pedido"><i class="fa fa-circle-o"></i> Pedido - Orçamento</a></li>
              <?php } ?>

              <?php if(verificarPermissao('vVenda')){ ?>
              <!-- <li><a href="<?php echo base_url(); ?>PDV"><i class="fa fa-circle-o"></i> PDV</a></li> -->

              <?php if (SUBDOMINIO == 'localhost' OR GRUPOLOJA == 'grupocell' OR GRUPOLOJA == 'gruporeccell' OR GRUPOLOJA == 'grupobyte' OR GRUPOLOJA == 'novasenergias') { ?>
                <li><a href="<?php echo base_url(); ?>Pdvtablet"><i class="fa fa-circle-o"></i> PDV - Cell</a></li>

                <li><a href="<?php echo base_url(); ?>Pdvtablet2"><i class="fa fa-circle-o"></i> PDV2 - Cell</a></li>

              <?php } else { ?>
              <li><a href="<?php echo base_url(); ?>pdv2"><i class="fa fa-circle-o"></i> PDV</a></li>
              <?php } ?>
              <?php } ?>

              <?php if(verificarPermissao('vVenda')){ ?>
               <!-- exibir só em dispositivo movel -->
              <!-- <li class="visible-xs"><a href="<?php echo base_url(); ?>Pdvtablet"><i class="fa fa-circle-o"></i> PDV - Cell</a></li>  -->
              <?php } ?>
              <?php if(verificarPermissao('vVenda')){ ?>
              <li><a href="<?php echo base_url(); ?>vendas"><i class="fa fa-circle-o"></i> Vendas</a></li>
              <?php } ?>
              <li><a href="<?php echo base_url(); ?>simulador"><i class="fa fa-circle-o"></i> Simulador Cartão</a></li>            
            </ul>

          </li>
        </ul>

        <ul class="sidebar-menu" data-widget="tree">
          <li class="treeview">
            <a href="#">
              <i class="fa fa-retweet"></i>
              <span>Movimentação</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <?php if(verificarPermissao('vAlmoxarifado')){ ?>
              <li><a href="<?php echo base_url(); ?>movimentacao"><i class="fa fa-circle-o"></i> Entrada/Saida mercadoria</a></li>
              <?php } ?>
              <?php if(verificarPermissao('vAlmoxarifado')){ ?>
              <li><a href="<?php echo base_url(); ?>cadastroimei"><i class="fa fa-circle-o"></i> Cadastro IMEI</a></li>
              <?php } ?>
              <?php if(verificarPermissao('vAlmoxarifado') and ($this->session->userdata('empresa_tipo') == '1')){ ?>
              <li><a href="<?php echo base_url(); ?>controlecotacao"><i class="fa fa-circle-o"></i> Compra para comercialização</a></li>
              <?php } ?>
              <?php if(verificarPermissao('vAlmoxarifado')){ ?>
              <li><a href="<?php echo base_url(); ?>compra"><i class="fa fa-circle-o"></i> Simulação de Compra</a></li>
              <?php } ?> 
              <?php if(verificarPermissao('vAlmoxarifado')){ ?>
              <li><a href="<?php echo base_url(); ?>devolucao"><i class="fa fa-circle-o"></i> Devolução Mercadoria</a></li>
              <?php } ?>
              <?php if(verificarPermissao('vAlmoxarifado')){ ?>
              <li><a href="<?php echo base_url(); ?>entradaxml"><i class="fa fa-circle-o"></i> Entrada de Xml</a></li>
              <?php } ?>
              <?php if(verificarPermissao('vAlmoxarifado')){ ?> 
              <li><a href="<?php echo base_url(); ?>movimentacao/fornecedorBaixarCompraListar"><i class="fa fa-circle-o"></i> Baixa Parcial - Compra</a></li> 
              <?php } ?> 

            </ul>

          </li>
        </ul>


      <ul class="sidebar-menu" data-widget="tree">
        <li class="treeview">
          <a href="#">
            <i class="fa fa-file-text"></i>
            <span>Relatórios</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <?php if(verificarPermissao('clienteRelatorio')){ ?>
            <li><a href="<?php echo base_url(); ?>relatorios/clientes"><i class="fa fa-circle-o"></i> Clientes</a></li>
            <?php } ?>
            <?php if(verificarPermissao('fornecedorRelatorio')){ ?>
            <!-- <li><a href="<?php echo base_url(); ?>relatorios/forncedor"><i class="fa fa-circle-o"></i> Forncedor</a></li> -->
            <?php } ?>
            <?php if(verificarPermissao('chatRelatorio')){ ?>
            <li><a href="<?php echo base_url(); ?>relatorios/chat"><i class="fa fa-circle-o"></i> Chat</a></li>
            <?php } ?>
            <?php if(verificarPermissao('produtoRelatorio')){ ?>
            <li><a href="<?php echo base_url(); ?>relatorios/produtos"><i class="fa fa-circle-o"></i> Produtos</a></li>
            <?php } ?>
            <?php if(verificarPermissao('simulacaoRelatorio') and ($this->session->userdata('empresa_tipo') == '1')){ ?>
            <li><a href="<?php echo base_url(); ?>relatorios/simulacaoCompra"><i class="fa fa-circle-o"></i> Simulação de Compra</a></li>
            <?php } ?>
            <?php if(verificarPermissao('comprasRelatorio') and ($this->session->userdata('empresa_tipo') == '1')){ ?>
            <li><a href="<?php echo base_url(); ?>relatorios/compras"><i class="fa fa-circle-o"></i>Compras</a></li>
            <?php } ?>
            <?php if(verificarPermissao('vendasRelatorio')){ ?>
            <li><a href="<?php echo base_url(); ?>relatorios/vendas"><i class="fa fa-circle-o"></i> Vendas</a></li>
            <?php } ?>
            <?php if(verificarPermissao('vendedoresRelatorio')){ ?>
            <li><a href="<?php echo base_url(); ?>relatorios/vendedores"><i class="fa fa-circle-o"></i> Vendedores</a></li>
            <?php } ?>
            <?php if(verificarPermissao('financeiroRelatorio')){ ?>
            <li><a href="<?php echo base_url(); ?>relatorios/financeiro"><i class="fa fa-circle-o"></i> Financeiro</a></li>
            <?php } ?>

            <?php if(verificarPermissao('vendedorExtRelatorio')){ ?>
            <li><a href="<?php echo base_url(); ?>relatorios/vendedorExterno"><i class="fa fa-circle-o"></i> Vendedor Externo</a></li>
            <?php } ?>

            <li><a href="<?php echo base_url(); ?>relatorios/inconsistencia"><i class="fa fa-circle-o"></i> Inconsistência</a></li>

            <li><a href="<?php echo base_url(); ?>balancoproduto"><i class="fa fa-circle-o"></i> Balanço - Produto</a></li>
            <?php if(verificarPermissao('financeiroRelatorio')){ ?>
            <li><a href="<?php echo base_url(); ?>relatorios/fechamentoCaixa"><i class="fa fa-circle-o"></i> Fechamento Caixa</a></li>
            <?php } ?>
          </ul>

        </li>

      </ul>

      <ul class="sidebar-menu" data-widget="tree">
        <li class="treeview">
          <a href="#">
            <i class="fa fa-gear"></i>
            <span>Configurações</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <?php if(verificarPermissao('vUsuario')){ ?>
            <li><a href="<?php echo base_url(); ?>usuarios"><i class="fa fa-circle-o"></i> Usuários</a></li>
            <?php } ?>
            <?php if(verificarPermissao('vPerfil')){ ?>
            <li><a href="<?php echo base_url(); ?>perfil"><i class="fa fa-circle-o"></i> Perfis</a></li>
            <!-- <li><a href="<?php echo base_url(); ?>parametro/editar"><i class="fa fa-circle-o"></i> Parâmetros </a></li> -->

            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Parâmetros
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url(); ?>parametro/editar"><i class="fa fa-circle-o"></i> Cartão / Chat</a></li>
                <li><a href="<?php echo base_url(); ?>palavrachave"><i class="fa fa-circle-o"></i> Palavras Chaves</a></li>
                <li><a href="<?php echo base_url(); ?>simulador/configurar"><i class="fa fa-circle-o"></i> Taxas Simulador</a></li>
                <li><a href="<?php echo base_url(); ?>Codigoproduto"><i class="fa fa-circle-o"></i> Código de Produtos</a></li>


   <!--         <li class="treeview">
                  <a href="#"><i class="fa fa-circle-o"></i> Level Two
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                  </ul>
                </li> -->
              </ul>
            </li>


            <?php } ?>
            <?php if(verificarPermissao('vEmitente')){ ?>
            <li><a href="<?php echo base_url(); ?>sistema/emitente"><i class="fa fa-circle-o"></i> Emitente</a></li>
            <?php } ?>
            <?php if(verificarPermissao('vBackup')){ ?>
            <li><a href="<?php echo base_url(); ?>sistema/backup"><i class="fa fa-circle-o"></i> Backup</a></li>
            <?php } ?>
          </ul>
        </li>

      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo ucfirst($this->uri->segment(1));?>
        <small><?php echo ucfirst($this->uri->segment(2));?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>sistema/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="<?php echo base_url().$this->uri->segment(1);?>"><?php echo ucfirst($this->uri->segment(1));?></a></li>
        <li class="active"><?php echo ucfirst($this->uri->segment(2));?></li>
      </ol>
    </section>

  <!-- Tratamento de mensagens do sitema -->
  <?php if($this->session->flashdata('error') or $this->session->flashdata('erro')){ ?>
      <div class="col-md-12">
        <div class="alert alert-danger alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <h5><?php echo $this->session->flashdata('erro') ? $this->session->flashdata('erro') : $this->session->flashdata('error') ; ?></h5>
        </div>
      </div>
      <?php }else if($this->session->flashdata('success')){ ?>
      <div class="col-md-12">
        <div class="alert alert-success alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <h5></i> <?php echo $this->session->flashdata('success'); ?></h5>
        </div>
      </div>
      <?php } ?>
