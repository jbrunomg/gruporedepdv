 </div>

 <!-- Modal Rejeitar registros -->
 <div class="example-modal">
  <div class="modal" id="rejeitar">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Rejeitar</h4>
        </div>
        <div class="modal-body">
          <p>Deseja realmente <strong> rejeitar </strong> a mercadoria ?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
          <a href="" class="btn btn-danger" id="excluirIdRejeita">Rejeitar</a>
        </div>
      </div>      
    </div>          
  </div>        
</div>  
<!-- Fim modal Rejeitar registros -->

<!-- Modal Excluir registros -->
 <div class="example-modal">
  <div class="modal" id="excluir">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Excluir</h4>
        </div>
        <div class="modal-body">
          <p>Deseja realmente excluir?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
          <a href="" class="btn btn-danger" id="excluirId">Excluir</a>
        </div>
      </div>      
    </div>          
  </div>        
</div>  
<!-- Fim modal Excluir registros -->

<!-- Modal Excluir registros -->
 
<!-- Fim modal Excluir registros -->

<!-- modal senha -->

<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button> 
        <h4 class="modal-title" id="mySmallModalLabel">Alterar Senha</h4> 
      </div> 
      <div class="modal-body">
          <form class="form-horizontal">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="">Senha Atual</label>                  
                    <input class="form-control" id="senha_atual"  type="password">
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="">Nova Senha</label>                  
                    <input class="form-control" id="nova_senha"  type="password">
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="">Confirma Senha</label>                  
                    <input class="form-control" id="confirma_senha"  type="password">
                </div>                
              </div>
              <span class="msg-error text-danger"></span>
              <span class="msg-success text-success"></span>
              <!-- /.box-body -->
              <div class="box-footer">
                <span id="alterarSenha" class="btn btn-primary pull-right">Alterar Senha</span>
              </div>
              <!-- /.box-footer -->
            </form>
      </div> 
    </div>
  </div>
</div>

<!-- fim modal senha -->

  <!-- /.content-wrapper -->
 <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.18
    </div>
    <strong>Copyright &copy; 2019- <?php echo date('Y');?> <a href="www.wdmtecnologia.com.br">WDM-Tecnologia</a>.</strong> All rights
    reserved.
  </footer>


<!-- VERIFICAR CONEXAO  -->
<div id="taja-cnx-geral" class="taja-cnx-geral">
    <div id="taxa-cnx-off" class="taja-cnx-geral">
        <svg viewBox="0 0 24 24" id="svg-cnx-off" height="24" width="24">
            <path
                d="M23.7805.2195c.2925.293.2925.768 0 1.061l-22.5 22.5C1.134 23.927.942 24 .75 24c-.192 0-.384-.073-.5305-.2195-.2925-.293-.2925-.768 0-1.061l12.1967947-12.1971509c-2.3773758-.1150468-4.79162015.7286756-6.6038447 2.5401009-.3905.3905-1.0235.3905-1.414 0-.3905-.3905-.3905-1.024 0-1.414 2.65728981-2.65728981 6.3696459-3.62280965 9.8005664-2.90852144l2.430738-2.43186193C11.7816792 4.58111626 6.15198938 5.65271062 2.27735 9.52735c-.3905.3905-1.0235.3905-1.414 0-.3905-.3905-.3905-1.0235 0-1.414 4.6714534-4.67191574 11.571522-5.78608437 17.3096457-3.34843552L22.7195.2195c.293-.2925.768-.2925 1.061 0zM12 18.5c.6905 0 1.25.5595 1.25 1.25S12.6905 21 12 21s-1.25-.5595-1.25-1.25.5595-1.25 1.25-1.25zm1.4175-4.81495c.9705.2455 1.8905.741 2.6485 1.499.3905.3905.3905 1.0235 0 1.414-.1955.1955-.451.293-.707.293-.256 0-.512-.0975-.707-.293-.7835333-.7835333-1.8301422-1.1445778-2.8581093-1.0880116L11.574 15.52855l1.8435-1.8435zm3.8154-3.8154c.848.4725 1.649 1.059 2.3685 1.779.391.39.391 1.023 0 1.414-.195.195-.451.293-.707.293-.2555 0-.5115-.098-.707-.293-.7285-.728-1.5575-1.291-2.439-1.7085zm2.1908-2.1908l1.4425-1.4425c.8.545 1.5615 1.168 2.2705 1.877.3905.3905.3905 1.0235 0 1.414-.1955.1955-.451.293-.707.293-.256 0-.5115-.0975-.707-.293-.5933333-.59333333-1.2283333-1.11861111-1.895162-1.57959491L19.4237 7.67885l1.4425-1.4425z">
            </path>
        </svg>
        <span>Você está offline no momento. </span> <a href="" id="btn-atualizar-cnx">Atualizar</a>
    </div>
    <div id="taxa-cnx-on" class="taja-cnx-geral">
        <svg width="24" id="svg-cnx-on" height="24" viewBox="0 0 24 24">
            <path
                d="M8.213 16.984c.97-1.028 2.308-1.664 3.787-1.664s2.817.636 3.787 1.664l-3.787 4.016-3.787-4.016zm-1.747-1.854c1.417-1.502 3.373-2.431 5.534-2.431s4.118.929 5.534 2.431l2.33-2.472c-2.012-2.134-4.793-3.454-7.864-3.454s-5.852 1.32-7.864 3.455l2.33 2.471zm-4.078-4.325c2.46-2.609 5.859-4.222 9.612-4.222s7.152 1.613 9.612 4.222l2.388-2.533c-3.071-3.257-7.313-5.272-12-5.272s-8.929 2.015-12 5.272l2.388 2.533z" />
        </svg> <span>Sua conexão com a internet foi restaurada.</span>
    </div>
</div>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-yellow"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                <p>New phone +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                <p>nora@example.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-file-code-o bg-green"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                <p>Execution time 5 seconds</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Resume
                <span class="label label-success pull-right">95%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Laravel Integration
                <span class="label label-warning pull-right">50%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Back End Framework
                <span class="label label-primary pull-right">68%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Other sets of options are available
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Allow the user to show his name in blog posts
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Chat Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Delete chat history
              <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>

<script type="text/javascript">
  
var base_url = "<?php echo base_url(); ?>";

</script>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url(); ?>assets/bower_components/jquery-ui/jquery-ui.min.js"></script>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="<?php echo base_url(); ?>assets/bower_components/raphael/raphael.min.js"></script>

<script src="<?php echo base_url(); ?>assets/bower_components/chart.js/Chart.js"></script>
<!-- <script src="<?php echo base_url(); ?>assets/bower_components/morris.js/morris.min.js"></script> -->
<!-- Sparkline -->
<script src="<?php echo base_url(); ?>assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url(); ?>assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url(); ?>assets/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url(); ?>assets/bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url(); ?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>assets/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="<?php echo base_url(); ?>assets/dist/js/pages/dashboard.js"></script> -->
<!-- DataTables -->
<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>


<script src="<?php echo base_url(); ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url(); ?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js "></script>

<script src="https://igorescobar.github.io/jQuery-Mask-Plugin/js/jquery.mask.min.js"></script>     
<!-- iCheck 1.0.1 -->
<script src="<?php echo base_url(); ?>assets/plugins/iCheck/icheck.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="<?php echo base_url(); ?>assets/dist/js/pages/dashboard2.js"></script> -->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>
<script src="<?php echo base_url(); ?>assets/dist/js/action.js"></script>
<script src="<?php echo base_url(); ?>assets/dist/js/actionTable.js"></script>

<script src="<?php echo base_url(); ?>assets/dist/js/maskmoney.js"></script>

<script src="<?php echo base_url(); ?>assets/dist/js/verificar-conexao.js"></script>



<script type="text/javascript">
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
  }); 

  $(document).ready(function(){

      $(".money").maskMoney();
      $('#datepickerCadastroIncio').datepicker({
          autoclose: true,
          format: 'dd/mm/yyyy',
      });
      $('#datepickerCadastroFim').datepicker({
          autoclose: true,
          format: 'dd/mm/yyyy'
      });
      $('#datepickerVendaIncio').datepicker({
          autoclose: true,
          format: 'dd/mm/yyyy',
      });
      $('#datepickerVendaFim').datepicker({
          autoclose: true,
          format: 'dd/mm/yyyy',
      });
      $('#dateVendaIncio').datepicker({
          autoclose: true,
          format: 'dd/mm/yyyy',
      });
      $('#dateVendaFim').datepicker({
          autoclose: true,
          format: 'dd/mm/yyyy',
      });
      $('#datepickerFinaVecIncio').datepicker({
          autoclose: true,
          format: 'dd/mm/yyyy',
      });
      $('#datepickerFinaVecFim').datepicker({
          autoclose: true,
          format: 'dd/mm/yyyy',
      });
      $('#datepickerFinaPagIncio').datepicker({
          autoclose: true,
          format: 'dd/mm/yyyy',
      });
      $('#datepickerFinaPagFim').datepicker({
          autoclose: true,
          format: 'dd/mm/yyyy',
      });
      $('#datepickerprodutosVendasIncio').datepicker({
          autoclose: true,
          format: 'dd/mm/yyyy',
      });
      $('#datepickerprodutosVendasFim').datepicker({
          autoclose: true,
          format: 'dd/mm/yyyy',
      });
      $('#datepickerprodutosVendidosIncio').datepicker({
          autoclose: true,
          format: 'dd/mm/yyyy',
      });
      $('#datepickerprodutosVendidosFim').datepicker({
          autoclose: true,
          format: 'dd/mm/yyyy',
      });
      $('#datepickerprodutosPMVIncio').datepicker({
          autoclose: true,
          format: 'dd/mm/yyyy',
      });
      $('#datepickerprodutosPMVFim').datepicker({
          autoclose: true,
          format: 'dd/mm/yyyy',
      });
      $('#datepickerGrupo').datepicker({
          autoclose: true,
          format: 'mm/yyyy',
      });
      $('#datepickerVendasExcluidas').datepicker({
          autoclose: true,
          format: 'mm/yyyy',
      });
	    $('#datepickerGrupoAno').datepicker({
          minViewMode: "years",
          autoclose: true,
          format: 'yyyy',
      });
      $('#datepickerVendasAnual').datepicker({
          autoclose: true,
          format: 'yyyy',
      });
      $('#datepickerBaixar').datepicker({
          autoclose: true,
          format: 'dd/mm/yyyy',
      }).datepicker("setDate", new Date());

      $('#datepickerAbertoAno').datepicker({
          minViewMode: "years",
          autoclose: true,
          format: 'yyyy',
      }).datepicker("setDate", new Date());

      $('#datepickerAbertoMes').datepicker({
          minViewMode: "months",
          autoclose: true,
          format: 'mm',
      }).datepicker("setDate", new Date());

      $('#datepickerBaixaIncio').datepicker({
          autoclose: true,
          format: 'dd/mm/yyyy',
      });

      $('#datepickerBaixaFim').datepicker({
          autoclose: true,
          format: 'dd/mm/yyyy',
      });

      $('#recebimentoOdem').datepicker({
          autoclose: true,
          format: 'dd/mm/yyyy',
      });

      $('#vencimentoOdem').datepicker({
          autoclose: true,
          format: 'dd/mm/yyyy',
      });

      $('#tabBaixar').DataTable({
        "order": [[ 0, "desc" ]]
      });

 
  }); 




</script> 




</body>
</html>

