<section class="content">
  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>
        </div>
        <!-- /.box-header -->
        
        <div class="col-md-12">
          <!-- Custom Tabs (Pulled to the right) -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs pull-right">
              <li><a href="#tab_1" data-toggle="tab">#</a></li>
              <li><a href="#tab_2" data-toggle="tab">Comissão</a></li>
              <li class="active"><a href="#tab_3" data-toggle="tab">Dados</a></li>      
              <li class="pull-left header"><i class="fa fa-th"></i> Historico Funcionário</li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane" id="tab_1">
                <b>????????</b>

                
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                <b>listagem mes/ano comissão valor por vendendor/funcionario</b>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane active" id="tab_3">
                <div class="row">

                  <div class="col-md-6">
                    <label for="descricao">Nome</label>
                    <input disabled type="text" class="form-control" id="funcionario_nome" name="funcionario_nome"  value="<?php echo $dados[0]->funcionario_nome; ?>" placeholder="Nome Completo">
                  </div>
          
                  <div class="col-md-6 form-group">
                    <label for="descricao">CEP</label>
                   <input disabled type="text" class="form-control" id="cep" name="funcionario_cep"  value="<?php echo $dados[0]->funcionario_cep; ?>" placeholder="Cep">
                  </div>

                  <div class="col-md-6">
                    <label for="funcionario_cpf">CPF</label>
                    <input disabled type="text" class="form-control" name="funcionario_cpf" id="funcionario_cpf"  value="<?php echo $dados[0]->funcionario_cpf; ?>" placeholder="CPF">    
                  </div>

                  <div class="col-md-6 form-group">
                    <label for="descricao">Endereço</label>
                    <input disabled type="text" class="form-control" id="endereco" name="funcionario_endereco"  value="<?php echo $dados[0]->funcionario_endereco; ?>" placeholder="Endereço">
                  </div>
                   
                  <div class="col-md-6">
                    <label for="descricao">Telefone</label>
                    <input disabled type="text" class="form-control" id="telefone"  name="funcionario_telefone"  value="<?php echo $dados[0]->funcionario_telefone; ?>" placeholder="Telefone">
                  </div>

                  <div class="col-md-6 form-group">
                    <label for="descricao">Bairro</label>
                    <input disabled type="text" class="form-control" id="bairro" name="funcionario_bairro"  value="<?php echo $dados[0]->funcionario_bairro; ?>" placeholder="Bairro">
                  </div>

                  <div class="col-md-6">
                    <label for="descricao">Celular</label>
                     <input disabled type="text" class="form-control" id="celular" name="funcionario_celular"  value="<?php echo $dados[0]->funcionario_celular; ?>" placeholder="Celular">
                  </div>
                
                  <div class="col-md-6 form-group">
                    <label for="descricao">Número</label>
                      <input disabled type="text" class="form-control" id="numero" name="funcionario_numero"  value="<?php echo $dados[0]->funcionario_numero; ?>" placeholder="Número">                            
                  </div>

                  <div class="col-md-6">
                    <label for="descricao">E-mail</label>
                    <input disabled type="text" class="form-control" id="e-mail" name="funcionario_email"  value="<?php echo $dados[0]->funcionario_email; ?>" placeholder="E-mail">
                  </div>
               
                  <div class="col-md-6 form-group">
                    <label for="descricao">Complemento</label>
                      <input disabled type="text" class="form-control" id="completo" name="funcionario_complemento"  value="<?php echo $dados[0]->funcionario_complemento; ?>" placeholder="Complemento">
                  </div>  

                  <div class="col-md-6">
                    <label for="##">V</label>
                      <input disabled type="text" class="form-control" id="##" name="##"  value="<?php  ?>" placeholder="##">
                  </div>  

                  <div class="col-md-6 form-group">
                    <label for="descricao">Cidade</label>
                      <select disabled class="form-control" name="funcionario_cidade" id="cidade">
                        <option value="">Selecione</option>
                        <?php foreach ($cidades as $valor) { ?>
                        <?php $selected = ($valor->nome == strtoupper($dados[0]->funcionario_cidade))?'SELECTED':''; ?>
                          <option value='<?php echo $valor->nome; ?>'  <?php echo $selected; ?>><?php echo $valor->nome; ?> </option>
                        <?php } ?>                                    
                      </select>
                  </div> 

                  <div class="col-md-6">
                    <label for="##">V</label>
                      <input disabled type="text" class="form-control" id="##" name="##"  value="<?php  ?>" placeholder="##">
                  </div> 

                  <div class="col-md-6">
                    <label for="descricao">Estado</label>
                      <select disabled class="form-control" name="funcionario_estado" id="estado">
                        <option value="">Selecione</option>
                        <?php foreach ($estados as $valor) { ?>
                        <?php $selected = ($valor->sigla == $dados[0]->funcionario_estado)?'SELECTED':''; ?>
                          <option value='<?php echo $valor->sigla; ?>' <?php echo $selected; ?>><?php echo $valor->nome; ?> </option>
                        <?php } ?>
                      </select>
                  </div>        
                </div>

              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->



        <!-- /.box-body -->
        <div class="box-footer">
          <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>" class="btn btn-default">Voltar</a>          
        </div>
        <!-- /.box-footer -->        
      </div>
    </div>
  </div>
</section>