<section class="content">
  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="<?php echo current_url(); ?>" method="post">
          <div class="box-body">

            <div class="form-group">
              <label for="categoria_cart_cnpj_credenciadora" class="col-sm-2 control-label">Cnpj Administradora Cartão*</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" name="categoria_cart_cnpj_credenciadora" id="cnpj" value="<?php echo set_value('categoria_cart_cnpj_credenciadora'); ?>" placeholder="Cnpj Administradora">
              </div>
            </div>

            <div class="form-group">
              <label for="categoria_cart_nome_credenciadora" class="col-sm-2 control-label">Nome Administradora Cartão*</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" id="categoria_cart_nome_credenciadora" name="categoria_cart_nome_credenciadora" value="<?php echo set_value('categoria_cart_nome_credenciadora'); ?>" placeholder="Nome Administradora">
              </div>
            </div>            


          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>" class="btn btn-default">Voltar</a>
            <button type="submit" class="btn btn-primary pull-right"><?php echo ucfirst($this->uri->segment(2)); ?></button>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
  </div>
</section>