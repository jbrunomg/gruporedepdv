<section class="content">
  <div class="row">
    <div class="col-md-12">
      <!-- Horizontal Form -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form id="form_categoria_cart" class="form-horizontal" action="<?= current_url(); ?>" method="post">

          <!--    DADOS GERAIS.    -->
          <input disabled type="hidden" class="form-control" name="id" id="id" value="<?= $this->session->userdata('usuario_id'); ?>">
          <input type="hidden" name="categoria_cart_id" name="categoria_cart_id" value="<?= $dados[0]->categoria_cart_id ?>">
          <!--    /DADOS GERAIS.    -->

          <div class="box-body">
            <div class="form-group">
              <label for="categoria_cart_cnpj_credenciadora" class="col-sm-2 control-label">Cnpj Administradora Cartão</label>
              <div class="col-sm-5">
                <input disabled type="text" class="form-control" name="categoria_cart_cnpj_credenciadora" id="cnpj" value="<?= $dados[0]->categoria_cart_cnpj_credenciadora; ?>" placeholder="Cnpj Administradora">
              </div>
            </div>

            <div class="form-group">
              <label for="categoria_cart_nome_credenciadora" class="col-sm-2 control-label">Nome Administradora Cartão</label>
              <div class="col-sm-5">
                <input disabled type="text" class="form-control" id="categoria_cart_nome_credenciadora" name="categoria_cart_nome_credenciadora" value="<?= $dados[0]->categoria_cart_nome_credenciadora; ?>" placeholder="Nome Administradora">
              </div>
            </div>

            <div class="form-group">
              <label for="categoria_cart_descricao" class="col-sm-2 control-label">Bandeira Cartão</label>
              <div class="col-sm-5">
                <input disabled type="text" class="form-control" id="categoria_cart_descricao" name="categoria_cart_descricao" value="<?= $dados[0]->categoria_cart_descricao; ?>" placeholder="Descrição">
              </div>
            </div>

            <div class="form-group">
              <label for="categoria_cart_numero_parcela" class="col-sm-2 control-label">Número de Parcelas</label>
              <div class="col-sm-5">
                <input disabled type="text" class="form-control" id="categoria_cart_numero_parcela" name="categoria_cart_numero_parcela" value="<?= $dados[0]->categoria_cart_numero_parcela; ?>" placeholder="Número de Parcelas">
              </div>
            </div>

            <div class="form-group">
              <label for="categoria_cart_percentual_parcela" class="col-sm-2 control-label">Percentual Parcela %</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="categoria_cart_percentual_parcela" name="categoria_cart_percentual_parcela" value="<?= $dados[0]->categoria_cart_percentual_parcela; ?>" placeholder="Taxa de Juros">
              </div>
            </div>

            <!-- /.box-body -->
            <div class="box-footer">
              <a href="<?= base_url($this->uri->segment(1)); ?>" class="btn btn-default">Voltar</a>
              <button id="btn_form" type="button" class="btn btn-primary pull-right">Alterar</button>
            </div>
            <!-- /.box-footer -->
        </form>
      </div>
    </div>
  </div>
</section>


<!-- jQuery 3 -->
<script src="<?= base_url("assets/bower_components/jquery/dist/jquery.min.js"); ?>"></script>
<script src="<?= base_url("assets/dist/js/jquery.validate.js") ?>"></script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script>
  //    VARIAVEIS.    //
  const ID_USER = $('#id').val();
  const BASE_URL = '<?= base_url(); ?>';
  //    /VARIAVEIS.    //

  //    ELEMENTOS.    //
  const input_percentual = $('#categoria_cart_percentual_parcela');
  const form = $('#form_categoria_cart');
  const btn_form = $('#btn_form');
  //   /ELEMENTOS.    //

  //    EVENTOS.    //
  input_percentual.on('input', function() {
    //    PERMITIR APENAS NÚMEROS E PONTO.    //
    let valorSemNaoNumericos = input_percentual.val().replace(/\D/g, '');

    if (valorSemNaoNumericos === '' || valorSemNaoNumericos === '0') {
      input_percentual.val('');
    } else {
      let numeroFormatado = (parseFloat(valorSemNaoNumericos) / 100).toFixed(2);
      input_percentual.val(numeroFormatado);
    }
  });

  btn_form.on('click', function() {
    form.validate({
      rules: {
        categoria_cart_percentual_parcela: {
          required: true,
          number: true
        }
      },
      messages: {
        categoria_cart_percentual_parcela: {
          required: "Campo obrigatório",
          number: "Apenas números"
        }
      }
    });

    if (!form.valid()) return false;

    let form_data = new FormData();
    let campos = $("#form_categoria_cart input")

    campos.each(function() {
      let valor = $(this).val();
      let nome = $(this).attr('name');
      form_data.append(nome, valor);
    });

    fetch(BASE_URL + 'index.php/categoriacartao/editar', {
        method: 'POST',
        body: form_data,
        headers: {
          'Accept': 'application/json'
        }
      }).then(response => response.json())
      .then(data => {
        location.reload();
      }).catch(error => {
        swal({
          title: "Erro",
          text: "Erro ao alterar o registro, tente novamente mais tarde!",
          icon: "error",
          button: "Ok",
        }).then(() => {
          location.reload();
        });
      });
  });
  //    /EVENTOS.    //
</script>