<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>MEGA | Controler</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css">
   <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/main.css">
  <link href="<?php echo base_url(); ?>assets/dist/css/custom.css" rel="stylesheet" type="text/css" />
  <!-- Autocomplete -->
  <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
  <link href="<?php echo base_url(); ?>/assets/dist/css/jquery-ui.css" rel="stylesheet" type="text/css" />
  <style>
    .ui-front{
      z-index: 1051;
    }
  </style>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!-- ADD THE CLASS sidebar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
<body class="hold-transition skin-<?php echo CLIENTE ?> sidebar-collapse sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url(); ?>sistema/dashboard" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>P</b>DV2</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b>LTE</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li><a href="#" class="filialAtual" style="background:#ff0; color:#fff;"><?php $loja = BDCAMINHO; $loja = explode("_", $loja); echo strtoupper($loja[2]); ?></a> 
          <input type="hidden" id="filialAtual" value="<?php echo BDCAMINHO; ?>"></li>
          <li><a href="#" class="clock"></a></li>
          <li><a href="http://localhost/sistemapdv/"><i class="fa fa-dashboard"></i></a></li>
          <li><a href="http://localhost/sistemapdv/settings"><i class="fa fa-cogs"></i></a></li>
          <li><a href="http://localhost/sistemapdv/pos/view_bill" target="_blank"><i class="fa fa-file-text-o"></i></a></li>
          <li><a href="http://localhost/sistemapdv/pos/register_details" data-toggle="ajax">Histórico de Vendas</a></li>
          <li><a href="http://localhost/sistemapdv/pos/today_sale" data-toggle="ajax">Venda do dia</a></li>
          <li><a href="http://localhost/sistemapdv/pos/close_register" data-toggle="ajax">Fechar Caixa</a></li>

          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bell-o"></i>
                <span class="label label-warning">1</span>
            </a>
            <ul class="dropdown-menu">
                <li class="header">Vendas recentes em aberto</li>
                <li>
                    <ul class="menu">
                        <li>
                        <a href="http://localhost/sistemapdv/pos/?hold=2" class="load_suspended">3030/0303/2016161616 08:11:01 PM (Cliente Padr?o)<br><strong>teste</strong></a>                            </li>
                    </ul>
                </li>
                <li class="footer"><a href="http://localhost/sistemapdv/sales/opened">Ver tudo</a></li>
            </ul>
          </li>

          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url(); ?>assets/dist/img/user2-160x160.jpg"  class="user-image" alt="Avatar" />
              <span class="hidden-xs"><?php echo $this->session->userdata('usuario_nome'); ?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="user-header">
                <img src="<?php echo base_url(); ?>assets/dist/img/user2-160x160.jpg"  class="img-circle" alt="Avatar" />
                <p>
                 <?php echo $this->session->userdata('usuario_nome'); ?> <br> <?php echo $this->session->userdata('usuario_perfil_nome'); ?>
                </p>
              </li>
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat" data-toggle="modal" data-target=".bs-example-modal-sm">Alterar Senha</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo base_url(); ?>sistema/logout" class="btn btn-default btn-flat">Sair</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url(); ?>assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Alexander Pierce</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU NAVEGAÇÃO</li>



        <li><a href="https://adminlte.io/docs"><i class="fa fa-shopping-cart"></i> <span>PDV</span></a></li>
        <li class="header">LABELS</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Sair</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Finalizar Venda</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Iniciar Venda</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
<!--     <section class="content-header">
      <h1>
        Sidebar Collapsed
        <small>Layout with collapsed sidebar on load</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Layout</a></li>
        <li class="active">Collapsed Sidebar</li>
      </ol>
    </section> -->

    <!-- Main content -->
	  <section class="content" id="conteudo" onclick="hideSideBar()">
		  <div class="row">
			  <div class="col-md-12">
				  <div class="box">
				  	<input type="hidden" id="idVenda" name="idVenda" value="<?php echo $this->uri->segment(3) ?>">
					  <div class="info-box p-m">
						  <div class="row">
							  <div class="col-md-4">
								  <div class="form-group">
									  <label>Grupo:</label>									 
									  	<select class="form-control" name="categoria_prod_id" id="grupo_prod">
					                        <option value="">Selecione</option>
					                        <?php foreach ($categorias as $valor) { ?>
					                          <option value='<?php echo $valor->categoria_prod_id; ?>'><?php echo $valor->categoria_prod_descricao; ?> </option>
					                        <?php } ?>
					                    </select>							
				     			  </div>
							  </div>
							  <div class="col-md-7">
								  <div class="form-group">
									  <label>Descrição:</label>
									   <select class="select2" style="width: 100%;" name="produto_desc" id="produto_desc">
				                        <option value="">Selecione um grupo</option>                       
				                       </select>  
								  </div>
							  </div>
							  <div class="col-md-1">
								  <div class="form-group">
									  <label>Estoque:</label>
									  <div class="info-box bg-aqua" style="color: #fff; font-weight: bold; font-size: 16px; text-align: right; padding: 6px; min-height: 100% !important;">
									  	  <span id="estoque">0</span>										  
									  </div>
								  </div>
							  </div>
						  </div>
						  <hr />
						  <div class="row">
							  <div class="col-md-4">
								  <form>
									  <div class="form-group">
										  <label for="quantidade">Quantidade:</label>
										  <input type="text" class="text-right form-control" id="quantidade" placeholder="0,00">
										  <center><p id="aviso_estoque" style="display: none"><font color="red">Produto Sem Estoque !</font></p></center>
									  </div>
									  <div class="form-group">
										  <label for="valor-unit">Valor unitário:</label>
										  <input onblur="verifyValue()" type="text" class="text-right form-control money" id="valor_unit" value="" name="valor_unit" placeholder="R$0,00">
										  <div class="row">
											  <div class="col-sm-4" onclick="applyPVM()" style="cursor: pointer">
												  <span>PVM: R$ <span id="pvm">35.00</span></span>
											  </div>
											  <div class="col-sm-4" onclick="applyPCD()" style="cursor: pointer">
												  <span>PCD: R$ <span id="pcd">55.00</span></span>
											  </div>
											  <div class="col-sm-4" onclick="applyPCC()" style="cursor: pointer">
												  <span>PCC: R$ <span id="pcc">60.00</span></span>
											  </div>
										  </div>
									  </div>
									  <div class="form-group">
										  <label for="total-item">Total Item:</label>
										  <input type="text" class="text-right form-control" id="total-item" placeholder="R$0,00">
									  </div>

									   <?php 
										  $total = 0;
										  foreach ($produtos as $p) { 
										  $total += $p['subTotal'] ?>
										<?php } ?>
									  <div class="form-group">
										  <label for="total-venda">Total Venda:</label>
										  <input disabled type="text" class="custon-input-venda text-right form-control" id="total-venda" value="<?php echo number_format ($total, 2, '.', '');  ?>" placeholder="R$0,00">
									  </div>

									  <div id="venda-iniciada" class="animated fadeInUp bg-blue info-venda-iniciada">INICIADA</div>
								  </form>
							  </div>
							  <div class="col-md-8 mt-s">
								  <div class="box-table">
									  <table class="table table-striped">
										 <thead>
											 <tr>
												 <th style="width: 10px">N</th>
												 <th>Grupo</th>
												 <th>Código</th>
												 <th width="40%">Descrição</th>
												 <th>QTD</th>
												 <th>Valor</th>
												 <th>Total Item</th>
											 </tr>
										 </thead>
										  <tbody>
										  <?php  $this->idItens = 0; $contador = 1; foreach ($produtos as $p) { ?>
										  	<tr>
												<td bgcolor="<?php echo $p['cor']?>"><font color="#fff"><?php echo $contador?></font></td>
												<td bgcolor="<?php echo $p['cor']?>"><font color="#fff"><?php echo $p['categoria_prod_descricao']?></font></td>
												<td bgcolor="<?php echo $p['cor']?>"><font color="#fff"><?php echo $p['produto_codigo']?></font></td>
												<td bgcolor="<?php echo $p['cor']?>"><font color="#fff"><?php echo $p['produto_descricao']?></font></td>
												<td bgcolor="<?php echo $p['cor']?>"><font color="#fff"><?php echo $p['quantidade']?></font></td>
												<td bgcolor="<?php echo $p['cor']?>"><font color="#fff"><?php echo $p['valor_unit']?></font></td>
												<td bgcolor="<?php echo $p['cor']?>"><font color="#fff"><?php echo $p['subTotal']?></font></td>
											</tr>
										   <?php $this->idItens = $p['idItens']; $contador++; } ?>
										  </tbody>
									  </table>
								  </div>
								  <div class="box-qtd">
									 <span class="text-qtd">QTDE ITENS: <?php echo count($produtos)?></span>
								  </div>
							  </div>
						  </div>
						  <hr />
						  <div class="row">
                  <input type="hidden" name="idItens" id="idItens" value="<?php echo $this->idItens;  ?>">
							<div class="col-md-12">
								<div class="box-btns">
									<button disabled id="btn-iniciar-venda" class="btn-custon btn btn-primary btn-flat">
										<span>F2 - Iniciar venda &nbsp; <i class="fa fa-plus-square"></i></span>
									</button>
                  <button onclick="excluirItens()" id="btn-excluir-item" class="btn-custon btn btn-danger btn-flat">
                    <span>F3 - Excluir Item &nbsp; <i class="fa fa-trash-o"></i></span>
                  </button>                 
									<button onclick="cancelarVenda()" id="btn-cancelar-venda" class="btn-custon btn btn-info btn-flat">
										<span>F4 - Cancelar venda &nbsp; <i class="fa fa-undo"></i></span>
									</button>
									<button onclick="finalizarVenda()" id="btn-finalizar-venda" class="btn-custon btn btn-warning btn-flat">
										<span>F5 - Finalizar venda &nbsp; <i class="fa fa-save"></i></span>
									</button>
									<button id="btn-sair" class="btn-custon btn btn-danger btn-flat">
										<span>F10 - Sair &nbsp; <i class="fa fa-sign-out"></i></span>
									</button>
								</div>
							</div>
						  </div>
					  </div>
				  </div>
			  </div>
		  </div>
	  </section>
  </div>
  <!-- /.content-wrapper -->



 <!--  MODAL 	-->

	<div class="modal fade" id="modal-default" style="">
		<div class="modal-dialog" style="width: 350px; margin: 100px auto">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" onclick="closeModal()" aria-label="Close">
						<span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title text-center">Informar senha</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<p>Informe a senha do gerente para liberação desta ação</p>
							<div class="form-group">
								<input onkeyup="habilitarBTN(this.value)" id="input-liberar-senha" class="form-control" placeholder="Senha do gerente" />
							</div>
							<button id="btn-confirm" disabled onclick="applyValor()" class="btn btn-primary btn-block">CONFIRMAR</button>
						</div>
					</div>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>


  <!-- /.content-wrapper -->
 <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2019- <?php echo date('Y');?> <a href="www.wdmtecnologia.com.br">WDM-Tecnologia</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark" id="control-sidebar">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-theme-demo-options-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <!-- <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li> -->
      <!-- <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li> -->
    </ul>
    <!-- Tab panes -->
      <div class="tab-content">
      <div id="control-sidebar-theme-demo-options-tab" class="tab-pane active">
        <div>
          <h3 class="control-sidebar-heading">Filias</h3>
             <div class="tab-pane active" id="control-sidebar-home-tab">
          <ul class="control-sidebar-menu">
            <?php
            foreach($filias as $fili) {
              $f = $fili;
              $filias = explode("_megacell_", $f);

              echo '<li><a href="#" class="filias'.($fili == BDCAMINHO ? ' active' : '').'" id="'.$fili.'">';
              if($filias[1] == 'matriz') {
                echo '<div class="menu-icon"><img src="'.base_url('assets/img/matriz.png').'" alt="" class="img-thumbnail img-circle img-responsive"></div>';
              } elseif($filias[1] == 'game') {
                echo '<div class="menu-icon"><img src="'.base_url('assets/img/game.png').'" alt="" class="img-thumbnail img-circle img-responsive"></div>';
              } elseif($filias[1] == 'carlos') {
                echo '<div class="menu-icon"><img src="'.base_url('assets/img/carlos.png').'" alt="" class="img-thumbnail img-circle img-responsive"></div>';
              } elseif($filias[1] == 'mega') {
                echo '<div class="menu-icon"><img src="'.base_url('assets/img/mega.png').'" alt="" class="img-thumbnail img-circle img-responsive"></div>';
              } elseif($filias[1] == 'paloma') {
                echo '<div class="menu-icon"><img src="'.base_url('assets/img/paloma.png').'" alt="" class="img-thumbnail img-circle img-responsive"></div>';
              } else {
                echo '<i class="menu-icon fa fa-folder-open bg-red"></i>';
              }
              echo '<div class="menu-info"><h4 class="control-sidebar-subheading">'.$filias[1].'</h4></div>
            </a></li>';
          }
          ?>
        </ul>
      </div>
        </div>
      </div>
<!--       <div class="tab-pane" id="control-sidebar-settings-tab">
          <h3 class="control-sidebar-heading">Categorias</h3>
          <div class="tab-pane active" id="control-sidebar-home-tab">
          <ul class="control-sidebar-menu" id='categorias_prod_filias'>
            <?php
            foreach($categorias as $category) {
              $filias = explode("_megacell_", BDCAMINHO);
              echo '<li><a href="#" class="category'.($category->categoria_prod_id == '1' ? ' active' : '').'" id="'.$category->categoria_prod_id.'">';
              if($filias[1] == 'matriz') {
                echo '<div class="menu-icon"><img src="'.base_url('assets/img/matriz.png').'" alt="" class="img-thumbnail img-circle img-responsive"></div>';
              } elseif($filias[1] == 'game') {
                echo '<div class="menu-icon"><img src="'.base_url('assets/img/game.png').'" alt="" class="img-thumbnail img-circle img-responsive"></div>';
              } elseif($filias[1] == 'carlos') {
                echo '<div class="menu-icon"><img src="'.base_url('assets/img/carlos.png').'" alt="" class="img-thumbnail img-circle img-responsive"></div>';
              } elseif($filias[1] == 'mega') {
                echo '<div class="menu-icon"><img src="'.base_url('assets/img/mega.png').'" alt="" class="img-thumbnail img-circle img-responsive"></div>';
              } elseif($filias[1] == 'paloma') {
                echo '<div class="menu-icon"><img src="'.base_url('assets/img/paloma.png').'" alt="" class="img-thumbnail img-circle img-responsive"></div>';
              } else {
                echo '<i class="menu-icon fa fa-folder-open bg-red"></i>';
              }
              echo '<div class="menu-info"><h4 class="control-sidebar-subheading">'.$category->categoria_prod_id.'</h4><p><strong>'.$category->categoria_prod_descricao.'</strong></p></div>
            </a></li>';
          }
          ?>
        </ul>
      </div>
      </div> -->
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button> 
        <h4 class="modal-title" id="mySmallModalLabel">Alterar Senha</h4> 
      </div> 
      <div class="modal-body">
          <form class="form-horizontal">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="">Senha Atual</label>                  
                    <input class="form-control" id="senha_atual"  type="password">
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="">Nova Senha</label>                  
                    <input class="form-control" id="nova_senha"  type="password">
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="">Confirma Senha</label>                  
                    <input class="form-control" id="confirma_senha"  type="password">
                </div>                
              </div>
              <span class="msg-error text-danger"></span>
              <span class="msg-success text-success"></span>
              <!-- /.box-body -->
              <div class="box-footer">
                <span id="alterarSenha" class="btn btn-primary pull-right">Alterar Senha</span>
              </div>
              <!-- /.box-footer -->
            </form>
      </div> 
    </div>
  </div>
</div>

<script type="text/javascript">  
var base_url = "<?php echo base_url(); ?>";
</script>

<!-- jQuery 3 -->
<!-- <script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script> --><!-- Buga o Autocomplete -->
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url(); ?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>assets/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/dist/js/adminlte.min.js"></script>
<!--Date picker-->
<script src="<?php echo base_url(); ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

<script src="<?php echo base_url(); ?>/assets/dist/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>
<script src="<?php echo base_url(); ?>assets/dist/js/actions-pdv2.js"></script>
<script src="<?php echo base_url(); ?>assets/dist/js/maskmoney.js"></script>

<script type="text/javascript">

	$('#datepicker').datepicker({
        autoclose: true
    });

    $(document).ready(function(){

	   $(".money").maskMoney();
	}); 

    $(function () {
     //Initialize Select2 Elements
     $('.select2').select2()
	 // $('#control-sidebar').addClass('control-sidebar-open')
	 // $('#control-sidebar').onBlur().removeClass('control-sidebar-open')
    });

    function hideSideBar() {
        $('#control-sidebar').removeClass('control-sidebar-open')
	}


  $('#grupo_prod').focus().select()

	function applyPVM() {
        setInputValue('pvm')
	}

    function applyPCD() {
        setInputValue('pcd')
    }

    function applyPCC() {
        setInputValue('pcc')
    }

    function setInputValue(el) {
        const element = document.querySelector(`#${el}`)
        const valor = element.innerHTML
        const input = document.querySelector('#valor_unit')
        input.value = valor
    }
    
    function verifyValue() {
        const element = document.querySelector('#pvm')
        const valor = Number(element.innerHTML)
        const input = document.querySelector('#valor_unit')
		const valorInput = Number(input.value)

		if(valorInput > 0 && (valorInput < valor)){
            $("#modal-default").modal()
		}
    }
    
    function applyValor() {
        $('#modal-default').modal('hide');
        document.querySelector('#input-liberar-senha').value = null;
        const el = document.querySelector('#btn-confirm')
        el.disabled = true
    }

    function habilitarBTN(valor) {
		const el = document.querySelector('#btn-confirm')
		if(valor.length > 0) {
		    el.disabled = false
		} else {
            el.disabled = true
		}
    }

    function closeModal() {
        $('#modal-default').modal('hide');
        const input = document.querySelector('#valor_unit')
		document.querySelector('#input-liberar-senha').value = null;
        const el = document.querySelector('#btn-confirm')
        el.disabled = true
        input.value = null;
    }

</script>  


</body>
</html>
