<section class="content">
  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="" method="post">                
          <div class="box-body">

            <div class="row">
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 17%</span>
                    <h5 class="description-header"><?php echo $dados[0]->categoria_prod_descricao ?></h5>
                    <span class="description-text">GRUPO/CATEGORIA</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <span class="description-percentage text-yellow"><i class="fa fa-caret-left"></i> 0%</span>
                    <h5 class="description-header"><?php echo date('d/m/Y', strtotime($dados[0]->produto_preco_ajuste_data)); ?></h5>
                    <span class="description-text">DATA-GERAÇÃO</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 20%</span>
                    <h5 class="description-header"><?php echo $dados[0]->produto_preco_ajuste_observacao ?></h5>
                    <span class="description-text">OBSERVAÇÃO</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block">
                    <span class="description-percentage text-red"><i class="fa fa-caret-down"></i> 18%</span>
                    <h5 class="description-header">1200</h5>
                    <span class="description-text">ATUALIZADO</span>
                  </div>
                  <!-- /.description-block -->
                </div>
              </div>
<!--             <div class="form-group">
              <label for="estoque_ajuste_data" class="col-sm-2 control-label">Data Geração</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" name="estoque_ajuste_data" id="data" value="<?php echo date('d/m/Y', strtotime($dados[0]->estoque_ajuste_data)); ?>" placeholder="">
              </div>
            </div>
            
            <div class="form-group">
              <label for="estoque_ajuste_observacao" class="col-sm-2 control-label">Documento </label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" name="estoque_ajuste_observacao" id="estoque_ajuste_observacao" value="<?php echo $dados[0]->estoque_ajuste_observacao ?>" placeholder="Observacao">
              </div>
            </div> --> 

          </div>

        </form>

        <fieldset class="col-md-12">
            <legend>Dados dos Produtos</legend>
            <div class="col-md-12">

            </div>
          </fieldset> 
          <input type="hidden" id="ajusteDebito" class="form-control" value="<?php echo $parametros[0]->parametro_cart_debito ?>">
          <input type="hidden" id="ajusteCredto" class="form-control" value="<?php echo $parametros[0]->parametro_cart_credito ?>">


          <div class="box-footer">
                        
              <!-- /.box-header -->
              <div id="divProdutos" class="box-body no-padding">
                <table id="examplee" class="table" width="100%">
                  <thead>
                    <tr>
                      <!-- <th>#</th> -->
                      <th>U. Atualização</th>
                      <th>Produto</th>
                      <th>Preço Custo</th>
                      <th>Preço Minimo</th>
                      <th>Preço Atual</th>
                      <th>Preço</th> 
                      <th>Cartão Debíto</th> 
                      <th>Cartão Credito</th>              
                    </tr>
                  </thead>  
                  <tbody>
                    <?php  

                    foreach ($dados as $p) {        
                       
                        echo '<tr>';
                        // echo '<td>'.$p->produtos_id.'</td>';
                        echo '<td>'.date('d/m/Y', strtotime($p->itens_produto_preco_data_atualizacao)).'</td>';
                        echo '<td>'.$p->produto_descricao.'</td>';
                        echo '<td>'.'R$ '.$p->produto_preco_custo.'</td>';
                        echo '<td> <input style="border:0;" placeholder="0.00" onblur="ajustarPreçoPMV(this)" class="precomin"  id="'.$p->produtos_id.'" type="text" class="money" name="campoPMV'.$p->produtos_id.'" value="'.$p->produto_preco_minimo_venda.'" idAjuste="'.$this->uri->segment(3).'"/></td>';   
                        // echo '<td>'.'R$ '.$p->produto_preco_minimo_venda.'</td>';
                        echo '<td>'.'R$ '.$p->produto_preco_venda.'</td>';
                        echo '<td> <input style="border:0;" placeholder="0.00" onblur="ajustarPreço(this)" class="precovenda"  id="'.$p->produtos_id.'" type="text" class="money" name="campo'.$p->produtos_id.'" value="'.$p->produto_preco_venda.'" idAjuste="'.$this->uri->segment(3).'"/></td>';
                        echo '<td> <input style="border:0;" placeholder="0.00" onblur="ajustarPreçoCD(this)" class="money"  id="'.$p->produtos_id.'" type="text" disabled="disabled" class="money" name="campoCD'.$p->produtos_id.'" value="'.$p->produto_preco_cart_debito.'" idAjuste="'.$this->uri->segment(3).'"/></td>';
                        echo '<td> <input style="border:0;" placeholder="0.00" onblur="ajustarPreçoCC(this)" class="money"  id="'.$p->produtos_id.'" type="text" disabled="disabled" class="money" name="campoCC'.$p->produtos_id.'" value="'.$p->produto_preco_cart_credito.'" idAjuste="'.$this->uri->segment(3).'"/></td>';                 
                    
                        echo '</tr>';
                    }?>
                                         

                  </tbody>
                  <tfoot>
                    <tr>                     
                      <th>ITENS:<?php echo ' '.count($dados); ?></th>     
                      
                      <th></th>                      
                    </tr>
                  </tfoot>
                </table>
              </div>            

          </div> 

          <!-- /.box-body -->
        <!--   <div class="box-footer">
            <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>" class="btn btn-default">Voltar</a>
            <button type="submit" class="btn btn-primary pull-right"><?php echo ucfirst($this->uri->segment(2)); ?></button>
          </div> -->
          <!-- /.box-footer -->


      </div>
    </div>
  </div>
</section>


<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <form action="<?php echo base_url() ?>index.php/produtos/excluir" method="post" >
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h5 id="myModalLabel">Excluir Produto</h5>
  </div>
  <div class="modal-body">
    <input type="hidden" id="idProduto" name="id" value="" />
    <h5 style="text-align: center">Deseja realmente excluir este produto?</h5>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
    <button class="btn btn-danger">Excluir</button>
  </div>
  </form>
</div>

  <div class="modal fade" id="modal-default">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Default Modal</h4>
        </div>
        <div class="modal-body">
          <p>One fine body&hellip;</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->


<script type="text/javascript">
  
  $('.precovenda').click(function(event) {
     $(this).select();
  });


  $('.precomin').click(function(event) {
     $(this).select();
  });

</script>


<script type="text/javascript">
  // Basic example
$(document).ready(function () {
$('#examplee').DataTable({
        "paging":   false,
        "ordering": false,
        "info":     false
});

});


</script>