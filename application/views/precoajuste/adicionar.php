<section class="content">
  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="<?php echo current_url(); ?>" method="post">
          <div class="box-body">

                <div class="form-group">
                  <label for="produto_preco_ajuste_categoria_id" class="col-sm-2 control-label">Grupo Produto</label>
                  <div class="col-sm-5">            
                    <select id="produto_preco_ajuste_categoria_id" class="form-control" data-placeholder="Selecione o grupo de produtos" name="produto_preco_ajuste_categoria_id">
                      <option value="">Selecione</option>
                      <?php foreach ($grupo as $g) { ?>
                      <option value='<?php echo $g->categoria_prod_id; ?>'><?php echo $g->categoria_prod_descricao; ?> </option>
                      <?php } ?>                       
                    </select>  
                  </div>
                </div>
       
                <div class="form-group">
                  <label for="produto_preco_ajuste_observacao" class="col-sm-2 control-label">Observação </label>
                  <div class="col-sm-5">              
                    <input type="text" class="form-control" name="produto_preco_ajuste_observacao" id="produto_preco_ajuste_observacao" value="<?php echo set_value('produto_preco_ajuste_observacao'); ?>" placeholder="Observacao">
                  </div>
                </div> 
  
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>" class="btn btn-default">Voltar</a>
            <button type="submit" class="btn btn-primary pull-right"><?php echo ucfirst($this->uri->segment(2)); ?></button>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
  </div>
</section>


<script type="text/javascript">

  $(function () {


    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })

  })


</script>