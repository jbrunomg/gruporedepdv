    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/dashboard.css">

      <!-- Main content -->
    <section class="content">
           <!-- Small boxes (Stat box) -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
        <div class="col-md-12">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">          
              <h2 style="font-size: 27px;">Receita Bruta - Geral</h2> 
              <p>Dia - R$ <?php echo $receitaBrutoDia; ?></p>
              <p>Mês - R$ <?php echo $receitaBrutoMes; ?></p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->        
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h2 style="font-size: 27px;">Custo - Mercadoria</h2> 
              <p>Dia - R$ <?php echo $custoMercadoriaDia; ?></p>
              <p>Mês - R$ <?php echo $custoMercadoriaMes; ?></p>
            </div>
             <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">           
              <h2 style="font-size: 27px;">Lucro - Geral</h2>
              <p>Dia - R$ <?php echo $lucroGeralDia; ?></p>
              <p>Mês - R$ <?php echo $lucroGeralMes; ?></p>           
            </div>
             <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
          </div>
        </div>
       
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h2 style="font-size: 27px;">Despesa - Geral</h2>
              <p>Dia - R$ <?php echo $despesaGeralDia; ?></p>
              <p>Mês - R$ <?php echo $despesaGeralMes; ?></p>
            </div>
             <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h2 style="font-size: 27px;">Lucro - Liquido</h2>
              <p>Dia - R$ <?php echo $lucroLiquidoDia; ?></p>
              <p>Mês - R$ <?php echo $lucroLiquidoMes; ?></p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
          </div>
        </div>

       </div> 
        </div>
        <!-- /.box-body -->
      </div>


      <div class="row">
        <div class="col-xs-12">
          <div class="box box-solid collapsed-box">
            <div class="box-header">
              <i class="fa fa-bar-chart-o"></i>

              <h3 class="box-title">Vendas</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" id="dataVenda" data-widget="collapse"><i id="iconVenda" class="fa fa-plus"></i>
                </button>
                <button type="button" class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

            <div class="col-xs-12">

               <div id="load" class="col-xs-6 col-md-12 text-center"></div>

               <div id="vendasAno" class="col-md-4 text-center"></div>

               <div id="vendasMes" class="col-md-4 text-center"></div>

               <div id="vendasDia" class="col-md-4 text-center"></div>

             </div>


            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <div class="col-xs-12">
          <div class="box box-solid collapsed-box">
            <div class="box-header">
              <i class="fa fa-bar-chart-o"></i>

              <h3 class="box-title">Produto - Valor Estoque</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" id="dataProdutoVP" data-widget="collapse"><i id="iconProdutoVP" class="fa fa-plus"></i>
                </button>
                <button type="button" class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

              <div class="col-md-12">

                   <div id="loadProdutoVP" class="col-md-12 text-center"></div>

                   <div id="produtoVP"></div>

              </div>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <div class="col-xs-12">
          <div class="box box-solid collapsed-box">
            <div class="box-header">
              <i class="fa fa-bar-chart-o"></i>

              <h3 class="box-title">Produto - Mais Vendidos</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" id="dataProdutoMV" data-widget="collapse"><i id="iconProdutoMV" class="fa fa-plus"></i>
                </button>
                <button type="button" class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

            <div class="col-xs-12">

               <div id="loadProdutoMV" class="col-xs-6 col-md-12 text-center"></div>

               <div id="produtoMVSemestre" class="col-md-4 text-center"></div>

               <div id="produtoMVTrimestre" class="col-md-4 text-center"></div>

               <div id="produtoMVMes" class="col-md-4 text-center"></div>

             </div>


            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <div class="col-xs-12">
          <div class="box box-solid collapsed-box">
            <div class="box-header">
              <i class="fa fa-bar-chart-o"></i>

              <h3 class="box-title">Produto - Parado Estoque</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" id="dataProdutoPE" data-widget="collapse"><i id="iconProdutoPE" class="fa fa-plus"></i>
                </button>
                <button type="button" class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

            <div class="col-xs-12">

               <div id="loadProdutoPE" class="col-xs-6 col-md-12 text-center"></div>

               <div id="produtoPE180Dias" class="col-md-4 text-center"></div>

               <div id="produtoPE90Dias" class="col-md-4 text-center"></div>

               <div id="produtoPE30Dias" class="col-md-4 text-center"></div>

             </div>


            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <div class="col-xs-12">
          <div class="box box-solid collapsed-box">
            <div class="box-header">
              <i class="fa fa-bar-chart-o"></i>

              <h3 class="box-title">Financeiro</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" id="dataFinanceiro" data-widget="collapse"><i id="iconFinanceiro" class="fa fa-plus"></i>
                </button>
                <button type="button" class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

            <div class="col-xs-12">

               <div id="loadFinanceiro" class="col-xs-6 col-md-12 text-center"></div>

               <div id="financeiroAno" class="col-xs-6 col-md-6 text-center"></div>

               <div id="financeiroMes" class="col-xs-6 col-md-6 text-center"></div>

             </div>


            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <div class="col-xs-12">
          <div class="box box-solid collapsed-box">
            <div class="box-header">
              <i class="fa fa-bar-chart-o"></i>

              <h3 class="box-title">Clientes</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" id="dataCliente" data-widget="collapse"><i id="iconCliente" class="fa fa-plus"></i>
                </button>
                <button type="button" class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

            <div class="col-xs-12">

               <div id="loadCliente" class="col-xs-6 col-md-12 text-center"></div>

               <div id="clienteAno" class="col-xs-6 col-md-6 text-center"></div>

               <div id="clienteMes" class="col-xs-6 col-md-6 text-center"></div>

             </div>


            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <div class="col-xs-12">
          <div class="box box-solid collapsed-box">
            <div class="box-header">
              <i class="fa fa-bar-chart-o"></i>

              <h3 class="box-title">Vendedores</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" id="dataVendedor" data-widget="collapse"><i id="iconVendedor" class="fa fa-plus"></i>
                </button>
                <button type="button" class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

            <div class="col-xs-12">

               <div id="loadVendedor" class="col-xs-6 col-md-12 text-center"></div>

               <div id="vendedorAno" class="col-md-4 text-center"></div>

               <div id="vendedorMes" class="col-md-4 text-center"></div>

               <div id="vendedorDia" class="col-md-4 text-center"></div>

             </div>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->


</section>


<div class="modal fade" id="modal-vendas">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalVendaLoja">Default Modal</h4>
      </div>
      <div id="loadModal" class="text-center"><h3>Carregando...</h3></div>
      <div class="modal-body" id="modalBodyVendas">
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modal-produtoVP">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalProdutoVPLoja">Default Modal</h4>
      </div>
      <div id="loadModalProdutoVP" class="text-center"><h3>Carregando...</h3></div>
      <div class="modal-body" id="modalBodyProdutoVP">
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modal-produtoMV">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalProdutoMVLoja">Default Modal</h4>
      </div>
      <div id="loadModalProdutoMV" class="text-center"><h3>Carregando...</h3></div>
      <div class="modal-body" id="modalBodyProdutoMV">
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modal-produtoPE">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalProdutoPELoja">Default Modal</h4>
      </div>
      <div id="loadModalProdutoPE" class="text-center"><h3>Carregando...</h3></div>
      <div class="modal-body" id="modalBodyProdutoPE">
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modal-financeiro">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalFinanceiroLoja">Default Modal</h4>
      </div>
      <div id="loadModalFinanceiro" class="text-center"><h3>Carregando...</h3></div>
      <div class="modal-body" id="modalBodyFinanceiro">
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modal-cliente">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalClienteLoja">Default Modal</h4>
      </div>
      <div id="loadModalCliente" class="text-center"><h3>Carregando...</h3></div>
      <div class="modal-body" id="modalBodyCliente">
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modal-vendedor">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalVendedorLoja">Default Modal</h4>
      </div>
      <div id="loadModalVendedor" class="text-center"><h3>Carregando...</h3></div>
      <div class="modal-body" id="modalBodyVendedor">
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->



<script src="<?php echo base_url(); ?>assets/dist/js/dashboard.js"></script>
<!-- jQuery Knob -->
<script src="<?php echo base_url(); ?>assets/bower_components/jquery-knob/js/jquery.knob.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url(); ?>assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- page script -->
<script>

  $(function() {
    setTimeout(function(){ 
      window.location.reload(1);
    }, 120000);
  });


  $(function () {
    /* jQueryKnob */

    $(".knob").knob({
      draw: function () {

        // "tron" case
        if (this.$.data('skin') == 'tron') {

          var a = this.angle(this.cv)  // Angle
              , sa = this.startAngle          // Previous start angle
              , sat = this.startAngle         // Start angle
              , ea                            // Previous end angle
              , eat = sat + a                 // End angle
              , r = true;

          this.g.lineWidth = this.lineWidth;

          this.o.cursor
          && (sat = eat - 0.3)
          && (eat = eat + 0.3);

          if (this.o.displayPrevious) {
            ea = this.startAngle + this.angle(this.value);
            this.o.cursor
            && (sa = ea - 0.3)
            && (ea = ea + 0.3);
            this.g.beginPath();
            this.g.strokeStyle = this.previousColor;
            this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);
            this.g.stroke();
          }

          this.g.beginPath();
          this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
          this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);
          this.g.stroke();

          this.g.lineWidth = 2;
          this.g.beginPath();
          this.g.strokeStyle = this.o.fgColor;
          this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
          this.g.stroke();

          return false;
        }
      }
    });
    /* END JQUERY KNOB */

    //INITIALIZE SPARKLINE CHARTS
    $(".sparkline").each(function () {
      var $this = $(this);
      $this.sparkline('html', $this.data());
    });

    /* SPARKLINE DOCUMENTATION EXAMPLES http://omnipotent.net/jquery.sparkline/#s-about */
    drawDocSparklines();

  });
  function drawDocSparklines() {
    // mouseover event demo sparkline
    $('.mouseoverdemo').sparkline();
    $('.mouseoverdemo').bind('sparklineRegionChange', function (ev) {
      var sparkline = ev.sparklines[0],
          region = sparkline.getCurrentRegionFields();
      value = region.y;
      $('.mouseoverregion').text("x=" + region.x + " y=" + region.y);
    }).bind('mouseleave', function () {
      $('.mouseoverregion').text('');
    });
  }

</script>








