

  <div id="head" style="padding-top: 9px; display: table;">

      <h3 style="width:200px; float:left;">#DEVOLUÇÃO:<?php echo $dados[0]->idDevolucao ?></h3>
      <p style="font-size: 12px; display: block; margin: 4px; text-align: right;">Data da Devolução - <?php echo date('d/m/Y', strtotime($dados[0]->dataDevolucao)); ?></p>
      <p style="font-size: 12px; display: block; margin: 4px; text-align: right;">Fornecedor - <?php echo strtoupper($dados[0]->fornecedor_nome); ?></p>      
      <p style="font-size: 12px; display: block; margin: 4px; text-align: right;">Documento - <?php echo $dados[0]->observacao ?></p>
      
      <br class="clear">
  </div>


            <table>
                <thead>
                    <tr>
                      <th style="font-size: 12px;"  class="text-center">Cód.</th>
                      <th style="font-size: 12px;"  class="text-center">Produto</th>
                      <th style="font-size: 12px;"  class="text-center">Quantidade</th>                    
                      <th style="font-size: 12px;"  class="text-center">Custo</th>   
                      <th style="font-size: 12px;"  class="text-center">Total</th>     
                    </tr>
                </thead>

                <tbody>

                    <?php
                      $total = 0;
                      $qtd = 0;
                      $totalDocumento = 0;
                                             
                     
                    foreach ($dados as $p) {

                      $total_item = number_format(($p->quantidadee * $p->prod_preco_custo), 2, '.', '');

                      $qtd = $qtd + $p->quantidadee;    

                      $totalDocumento = $totalDocumento + $total_item;

                      ?>

                        <tr class="black">
                          <td class="black"> <?php echo $p->produto_codigo; ?></td>
                          <td class="black"> <?php echo $p->produto_descricao; ?></td>
                          <td class="black"> <?php echo $p->quantidadee; ?></td>
                          <td class="black"> <?php echo $p->prod_preco_custo; ?></td>
                          <td class="black"> <?php echo 'R$ '.$total_item; ?> </td>  
                        </tr>

                      <?php } ?>
                  </tbody>
                 
                  <tfooter>      

                   <tr class="black">                    
                      <td>ITENS: <?php echo ' '.count($dados); ?></td>
                      <td></td>                      
                      <td>VOLUME: <?php echo ' '.$qtd; ?></td>
                      <td></td>
                      <td>VALOR TOTAL: <?php echo number_format($totalDocumento, 2, '.', ''); ?></td>       
                    </tr>
                  <tfooter>  
                    
                </table>   




