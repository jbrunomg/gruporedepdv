
 

<input id="devId" type="hidden" value="<?php echo $dados[0]->idDevolucao ?>">
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <!-- Horizontal Form -->
       <div class="box box-primary collapsed-box">
           <div class="box-header with-border">
            <h3>#DEVOLUÇÃO:<?php echo $dados[0]->idDevolucao ?></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
            </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="" method="post">
          <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $dados[0]->idDevolucao; ?>" placeholder="id">        
          <div class="box-body">
            <div class="form-group">
              <label for="dataDevolucao" class="col-sm-2 control-label">Data da Entrada/Saída </label>
              <div class="col-sm-2">              
                <input type="text" class="form-control" name="dataDevolucao" id="data" value="<?php echo date('d/m/Y', strtotime($dados[0]->dataDevolucao)); ?>" disabled>
              </div>

           
              <label for="fornecedor_nome" class="col-sm-2 control-label">Fonecedor</label>
              <div class="col-sm-2">              
                <input type="text" class="form-control" name="fornecedor_nome" id="fornecedor_nome" value="<?php echo $dados[0]->fornecedor_nome ?>" disabled>
              </div>
   
           
            </div>

            <div class="form-group">
              <label for="observacao" class="col-sm-2 control-label">Observação: </label>
              <div class="col-sm-2">              
                <input type="text" class="form-control" name="observacao" id="observacao" value="<?php echo $dados[0]->observacao ?>" disabled>
              </div>

         
            </div> 

            <div class="span12" style="padding: 1%; margin-left: 0">                
                <a href="<?php echo base_url() ?>index.php/devolucao" class="btn btn-default"><i class="icon-arrow-left"></i> Voltar</a>
              </div>
            </div> 



          </div>

        </form>


        <div class="box-footer">

          <div class="box-header with-border">
            <h3>Dados dos Produtos</h3>
              <div class="box-tools pull-right">  
              <a href="<?php echo base_url().'devolucao/devolucaoExcel/'.$this->uri->segment(3); ?>" class="btn btn-success pull-right"><i class="glyphicon  glyphicon-download icon-white"></i> Baixar Excel</a>
                <a title="Imprimir" target="_blank" class="btn btn-mini btn-default" href="<?php echo base_url().'devolucao/imprimirDevolucao/'.$this->uri->segment(3); ?>"><i class="glyphicon glyphicon-print icon-white"></i> Imprimir</a>
              </div>
          </div>

          <!-- /.box-header -->
          <div id="divProdutos" class="box-body">
            <form id="formProdutos" action="<?php echo base_url().'devolucao/entradaProdutos/'.$this->uri->segment(3); ?>" method="post">
              <table class="table table-condensed no-padding" id="tableProdutos"  style="width: 100%;">
                <thead>
                  <tr>
                    <th class="text-center">Cód.</th>
                    <th class="text-center" width="20%">Produto</th>
                    <th class="text-center">Quantidade</th>                    
                    <th class="text-center">Custo</th>       
                    <th class="text-center">Total</th>                      
                  </tr>
                </thead>  
                <tbody>
                  <?php
                  $total = 0;
                  $qtd = 0;
                  $seq = 1;
                  $totalDocumento = 0;
                  $total_item = 0;


                  // echo '<pre>'; var_dump($dados); die();

                  foreach ($dados as $p) { 

                  $total_item = number_format(($p->quantidadee * $p->prod_preco_custo), 2, '.', '');

                  $qtd = $qtd + $p->quantidadee;    

                  $totalDocumento = $totalDocumento + $total_item;
   
                  ?>

                    <tr>
                      <td class="text-center"> <?php echo $p->produto_codigo; ?> </td>
                      <td class="text-center"> <?php echo $p->produto_descricao; ?> </td>
                      <td class="text-center"> <?php echo $p->quantidadee; ?> </td>
                      <td class="text-center"> <?php echo $p->prod_preco_custo; ?> </td>
                      <td class="text-center"> <?php echo $total_item; ?> </td>                   
                 
                    </tr>

                    <?php } ?>


                  </tbody>

                  <tfoot>
                    <tr>                     
                      <th>ITENS: <?php echo ' '.count($dados); ?></th>
                      <th></th>                      
                      <th>VOLUME: <?php echo ' '.$qtd; ?></th>
                      <th></th>
                      <th>VALOR TOTAL:  <?php echo number_format($totalDocumento, 2, '.', ''); ?></th>
                      <th></th>                      
                    </tr>
                  </tfoot>
                </table>
              </form>
            </div>            
          </div> 
        </div>
      </div>
    </div>
  </section>

