<br><br><section class="content">
  <div class="box">
    <div class="box-header with-border">
      <div class="col-md-2">
        <?php if(verificarPermissao('aAlmoxarifado')){ ?>
        <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/adicionar" type="button" class="btn btn-block btn-default btn-flat">Adicionar</a>
        <?php } ?>
      </div>          
    </div>
    <div class="box-body">
      <div class="container-fluid">
        <table id="example" class="table" width="100%">
        <thead>
          <tr>
              <th class="columns01 col_default">Data</th>
              <th class="columns02 col_default">Fornecedor</th>
              <th class="columns02 col_default">Observação</th>
              <th class="columns02 col_default">Total</th>             
              <th class="columns02 col_default">Status</th>
              <th>Ações</th>           
          </tr>
          </thead>
          <tbody>
           <?php foreach ($dados as $dado){ 
              $dado->idDevolucao; 
            ?>
              
              <tr>
                   <td class="text-left"><?php echo date('d/m/Y',  strtotime($dado->dataDevolucao)); ?></td>
                   <td class="text-left"><?php echo $dado->fornecedor_nome; ?></td>
                   <td class="text-left"><?php echo $dado->observacao; ?></td>
                   <td class="text-left"><?php echo $dado->valorTotal; ?></td>
                   <?php if ($dado->finalizado == '1') { ?>
                    <td class="text-left">FINALIZADO</td>
                  <?php } else { ?>
                    <td class="text-left">ABERTO</td>
                  <?php } ?> 
                   
                   <td>
                      <?php if(verificarPermissao('vPedido')){ ?>
                        <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/visualizar/<?php echo $dado->idDevolucao; ?>" data-toggle="tooltip" title="Visualizar"><i class="fa fa-search  text-success"></i> </a>

                      <?php } ?>

                      <?php if(verificarPermissao('ePedido')){ ?>
                        <?php if($dado->finalizado == '0'){ ?>
                      <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/editar/<?php echo $dado->idDevolucao; ?>" data-toggle="tooltip" title="Editar"><i class="fa fa-edit"></i> </a>
                         <?php } ?>
                      <?php } ?>

                      <?php if(verificarPermissao('vPedido')){ ?>
                      <a style="cursor:pointer;" title="Deletar" data-toggle="modal" data-target="#excluir" onclick="excluir('<?php echo base_url().$this->uri->segment(1)."/excluir/".$dado->idDevolucao; ?>');" ><i class="fa fa-trash  text-danger"></i></a>
                      <?php } ?>             
                   </td>

              </tr>
              
          <?php }  ?>    
          </tbody>

      </table>
        </div>

     </div>
  </div>

</section>

