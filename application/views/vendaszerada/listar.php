<br><br><section class="content">
  <div class="box">
    <div class="box-header with-border">
      <div class="col-md-2">
      
      </div>          
    </div>
    <div class="box-body">
      <div class="container-fluid">
        <table id="example" class="table" width="100%">
          <thead>
            <tr>                                            
              <th>Vendedor</th>
              <th>Cliente</th>
              <th>N° da Nota</th>
              <th>Data</th>
              <th>Total</th>
              <th>Ações</th>
            </tr>
          </thead>        
          <tbody>
           <?php foreach ($dados as $dado){ 
              $dado->idVendas; 
            ?>
              
              <tr>
                   <td><?php echo $dado->usuario_nome; ?></td>
                   <td><?php echo $dado->cliente_nome; ?></td>
                   <td><?php echo $dado->idVendas; ?></td>
                   <td><?php echo $dado->dataVenda; ?></td>
                   <td><?php echo $dado->valorTotal; ?></td>
                   <td>
                      <!-- NOVA VENDA -->
           <!--            <?php if(verificarPermissao('vVenda')){ ?>
                        <?php if($dado->faturado == '1'){ ?>
                      <a href="#" onClick="(function(){window.open('<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/visualizarNota/<?php echo $dado->idVendas; ?>','MyWindow','toolbar=no,location=no,directories=no,status=no,menubar=yes,scrollbars=yes,resizable=yes,width=350,height=600');return false;})();return false;"data-toggle="tooltip" title="Visualizar"><i class="fa fa-search  text-success"></i> </a>
                        <?php } ?>
                      <?php } ?> -->

            <!--                   <a class="btn btn-app">
                                <i class="fa fa-repeat"></i> Repeat
                              </a> -->

                      <?php if(verificarPermissao('eVenda')){ ?>
                        <div id="reativar<?php echo $dado->idVendas; ?>">
                         <a style="cursor:pointer;" data-toggle="modal" data-target="#modalSenha" onclick="idVendasModal('<?php echo $dado->idVendas; ?>');" data-toggle="tooltip" title="Reativar"><i class="fa fa-repeat"></i> Reativar </a>
                        </div>
                      <?php } ?>

                   <!--    <?php if(verificarPermissao('dVenda')){ ?>                     
                      <a style="cursor:pointer;" title="Deletar" data-toggle="modal" data-target="#excluir" onclick="excluir('<?php echo base_url().$this->uri->segment(1)."/excluir/".$dado->idVendas."/A" ?>');" ><i class="fa fa-trash text-danger"></i></a>
                      <?php } ?> -->

                   </td>

              </tr>
              
          <?php }  ?>    
          </tbody>

          <tfoot>
            <tr>                     
              <th>Vendedor</th>
              <th>Cliente</th>
              <th>N° da Nota</th>
              <th>Data</th>
              <th>Total</th>
              <th>Ações</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</section>



 <!--  MODAL  -->
  <div class="modal fade" id="modalSenha" data-backdrop="static" data-keyboard="false" style="z-index: 99999" tabindex="-1" role="dialog">
    <div class="modal-dialog" style="width: 350px; margin: 100px auto">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" onclick="closeModal()" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title text-center">Informar senha</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <p>Informe a senha do gerente para liberação desta ação</p>
              <div class="form-group">
                <input type="password" onkeyup="habilitarBTN(this.value)" id="input-liberar-senha" class="form-control" placeholder="Senha do gerente" />
                <input type="hidden" id="idVendasModal"/>
              </div>
              <button id="btn-confirm" disabled class="btn btn-primary btn-block">CONFIRMAR</button>
            </div>
          </div>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>




<script type="text/javascript">

    function habilitarBTN(valor) {
      const el = document.querySelector('#btn-confirm')
      if(valor.length > 0) {
        el.disabled = false
      } else {
          el.disabled = true
      }
    }

    function closeModal() {
        $('#modalSenha').modal('hide');
        document.querySelector('#input-liberar-senha').value = null;
        document.querySelector('#idVendasModal').value = null;
        const el = document.querySelector('#btn-confirm')
        el.disabled = true
    }

    function idVendasModal(idVendas) {
      $('#idVendasModal').val(idVendas);
    }

    $('#btn-confirm').click(function(event) {

        let senha = $('#input-liberar-senha').val();
        let id = $('#idVendasModal').val();

        $.ajax({
          method: "POST",
          url: base_url+"pdv2/validacaoGerente/",
          dataType: "JSON",
          data: { senha },
          success: function(data)
          {
            if(data.result == true){
              $('#reativar'+id).html('<a onclick="idNota('+id+');"  href="'+base_url+'vendas/reativaZeradas/'+id+'" title="Reativar"><i class="fa fa-edit text-success"></i> </a>');
              closeModal();
            }else{
              alert('Senha Invalida.');
              location.reload();
            }
          }
          
        });
    }); 

    function idNota(venda) {
      window.open(base_url+"vendas/visualizarNota/"+venda+"/1", "Visualizar", "toolbar=no,location=no,directories=no,status=no,menubar=yes,scrollbars=yes,resizable=yes,width=350,height=600");
    }

</script>