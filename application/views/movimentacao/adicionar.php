<section class="content">
  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="<?php echo current_url(); ?>" method="post">
          <div class="box-body">

           <!--  <div class="row">
              <div class="col-md-6"> -->

                <div class="form-group">
                  <label for="movimentacao_produto_tipo" class="col-sm-2 control-label">Tipo </label>
                  <div class="col-sm-5">                 
                    <select class="form-control" name="movimentacao_produto_tipo" id="movimentacao_produto_tipo">
                      <option value="Entrada">Entrada</option>
                      <option value="Saída">Saída</option>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="movimentacao_produto_data_cadastro" class="col-sm-2 control-label">Data da Entrada/Saída </label>
                  <div class="col-sm-5">              
                    <input type="text" class="form-control" name="movimentacao_produto_data_cadastro" id="data" value="<?php echo date('d-m-Y');  ?>" placeholder="">
                  </div>
                </div>

                <div style="display: none" id="saida">
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Tipo Saida</label>
                    <div class="col-sm-5">              
                      <select class="form-control" name="tipo_saida" id="tipo_saida">
                        <option value="fornecedor">Fornecedor</option>
                        <option value="filial">Filial</option>
                      </select>
                    </div>
                  </div>

                  <div class="form-group" id="div_filial" style="display: none">
                    <label class="col-sm-2 control-label">Filial</label>
                    <div class="col-sm-5">              
                      <select class="form-control" name="filial" id="filial">
                        <option value="">Selecione</option> <!-- Nessesario para saida de mercadoria (fornecedor) -->
                        <?php foreach ($filias as $filial) {                           
                          $arr = explode('_',$filial->Nome); 
                          $i = count($arr) - 1;
                        ?>                   
                          <option value"<?php echo $filial->Nome ?>"><?php echo $arr[$i]; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>                  
                </div>

                <div class="form-group" id="fornecedor">
                  <label for="movimentacao_fornecedor_id" class="col-sm-2 control-label">Forncedor</label>
                  <div class="col-sm-5">              
                    
                    <select class="form-control" name="movimentacao_fornecedor_id" id="movimentacao_fornecedor_id">
                      <option value="">Selecione</option>
                      <?php foreach ($fornecedor as $valor) { ?>
                        <option value='<?php echo $valor->fornecedor_id; ?>'><?php echo $valor->fornecedor_nome; ?> </option>
                      <?php } ?>
                    </select>
                  </div>
                </div>



         <!--      </div>  
              <div class="col-md-6"> -->

                <div class="form-group">
                  <label for="movimentacao_produto_documento" class="col-sm-2 control-label">Documento </label>
                  <div class="col-sm-5">              
                    <input type="text" class="form-control" name="movimentacao_produto_documento" id="movimentacao_produto_documento" value="<?php echo set_value('movimentacao_produto_documento'); ?>" placeholder="Documento">
                  </div>
                </div>  

                <div class="form-group" id="div_tipo_moeda">
                  <label for="tipo_moeda" class="col-sm-2 control-label">Tipo moeda </label>
                  <div class="col-sm-5">                 
                    <select class="form-control" name="tipo_moeda" id="tipo_moeda">
                      <option value="">Selecione</option>
                      <option value="real">Real</option>
                      <option value="dolar">Dolar</option>
                    </select>
                  </div>
                </div>

                <div class="form-group" id="div_percent_real" style="display: none;">
                  <label for="percent" class="col-sm-2 control-label">%</label>
                  <div class="col-sm-5">              
                    <input type="text" class="form-control dinheiro" name="percent" id="percent" value="0.00" placeholder="Porcentagem">
                  </div>
                </div>  

                <div class="form-group" id="div_cotacao_moeda" style="display: none;">
                  <label for="cotacao_moeda" class="col-sm-2 control-label">Cotação Dolar</label>
                  <div class="col-sm-5">              
                    <input type="text" class="form-control dinheiro" name="cotacao_moeda" id="cotacao_moeda" value="<?php echo set_value('cotacao_moeda'); ?>" placeholder="Cotação">
                  </div>
                </div>  

                <div class="form-group" id="imeidiv">
                  <label for="imei" class="col-sm-2 control-label">Cadastrar IMEI </label>
                  <div class="col-sm-5">              
                    <input  type="checkbox" class="form-check-input" id="imei" name="imei" value="1">
                    <input  type="hidden" class="form-check-input" id="imeiEmpresa" name="imeiEmpresa" value="<?php echo $this->session->userdata('empresa_tipo');?>">
                  </div>
                </div> 

        <!--       </div> 
            </div> -->
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>" class="btn btn-default">Voltar</a>
            <button type="submit" class="btn btn-primary pull-right"><?php echo ucfirst($this->uri->segment(2)); ?></button>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
  </div>
</section>


<script type="text/javascript">

  $(function () {


    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })

  })


</script>