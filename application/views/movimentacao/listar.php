<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <div class="col-md-2">
        <?php if(verificarPermissao('aAlmoxarifado')){ ?>
        <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/adicionar" type="button" class="btn btn-block btn-default btn-flat">Adicionar</a>
        <?php } ?>
      </div>          
    </div>
    <div class="box-body">
      <div class="container-fluid">
      <table id="tableMovimentacao" class="table table-bordered table-striped" width="100%">
          <thead>
            <tr>                                            
              <th>Código</th>
              <th>Filial</th>
              <th>Tipo</th>
              <th>Data</th>
              <th>Documento</th>   
              <th>Total</th>             
              <th>Ações</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
          <tfoot>
            <tr>                     
              <th>Código</th>
              <th>Filial</th>
              <th>Tipo</th>
              <th>Data</th>
              <th>Documento</th>   
              <th>Total</th>             
              <th>Ações</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">
  // Basic example
// $(document).ready(function () {
// $('#examplee').DataTable({
// "ordering": false // false to disable sorting (or any other option)
// });

// });


</script>
