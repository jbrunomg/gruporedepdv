<?php 
  $totalDocumento = 0;
  foreach ($produtos as $prod) {
    $totalDocumento += ($prod->quantidadeProduto * $prod->custoProduto);
  }
 ?>

  <div id="head" style="padding-top: 9px; display: table;">

      <h3 style="width:200px; float:left;">#<?php echo $dados[0]->movimentacao_produto_tipo ?>:<?php echo $dados[0]->movimentacao_produto_id ?></h3>
      <p style="font-size: 12px; display: block; margin: 4px; text-align: right;">Data da Entrada/Saída - <?php echo date('d/m/Y', strtotime($dados[0]->movimentacao_produto_data_cadastro)); ?></p>
      <p style="font-size: 12px; display: block; margin: 4px; text-align: right;">Filial - <?php echo strtoupper($dados[0]->movimentacao_produto_saida_filial); ?></p>
      <!-- <p style="font-size: 12px; display: block; margin: 4px; text-align: right;"><?php echo $emitente[0]->emitente_telefone  ?> </p> -->
      <p style="font-size: 12px; display: block; margin: 4px; text-align: right;">Documento - <?php echo $dados[0]->movimentacao_produto_documento ?></p>
      <p style="font-size: 12px; display: block; margin: 4px; text-align: right;">Tipo Moeda -<?php echo $dados[0]->movimentacao_tipo_moeda ?> </p>
      <br class="clear">
  </div>


<!--             <?php if($dados[0]->movimentacao_produto_saida_tipo == 'fornecedor') {?>
              <label for="fornecedor_nome" class="col-sm-2 control-label">Fonecedor</label>
              <div class="col-sm-2">              
                <input type="text" class="form-control" name="fornecedor_nome" id="fornecedor_nome" value="<?php echo $dados[0]->fornecedor_nome ?>" disabled>
              </div>
            <?php }else{?>
              <label for="movimentacao_produto_saida_filial" class="col-sm-2 control-label">Filial</label>
              <div class="col-sm-2">              
                <input type="text" class="form-control" name="movimentacao_produto_saida_filial" id="movimentacao_produto_saida_filial" value="<?php echo $dados[0]->movimentacao_produto_saida_filial ?>" disabled>
              </div>
            <?php } ?> -->



            <table>
                <thead>
                    <tr>
                      <th style="font-size: 12px;">Cód</th>
                      <th style="font-size: 12px;">Produto</th>
                      <th style="font-size: 12px;">Quantidade</th>                             
                      <th style="font-size: 12px;">Dolar</th>
                      <th style="font-size: 12px;">Custo</th>
                      <th style="font-size: 12px;">Minimo</th>
                      <th style="font-size: 12px;">Venda</th>
                      <th style="font-size: 12px;">Total</th>
                    </tr>
                </thead>

                    <?php
                      $total = 0;
                      $qtd = 0;
                      $seq = 1;
                                             
                     
                    foreach ($produtos as $p) {

                      $total_item = number_format(($p->quantidadeProduto * $p->custoProduto), 2, '.', '');      
                      $qtd = $qtd + $p->quantidadeProduto; ?>

                        <tr class="black">
                          <td class="black"><?php echo $p->codigoProduto ?></td>
                          <td class="black"><?php echo $p->descricaoProduto ?></td>
                          <td class="black"><?php echo $p->quantidadeProduto ?></td>
                          <td class="black"><?php echo $p->dolarProduto ?></td>    
                          <td class="black"><?php echo $p->custoProduto ?></td>
                          <td class="black"><?php echo $p->minimoProduto ?></td>    
                          <td class="black"><?php echo $p->vendaProduto ?></td>
                          <td class="black"><?php echo 'R$ '.$total_item ?></td>    
                        </tr>

                      <?php } ?>

                   <tr class="black">                    
                      <td>ITENS: <?php echo ' '.count($produtos); ?></td>
                      <td></td>                      
                      <td>VOLUME: <?php echo ' '.$qtd; ?></td>
                      <td></td>
                      <td>VALOR TOTAL: <?php echo number_format($totalDocumento, 2, '.', ''); ?></td>
                      <td></td>                      
                    </tr>
                </table>   




