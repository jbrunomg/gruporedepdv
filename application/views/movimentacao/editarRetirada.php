<?php 
  $totalDocumento = 0;
  foreach ($produtos as $prod) {
    $totalDocumento += ($prod->quantidadeProduto * $prod->custoProduto);
  }
 ?>


  <style type="text/css">
    .btn-successteste {
      color: #fff;
      background-color: #5cb85c;
      border-color: #4cae4c;
    }
    .btn-successteste:focus,
    .btn-successteste.focus {
      color: #fff;
      background-color: #ff0909;
      border-color: #ff0909;
    }
    .btn-successteste:hover {
      color: #fff;
      background-color: #449d44;
      border-color: #398439;
    }
 </style>

 

<input id="movId" type="hidden" value="<?php echo $dados[0]->movimentacao_produto_id ?>">
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <!-- Horizontal Form -->
        <div class="box box-primary collapsed-box">
           <div class="box-header with-border">
            <h3>#<?php echo $dados[0]->movimentacao_produto_tipo ?>:<?php echo $dados[0]->movimentacao_produto_id ?></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
            </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="" method="post">
          <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $dados[0]->movimentacao_produto_id; ?>" placeholder="id">        
          <div class="box-body">
            <div class="form-group">
              <label for="movimentacao_produto_data_cadastro" class="col-sm-2 control-label">Data da Entrada/Saída </label>
              <div class="col-sm-2">              
                <input type="text" class="form-control" name="movimentacao_produto_data_cadastro" id="data" value="<?php echo date('d/m/Y', strtotime($dados[0]->movimentacao_produto_data_cadastro)); ?>" disabled>
              </div>

            <?php if($dados[0]->movimentacao_produto_saida_tipo == 'fornecedor') {?>
              <label for="fornecedor_nome" class="col-sm-2 control-label">Fonecedor</label>
              <div class="col-sm-2">              
                <input type="text" class="form-control" name="fornecedor_nome" id="fornecedor_nome" value="<?php echo $dados[0]->fornecedor_nome ?>" disabled>
              </div>
            <?php }else{?>
              <label for="movimentacao_produto_saida_filial" class="col-sm-2 control-label">Filial</label>
              <div class="col-sm-2">              
                <input type="text" class="form-control" name="movimentacao_produto_saida_filial" id="movimentacao_produto_saida_filial" value="<?php echo $dados[0]->movimentacao_produto_saida_filial ?>" disabled>
              </div>
            <?php } ?>
            </div>

            <div class="form-group">
              <label for="movimentacao_produto_documento" class="col-sm-2 control-label">Documento </label>
              <div class="col-sm-2">              
                <input type="text" class="form-control" name="movimentacao_produto_documento" id="movimentacao_produto_documento" value="<?php echo $dados[0]->movimentacao_produto_documento ?>" disabled>
              </div>
              <label for="movimentacao_tipo_moeda" class="col-sm-2 control-label">Tipo Moeda </label>
              <div class="col-sm-2">              
                <input type="text" class="form-control" name="movimentacao_tipo_moeda" id="movimentacao_tipo_moeda" value="<?php echo $dados[0]->movimentacao_tipo_moeda ?>" disabled>
              </div>
            </div> 

            <div class="span12" style="padding: 1%; margin-left: 0">                
                <a href="<?php echo base_url() ?>index.php/movimentacao" class="btn btn-default"><i class="icon-arrow-left"></i> Voltar</a>
              </div>
            </div> 



          </div>

        </form>

        <fieldset class="col-md-12">
          <legend>Dados dos Produtos</legend>
          <div class="col-md-12">
            <form>

              <div class="form-group"> 

                <!-- <input type="hidden" name="produto_id" id="produto_id" /> -->                     
                <input type="hidden" name="movimentacao_produto_id" id="movimentacao_produto_id" value="<?php echo $dados[0]->movimentacao_produto_id ?>" />
                <input type="hidden" class="form-control" name="tipo" id="tipomovimento" value="<?php echo $dados[0]->movimentacao_produto_tipo; ?>" placeholder="tipo">
                <input type="hidden" name="estoque" id="estoque" value=""/>
                <input type="hidden" id="dolar_cotacao" value="<?php echo $dados[0]->movimentacao_cotacao_moeda; ?>">

                <!-- <label for="produto_descricao" class="control-label">Produto </label>                                     -->
                <!-- <input type="text" class="form-control" name="produto_descricao" id="produto_descricao" placeholder="Digite o nome do produto"> -->    

                <div class="col-xs-2"> 
                  <label for="quantidade" class="control-label">Grupo </label> 
                  <div class="input-group margin col-xs-12">                                 
                    <select class="form-control" name="categoria_prod_id" id="grupo_prod">
                      <option value="">Selecione</option>
                      <?php foreach ($categoria_prod as $valor) { ?>
                      <option value='<?php echo $valor->categoria_prod_id; ?>'><?php echo $valor->categoria_prod_descricao; ?> </option>
                      <?php } ?>
                    </select>
                  </div>
                </div> 

                <div class="col-xs-8"> 
                  <label for="quantidade" class="control-label">Produto </label> 
                  <div class="input-group margin">
                    <div class="input-group-btn">
                      <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-default">Prod. +</button>
                    </div>                
                    <!-- <input type="text"  class="form-control" name="produto_descricao" id="produto_descricao" placeholder="Digite o nome do produto"> -->          
                    <select class="select2" style="width: 100%;" name="produto_desc" id="produto_desc_mov">
                      <option value="">Selecione um grupo</option>                       
                    </select>    
                  </div>
                </div> 

                <div class="col-xs-2"> 
                  <label for="quantidade" class="control-label">Quantidade </label> 
                  <div class="input-group margin">                                 
                    <input type="text"  class="form-control" name="quantidade" id="quantidade_mov" placeholder="Quantidade">
                    <input type="hidden"  class="form-control" name="custo" id="custo">
                    <input type="hidden"  class="form-control" name="minimo" id="minimo">

                  </div>
                </div>                 

              </div>

              <div class="form-group">

                <input type="hidden" id="produto_id" value="0">
                <input type="hidden" id="item_produto_codigo" value="">
                <input type="hidden" id="margem" value="0">
                <input type="hidden" id="porcentagem" value="<?php echo $dados[0]->movimentacao_produto_porcentagem ? $dados[0]->movimentacao_produto_porcentagem : 0; ?>">
                <input type="hidden" name="quantidadeProdutos" id="quantidadeProdutos" value="0">
                <input type="hidden" class="form-control text-center" name="tot_item" id="tot_item"  value="0.00" readonly>

              </div> 



              <div class="col-md-3">
                <div class="form-group">
                  <label for="btnAdicionarProduto" class="control-label">&nbsp </label> </br>
                  <!-- <span class="btn btn-success" id="btnSaidaProduto"><i class="icon-white icon-plus"></i> Adicionar</span> -->
                  <button type="button" class="btn btn-successteste span12" id="btnSaidaProduto"><i class="icon-white icon-plus"></i> Adicionar</button> 
                  <button type="submit" class="btn btn-primary" onclick='return pergunta();' form="formProdutos"><i class="icon-white icon-plus"></i> Finalizar Movimentação </button>                
                </div>
              </div>  

            </form>
          </div>
        </fieldset> 


        <div class="box-footer">

          <!-- /.box-header -->
          <div id="divProdutos" class="box-body">
            <form id="formProdutos" action="<?php echo base_url().'movimentacao/saidaProdutos/'.$this->uri->segment(3); ?>" method="post">
              <table class="table table-condensed no-padding" id="tableProdutos"  style="width: 100%;">
                <thead>
                  <tr>
                    <th class="text-center">Cód.</th>
                    <th class="text-center" width="20%">Produto</th>
                    <th class="text-center">Estoque</th>                     
                    <th class="text-center">Quantidade</th>  
                    <th class="text-center">Preço Custo</th> 
                    <th class="text-center">Sub Total</th>                    
                    <th class="text-center">Ações</th>
                  </tr>
                </thead>  
                <tbody>
                  <?php
                    $total = 0;
                    $qtd = 0;
                    $seq = 1;


                    foreach ($produtos as $p) {  

                     // v($produtos);die();

                    // $subTotal = $p->quantidadeProduto + $p->precocusto; 

                    // $total = $total + $subTotal; 

                      $total_item = number_format(($p->quantidadeProduto * $p->precocusto), 2, '.', '');   

                      $total = $total + $total_item; 
                         
                      $qtd = $qtd + $p->quantidadeProduto;
                  ?>

                    <tr>
                      <td> <input type="text" class="text-center col-md-12"  value="<?php echo $p->codigoProduto; ?>" style="border: 0;" readonly></td>

                      <td> <input type="text" class="text-center col-md-12"  value="<?php echo $p->descricaoProduto; ?>" style="border: 0;" readonly></td>

                      <td class="text-center"> <span class="label label-<?php echo $p->Estoque < $p->quantidadeProduto? 'danger' : 'success' ?>"> <?php echo $p->Estoque ?> </td>

                      <td> <input type="text" class="text-center col-md-12"  value="<?php echo $p->quantidadeProduto; ?>" style="border: 0;" readonly></td>

                      <td> <input type="text" class="text-center col-md-12"  value="<?php echo $p->precocusto; ?>" style="border: 0;" readonly></td>

                      <td> <input type="text" class="text-center col-md-12"  value="<?php echo $total_item ?>" style="border: 0;" readonly></td>
                    
                      <td>
                        <a type="button" onclick="RemoveTableRow(this)" produtoId="<?php echo $p->itemMovimentacaoId; ?>" quantidade="<?php echo $p->quantidadeProduto; ?>" valor="<?php echo $total_item ?>"><i class="btn btn-danger fa fa-trash"></i></a>
                      </td>
                    </tr>

                    <?php } ?>


                  </tbody>
                  <tfoot>
                    <tr>                     
                      <th>ITENS: <input id="itens" type="text" value="<?php echo ' '.count($produtos); ?>" style="border: 0;" readonly></th>
                      <th></th>                      
                      <th>VOLUME: <input id="volume" type="text" value="<?php echo ' '.$qtd; ?>" style="border: 0;" readonly></th>
                      <th></th>  
                      <th>TOTAL: R$<input id="vl_documento" type="text" value="<?php echo ' '.number_format($total,2,'.',''); ?>" style="border: 0;" readonly></th>                    
                    </tr>
                  </tfoot>
                </table>
              </form>
            </div>            
          </div> 
        </div>
      </div>
    </div>
  </section>


  <!-- Modal -->

  <div class="modal fade" id="modal-default">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Adicionar Produto</h4>
          </div>
          <div class="modal-body">
            <div class="content">

              <div class="form-group">
                <label>Grupo</label>
                <select class="form-control" id="produto_categoria_id">
                  <option value="0">Selecione</option>
                  <?php foreach ($categoria_prod as $valor) { ?>
                  <option value='<?php echo $valor->categoria_prod_id; ?>'><?php echo $valor->categoria_prod_descricao; ?> </option>
                  <?php } ?>
                </select>
              </div>

              <div class="form-group">
                <label>Descricao</label>
                <input class="col-md-6 form-control" id="modal_descricao_produto"></input>
              </div>
              <div class="form-group">
                <label>Codigo</label>
                <input class="col-md-6 form-control" id="produto_codigo"></input>
              </div>

            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Fechar</button>
            <button type="button" class="btn btn-primary" id='addProdutoAjax'>Adicionar</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <script type="text/javascript">

      function pergunta(){ 
       // retorna true se confirmado, ou false se cancelado
       return confirm('Tem certeza que quer Finalizar Movimentação ?');
      }

    </script>
