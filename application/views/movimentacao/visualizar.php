<?php 
  // $totalDocumento = 0;
  // foreach ($produtos as $prod) {
  //   $totalDocumento += ($prod->quantidadeProduto * $prod->custoProduto);
  // }
 ?>

 

<input id="movId" type="hidden" value="<?php echo $dados[0]->movimentacao_produto_id ?>">
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <!-- Horizontal Form -->
       <div class="box box-primary collapsed-box">
           <div class="box-header with-border">
            <h3>#<?php echo $dados[0]->movimentacao_produto_tipo ?>:<?php echo $dados[0]->movimentacao_produto_id ?></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
            </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="" method="post">
          <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $dados[0]->movimentacao_produto_id; ?>" placeholder="id">        
          <div class="box-body">
            <div class="form-group">
              <label for="movimentacao_produto_data_cadastro" class="col-sm-2 control-label">Data da Entrada/Saída </label>
              <div class="col-sm-2">              
                <input type="text" class="form-control" name="movimentacao_produto_data_cadastro" id="data" value="<?php echo date('d/m/Y', strtotime($dados[0]->movimentacao_produto_data_cadastro)); ?>" disabled>
              </div>

            <?php if($dados[0]->movimentacao_produto_saida_tipo == 'fornecedor') {?>
              <label for="fornecedor_nome" class="col-sm-2 control-label">Fonecedor</label>
              <div class="col-sm-2">              
                <input type="text" class="form-control" name="fornecedor_nome" id="fornecedor_nome" value="<?php echo $dados[0]->fornecedor_nome ?>" disabled>
              </div>
            <?php }else{?>
              <label for="movimentacao_produto_saida_filial" class="col-sm-2 control-label">Filial</label>
              <div class="col-sm-2">              
                <input type="text" class="form-control" name="movimentacao_produto_saida_filial" id="movimentacao_produto_saida_filial" value="<?php echo $dados[0]->movimentacao_produto_saida_filial ?>" disabled>
              </div>
            <?php } ?>
            </div>

            <div class="form-group">
              <label for="movimentacao_produto_documento" class="col-sm-2 control-label">Documento </label>
              <div class="col-sm-2">              
                <input type="text" class="form-control" name="movimentacao_produto_documento" id="movimentacao_produto_documento" value="<?php echo $dados[0]->movimentacao_produto_documento ?>" disabled>
              </div>
              <label for="movimentacao_tipo_moeda" class="col-sm-2 control-label">Tipo Moeda </label>
              <div class="col-sm-2">              
                <input type="text" class="form-control" name="movimentacao_tipo_moeda" id="movimentacao_tipo_moeda" value="<?php echo $dados[0]->movimentacao_tipo_moeda ?>" disabled>
              </div>
            </div> 

            <div class="span12" style="padding: 1%; margin-left: 0">                
                <a href="<?php echo base_url() ?>index.php/movimentacao" class="btn btn-default"><i class="icon-arrow-left"></i> Voltar</a>
              </div>
            </div> 



          </div>

        </form>


        <div class="box-footer">

          <div class="box-header with-border">
            <h3>Dados dos Produtos</h3>
              <div class="box-tools pull-right">             
                <a title="Imprimir" target="_blank" class="btn btn-mini btn-default" href="<?php echo base_url().'movimentacao/imprimirEntradaSaida/'.$this->uri->segment(3); ?>"><i class="glyphicon glyphicon-print icon-white"></i> Imprimir</a>
              </div>
          </div>

          <!-- /.box-header -->
          <div id="divProdutos" class="box-body">
            <form id="formProdutos" action="<?php echo base_url().'movimentacao/entradaProdutos/'.$this->uri->segment(3); ?>" method="post">
              <table class="table table-condensed no-padding" id="tableProdutos"  style="width: 100%;">
                <thead>
                  <tr>
                    <th class="text-center">Cód.</th>
                    <th class="text-center" width="20%">Produto</th>
                    <th class="text-center">Quantidade</th>
                    <th class="text-center">Dolar</th>
                    <th class="text-center">Custo</th>
                    <th class="text-center">Minimo</th>
                    <th class="text-center">Venda</th>
                    <th class="text-center">Total</th>                      
                  </tr>
                </thead>  
                <tbody>
                  <?php
                  $total = 0;
                  $qtd = 0;
                  $seq = 1;
                  $totalDocumento = 0;

                  // echo '<pre>'; var_dump($categoria_prod); die();

                  foreach ($produtos as $p) {     

                  if ( $dados[0]->movimentacao_tipo_moeda == 'dolar') {

                      $total_item = number_format(($p->quantidadeProduto * $p->dolarProduto * $dados[0]->movimentacao_cotacao_moeda ), 2, '.', '');
                    
                    }  else {
                    
                      $total_item = number_format(($p->quantidadeProduto * $p->custoProduto), 2, '.', ''); 
                  }
                          
                      $totalDocumento = $totalDocumento + $total_item;

                      $qtd = $qtd + $p->quantidadeProduto;
                      // $total = $total + ($p->precoUnitario * $p->item_movimentacao_produto_quantidade);
                    ?>
                    <tr>
                      <td class="text-center"> <?php echo $p->codigoProduto; ?> </td>
                      <td class="text-center"> <?php echo $p->descricaoProduto; ?> </td>
                      <td class="text-center"> <?php echo $p->quantidadeProduto; ?> </td>
                      <td class="text-center"> <?php echo $p->dolarProduto; ?> </td>
                      <td class="text-center"> <?php echo $p->custoProduto; ?> </td>
                      <td class="text-center"> <?php echo $p->minimoProduto; ?> </td>
                      <td class="text-center"> <?php echo $p->vendaProduto; ?> </td>
                      <td class="text-center"> <?php echo $total_item; ?> </td>
                      <!-- <td> <input type="text" class="text-center col-md-12"  value="<?php echo $p->codigoProduto; ?>" style="border: 0;" readonly></td>
                      <td> <input type="text" class="text-center col-md-12"  value="<?php echo $p->descricaoProduto; ?>" style="border: 0;" readonly></td>
                      <td> <input type="text" class="text-center col-md-12"  value="<?php echo $p->quantidadeProduto; ?>" style="border: 0;" readonly></td>
                      <td> <input type="text" class="text-center col-md-12"  value="<?php echo $p->dolarProduto; ?>" style="border: 0;" readonly></td>
                      <td> <input type="text" class="text-center col-md-12"  value="<?php echo $p->custoProduto; ?>" style="border: 0;" readonly></td>
                      <td> <input type="text" class="text-center col-md-12"  value="<?php echo $p->minimoProduto; ?>" style="border: 0;" readonly></td>
                      <td> <input type="text" class="text-center col-md-12"  value="<?php echo $p->vendaProduto; ?>" style="border: 0;" readonly></td>
                      <td> <input type="text" class="text-center col-md-12"  value="<?php echo $total_item ?>" style="border: 0;" readonly></td>  -->
                    </tr>

                    <?php } ?>


                  </tbody>

                  <tfoot>
                    <tr>                     
                      <th>ITENS: <?php echo ' '.count($produtos); ?></th>
                      <th></th>                      
                      <th>VOLUME: <?php echo ' '.$qtd; ?></th>
                      <th></th>
                      <th>VALOR TOTAL:  <?php echo number_format($totalDocumento, 2, '.', ''); ?></th>
                      <th></th>                      
                    </tr>
                  </tfoot>
                </table>
              </form>
            </div>            
          </div> 
        </div>
      </div>
    </div>
  </section>


  <!-- Modal -->

  <div class="modal fade" id="modal-default">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Adicionar Produto</h4>
          </div>
          <div class="modal-body">
            <div class="content">

              <div class="form-group">
                <label>Grupo</label>
                <select class="form-control" id="produto_categoria_id">
                  <option value="0">Selecione</option>
                  <?php foreach ($categoria_prod as $valor) { ?>
                  <option value='<?php echo $valor->categoria_prod_id; ?>'><?php echo $valor->categoria_prod_descricao; ?> </option>
                  <?php } ?>
                </select>
              </div>

              <div class="form-group">
                <label>Descricao</label>
                <input class="col-md-6 form-control" id="modal_descricao_produto"></input>
              </div>
              <div class="form-group">
                <label>Codigo</label>
                <input class="col-md-6 form-control" id="produto_codigo"></input>
              </div>

            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Fechar</button>
            <button type="button" class="btn btn-primary" id='addProdutoAjax'>Adicionar</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
