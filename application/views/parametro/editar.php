<section class="content">
  <div class="row" >
    <div class="col-md-6">
    <!-- Horizontal Form -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Cartão</h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="" method="post">
        <input type="hidden" class="form-control" name="parametro_id" id="id" value="<?php echo $dados[0]->parametro_id; ?>" placeholder="id">
          <div class="box-body">

            <div class="form-group">
              <label for="parametro_cart_debito" class="col-sm-2 control-label">% cartão débito*</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" name="parametro_cart_debito" id="parametro_cart_debito" value="<?php echo $dados[0]->parametro_cart_debito; ?>" placeholder="Descrição">
              </div>
            </div> 

            <div class="form-group">
              <label for="parametro_cart_credito" class="col-sm-2 control-label">% cartão credito*</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" name="parametro_cart_credito" id="parametro_cart_credito" value="<?php echo $dados[0]->parametro_cart_credito; ?>" placeholder="Descrição">
              </div>
            </div>   
   

          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>" class="btn btn-default">Voltar</a>
            <button type="submit" formaction="<?php echo current_url(); ?>" class="btn btn-primary pull-right"><?php echo ucfirst($this->uri->segment(2)); ?></button>
            <button type="submit" formaction="<?php echo base_url(); ?>parametro/updateMassa" class="btn btn-success pull-right">Update Produtos</button></a>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
    <div class="col-md-6">
    <!-- Horizontal Form -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Senha Gerente</h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="text-left" action="<?php echo base_url(); ?>parametro/editarSenhaGerente" method="post">
        <input type="hidden" class="form-control" name="parametro_id" id="id" value="<?php echo $dados[0]->parametro_id; ?>" placeholder="id">
        <br><br>
          <div class="box-body">
            <div class="form-group ml-2">
              <div class="form-check">
                <input class="form-check-input" type="radio" name="parametro_senha_gerente" id="parcial" value="0" <?php echo $dados[0]->parametro_senha_gerente == '0' ? 'checked' : ''; ?>>
                <label class="form-check-label ">Parcial (Produto a Produto)</label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="radio" name="parametro_senha_gerente" id="geral" value="1" <?php echo $dados[0]->parametro_senha_gerente == '1'? 'checked': ''; ?>>
                <label class="form-check-label">Geral (Ao Final da Venda)</label>
              </div>
            </div>
          </div>
          <br>

          <!-- /.box-body -->
          <div class="box-footer">
            <button type="submit" class="btn btn-primary pull-right"><?php echo ucfirst($this->uri->segment(2)); ?></button>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
  </div>
  
</section>

<section class="content">
<!-- SELECT CHAT -->
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Chat - Boot</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
      

      <form class="form-horizontal" action="<?php echo base_url(); ?>parametro/editarChat" method="post">
      
        <input type="hidden" class="form-control" name="parametro_id" id="id" value="<?php echo $dados[0]->parametro_id; ?>" placeholder="id">
        

        <div class="box-body">
          <div class="col-md-12">
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label>Exibir apenas com estoque</label>
                <select class="form-control select2" name="parametro_chat_estoque"  style="width: 100%;">
                  <option value='N' <?php echo ($dados[0]->parametro_chat_estoque == 'N')?'Selected':'';?>>Não</option>
                  <option value='S' <?php echo ($dados[0]->parametro_chat_estoque == 'S')?'Selected':'';?>>Sim</option>                 
                </select>

                
              </div>
              <!-- /.form-group -->
              <div class="form-group">
                <label>Exibir Preço</label>
                <select class="form-control select2" name="parametro_chat_preco" style="width: 100%;">
                  <option value='PMV' <?php echo ($dados[0]->parametro_chat_preco == 'PMV')?'Selected':'';?>>Preço minimo venda</option>
                  <option value='PV' <?php echo ($dados[0]->parametro_chat_preco == 'PV')?'Selected':'';?>>Preço venda</option>    
                  <option value='P2' <?php echo ($dados[0]->parametro_chat_preco == 'P2')?'Selected':'';?>>Ambos</option>               
                </select>
              </div>
              <!-- /.form-group -->
            </div>
            <!-- /.col -->
            <div class="col-md-2">
            </div>
            <!-- /.col -->

            <div class="col-md-4">
              <div class="form-group">
                <label>Redirecionar para o whatsApp, após quantas tentativa ?</label>
                <select class="form-control select2" name="parametro_chat_tentiva_busca"  style="width: 100%;">
                  <option value='4' <?php echo ($dados[0]->parametro_chat_tentiva_busca == '4')?'Selected':'';?>>4</option>
                  <option value='3' <?php echo ($dados[0]->parametro_chat_tentiva_busca == '3')?'Selected':'';?>>3</option>
                  <option value='2' <?php echo ($dados[0]->parametro_chat_tentiva_busca == '2')?'Selected':'';?>>2</option>
                  <option value='1' <?php echo ($dados[0]->parametro_chat_tentiva_busca == '1')?'Selected':'';?>>1</option>                                       
                </select>
              </div>
              <!-- /.form-group -->
              <div class="form-group">
                <label>Nº whatsApp para redirecionamento e ligação.</label>
                <input type="text" class="form-control" name="parametro_chat_n_whatsapp" id="parametro_chat_n_whatsapp" value="<?php echo $dados[0]->parametro_chat_n_whatsapp; ?>" placeholder="DDD + Nº WhatsApp para contato ">
              </div>
              <!-- /.form-group -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
          </div>

        </div>
      
        <!-- /.box-body -->
          <div class="box-footer">
            <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>" class="btn btn-default">Voltar</a>
            <button type="submit" class="btn btn-primary pull-right"><?php echo ucfirst($this->uri->segment(2)); ?></button>
            </a>
          </div>
        </form>
        
          <!-- /.box-footer -->
        <div class="box-footer">
          Visite <a href="<?php echo base_url(); ?>chat">seu chat</a> e veja como ficou a configuração.
        </div>

       
      </div>
      <!-- /.box -->
</section>  






<style type="text/css">
  button{
    margin: 0 5px;
}
</style>