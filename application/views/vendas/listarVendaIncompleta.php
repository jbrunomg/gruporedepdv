<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <div class="col-md-2">
      
      </div>          
    </div>
    <div class="box-body">
      <div class="container-fluid">
        <table id="example" class="table" width="100%">
          <thead>
            <tr>   
              <th>Categoria</th>                                         
              <th>Produto</th>
              <th>Estoque</th>
              <th>IMEI</th>          
              <th>Ações</th>
            </tr>
          </thead>        
          <tbody>
           <?php foreach ($dados as $dado){ ?>              
              <tr>
                <td><?php echo $dado->categoria_prod_descricao; ?></td>
                <td><?php echo $dado->produto_descricao; ?></td>
                <td><?php echo $dado->produto_estoque; ?></td>
                <td><?php echo $dado->IMEI_Cadastrado; ?></td>
                
                <td>
                <!-- NOVA VENDA -->
                  <?php if(verificarPermissao('eProduto')){ ?>
                  <a href="#" data-toggle="tooltip" title="Editar"><i class="fa fa-edit"></i> </a>
                  <?php } ?>                        
                </td>
              </tr>              
          <?php }  ?>    
          </tbody>
          <tfoot>
            <tr>  
              <th>Categoria</th>                    
              <th>Produto</th>
              <th>Estoque</th>
              <th>IMEI</th>          
              <th>Ações</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</section>