
    
<style type="text/css" media="all">
	@media print {
		.no-print { display: none; }
		#wrapper { max-width: 480px; width: 100%; min-width: 250px; margin: 0 auto; }
		.pagebreak { page-break-before: always; } /* page-break-after works, as well */
	}

	p{
	font-family:Arial, Helvetica, sans-serif;
	}
	
	
</style>
<body>
<div class='container' style='max-width:300px;padding:5px 5px;'<table width="100%">
<tr>
<?php if ($notaExcl == 'danger') { ?>
    <h2 style='text-align:center;font-size: 16px;margin-top:0px;margin-bottom:5px;'><i>NOTA - CANCELADA</i></h2>
<?php } else { ?>    
    <!-- <h2 style='text-align:center;font-size: 16px;margin-top:0px;margin-bottom:5px;'><i>Cupom Não Fiscal</i></h2> -->
    <!-- <h2 style='text-align:center;font-size: 16px;margin-top:0px;margin-bottom:5px;'><i>Cupom Não Fiscal</i></h2> -->
    <h2 style='text-align:center;font-size: 16px;margin-top:0px;margin-bottom:5px;'><i>Orçamento</i></h2>
    <?php 
        if ($financeiro[0]->financeiro_baixado == '0') {
            // Calcular a diferença de dias
            $data_pagamento = new DateTime($financeiro[0]->data_pagamento);
            $data_vencimento = new DateTime($financeiro[0]->data_vencimento);
            $diferenca = $data_pagamento->diff($data_vencimento);

            // Formatar o texto
            echo '<h2 style="text-align:center;font-size: 16px;margin-top:0px;margin-bottom:5px;">Data Pagamento: ' . $diferenca->days . ' Dias ' . 
                 date('d/m/Y', strtotime($financeiro[0]->data_pagamento)). '</h2>';
        }
    ?>
<?php } ?>


<td colspan="2"><div style='font-size:10px;'><?php echo $emitente[0]->emitente_nome ?><br><?php echo ($emitente[0]->emitente_cnpj == '') ? '' : 'CNPJ: '.$emitente[0]->emitente_cnpj ; ?><?php echo ($emitente[0]->emitente_ie == '') ? '' : 'I.E.: '.$emitente[0]->emitente_ie;?><br><?php echo $emitente[0]->emitente_rua ?><br><?php echo $emitente[0]->emitente_bairro ?>, <?php echo $emitente[0]->emitente_cidade.' - '.$emitente[0]->emitente_uf?></div><hr></td>
</tr>
<tr>
<!-- <td colspan="3" class="menor"><div style='text-align:center;font-size:11px;'>Cupom - Documento auxiliar<br> da Nota de Consumidor</td> -->
</tr>
<tr>
<!-- <td colspan="3" class="menor"><br><b>Não permite aproveitamento de crédito de ICMS<b></td> -->
</tr>
</div></table>
<hr>
<table width='100%'>

<tr>
<td><div style='text-align:left;font-size:10px;'>#</div></td>
<td><div style='text-align:left;font-size:10px;'>LJ</div></td>
<td><div style='text-align:left;font-size:10px;'>GP</div></td>
<td><div style='text-align:left;font-size:10px;'>DESCRIÇÃO</div></td>
<td><div style='text-align:right;font-size:10px;'>QTD.</div></td>
<td><div style='text-align:right;font-size:10px;'>VL.UNIT.</div></td>
<td><div style='text-align:right;font-size:10px;'>VL.TOTAL</div></td>
</tr>
<?php 
$seq = 1;
$totalProdutos = 0;
	foreach ($dados as $d){
$totalProdutos = $totalProdutos + $d->subTotal; 
?>
<tr>
<td align="left"><div style='text-align:left;font-size:10px;'><?php echo $seq ++ ?></div></td>
<td align="left"><div style='text-align:left;font-size:10px;'><?php   echo substr(strtoupper($d->filial), 0, 2)  ?></div></td>
<td align="left"><div style='text-align:left;font-size:10px;'><?php   echo $d->produto_codigo ?></div></td>
<td align="left"><div style='text-align:left;font-size:10px;'><?php   echo $d->produto_descricao .'<br>'.$d->imei_valor?></div></td>
<td align="right"><div style='text-align:right;font-size:10px;'><?php echo $d->quantidade ?> <?php   echo $d->produto_unidade ?></div></td>
<td align="right"><div style='text-align:right;font-size:10px;'>R$<?php  echo $d->valor_unitario ?></div></td>
<td align="right"><div style='text-align:right;font-size:10px;'><?php  echo 'R$ '. number_format($d->subTotal,2,",",".") ?></div></td>
</tr>
<?php } ?>



</table>
<hr>
<table width="100%">
<tr>
<td align="left"><div style='text-align:left;'>QTD. TOTAL DE ITENS</div></td>
<td align="right"><div style='text-align:right;'><?php echo $seq - 1 ?></div></td>
</tr>
<tr>
<td align="left"><div style='text-align:left;'>VALOR TOTAL R$</div></td>
<td align="right"><div style='text-align:right;'><?php echo number_format($totalProdutos,2,".",".") ?></div></td>
</tr>
<tr>
<td align="left"><div style='text-align:left;'>DESCONTO R$</div></td>
<td align="right"><div style='text-align:right;'><?php echo  number_format($desconto,2,".",".") ?></div></td>
</tr>
<tr>
<td align="left"><div style='text-align:left;'><strong>VALOR A PAGAR R$</strong></div></td>
<td align="right"><div style='text-align:right;'><strong><?php echo number_format($totalProdutos - $desconto,2,".",".") ?></strong></div></td>
</tr>

<tr>
	<td align="left"><div style='text-align:left;'>FORMA DE PAGAMENTO</div></td>
</tr>
<?php $valorPago = 0; $recibido = 0; foreach ($financeiro as $f) { $valorPago += $f->financeiro_valor; $recibido = $f->financeiro_baixado == '0' ? 1 : 0?>
	<tr>
		<td align="right" style="width: 155px;"><div style='text-align:right;'><?php echo $f->financeiro_forma_pgto; if ($f->financeiro_forma_pgto == 'Cartão de Crédito' && !empty($f->financeiro_parcela_cart)) { echo '&ensp;'.$f->financeiro_parcela_cart . 'x';}?></div></td>
		<td align="right"><div style='text-align:right;'><?php echo  number_format($f->financeiro_valor,2,".",".") ?></div></td>
	</tr>
<?php } ?>
<tr>
	<td align="left"><div style='text-align:left;'>VALOR PAGO R$</div></td>
	<td align="right"><div style='text-align:right;'><?php echo  number_format($valorPago,2,".",".") ?></div></td>
</tr>

<?php // foreach ($pagamento as $pgt) { $troco  = ($pgt->valor_pago - $pgt->valor); ?>
<!-- <tr>
<td align="left" style='text-align:left;'><?php // echo $pgt->forma_pgto ?></td>
<td align="right" style='text-align:right;'>58,53</td>
</tr> -->
<!-- <?php //} ?> -->

<tr>
<td align="left" style='text-align:left;'>Troco R$</td>
<td align="right" style='text-align:right;'><?php echo $troco ?></td>
</tr>
<tr>
<!-- <td align="left"><div style='text-align:left;'>Informação dos Tributos Incidentes<br>(Lei Federal 12.741/2012)</div></td> -->
<!-- <td align="right"><div style='text-align:right;'>0,00</div></td> -->
</tr>
</table>
<hr><table width="100%">
<tr>
<!-- <td  colspan="3"><div style='word-break: break-all;font-size:11px'>Consulte pela Chave de Acesso em <br>nfce.sefaz.pe.gov.br/nfce/consulta<br />26191124942487000190650010000002521287177940</div></td> -->
</tr>
</table>
<table width="80%" align="center">
<tr>
<td colspan="3"><b><?php echo $cliente ?></b></td>
</tr>
</table>
<table width="80%" align="center">
<tr>
<td>Cupom nº <?php echo $venda ?></td>
<td>Série: 001</td>
<td>
    <?php 
    if ($recibido == '0') {
        echo date("d/m/Y h:i:sa");
    } else {
        // Calcular a diferença de dias
        $data_pagamento = new DateTime($financeiro[0]->data_pagamento);
        $data_vencimento = new DateTime($financeiro[0]->data_vencimento);
        $diferenca = $data_pagamento->diff($data_vencimento);

        // Formatar o texto
        echo 'Data Pagamento: ' . $diferenca->days . ' Dias ' . 
             date('d/m/Y', strtotime($financeiro[0]->data_pagamento));
    }
    ?>
</td>

</tr>
<tr>
<td colspan="3"><b><?php echo $vendendor ?></b></td>
</tr>
<tr>
<td colspan="3">Protocolo de autorização: <?php echo $protocolo ?><br />Data de autorização: <?php echo $dataVenda ?><br />Data de modificação: <?php echo $dataAtualizacao ?> </td>
</tr>
</table>
<table width="100%">
<tr>
<!-- <td  colspan="3"><img src="https://chart.googleapis.com/chart?cht=qr&chs=300x300&chl=http%3A%2F%2Fwww.nfe.fazenda.gov.br%2Fportal%2Fconsulta.aspx%3FtipoConsulta%3Dcompleta%26tipoConteudo%3DXbSeqxE8pl8%3DMobLanche_PDVPARATODOS.COM.BR&choe=UTF-8&chld=L|4" style='max-width:150px;'></td> -->

<td  colspan="3"><img src="https://chart.googleapis.com/chart?cht=qr&chs=300x300&chl=<?php echo base_url(); ?>notas/visualizarDanfe/<?php echo $venda ?>" style='max-width:150px;'></td>


</tr>
</table>
<hr>

<div class="pagebreak"> </div>

<hr>
<table width="80%" align="center">
<tr>
<h2 style='text-align:center;font-size: 16px;margin-top:0px;margin-bottom:5px;'><i>2º Via - Loja</i></h2>
</tr>
<tr>
<td>Cupom nº <b><?php echo $venda ?></b></td>
<td>Total: <b><?php echo 'R$ '.number_format($totalProdutos - $desconto,2,".","."); ?></b></td>
<td><?php echo date("d/m/Y h:i:sa"); ?></td>
</tr>
<tr>
<td colspan="3"><b><?php echo $vendendor ?></b></td>
</tr>
<tr>
<td colspan="3">Protocolo de autorização: <?php echo $protocolo ?><br />Data de autorização: <?php echo $dataVenda ?><br />Data de modificação: <?php echo $dataAtualizacao ?> </td>
</tr>
</table>
<hr>

<table width="100%" class="no-print">
	<tr>
		<td colspan="3">
			<button
				style="border: 0; cursor: pointer; background: #367fa9; display: block; width: 100%; padding: 10px; text-align: center; text-transform: uppercase;color: #fff"
				href="javascript:window.print()"
				id="web_print" class="btn btn-block btn-primary"
				onClick="window.print();return false;">
				Impressão Web
			</button>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<button
				style="border: 0; cursor: pointer; background: #e08e0b; display: block; width: 100%; padding: 10px; text-align: center; text-transform: uppercase;color: #fff"
				href="javascript:window.print()"
				id="web_print" class="btn btn-block btn-primary"
				onClick="javascript:window.close();return false;">
				Fechar
			</button>
		</td>
	</tr>

</table>
<div style="clear:both;"></div>
<div class="col-xs-12 no-print" style="background:#F5F5F5; padding:10px;">
	<font size="-2">
		<p style="font-weight:bold;">Favor alterar as configurações de impressão de seu browser</p>
		<p style="text-transform: capitalize;"><strong>FireFox:</strong> Arquivo &gt; Configurar impressora &gt; Margem &amp;Cabeçalho/Rodapé --Nenhum--</p>
		<p style="text-transform: capitalize;"><strong>Chrome:</strong> Menu &gt; Impressora &gt; Disabilitar Cabeçalho/Rodapé Opções &amp; Setar margem em branco</p></div>
<font>
	<div style="clear:both;"></div>
</div>
</body>
</html>
