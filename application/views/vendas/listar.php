<br><br>
<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <div class="box-body">
        <table id="tableVenda" class="table table-bordered table-striped">
          <thead>
            <tr>
                <th class="columns01 col_default">Vendedor</th>
                <th class="columns02 col_default">Cliente</th>
                <th class="columns02 col_default">N° da Nota</th>
                <th class="columns01 col_default">Data</th>    
                <th class="columns02 col_default">Total</th>
                <th>Ações</th>           
            </tr>
          </thead>
          <tbody>  
          </tbody>
        </table> 
      </div>
    </div>
  </div>
</section>


<!-- Modal Excluir registros -->
 <div class="example-modal">
  <div class="modal" id="excluirVendas">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Excluir</h4>
        </div>
        <div class="modal-body">
          <p id="vendasExcluir">Deseja realmente excluir esta Venda Nº ?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
          <a href="" class="btn btn-danger" id="excluirIdVendas">Excluir</a>
        </div>
      </div>      
    </div>          
  </div>        
</div>  
<!-- Fim modal Excluir registros -->

