<!-- Header -->

<?php 
 // echo "<pre>";
 // var_dump($pagina);die();
?>
<style type="text/css">
    @media print {
        @page {
            margin-left: 15mm;
            margin-right: 15mm;
        }

        footer {
            page-break-after: always;
        }
    }

    * {
        margin: 0;
    }

    .ui-widget-content {
        border: none !important;
    }

    .nfe-square {
        margin: 0 auto 2cm;
        box-sizing: border-box;
        width: 2cm;
        height: 1cm;
        border: 1px solid #000;
    }

    .nfeArea.page {
        width: 18cm;
        position: relative;
        font-family: "Times New Roman", serif;
        color: #000;
        margin: 0 auto;
        overflow: hidden;
    }

    .nfeArea .font-12 {
        font-size: 12pt;
    }

    .nfeArea .font-8 {
        font-size: 8pt;
    }

    .nfeArea .bold {
        font-weight: bold;
    }
    /* == TABELA == */
    .nfeArea .area-name {
        font-family: "Times New Roman", serif;
        color: #000;
        font-weight: bold;
        margin: 5px 0 0;
        font-size: 6pt;
        text-transform: uppercase;
    }

    .nfeArea .txt-upper {
        text-transform: uppercase;
    }

    .nfeArea .txt-center {
        text-align: center;
    }

    .nfeArea .txt-right {
        text-align: right;
    }

    .nfeArea .nf-label {
        text-transform: uppercase;
        margin-bottom: 3px;
        display: block;
    }

        .nfeArea .nf-label.label-small {
            letter-spacing: -0.5px;
            font-size: 4pt;
        }

    .nfeArea .info {
        font-weight: bold;
        font-size: 8pt;
        display: block;
        line-height: 1em;
    }

    .nfeArea table {
        font-family: "Times New Roman", serif;
        color: #000;
        font-size: 5pt;
        border-collapse: collapse;
        width: 100%;
        border-color: #000;
        border-radius: 5px;
    }

    .nfeArea .no-top {
        margin-top: -1px;
    }

    .nfeArea .mt-table {
        margin-top: 3px;
    }

    .nfeArea .valign-middle {
        vertical-align: middle;
    }

    .nfeArea td {
        vertical-align: top;
        box-sizing: border-box;
        overflow: hidden;
        border-color: #000;
        padding: 1px;
        height: 5mm;
    }

    .nfeArea .tserie {
        width: 32.2mm;
        vertical-align: middle;
        font-size: 8pt;
        font-weight: bold;
    }

        .nfeArea .tserie span {
            display: block;
        }

        .nfeArea .tserie h3 {
            display: inline-block;
        }

    .nfeArea .entradaSaida .legenda {
        text-align: left;
        margin-left: 2mm;
        display: block;
    }

        .nfeArea .entradaSaida .legenda span {
            display: block;
        }

    .nfeArea .entradaSaida .identificacao {
        float: right;
        margin-right: 2mm;
        border: 1px solid black;
        width: 5mm;
        height: 5mm;
        text-align: center;
        padding-top: 0;
        line-height: 5mm;
    }

    .nfeArea .hr-dashed {
        border: none;
        border-top: 1px dashed #444;
        margin: 5px 0;
    }

    .nfeArea .client_logo {
        height: 27.5mm;
        width: 28mm;
        margin: 0.5mm;
    }

    .nfeArea .title {
        font-size: 10pt;
        margin-bottom: 2mm;
    }

    .nfeArea .txtc {
        text-align: center;
    }

    .nfeArea .pd-0 {
        padding: 0;
    }

    .nfeArea .mb2 {
        margin-bottom: 2mm;
    }

    .nfeArea table table {
        margin: -1pt;
        width: 100.5%;
    }

    .nfeArea .wrapper-table {
        margin-bottom: 2pt;
    }

        .nfeArea .wrapper-table table {
            margin-bottom: 0;
        }

            .nfeArea .wrapper-table table + table {
                margin-top: -1px;
            }

    .nfeArea .boxImposto {
        table-layout: fixed;
    }

        .nfeArea .boxImposto td {
            width: 11.11%;
        }

        .nfeArea .boxImposto .nf-label {
            font-size: 5pt;
        }

        .nfeArea .boxImposto .info {
            text-align: right;
        }

    .nfeArea .wrapper-border {
        border: 1px solid #000;
        border-width: 0 1px 1px;
        height: 60.7mm; 
        /*height: 125.7mm;*/

    }

        .nfeArea .wrapper-border table {
            margin: 0 -1px;
            width: 100.4%;
        }

    .nfeArea .content-spacer {
        display: block;
        height: 10px;
    }

    .nfeArea .titles th {
        padding: 3px 0;
    }

    .nfeArea .listProdutoServico td {
        padding: 0;
    }

    .nfeArea .codigo {
        display: block;
        text-align: center;
        margin-top: 5px;
    }

    .nfeArea .boxProdutoServico tr td:first-child {
        border-left: none;
    }

    .nfeArea .boxProdutoServico td {
        font-size: 6pt;
        height: auto;
    }

    .nfeArea .boxFatura span {
        display: block;
    }

    .nfeArea .boxFatura td {
        border: 1px solid #000;
    }

    .nfeArea .freteConta .border {
        width: 5mm;
        height: 5mm;
        float: right;
        text-align: center;
        line-height: 5mm;
        border: 1px solid black;
    }

    .nfeArea .freteConta .info {
        line-height: 5mm;
    }

    .page .boxFields td p {
        font-family: "Times New Roman", serif;
        font-size: 5pt;
        line-height: 1.2em;
        color: #000;
    }

    .nfeArea .imgCanceled {
        position: absolute;
        top: 75mm;
        left: 30mm;
        z-index: 3;
        opacity: 0.8;
        display: none;
    }

    .nfeArea.invoiceCanceled .imgCanceled {
        display: block;
    }

    .nfeArea .imgNull {
        position: absolute;
        top: 75mm;
        left: 20mm;
        z-index: 3;
        opacity: 0.8;
        display: none;
    }

    .nfeArea.invoiceNull .imgNull {
        display: block;
    }

    .nfeArea.invoiceCancelNull .imgCanceled {
        top: 100mm;
        left: 35mm;
        display: block;
    }

    .nfeArea.invoiceCancelNull .imgNull {
        top: 65mm;
        left: 15mm;
        display: block;
    }

    .nfeArea .page-break {
        page-break-before: always;
    }

    .nfeArea .block {
        display: block;
    }

    .label-mktup {
        font-family: Arial !important;
        font-size: 8px !important;
        padding-top: 8px !important;
    }
</style>

<!--  <style type="text/css">
   table.page-break{
      page-break-after:always
    };
    table.myFormat tr td {
     font-size: 8px;
    }
</style> -->

<!-- /Header -->
<!-- Recebimentos -->



<div class="page nfeArea">
    <img class="imgCanceled" src="tarja_nf_cancelada.png" alt="" />
    <img class="imgNull" src="tarja_nf_semvalidade.png" alt="" />

    <?php
        for ($i=0; $i < $pagina ; $i++) {  
   
    ?> 
    <div class="boxFields" style="padding-top: 20px;">
        <table cellpadding="0" cellspacing="0" border="1">
            <tbody>
                <tr>
                    <td colspan="2" class="txt-upper">
                        Recebemos de <?php echo $emitente[0]->emitente_nome ?> os produtos e serviços constantes na nota indicada ao lado
                    </td>
                    <td rowspan="2" class="tserie txt-center">
                        <span class="font-12" style="margin-bottom: 5px;">Nota</span>
                        <span>Nº <?php echo $dados[0]->idVendas ?></span>
                        <!-- <span>Série 099</span> -->
                    </td>
                </tr>
                <tr>
                    <td style="width: 32mm">
                        <span class="nf-label">Data de recebimento</span>
                    </td>
                    <td style="width: 124.6mm">
                        <span class="nf-label">Identificação de assinatura do Recebedor</span>
                    </td>
                </tr>
            </tbody>
        </table>
        <hr class="hr-dashed" />
        <table cellpadding="0" cellspacing="0" border="1">
            <tbody>
                <tr>
                    <td rowspan="3" style="width: 30mm">
                        <img class="client_logo" src="<?php echo  $emitente[0]->emitente_url_logo;  ?>" width="200" height="100"  alt="" onerror=" javascript:this.src='data:image/png;base64,"/>
                    </td>
                    <td rowspan="3" style="width: 46mm; font-size: 7pt;" class="txt-center">
                        <span class="mb2 bold block"><?php echo $emitente[0]->emitente_nome ?></span>
                        <span class="block"><?php echo $emitente[0]->emitente_rua ?><br><?php echo $emitente[0]->emitente_bairro ?></span>
                        <span class="block">
                            <?php echo $emitente[0]->emitente_bairro ?> - <?php echo $emitente[0]->emitente_cep ?>
                        </span>
                        <span class="block">
                            <?php echo $emitente[0]->emitente_cidade.' - '.$emitente[0]->emitente_uf?> <br> Fone: <?php echo $emitente[0]->emitente_telefone ?>
                        </span>
                    </td>
                    <td rowspan="3" class="txtc txt-upper" style="width: 34mm; height: 29.5mm;">
                        <h3 class="title">NOTA</h3>
                        <p class="mb2">Documento auxiliar da Nota</p>
                        <p class="entradaSaida mb2">
                            <span class="identificacao">
                                <span>1</span>
                            </span>
                            <span class="legenda">
                                <span>0 - Entrada</span>
                                <span>1 - Saída</span>
                            </span>
                        </p>
                        <p>
                            <span class="block bold">
                                <span>Nº</span>
                                <span><?php echo $dados[0]->idVendas ?></span>
                            </span>
                        <!--     <span class="block bold">
                                <span>SÉRIE:</span>
                                <span>0099</span>
                            </span> -->
                            <span class="block">
                                <span>Página</span>
                                <span><?php echo $i+1; ?></span>
                                <span>de</span>
                                <span><?php echo $pagina; ?></span>
                            </span>
                        </p>
                    </td>
                    <td class="txt-upper" style="width: 85mm;">
                        <span class="nf-label">Observação:</span>
                        <span class="bold block txt-center info"><?php echo $dados[0]->observacao;  ?></span>
                    </td>
                </tr>
         <!--        <tr>
                    <td>
                        <span class="nf-label">CHAVE DE ACESSO</span>
                        <span class="bold block txt-center info"><?php echo $protocolo ?></span>
                    </td>
                </tr> -->
                <tr>
                    <td class="txt-center valign-middle">
                        <span class="block">Consulta de autenticidade </span> não fiscal.
                    </td>
                </tr>
            </tbody>
        </table>
        <!-- Natureza da Operação -->
        <table cellpadding="0" cellspacing="0" class="boxNaturezaOperacao no-top" border="1">
            <tbody>
                <tr>
                    <td>
                        <span class="nf-label">NATUREZA DA OPERAÇÃO</span>
                        <span class="info"> VENDA </span>
                    </td>
                    <td style="width: 84.7mm;">
                        <span class="nf-label">CNPJ</span>
                        <span class="info"><?php echo $emitente[0]->emitente_cnpj ?></span>
                    </td>
                </tr>
            </tbody>
        </table>

        <!-- Inscrição -->
<!--    <table cellpadding="0" cellspacing="0" class="boxInscricao no-top" border="1">
            <tbody>
                <tr>
                    <td>
                        <span class="nf-label">INSCRIÇÃO ESTADUAL</span>
                        <span class="info"><?php echo $emitente[0]->emitente_ie ?></span>
                    </td>
                    <td style="width: 67.5mm;">
                        <span class="nf-label">INSCRIÇÃO ESTADUAL DO SUBST. TRIB.</span>
                        <span class="info"> [nl_company_ie_st] </span>
                    </td>
                    <td style="width: 64.3mm">
                        <span class="nf-label">CNPJ</span>
                        <span class="info"><?php echo $emitente[0]->emitente_cnpj ?></span>
                    </td>
                </tr>
            </tbody>
        </table> -->

        <!-- Destinatário/Emitente -->
        <p class="area-name">Destinatário/Emitente</p>
        <table cellpadding="0" cellspacing="0" class="boxDestinatario" border="1">
            <tbody>
                <tr>
                    <td class="pd-0">
                        <table cellpadding="0" cellspacing="0" border="1">
                            <tbody>
                                <tr>
                                    <td>
                                        <span class="nf-label">NOME/RAZÃO SOCIAL</span>
                                        <span class="info"><?php echo $dados[0]->cliente_nome ?></span>
                                    </td>
                                    <td style="width: 40mm">
                                        <span class="nf-label">CNPJ/CPF</span>
                                        <span class="info"><?php echo $dados[0]->cliente_cpf_cnpj  ?></span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td style="width: 22mm">
                        <span class="nf-label">DATA DE EMISSÃO</span>
                        <span class="info"><?php echo date('d/m/Y') ?></span>
                    </td>
                </tr>
                <tr>
                    <td class="pd-0">
                        <table cellpadding="0" cellspacing="0" border="1">
                            <tbody>
                                <tr>
                                    <td>
                                        <span class="nf-label">ENDEREÇO</span>
                                        <span class="info"><?php echo $dados[0]->cliente_endereco. ', '. $dados[0]->cliente_numero ?></span>
                                    </td>
                                    <td style="width: 47mm;">
                                        <span class="nf-label">BAIRRO/DISTRITO</span>
                                        <span class="info"><?php echo $dados[0]->cliente_bairro ?></span>
                                    </td>
                                    <td style="width: 37.2 mm">
                                        <span class="nf-label">CEP</span>
                                        <span class="info"><?php echo $dados[0]->cliente_cep ?></span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td>
                        <span class="nf-label">DATA DE ENTR./SAÍDA</span>
                        <span class="info"><?php echo date('d/m/Y', strtotime($dados[0]->dataVenda)) ?></span>
                    </td>
                </tr>
                <tr>
                    <td class="pd-0">
                        <table cellpadding="0" cellspacing="0" style="margin-bottom: -1px;" border="1">
                            <tbody>
                                <tr>
                                    <td>
                                        <span class="nf-label">MUNICÍPIO</span>
                                        <span class="info"><?php echo $dados[0]->cliente_cidade ?></span>
                                    </td>
                                    <td style="width: 34mm">
                                        <span class="nf-label">UF</span>
                                        <span class="info"><?php echo $dados[0]->cliente_estado ?></span>
                                    </td>
                                    <td style="width: 28mm">
                                        <span class="nf-label">FONE/FAX</span>
                                        <span class="info"><?php echo $dados[0]->cliente_telefone ?></span>
                                    </td>
                                    <td style="width: 51mm">
                                        <span class="nf-label">CEL</span>
                                        <span class="info"><?php echo $dados[0]->cliente_celular ?></span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td>
                        <span class="nf-label">HORA ENTR./SAÍDA</span>
                        <span class="info"><?php echo date('H:i:s', strtotime($dados[0]->horaVenda)) ?> </span>
                    </td>
                </tr>
            </tbody>
        </table>

        <!-- Fatura -->
     <!--    <div class="boxFatura">
            <p class="area-name">Fatura</p>            
        </div> -->

        <!-- Calculo do Imposto -->
<!--         <p class="area-name">Calculo do imposto</p>
        <div class="wrapper-table">
            <table cellpadding="0" cellspacing="0" border="1" class="boxImposto">
                <tbody>
                    <tr>
                        <td>
                            <span class="nf-label label-small">BASE DE CÁLC. DO ICMS</span>
                            <span class="info"> [tot_bc_icms] </span>
                        </td>
                        <td>
                            <span class="nf-label">VALOR DO ICMS</span>
                            <span class="info"> [tot_icms] </span>
                        </td>
                        <td>
                            <span class="nf-label label-small" style="font-size: 4pt;">BASE DE CÁLC. DO ICMS ST</span>
                            <span class="info"> [tot_bc_icms_st] </span>
                        </td>
                        <td>
                            <span class="nf-label">VALOR DO ICMS ST</span>
                            <span class="info"> [tot_icms_st] </span>
                        </td>
                        <td>
                            <span class="nf-label label-small">V. IMP. IMPORTAÇÃO</span>
                            <span class="info"></span>
                        </td>
                        <td>
                            <span class="nf-label label-small">V. ICMS UF REMET.</span>
                            <span class="info"></span>
                        </td>
                        <td>
                            <span class="nf-label">VALOR DO FCP</span>
                            <span class="info"> [tot_icms_fcp]  </span>
                        </td>
                        <td>
                            <span class="nf-label">VALOR DO PIS</span>
                            <span class="info"></span>
                        </td>
                        <td>
                            <span class="nf-label label-small">V. TOTAL DE PRODUTOS</span>
                            <span class="info"><?php echo $dados[0]->valorTotal ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="nf-label">VALOR DO FRETE</span>
                            <span class="info"> [vl_shipping] </span>
                        </td>
                        <td>
                            <span class="nf-label">VALOR DO SEGURO</span>
                            <span class="info"> [vl_insurance]</span>
                        </td>
                        <td>
                            <span class="nf-label">DESCONTO</span>
                            <span class="info"> [vl_discount] </span>
                        </td>
                        <td>
                            <span class="nf-label">OUTRAS DESP.</span>
                            <span class="info"> [vl_other_expense] </span>
                        </td>
                        <td>
                            <span class="nf-label">VALOR DO IPI</span>
                            <span class="info"> [tot_total_ipi_tax]  </span>
                        </td>
                        <td>
                            <span class="nf-label">V. ICMS UF DEST.</span>
                            <span class="info"></span>
                        </td>
                        <td>
                            <span class="nf-label label-small">V. APROX. DO TRIBUTO</span>
                            <span class="info"> [ApproximateTax]  </span>
                        </td>
                        <td>
                            <span class="nf-label label-small">VALOR DA CONFINS</span>
                            <span class="info"></span>
                        </td>
                        <td>
                            <span class="nf-label label-small">V. TOTAL DA NOTA</span>
                            <span class="info"><?php echo $dados[0]->valorTotal ?> </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div> -->


        <!-- Transportador/Volumes transportados -->
        <!-- 
        <p class="area-name">Transportador/volumes transportados</p>
        <table cellpadding="0" cellspacing="0" border="1">
            <tbody>
                <tr>
                    <td>
                        <span class="nf-label">RAZÃO SOCIAL</span>
                        <span class="info">0</span>
                    </td>
                    <td class="freteConta" style="width: 32mm">
                        <span class="nf-label">FRETE POR CONTA</span>
                        <div class="border">
                            <span class="info"> 0 </span>
                        </div>
                        <p>0 - Emitente</p>
                        <p>1 - Destinatário</p>
                    </td>
                    <td style="width: 17.3mm">
                        <span class="nf-label">CÓDIGO ANTT</span>
                        <span class="info"> [ds_transport_rntc] </span>
                    </td>
                    <td style="width: 21.8mm">
                        <span class="nf-label">PLACA</span>
                        <span class="info">0</span>
                    </td>
                    <td style="width: 11.3mm">
                        <span class="nf-label">UF</span>
                        <span class="info">0</span>
                    </td>
                    <td style="width: 49.5mm">
                        <span class="nf-label">CNPJ/CPF</span>
                        <span class="info">0</span>
                    </td>
                </tr>
            </tbody>
        </table>

        <table cellpadding="0" cellspacing="0" border="1" class="no-top">
            <tbody>
                <tr>
                    <td class="field endereco">
                        <span class="nf-label">ENDEREÇO</span>
                        <span class="content-spacer info">0</span>
                    </td>
                    <td style="width: 32mm">
                        <span class="nf-label">MUNICÍPIO</span>
                        <span class="info">0</span>
                    </td>
                    <td style="width: 31mm">
                        <span class="nf-label">UF</span>
                        <span class="info">0 </span>
                    </td>
                    <td style="width: 51.4mm">
                        <span class="nf-label">INSC. ESTADUAL</span>
                        <span class="info"> 0 </span>
                    </td> 
                </tr>
            </tbody>
        </table>
  
        <table cellpadding="0" cellspacing="0" border="1" class="no-top">
            <tbody>
                <tr>
                    <td class="field quantidade">
                        <span class="nf-label">QUANTIDADE</span>
                        <span class="content-spacer info">0</span>
                    </td>
                    <td style="width: 31.4mm">
                        <span class="nf-label">ESPÉCIE</span>
                        <span class="info">0</span>
                    </td>
                    <td style="width: 31mm">
                        <span class="nf-label">MARCA</span>
                        <span class="info">0</span>
                    </td>
                    <td style="width: 31.5mm">
                        <span class="nf-label">NUMERAÇÃO</span>
                        <span class="info">0</span>
                    </td>
                    <td style="width: 31.5mm">
                        <span class="nf-label">PESO BRUTO</span>
                        <span class="info"> 0 </span>
                    </td>
                    <td style="width: 32.5mm">
                        <span class="nf-label">PESO LÍQUIDO</span>
                        <span class="info"> 0 </span>
                    </td> 
                </tr>
            </tbody>
        </table>

        -->

        <!-- Dados do produto/serviço -->
        <p class="area-name">Dados do produto/serviço</p>
        <div class="wrapper-border">
            <table cellpadding="0" cellspacing="0" border="1" class="boxProdutoServico">
                <thead class="listProdutoServico" id="table">
                    <tr class="titles">
                        <th class="cod" style="width: 15.5mm">CÓDIGO</th>
                        <th class="descrit" style="width: 96.1mm">DESCRIÇÃO DO PRODUTO/SERVIÇO</th>
                        <!-- <th class="descrit" style="width: 66.1mm">DESCRIÇÃO DO PRODUTO/SERVIÇO</th>
                        <th class="ncmsh">NCMSH</th>
                        <th class="cst">CST</th>
                        <th class="cfop">CFOP</th> -->
                        <th class="un">UN</th>
                        <th class="amount">QTD.</th>
                        <th class="valUnit">VLR.UNIT</th>
                        <th class="valTotal">VLR.TOTAL</th>
                       <!--  <th class="bcIcms">BC ICMS</th>
                        <th class="valIcms">VLR.ICMS</th>
                        <th class="valIpi">VLR.IPI</th>
                        <th class="aliqIcms">ALIQ.ICMS</th>
                        <th class="aliqIpi">ALIQ.IPI</th> -->
                    </tr>
                </thead>
                <tbody>

                    <!-- [items] -->

                    <?php 
                    $seq = 1;
                    $totalProdutos = 0;
                
                    switch ($i+1) {
                        case 1:
                            $iniciarItens = 0;
                            break;
                        case 2:
                            $iniciarItens = 35; // 22
                            break;
                        case 3:
                            $iniciarItens = 70; // 44
                        case 4:
                            $iniciarItens = 105; // 66   
                            break;
                    }

                    for ($j=$iniciarItens; $j < count($dados) ; $j++){     
                    $totalProdutos = $totalProdutos + $dados[$j]->subTotal; 
                    ?>
                    <tr>
                        <?php // echo  $seq ++ ?>
                        <td align="left"><div style='text-align:left;font-size:9px;'><?php   echo $dados[$j]->produto_codigo ?></div></td>
                        <td align="left"><div style='text-align:left;font-size:9px;'><?php   echo $dados[$j]->produto_descricao.' '.$dados[$j]->imei_valor ?></div></td>    
                      <!--   <td align="left"><div style='text-align:left;font-size:9px;'></div></td>
                        <td align="left"><div style='text-align:left;font-size:9px;'></div></td>
                        <td align="left"><div style='text-align:left;font-size:9px;'></div></td> -->
                        <td align="left"><div style='text-align:left;font-size:9px;'><?php   echo $dados[$j]->produto_unidade ?></div></td>  
                        <td align="left"><div style='text-align:left;font-size:9px;'><?php echo $dados[$j]->quantidade ?></div></td>
                        <td align="left"><div style='text-align:left;font-size:9px;'>R$<?php  echo $dados[$j]->valor_unitario ?></div></td>
                        <td align="left"><div style='text-align:left;font-size:9px;'><?php  echo 'R$ '. number_format($dados[$j]->subTotal,2,",",".") ?></div></td>
                        <!-- <td align="left"><div style='text-align:left;font-size:9px;'></div></td>
                        <td align="left"><div style='text-align:left;font-size:9px;'></div></td>
                        <td align="left"><div style='text-align:left;font-size:9px;'></div></td>
                        <td align="left"><div style='text-align:left;font-size:9px;'></div></td>
                        <td align="left"><div style='text-align:left;font-size:9px;'></div></td>   -->                  
                    </tr>

                    <?php 
                    // if($seq == 22){
                    if($seq == 22){    
                        break;                        
                    }
                        $seq++;
                    ?> 

                <?php } ?>

                </tbody>
            </table>
        </div>

        <!-- Calculo de ISSQN -->
        <p class="area-name">Pagamento</p>
        <table cellpadding="0" cellspacing="0" border="1" class="boxIssqn">
            <tbody>

                <tr>
                    <td class="field inscrMunicipal">
                        <span class="nf-label">FORMA PAGAMENTO</span>
                        <?php foreach ($financeiro as $f) { ?>
                           <span class="info txt-center"> <?php echo ($f->financeiro_parcela_cart) ? $f->financeiro_forma_pgto.' '.$f->financeiro_parcela_cart.' x' : $f->financeiro_forma_pgto  ?><?php echo ' - R$ '. number_format($f->financeiro_valor,2,",",".");  ?> </span>
                        <?php } ?>
                        
                    </td>
                    <td class="field valorTotal">
                        <span class="nf-label">VALOR TOTAL DOS PRODUTO(s)</span>
                        <span class="info txt-right"><?php echo 'R$ '. number_format($dados[0]->valorTotal,2,",",".")  ?> </span>
                    </td>
             <!--        <td class="field baseCalculo">
                        <span class="nf-label">BASE DE CÁLCULO DO ISSQN</span>
                        <span class="info txt-right"> [tot_bc_issqn] </span>
                    </td>
                    <td class="field valorIssqn">
                        <span class="nf-label">VALOR DO ISSQN</span>
                        <span class="info txt-right"> [tot_issqn] </span>
                    </td> -->
                </tr>
            </tbody>
        </table>


                <!-- Observações Gerai -->
        <p class="area-name">Observações Gerais</p>
        <table cellpadding="0" cellspacing="0" border="1">
            <tbody>
                <tr>
                    <!-- <td colspan="2" class="txt-upper"> -->
                    <td colspan="2" >
                        <p  style='padding: 1px 15px; font-size:6.1pt;font-family:"Arial","sans-serif"'>

                        <!-- <?php echo $configuracaosite[0]->conf_venda_cell ?>  -->

                        </p>
                        
                       <p  style='padding: 1px 15px; font-size:6.1pt;font-family:"Arial","sans-serif"'>         
                        O aparelho adquirido possui garantia de
                        180 (cento e oitenta) dias corridos ( APARELHO VITRINE OU SEMINOVO) , no mesmo estabelecimento onde foi adquirido, de acordo com o artigo 26, II do Código de Defesa do Consumidor.
                        365 (trezentos e sessenta e cinco) dias corridos ( XIAOMIS LACRADOS) , no mesmo estabelecimento onde foi adquirido, de acordo com o artigo 26, II do Código de Defesa do Consumidor.<br>

                        O aparelho adquirido também possui manutenção de forma vitalícia. 
                        Precisando de algum reparo, o cliente pagará apenas a peça. Perde-se a garantia, se o aparelho for repassado para terceiros.
                        A contagem do prazo se inicia a partir da entrega efetiva do produto, de acordo com artigo 26, §1º do Código de Defesa do Consumidor.
                        Em caso de defeito, dentro do prazo estipulado acima, cliente deve se dirigir a assistência técnica própria da empresa.
                        O SELO FAZ PARTE DA GARANTIA.<br>
                        CASO O SELO DO APARELHO SEJA REMOVIDO, O CLIENTE PERDERÁ A GARANTIA TOTAL DO APARELHO.<br>

                        FIX TOUCH Rua Padre carapuceiro,706 sala 102
                        Empresarial Carlos Pena Filho(81)99482-5732/ (81)99644-7376<br>
                         
                        A garantia não se aplica quando ficar comprovado pela análise técnica que o problema apresentado no aparelho foi causado por culpa exclusiva do consumidor ou de terceiros. Ou seja, quando ficar comprovado que o problema apresentado foi derivado de mau uso e não um defeito de fabricação do aparelho, de acordo com o artigo 14, §3º, II do Código de Defesa do Consumidor.<br>

                        SENHAS PESSOAIS, SÃO RESPONSABILIDADE DO CONSUMIDOR.<br>
                        A loja não se responsabiliza em esquecimento do cliente em senhas de Icloud, tela, ou qualquer outro tipo de senha. Todas as senhas são responsabilidade TOTAL do consumidor. O esquecimento, é caracterizado como mal uso.<br>
                         
                        NÃO serão aceitos para reparo aparelhos com IMEI diferente do que consta no recibo de compra.
                        Ficando comprovado defeito de fabricação no aparelho, cliente terá seu aparelho devolvido com os devidos reparos dentro do prazo de 30 DIAS
                        artigo 18 da Defesa do Consumidor, tanto o fornecedor quanto o fabricante têm 30 dias para sanar o problema do produto (TROCA OU REPARO). Após este prazo o consumidor pode exigir um produto similar, a devolução total do dinheiro ou ainda o abatimento proporcional do preço do produto.<br>
                        NÃO são aceitas devoluções por arrependimento / desistência da compra, visto que não existe direito de arrependimento nas compras efetuadas em lojas físicas.
                      </p> 
                                             
                    </td>
                </tr>
                <tr>
                    <td style="width: 78mm">
                        <span class="nf-label">PRAZO DA GARANTIA LIMITADA</span>
                    </td>
                    <td style="width: 78mm">
                        <span class="nf-label">DATA DE EXPIRAÇÃO</span>
                    </td>
                </tr>
            </tbody>
        </table>


        <p class="area-name"></p>
        <table cellpadding="0" cellspacing="0" border="1">
            <tbody>
                <tr>
                    <!-- <td colspan="2" class="txt-upper"> -->
                    <td colspan="2">
                    <p  style='padding: 1px 15px; font-size:6.1pt;font-family:"Arial","sans-serif"'>     
                    Adquirindo qualquer produto SEMINOVO COM GARANTIA APPLE, estou ciente que a garantia será direto com uma LOJA AUTORIZADA APPLE.
                    As condições não podem violar as regras de garantia da APPLE já mencionadas de forma resumida acima e que podem ser consultadas direto no site da Apple:
                    (www.apple.com/legal/warranty/products/ios-warranty-brazilian-portuguese.html).</p>
                    </td>
                </tr>
                <tr>
                    <td style="width: 78mm">
                        <span class="nf-label">PRAZO DA GARANTIA LIMITADA APPLE</span>
                    </td>
                    <td style="width: 78mm">
                        <span class="nf-label">DATA DE EXPIRAÇÃO</span>
                    </td>
                </tr>
            </tbody>
        </table>

        <p class="area-name"></p>
        <table cellpadding="0" cellspacing="0" border="1">
            <tbody>
                <tr>
                    <!-- <td colspan="2" class="txt-upper"> -->
                    <td colspan="2">
                    <p  style='padding: 1px 15px; font-size:6.1pt;font-family:"Arial","sans-serif"'>     
                    Adquirindo qualquer produto LACRADO COM GARANTIA APPLE, estou ciente que a garantia de 1 ano será direto com uma LOJA AUTORIZADA APPLE.
                    As condições não podem violar as regras de garantia da APPLE já mencionadas de forma resumida acima e que podem ser consultadas direto no site da Apple:
                    (www.apple.com/legal/warranty/products/ios-warranty-brazilian-portuguese.html).</p>
                    </td>
                </tr>
                <tr>
                    <!-- <td colspan="2" class="txt-upper"> -->
                    <td colspan="2">
                    <p  style='padding: 1px 15px; font-size:6.1pt;font-family:"Arial","sans-serif"'>     
                    Estou ciente que o produto adquirido pode ser NACIONAL ou IMPORTADO, fato que não interfere em relação a sua garantia apple no Brasil.</p>
                    </td>
                </tr>
                <tr>
                    <!-- <td colspan="2" class="txt-upper"> -->
                    <td colspan="2">
                    <p  style='padding: 1px 15px; font-size:6.1pt;font-family:"Arial","sans-serif"'>     
                    Colocando no négocio como forma de pagamento algum aparelho/produto, declaro para os devidos fins que o aparelho abaixo mencionado é de minha propiedade e
                    que responderei de forma penal e civil por fatos ocorridos com ele até a presente data. Declaro também que o mesmo não tem nenhuma restrição, não é furtado ou
                    roubado, não tem vínculos com operadoras e está em perfeitas condições de uso.
                    </p>
                    </td>
                </tr>
                <tr>
                    <td style="width: 78mm">
                        <span class="nf-label">MODELO DO APARELHO</span>
                        <span><?php // echo $result->modelo_aparelho ?></span>

                    </td>
                    <td style="width: 78mm">
                        <span class="nf-label">IMEI DO APARELHO</span>
                        <span><?php // echo $result->imei_aparelho ?></span>

                    </td>
                </tr>
            </tbody>
        </table>





        <!-- Dados adicionais -->
        <p class="area-name"></p>
        <table cellpadding="0" cellspacing="0" border="1" class="boxDadosAdicionais">
            <tbody>
                <tr>
                    <td class="field infoComplementar">
                        <span class="nf-label">ASSINATURA DO CLIENTE</span>
                        <span></span>
                    </td>
                    <td class="field reservaFisco" style="height: 10mm">
                        <span class="nf-label">ASSINATURA VENDEDOR</span>
                        <span></span>
                    </td>
                </tr>
            </tbody>
        </table>

        <footer>
            <table cellpadding="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td style="text-align: right"><strong>Empresa de Software www.wdmtecnologia.com.br</strong></td>
                    </tr>
                </tbody>
            </table>
        </footer>
    </div>
    <!-- [page-break] -->

    <?php } ?>
    
</div>

