<?php
 
  $filial = BDCAMINHO;
  $filial = explode("_", $filial);
  $filial = strtoupper($filial[2]);

?>
<section class="content">
  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"><?php echo ($dados[0]->faturado == '0') ? '<span class="label label-danger">#NaoFaturado</span>' : ''; ?></h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/editarExe" method="post">
        <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $dados[0]->idVendas; ?>" placeholder="id">        
          <div class="box-body">

            <h3>#Venda:<?php echo $dados[0]->idVendas ?></h3>
 

            <div class="form-group">
              <label for="cliente" class="col-sm-2 control-label">Cliente </label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" name="cliente_nome" id="cliente_nome" value="<?php echo $dados[0]->cliente_nome ?>" placeholder="Cliente">
                <input type="hidden" id="cliente_id" name="cliente_id" value="<?php echo $dados[0]->clientes_id ?>">
              </div>
            </div>
            <div class="form-group">
              <label for="dataVenda" class="col-sm-2 control-label">Data da Venda </label>
              <div class="col-sm-5">              
                <input type="date" class="form-control" name="dataVenda" value="<?php echo $dados[0]->dataVenda; ?>" placeholder="">
              </div>
            </div>
            <div class="form-group">
              <label for="observacao" class="col-sm-2 control-label">Observação </label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" name="observacao" id="observacao" value="<?php echo $dados[0]->observacao ?>" placeholder="Observação">
              </div>
            </div> 
            <div class="form-group">
              <label for="nota" class="col-sm-2 control-label">Emitir Nota </label>
              <div class="col-sm-5"> 

                <input  type="checkbox" class="form-check-input" id="nota" name="nota" value="1" <?php echo $dados[0]->emitirNota =='1'?'checked':'';?>   >
              </div>
            </div>  


            <div class="span12" style="padding: 1%; margin-left: 0">

                <div class="span8 offset2" style="text-align: center"> 
                    <button type="submit" class="btn btn-primary">Alterar</button>     
                      
                      <a href="<?php echo base_url(); ?>vendas/visualizarDanfe/<?php echo $dados[0]->idVendas; ?>" target="_blank" data-toggle="tooltip" title="Finalizar" class="btn btn-danger"><i class="icon-fa-eye"></i> Visualizar Danfe</a>

                      <a href="<?php echo base_url(); ?>vendas/visualizarNtGarantia/<?php echo $dados[0]->idVendas; ?>" target="_blank" data-toggle="tooltip" title="Finalizar" class="btn btn-warning"><i class="icon-fa-eye"></i> Visualizar Nota Garantia</a>

            
                    <?php if($dados[0]->faturado == '1'){  ?>
                      <a href="#" onClick="(function(){window.open('<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/visualizar/<?php echo $dados[0]->idVendas; ?>','MyWindow','toolbar=no,location=no,directories=no,status=no,menubar=yes,scrollbars=yes,resizable=yes,width=350,height=600');return false;})();return false;"data-toggle="tooltip" title="Visualizar" class="btn btn-info"><i class="icon-fa-eye"></i> Visualizar Venda</a>
                    <?php }elseif ($dados[0]->faturado == '0') { ?>
                      <a href="#" onClick="(function(){window.open('<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/visualizar/<?php echo $dados[0]->idVendas; ?>','MyWindow','toolbar=no,location=no,directories=no,status=no,menubar=yes,scrollbars=yes,resizable=yes,width=350,height=600');return false;})();return false;"data-toggle="tooltip" title="Visualizar" class="btn btn-info"><i class="icon-fa-eye"></i> Visualizar Venda</a>
                    <?php  } else {  ?>

                      <?php if (SUBDOMINIO == 'localhost' OR GRUPOLOJA == 'grupocell' OR GRUPOLOJA == 'gruporeccell' OR GRUPOLOJA == 'grupobyterecife')  { ?>
                      <a href="<?php echo base_url(); ?>Pdvtablet/finalizarVenda/<?php echo $dados[0]->idVendas; ?>" data-toggle="tooltip" title="Finalizar" class="btn btn-danger"><i class="icon-fa-eye"></i> Finalizar Venda</a>
                      <?php }else{ ?>
                      <a href="<?php echo base_url(); ?>pdv2/finalizarVenda/<?php echo $dados[0]->idVendas; ?>" data-toggle="tooltip" title="Finalizar" class="btn btn-danger"><i class="icon-fa-eye"></i> Finalizar Venda</a>
                      <?php } } ?>

                    <?php  $contador = 0; foreach ($financeiro as $key) { 
                      if ($key->financeiro_forma_pgto === 'Dinheiro' and $dados[0]->dataVenda == date('Y-m-d') and $contador == 0) { ?>


                        <a style="cursor:pointer;" data-toggle="modal" data-target="#modalSenhaInverte" onclick="idVendasModalInverte('<?php echo $dados[0]->idVendas; ?>', '<?php echo $key->idFinanceiro ?>');" class="btn btn-success">Inverter Venda Fiado</a>

                    <?php  $contador++; } } ?>
                    <a href="<?php echo base_url() ?>index.php/vendas" class="btn btn-default"><i class="icon-arrow-left"></i> Voltar</a>
                </div>
            </div> 



          </div>

      </form>

        <fieldset class="col-md-12">
            <legend>Dados dos Produtos</legend>

        </fieldset> 

          <div class="box-footer">
                        
              <!-- /.box-header -->
              <div id="divProdutos" class="box-body no-padding">
                <table class="table table-condensed">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>LJ</th>
                      <th>Produto</th>
                      <th>Imei</th>
                      <th>Quantidade</th>
                      <th>Minimo</th>
                      <th>Venda</th>
                      <th>SubTotal</th>                      
                      <th style="width: 40px">Ações</th>
                    </tr>
                  </thead>  
                  <tbody>
                    <?php
                    $total = 0;
                    $qtd = 0;
                    $seq = 1;
                     // echo "<pre>";
                     // var_dump($produtos);die();
                    $totalGeral = 0;
                    foreach ($produtos as $p) {  $qtd = $qtd + $p->quantidade; ?>
                        <tr>
                        <td><?php echo $seq ++ ?></td>
                        <td><?php echo substr(strtoupper($p->filial), 0, 2) ?></td>
                        <td><?php echo $p->produto_descricao ?></td>
                        <?php if (!empty($p->imei_valor)) { ?>
                           <td><u><a style="cursor:pointer;color:#000;" title="Trocar" data-toggle="modal" data-target="#trocaImei"
                           onclick="trocaImei('<?php echo $dados[0]->idVendas ?>', '<?php echo $p->produto_descricao ?>', '<?php echo $p->produto_id ?>', '<?php echo $p->idItens ?>', '<?php echo $p->imei_valor ?>','<?php echo $p->prod_preco_custo ?>','<?php echo ($p->subTotal/($p->quantidade == 0 ?  1 : $p->quantidade )) ?>')" >
                           <?php echo $p->imei_valor ?></a></u></td>
                        <?php }else { ?> <td></td> <?php }?>
                        <td><?php echo $p->quantidade ?></td>
                        <td><?php echo $p->produto_preco_minimo_venda ?></td>
                        <td><?php echo $p->produto_preco_venda ?></td>
                        <td><?php echo $p->subTotal ?></td>
                        <?php  $totalGeral += $p->subTotal ; ?>


                      <?php if ($dados[0]->faturado == '0') { ?>  
                      <td>
                        <a title="SEM PERMISSÃO"><i class="fa fa-unlock-alt  text-danger"></i></a>
                      </td> 
                      <?php } else { ?>                  

                      <?php if (substr(strtoupper($p->filial), 0, 2) == substr(strtoupper($filial), 0, 2)) { ?>
                                                         
                      <td> 
                        <a style="cursor:pointer;" title="Deletar" data-toggle="modal" data-target="#excluirVendas"
                      onclick="excluirVendas('<?php echo $dados[0]->idVendas ?>', '<?php echo $p->quantidade ?>', '<?php echo $p->produto_descricao ?>', '<?php echo $p->produto_id ?>', '<?php echo $p->idItens ?>','<?php echo $p->prod_preco_custo ?>','<?php echo ($p->subTotal/($p->quantidade == 0 ?  1 : $p->quantidade )) ?>' )" >
                      <i class="fa fa-trash  text-danger"></i></a>
                      </td>

                       <?php } else {  ?>
                         <td>
                          <a title="SEM PERMISSÃO"><i class="fa fa-unlock-alt  text-danger"></i></a>
                         </td> 
                       </tr> 
                      <?php } ?>  

                      <?php } ?> 
                      
                      <?php } ?>
                                         

                  </tbody>
                  <tfoot>
                    <tr>                     
                      <th>ITENS:<?php echo ' '.count($produtos); ?></th>
                      <th></th>                      
                      <th>VOLUME:<?php echo ' '.$qtd; ?></th>
                      <th></th>  
                      <th></th>  
                      <th colspan="2" >TOTAL GERAL: <?php echo ' '.number_format($totalGeral,2,".",".");?></th> 
                      <input type="hidden" name="total" id="total" value="<?php echo number_format($totalGeral,2,".",".");?>">                     
                    </tr>
                  </tfoot>
                </table>
              </div>            

          </div> 

          <!-- /.box-body -->
        <!--   <div class="box-footer">
            <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>" class="btn btn-default">Voltar</a>
            <button type="submit" class="btn btn-primary pull-right"><?php echo ucfirst($this->uri->segment(2)); ?></button>
          </div> -->
          <!-- /.box-footer -->


      </div>
    </div>
  </div>
</section>


<div class="example-modal">
  <div class="modal" id="excluirVendas">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Excluir Itens da Vendas</h4>
        </div>
        <div class="modal-body">
        <div class="row">
        <form role="form" action="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/excluirProduto"  method="post" enctype="multipart/form-data">
          <input class="form-control" id="idVendas" name="idVendas" type="hidden" value="">
          <div class="col-xs-10">
            <label class="control-label" for="produtoM">Produto</label>
            <div class="form-group">
              <input class="form-control" id="produtoM" name="produtoM" type="text" value="" disabled="disabled">
              <input class="form-control" id="produtoID" name="produtoID" type="hidden" value="">
              <input class="form-control" id="idItens" name="idItens" type="hidden" value="">
              <input class="form-control" id="precoCusto" name="precoCusto" type="hidden" value="">
              <input class="form-control" id="precoVendido" name="precoVendido" type="hidden" value="">
            </div>
          </div>

          <div class="col-xs-3">
            <label class="control-label" for="quantidade">Quantidade</label>
            <div class="input-group">
              <span class="input-group-addon">
                <button style="background:transparent;border:none" id="btn" disabled></button>
              </span>
              <input class="form-control" id="quantidadeH" name="quantidadeH" type="hidden" value="">
              <input class="form-control" id="quantidadeM" name="quantidadeM" type="text" value="">

            </div>
          </div>

          <div class="col-xs-7">
            <label class="control-label" for="motivo">Motivo</label>
            <div class="form-group">
             <select required class="form-control" name="motivo">
                        <option value='1'>Avaria do Produto</option>
                        <option value='2'>Reembolso do Produto</option>
                        <option value='3'>Avaria com Reembolso</option>
             </select>
            </div>
          </div>

          <div class="col-xs-10">
            <label for="fornecedor_id" class="control-label">Forncedor</label>
            <div class="form-group">                     
              <select class="form-control" name="fornecedor_id" id="fornecedor_id">
                <?php foreach ($fornecedor as $valor) { ?>
                  <option value='<?php echo $valor->fornecedor_id; ?>'><?php echo $valor->fornecedor_nome; ?> </option>
                <?php } ?>
              </select>
            </div>
          </div>

          <div class="col-xs-12">
            <label class="control-label" for="tipo">Observações</label>
            <div class="form-group">
              <textarea cols='70' rows='5' style="resize: none" name="observacao"></textarea>
            </div>
          </div>



        </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-danger">Excluir</button>
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
          <!-- <a href="" class="btn btn-danger" id="exVendas">Excluir</a> -->
        </div>
      </div> 
      </form>        
    </div>          
  </div>        
</div>  


<div class="example-modal">
  <div class="modal" id="trocaImei" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Troca de Aparelho</h4>
        </div>
        <div class="modal-body">
        <div class="row">
        <form role="form" action="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/trocaImei"  method="post" enctype="multipart/form-data">
          <input class="form-control" id="idVendasM" name="idVendasM" type="hidden" value="">
          <div class="col-xs-10">
            <label class="control-label" for="imeiM">Número do Imei</label>
            <div class="form-group">
              <input class="form-control" id="imeiM" name="imeiM" type="text" value="" disabled="disabled">
              <input class="form-control" id="antigoImei" name="antigoImei" type="hidden" value="">
              <input class="form-control" id="produtoIDM" name="produtoIDM" type="hidden" value="">
              <input class="form-control" id="idItensM" name="idItensM" type="hidden" value="">
              <input class="form-control" id="precoCustoM" name="precoCustoM" type="hidden" value="">
              <input class="form-control" id="precoVendidoM" name="precoVendidoM" type="hidden" value="">
            </div>
          </div>
          
          <div class="col-xs-10">
            <label class="control-label" for="motivo">Motivo</label>
            <div class="input-group">
              <input class="form-control" id="motivoM" name="motivoM"  value="Troca de Aparelho" disabled="disabled">
              <input class="form-control" id="motivoMT" name="motivoMT" type="hidden" value="4">
              <div class="input-group-btn senhaGerenteRed">
                <button type="button" class="btn btn-danger" id="senhaGerente"><i class="fa fa-lock"></i></button>
              </div>
              <div class="input-group-btn senhaGerenteGreen" style="display:none;">
                <button type="button" class="btn btn-success"><i class="fa fa-unlock"></i></button>
              </div>
            </div><br>
          </div>

          <div class="col-xs-10">
             <p id="imeiSenhaAlert" style="display: none; color: red;">Senha Invalida!</p>
          </div>

          <div class="col-xs-10" id="imeiSenha" style="display:none;">
            <label class="control-label" for="imeiSenhaInput">Senha Gerente</label>
            <div class="input-group">
              <input class="form-control" id="imeiSenhaInput" name="imeiSenhaInput" type="password" value="">
              <div class="input-group-btn">
                <button type="button" class="btn btn-success" id="senhaGerenteEnviar"><i class="fa fa-unlock"></i></button>
              </div>           
            </div><br>
          </div>
        
          <div class="col-xs-12">
            <label for="novoImei" class="control-label">Novos Imei</label>
            <div class="col-xs-12">                     
              <select  class="select2"  name="novoImei" id="novoImei">
              </select>
            </div>
          </div>

          <div class="col-xs-12">
            <label class="control-label" for="tipo">Observações</label>
            <div class="col-xs-12">
              <textarea class="col-xs-10 form-control" rows='5' style="resize: none" name="observacaoM"></textarea>
            </div>
          </div>

        </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-danger">Alterar</button>
          <button type="button" class="btn btn-default pull-left" onclick="closeModal()" data-dismiss="modal">Cancelar</button>
        </div>
      </div> 
      </form>        
    </div>          
  </div>        
</div>  


 <!--  MODAL  -->
 <div class="modal fade" id="modalSenhaInverte" data-backdrop="static" data-keyboard="false" style="z-index: 99999" tabindex="-1" role="dialog">
    <div class="modal-dialog" style="width: 350px; margin: 100px auto">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" onclick="closeModalInverte()" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title text-center">Informar senha</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <p>Informe a senha do gerente para liberação desta ação</p>
              <div class="form-group">
                <input type="password" onkeyup="habilitarBTN(this.value)" id="input-liberar-senha" class="form-control" placeholder="Senha do gerente" />
                <input type="hidden" id="idVendasModal"/>
                <input type="hidden" id="idFinanceiroModal"/>
              </div>
              <button id="btn-confirm" disabled class="btn btn-primary btn-block">CONFIRMAR</button>
            </div>
          </div>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>


<script>

  $('#senhaGerente').click(function () { 
    $('#imeiSenha').show();
    $('#imeiSenhaAlert').hide();
    document.querySelector('#imeiSenhaInput').value = null;
  });


  function closeModal() {
    $('#imeiSenha').hide();
    $('#imeiSenhaAlert').hide();
    $('.senhaGerenteRed').show();
    $('.senhaGerenteGreen').hide();
    document.querySelector('#imeiSenhaInput').value = null;
    $('#motivoM').val('Troca de Aparelho');
    $('.senhaGerente').html('<button type="button" class="btn btn-danger" id="senhaGerente"><i class="fa fa-lock"></i></button>');
  }

  $('#senhaGerenteEnviar').click(function(event) {

    let senha = $('#imeiSenhaInput').val();

    $.ajax({
      method: "POST",
      url: base_url+"pdv2/validacaoGerente/",
      dataType: "JSON",
      data: { senha },
      success: function(data)
      {
        if(data.result == true){
          $('#motivoM').val('Troca de Aparelho Gerente');
          $('#motivoMT').val('5');
          $('.senhaGerenteRed').hide();
          $('.senhaGerenteGreen').show();
          $('#imeiSenha').hide();
          trocaImeiTodos();
          document.querySelector('#imeiSenhaInput').value = null;
        }else{
          $('#imeiSenha').hide();
          $('#imeiSenhaAlert').show();
        }
      }
      
    });
  }); 


  function trocaImeiTodos() {

    $.ajax({
      method: "POST",
      url: base_url+"vendas/buscarImeiTodos/",
      dataType: "html"
      })
      .done(function( response ) {

      $('#novoImei').html(response);
      
    });

  }

  function habilitarBTN(valor) {
    const el = document.querySelector('#btn-confirm')
    if(valor.length > 0) {
      el.disabled = false
    } else {
        el.disabled = true
    }
  }

  function idVendasModalInverte(idVendas, idFinanceiro) {
    $('#idVendasModal').val(idVendas);
    $('#idFinanceiroModal').val(idFinanceiro);
  }

  function closeModalInverte() {
      $('#modalSenhaInverte').modal('hide');
      document.querySelector('#input-liberar-senha').value = null;
      document.querySelector('#idVendasModal').value = null;
      document.querySelector('#idFinanceiroModal').value = null;
      const el = document.querySelector('#btn-confirm')
      el.disabled = true
  }

  $('#btn-confirm').click(function(event) {

      let senha = $('#input-liberar-senha').val();
      let id = $('#idVendasModal').val();
      let idFinanceiro = $('#idFinanceiroModal').val();

      $.ajax({
        method: "POST",
        url: base_url+"pdv2/validacaoGerente/",
        dataType: "JSON",
        data: { senha },
        success: function(data)
        {
          if(data.result == true){

            $.ajax({
              method: "POST",
              url: base_url+"vendas/inverteVenda/",
              dataType: "JSON",
              data: { id: id, idFinanceiro: idFinanceiro },
              success: function(data)
              {
                if(data.result == true){
                  alert('Dados alterados com sucesso!');
                  location.reload();
                }else{
                  alert('Algo deu errado. Tente Novamente!');
                  location.reload();
                }
              }
              
            });

      
          }else{
            alert('Senha Invalida.');
            location.reload();
          }
        }
        
      });
  }); 

</script>



