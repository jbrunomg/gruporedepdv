
    
<style type="text/css" media="all">
	@media print {
		.no-print { display: none; }
		#wrapper { max-width: 480px; width: 100%; min-width: 250px; margin: 0 auto; }
		.pagebreak { page-break-before: always; } /* page-break-after works, as well */
	}

	p{
	font-family:Arial, Helvetica, sans-serif;
	}

	
</style>
<body>
<div class='container' style='max-width:300px;padding:5px 5px;'<table width="100%">
<tr>
<h2 style='text-align:center;font-size: 16px;margin-top:0px;margin-bottom:5px;'><i>Troca Mercadoria</i></h2>

<td colspan="2"><div style='font-size:10px;'><?php echo $emitente[0]->emitente_nome ?><br><?php echo ($emitente[0]->emitente_cnpj == '') ? '' : 'CNPJ: '.$emitente[0]->emitente_cnpj ; ?><?php echo ($emitente[0]->emitente_ie == '') ? '' : 'I.E.: '.$emitente[0]->emitente_ie;?><br><?php echo $emitente[0]->emitente_rua ?><br><?php echo $emitente[0]->emitente_bairro ?>, <?php echo $emitente[0]->emitente_cidade.' - '.$emitente[0]->emitente_uf?></div><hr></td>
</tr>
<tr>
<!-- <td colspan="3" class="menor"><div style='text-align:center;font-size:11px;'>Cupom - Documento auxiliar<br> da Nota de Consumidor</td> -->
</tr>
<tr>
<!-- <td colspan="3" class="menor"><br><b>Não permite aproveitamento de crédito de ICMS<b></td> -->
</tr>
</div></table>
<hr>
<table width='100%'>
<tr>
<td>Cupom nº <?php echo $troca ?></td>
<td>Venda: <?php echo $dados[0]->venda_id ?></td>
<td><?php echo date("d/m/Y h:i:sa"); ?></td>
</tr>
<tr>
<td colspan="3">De: <?php echo $antigo[0]->produto_descricao.' - '.$antigo[0]->imei_valor ?></td>
</tr>
<tr>
<td colspan="3">Para: <?php echo $novo[0]->produto_descricao.' - '.$novo[0]->imei_valor?></td>
<tr>
<tr>
<td colspan="3">Vendendor: <b><?php echo $vendendor ?></b></td>
</tr>
<tr>
</table>

<hr><table width="100%">
<table width="80%" align="center">
<tr>
<td colspan="3"><b>Motivo <?php echo $dados[0]->produto_avaria_motivo == '4' ?  'Troca de Aparelho' : 'Troca de Aparelho Gerente' ?></b></td>
</tr>
</table>
<table width="80%" align="center">
<tr>
<td colspan="3"><b><?php echo $dados[0]->produto_avaria_observacao ?></b></td>
</tr>
</table>
<hr><table width="100%">
<tr>
<!-- <td  colspan="3"><div style='word-break: break-all;font-size:11px'>Consulte pela Chave de Acesso em <br>nfce.sefaz.pe.gov.br/nfce/consulta<br />26191124942487000190650010000002521287177940</div></td> -->
</tr>
</table>
<table width="80%" align="center">
<tr>
<td colspan="3"><b><?php echo $cliente ?></b></td>
</tr>
</table>
<table width="80%" align="center">
<td colspan="3">Data de autorização: <?php echo $dataVenda ?><br />Data de modificação: <?php echo $dataAtualizacao ?> </td>
</tr>
</table>
<table width="100%">
<tr>

</tr>
</table>

<div class="pagebreak"> </div>

<hr>
<table width="80%" align="center">
<tr>
<h2 style='text-align:center;font-size: 16px;margin-top:0px;margin-bottom:5px;'><i>2º Via - Loja</i></h2>
</tr>
<tr>
<td>Cupom nº <?php echo $troca ?></td>
<td>Venda: <?php echo $dados[0]->venda_id ?></td>
<td><?php echo date("d/m/Y h:i:sa"); ?></td>
</tr>
<tr>
<td colspan="3">De: <?php echo $antigo[0]->produto_descricao.' - '.$antigo[0]->imei_valor ?></td>
</tr>
<tr>
<td colspan="3">Para: <?php echo $novo[0]->produto_descricao.' - '.$novo[0]->imei_valor?></td>
<tr>
<td colspan="3">Vendendor: <b><?php echo $vendendor ?></b></td>
</tr>
<tr>

<table width="80%" align="center">
<tr>
<td colspan="3"><b>Motivo <?php echo $dados[0]->produto_avaria_motivo == '4' ?  'Troca de Aparelho' : 'Troca de Aparelho Gerente' ?></b></td>
</tr>
</table>
<table width="80%" align="center">
<tr>
<td colspan="3"><b><?php echo $dados[0]->produto_avaria_observacao ?></b></td>
</tr>
</table>
<hr>
<table width="80%" align="center">
<tr>
<td colspan="3"><b><?php echo $cliente ?></b></td>
</tr>
</table>

<td colspan="3">Data de autorização: <?php echo $dataVenda ?><br />Data de modificação: <?php echo $dataAtualizacao ?> </td>
</tr>
</table>
<hr>

<table width="100%" class="no-print">
	<tr>
		<td colspan="3">
			<button
				style="border: 0; cursor: pointer; background: #367fa9; display: block; width: 100%; padding: 10px; text-align: center; text-transform: uppercase;color: #fff"
				href="javascript:window.print()"
				id="web_print" class="btn btn-block btn-primary"
				onClick="window.print();return false;">
				Impressão Web
			</button>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<button
				style="border: 0; cursor: pointer; background: #e08e0b; display: block; width: 100%; padding: 10px; text-align: center; text-transform: uppercase;color: #fff"
				href="javascript:window.print()"
				id="web_print" class="btn btn-block btn-primary"
				onClick="javascript:window.close();return false;">
				Fechar
			</button>
		</td>
	</tr>

</table>
<div style="clear:both;"></div>
<div class="col-xs-12 no-print" style="background:#F5F5F5; padding:10px;">
	<font size="-2">
		<p style="font-weight:bold;">Favor alterar as configurações de impressão de seu browser</p>
		<p style="text-transform: capitalize;"><strong>FireFox:</strong> Arquivo &gt; Configurar impressora &gt; Margem &amp;Cabeçalho/Rodapé --Nenhum--</p>
		<p style="text-transform: capitalize;"><strong>Chrome:</strong> Menu &gt; Impressora &gt; Disabilitar Cabeçalho/Rodapé Opções &amp; Setar margem em branco</p></div>
<font>
	<div style="clear:both;"></div>
</div>
</body>
</html>
