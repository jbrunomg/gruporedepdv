<style type="text/css" media="all">
	@media print {
		.no-print { display: none; }
		#wrapper { max-width: 480px; width: 100%; min-width: 250px; margin: 0 auto; }
		.pagebreak { page-break-before: always; } /* page-break-after works, as well */
	}
	p {
		font-family: Arial, Helvetica, sans-serif;
	}
</style>
<body>
<div class='container' style='max-width:300px;padding:5px 5px;'>

	<h2 style='text-align:center;font-size: 16px;margin-top:0px;margin-bottom:5px;'><i>Troca Mercadoria</i></h2>
	<div style='font-size:10px;'>
		<?php echo $emitente[0]->emitente_nome ?><br>
		<?php echo ($emitente[0]->emitente_cnpj == '') ? '' : 'CNPJ: '.$emitente[0]->emitente_cnpj; ?>
		<?php echo ($emitente[0]->emitente_ie == '') ? '' : 'I.E.: '.$emitente[0]->emitente_ie; ?><br>
		<?php echo $emitente[0]->emitente_rua ?><br>
		<?php echo $emitente[0]->emitente_bairro ?>, <?php echo $emitente[0]->emitente_cidade.' - '.$emitente[0]->emitente_uf ?>
	</div>
	<hr>

	<?php foreach ($resultados as $dado): ?>
		<table width="100%">
			<tr>
				<td>Cupom nº <?php echo $dado['dados']->produto_avaria_id?></td>
				<td>Venda: <?php echo $dado['dados']->venda_id ?></td>
				<td><?php echo date("d/m/Y h:i:sa"); ?></td>
			</tr>
			<tr>
				<td colspan="3">De: <?php echo $dado['antigo'][0]->produto_descricao.' - '.$dado['antigo'][0]->imei_valor ?></td>
			</tr>
			<tr>
				<td colspan="3">Para: <?php echo $dado['novo'][0]->produto_descricao.' - '.$dado['novo'][0]->imei_valor ?></td>
			<tr>
			<tr>
				<td colspan="3">Vendendor: <b><?php echo $dado['vendendor'] ?></b></td>
			</tr>
			<tr>
				<td colspan="3">Data de autorização: <?php echo $dado['dataVenda'] ?><br>Data de modificação: <?php echo $dado['dataAtualizacao'] ?></td>
			</tr>
		</table>
		<hr>
	<?php endforeach; ?>

	<table width="100%" class="no-print">
		<tr>
			<td colspan="3">
				<button style="border: 0; cursor: pointer; background: #367fa9; display: block; width: 100%; padding: 10px; text-align: center; text-transform: uppercase; color: #fff" onclick="window.print();">Impressão Web</button>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<button style="border: 0; cursor: pointer; background: #e08e0b; display: block; width: 100%; padding: 10px; text-align: center; text-transform: uppercase; color: #fff" onclick="window.close();">Fechar</button>
			</td>
		</tr>
	</table>
</div>
</body>
</html>
