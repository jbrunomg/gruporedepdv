<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <title>Redirecionamento</title>
</head>
<body>
    <script>
        // Abrir nova janela
        window.open("<?php echo $popup_url; ?>", "_blank", "toolbar=no,location=no,directories=no,status=no,menubar=yes,scrollbars=yes,resizable=yes,width=350,height=600");

        // Redirecionar a página principal
        window.location.href = "<?php echo $redirect_url; ?>";
    </script>
</body>
</html>
