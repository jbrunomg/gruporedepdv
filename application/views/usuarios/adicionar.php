<section class="content">
  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="<?php echo current_url(); ?>" method="post">
          <div class="box-body">

            <div class="form-group">
              <label for="usuario_cpf" class="col-sm-2 control-label">CPF</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" name="usuario_cpf" id="cpf" value="<?php echo set_value('usuario_cpf'); ?>" placeholder="CPF">
              </div>
            </div>

            <div class="form-group">
              <label for="usuario_nome" class="col-sm-2 control-label">Nome Completo</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" id="usuario_nome" name="usuario_nome" value="<?php echo set_value('usuario_nome'); ?>" placeholder="Nome Completo">
              </div>
            </div> 

            <div class="form-group">
              <label for="usuario_email" class="col-sm-2 control-label">E-mail</label>
              <div class="col-sm-5">                
                <input type="text" class="form-control" id="usuario_email" name="usuario_email" value="<?php echo set_value('usuario_email'); ?>" placeholder="E-mail">
              </div>
            </div>

            <div class="form-group">
              <label for="usuario_datanasc" class="col-sm-2 control-label">Data Nascimento</label>
              <div class="col-sm-5">              
                <input type="date" class="form-control" name="usuario_datanasc" id="cpf" value="<?php echo set_value('usuario_datanasc'); ?>" placeholder="Data Nascimento">
              </div>
            </div>


            <div class="form-group">
              <label for="usuario_senha" class="col-sm-2 control-label">Senha</label>
              <div class="col-sm-5">              
                <input type="password" class="form-control" id="usuario_senha" name="usuario_senha" value="<?php echo set_value('usuario_senha'); ?>" placeholder="Senha">
              </div>
            </div>

            <div class="form-group">
              <label for="usuario_perfil" class="col-sm-2 control-label">Situação</label>
              <div class="col-sm-5">                
                <select class="form-control" name="situacao" id="situacao">
                  <option value="1">Ativo</option>
                  <option value="0">Inativo</option>
                </select>
              </div>
            </div>  

            <div class="form-group">
              <label for="usuario_perfil" class="col-sm-2 control-label">Perfil</label>
              <div class="col-sm-5">              
                
                <select class="form-control" name="usuario_perfil" id="usuario_perfil">
                  <option value="">Selecione</option>
                  <?php foreach ($perfil as $valor) { ?>
                    <option value='<?php echo $valor->perfil_id; ?>'><?php echo $valor->perfil_descricao; ?> </option>
                  <?php } ?>
                </select>
              </div>
            </div>


          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>" class="btn btn-default">Voltar</a>
            <button type="submit" class="btn btn-primary pull-right"><?php echo ucfirst($this->uri->segment(2)); ?></button>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
  </div>
</section>