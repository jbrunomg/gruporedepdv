

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="<?php echo base_url(); ?>assets/usuarios/<?php echo $dados[0]->usuario_imagem  ?>" alt="User profile picture">

              <h3 class="profile-username text-center"><?php echo $dados[0]->usuario_nome; ?></h3>

              <?php foreach ($perfil as $valor) { ?>
                <?php if ($valor->perfil_id == $dados[0]->usuario_perfil) { ?>
                    <p class="text-muted text-center"><?php echo $valor->perfil_descricao; ?></p>
                <?php  } ?>  
              <?php } ?>        

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Vendas</b> <a class="pull-right"><?php echo $venda[0]->total; ?></a>
                </li>
                <li class="list-group-item">
                  <b>Grupo</b> <a class="pull-right"><?php echo $grupo[0]->categoria_prod_descricao; ?></a>
                </li>
                <li class="list-group-item">
                  <?php for ($i=0; $i < count($ranck) ; $i++) { 
                    if ($ranck[$i]->vendedor == $dados[0]->usuario_nome) { ?>
                    <b>Ranck</b> <a class="pull-right"><?php echo $i ?>º</a>
                  <?php } } ?>                  
                </li>
              </ul>

              <a href="<?php echo base_url(); ?>relatorios/vendedores" class="btn btn-primary btn-block"><b>Relatório Gerais</b></a>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">About Me</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <strong><i class="fa fa-book margin-r-5"></i> Education</strong>

              <p class="text-muted">
                B.S. in Computer Science from the University of Tennessee at Knoxville
              </p>

              <hr>

              <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>

              <p class="text-muted">Malibu, California</p>

              <hr>

              <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>

              <p>
                <span class="label label-danger">UI Design</span>
                <span class="label label-success">Coding</span>
                <span class="label label-info">Javascript</span>
                <span class="label label-warning">PHP</span>
                <span class="label label-primary">Node.js</span>
              </p>

              <hr>

              <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>

              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#activity" data-toggle="tab">Perfil</a></li>
              <li><a href="#timeline" data-toggle="tab">Timeline</a></li>
              <!-- <li><a href="#settings" data-toggle="tab">Avatar</a></li> -->
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="activity">


        <form class="form-horizontal" action="<?php echo current_url(); ?>" method="post" enctype="multipart/form-data" >
          <input type="hidden" class="form-control" name="id" id="id"  value="<?php echo $dados[0]->usuario_id; ?>" >
          <input type="hidden" class="form-control" name="usuario_imagem" id="usuario_imagem"  value="<?php echo $dados[0]->usuario_imagem; ?>" >
          <div class="box-body">

            <div class="form-group">
              <label for="usuario_cpf" class="col-sm-2 control-label">CPF</label>
              <div class="col-sm-5">              
                <input disabled type="text" class="form-control" name="usuario_cpf" id="usuario_cpf"  value="<?php echo $dados[0]->usuario_cpf; ?>" placeholder="CPF">
              </div>
            </div>

            <div class="form-group">
              <label for="usuario_nome" class="col-sm-2 control-label">Nome Completo</label>
              <div class="col-sm-5">              
                <input disabled type="text" class="form-control" id="usuario_nome" name="usuario_nome"  value="<?php echo $dados[0]->usuario_nome; ?>" placeholder="Nome Completo">
              </div>
            </div> 

            <div class="form-group">
              <label for="usuario_email" class="col-sm-2 control-label">E-mail</label>
              <div class="col-sm-5">                
                <input disabled type="text" class="form-control" id="e-mail" name="usuario_email"  value="<?php echo $dados[0]->usuario_email; ?>" placeholder="E-mail">
              </div>
            </div>

            <div class="form-group">
              <label for="usuario_senha" class="col-sm-2 control-label">Senha</label>
              <div class="col-sm-5">              
                <input disabled type="password" class="form-control" id="senha" name="usuario_senha" value="<?php echo set_value('usuario_senha'); ?>" placeholder="**********">
              </div>
            </div>

            <div class="form-group">
              <label for="usuario_perfil" class="col-sm-2 control-label">Situação</label>
              <div class="col-sm-5">  

           <?php if($dados[0]->usuario_ativo == 1){ $situacao = 'Ativo'; } else { $situacao = 'Inativo'; } ?>              
           <input disabled type="text" class="form-control" id="situacao" name="situacao" value="<?php echo $situacao; ?>" >  
               
                  
              </div>
            </div>

            <div class="form-group">
              <label for="usuario_perfil" class="col-sm-2 control-label">Perfil</label>
              <div class="col-sm-5"> 

              <?php foreach ($perfil as $valor) { ?>
                <?php if ($valor->perfil_id == $dados[0]->usuario_perfil) { ?>              
                    <input disabled type="text" class="form-control" id="perfil_descricao" name="perfil_descricao" value="<?php echo $valor->perfil_descricao; ?>" >  
                <?php  } ?>  
              <?php } ?>                           
   
              </div>
            </div>

        

               
          <div></div>
  <!--         <div class="box-footer">
            <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>" class="btn btn-default">Voltar</a>
            <button type="submit" class="btn btn-primary pull-right"><?php echo ucfirst($this->uri->segment(2)); ?></button>
          </div> -->


          </div>
          <!-- /.box-body -->
      
        </form>




              </div>
              <!-- /.tab-pane -->

              <div class="tab-pane" id="timeline">
                <!-- The timeline -->
                <ul class="timeline timeline-inverse">
                  <!-- timeline time label -->
                  <li class="time-label">
                        <span class="bg-red">
                          10 Feb. 2014
                        </span>
                  </li>
                  <!-- /.timeline-label -->
                  <!-- timeline item -->
                  <li>
                    <i class="fa fa-envelope bg-blue"></i>

                    <div class="timeline-item">
                      <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>

                      <h3 class="timeline-header"><a href="#">Support Team</a> sent you an email</h3>

                      <div class="timeline-body">
                        Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                        weebly ning heekya handango imeem plugg dopplr jibjab, movity
                        jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                        quora plaxo ideeli hulu weebly balihoo...
                      </div>
                      <div class="timeline-footer">
                        <a class="btn btn-primary btn-xs">Read more</a>
                        <a class="btn btn-danger btn-xs">Delete</a>
                      </div>
                    </div>
                  </li>
                  <!-- END timeline item -->
                  <!-- timeline item -->
                  <li>
                    <i class="fa fa-user bg-aqua"></i>

                    <div class="timeline-item">
                      <span class="time"><i class="fa fa-clock-o"></i> 5 mins ago</span>

                      <h3 class="timeline-header no-border"><a href="#">Sarah Young</a> accepted your friend request
                      </h3>
                    </div>
                  </li>
                  <!-- END timeline item -->
                  <!-- timeline item -->
                  <li>
                    <i class="fa fa-comments bg-yellow"></i>

                    <div class="timeline-item">
                      <span class="time"><i class="fa fa-clock-o"></i> 27 mins ago</span>

                      <h3 class="timeline-header"><a href="#">Jay White</a> commented on your post</h3>

                      <div class="timeline-body">
                        Take me to your leader!
                        Switzerland is small and neutral!
                        We are more like Germany, ambitious and misunderstood!
                      </div>
                      <div class="timeline-footer">
                        <a class="btn btn-warning btn-flat btn-xs">View comment</a>
                      </div>
                    </div>
                  </li>
                  <!-- END timeline item -->


                  <!-- timeline time label -->
                  <li class="time-label">
                        <span class="bg-green">
                          3 Jan. 2014
                        </span>
                  </li>
                  <!-- /.timeline-label -->
                  <!-- timeline item -->
                  <li>
                    <i class="fa fa-camera bg-purple"></i>

                    <div class="timeline-item">
                      <span class="time"><i class="fa fa-clock-o"></i> 2 days ago</span>

                      <h3 class="timeline-header"><a href="#">Mina Lee</a> uploaded new photos</h3>

                      <div class="timeline-body">
                 <!--        <img src="http://placehold.it/150x100" alt="..." class="margin">
                        <img src="http://placehold.it/150x100" alt="..." class="margin">
                        <img src="http://placehold.it/150x100" alt="..." class="margin">
                        <img src="http://placehold.it/150x100" alt="..." class="margin"> -->
                      </div>
                    </div>
                  </li>
                  <!-- END timeline item -->
                  <li>
                    <i class="fa fa-clock-o bg-gray"></i>
                  </li>
                </ul>
              </div>
              <!-- /.tab-pane -->

              <div class="tab-pane" id="settings">
                

              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->