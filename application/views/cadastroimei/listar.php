<section class="content">
  <div class="box">
    <!-- <div class="box-header with-border">
      <div class="col-md-2">
        <?php if(verificarPermissao('aAlmoxarifado')){ ?>
        <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/adicionar" type="button" class="btn btn-block btn-default btn-flat">Adicionar</a>
        <?php } ?>
      </div>          
    </div> -->
    <div class="box-body">
      <div class="container-fluid">
        <table id="examplee" class="table" width="100%">
          <thead>
            <tr>                                            
              <th>Código</th>
              <th>Filial</th>
              <th>Tipo</th>
              <th>Data</th>
              <th>Documento</th>
              <th>Produto / Imei</th>   
              <th>Total</th>             
              <th>Ações</th>
            </tr>
          </thead>
          <tbody>

            <?php foreach ($dados as $d){ ?>
            <tr>                 
              <td> <?php echo $d->movimentacao_produto_id;?></td> 
              <td> <?php echo $d->movimentacao_produto_saida_filial == NULL ? ' -------' : $d->movimentacao_produto_saida_filial;?></td> 
              <td> <?php echo $d->movimentacao_produto_tipo;?></td> 
              <td> <?php echo $d->movimentacao_produto_data_cadastro;?></td> 
              <td> <?php echo $d->movimentacao_produto_documento;?></td>
              <td> <?php echo $d->quantPr.' / '.$d->QuantIm;?></td>
              <td> <?php echo $d->total;?></td>
                             
              <td>
                <?php if(verificarPermissao('eAlmoxarifado')){ ?>
                <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/editar/<?php echo $d->movimentacao_produto_id; ?>" data-toggle="tooltip" title="Editar"><i class="fa fa-edit"></i> </a>
                <?php } ?>              
              </td>
            </tr>
            <?php } ?>

          </tbody>
          <tfoot>
            <tr>                     
              <th>Código</th>
              <th>Tipo</th>
              <th>Data</th>
              <th>Documento</th>              
              <th>Ações</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">
  // Basic example
$(document).ready(function () {
$('#examplee').DataTable({
"ordering": false // false to disable sorting (or any other option)
});

});


</script>
