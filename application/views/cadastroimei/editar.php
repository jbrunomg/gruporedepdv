<style>
.modal-header::after,
.modal-header::before {
  display: none;

}
</style>

<section class="content">
  <div class="row">
    <div class="col-md-12">
      <!-- Horizontal Form -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="#" method="post">
          <input type="hidden" class="form-control" name="id" id="idMovimento"
            value="<?php echo $dados[0]->movimentacao_produto_id; ?>" placeholder="id">
          <div class="box-body">

            <h3>#ENTRADA: <?php echo $dados[0]->movimentacao_produto_id ?></h3>
            <div class="form-group">
              <label for="movimentacao_produto_data_cadastro" class="col-sm-2 control-label">Data da Entrada </label>
              <div class="col-sm-5">
                <input type="date" class="form-control" name="movimentacao_produto_data_cadastro"
                  value="<?php echo $dados[0]->movimentacao_produto_data_cadastro; ?>" disabled>
              </div>
            </div>
            <div class="form-group">
              <label for="movimentacao_produto_documento" class="col-sm-2 control-label">Documento </label>
              <div class="col-sm-5">
                <input type="text" class="form-control" name="movimentacao_produto_documento"
                  id="movimentacao_produto_documento" value="<?php echo $dados[0]->movimentacao_produto_documento ?>"
                  disabled>
              </div>
            </div>

          </div>
        </form>

        <fieldset class="col-xs-12">
          <legend>Dados dos Produtos</legend>


          <form id="formProdutos" method="post">
            <div class="row">

              <div class="col-xs-1">
                <label for="Grupo">Grupo</label>
                <select class="form-control" name="grupo_prod_imei" id="grupo_prod_imei">
                  <option value="">Selecione</option>
                  <?php foreach ($categoria_prod as $valor) { ?>
                  <option value='<?php echo $valor->categoria_prod_id; ?>'>
                    <?php echo $valor->categoria_prod_descricao; ?> </option>
                  <?php } ?>
                </select>
              </div>

              <div class="col-xs-3">
                <label for="exampleInputPassword1">Produto</label>
                <div class="input-group">
                  <div class="input-group-btn">
                    <button type="button" class="btn btn-danger">P</button>
                  </div>
                  <!-- /btn-group -->
                  <select class="select2 select-access-value" style="width: 100%;" name="produto_desc_imei"
                    id="produto_desc_imei">
                    <option value="">Selecione um produto</option>
                  </select>
                </div>
              </div>

              <div class="col-xs-2">
                <label for="exampleInputPassword1">IMEI</label>
                <input type="text" id="imei" name="imei" class="form-control">
              </div>
              
              <div class="col-xs-2">
                <label for="exampleInputPassword1">% Bateria</label>
                <input type="text" id="bateria" name="bateria" class="form-control">
              </div>

              <div class="col-xs-2">
                <label for="exampleInputPassword1">Obs.</label>
                <input type="text" id="obs" name="obs" class="form-control">
              </div>
         

              <div class="col-xs-2">
                <label for="btnAdicionarProdutoImei" class="control-label">&nbsp;</label> </br>
                <button class="btn btn-success" id="btnAdicionarProdutoImei" type="button"><i
                    class="icon-white icon-plus"></i>
                  Adicionar</button>
              </div>

            </div>
          </form>


        </fieldset>






        <div class="box-footer">

          <!-- /.box-header -->
          <input type="hidden" id="seq" />
          <div id="divProdutos" class="box-body no-padding">
            <table id="table_produtos" class="table table-condensed">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Produto</th>
                  <th>IMEI</th>
                  <th>% Bateria</th>
                  <th style="width: 40px">Ações</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
              <tfoot>
                <tr>
                  <th id="totalItens"></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                </tr>
              </tfoot>
            </table>
          </div>

        </div>

      </div>
    </div>
  </div>
</section>
<div id="ContainerRendarModal">
  <div class="modal fade" id="dynamicModal" data-backdrop="static" data-keyboard="false" style="z-index: 99999"
    tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header" style="display: block;">
          <div
            style="display: grid;grid-template-columns: 1fr min-content;grid-template-rows: 1fr;align-items: center;justify-content: space-around;width: 100%;">
            <h4 class="modal-title">ESSE IMEI JÁ EXISTE, DESEJA CADASTRAR?</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="modalClose">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>O IMEI informado já está registrado no sistema e o produto correspondente já foi vendido.Se você deseja
              adicionar um novo produto com este IMEI, por favor confirme.</p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal" id="modalCloseButton1">Cancelar</button>
          <button type="button" class="btn btn-success" id="modalConfirmButton">Confirmar</button>
        </div>
      </div>
    </div>
  </div>
</div>

<!--  MODAL  -->
<div class="modal fade" id="modalVideo" data-backdrop="static" data-keyboard="false" style="z-index: 99999"
  tabindex="-1" role="dialog">
  <div class="modal-dialog" style="width: 350px; margin: 100px auto">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" onclick="ativaVideo()" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center">Aponte seu QR Code</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div id="divVideo">
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>




<script src="<?php echo base_url() ?>assets/dist/js/notification.js"></script>
<script type="text/javascript">
function createModal() {
  $('#dynamicModal').modal('show');

  $('#modalCloseButton1, #modalClose').on('click', function() {
    $('#dynamicModal').modal('hide');
    $('#imei').val('').focus();
  });

  $("#modalConfirmButton").click(function() {

    var imei = document.getElementById('imei').value;

    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>index.php/cadastroimei/editarImeiProdutoVendido",
      data: "imei=" + imei,
      dataType: 'json',
      success: function(data) {
        $("#myModal").modal('hide');
        if (data.result == true) {
          // notification(data.infor, 'success', 2500);
          $('#dynamicModal').modal('hide');
          btnAdicionarProdutoImei();
          // $("#divProdutos").load("<?php echo current_url();?> #divProdutos");
        } else {
          notification(data.infor, 'error', 3500);
          $('#dynamicModal').modal('hide');
          $("#imei").val('').focus();
        }
      }
    });
  })

}

function requisicao(url, dados = "") {
  var data = $.ajax({
    url: url,
    async: false,
    type: 'post',
    datatycreateModalpe: 'json',
    data: dados,
    success: function(data) {
      data = JSON.parse(data);
    }
  });

  return JSON.parse(data.responseText);
}

function reloadProduto(produto) {

  var grupo = $('#grupo_prod_imei').val();
  var idMovimento = $('#idMovimento').val();
  $.ajax({
      method: "POST",
      url: base_url + "cadastroimei/selecionarProduto/",
      dataType: "html",
      data: {
        grupo: grupo,
        idMovimento: idMovimento
      }
    })
    .done(function(response) {

      $('#produto_desc_imei').html(response);
      $('#produto_desc_imei').removeAttr("disabled", "disabled");
      $('.select-access-value').val(produto).trigger('change');

    });
}

function btnAdicionarProdutoImei() {

  let grupo = $('#grupo_prod_imei').val();
  idMovimento = $('#idMovimento').val();
  imei    = $('#imei').val();
  bateria = $('#bateria').val();
  obs     = $('#obs').val();
  produto = $('#produto_desc_imei').val();
  produtoDesc = $('#produto_desc_imei').find(':selected').attr('dataProdutoDesc');
  quantidadeProduto = $('#produto_desc_imei').find(':selected').attr('dataQuant');
  seq = $("#seq").val();

  let dados = {
    grupo: grupo,
    idMovimento: idMovimento,
    imei: imei,
    bateria: bateria,
    obs:     obs,
    produto: produto,
    quantidadeProduto: quantidadeProduto
  };

  if (imei.length < 15) {
    notification("Número de IMEI inválido!,No minimo e necessario 15 caraqueteres.", 'error', 3500);
    return false;
  }

  if (imei.length > 17) {
    notification("Número de IMEI inválido!,No maximo e necessario 17 caraqueteres.", 'error', 3500);
    return false;
  }

  let idProduto = requisicao(base_url + "cadastroimei/adicionarProduto", dados);

  reloadProduto(produto);

  if (!(idProduto.result)) {
    if (idProduto.erroQuantidade) {
      notification('' + idProduto.erroQuantidade + '', 'error', 3500);

      // location.reload();
      return false;
    } else {
      notification('' + idProduto.erro + '', 'error', 3500);
      return false;
    }
  }

  let body = "";
  body += "<tr>";
  body += "<td>" + (seq) + "</td>";
  body += "<td>" + produtoDesc + "</td>";
  body += "<td>" + imei + "</td>";
  body += "<td>" + bateria + "</td>";
  body += "<td><a  href='" + base_url + "index.php/cadastroimei/excluirProduto/" + idProduto.id + "/" +
    idMovimento + "' title='Excluir Produto' ><i class='fa fa-trash text-danger'></i></a></td>";
  body += "</tr>";
  $("#table_produtos tbody").append(body);
  $("#totalItens").html("ITENS: " + (seq) + "");
  $("#imei").val('').focus();

}

$(document).ready(function() {
  let id = $('#idMovimento').val();
  let dados = {
    id: id
  }
  let responseItens = requisicao(base_url + "cadastroimei/buscarItensProdutos", dados);
  let produtos = "";
  let seq = 1;

  responseItens.map(function(data) {
    produtos += "<tr>";
    produtos += "<td>" + seq + "</td>";
    produtos += "<td>" + data.produto_descricao + "</td>";
    produtos += "<td>" + data.imei_valor + "</td>";
    produtos += "<td><a  href='" + base_url + "index.php/cadastroimei/excluirProduto/" + data.imei_id + "/" +
      data.movimentacao_id + "' title='Excluir Produto' ><i class='fa fa-trash text-danger'></i></a></td>";
    produtos += "</tr>";
    seq++;
  });

  $("#table_produtos tbody").append(produtos);
  $("#seq").val(seq);
  $("#totalItens").html("ITENS: " + responseItens.length + "");

});

$('#grupo_prod_imei').change(function() {

  var grupo = $('#grupo_prod_imei').val();
  var idMovimento = $('#idMovimento').val();
  $.ajax({
      method: "POST",
      url: base_url + "cadastroimei/selecionarProduto/",
      dataType: "html",
      data: {
        grupo: grupo,
        idMovimento: idMovimento
      }
    })
    .done(function(response) {
      $('#produto_desc_imei').html(response);
      $('#produto_desc_imei').removeAttr("disabled", "disabled");

    });

});

$('#imei').change(function() {

  var imei = $('#imei').val();
  $.ajax({
    method: "POST",
    url: base_url + "cadastroimei/validarImei/",
    dataType: "json",
    data: {
      imei: imei,
    },
    success: function(data) {
      if (data.venda) {
        createModal();
        return;
      }

      if (data.result) {
        notification("Número de IMEI Ja foi cadastrado", 'error', 3500);
        $('#imei').val('')
      }
    }
  });
});

$('#btnAdicionarProdutoImei').click(btnAdicionarProdutoImei);
</script>
<script type="text/javascript" src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
<script>
var ativo = true;

function ativaVideo() {

  $('#divVideo').html("<video width='320px' height='220px' id='preview'></video>");

  let scanner = new Instascan.Scanner({
    video: document.getElementById('preview')
  });

  if (ativo) {
    scanner.addListener('scan', function(content) {
      $('#imei').val(content);
      scanner.stop();
      ativo = true;
      $('#modalVideo').modal('hide');
    });
    Instascan.Camera.getCameras().then(cameras => {
      if (cameras.length > 0) {
        scanner.start(cameras[0]);
        ativo = false;
      } else {
        console.error("Não existe câmera no dispositivo!");
      }
    });

  } else {
    scanner.stop();
    $('#modalVideo').modal('hide');
  }
}
</script>
