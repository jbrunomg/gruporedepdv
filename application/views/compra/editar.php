<?php 
  $valorTotal = 0;
?>

<section class="content" style="height:100%">
  <input type="hidden" id="idCompra" value="<?php echo $this->uri->segment(3); ?>">

  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="#" method="POST">                
          <div class="box-body">

            <div class="row">
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <!-- <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 17%</span> -->
                    <h5 class="description-header"><?php echo $categoria; ?></h5>
                    <span class="description-text">GRUPO/CATEGORIA</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <!-- <span class="description-percentage text-yellow"><i class="fa fa-caret-left"></i> 0%</span> -->
                    <h5 class="description-header">De: <?php echo $dataInicio.' a '.$dataFim ?></h5>
                    <span class="description-text">DATA-GERAÇÃO</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <!-- <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 20%</span> -->
                    <h5 class="description-header"><?php echo $observacao; ?></h5>
                    <span class="description-text">OBSERVAÇÃO</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block">
                    <!-- <span class="description-percentage text-red"><i class="fa fa-caret-down"></i> 18%</span>  -->
                    <h5 class="description-header"><?php echo ($tipo = 'produto_codigo') ? 'Prod. Nome' : 'Prod. Codigo' ; ?></h5>
                    <span class="description-text">TIPO APONTAMENTO</span>
                  </div>
                  <!-- /.description-block -->
                </div>
              </div>
          </div>

        </form>

        <fieldset class="col-md-12">
            <legend>
              <a href="<?php echo base_url().'compra/prepararCompra/'.$this->uri->segment(3); ?>" type="button" class="btn btn-primary col-md-2" id="btnPrepararCompra" >Finalizar Pedido</a>
            </legend>            
        </fieldset>   

        <fieldset class="col-md-12">
            <legend>Dados dos Produtos</legend>
        </fieldset>     

          <div class="box-footer">
                        
              <!-- /.box-header -->
              <div id="divProdutos" class="box-body no-padding" style="overflow-y:scroll; height:500px; width:100%;">
                <table class="table table-condensed" id="tbProduto">
                  <thead>
                    <tr>
                      <th>Produto</th>
                      <th>Estoque</th>
                      <th>Vendidos</th>
                      <!-- <th>Preço Custo</th>   -->
                      <th>Apontador</th>             
                      <th>Quantidade</th>
                      <th>Total</th>              
                    </tr>
                  </thead>  
                  <tbody>
                    <div>

                    <?php  foreach ($produtos as $produto) { ?>
                      <tr>
                        <td><?php echo $produto->produto_descricao; ?></td>
                        <td><?php echo $produto->produto_estoque; ?></td>
                        <td><?php echo $produto->vendidos; ?></td>
                        <td style="display:none;"><p id="custo_<?php echo $produto->produto_id; ?>"> <?php echo $produto->produto_preco_custo; ?> </p></td>                        

                        <!-- TIRAR ESSA REGRA E COLOCAR NO CONTROLLER (Bruno 16112019) -->
                        <?php if ( ($produto->vendidos == 0) or ($produto->vendidos < $produto->produto_estoque) ) { ?>
                          <td class="text-center"><?php echo  0  ?></td>                          
                        <?php } else if ($produto->vendidos >= $produto->produto_estoque) { ?>
                          <td class="text-center"><?php echo  $produto->vendidos == null ? 0 : $produto->vendidos - $produto->produto_estoque  ?></td>
                        <?php }  ?>
                      

                        <td>
                          <input style="border:0;" onKeyPress="return so_numero(event)" placeholder="0" type="text" onblur="ajustarTotal(this)" qtdAnterior="0" class="text-center" id="<?php echo $produto->produto_id ?>" name="quant_id_<?php echo $produto->produto_id ?>" value="<?php   echo $produto->quantidade; ?>">
                        </td>
                        <td>
                          <?php 
                            $soma = number_format(($produto->produto_preco_custo * $produto->quantidade), 2,'.','');
                            $valorTotal += $soma; 
                          ?>
                          <p id="total_<?php echo $produto->produto_id?>"><?php echo $soma; ?></p>
                        </td>
                      <tr>
                    <?php } ?>

                    </div>
                  </tbody>
                </table>                
              </div>
              <div class="col-md-12" style="padding: 20px">
                  <h1 class="text-right">R$ <b id="qtdtotal"><?php echo number_format($valorTotal, 2, '.', ''); ?></b></h1>
                </div>            

          </div> 

      </div>
    </div>
  </div>
</section>


<script type="text/javascript">

    $(document).ready(function () {
    let total = $('#qtdtotal').text();

    if(total <= 0){ 
      $("#btnPrepararCompra").hide();
    } else {
      $("#btnPrepararCompra").show();
    }
  });
  
</script>