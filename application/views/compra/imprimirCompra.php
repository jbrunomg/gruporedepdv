
  <div id="head" style="padding-top: 9px; display: table;">

      <h4 style="width:180px; float:left;">Simulação Compra #<?php echo $simulacao ?></h4>
      <p style="font-size: 12px; display: block; margin: 4px; text-align: right;">GRUPO/CATEGORIA - <?php echo $categoria; ?></p>
      <p style="font-size: 12px; display: block; margin: 4px; text-align: right;">DATA-GERAÇÃO - <?php echo $dataInicio.' - '.$dataFim; ?></p>
      <p style="font-size: 12px; display: block; margin: 4px; text-align: right;">OBSERVAÇÃO - <?php echo $observacao ?></p>
      <br class="clear">
  </div>


    <table>
        <thead>
            <tr>
              <th style="font-size: 12px;">Produto</th>
              <th style="font-size: 12px;">Estoque</th>
              <th style="font-size: 12px;">Vendidos</th>                             
              <th style="font-size: 12px;">Preço Custo</th>
              <th style="font-size: 12px;">Apontador</th>
              <th style="font-size: 12px;">Quantidade</th>
              <th style="font-size: 12px;">Total</th>
              <th style="font-size: 12px;">Funcionario</th>
            </tr>
        </thead>

        <tbody>                                  
          <?php $valorTotal = 0;  foreach ($produtos as $produto) { ?>
            <tr class="black">
              <td class="black"><?php echo $produto->produto_descricao; ?></td>
              <td class="black"><?php echo $produto->produto_estoque; ?></td>
              <td class="black"><?php echo $produto->vendidos; ?></td>
              <td class="black"> <?php echo $produto->produto_preco_custo; ?> </td>

 
              <!-- TIRAR ESSA REGRA E COLOCAR NO CONTROLLER (Bruno 16112019) -->
              <?php if ( ($produto->vendidos == 0) or ($produto->vendidos < $produto->produto_estoque) ) { ?>
                <td class="text-center"><?php echo  0  ?></td>                          
              <?php } else if ($produto->vendidos >= $produto->produto_estoque) { ?>
                <td class="text-center"><?php echo  $produto->vendidos == null ? 0 : $produto->vendidos - $produto->produto_estoque  ?></td>
              <?php }  ?>
             

              <td class="black">
                <?php echo $produto->quantidade; ?>
              </td>
              <td class="black">
                <?php 
                  $soma = number_format(($produto->produto_preco_custo * $produto->quantidade), 2,'.','');
                  $valorTotal += $soma; 
                ?>
                <?php echo $soma; ?>
              </td>
              <td class="black"><?php echo $produto->funcionario ?></td>
            <tr>
          <?php } ?>
        </tbody>
      </table>

      <br class="clear"> 

      <table>
          <tr class="black">                     
            <th colspan="8"><h5>Valor Total: R$ <?php echo number_format($valorTotal, 2, '.', ''); ?></h5></th>                        
          </tr>
      </table>
       



