<section class="content">
  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="<?php echo current_url(); ?>" method="post">
          <div class="box-body">

                <div class="form-group">
                  <label for="compra_categoria_id" class="col-sm-2 control-label">Grupo Produto</label>
                  <div class="col-sm-5">                            
                    <select id="compra_categoria_id" class="form-control" data-placeholder="Selecione o grupo de produtos" name="compra_categoria_id">
                      <option value="">Selecione</option>
                      <?php foreach ($grupo as $g) { ?>
                      <option value='<?php echo $g->categoria_prod_id; ?>'><?php echo $g->categoria_prod_descricao; ?> </option>
                      <?php } ?>                       
                    </select>  
                  </div>
                </div>
       
                <div class="form-group">
                  <label for="compra_observacao" class="col-sm-2 control-label">Observação </label>
                  <div class="col-sm-5">              
                    <input type="text" class="form-control" name="compra_observacao" id="compra_observacao" value="<?php echo set_value('compra_observacao'); ?>" placeholder="Observacao">
                  </div>
                </div> 

                <div class="form-group">
                  <label for="compra_observacao" class="col-sm-2 control-label">Tipo Apontamento </label>
                  <div class="col-sm-5">              
                    <select class="form-control" name="tipo" id="tipo">
                      <option value="produto_descricao">Produto Nome</option>
                      <option value="produto_codigo">Produto Codigo</option>
                    </select>
                  </div>
                </div> 

                <div class="form-group">
                  <label for="compra_data_inicio" class="col-sm-2 control-label">Data Inicial </label>
                  <div class="col-sm-2">              
                    <input type="date" class="form-control" name="compra_data_inicio"  value="<?php echo set_value('compra_data_inicio'); ?>" >
                  </div>

                  <label for="compra_data_final" class="col-sm-1 control-label">Data Final </label>
                  <div class="col-sm-2">              
                    <input type="date" class="form-control" name="compra_data_final" value="<?php echo set_value('compra_data_final'); ?>">
                  </div>
                </div> 
  
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>" class="btn btn-default">Voltar</a>
            <button type="submit" class="btn btn-primary pull-right"><?php echo ucfirst($this->uri->segment(2)); ?></button>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
  </div>
</section>


<script type="text/javascript">

  $(function () {


    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })

  })

  $(document).ready(function () {

    document.getElementById('grupo_prod_devolucao').focus(); 

  });

</script>