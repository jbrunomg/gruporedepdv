<?php 
  $valorTotal = 0;
?>
<section class="content">
  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="" method="POST">                
          <div class="box-body">

            <div class="row">
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <!-- <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 17%</span> -->
                    <h5 class="description-header"><?php echo $categoria; ?></h5>
                    <span class="description-text">GRUPO/CATEGORIA</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <!-- <span class="description-percentage text-yellow"><i class="fa fa-caret-left"></i> 0%</span> -->
                    <h5 class="description-header">De: <?php echo $dataInicio.' a '.$dataFim ?></h5>
                    <span class="description-text">DATA-GERAÇÃO</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <!-- <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 20%</span> -->
                    <h5 class="description-header"><?php echo $observacao; ?></h5>
                    <span class="description-text">OBSERVAÇÃO</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block">
                    <!-- <span class="description-percentage text-red"><i class="fa fa-caret-down"></i> 18%</span>
                    <h5 class="description-header">1200</h5>
                    <span class="description-text">ATUALIZADO</span> -->
                  </div>
                  <!-- /.description-block -->
                </div>
              </div>
          </div>

        </form>

        <fieldset class="col-md-12">
          <div class="box-header with-border">
            <legend>Dados dos Produtos</legend>
              <div class="box-tools pull-right">             
                <a title="Imprimir" target="_blank" class="btn btn-mini btn-default" href="<?php echo base_url().'compra/imprimirCompra/'.$this->uri->segment(3); ?>"><i class="glyphicon glyphicon-print icon-white"></i> Imprimir</a>
              </div>
          </div>
          </fieldset> 



          <div class="box-footer">

                        
              <!-- /.box-header -->
              <div id="divProdutos" class="box-body no-padding">
                <table class="table table-condensed">
                  <thead>
                    <tr>
                      <th class="text-center">Produto</th>
                      <th class="text-center">Estoque</th>
                      <th class="text-center">Vendidos</th>
                      <th class="text-center">Preço Custo</th>
                      <th class="text-center">Apontador</th>               
                      <th class="text-center">Quantidade</th>
                      <th class="text-right">Total</th>     
                      <th class="text-center">Funcionario</th>        
                    </tr>
                  </thead>  
                  <tbody>                                  
                    <?php foreach ($produtos as $produto) { ?>
                      <tr>
                        <td><?php echo $produto->produto_descricao; ?></td>
                        <td class="text-center"><?php echo $produto->produto_estoque; ?></td>
                        <td class="text-center"><?php echo $produto->vendidos; ?></td>
                        <td class="text-center"> <?php echo $produto->produto_preco_custo; ?> </td>

                        <!-- TIRAR ESSA REGRA E COLOCAR NO CONTROLLER (Bruno 16112019) -->
                        <?php if ( ($produto->vendidos == 0) or ($produto->vendidos < $produto->produto_estoque) ) { ?>
                          <td class="text-center"><?php echo  0  ?></td>                          
                        <?php } else if ($produto->vendidos >= $produto->produto_estoque) { ?>
                          <td class="text-center"><?php echo  $produto->vendidos == null ? 0 : $produto->vendidos - $produto->produto_estoque  ?></td>
                        <?php }  ?>
                       

                        <td class="text-center">
                          <?php echo $produto->quantidade; ?>
                        </td>
                        <td class="text-right">
                          <?php 
                            $soma = number_format(($produto->produto_preco_custo * $produto->quantidade), 2,'.','');
                            $valorTotal += $soma; 
                          ?>
                          <?php echo $soma; ?>
                        </td>
                        <td class="text-center"><?php echo $produto->funcionario ?></td>
                      <tr>
                    <?php } ?>
                  </tbody>
                  <tfoot>
                    <tr>                     
                      <th><h3>Valor Total: R$ <?php echo number_format($valorTotal, 2, '.', ''); ?></h3></th>     
                      <th></th>                      
                    </tr>
                  </tfoot>
                </table>
              </div>            

          </div> 

      </div>
    </div>
  </div>
</section>
