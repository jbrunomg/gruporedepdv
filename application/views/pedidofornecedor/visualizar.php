<section class="content">
  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/editarExe" method="post">
            <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $dados[0]->idPedidos; ?>" placeholder="id">        
            <div class="box-body">

                <h3>#Pedido:<?php echo $dados[0]->idPedidos ?></h3>
                <div class="form-group">
                  <label for="cliente" class="col-sm-2 control-label">Fornecedor </label>
                  <div class="col-sm-5">              
                    <input disabled type="text" class="form-control" name="fornecedor_nome" id="fornecedor_nome" value="<?php echo $dados[0]->fornecedor_nome ?>" placeholder="Cliente">
                    <input type="hidden" id="fornecedor_id" name="fornecedor_id" value="<?php echo $dados[0]->fornecedor_id ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="dataPedido" class="col-sm-2 control-label">Data do Pedido </label>
                  <div class="col-sm-5">              
                    <input disabled type="date" class="form-control" name="dataPedido" value="<?php echo $dados[0]->dataPedido; ?>" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="observacao" class="col-sm-2 control-label">Observação </label>
                  <div class="col-sm-5">              
                    <input disabled type="text" class="form-control" name="observacao" id="observacao" value="<?php echo $dados[0]->observacao ?>" placeholder="Observação">
                  </div>
                </div> 
  
            </div>

        </form>

        <fieldset class="col-md-12">
            <legend>Dados dos Produtos</legend>
            <div class="col-md-12">

            </div>
          </fieldset> 



          <div class="box-footer">
                        
              <!-- /.box-header -->
              <div id="divProdutos" class="box-body no-padding">
                <table class="table table-condensed">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Produto</th>                     
                      <th>Quantidade</th>                      
                      <th>Pago</th>
                      <th>ValorFatura</th>                      
                     
                    </tr>
                  </thead>  
                  <tbody>
                    <?php
                    $total = 0;
                    $qtd = 0;
                    $seq = 1;
                     // echo "<pre>";
                     // var_dump($produtos);die();
                    $totalGeral = 0;

                    foreach ($produtos as $p) {            

                        $qtd = $qtd + $p->quantidade;                        
                        echo '<tr>';
                        echo '<td>'.$seq ++.'</td>';
                        echo '<td>'.$p->produto_descricao.'</td>';
                        echo '<td>'.$p->quantidade.'</td>';                        
                        echo '<td>'.'Yes ou No'.'</td>';
                        echo '<td>'.'R$ DA FATURA'.'</td>';             
                                                                       
                        echo '</tr>';
                    }?>
                                         

                  </tbody>
                  <tfoot>
                    <tr>                     
                      <th>ITENS:<?php echo ' '.count($produtos); ?></th>
                      <th></th>                      
                      <th>VOLUME:<?php echo ' '.$qtd; ?></th>
                      <th></th>  
                      <th></th>  
                      <th></th>                    
                    </tr>
                  </tfoot>
                </table>
              </div>            

          </div> 

          <!-- /.box-body -->
        <!--   <div class="box-footer">
            <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>" class="btn btn-default">Voltar</a>
            <button type="submit" class="btn btn-primary pull-right"><?php echo ucfirst($this->uri->segment(2)); ?></button>
          </div> -->
          <!-- /.box-footer -->


      </div>
    </div>
  </div>
</section>



