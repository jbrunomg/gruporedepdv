<section class="content">
  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/editarExe" method="post">
        <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $dados[0]->idPedidos; ?>" placeholder="id">        
          <div class="box-body">

            <h3>#Pedido:<?php echo $dados[0]->idPedidos ?></h3>
            <div class="form-group">
              <label for="cliente" class="col-sm-2 control-label">Fornecedor </label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" name="fornecedor_nome" id="fornecedor_nome" value="<?php echo $dados[0]->fornecedor_nome ?>" placeholder="Cliente">
                <input type="hidden" id="fornecedor_id" name="fornecedor_id" value="<?php echo $dados[0]->fornecedor_id ?>">
              </div>
            </div>
            <div class="form-group">
              <label for="dataPedido" class="col-sm-2 control-label">Data do Pedido </label>
              <div class="col-sm-5">              
                <input type="date" class="form-control" name="dataPedido" value="<?php echo $dados[0]->dataPedido; ?>" placeholder="">
              </div>
            </div>
            <div class="form-group">
              <label for="observacao" class="col-sm-2 control-label">Observação </label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" name="observacao" id="observacao" value="<?php echo $dados[0]->observacao ?>" placeholder="Observação">
              </div>
            </div> 
  
            <div class="span12" style="padding: 1%; margin-left: 0">

                <div class="span8 offset2" style="text-align: center"> 
                <?php if(verificarPermissao('ePedido')){ ?> 
                  <span class="btn btn-danger" id="btnVendaPedido"><i class="icon-white icon-plus"></i> Transformar Pedido em Despesa</span>
                <?php } ?>
                    <!-- <a href="#"  id="btnVendaPedido" class="btn btn btn-success"><i class="icon-arrow-left"></i> Transformar Pedido em Venda</a>  -->    
                    <button type="submit" class="btn btn-primary">Alterar</button>
                    <a href="#" onClick="(function(){window.open('<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/visualizar/<?php echo $dados[0]->idPedidos; ?>','MyWindow','toolbar=no,location=no,directories=no,status=no,menubar=yes,scrollbars=yes,resizable=yes,width=350,height=600');return false;})();return false;"data-toggle="tooltip" title="Visualizar" class="btn btn-info"><i class="icon-fa-eye"></i> Visualizar Pedido</a>

                    <a href="<?php echo base_url() ?>index.php/pedidofornecedor" class="btn btn-default"><i class="icon-arrow-left"></i> Voltar</a>
                </div>
            </div> 



          </div>

                  </form>

        <fieldset class="col-md-12">
            <legend>Dados dos Produtos</legend>
            <div class="col-md-12">
              <form id="formProdutos"  method="post">

                <div class="form-group"> 
                                             
                  <input type="hidden" name="idPedidos" id="idPedidos" value="<?php echo $dados[0]->idPedidos ?>" />                
                  <input type="hidden" name="estoque" id="estoque" value=""/> 
             
                  <div class="col-xs-2"> 
                    <label for="quantidade" class="control-label">Grupo </label> 
                    <div class="input-group margin col-xs-12">                                 
                      <select class="form-control" name="categoria_prod_id" id="grupo_prod_pedido">
                        <option value="">Selecione</option>
                        <?php foreach ($categoria_prod as $valor) { ?>
                          <option value='<?php echo $valor->categoria_prod_id; ?>'><?php echo $valor->categoria_prod_descricao; ?> </option>
                        <?php } ?>
                      </select>
                    </div>
                  </div> 

                  <div class="col-xs-6"> 
                    <label for="quantidade" class="control-label">Produto </label> 
                    <div class="input-group margin">
                      <div class="input-group-btn">
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-default" >P</button>
                      </div>                
                             
                      <select class="select2" style="width: 100%;" name="produto_desc" id="produto_desc">
                        <option value="">Selecione um grupo</option>                       
                      </select>    
                    </div>
                  </div> 

<!--                   <div class="col-xs-2"> 
                    <label for="subTotal" class="control-label">Valor Unitário</label> 
                    <div class="input-group margin" id="valord">                                 
                      <input type="text"  class="form-control money">
                    </div>
                  </div>  -->    


                  <div class="col-xs-2"> 
                    <label for="quantidade" class="control-label">Quantidade </label> 
                    <div class="input-group margin">                                 
                      <input type="text"  class="form-control" name="quantidade" id="quantidade" placeholder="Quantidade" required="required">
                    </div>
                  </div>  


                <div class="col-md-2">
                  <div class="form-group">
                    <label for="btnAdicionarProdutoPedidoFornecedor" class="control-label">&nbsp </label> </br>
                    <span class="btn btn-success" id="btnAdicionarProdutoPedidoFornecedor"><i class="icon-white icon-plus"></i> Adicionar</span>                 
                  </div>
                </div>  
              

                </div>              



              </form>
            </div>
          </fieldset> 



          <div class="box-footer">
                        
              <!-- /.box-header -->
              <div id="divProdutos" class="box-body no-padding">
                <table class="table table-condensed">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Produto</th>                     
                      <th>Quantidade</th>                      
                      <th>Pago</th>
                      <th>ValorFatura</th>                      
                      <th style="width: 40px">Ações</th>
                    </tr>
                  </thead>  
                  <tbody>
                    <?php
                    $total = 0;
                    $qtd = 0;
                    $seq = 1;
                     // echo "<pre>";
                     // var_dump($produtos);die();
                    $totalGeral = 0;

                    foreach ($produtos as $p) {            

                        $qtd = $qtd + $p->quantidade;                        
                        echo '<tr>';
                        echo '<td>'.$seq ++.'</td>';
                        echo '<td>'.$p->produto_descricao.'</td>';
                        echo '<td>'.$p->quantidade.'</td>';                        
                        echo '<td>'.'Yes ou No'.'</td>';
                        echo '<td>'.'R$ DA FATURA'.'</td>';
                       
                        echo '<td><a  href="'.base_url().'index.php/pedidofornecedor/excluirProduto/'.$dados[0]->idPedidos.'/'.$p->idItens.'" title="Excluir Produto" ><i class="fa fa-trash text-danger"></i></a></td>';
                        //echo '<td><a style="margin: 1%" href="#modal-excluir" role="button" data-toggle="modal" produto="'.$p->produto_id.'" class="btn btn-danger tip-top" title="Excluir Produto"><i class="icon-remove icon-white"></i></a></td>';                                                   
                        echo '</tr>';
                    }?>
                                         

                  </tbody>
                  <tfoot>
                    <tr>                     
                      <th>ITENS:<?php echo ' '.count($produtos); ?></th>
                      <th></th>                      
                      <th>VOLUME:<?php echo ' '.$qtd; ?></th>
                      <th></th>  
                      <th></th>  
                      <th></th>                    
                    </tr>
                  </tfoot>
                </table>
              </div>            

          </div> 

          <!-- /.box-body -->
        <!--   <div class="box-footer">
            <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>" class="btn btn-default">Voltar</a>
            <button type="submit" class="btn btn-primary pull-right"><?php echo ucfirst($this->uri->segment(2)); ?></button>
          </div> -->
          <!-- /.box-footer -->


      </div>
    </div>
  </div>
</section>



