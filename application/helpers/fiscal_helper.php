<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');


function statusSefaz($dadosNf)
{
	header('Content-Type: text/html; charset=utf-8');
	require_once __DIR__ . "/fiscal/src/WebmaniaBR/NFe.php";

	$settings = array(
		'oauth_access_token' => $dadosNf[0]->oauth_access_token,
		'oauth_access_token_secret' => $dadosNf[0]->oauth_access_token_secret,
		'consumer_key' => $dadosNf[0]->consumer_key,
		'consumer_secret' => $dadosNf[0]->consumer_secret
	);

	$webmaniabr = new NFe($settings);
	$response = $webmaniabr->statusSefaz();

	if (isset($response->error)) {
		return $response->error;
	} else {
		return $response ? true : false;
	}
}

function validarCertificadoSefaz($dadosNf)
{
	header('Content-Type: text/html; charset=utf-8');
	require_once __DIR__ . "/fiscal/src/WebmaniaBR/NFe.php";

	$settings = array(
		'oauth_access_token' => $dadosNf[0]->oauth_access_token,
		'oauth_access_token_secret' => $dadosNf[0]->oauth_access_token_secret,
		'consumer_key' => $dadosNf[0]->consumer_key,
		'consumer_secret' => $dadosNf[0]->consumer_secret
	);

	$webmaniabr = new NFe($settings);
	$response = $webmaniabr->validadeCertificado();

	$dados = array();

	if (isset($response->error)) {
		$dados['status'] = false;
		$dados['msg']    = 'Erro: ' . $response->error . '.';
	} else {
		//var_dump($response);die();
		if ($response > 45) {
			$dados['status'] = true;
			$dados['msg']    = 'Certificado Digital A1 válido por ' . $response . ' dias.';
		} elseif ($response <= 45 && $response >= 1) {
			$dados['status'] = true;
			$dados['msg']    = 'Emita um novo Certificado Digital A1 - vencerá em ' . $response . ' dias.';
		} else {
			$dados['status'] = false;
			$dados['msg']    = 'Certificado Digital A1 vencido. Emita um novo para continuar operando.';
		}
	}
	return $dados;
}

function emitirNotaFiscal($dadosNf)
{
	// echo "<pre>";
	// var_dump($dadosNf);die();
	header('Content-Type: text/html; charset=utf-8');
	require_once __DIR__ . "/fiscal/src/WebmaniaBR/NFe.php";

	$settings = array(
		'oauth_access_token' => $dadosNf['emitente'][0]->oauth_access_token,
		'oauth_access_token_secret' => $dadosNf['emitente'][0]->oauth_access_token_secret,
		'consumer_key' => $dadosNf['emitente'][0]->consumer_key,
		'consumer_secret' => $dadosNf['emitente'][0]->consumer_secret
	);

	$webmaniabr = new NFe($settings);
	// die(var_dump($webmaniabr));

	$ambiente = 2; // 1 - Produção ou 2 - Homologação // Identificação do Ambiente do Sefaz 
	// Pedido pesso fisica.
	$documento = str_replace('.', '', str_replace('/', '', str_replace('-', '', $dadosNf['result']->cliente_documento)));
	$pessoaFisica = (strlen($documento) < 14) ? true : false;
	// die(var_dump(strlen($dadosNf['result']->cliente_documento)));
	$forma_pagamento  = explode(',', $dadosNf['PedidoNFCe'][0]->forma_pagamento);
	$valor_pagamento  = explode(',', $dadosNf['PedidoNFCe'][0]->valor_pagamento);

	if (!$pessoaFisica) {
		// Pessoa Juridica
		$data = array(
			'ID'				=> $dadosNf['result']->fiscal_nf_id, // Número do pedido
			'operacao' 			=> (int)$dadosNf['result']->fiscal_nf_operacao, // Tipo de Operação da Nota Fiscal
			'natureza_operacao'	=> $dadosNf['result']->fiscal_nf_natureza_operacao, // Natureza da Operação
			'modelo' 			=> $dadosNf['result']->fiscal_nf_modelo, // Modelo da Nota Fiscal 
			'emissao' 			=> $dadosNf['result']->fiscal_nf_emissao, // Tipo de Emissão da NF-e 
			'finalidade' 		=> (int)$dadosNf['result']->fiscal_nf_finalidade, // Finalidade de emissão da Nota Fiscal
			'ambiente' 			=> $ambiente, //(integer)$dadosNf['result']->ambiente, // Identificação do Ambiente do Sefaz 
			'nfe_referenciada' 	=> $dadosNf['result']->fiscal_nf_nfe_referenciada, // Chave de acesso da NF-e emitida anteriormente.
			'cliente' 			=> array(
				'cnpj'				=> $dadosNf['result']->cliente_documento, // cnpj pessoa juridica
				'ie' 				=> $dadosNf['result']->cliente_rgie, // cnpj pessoa juridica
				'razao_social' 		=> $dadosNf['result']->cliente_nomeCliente, // (pessoa fisica) Nome completo
				'endereco' 			=> $dadosNf['result']->cliente_rua, // Endereço de entrega dos produtos
				'complemento' 		=> '', // Complemento do endereço de entrega
				'numero' 			=> $dadosNf['result']->cliente_numero, // Número do endereço de entrega
				'bairro' 			=> $dadosNf['result']->cliente_bairro, // Bairro do endereço de entrega
				'cidade' 			=> $dadosNf['result']->cliente_cidade, // Cidade do endereço de entrega
				'uf' 				=> $dadosNf['result']->cliente_estado, // Estado do endereço de entrega
				'cep' 				=> $dadosNf['result']->cliente_cep, // CEP do endereço de entrega
				'telefone' 			=> $dadosNf['result']->cliente_telefone, // Telefone do cliente
				'email' 			=> $dadosNf['result']->cliente_email, // E-mail do cliente para envio da NF-e
				'consumidor_final' 	=> $dadosNf['result']->consumidor_final // consumidor final cliente para envio da NF-e

			),
			'pedido' 			=> array(
				'pagamento' 					=> $dadosNf['result']->fiscal_nf_forma_pgto, // Indicador da forma de pagamento 
				'presenca' 						=> (int)$dadosNf['result']->fiscal_nf_presenca, // Indicador de presença do comprador no estabelecimento comercial no momento da operação 
				'intermediador' 				=> 0, // Indicador de intermediador/marketplace
				'modalidade_frete' 				=> (int)$dadosNf['result']->fiscal_nf_modalidade_frete, // Modalidade do frete 
				'frete' 						=> $dadosNf['result']->fiscal_nf_valor_frete, // Total do frete 
				'desconto' 						=> $dadosNf['result']->fiscal_nf_valor_desconto, // Total do desconto 
				'total' 						=> $dadosNf['result']->fiscal_nf_total, // Total do pedido - sem descontos						
				'informacoes_complementares' 	=> $dadosNf['result']->fiscal_nf_informacoes_complementares, // Informações Complementares ao Consumidor
				// Teste NFc-e
				// P.J não emite NFc-e
				'forma_pagamento' 				=> $forma_pagamento, //["01","03"], // dinheiro e cartão de crédito Observação: Regra de validação valida a partir de 01/02/2021 para homologação e 01/09/2021 para produção
				'valor_pagamento' 				=> $valor_pagamento, // ["25.00","50.00"] // valor total de R$75,00		        
			)
		);
	} else {
		if ($dadosNf['result']->fiscal_nf_modelo == '2') {
			$cnpj_credenciadora = explode(',', $dadosNf['PedidoNFCe'][0]->cnpj_credenciadora);
			$bandeira_cart    	= explode(',', $dadosNf['PedidoNFCe'][0]->bandeira_cart);
			$autorizacao_NSU    = explode(',', $dadosNf['PedidoNFCe'][0]->autorizacao_NSU);

			# 2 - NFc-e c/ Cartão
			$pedido1 = array(
				'pedido' => array(
					'pagamento' 					=> (int)$dadosNf['result']->fiscal_nf_forma_pgto, // Indicador da forma de pagamento 
					'presenca' 						=> (int)$dadosNf['result']->fiscal_nf_presenca, // Indicador de presença do comprador no estabelecimento comercial no momento da operação 
					'intermediador' 				=> 0, // Indicador de intermediador/marketplace
					'modalidade_frete' 				=> (int)$dadosNf['result']->fiscal_nf_modalidade_frete, // Modalidade do frete 
					'frete' 						=> $dadosNf['result']->fiscal_nf_valor_frete, // Total do frete 
					'desconto' 						=> $dadosNf['result']->fiscal_nf_valor_desconto, // Total do desconto 
					'total' 						=> $dadosNf['result']->fiscal_nf_total, // Total do pedido - sem descontos			
					'informacoes_complementares' 	=> $dadosNf['result']->fiscal_nf_informacoes_complementares, // Informações Complementares ao Consumidor
					// Emissão NFc-e
					'forma_pagamento' 				=> $forma_pagamento, //["01","03"], // dinheiro e cartão de crédito
					'valor_pagamento' 				=> $valor_pagamento, // ["25.00","50.00"] // valor total de R$75,00	
					'valor_troco' 					=> 0.00,
					// Emissão NFc-e com cartão crédito ou débito
					'cnpj_credenciadora' 			=> $cnpj_credenciadora,
					'bandeira' 						=> $bandeira_cart,
					'autorizacao' 					=> $autorizacao_NSU
				)
			);
		} else {
			$pedido1 = array(
				'pedido'	=> array(
					'pagamento' => (int)$dadosNf['result']->fiscal_nf_forma_pgto, // Indicador da forma de pagamento (0 - Avista / 1 - prazo)
					// Emissão NF-e
					'forma_pagamento' 				=> $forma_pagamento, //["01","03"], // dinheiro e cartão de crédito
					'valor_pagamento' 				=> $valor_pagamento, // ["25.00","50.00"] // valor total de R$75,00
					'presenca'        				=> (int)$dadosNf['result']->fiscal_nf_presenca, // Indicador de presença do comprador no estabelecimento comercial no momento da operação 
					'intermediador'   				=> 0, // Indicador de intermediador/marketplace
					'modalidade_frete' 				=> (int)$dadosNf['result']->fiscal_nf_modalidade_frete, // Modalidade do frete 
					'frete'    						=> $dadosNf['result']->fiscal_nf_valor_frete, // Total do frete 
					'desconto' 						=> $dadosNf['result']->fiscal_nf_valor_desconto, // Total do desconto 
					'total' 						=> $dadosNf['result']->fiscal_nf_total, // Total do pedido - sem descontos
					'informacoes_complementares' 	=> $dadosNf['result']->fiscal_nf_informacoes_complementares, // Informações Complementares ao Consumidor			       	
				)
			);
		}
		// var_dump( strlen($dadosNf['result']->cliente_documento) );die();
		if (strlen($dadosNf['result']->cliente_documento) > 10) {
			$pedido = array(
				'ID' => $dadosNf['result']->fiscal_nf_id, // Número do pedido
				'operacao' => (int)$dadosNf['result']->fiscal_nf_operacao, // Tipo de Operação da Nota Fiscal
				'natureza_operacao' => $dadosNf['result']->fiscal_nf_natureza_operacao, // Natureza da Operação
				'modelo' => $dadosNf['result']->fiscal_nf_modelo, // Modelo da Nota Fiscal 
				'emissao' => $dadosNf['result']->fiscal_nf_emissao, // Tipo de Emissão da NF-e 
				'finalidade' => (int)$dadosNf['result']->fiscal_nf_finalidade, // Finalidade de emissão da Nota Fiscal
				'ambiente' => $ambiente, //(integer)$dadosNf['result']->ambiente, // Identificação do Ambiente do Sefaz 
				'nfe_referenciada' => $dadosNf['result']->fiscal_nf_nfe_referenciada, // Chave de acesso da NF-e emitida anteriormente.
				'cliente' => array(
					'cpf' => $dadosNf['result']->cliente_documento, // (pessoa fisica) Número do CPF
					'nome_completo' => $dadosNf['result']->cliente_nomeCliente, // (pessoa fisica) Nome completo
					'endereco' => $dadosNf['result']->cliente_rua, // Endereço de entrega dos produtos
					'complemento' => '', // Complemento do endereço de entrega
					'numero' => $dadosNf['result']->cliente_numero, // Número do endereço de entrega
					'bairro' => $dadosNf['result']->cliente_bairro, // Bairro do endereço de entrega
					'cidade' => $dadosNf['result']->cliente_cidade, // Cidade do endereço de entrega
					'uf' => $dadosNf['result']->cliente_estado, // Estado do endereço de entrega
					'cep' => $dadosNf['result']->cliente_cep, // CEP do endereço de entrega
					'telefone' => $dadosNf['result']->cliente_telefone, // Telefone do cliente
					'email' => $dadosNf['result']->cliente_email, // E-mail do cliente para envio da NF-e
					'consumidor_final' => 1 // consumidor final cliente para envio da NF-e
				)
			);
		} else {
			$pedido = array(
				'ID' => $dadosNf['result']->fiscal_nf_id, // Número do pedido
				'operacao' => (int)$dadosNf['result']->fiscal_nf_operacao, // Tipo de Operação da Nota Fiscal
				'natureza_operacao' => $dadosNf['result']->fiscal_nf_natureza_operacao, // Natureza da Operação
				'modelo' => $dadosNf['result']->fiscal_nf_modelo, // Modelo da Nota Fiscal 
				'emissao' => $dadosNf['result']->fiscal_nf_emissao, // Tipo de Emissão da NF-e 
				'finalidade' => (int)$dadosNf['result']->fiscal_nf_finalidade, // Finalidade de emissão da Nota Fiscal
				'ambiente' => $ambiente, //(integer)$dadosNf['result']->ambiente, // Identificação do Ambiente do Sefaz 
				'nfe_referenciada' => $dadosNf['result']->fiscal_nf_nfe_referenciada, // Chave de acesso da NF-e emitida anteriormente.
			);
		}

		$data = array_merge($pedido, $pedido1);
	}
	// Produtos
	$items = array();
	// die(var_dump($dadosNf['vendas']));
	foreach ($dadosNf['vendas'] as $item) :
		$data['produtos'][] = array(
			'nome' => ($item->imei_valor <> '') ? $item->produto_descricao . ' - ' . $item->imei_valor : $item->produto_descricao, // Nome do produto
			// 'sku' => $item->produto_id, // Código identificador - SKU -- Bruno Nf-e versão 3.10
			'sku' => $item->produto_codigo_barra <> '' ? $item->produto_codigo_barra : $item->produto_id, // Código identificador - SKU
			'ncm' => str_pad($item->produto_ncm, 8, 0, STR_PAD_LEFT), // Código NCM
			'cest' =>  str_pad($item->produto_cest, 7, 0, STR_PAD_LEFT), // Código CEST
			'quantidade' => $item->quantidade, // Quantidade de itens
			'unidade' => $item->produto_unidade, // Unidade de medida da quantidade de itens 
			'peso'    => $item->produto_peso, // Peso em KG. Ex: 800 gramas = 0.800 KG
			'origem'   => (int)$item->produto_origem, // Origem do produto 
			'subtotal' => $item->subTotal / $item->quantidade, // $item->precoVendido, // Preço unitário do produto - sem descontos
			'total' => $item->subTotal, // ($item->precoVendido * $item->quantidade) // Preço total (quantidade x preço unitário) - sem descontos 
			//'classe_imposto' => 'REF113516' // Referência do imposto cadastrado 	 
			'tributos_federais' => $item->imposto_tributos_federais,
			'tributos_estaduais' => $item->imposto_tributos_estaduais,
			'impostos' => [
				'icms' => [
					'codigo_cfop' => $item->imposto_icms_codigo_CFOP,
					'situacao_tributaria' =>  $item->imposto_icms_situacao_tributaria
				],
				'ipi' => [
					'situacao_tributaria' =>  $item->imposto_ipi_situacao_tributaria,
					'codigo_enquadramento' =>  (int) $item->imposto_ipi_codigo_enquadramento,
					'aliquota' =>  $item->imposto_ipi_aliquota
				],
				'pis' => [
					'situacao_tributaria' =>  $item->imposto_pis_situacao_tributaria,
					'aliquota' =>  $item->imposto_pis_aliquota
				],
				'cofins' => [
					'situacao_tributaria' =>  $item->imposto_cofins_situacao_tributaria,
					'aliquota' =>  $item->imposto_cofins_aliquota
				]
			]
		);
	endforeach;
	// Emissão
	$response = $webmaniabr->emissaoNotaFiscal($data);
	
	$msg = "";
	// Retorno
	if (isset($response->error)) {
		$msg .= 'Erro: ' . $response->error . '';

		if (isset($response->log)) {

			$msg .= 'Log:';
			$msg .= '<ul>';

			foreach ($response->log as $erros) {
				foreach ($erros as $erro) {
					$msg .= '<li>' . $erro . '</li>';
				}
			}

			$msg .= '</ul>';
		}

		$dados['status'] = false;
		$dados['msg']    = $msg;
	} else {
		$dados['status'] = true;
		$dados['msg']    = '<h2>NF-e enviada com sucesso.</h2>';

		$status = (string) $response->status; // aprovado, reprovado, cancelado, processamento ou contingencia
		$nfe = (int) $response->nfe; // número da NF-e
		$serie = (int) $response->serie; // número de série
		//$recibo = (int) $response->recibo; // número do recibo
		$chave = $response->chave; // número da chave de acesso
		$xml = (string) $response->xml; // URL do XML
		$danfe = (string) $response->danfe; // URL do Danfe (PDF)
		$log = $response->log;

		$dados['resposta'] = $response;
	}
	return $dados;
	/*echo "<pre>";/
	die();
*/
}

function cancelamentoNotaFiscal($dadosNf)
{
	header('Content-Type: text/html; charset=utf-8');
	require_once __DIR__ . "/fiscal/src/WebmaniaBR/NFe.php";

	$settings = array(
		'oauth_access_token' => $dadosNf['emitente'][0]->oauth_access_token,
		'oauth_access_token_secret' => $dadosNf['emitente'][0]->oauth_access_token_secret,
		'consumer_key' => $dadosNf['emitente'][0]->consumer_key,
		'consumer_secret' => $dadosNf['emitente'][0]->consumer_secret
	);

	$webmaniabr = new NFe($settings);

	$chave    = $dadosNf['result']->fiscal_nf_fiscal_chave;
	$motivo   = 'Cancelamento por motivos administrativos.';
	$ambiente = 2;

	$response = $webmaniabr->cancelarNotaFiscal($chave, $motivo, $ambiente);

	$msg = "";

	// die(var_dump($response));
	if (isset($response->error)) {
		$msg .= 'Erro: ' . $response->error . '';

		if (isset($response->log)) {
			$msg .= 'Log:';
			$msg .= '<ul>';
			foreach ($response->log as $erros) {
				foreach ($erros as $erro) {
					$msg .= '<li>' . $erro . '</li>';
				}
			}

			$msg .= '</ul>';
		}
		$dados['status'] = false;
		$dados['msg']    = $msg;
	} else {
		$dados['status'] = true;
		$dados['msg']    = 'Resultado do Cancelamento:';

		$status = (string) $response->status;
		$xml = (string) $response->xml;
		$log = $response->log;

		$dados['status'] = true;
		$dados['resposta'] = $response;
	}
	return $dados;
}

function cartacorrecaoNotaFiscal($dadosNf)
{
	header('Content-Type: text/html; charset=utf-8');
	require_once __DIR__ . "/fiscal/src/WebmaniaBR/NFe.php";

	$ambiente = 2; //(integer)$dadosNf['result']->ambiente, // Identificação do Ambiente do Sefaz 

	$settings = array(
		'oauth_access_token' => $dadosNf['emitente'][0]->oauth_access_token,
		'oauth_access_token_secret' => $dadosNf['emitente'][0]->oauth_access_token_secret,
		'consumer_key' => $dadosNf['emitente'][0]->consumer_key,
		'consumer_secret' => $dadosNf['emitente'][0]->consumer_secret
	);

	$webmaniabr = new NFe($settings);

	$chave      = $dadosNf['result']->cc_chave;
	$correcao   = $dadosNf['result']->cc_correcao;
	$ambiente   = $ambiente;

	$response = $webmaniabr->cartacorrecaoNotaFiscal($chave, $correcao, $ambiente);

	$msg = "";

	if (isset($response->error)) {
		$msg .= 'Erro: ' . $response->error . '';

		if (isset($response->log)) {

			$msg .= 'Log:';
			$msg .= '<ul>';

			foreach ($response->log as $erros) {
				foreach ($erros as $erro) {
					$msg .= '<li>' . $erro . '</li>';
				}
			}
			$msg .= '</ul>';
		}
		$dados['status'] = false;
		$dados['msg']    = $msg;
	} else {
		$dados['status'] = true;
		$dados['msg']    = '<h2>CC-e atualizada com sucesso.</h2>';

		$status = (string) $response->status;
		$xml = (string) $response->xml;
		$log = $response->log;

		$dados['status'] = true;
		$dados['resposta'] = $response;
	}
	return $dados;
}

function devolucaoNotaFiscal($dadosNf)
{
	// echo "<pre>";
	// var_dump($dadosNf);die();
	header('Content-Type: text/html; charset=utf-8');
	require_once __DIR__ . "/fiscal/src/WebmaniaBR/NFe.php";

	$ambiente = 2; //(integer)$dadosNf['result']->ambiente, // Identificação do Ambiente do Sefaz 

	$settings = array(
		'oauth_access_token' => $dadosNf['emitente'][0]->oauth_access_token,
		'oauth_access_token_secret' => $dadosNf['emitente'][0]->oauth_access_token_secret,
		'consumer_key' => $dadosNf['emitente'][0]->consumer_key,
		'consumer_secret' => $dadosNf['emitente'][0]->consumer_secret
	);

	//echo "<pre>"; var_dump($settings);die();
	// Bruno Magnata - 271017 	
	$webmaniabr = new NFe($settings);
	$chave      = $dadosNf['result']->fiscal_nf_nfe_referenciada; //$dadosNf['result']->fiscal_chave;	
	$natureza_operacao =  $dadosNf['result']->fiscal_nf_natureza_operacao;
	$codigo_cfop =  $dadosNf['result']->fiscal_cefop_devolucao;
	$produtos    =  array("3");
	$quantidade  =  array("2");
	$ambiente =  $ambiente; // 1 - Produção ou 2 - Homologação
	$response =  $webmaniabr->devolucaoNotaFiscal($chave, $natureza_operacao, $codigo_cfop, $ambiente, $produtos, $quantidade);

	$msg = "";
	// Retorno
	if (isset($response->error)) {
		$msg .= 'Erro: ' . $response->error . '';
		if (isset($response->log)) {

			$msg .= 'Log:';
			$msg .= '<ul>';

			foreach ($response->log as $erros) {
				foreach ($erros as $erro) {
					$msg .= '<li>' . $erro . '</li>';
				}
			}
			$msg .= '</ul>';
		}

		$dados['status'] = false;
		$dados['msg']    = $msg;
	} else {
		$dados['status'] = true;
		$dados['msg']    = '<h2>NF-e de devolução emitida com sucesso.</h2>';

		$status = (string) $response->status; // aprovado, reprovado, cancelado, processamento ou contingencia
		$nfe = (int) $response->nfe; // número da NF-e
		$serie = (int) $response->serie; // número de série
		$recibo = (int) $response->recibo; // número do recibo
		$chave = $response->chave; // número da chave de acesso
		$xml = (string) $response->xml; // URL do XML
		$danfe = (string) $response->danfe; // URL do Danfe (PDF)
		$log = $response->log;

		$dados['resposta'] = $response;
	}
	return $dados;
}
