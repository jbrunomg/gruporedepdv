<?php if (!defined('BASEPATH')) exit ('No direct script access allowed');

function emitirNotaFiscalDeServico($dadosNfs)
{
	header('Content-Type: text/html; charset=utf-8');
	require_once __DIR__ . "/fiscal/src/WebmaniaBR/NFSe.php";

	$settings = array(
        'authorization_bearer_token' => $dadosNfs[0]->bearer_access_token
    );

	$webmaniabr = new NFSe($settings);

	return $webmaniabr->emitirNotaFiscalDeServico($dadosNfs[1]);
}

function cancelamentoNotaFiscalDeServico($dadosNfs)
{
	header('Content-Type: text/html; charset=utf-8');
	require_once __DIR__ . "/fiscal/src/WebmaniaBR/NFSe.php";

	$settings = array(
        'authorization_bearer_token' => $dadosNfs[0]->bearer_access_token
    );

	$webmaniabr = new NFSe($settings);

    return $webmaniabr->cancelarNotaFiscalDeServico($dadosNfs[1]);
}
