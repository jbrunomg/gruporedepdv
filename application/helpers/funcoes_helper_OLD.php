<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @param $parametro Função para auxiliar na programação -> die + var_dump
 */
function v($parametro)
{
	echo "<pre>";
	var_dump($parametro);
	die();
}

function p($parametro)
{
    echo "<pre>";
    print_r($parametro);
    die();
}

/**
 * [Upload description] Função para enviar imagens para o servidor
 * @return [type] [description]
 */
function Upload($caminho,$nome)
{

    $CI = & get_instance();  //get instance, access the CI superobject

    $configUpload['upload_path']   = $caminho;
    $configUpload['allowed_types'] = 'gif|jpg|png|jpeg';
    $configUpload['overwrite']     = true;

    $CI->upload->initialize($configUpload);

    if (!is_dir($caminho)) {
        mkdir($caminho, 0777, TRUE);

    }

    $extensao = pathinfo($_FILES['imagem']['name'],PATHINFO_EXTENSION);

    $_FILES['imagem']['name'] = $nome .'.'.$extensao;

    if ( ! $CI->upload->do_upload('imagem')){

        return false;

    } else {

        $data['imagem'] = $CI->upload->data();
        return $data;

    }

}

/**
 * [verificarPermissao description] Método para validar o acesso do usuário aos módulos do sistema
 * @param $param módulo a ser validado
 * @return bool retorna se tem ou não a permissão
 */
function verificarPermissao($permissao)
{       
      $CI = & get_instance();  //get instance, access the CI superobject
      $permissoes = unserialize($CI->session->userdata('perfil_permissoes'));
        
       if (array_key_exists($permissao, $permissoes)){          

          return true;

       } else {                             

          return false;

       }
}

/**
* Gera um arquivo PDF no browser.
* Para fazer o download direto sem ter que visualizar no browser, basta passar o nome do arquivo como parametro na funcao mPDF::Output(<nome_do_arquivo_aqui>).
* Verificar o manual do mPDF para mais informacoes. (https://mpdf.github.io/)
*
* @param string $HTMLInput Guarda a view a ser reproduzida no PDF em formato de string
*
* @return void
*/
function gerarPDF($html = '', $formato = '') {

    // Require composer autoload
    require_once __DIR__ . '/../../vendor/autoload.php';

    $mpdf = new mPDF('c',$formato, 12, '', 8, 8, 10, 10, 9, 9, 'L');

    $mpdf->SetFooter('{PAGENO}/{nb}');

    $stylesheet = file_get_contents('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css');
    $stylesheet2 = "table{
      width: 100%;
      text-align:center;
      border: 1px solid #000;
      border-collapse: collapse;
      background: #fff;
      color: #000;
    }

    th{
      border: 1px solid #000;
    }
    tr.even{
      background: #fff;
    }
    tr.black{
      background: #fff;
    }
    td.black{
      color: #000;
      border: 1px solid #000;
      border-collapse: collapse;
    }
    td{
      color: black;
      font-size: 9px;
      border: 1px solid #000;
      border-collapse: collapse;
    }

    ";
    $mpdf->WriteHTML($stylesheet,1); 
    $mpdf->WriteHTML($stylesheet2,1); 
    $mpdf->WriteHTML($html,2);
    $mpdf->Output();

}

/**
 * [mesPorextenso description] Retorna o mês por extenso
 * @param $mes o número referente ao mês desejado
 * @return string Retorna o mês em extenso
 */
function mesPorextenso($mes){

    switch ($mes){

        case "1":  $mes = "Janeiro"; break;
        case "2":  $mes = "Fevereiro"; break;
        case "3":  $mes = "Março"; break;
        case "4":  $mes = "Abril"; break;
        case "5":  $mes = "Maio"; break;
        case "6":  $mes = "Junho"; break;
        case "7":  $mes = "Julho"; break;
        case "8":  $mes = "Agosto"; break;
        case "9":  $mes = "Setembro"; break;
        case "10": $mes = "Outubro"; break;
        case "11": $mes = "Novembro"; break;
        case "12": $mes = "Dezembro"; break;

        default:

    }

    return $mes;

}

/**
 * [validaCep description] Verifica se o cep informado é válido
 * @param $cep recebe o cep a ser validado
 * @return bool retorna se é válido ou não
 */
function validaCep($cep) {

    $cep = trim($cep);

    $avaliaCep = preg_match("^[0-9]{5}[0-9]{3}$^", $cep);

    if($avaliaCep != true){

        return false;

    } else {

        return true;

    }
}

/**
 * [validaCPFCNPJ description] Verifica se o cpf/cnpj informado é válido
 * @param $cpfcnpj cnpj/cpf a ser validado
 * @return bool retorna se é válido ou não
 */
function validaCPFCNPJ($cpfcnpj) {

    $cpfcnpj = trim($cpfcnpj);
    $cpfcnpj = preg_replace('/[^0-9]/', '', (string) $cpfcnpj);

    switch (strlen($cpfcnpj)) {

        case 11:
            if (preg_match("^([0-9]){3}([0-9]){3}([0-9]){3}([0-9]){2}$^", $cpfcnpj))
                return true;
            break;

        case 14:
            if (preg_match("^([0-9]){2}([0-9]){3}([0-9]){3}([0-9]){4}([0-9]){2}$^", $cpfcnpj))
                return true;
            break;

        default:
            return false;
            break;
    }

}
     

function formatarDataBr($data){

    return date('d/m/Y', strtotime($data));

}  

function formatarDataEn($data){

    $data = str_replace('/', '-', $data);
    return date('Y-m-d', strtotime($data));

}  

function soNumeros($valor){
   $valor = preg_replace('/[^-9]/', '', $valor);
   return $valor;
}
     
?>