<?php

class NFSe
{
    public $authorizationBearerToken;

    public function __construct(array $vars)
    {
        $this->authorizationBearerToken = $vars['authorization_bearer_token'];
    }

    /**
     * Saiba mais sobre emissão de notas fiscais de serviço:
     * https://webmaniabr.com/docs/rest-api-nfse/#emitir-nfse
     */
    public function emitirNotaFiscalDeServico($data)
    {
        $response = self::connectWebmaniaBR('POST', 'https://api.webmaniabr.com/2/nfse/emissao', $data);

        return $response;
    }

    /**
     * Saiba mais sobre cancelamento de notas fiscais de serviço:
     * https://webmaniabr.com/docs/rest-api-nfse/#cancelar-nfse
     */
    public function cancelarNotaFiscalDeServico($data)
    {
        $response = self::connectWebmaniaBR('PUT', 'https://api.webmaniabr.com/2/nfse/cancelar', $data);

        return $response;
    }

    public function connectWebmaniaBR($request, $endpoint, $data)
    {
        @set_time_limit( 300 );
        ini_set('max_execution_time', 300);
        ini_set('max_input_time', 300);
        ini_set('memory_limit', '256M');

        $headers = array(
            'Content-Type: application/json',
            'Accept: application/json',
            'Authorization: Bearer ' . $this->authorizationBearerToken
        );

        $rest = curl_init();
        curl_setopt($rest, CURLOPT_CONNECTTIMEOUT , 300); 
        curl_setopt($rest, CURLOPT_TIMEOUT, 300);
        curl_setopt($rest, CURLOPT_URL, $endpoint.'?time='.time());
        curl_setopt($rest, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($rest, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($rest, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($rest, CURLOPT_CUSTOMREQUEST, $request);
        curl_setopt($rest, CURLOPT_POSTFIELDS, json_encode( $data ));
        curl_setopt($rest, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($rest, CURLOPT_FRESH_CONNECT, true);
        $response = json_decode(curl_exec($rest));
        curl_close($rest);

        $existeErro = is_object($response) && property_exists($response, 'message') || property_exists($response, 'msg') || property_exists($response, 'error') || $response->status == 'reprovado';

        if ($existeErro) {
            if (property_exists($response, 'message')) {
                throw new Exception($response->message);
            } else if (property_exists($response, 'msg')) {
                throw new Exception($response->msg);
            } else if (property_exists($response, 'error')) {
                throw new Exception($response->error);
            } else {
                throw new Exception($response->motivo[0]);
            }
        }

        return $response;
    }
}
