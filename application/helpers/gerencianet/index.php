<?php 

 
require __DIR__.'/vendor/autoload.php'; // caminho relacionado ao Composer
 
use Gerencianet\Exception\GerencianetException;
use Gerencianet\Gerencianet;
 
function inicarTransacao($value='')
{

	$clientId = 'Client_Id_d96016a657682e092e9afd3f5ef9617e44e2c85d'; // insira seu Client_Id, conforme o ambiente (Des ou Prod)
	$clientSecret = 'Client_Secret_191a7de70d39ad33f98af7a490dd3fffff19579a'; // insira seu Client_Secret, conforme o ambiente (Des ou Prod)
	 
	$options = [
	  'client_id' => $clientId,
	  'client_secret' => $clientSecret,
	  'sandbox' => true // altere conforme o ambiente (true = desenvolvimento e false = producao)
	];


	$item_1 = [
	    'name' => 'Item 1', // nome do item, produto ou serviço
	    'amount' => 1, // quantidade
	    'value' => 1000 // valor (1000 = R$ 10,00)
	];
	 
	$item_2 = [
	    'name' => 'Item 2', // nome do item, produto ou serviço
	    'amount' => 2, // quantidade
	    'value' => 2000 // valor (2000 = R$ 20,00)
	];
	 
	$items =  [
	    $item_1,
	    $item_2
	];

	//Exemplo para receber notificações da alteração do status da transação.
	//$metadata = ['notification_url'=>'sua_url_de_notificacao_.com.br']
	//Outros detalhes em: https://dev.gerencianet.com.br/docs/notificacoes

	//Como enviar seu $body com o $metadata
	//$body  =  [
	//    'items' => $items,
	//    'metadata' => $metadata
	//];

	$body  =  [
	    'items' => $items
	];

	try {
	    $api = new Gerencianet($options);
	    $charge = $api->createCharge([], $body);
	 
	    print_r($charge);
	    
	} catch (GerencianetException $e) {
	    print_r($e->code);
	    print_r($e->error);
	    print_r($e->errorDescription);
	} catch (Exception $e) {
	    print_r($e->getMessage());
	}
}

//associarFormaPagamento(128504);

function associarFormaPagamento($charge_id)
{
	$clientId = 'Client_Id_d96016a657682e092e9afd3f5ef9617e44e2c85d'; // insira seu Client_Id, conforme o ambiente (Des ou Prod)
	$clientSecret = 'Client_Secret_191a7de70d39ad33f98af7a490dd3fffff19579a'; // insira seu Client_Secret, conforme o ambiente (Des ou Prod)
	 
	$options = [
	  'client_id' => $clientId,
	  'client_secret' => $clientSecret,
	  'sandbox' => true // altere conforme o ambiente (true = desenvolvimento e false = producao)
	];

	// $charge_id refere-se ao ID da transação gerada anteriormente
	$params = [
	  'id' => $charge_id
	];
	 
	$customer = [
	  'name' => 'Gorbadoc Oldbuck', // nome do cliente
	  'cpf' => '94271564656' , // cpf válido do cliente
	  'phone_number' => '5144916523' // telefone do cliente
	];
	 
	$bankingBillet = [
	  'expire_at' => '2018-12-12', // data de vencimento do boleto (formato: YYYY-MM-DD)
	  'customer' => $customer
	];
	 
	$payment = [
	  'banking_billet' => $bankingBillet // forma de pagamento (banking_billet = boleto)
	];
	 
	$body = [
	  'payment' => $payment
	];
	 
	try {
	    $api = new Gerencianet($options);
	    $charge = $api->payCharge($params, $body);
	 
	    print_r($charge);
	} catch (GerencianetException $e) {
	    print_r($e->code);
	    print_r($e->error);
	    print_r($e->errorDescription);
	} catch (Exception $e) {
	    print_r($e->getMessage());
	}
	}
 ?>