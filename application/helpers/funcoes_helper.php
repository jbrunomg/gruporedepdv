<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @param $parametro Função para auxiliar na programação -> die + var_dump
 */
function v($parametro)
{
	echo "<pre>";
	var_dump($parametro);
	die();
}

function p($parametro)
{
    echo "<pre>";
    print_r($parametro);
    die();
}

/**
 * [Upload description] Função para enviar imagens para o servidor
 * @return [type] [description]
 */
function Upload($caminho,$nome)
{

    $CI = & get_instance();  //get instance, access the CI superobject

    $configUpload['upload_path']   = $caminho;
    $configUpload['allowed_types'] = 'gif|jpg|png|jpeg';
    $configUpload['overwrite']     = true;

    $CI->upload->initialize($configUpload);

    if (!is_dir($caminho)) {
        mkdir($caminho, 0777, TRUE);

    }

    $extensao = pathinfo($_FILES['imagem']['name'],PATHINFO_EXTENSION);

    $_FILES['imagem']['name'] = $nome .'.'.$extensao;

    if ( ! $CI->upload->do_upload('imagem')){

        return false;

    } else {

        $data['imagem'] = $CI->upload->data();
        return $data;

    }

}

/**
 * [verificarPermissao description] Método para validar o acesso do usuário aos módulos do sistema
 * @param $param módulo a ser validado
 * @return bool retorna se tem ou não a permissão
 */
function verificarPermissao($permissao)
{
      $CI = & get_instance();  //get instance, access the CI superobject
      $permissoes = unserialize($CI->session->userdata('perfil_permissoes'));


    // https://stackoverflow.com/questions/12747066/warning-array-key-exists-expects-parameter-2-to-be-array-boolean-given
    //if(array_key_exists($permissao, $permissoes)){

    if (is_array($permissoes) && array_key_exists($permissao, $permissoes)) {
          return true;

       } else {

          return false;

       }
}

/**
* Gera um arquivo PDF no browser.
* Para fazer o download direto sem ter que visualizar no browser, basta passar o nome do arquivo como parametro na funcao mPDF::Output(<nome_do_arquivo_aqui>).
* Verificar o manual do mPDF para mais informacoes. (https://mpdf.github.io/)
*
* @param string $HTMLInput Guarda a view a ser reproduzida no PDF em formato de string
*
* @return void
*/
function gerarPDF($html = '', $formato = '') {

    // Require composer autoload
    require_once __DIR__ . '/../../vendor/autoload.php';

    $mpdf = new \Mpdf\Mpdf([
    'mode' => 'utf-8',
    'format' => $formato,
    'orientation' => 'P'
    ]);

    $mpdf->SetFooter('{PAGENO}/{nb}');

    $stylesheet = file_get_contents('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css');
    $stylesheet2 = "table{
      width: 100%;
      text-align:center;
      border: 1px solid #000;
      border-collapse: collapse;
      background: #fff;
      color: #000;
    }

    th{
      border: 1px solid #000;
    }
    tr.even{
      background: #fff;
    }
    tr.black{
      background: #fff;
    }
    td.black{
      color: #000;
      border: 1px solid #000;
      border-collapse: collapse;
    }
    td{
      color: black;
      font-size: 9px;
      border: 1px solid #000;
      border-collapse: collapse;
    }

    ";
    $mpdf->WriteHTML($stylesheet,1);
    $mpdf->WriteHTML($stylesheet2,1);
    $mpdf->WriteHTML($html,2);
    $mpdf->Output();

}

/**
 * [mesPorextenso description] Retorna o mês por extenso
 * @param $mes o número referente ao mês desejado
 * @return string Retorna o mês em extenso
 */
function mesPorextenso($mes){

    switch ($mes){

        case "1":  $mes = "Janeiro"; break;
        case "2":  $mes = "Fevereiro"; break;
        case "3":  $mes = "Março"; break;
        case "4":  $mes = "Abril"; break;
        case "5":  $mes = "Maio"; break;
        case "6":  $mes = "Junho"; break;
        case "7":  $mes = "Julho"; break;
        case "8":  $mes = "Agosto"; break;
        case "9":  $mes = "Setembro"; break;
        case "10": $mes = "Outubro"; break;
        case "11": $mes = "Novembro"; break;
        case "12": $mes = "Dezembro"; break;

        default:

    }

    return $mes;

}

/**
 * [validaCep description] Verifica se o cep informado é válido
 * @param $cep recebe o cep a ser validado
 * @return bool retorna se é válido ou não
 */
function validaCep($cep) {

    $cep = trim($cep);

    $avaliaCep = preg_match("^[0-9]{5}[0-9]{3}$^", $cep);

    if($avaliaCep != true){

        return false;

    } else {

        return true;

    }
}

/**
 * [validaCPFCNPJ description] Verifica se o cpf/cnpj informado é válido
 * @param $cpfcnpj cnpj/cpf a ser validado
 * @return bool retorna se é válido ou não
 */
function validaCPFCNPJ($cpfcnpj) {

    $cpfcnpj = trim($cpfcnpj);
    $cpfcnpj = preg_replace('/[^0-9]/', '', (string) $cpfcnpj);

    switch (strlen($cpfcnpj)) {

        case 11:
            if (preg_match("^([0-9]){3}([0-9]){3}([0-9]){3}([0-9]){2}$^", $cpfcnpj))
                return true;
            break;

        case 14:
            if (preg_match("^([0-9]){2}([0-9]){3}([0-9]){3}([0-9]){4}([0-9]){2}$^", $cpfcnpj))
                return true;
            break;

        default:
            return false;
            break;
    }

}


function formatarDataBr($data){

    return date('d/m/Y', strtotime($data));

}

function formatarDataEn($data){

    $data = str_replace('/', '-', $data);
    return date('Y-m-d', strtotime($data));

}

function soNumeros($valor){
   $valor = preg_replace('/[^-9]/', '', $valor);
   return $valor;
}

function gerarXML($dados, $emitente, $financeiro) {

    $xml = '<?xml version="1.0" encoding="utf-8" ?>
    <nfeProc versao="4.00" xmlns="http://www.portalfiscal.inf.br/nfe">
    <NFe xmlns="http://www.portalfiscal.inf.br/nfe">
    <infNFe Id="NCell'.$dados[0]->idVendas.str_replace('-', '', $dados[0]->dataVenda).str_replace(':', '', $dados[0]->horaVenda).'" versao="4.00">
    <ide>
    <cUF>26</cUF>
    <cNF>89013267</cNF>
    <natOp>VENDA</natOp>
    <mod>55</mod>
    <serie>1</serie>
    <nNF>'.$dados[0]->idVendas.'</nNF>
    <dhEmi>'.$dados[0]->dataVenda.'</dhEmi>
    <dhSaiEnt>'.$dados[0]->dataVenda.'</dhSaiEnt>
    <tpNF>1</tpNF>
    <idDest>1</idDest>
    <cMunFG>2611606</cMunFG>
    <tpImp>1</tpImp>
    <tpEmis>1</tpEmis>
    <cDV>9</cDV>
    <tpAmb>1</tpAmb>
    <finNFe>1</finNFe>
    <indFinal>0</indFinal>
    <indPres>1</indPres>
    <indIntermed>0</indIntermed>
    <procEmi>0</procEmi>
    <verProc>4.00|WEBMANIABR</verProc>
    </ide>
    <emit>
    <CNPJ>'.$emitente[0]->emitente_cnpj.'</CNPJ>
    <xNome>'.$emitente[0]->emitente_nome.'</xNome>
    <xFant>'.$emitente[0]->emitente_nome_fantasia.'</xFant>
    <enderEmit>
    <xLgr>'.$emitente[0]->emitente_rua.'</xLgr>
    <nro>'.$emitente[0]->emitente_numero.'</nro>
    <xCpl>'.$emitente[0]->emitente_complemento.'</xCpl>
    <xBairro>'.$emitente[0]->emitente_bairro.'</xBairro>
    <cMun>2611606</cMun>
    <xMun>>'.$emitente[0]->emitente_cidade.'</xMun>
    <UF>>'.$emitente[0]->emitente_uf.'</UF>
    <CEP>>'.$emitente[0]->emitente_cep.'</CEP>
    <cPais>1058</cPais>
    <xPais>BRASIL</xPais>
    </enderEmit>
    <IE>'.$emitente[0]->emitente_ie.'</IE>
    <CRT>1</CRT>
    </emit>
    <dest>
    <CNPJ>'.$dados[0]->cliente_cpf_cnpj.'</CNPJ>
    <xNome>'.$dados[0]->cliente_nome.'</xNome>
    <enderDest>
    <xLgr>'.$dados[0]->cliente_endereco.'</xLgr>
    <nro>'.$dados[0]->cliente_numero.'</nro>
    <xBairro>'.$dados[0]->cliente_bairro.'</xBairro>
    <cMun>2609600</cMun>
    <xMun>'.$dados[0]->cliente_cidade.'</xMun>
    <UF>'.$dados[0]->cliente_estado.'</UF>
    <CEP>'.$dados[0]->cliente_cep.'</CEP>
    <cPais>1058</cPais>
    <xPais>BRASIL</xPais>
    <fone>'.$dados[0]->cliente_celular.'</fone>
    </enderDest>
    <indIEDest>2</indIEDest>
    <email>andersonjksilva@outlook.com</email>
    </dest>
    <autXML>
    <CNPJ>30902296000105</CNPJ>
    </autXML>';

    for ($i=0; $i < count($dados); $i++) {
        $xml .= '<det nItem="'.($i+1).'">
        <prod>
        <cProd>'.trim($dados[$i]->produto_codigo).'</cProd>
        <cEAN>SEM GTIN</cEAN>
        <xProd>'.$dados[$i]->produto_descricao.' | '.$dados[$i]->imei_valor.'</xProd>
        <NCM>85441100</NCM>
        <CEST>1200700</CEST>
        <CFOP>5405</CFOP>
        <uCom>UND</uCom>
        <qCom>'.$dados[$i]->quantidade.'</qCom>
        <vUnCom>'.$dados[$i]->valor_unitario.'</vUnCom>
        <vProd>'.$dados[$i]->subTotal.'</vProd>
        <cEANTrib>SEM GTIN</cEANTrib>
        <uTrib>KG</uTrib>
        <qTrib>0.2</qTrib>
        <vUnTrib>80.0000</vUnTrib>
        <indTot>1</indTot>
        <xPed>7205</xPed>
        </prod>
        <imposto>
        <vTotTrib>0.00</vTotTrib>
        <ICMS>
        <ICMSSN500>
        <orig>0</orig>
        <CSOSN>500</CSOSN>
        <vBCSTRet>0.00</vBCSTRet>
        <pST>0.00</pST>
        <vICMSSubstituto>0.00</vICMSSubstituto>
        <vICMSSTRet>0.00</vICMSSTRet>
        </ICMSSN500>
        </ICMS>
        <IPI>
        <cEnq>999</cEnq>
        <IPITrib>
        <CST>99</CST>
        <vBC>0.00</vBC>
        <pIPI>0.00</pIPI>
        <vIPI>0.00</vIPI>
        </IPITrib>
        </IPI>
        <PIS>
        <PISOutr>
        <CST>99</CST>
        <vBC>0.00</vBC>
        <pPIS>0.00</pPIS>
        <vPIS>0.00</vPIS>
        </PISOutr>
        </PIS>
        <COFINS>
        <COFINSOutr>
        <CST>99</CST>
        <vBC>0.00</vBC>
        <pCOFINS>0.00</pCOFINS>
        <vCOFINS>0.00</vCOFINS>
        </COFINSOutr>
        </COFINS>
        </imposto>
        </det>';
    }

    $xml .= '<total>
    <ICMSTot>
    <vBC>0.00</vBC>
    <vICMS>0.00</vICMS>
    <vICMSDeson>0.00</vICMSDeson>
    <vFCPUFDest>0.00</vFCPUFDest>
    <vICMSUFDest>0.00</vICMSUFDest>
    <vICMSUFRemet>0.00</vICMSUFRemet>
    <vFCP>0.00</vFCP>
    <vBCST>0.00</vBCST>
    <vST>0.00</vST>
    <vFCPST>0.00</vFCPST>
    <vFCPSTRet>0.00</vFCPSTRet>
    <vProd>16.00</vProd>
    <vFrete>0.00</vFrete>
    <vSeg>0.00</vSeg>
    <vDesc>0.00</vDesc>
    <vII>0.00</vII>
    <vIPI>0.00</vIPI>
    <vIPIDevol>0.00</vIPIDevol>
    <vPIS>0.00</vPIS>
    <vCOFINS>0.00</vCOFINS>
    <vOutro>0.00</vOutro>
    <vNF>16.00</vNF>
    <vTotTrib>0.00</vTotTrib>
    </ICMSTot>
    </total>
    <transp>
    <modFrete>9</modFrete>
    </transp>
    <pag>
    <detPag>
    <indPag>0</indPag>
    <tPag>03</tPag>
    <vPag>'.$financeiro[0]->financeiro_valor.'</vPag>
    <card>
    <tpIntegra>2</tpIntegra>
    </card>
    </detPag>
    </pag>
    <infAdic>
    <infAdFisco>ICMS RECOLHIDO ANTECIPADAMENTE POR SUBSTITUICAO TRIBUTARIA, NOS TERMOS DO ANEXO XV DO RICMS /</infAdFisco>
    <infCpl>Nao recolhimento do diferencial por forca da ADI 5464 /</infCpl>
    </infAdic>
    <infRespTec>
    <CNPJ>30902296000105</CNPJ>
    <xContato>WebmaniaBR Desenvolvimento de Software LTDA</xContato>
    <email>suporte@webmaniabr.com</email>
    <fone>4126264217</fone>
    </infRespTec>
    </infNFe>
    <Signature xmlns="http://www.w3.org/2000/09/xmldsig#">
    <SignedInfo>
    <CanonicalizationMethod Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315"></CanonicalizationMethod>
    <SignatureMethod Algorithm="http://www.w3.org/2000/09/xmldsig#rsa-sha1"></SignatureMethod>
    <Reference URI="#NFe26230738541477000157550010000017831890132679">
    <Transforms>
    <Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature"></Transform>
    <Transform Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315"></Transform>
    </Transforms>
    <DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1"></DigestMethod>
    <DigestValue>XkvM39z6KMeyXyc6acdxMzu0ZF4=</DigestValue>
    </Reference>
    </SignedInfo>
    <SignatureValue>rpwUnIgoyVsFq2shNkJEOQnd23D+yN0WY8nXET9GSFuy3L5Af2PgxJN8FBSTiRRbuEagtKHHBQ9qpdv9dsBwoSqNzA3mFx9RzYkJDznVgpxtdOeO/496fblMuKXd6XNZrQOMCbUC0BU/j0WIRYGHb/lIZsV3MczCHOnjdUxyHr8FdcCNXC3HFh5vUsXyWkOUjfIYDypzXI6jMFBhMBg1DQLYx+/6nxJCgShnUsX66jzWSAbRjQtUIttfZWb4avup7KYmSM+IYCrxeZxDh4fnDsXRsPmIQnTCeCRf8xqXWwwv6GAwM3/Hrf6Dcdt0D0YQa1ez2GLTvyDNQNZ/aw2K+Q==</SignatureValue>
    <KeyInfo>
    <X509Data>
    <X509Certificate>MIIH6TCCBdGgAwIBAgIIS6evjuLvRmswDQYJKoZIhvcNAQELBQAwdjELMAkGA1UEBhMCQlIxEzARBgNVBAoTCklDUC1CcmFzaWwxNjA0BgNVBAsTLVNlY3JldGFyaWEgZGEgUmVjZWl0YSBGZWRlcmFsIGRvIEJyYXNpbCAtIFJGQjEaMBgGA1UEAxMRQUMgU0FGRVdFQiBSRkIgdjUwHhcNMjIwOTIwMTg1ODUwWhcNMjMwOTIwMTg1ODUwWjCB9zELMAkGA1UEBhMCQlIxEzARBgNVBAoTCklDUC1CcmFzaWwxCzAJBgNVBAgTAlBFMQ8wDQYDVQQHEwZSRUNJRkUxNjA0BgNVBAsTLVNlY3JldGFyaWEgZGEgUmVjZWl0YSBGZWRlcmFsIGRvIEJyYXNpbCAtIFJGQjEWMBQGA1UECxMNUkZCIGUtQ05QSiBBMTEXMBUGA1UECxMOMjI2Nzc0MjcwMDAxNjExEzARBgNVBAsTCnByZXNlbmNpYWwxNzA1BgNVBAMTLkFOQSBMVUlaQSBNQVJRVUVTIERPIE5BU0NJTUVOVE86Mzg1NDE0NzcwMDAxNTcwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDJFX+i1S8eWmPAVxoqRU220m30mMN/5RrfZ6N6fRejKWQW2CQGw0FtLutqykZyOD447znK0kqmo8KdFDXc0IsuisIiwqxytvXuzvZA8fB7EQvz+jy1uEQkn0cMoCM2RwaV5vnquUC8GGeA6hU+YVIqaMHuG6xYlUlBgvWGhsJDGPPOqHcjmAufSZVKyq5RP4Q78rgST4uDO+QN1MJvoIJB52NDwFg2wJmJPKcNKBBfG6A9XQEqw01vrxX3HfZqXZNOgaK31McxZu0O4DrM61FH2Tg9XwfzOIyI3PyEM3lWK1ayFTWdXg/r5Ah8uOw+qMGBb6+AkprHOmw6sCG527YdAgMBAAGjggL3MIIC8zAfBgNVHSMEGDAWgBQpXkvVRky7/hanY8EdxCby3djzBTAOBgNVHQ8BAf8EBAMCBeAwbQYDVR0gBGYwZDBiBgZgTAECATMwWDBWBggrBgEFBQcCARZKaHR0cDovL3JlcG9zaXRvcmlvLmFjc2FmZXdlYi5jb20uYnIvYWMtc2FmZXdlYnJmYi9hYy1zYWZld2ViLXJmYi1wYy1hMS5wZGYwga4GA1UdHwSBpjCBozBPoE2gS4ZJaHR0cDovL3JlcG9zaXRvcmlvLmFjc2FmZXdlYi5jb20uYnIvYWMtc2FmZXdlYnJmYi9sY3ItYWMtc2FmZXdlYnJmYnY1LmNybDBQoE6gTIZKaHR0cDovL3JlcG9zaXRvcmlvMi5hY3NhZmV3ZWIuY29tLmJyL2FjLXNhZmV3ZWJyZmIvbGNyLWFjLXNhZmV3ZWJyZmJ2NS5jcmwwgbcGCCsGAQUFBwEBBIGqMIGnMFEGCCsGAQUFBzAChkVodHRwOi8vcmVwb3NpdG9yaW8uYWNzYWZld2ViLmNvbS5ici9hYy1zYWZld2VicmZiL2FjLXNhZmV3ZWJyZmJ2NS5wN2IwUgYIKwYBBQUHMAKGRmh0dHA6Ly9yZXBvc2l0b3JpbzIuYWNzYWZld2ViLmNvbS5ici9hYy1zYWZld2VicmZiL2FjLXNhZmV3ZWJyZmJ2NS5wN2IwgbsGA1UdEQSBszCBsIEUUkVDSUZFRklPU0BHTUFJTC5DT02gKgYFYEwBAwKgIRMfQU5BIExVSVpBIE1BUlFVRVMgRE8gTkFTQ0lNRU5UT6AZBgVgTAEDA6AQEw4zODU0MTQ3NzAwMDE1N6A4BgVgTAEDBKAvEy0xMjEyMjAwMTcxMzc3NjA0NDk3MDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDCgFwYFYEwBAwegDhMMMDAwMDAwMDAwMDAwMB0GA1UdJQQWMBQGCCsGAQUFBwMCBggrBgEFBQcDBDAJBgNVHRMEAjAAMA0GCSqGSIb3DQEBCwUAA4ICAQCXUJf59uWl1BnwCNL/pRAO4yzGbt1WB+2er0ErXl5SO8Stu1he18tLZ/kxLoxdvHrc7L8cDGSXJpL0kqhDvMK4OQOffhNmb5PRmVrUH7d2H49xXzj4Q9Ixkgd+lA/ipACG0bIrBNc3ac4Rd+PSzkDotO+EPS2TtcJInzgPd3DZI4JBqm5BPp9NTLF3Yyi2WV+CUuwFYRR6Dc+0TJ8AkGQxzDlDVVcpVbtNvSlOxFdfnYFLhAt1o46CWsLQYaXh8x0oLx1jI64wc99CQLljxU16hYw8H2t5ymJYmTG5QVathaCxPNk2u6jUtYg0oOz9v8nYJoytO9kLpfu9LxFwk/Omb4qcVIyEyzwszsbrbuUsTZdnyhNp4OHhZnOdL/llWKdkMx1+WDdsI3I1VpoA/tklXWOrLnRy7uMooAPep7LbzCYCl4OeJNy42C7C1LRVRQnPx7ls4sANWTZ7zST+YwdOdjqm8HzrDBmMZrfueXOMxsZGDpEN6nn+hP01p72J/xp5Jfg05PxOwuukdOe7doWvXpGYGv79NPIEcjXUKpzdrxHdufI+C8uHEfokCfEo+p/ok6JFnC/kt2ELyLNYteQDEboY7CC1oCH6VJjiGT/yHV55pHmSZYuvUkVkGH61JmhalUhW0d0jlksof9hHvcX5CtPxRxBdrrgB8GaqXSPAYw==</X509Certificate>
    </X509Data>
    </KeyInfo>
    </Signature>
    </NFe>
    <protNFe versao="4.00">
    <infProt xmlns="http://www.portalfiscal.inf.br/nfe">
    <tpAmb>1</tpAmb>
    <verAplic>NFEPE_P_2.1.0</verAplic>
    <chNFe>26230738541477000157550010000017831890132679</chNFe>
    <dhRecbto>2023-07-06T13:51:42-03:00</dhRecbto>
    <nProt>126230058487597</nProt>
    <digVal>XkvM39z6KMeyXyc6acdxMzu0ZF4=</digVal>
    <cStat>100</cStat>
    <xMotivo>Autorizado o uso da NF-e</xMotivo>
    </infProt>
    </protNFe>
    </nfeProc>';
    return $xml;

}

/**
 * Retorna uma resposta JSON com um código de status especificado.
 *
 * @param array|object $dados Dados para serem enviados como JSON.
 * @param int $status O código de status da resposta HTTP.
 * @return void
 */
function outputJSON($dados, $status)
{
    $CI = &get_instance();
    $CI->output
        ->set_status_header($status)
        ->set_content_type('application/json')
        ->set_output(json_encode($dados));
    $CI->output->_display();
    exit;
}

?>
