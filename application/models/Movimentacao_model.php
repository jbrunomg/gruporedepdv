<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Movimentacao_model extends CI_Model {

	public $tabela  = "movimentacao_produtos";
	public $visivel = "movimentacao_produto_visivel";
	public $chave   = "movimentacao_produto_id";

	// public function listar()
	// {
	// 	$this->db->select("*, ROUND(SUM(`item_movimentacao_produto_custo` * `item_movimentacao_produto_quantidade`), 2) AS total");
	// 	$this->db->join('itens_movimentacao_produtos','`movimentacao_produtos`.`movimentacao_produto_id` = `itens_movimentacao_produtos`.`item_movimentacao_produto_movimentacao_produto_id`');
	// 	$this->db->where($this->visivel, 1);
	// 	$this->db->where('item_movimentacao_produto_visivel', 1);
	// 	$this->db->where('movimentacao_produto_chave_xml', null);
	// 	$this->db->group_by('movimentacao_produto_id');
	// 	$this->db->order_by("movimentacao_produto_id", "desc");


	// 	return $this->db->get($this->tabela)->result();
	// }

	public function listar($limit, $start, $search) 
	{
		$this->db->select('*, ROUND(SUM(`item_movimentacao_produto_custo` * `item_movimentacao_produto_quantidade`), 2) AS total');
		$this->db->from($this->tabela);
		$this->db->join('itens_movimentacao_produtos','`movimentacao_produtos`.`movimentacao_produto_id` = `itens_movimentacao_produtos`.`item_movimentacao_produto_movimentacao_produto_id`');
		$this->db->where('item_movimentacao_produto_visivel', 1);
		$this->db->where('movimentacao_produto_visivel', 1);
		$this->db->where('movimentacao_produto_chave_xml', null);
		if ($search == '-------') { 
			$this->db->where('movimentacao_produto_saida_filial', '');
		}
		
		if (!empty($search) and $search != '-------') {
			$this->db->group_start();
			$this->db->like('movimentacao_produto_id', $search); 
			$this->db->or_like('movimentacao_produto_documento', $search); 
			$this->db->or_like('movimentacao_produto_tipo', $search); 
			$this->db->or_like('movimentacao_produto_saida_filial', $search);
			$this->db->or_like("DATE_FORMAT(movimentacao_produto_data_cadastro, '%d/%m/%Y')", $search);
			$this->db->group_end();
		}
		$this->db->group_by('movimentacao_produto_id');
		$this->db->order_by("movimentacao_produto_id", "desc");
		$this->db->limit($limit, $start);
	
		// Execute a consulta e retorne os resultados
		$resultados = $this->db->get()->result();
	
		$this->db->from($this->tabela);
		$this->db->join('itens_movimentacao_produtos','`movimentacao_produtos`.`movimentacao_produto_id` = `itens_movimentacao_produtos`.`item_movimentacao_produto_movimentacao_produto_id`');
		$this->db->where('item_movimentacao_produto_visivel', 1);
		$this->db->where('movimentacao_produto_visivel', 1);
		$this->db->where('movimentacao_produto_chave_xml', null);
		if ($search == '-------') {
			$this->db->where('movimentacao_produto_saida_filial', '');
		}
		
		if (!empty($search) and $search != '-------') {
			$this->db->group_start();
			$this->db->like('movimentacao_produto_id', $search); 
			$this->db->or_like('movimentacao_produto_documento', $search); 
			$this->db->or_like('movimentacao_produto_tipo', $search); 
			$this->db->or_like('movimentacao_produto_saida_filial', $search);
			$this->db->or_like("DATE_FORMAT(movimentacao_produto_data_cadastro, '%d/%m/%Y')", $search);
			$this->db->group_end();
		}
		$this->db->group_by('movimentacao_produto_id');
		$this->db->order_by("movimentacao_produto_id", "desc");
		$total_registros = $this->db->count_all_results();
		
		return array('dados' => $resultados, 'total_registros' => $total_registros );
	}

	public function listarId($id, $tipoSaida)
	{
		$this->db->select("*");
		if ($tipoSaida == 'fornecedor') {
		  $this->db->join('fornecedores','fornecedores.fornecedor_id = movimentacao_produtos.movimentacao_fornecedor_id');
		}
		$this->db->where($this->chave, $id);
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}

	public function consultaTipo($id)
	{
		$this->db->where($this->chave, $id);
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}



	public function adicionar($dados)
	{
		$this->db->insert($this->tabela, $dados);

		if ($this->db->affected_rows() == '1')
		{
			//return TRUE;
			return $this->db->insert_id($this->tabela);
		}

		return FALSE;
	}

	public function editar($dados, $id)
	{
		$this->db->where($this->chave,$id);

		if($this->db->update($this->tabela,$dados))
		{
			return true;
		}

		return false;
	}

	public function excluir($dados, $id)
	{
		$this->db->where($this->chave,$id);
		$this->db->update($this->tabela,$dados);

		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}

	public function visualizar($id)
	{
		$this->db->select("*");
		$this->db->where($this->chave, $id);
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}

	public function getProdutos($id = null){

        $this->db->select('itens_movimentacao_produtos.*, produto.*');

        $this->db->from('itens_movimentacao_produtos');

        $this->db->join('produto','produto.produto_id = itens_movimentacao_produtos.item_movimentacao_produto_movimentacao_produto_id');

        $this->db->where('item_movimentacao_produto_produto_id',$id);
        $this->db->where('produto.produto_visivel',1);

        return $this->db->get()->result();

    }

    function add($table,$data,$returnId = false){

        $this->db->insert($table, $data);

        if ($this->db->affected_rows() == '1')

		{
            if($returnId == true){

                return $this->db->insert_id($table);
            }
			return TRUE;
		}
		return FALSE;
    }

    function delete($table,$fieldID,$ID){

        $this->db->where($fieldID,$ID);

        $this->db->delete($table);

        if ($this->db->affected_rows() == '1')

		{
			return TRUE;

		}

		return FALSE;

    }


    public function autoCompleteProduto($termo){

        $this->db->select('*');
        $this->db->where('produto.produto_visivel',1);
        $this->db->limit(5);
        $this->db->like('produto_descricao', $termo);
        $query = $this->db->get('produto')->result();

        if(count($query) > 0){
            foreach ($query as $row){
                $row_set[] = array('label'=>$row->produto_descricao.' | Estoque: '.$row->produto_estoque,'id'=>$row->produto_id);
            }
            echo json_encode($row_set);
        }
    }

    public function pegarProdutos($grupo)
	{
		$this->db->select('*');
		$this->db->where('produto_visivel',1);
		$this->db->where('produto_categoria_id',$grupo);
		$this->db->order_by('produto_descricao');
		return $this->db->get('produto')->result();
	}

	public function pegarProdutoId($produto)
	{
		$this->db->select('*, categoria_prod_margem');
		$this->db->join('categoria_produto', 'categoria_prod_id = produto_categoria_id');

		$this->db->where('produto_visivel',1);
		$this->db->where('produto_id',$produto);

		return $this->db->get('produto')->result();
	}

    public function pegarFornecedor()
	{
		$this->db->select('*');
		$this->db->where('fornecedor_visivel',1);
		return $this->db->get('fornecedores')->result();
	}

	public function pegarCategoriaProd($ids)
	{

		$sql = 'SELECT * FROM `categoria_produto` WHERE `categoria_prod_visivel` = 1';

		if ($ids) {
		  $sql .= ' AND `categoria_prod_id` IN('.$ids.')';
		}


        return $this->db->query($sql)->result();
	}

	public function addProduto($dados)
	{

		$sql = "SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE '%_".GRUPOLOJA."_%' ";
        $bases = $this->db->query($sql)->result();

        foreach ($bases as $b) {

            $this->db->insert($b->schema_name.'.'.'produto', $dados);

        }

        if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}

			return FALSE;

	}

	public function addItemMovimentoProduto($dados)
	{
		$this->db->insert('itens_movimentacao_produtos', $dados);

		if ($this->db->affected_rows() == '1')
		{
			return $this->db->insert_id('itens_movimentacao_produtos');
		}

		return FALSE;
	}

	public function verificaMovimentoProduto($idMovimentacao, $idProduto)
	{
		$this->db->select('COUNT(*) as total');
		$this->db->from('itens_movimentacao_produtos');
		$this->db->where('item_movimentacao_produto_movimentacao_produto_id',$idMovimentacao);
		$this->db->where('item_movimentacao_produto_produto_id', $idProduto);
		$this->db->where('item_movimentacao_produto_visivel', 1);
		return $this->db->get()->result();

	}

	public function deleteItemMovimentoProduto($id)
	{
		$this->db->set('item_movimentacao_produto_visivel', 0);
		$this->db->where('item_movimentacao_produto_id', $id);

		$this->db->update('itens_movimentacao_produtos');

		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;

	}

	// Não sabia se a função se a função "getProdutos" era usada em outro lugar então criei essa.
	public function getProdutosList($idMovimentacao){

        $this->db->select(
        	'itens_movimentacao_produtos.item_movimentacao_produto_id AS itemMovimentacaoId,
        	 itens_movimentacao_produtos.`item_movimentacao_produto_produto_id` AS idProduto,
        	 produto.`produto_codigo` AS codigoProduto,
        	 produto.`produto_descricao` AS descricaoProduto,
        	 produto.`produto_preco_custo` AS precocusto,
        	 itens_movimentacao_produtos.`item_movimentacao_produto_quantidade` AS quantidadeProduto,
        	 itens_movimentacao_produtos.`item_movimentacao_produto_dolar` AS dolarProduto,
        	 itens_movimentacao_produtos.`item_movimentacao_produto_custo` AS custoProduto,
        	 itens_movimentacao_produtos.`item_movimentacao_produto_minimo` AS minimoProduto,
        	 itens_movimentacao_produtos.`item_movimentacao_produto_preco_unitario` AS vendaProduto,
        	 produto.produto_estoque AS Estoque '
        );

        $this->db->from('itens_movimentacao_produtos');

        $this->db->join('produto','produto.produto_id = itens_movimentacao_produtos.item_movimentacao_produto_produto_id');

        $this->db->join('movimentacao_produtos',
        	'movimentacao_produtos.movimentacao_produto_id = itens_movimentacao_produtos.item_movimentacao_produto_movimentacao_produto_id');

        $this->db->where('item_movimentacao_produto_movimentacao_produto_id',$idMovimentacao);
        $this->db->where('item_movimentacao_produto_visivel', 1);
        $this->db->where('movimentacao_produtos.movimentacao_produto_visivel', 1);
        $this->db->order_by('produto.`produto_descricao`');

        return $this->db->get()->result();

    }



    public function updateProdutoEntrada($produto)
    {

    	// echo "<pre>"; var_dump($produto); exit();
    	if ($produto->dolarProduto != '0.00') {
    	   $this->db->set('produto_preco_dolar', $produto->dolarProduto);
    	}

    	if ($produto->custoProduto!= '0.00') {
    	   $this->db->set('produto_preco_custo', $produto->custoProduto);
    	}

    	if ($produto->minimoProduto!= '0.00') {
        	$this->db->set('produto_preco_minimo_venda', $produto->minimoProduto);
    	}

    	if ( $produto->vendaProduto!= '0.00') {
    		$this->db->set('produto_preco_venda', $produto->vendaProduto);
    	}

    	$this->db->set('produto_estoque', ($produto->Estoque + $produto->quantidadeProduto));
		$this->db->where('produto_id', $produto->idProduto);
		$this->db->update('produto');

		if($this->db->affected_rows() == '1')
		{
			return true;

		}

		return false;

    }

    public function updateProdutoSaida($produto)
    {

    	// $this->db->set('produto_preco_dolar', $produto->dolarProduto);
    	// $this->db->set('produto_preco_custo', $produto->custoProduto);
    	// $this->db->set('produto_preco_minimo_venda', $produto->minimoProduto);
    	// $this->db->set('produto_preco_venda', $produto->vendaProduto);
    	$this->db->set('produto_estoque', ($produto->Estoque - $produto->quantidadeProduto));

		$this->db->where('produto_id', $produto->idProduto);

		$this->db->update('produto');

		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
    }

    // Antigo
  //   public function updateStatusMovimentacao($idMovimentacao)
  //   {
  //   	$this->db->set('movimentacao_produto_status', 0);
  //   	$this->db->where('movimentacao_produto_id', $idMovimentacao);
		// $this->db->update('movimentacao_produtos');

		// if($this->db->affected_rows() == '1')
		// {
		// 	return true;
		// }

		// return false;
  //   }

    public function updateStatusMovimentacao($idMovimentacao)
    {
        $this->db->select("*");
		$this->db->where($this->chave, $idMovimentacao);
		$this->db->where($this->visivel, 1);
		$result = $this->db->get($this->tabela)->result();

		$this->db->select("*");
		// $this->db->where('item_movimentacao_produto_produto_id', $result[0]->movimentacao_produto_id);
		$this->db->where('item_movimentacao_produto_movimentacao_produto_id', $result[0]->movimentacao_produto_id);
		$this->db->where('item_movimentacao_produto_visivel', 1);
		$resulItens = $this->db->get('itens_movimentacao_produtos')->result();

        if($result[0]->movimentacao_produto_tipo == 'Saída' and $result[0]->movimentacao_produto_saida_tipo == 'filial'){

        	$this->db->set('movimentacao_produto_status', 0);
			$this->db->where('movimentacao_produto_id', $idMovimentacao);
			$this->db->update('movimentacao_produtos');

        	$loja = explode('_', BDCAMINHO);
        	$lojadestino = $loja[0].'_'.$loja[1].'_'.$result[0]->movimentacao_produto_saida_filial;

			$dados = array(
        		'movimentacao_produto_documento'       => $result[0]->movimentacao_produto_documento.' </br> Emitido: '.$loja[2],
				'movimentacao_produto_tipo'            => 'Entrada',
                'movimentacao_produto_saida_tipo'      => $result[0]->movimentacao_produto_saida_tipo,
                'movimentacao_produto_saida_filial'    => $result[0]->movimentacao_produto_saida_filial,
                'movimentacao_fornecedor_id'           => $result[0]->movimentacao_fornecedor_id,
				'movimentacao_produto_usuario_id'      => $result[0]->movimentacao_produto_usuario_id,
				'movimentacao_produto_data_cadastro'   => date('Y-m-d'),
                'movimentacao_tipo_moeda'              => $result[0]->movimentacao_tipo_moeda,
                'movimentacao_cotacao_moeda'           => $result[0]->movimentacao_cotacao_moeda,
                'movimentacao_produto_porcentagem'     => $result[0]->movimentacao_produto_porcentagem,
                'movimentacao_saida_origem_nome'       => $loja[2],
				'movimentacao_produto_imei'            => $result[0]->movimentacao_produto_imei,
                'movimentacao_saida_origem_id'         => $result[0]->movimentacao_produto_id,
				'movimentacao_produto_visivel'         => 1
        	);

			$this->db->insert($lojadestino.'.'.'movimentacao_produtos', $dados);

			if ($this->db->affected_rows() == '1')
			{
				$novoID = $this->db->insert_id('movimentacao_produtos');
			}

			//var_dump($resulItens);die();
			foreach ($resulItens as $itens) {

			    $dados = array(
		        	'item_movimentacao_produto_quantidade'        => $itens->item_movimentacao_produto_quantidade,
		            'item_movimentacao_produto_preco_unitario'    => $itens->item_movimentacao_produto_preco_unitario,
		            'item_movimentacao_produto_custo'             => $itens->item_movimentacao_produto_custo,
		            'item_movimentacao_produto_minimo'            => $itens->item_movimentacao_produto_minimo,
		            'item_movimentacao_produto_produto_id'        => $itens->item_movimentacao_produto_produto_id,
		            'item_movimentacao_produto_dolar'             => $itens->item_movimentacao_produto_dolar,
		            'item_movimentacao_produto_movimentacao_produto_id'     => $novoID
		        );

			    $this->db->insert($lojadestino.'.'.'itens_movimentacao_produtos', $dados);

				}

		        if ($this->db->affected_rows() == '1')

				{
					return TRUE;

				}

				return FALSE;




        }else{

	        $this->db->set('movimentacao_produto_status', 0);
	    	$this->db->where('movimentacao_produto_id', $idMovimentacao);

			$this->db->update('movimentacao_produtos');

			if($this->db->affected_rows() == '1')
			{
				return true;
			}

			return false;
        }



    }

    public function getFilias()
    {
    	$sql = 'SELECT schema_name AS Nome FROM information_schema.schemata WHERE schema_name LIKE "%_'.GRUPOLOJA.'_%" and schema_name <> "'.BDCAMINHO.'"';

        $result = $this->db->query($sql)->result();

        return $result;

    }


    public function mercadoriaLiberada()
	{
		$this->db->select("*, ROUND(SUM(`item_movimentacao_produto_custo` * `item_movimentacao_produto_quantidade`), 2) AS total");
		$this->db->join('itens_movimentacao_produtos','`movimentacao_produtos`.`movimentacao_produto_id` = `itens_movimentacao_produtos`.`item_movimentacao_produto_movimentacao_produto_id`');
		$this->db->where($this->visivel, 1);
		$this->db->where('movimentacao_produto_tipo', 'Entrada');
		$this->db->where('movimentacao_produto_status', 1);
		$this->db->group_by('movimentacao_produto_id');

		return $this->db->get($this->tabela)->result();
	}



	public function EstornoMovimentacao($idMovimentacao)
    {
        $this->db->select("*");
        $this->db->join('itens_movimentacao_produtos','`movimentacao_produtos`.`movimentacao_produto_id` = `itens_movimentacao_produtos`.`item_movimentacao_produto_movimentacao_produto_id`');
		$this->db->where('item_movimentacao_produto_movimentacao_produto_id', $idMovimentacao);
		$resulItens = $this->db->get($this->tabela)->result();


		// Da update na tabela de produtos da (Origem) Ex: Matriz

		$loja = explode('_', BDCAMINHO);
        $lojadestino = $loja[0].'_'.$loja[1].'_'.$resulItens[0]->movimentacao_saida_origem_nome;


		foreach ($resulItens as $itens) {

		$sql =  " UPDATE {$lojadestino}.`produto` SET `produto_estoque` = `produto_estoque` + {$itens->item_movimentacao_produto_quantidade} WHERE `produto_id` = {$itens->item_movimentacao_produto_produto_id}; ";
        $bases =  $this->db->query($sql);

		}


        if ($bases){

        	// Liberar lote na Origem
			$dados = array(
	            'movimentacao_produto_status'  => 1
	        );
	        $this->db->where('movimentacao_produto_id', $itens->movimentacao_saida_origem_id);
		    $this->db->update($lojadestino.'.'.'movimentacao_produtos', $dados);

		    // Exluir Lote no Destino

		    $dados = array(
	            '`movimentacao_produto_documento`'  => $itens->movimentacao_produto_documento.'</br> <del>Mercadoria Rejeitada</del> ' ,
	            '`movimentacao_produto_status`'    => 0
	        );
	        $this->db->where('movimentacao_produto_id', $itens->movimentacao_produto_id);
		    $this->db->update('movimentacao_produtos', $dados);

			return TRUE;

		}

		return FALSE;


    }


	public function fornecedorBaixarListar()
	{
		$this->db->select("*");
		$this->db->join('usuarios', 'usuarios.usuario_id = baixar_compra.id_usuario', 'left');
		$this->db->join('fornecedores', 'fornecedores.fornecedor_id = baixar_compra.id_fornecedor');
		$this->db->order_by('id_baixa', 'desc');
		return $this->db->get('baixar_compra')->result();
	}

	public function fornecedorPendentes()
	{

		$this->db->select('fornecedores.fornecedor_id, fornecedores.fornecedor_nome AS fornecedor,
		 ROUND(SUM(`item_movimentacao_produto_custo` * `item_movimentacao_produto_quantidade`), 2) AS valor_total_produtos,	
		 /* SUM(itens_movimentacao_produtos.item_movimentacao_produto_custo) AS valor_total_produtos, */
		 COALESCE(baixar_compra_agg.valor_pago, 0) AS valor_pago');
		$this->db->from('movimentacao_produtos');
		$this->db->join('itens_movimentacao_produtos', 'movimentacao_produtos.movimentacao_produto_id = itens_movimentacao_produtos.item_movimentacao_produto_movimentacao_produto_id');
		$this->db->join('fornecedores', 'movimentacao_produtos.movimentacao_fornecedor_id = fornecedores.fornecedor_id');
		$this->db->join('(SELECT id_fornecedor, SUM(valor_baixa) AS valor_pago FROM baixar_compra GROUP BY id_fornecedor) baixar_compra_agg', 
    	'movimentacao_produtos.movimentacao_fornecedor_id = baixar_compra_agg.id_fornecedor', 'left');
		$this->db->where('movimentacao_produto_tipo', 'Entrada');
		$this->db->where('movimentacao_produto_visivel', 1);
		$this->db->group_by(['fornecedores.fornecedor_id', 'fornecedores.fornecedor_nome']);



		
		// $this->db->select('fornecedores.fornecedor_id, fornecedores.fornecedor_nome AS fornecedor, SUM(itens_movimentacao_produtos.item_movimentacao_produto_custo) AS valor_total_produtos');
		// $this->db->from('movimentacao_produtos');
		// $this->db->join('itens_movimentacao_produtos', 'movimentacao_produtos.movimentacao_produto_id = itens_movimentacao_produtos.item_movimentacao_produto_movimentacao_produto_id');
		// $this->db->join('fornecedores', 'movimentacao_produtos.movimentacao_fornecedor_id = fornecedores.fornecedor_id');
		// $this->db->where('movimentacao_produto_tipo', 'Entrada');
		// $this->db->where('movimentacao_produto_visivel', 1);
		// $this->db->group_by('fornecedores.fornecedor_id, fornecedores.fornecedor_nome');
	
		$query = $this->db->get();
		return $query->result();
	}
	
	public function fornecedorBaixarAdd($dados)
	{
		// die($dados);
		$this->db->insert('baixar_compra', $dados);

		if ($this->db->affected_rows() == '1') {
			return TRUE;
		}
	}





}
