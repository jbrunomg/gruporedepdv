<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PDV_model extends CI_Model
{

  public $tabela  = "vendas";
  public $visivel = "vendas_visivel";
  public $chave   = "idVendas";


  public function selecionarCliente()
  {
    $this->db->select('*');
    $this->db->where('cliente_visivel', 1);
    return $this->db->get('clientes')->result();
  }

  public function autoCompleteClientes($termo)
  {
    $this->db->select('*');
    $this->db->limit(5);
    $this->db->where('cliente_visivel', 1);
    $this->db->like('cliente_nome', $termo);
    $this->db->or_like('cliente_cpf_cnpj', $termo);
    $this->db->or_like('cliente_celular', $termo);

    $query = $this->db->get('clientes')->result();

    if (count($query) > 0) {
      foreach ($query as $row) {
        $row_set[] = array('label' => $row->cliente_nome, 'id' => $row->cliente_id);
      }
      echo json_encode($row_set);
    }
  }

  //  public function addCliente($dados)
  // {
  // 	$this->db->insert('clientes', $dados);

  // 	if ($this->db->affected_rows() == '1')
  // 	{
  // 		return TRUE;
  // 	}

  // 	return FALSE; 
  // }

  public function addCliente($dados)
  {
    $sql = "SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE '%_" . GRUPOLOJA . "_%' ";
    $bases = $this->db->query($sql)->result();

    foreach ($bases as $b) {
      $this->db->insert($b->schema_name . '.' . 'clientes', $dados);
    }

    if ($this->db->affected_rows() == '1') {
      return TRUE;
    }

    return FALSE;
  }


  public function verificarCliente($dados)
  {
    $this->db->select('*');
    $this->db->where($dados);
    if ($this->db->get('clientes')->num_rows() > 0) {
      return TRUE;
    } else {
      return FALSE;
    }
  }

  public function getProductByCode($code, $loja)
  {
    // $q = $this->db->get_where('produto', array('produto_codigo' => $code), 1);

    $this->db->select(' CONCAT("' . $loja . '_"' . ',produto_id) AS produto_id, produto_codigo, produto_descricao, produto_estoque, produto_preco_venda,
        produto_preco_minimo_venda ,produto_preco_cart_debito, produto_preco_cart_credito  ');
    $this->db->where('produto_codigo', $code);
    $q = $this->db->get('' . $loja . '.produto');

    if ($q->num_rows() > 0) {
      return $q->row();
    }
    return FALSE;
  }

  //  public function listarProdutos()
  // {
  // 	$this->db->select("*");				
  // 	$this->db->where('produto_visivel', 1);

  // 	return $this->db->get('produto')->result();
  // }

  public function listarProdutos($category_id = NULL, $limit, $start, $loja)
  {
    $this->db->select(' CONCAT("' . $loja . '_"' . ',produto_id) AS produto_id, produto_codigo, produto_descricao, produto_estoque, produto_preco_venda, produto_preco_minimo_venda ,produto_preco_cart_debito, produto_preco_cart_credito  ');
    $this->db->limit($limit, $start);
    $this->db->where('produto_categoria_id', $category_id);
    $this->db->where('produto_visivel', 1);
    $this->db->order_by("produto_codigo", "asc");
    $query = $this->db->get('' . $loja . '.produto');

    if ($query->num_rows() > 0) {
      foreach ($query->result() as $row) {
        $data[] = $row;
      }
      return $data;
    }
    return false;
  }

  //  public function listarProdutosGrupo($grupo)
  // {
  // 	$this->db->select("*");				
  // 	$this->db->where('produto_visivel', 1);
  // 	$this->db->where('produto_categoria_id', $grupo);

  // 	return $this->db->get('produto')->result();
  // }

  public function listarProdutosGrupo($category_id = NULL, $loja)
  {
    if ($category_id) {
      $this->db->select(' CONCAT("' . $loja . '_"' . ',produto_id) AS produto_id, produto_codigo, produto_descricao, produto_estoque, produto_preco_venda, produto_preco_minimo_venda ,produto_preco_cart_debito, produto_preco_cart_credito  ');
      $this->db->where('produto_categoria_id', $category_id);
      $this->db->where('produto_visivel', 1);
      return $this->db->count_all_results('' . $loja . '.produto');
    } else {
      return $this->db->count_all("produto");
    }
  }


  //  public function categorias()
  // {
  // 	$this->db->select("*");		
  // 	$this->db->where('categoria_prod_visivel', 1);
  // 	return $this->db->get('categoria_produto')->result();
  // }

  public function getAllFilias()
  {

    $sql = " SELECT schema_name, '' as cor FROM information_schema.schemata WHERE schema_name LIKE '%_" . GRUPOLOJA . "_%' ";
    $bases = $this->db->query($sql)->result();


    foreach ($bases as $b) {

      $this->db->select('emitente_cor_empresa');

      $result = $this->db->get($b->schema_name . '.' . 'emitente')->result();

      $b->cor = $result[0]->emitente_cor_empresa;
    }
    return $bases;
  }


  public function getAllCategorias($lojas)
  {
    $this->db->order_by('categoria_prod_id');
    $this->db->where('categoria_prod_visivel', 1);
    $q = $this->db->get('' . $lojas . '.categoria_produto');
    if ($q->num_rows() > 0) {
      foreach (($q->result()) as $row) {
        $data[] = $row;
      }
      return $data;
    }
    return FALSE;
  }

  public function getProductNames($term, $limit = 10)
  {
    $this->db->where("(produto_descricao LIKE '%" . $term . "%' OR produto_codigo LIKE '%" . $term . "%' OR  concat(produto_descricao, ' (', produto_codigo, ')') LIKE '%" . $term . "%')");
    $this->db->limit($limit);
    $this->db->where('produto_visivel', 1);
    $q = $this->db->get('produto');
    if ($q->num_rows() > 0) {
      foreach (($q->result()) as $row) {
        $data[] = $row;
      }
      return $data;
    }
    return FALSE;
  }

  public function getProductByID($id)
  {
    $q = $this->db->get_where('produto', array('produto_id' => $id), 1);
    if ($q->num_rows() > 0) {
      return $q->row();
    }
    return FALSE;
  }


  public function addSale($data, $items)
  {

    $lojaAtual = BDCAMINHO;
    $lojaAtual = explode("_", $lojaAtual);
    $lojaAtual = $lojaAtual[2];

    if ($this->db->insert('vendas', $data)) {
      $sale_id = $this->db->insert_id();

      foreach ($items as $item) {
        $item['vendas_id'] = $sale_id;

        var_dump($item);
        if ($this->db->insert('itens_de_vendas', $item)) {
          $product = $this->getProductByID($item['produtos_id']);
        }
      }

      $this->db->select('quantidade, produtos_id, filial');
      $this->db->from('itens_de_vendas');
      $this->db->where('vendas_id', $sale_id);

      foreach ($this->db->get()->result() as $row) {
        if ($row->filial == $lojaAtual) {
          $sqlAtualizar = "UPDATE produto set produto_estoque = produto_estoque - ? WHERE produto_id = ?";
          $this->db->query($sqlAtualizar, array($row->quantidade, $row->produtos_id));
        }
      }

      return array('sale_id' => $sale_id, 'message' => $msg);
    }

    return false;
  }

  public function emitente()
  {
    $this->db->select("*");

    return $this->db->get('emitente')->result();
  }

  // ================ PDV MODELO 2 - INICIO =====================================

  public function pegarProdutos($grupo, $filial)
  {

    $this->db->select('*');

    if (BDCAMINHO == $filial) {

      //die('AQUI');

      //  and LOWER(imei_filial) <> "'.SUBDOMINIO.'"

      $this->db->join('' . $filial . '.itens_de_imei', '`produto`.`produto_id` = itens_de_imei.produtos_id and imei_visivel = 1 ', 'LEFT');

      //  $this->db->join(''.$filial .'.itens_de_imei','`produto`.`produto_id` = itens_de_imei.produtos_id and imei_visivel = 1 and LOWER(imei_filial) <> "cdd" ','LEFT'); 

    } else {

      //die('AQUI2');
      $this->db->join('' . $filial . '.itens_de_imei', '`produto`.`produto_id` = itens_de_imei.produtos_id and imei_visivel = 1 and imei_filial IS NULL', 'LEFT');
    }

    $this->db->where('produto_visivel', 1);
    $this->db->where('produto_estoque >', 0); // tentativa de melhor a velocidade da busca
    $this->db->where('produto_categoria_id', $grupo);
    $this->db->where('vendas_id is null');
    $this->db->order_by('produto_descricao');
    return $this->db->get('' . $filial . '.produto')->result();
  }

  public function pegarProdutosId($produto, $filial)
  {
    $this->db->select('*');
    $this->db->where('produto_visivel', 1);
    $this->db->where('produto_id', $produto);
    return $this->db->get('' . $filial . '.produto')->result();
  }

  public function adicionarVenda($dados)
  {
    $this->db->insert($this->tabela, $dados);

    if ($this->db->affected_rows() == '1') {
      return $this->db->insert_id($this->tabela);
    }

    return FALSE;
  }


  public function getVenda($id)
  {
    $this->db->select('*');
    $this->db->where('venda_visivel', 1);
    $this->db->where('idVendas', $id);
    $this->db->join('clientes ', '`vendas`.`clientes_id` = `clientes`.`cliente_id` ');
    return $this->db->get('vendas')->result();
  }


  public function additensVendas($tabela, $dados)
  {
    try {
      $this->db->trans_begin();
      $this->db->insert($tabela, $dados);

      if ($this->db->affected_rows() >= '1' && $this->db->trans_status() === TRUE) {
        $this->db->trans_commit();
        return true;
      }

      $this->db->trans_rollback();
      return FALSE;
    } catch (Exception $e) {
      $this->db->trans_commit();
      return true;
    }
  }

  public function validacao($produto, $quantidade)
  {
    try {
      $this->db->trans_begin();
      $sqlConsultar = "SELECT IF(`produto_estoque` >= " . $quantidade . ", 'Y','N') AS quantidade FROM `produto` WHERE `produto_id` = " . $produto . " ";

      $resultado = $this->db->query($sqlConsultar)->result();
      if ($resultado[0]->quantidade == 'N' || $this->db->trans_status() === FALSE) {
        $this->db->trans_rollback();
        return false;
        // break;  // PHP7 Descontinuado
      }

      $this->db->trans_commit();
      return true;
    } catch (Exception $e) {
      $this->db->trans_rollback();
      return false;
    }
  }

  public function estoqueProduto($produto, $quantidade)
  {
    try {
      $this->db->trans_begin();
      $sqlAtualizar = "UPDATE produto set produto_estoque = produto_estoque - ?, produto_data_ultima_venda = CURDATE() WHERE produto_id = ?";
      $this->db->query($sqlAtualizar, array($quantidade, $produto));

      if ($this->db->affected_rows() >= '1' && $this->db->trans_status() === TRUE) {
        $this->db->trans_commit();
        return TRUE;
      }

      $this->db->trans_rollback();
      return FALSE;
    } catch (Exception $e) {
      $this->db->trans_rollback();
      return FALSE;
    }
  }

  public function imeiVenda($imei, $venda)
  {
    try {
      $this->db->trans_begin();
      $sqlAtualizar = "UPDATE itens_de_imei set vendas_id = '" . $venda . "' WHERE imei_valor = '" . $imei . "'";
      $this->db->query($sqlAtualizar);

      if ($this->db->affected_rows() >= '1' && $this->db->trans_status() === TRUE) {
        $this->db->trans_commit();
        return TRUE;
      }

      $this->db->trans_rollback();
      return FALSE;
    } catch (Exception $e) {
      $this->db->trans_rollback();
      return FALSE;
    }
  }

  public function imeiVendaFilial($imei, $filial)
  {
    try {
      $this->db->trans_begin();
      $baseAtual = BDCAMINHO;
      $baseAtual = explode("_", $baseAtual);
      $baseAtual = strtoupper($baseAtual[2]);
  
      $sql = "SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE '%_" . GRUPOLOJA . "_" . $filial . "%' ";
      $base = $this->db->query($sql)->result();
      $base = $base[0]->schema_name;
  
      $sqlAtualizar = "UPDATE $base.itens_de_imei set imei_filial = '" . $baseAtual . "' WHERE imei_valor = '" . $imei . "'";
      $this->db->query($sqlAtualizar);
  
      if ($this->db->affected_rows() >= '1' && $this->db->trans_status() === TRUE) {
        $this->db->trans_commit();
        return TRUE;
      }

      $this->db->trans_rollback();
      return FALSE;
    } catch (Exception $e) {
      $this->db->trans_rollback();
      return FALSE;
    }
  }

  public function imeiRetirada($imei)
  {
    try {
      $this->db->trans_begin();
      $sqlAtualizar = "UPDATE itens_de_imei set vendas_id = NULL WHERE imei_valor = '" . $imei . "'";
      $this->db->query($sqlAtualizar);
  
      if ($this->db->affected_rows() >= '1' && $this->db->trans_status() === TRUE) {
        $this->db->trans_commit();
        return TRUE;
      }
      $this->db->trans_rollback();
      return FALSE;
    } catch (Exception $e) {
      $this->db->trans_rollback();
      return FALSE;
    }
  }

  public function imeiRetiradaFilial($imei, $filial)
  {
    try {
      $this->db->trans_begin();
      $base = BDCAMINHO;
      $base = explode("_", $base);
      $base = $base[0];

      $base = $base . '_' . GRUPOLOJA . '_' . strtolower($filial);
      // $sql = "SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE '%_".GRUPOLOJA."_".$filial."%' ";
      // $base = $this->db->query($sql)->result();
      // $base = $base[0]->schema_name;
      $sqlAtualizar = "UPDATE  $base.itens_de_imei set imei_filial = NULL WHERE imei_valor = '" . $imei . "'";
      $this->db->query($sqlAtualizar);

      if ($this->db->affected_rows() >= '1' && $this->db->trans_status() === TRUE) {
        $this->db->trans_commit();
        return TRUE;
      }
      $this->db->trans_rollback();
      return FALSE;
    } catch (Exception $e) {
      $this->db->trans_rollback();
      return FALSE;
    }
  }


  public function getVendaProdutos($id = null)
  {

    $this->db->select('`itens_de_vendas`.`idItens`,
                             `categoria_produto`.`categoria_prod_descricao`,
                             `produto`.`produto_codigo`,
                             `produto`.`produto_descricao`,
                             `itens_de_vendas`.`quantidade`,
                             `itens_de_vendas`.`filial`,
                             `itens_de_vendas`.`imei_valor`,
                             `itens_de_vendas`.`prod_preco_minimo_venda`,
                             CONCAT(`itens_de_vendas`.`subTotal` / `itens_de_vendas`.`quantidade`) AS valor_unit,
                             `itens_de_vendas`.`subTotal`');

    $this->db->from('itens_de_vendas');

    $this->db->join('vendas', '`itens_de_vendas`.`vendas_id` = vendas.`idVendas`');
    $this->db->join('produto', '`itens_de_vendas`.`produtos_id` = produto.`produto_id`');
    $this->db->join('categoria_produto', '`produto`.`produto_categoria_id` = categoria_produto.`categoria_prod_id`');

    $this->db->where('`itens_de_vendas`.vendas_id', $id);

    return $this->db->get()->result_array();
  }


  // public function excluirItens($id){

  //     $sql = "DELETE FROM itens_de_vendas WHERE 
  //     idItens IN (SELECT * FROM (SELECT MAX(b.idItens) FROM `itens_de_vendas`b WHERE b.`vendas_id` = ". $id .") AS T)";
  //     $this->db->query($sql);

  //     if ($this->db->affected_rows() == '1'){
  //       return TRUE;
  //     }   
  //     return FALSE;      

  //   }

  public function consultarEstoque($idVenda, $idItens)
  {

    $this->db->select('`itens_de_vendas`.`quantidade`, `itens_de_vendas`.`filial`, `itens_de_vendas`.`produtos_id`');
    $this->db->where('vendas_id', $idVenda);
    $this->db->where('idItens', $idItens);
    $this->db->from('itens_de_vendas');
    return $this->db->get()->result_array();
  }

  public function excluirItensPDV2($idVenda, $idItens)
  {
    try {
      $this->db->trans_begin();
      $this->db->where('vendas_id', $idVenda);
      $this->db->where('idItens', $idItens);

      $this->db->delete('itens_de_vendas');

      if ($this->db->affected_rows() >= '1' && $this->db->trans_status() === TRUE) {
        $this->db->trans_commit();
        return TRUE;
      }

      $this->db->trans_rollback();
      return FALSE;
    } catch (Exception $e) {
      $this->db->trans_rollback();
      return FALSE;
    }
  }

  public function consultaItensExcluir($idVenda)
  {

    $this->db->where('vendas_id', $idVenda);
    $this->db->from('itens_de_vendas');
    return $this->db->get()->result_array();
  }

  public function estoqueAdd($quantidade, $produto)
  {
    try {
      $this->db->trans_begin();
      $query =  "UPDATE produto set produto_estoque = produto_estoque + ? WHERE produto_id = ?";
      $this->db->query($query, array($quantidade, $produto));

      if ($this->db->affected_rows() >= '1' && $this->db->trans_status() === TRUE) {
        $this->db->trans_commit();
        return TRUE;
      }
      $this->db->trans_rollback();
      return FALSE;
    } catch (Exception $e) {
      $this->db->trans_rollback();
      return FALSE;
    }
  }

  // public function cancelarVendaPDV2($idVenda){

  //     $this->db->where('vendas_id',$idVenda);

  //     $this->db->delete('itens_de_vendas');

  //     // if ($this->db->affected_rows() == '1')

  //     // {
  //       return TRUE;
  //     // }   

  //     // return FALSE;    

  // }

  public function autoCompleteUsuarios($termo)
  {
    $this->db->select('*');
    $this->db->limit(5);
    $this->db->like('usuario_nome', $termo);
    $this->db->where('usuario_visivel', 1);
    $query = $this->db->get('usuarios')->result();

    if (count($query) > 0) {
      foreach ($query as $row) {
        $row_set[] = array('label' => $row->usuario_nome, 'id' => $row->usuario_id);
      }
      echo json_encode($row_set);
    }
  }

  public function finalizarVenda($dados, $id)
  {

    $this->db->where('idVendas', $id);


    if ($this->db->update('vendas', $dados)) {
      return true;
    }

    return false;
  }

  public function financeiro($dados)
  {
    $this->db->insert('financeiro', $dados);

    if ($this->db->affected_rows() == '1') {
      return true;
    }

    return FALSE;
  }


  public function financeiroUpdate($dados, $id)
  {

    $this->db->where('idFinanceiro', $id);


    if ($this->db->update('financeiro', $dados)) {
      return true;
    }

    return false;
  }


  public function consultarFinanceiro($id)
  {
    $this->db->select('SUM(`subTotal`) AS total, filial, idFinanceiro');
    $this->db->join('financeiro', ' financeiro.`vendas_id` = `itens_de_vendas`.`vendas_id`', 'left');
    $this->db->where('itens_de_vendas.vendas_id', $id);
    $this->db->group_by('filial, idFinanceiro');
    return $this->db->get('itens_de_vendas')->result();
  }


  public function verificarFinanceiro($id)
  {
    $this->db->select('idFinanceiro');
    $this->db->where('vendas_id', $id);
    return $this->db->get('financeiro')->result();
  }


  public function excluirVenda($idVenda)
  {

    $result =  $this->consultaItensExcluir($idVenda);  // pegar apenas itens da propria loja 

    foreach ($result as $r) {
      if ($r['filial'] == SUBDOMINIO) {
        $this->estoqueAdd($r['quantidade'], $r['produtos_id']);
        $this->trocaImei('itens_de_imei', array('vendas_id' => null), 'imei_valor', $r['imei_valor']);
      } else {
        $this->imeiRetiradaFilial($r['imei_valor'], $r['filial']);
      }
    }

    $this->db->set('venda_visivel', 0);
    $this->db->where('idVendas', $idVenda);
    $this->db->update('vendas');

    if ($this->db->trans_status() === true) {
      $this->db->trans_commit();
      return true;
    } else {
      $this->db->trans_rollback();
      return false;
    }
  }

  public function trocaImei($tabela, $dados, $chave = null, $id = null, $where = null)
  {
    if ($where) {
      $this->db->where($where);
    } else {
      $this->db->where($chave, $id);
    }


    if ($this->db->update($tabela, $dados)) {
      return true;
    }

    return false;
  }

  public function validacaoGerente($senha)
  {

    $this->db->select('IF("' . $senha . '" = `usuarios`.`usuario_senha`,"success","danger")AS validacao');
    $this->db->where_in('usuario_perfil', [1, 2]);
    $result = $this->db->get('usuarios')->result();

    foreach ($result as $key) {
      if ($key->validacao == 'success') {

        return true;
        // break;  // PHP7 Descontinuado            
      }
    }

    return false;
  }


  public function getAllHistorico($usuario)
  {
    $data = date('Y-m-d');
    $loja = BDCAMINHO;
    $loja = explode("_", $loja);

    $query =  "SELECT ROUND(SUM(`itens_de_vendas`.`subTotal`),2) AS total , financeiro.`financeiro_forma_pgto`AS forma_pgto
                FROM vendas 
                LEFT JOIN `financeiro`
                ON `vendas`.`idVendas` = `financeiro`.`vendas_id`
                LEFT JOIN `itens_de_vendas`
                ON `vendas`.`idVendas` = `itens_de_vendas`.`vendas_id`
                WHERE vendas.`usuarios_id` = $usuario AND dataVenda = '$data' AND `venda_visivel` = 1
                AND `financeiro`.`financeiro_descricao` LIKE '%$loja[2]%' ESCAPE '!'
                AND `itens_de_vendas`.`filial` LIKE '%$loja[2]%' ESCAPE '!'
                GROUP BY financeiro.`financeiro_forma_pgto`";
    // echo "<pre>";
    // exit($query);
    return $this->db->query($query)->result_array();
  }

  public function getAllVendedores()
  {
    $ano = date('Y');
    $mes = date('m');
    $loja = BDCAMINHO;
    $loja = explode("_", $loja);

    $query =  "SELECT ROUND(SUM(`itens_de_vendas`.`subTotal`),2) AS total, `usuarios`.`usuario_nome` AS vendedor
                FROM vendas 
                LEFT JOIN `usuarios`
                ON vendas.`usuarios_id` = `usuarios`.`usuario_id`
                LEFT JOIN `itens_de_vendas`
                ON `vendas`.`idVendas` = `itens_de_vendas`.`vendas_id`
               /* LEFT JOIN `financeiro`
                ON `vendas`.`idVendas` = `financeiro`.`vendas_id` */
                WHERE MONTH(dataVenda) =  '$mes' AND YEAR(dataVenda) = '$ano' AND `venda_visivel` = 1
                AND `itens_de_vendas`.`filial` LIKE '%$loja[2]%' ESCAPE '!'
               /* AND `financeiro`.`financeiro_descricao` LIKE '%$loja[2]%' ESCAPE '!' */
                GROUP BY vendas.`usuarios_id`
                ORDER BY total DESC
                LIMIT 3";
    // echo "<pre>";
    // exit($query);
    return $this->db->query($query)->result_array();
  }



  public function addRetirada($tabela, $dados)
  {
    $this->db->insert($tabela, $dados);

    if ($this->db->affected_rows() == '1') {
      return true;
    }

    return FALSE;
  }

  public function consultaCreditoCliente($idCliente)
  {
    $this->db->select("SUM(financeiro.`financeiro_valor`) AS valor");
    //    $this->db->join('clientes', 'financeiro.`financeiro_forn_clie_id` = clientes.`cliente_id`');
    $this->db->where('financeiro.`financeiro_baixado`', 0);
    $this->db->where('financeiro.`financeiro_visivel`', 1);
    $this->db->where('financeiro.`financeiro_forn_clie_id`', $idCliente);

    return $this->db->get('financeiro')->result();
  }

  public function getCreditoCliente($idCliente)
  {
    $query = "SELECT cliente_limite_cred_cliente as credito FROM clientes WHERE cliente_id = $idCliente";
    $result = $this->db->query($query)->row();
    return $result->credito ? $result->credito : 0.00;
  }

  // ================ PDV MODELO 2 - FIM =====================================

  // ================ PDV MODELO 3 - INICIO =====================================

  public function getAllProdutos($filial)
  {
    $this->db->select('*');
    if (BDCAMINHO == $filial) {
      $this->db->join('' . $filial . '.itens_de_imei', '`produto`.`produto_id` = itens_de_imei.produtos_id and imei_visivel = 1 ', 'LEFT');
    } else {
      $this->db->join('' . $filial . '.itens_de_imei', '`produto`.`produto_id` = itens_de_imei.produtos_id and imei_visivel = 1 and imei_filial IS NULL', 'LEFT');
    }
    $this->db->join('categoria_produto', '`categoria_produto`.`categoria_prod_id` = produto.produto_categoria_id ', 'LEFT');
    $this->db->where('produto_visivel', 1);
    $this->db->where('vendas_id is null');
    $this->db->where('`produto_estoque` > 0');
    $this->db->order_by('produto_descricao');
    return $this->db->get('' . $filial . '.produto')->result();
  }

  public function getDiaCliente($id)
  {
    $this->db->select('cliente_limite_dias_cliente');
    $this->db->where('cliente_id', $id);
    $query = $this->db->get('clientes')->row();

    // Verifica se há resultado antes de retornar
    return $query ? $query->cliente_limite_dias_cliente : null;
  }
}
