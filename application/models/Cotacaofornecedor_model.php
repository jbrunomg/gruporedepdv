<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cotacaofornecedor_model extends CI_Model {

    public function cotacaoFornecedor($idForn,$idCota)
    {
        $sql = 'SELECT `cotacao_fornecedor_id`, `produto_descricao`, `cotacao_fornecedor_quantidade`, `cotacao_fornecedor_valor`, `categoria_prod_descricao`
            FROM `cotacao_fornecedor` 
            INNER JOIN `produto` ON cotacao_fornecedor.`cotacao_fornecedor_produto_id` = produto.`produto_id`
            INNER JOIN `categoria_produto` ON `produto`.`produto_categoria_id` = `categoria_produto`.`categoria_prod_id` 
            WHERE `cotacao_fornecedor_forn_id` = '.$idForn.' AND `cotacao_fornecedor_contacao_id` = '.$idCota.';'; 

            return $this->db->query($sql)->result();
    }

    public function pegarFornecedor($id)
    {
        $this->db->select("*");
        $this->db->where('fornecedor_id', $id);

        return $this->db->get('fornecedores')->result();
    }


    public function editarAjustePreco($dados)
    {
        $this->db->where('cotacao_fornecedor_id', $dados['cotacao_fornecedor_id']);
        $this->db->where('cotacao_fornecedor_contacao_id', $dados['cotacao_fornecedor_contacao_id']);

        $this->db->set('cotacao_fornecedor_valor', $dados['cotacao_fornecedor_valor']);
        
        if($this->db->update('cotacao_fornecedor'))
        {
            return true;
        }

        return false;
    }


}