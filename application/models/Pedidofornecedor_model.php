<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pedidofornecedor_model extends CI_Model {

	public $tabela  = "pedidos_fornecedor";
	public $visivel = "pedido_visivel";
	public $chave   = "idPedidos";

 public function listarPedidos($id)
	{
		$this->db->select("*");
		$this->db->join('fornecedores ',' pedidos_fornecedor.`clientes_id` = fornecedores.`cliente_id` ');	
		$this->db->join('usuarios ',' pedidos_fornecedor.`usuarios_id` = usuarios.`usuario_id` ');		
		$this->db->where('fornecedor_id', $id);		
		$this->db->where($this->visivel, 1);

		return $this->db->get($this->tabela)->result();
	}

 public function listarId($id)
	{
		$this->db->select("*");		
		$this->db->where($this->chave, $id);
		$this->db->join('fornecedores ',' pedidos_fornecedor.`fornecedor_id` = fornecedores.`fornecedor_id` ');	
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}

 public function adicionar($dados)
	{
		$this->db->insert($this->tabela, $dados);

		if ($this->db->affected_rows() == '1')
		{
			//return TRUE;
			return $this->db->insert_id($this->tabela);
		}
		
		return FALSE; 
	}

 public function editar($dados, $id)
	{
		$this->db->where($this->chave,$id);		
		
		if($this->db->update($this->tabela,$dados))
		{
			return true;
		}

		return false;
	}

 public function pegarProdutos($grupo)
	{
		$this->db->select('*');
		$this->db->where('produto_visivel',1);
		$this->db->where('produto_categoria_id',$grupo);
		return $this->db->get('produto')->result();
	}
 
 public function pegarProdutosID($produto)
	{
		$this->db->select('*');
		$this->db->where('produto_visivel',1);
		$this->db->where('produto_id',$produto);
		return $this->db->get('produto')->result();

	}

 public function add($table,$data,$returnId = false){

        $this->db->insert($table, $data);         

        if ($this->db->affected_rows() == '1')

		{
            if($returnId == true){

                return $this->db->insert_id($table);
            }
			return TRUE;
		}		
		return FALSE;      
    }

  public function addTotal($id, $subTotal){
     
       $sql = "UPDATE pedidos set valorTotal = valorTotal + ? WHERE idPedidos = ?";
        $this->db->query($sql, array($subTotal, $id));    

        if ($this->db->affected_rows() == '1'){
			return TRUE;
		}		
		return FALSE;      
    }

 public function pegarFornecedor($id)
	{
		$this->db->select('*');
		$this->db->where('fornecedor_visivel',1);
		$this->db->where('fornecedor_id',$id);
		return $this->db->get('fornecedores')->result();
	}

 public function pegarCategoriaProd()
	{
		$this->db->select('*');
		$this->db->where('categoria_prod_visivel',1);
		return $this->db->get('categoria_produto')->result();
	}

 public function getProdutos($id = null){

        $this->db->select('IF(`produto_estoque` >= `quantidade`,"success","danger")AS situacao_estoque, itens_de_pedidos_fornecedor.*, produto.*');

        $this->db->from('itens_de_pedidos_fornecedor');

        $this->db->join('produto','produto.produto_id = itens_de_pedidos_fornecedor.produtos_id');

        $this->db->where('pedidos_fornecedor_id',$id);
        $this->db->where('produto.produto_visivel',1);

        return $this->db->get()->result();

    }


  public function Pedido($dados,$id)
	{	

		$this->db->where($this->chave,$id);
		
		
		if($this->db->update($this->tabela,$dados))
		{
			return true;
		}

		return false;

	}

 public function validacao($id){

        $this->db->select('quantidade, produtos_id');
        $this->db->from('itens_de_pedidos');
        $this->db->where('pedidos_id',$id);
        
        foreach ($this->db->get()->result() as $row){

            $sqlConsultar = "SELECT IF(`produto_estoque` > ".$row->quantidade.", 'Y','N') AS quantidade FROM `produto` WHERE `produto_id` = ".$row->produtos_id." ";

            $resultado = $this->db->query($sqlConsultar)->result();

             if ($resultado[0]->quantidade == 'N') {

                return false;
                break;         
             }

        }

      return true;
   }

  public function itensVenda($id, $valorTotal, $cliente, $usuario){


     $sql = "INSERT INTO vendas (dataVenda, valorTotal, clientes_id, usuarios_id) VALUES (CURRENT_TIMESTAMP, '".$valorTotal."', '".$cliente."', '".$usuario."')";            

     $this->db->query($sql);  

     if ($this->db->affected_rows() == '1'){

          $idVendas = $this->db->insert_id('pedidos');

          $sqlIten = "INSERT INTO `itens_de_vendas` (
                        `subTotal`,
                        `percentual_desc`,
                        `quantidade`,
                        `vendas_id`,
                        `produtos_id`
                      ) 
                      SELECT 
                        `subTotal`,
                        `percentual_desc`,
                        `quantidade`,
                        {$idVendas},
                        `produtos_id` 
                      FROM
                        `itens_de_pedidos` 
                        
                        WHERE pedidos_id = ".$id;

          $this->db->query($sqlIten); 



          $this->db->select('quantidade, produtos_id');
          $this->db->from('itens_de_pedidos');
          $this->db->where('pedidos_id',$id);
          
          foreach ($this->db->get()->result() as $row){
              $sqlAtualizar = "UPDATE produto set produto_estoque = produto_estoque - ? WHERE produto_id = ?";
              $this->db->query($sqlAtualizar, array($row->quantidade,$row->produtos_id));
          } 

      }                


  }

  public function delete($table,$fieldID,$ID){

        $this->db->where($fieldID,$ID);

        $this->db->delete($table);

        if ($this->db->affected_rows() == '1')

		{
			return TRUE;

		}

		return FALSE;        

    }  

  public function retirarValor($subTotal, $idPedido){

        $sql = "UPDATE pedidos set valorTotal = valorTotal - ? WHERE idPedidos = ?";
        $this->db->query($sql, array($subTotal, $idPedido));     

        if ($this->db->affected_rows() == '1')

		{
			return TRUE;

		}

		return FALSE;        

    } 

  public function visualizar($id)
	{

		$query =  "SELECT * FROM pedidos
					LEFT JOIN `clientes`
					ON pedidos_fornecedor.`clientes_id` = clientes.`cliente_id`
					LEFT JOIN `usuarios`
					ON pedidos_fornecedor.`usuarios_id` = usuarios.`usuario_id`
				    LEFT JOIN `itens_de_pedidos_fornecedor`
					ON pedidos_fornecedor.`idPedidos` = itens_de_pedidos_fornecedor.`pedidos_id`
					LEFT JOIN `produto`
					ON itens_de_pedidos_fornecedor.`produtos_id` = produto.`produto_id`
    
		      WHERE pedidos_fornecedor.`pedido_visivel` = 1 
		      AND pedidos_fornecedor.`idPedidos` = $id";
		// echo "<pre>";
		// exit($query);
        return $this->db->query($query)->result();

   }

   public function emitente()
	{
		$this->db->select("*");

		return $this->db->get('emitente')->result();
	}

   public function excluir($pedido_visivel, $id)
	{

		$query =  "UPDATE `pedidos_fornecedor` SET `pedido_visivel` = $pedido_visivel WHERE `idPedidos` = '$id'";
		// echo "<pre>";
		// exit($query);
        return $this->db->query($query);

   }

}