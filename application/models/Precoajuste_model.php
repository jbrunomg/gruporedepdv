<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Precoajuste_model extends CI_Model {
	
	public $tabela  = "produto_preco_ajuste";
	public $visivel = "produto_preco_ajuste_visivel";
	public $chave   = "produto_preco_ajuste_id";

	public function listar()
	{
		$query = $this->db->query(" SELECT pd.`produto_preco_ajuste_id`, ct.categoria_prod_descricao, pd.`produto_preco_ajuste_data`,
	 (SELECT COUNT(*) FROM `itens_produto_preco_ajuste` WHERE `preco_atual` IS NOT NULL AND itens_produto_preco_ajuste.`produto_preco_ajuste_id` = pd.`produto_preco_ajuste_id`) AS feito,
	 (SELECT COUNT(*) FROM `itens_produto_preco_ajuste` WHERE `preco_atual` IS NULL AND itens_produto_preco_ajuste.`produto_preco_ajuste_id` = pd.`produto_preco_ajuste_id`) AS falta
	FROM `produto_preco_ajuste` AS pd JOIN `categoria_produto` AS ct
	ON pd.`produto_preco_ajuste_categoria_id` = ct.`categoria_prod_id` WHERE pd.`produto_preco_ajuste_visivel` = 1  ORDER BY pd.`produto_preco_ajuste_id` DESC ");
    	return $query->result();		
	}

	public function listarId($id)
	{
		$query = $this->db->query('  SELECT `idItens`,`produto_codigo`,`produto_preco_ajuste`.`produto_preco_ajuste_data`, `produto_preco_ajuste_observacao`, `categoria_prod_descricao`, `produto_descricao`, `produto_preco_venda`, produto_preco_cart_debito, produto_preco_cart_credito, `produto_estoque`, itens_produto_preco_ajuste.`produtos_id`, `produto_preco_custo`, `produto_preco_minimo_venda`, view_ultimo_ajuste_preco.`itens_produto_preco_data_atualizacao`
            FROM `itens_produto_preco_ajuste` 
            INNER JOIN `produto` ON `itens_produto_preco_ajuste`.`produtos_id` = `produto`.`produto_id`
            INNER JOIN `categoria_produto` ON `categoria_produto`.`categoria_prod_id` = `produto`.`produto_categoria_id`
            INNER JOIN `produto_preco_ajuste`  ON `produto_preco_ajuste`.`produto_preco_ajuste_id` = `itens_produto_preco_ajuste`.`produto_preco_ajuste_id`
            LEFT JOIN view_ultimo_ajuste_preco   ON itens_produto_preco_ajuste.`produtos_id` =  view_ultimo_ajuste_preco.`produtos_id`  
            WHERE `produto`.`produto_visivel`= 1   AND  `itens_produto_preco_ajuste`.`produto_preco_ajuste_id` = '.$id . ' ORDER BY `produto_descricao`;');
    	return $query->result();
	}

	public function visualizar($id)
	{
		$query = $this->db->query('  SELECT `idItens`,`produto_codigo`,`produto_preco_ajuste_data`, `produto_preco_ajuste_observacao`, `categoria_prod_descricao`, `produto_descricao`, `produto_preco_venda`, `preco_atual`, `preco`, `usuario_nome`, `produto_preco_custo`, `produto_preco_minimo_venda`
  			FROM `itens_produto_preco_ajuste` 
			INNER JOIN `produto` ON `itens_produto_preco_ajuste`.`produtos_id` = `produto`.`produto_id`
			INNER JOIN `categoria_produto` ON `categoria_produto`.`categoria_prod_id` = `produto`.`produto_categoria_id`
			INNER JOIN `produto_preco_ajuste`  ON `produto_preco_ajuste`.`produto_preco_ajuste_id` = `itens_produto_preco_ajuste`.`produto_preco_ajuste_id` 
			LEFT JOIN `usuarios`  ON `usuarios`.`usuario_id` = `itens_produto_preco_ajuste`.`itens_produto_preco_usuario_id` 
			WHERE `produto`.`produto_visivel`= 1   AND  `itens_produto_preco_ajuste`.`produto_preco_ajuste_id` = '.$id );
    	return $query->result();
	}


    public function adicionar($dados)
    {
        $this->db->insert($this->tabela, $dados);

        if ($this->db->affected_rows() == '1')
        {
            //return TRUE;
            return $this->db->insert_id($this->tabela);
        }
        
        return FALSE; 
    }


	// public function adicionar($dados)
	// {
	// 	$sql = "SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE '%_megacell_%' ";
 //        $bases = $this->db->query($sql)->result();;


 //        foreach ($bases as $b) {
 //            $this->db->insert($b->schema_name.'.'.$this->tabela, $dados);
 //        }

 //        if ($this->db->affected_rows() == '1')
 //        {
 //            return $this->db->insert_id($this->tabela);
 //        }
            
 //        return FALSE;
	// }

	public function editar($dados)
	{
		$sql = "SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE '%_".GRUPOLOJA."_%' ";
        $bases = $this->db->query($sql)->result();;
        
        foreach ($bases as $b) {
	        $this->db->where('produto_id',$dados['idProduto']);
			$this->db->set('produto_preco_venda', $dados['produto_preco_venda']);
			$this->db->set('produto_preco_cart_debito', $dados['produto_preco_cart_debito']);
			$this->db->set('produto_preco_cart_credito', $dados['produto_preco_cart_credito']);	
            $this->db->update($b->schema_name.'.'.'produto');

        }

        if($this->db->trans_status() === true){
            $this->db->trans_commit();
            return true;
        }else{
            $this->db->trans_rollback();
            return false;
        }

	}

    public function editarPMV($dados)
    {

        $sql = "SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE '%_".GRUPOLOJA."_%' ";
        $bases = $this->db->query($sql)->result();;
        
        foreach ($bases as $b) {
            $this->db->where('produto_id',$dados['idProduto']);
            $this->db->set('produto_preco_minimo_venda', $dados['produto_preco_minimo_venda']);   
            $this->db->update($b->schema_name.'.'.'produto');

        }

        if($this->db->trans_status() === true){
            $this->db->trans_commit();
            return true;
        }else{
            $this->db->trans_rollback();
            return false;
        }

    }

	public function editarCD($dados)
	{

	    $sql = "SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE '%_".GRUPOLOJA."_%' ";
        $bases = $this->db->query($sql)->result();;
        
        foreach ($bases as $b) {
			$this->db->where('produto_id',$dados['idProduto']);
			$this->db->set('produto_preco_cart_debito', $dados['produto_preco_cart_debito']);	
            $this->db->update($b->schema_name.'.'.'produto');

        }

        if($this->db->trans_status() === true){
            $this->db->trans_commit();
            return true;
        }else{
            $this->db->trans_rollback();
            return false;
        }

	}

	public function editarCC($dados)
	{

	    $sql = "SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE '%_".GRUPOLOJA."_%' ";
        $bases = $this->db->query($sql)->result();;
        
        foreach ($bases as $b) {
			$this->db->where('produto_id',$dados['idProduto']);
			$this->db->set('produto_preco_cart_credito', $dados['produto_preco_cart_credito']);	
            $this->db->update($b->schema_name.'.'.'produto');

        }

        if($this->db->trans_status() === true){
            $this->db->trans_commit();
            return true;
        }else{
            $this->db->trans_rollback();
            return false;
        }

	}

	public function editarAjustePreco($dados)
	{
		$sql = "SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE '%_".GRUPOLOJA."_%' ";
        $bases = $this->db->query($sql)->result();;
        
        foreach ($bases as $b) {

			$this->db->where('produtos_id', $dados['idProduto']);
			$this->db->where('produto_preco_ajuste_id', $dados['idAjuste']);

			$this->db->set('preco_atual', $dados['produto_preco_venda']);
			$this->db->set('preco_atual_cartao_debito', $dados['produto_preco_cart_debito']);
			$this->db->set('preco_atual_cartao_credito', $dados['produto_preco_cart_credito']);
			$this->db->set('itens_produto_preco_usuario_id', $dados['itens_produto_preco_usuario_id']);
            $this->db->update($b->schema_name.'.'.'itens_produto_preco_ajuste');

        }

        if($this->db->trans_status() === true){
            $this->db->trans_commit();
            return true;
        }else{
            $this->db->trans_rollback();
            return false;
        }

	}

   public function editarAjustePrecoPMV($dados)
    {

        $sql = "SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE '%_".GRUPOLOJA."_%' ";
        $bases = $this->db->query($sql)->result();;
        
        foreach ($bases as $b) {

            $this->db->where('produtos_id', $dados['idProduto']);
            $this->db->where('produto_preco_ajuste_id', $dados['idAjuste']);

            $this->db->set('produto_preco_minimo_venda_atual', $dados['produto_preco_minimo_venda']);
            $this->db->set('itens_produto_preco_usuario_id', $dados['itens_produto_preco_usuario_id']);
            $this->db->update($b->schema_name.'.'.'itens_produto_preco_ajuste');

        }

        if($this->db->trans_status() === true){
            $this->db->trans_commit();
            return true;
        }else{
            $this->db->trans_rollback();
            return false;
        }

    }

    public function editarAjustePrecoCD($dados)
	{

	    $sql = "SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE '%_".GRUPOLOJA."_%' ";
        $bases = $this->db->query($sql)->result();;
        
        foreach ($bases as $b) {

			$this->db->where('produtos_id', $dados['idProduto']);
			$this->db->where('produto_preco_ajuste_id', $dados['idAjuste']);

			$this->db->set('preco_atual_cartao_debito', $dados['produto_preco_cart_debito']);
			$this->db->set('itens_produto_preco_usuario_id', $dados['itens_produto_preco_usuario_id']);
            $this->db->update($b->schema_name.'.'.'itens_produto_preco_ajuste');

        }

        if($this->db->trans_status() === true){
            $this->db->trans_commit();
            return true;
        }else{
            $this->db->trans_rollback();
            return false;
        }

	}

	public function editarAjustePrecoCC($dados)
	{
	    $sql = "SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE '%_".GRUPOLOJA."_%' ";
        $bases = $this->db->query($sql)->result();;
        
        foreach ($bases as $b) {

			$this->db->where('produtos_id', $dados['idProduto']);
			$this->db->where('produto_preco_ajuste_id', $dados['idAjuste']);

			$this->db->set('preco_atual_cartao_credito', $dados['produto_preco_cart_credito']);
			$this->db->set('itens_produto_preco_usuario_id', $dados['itens_produto_preco_usuario_id']);
            $this->db->update($b->schema_name.'.'.'itens_produto_preco_ajuste');

        }

        if($this->db->trans_status() === true){
            $this->db->trans_commit();
            return true;
        }else{
            $this->db->trans_rollback();
            return false;
        }

	}

	public function excluir($dados, $id)
	{
		$sql = "SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE '%_".GRUPOLOJA."_%' ";
        $bases = $this->db->query($sql)->result();;
        
        foreach ($bases as $b) {
            $this->db->where($this->chave,$id); 
            $this->db->update($b->schema_name.'.'.$this->tabela,$dados);

        }

        if($this->db->trans_status() === true){
            $this->db->trans_commit();
            return true;
        }else{
            $this->db->trans_rollback();
            return false;
        }

	}

	public function pegarGrupoProduto() 
	{
		$this->db->select('*');
		$this->db->where('categoria_prod_visivel', 1);
		return $this->db->get('categoria_produto')->result();
	}

    public function prepararItens($categoria,$id)
    {
        $sql = ' INSERT INTO `itens_produto_preco_ajuste` (`preco`,`produtos_id`,`produto_preco_ajuste_id`)
            SELECT `produto_preco_venda`, `produto_id`, '.$id.' FROM produto WHERE `produto_categoria_id` = '.$categoria;
        return $this->db->query($sql);

    }

	// public function prepararItens($categoria,$id)
	// {

	//     $sql = "SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE '%_megacell_%' ";
 //           $bases = $this->db->query($sql)->result();;
        
 //           foreach ($bases as $b) {

 //        	$sql = ' INSERT INTO '.$b->schema_name.'.`itens_produto_preco_ajuste` (`preco`,`produtos_id`,`produto_preco_ajuste_id`)
	//         SELECT `produto_preco_venda`, `produto_id`, '.$id.' FROM '.$b->schema_name.'. produto WHERE `produto_categoria_id` = '.$categoria;
	//         $this->db->query($sql);

 //        }

 //        if($this->db->trans_status() === true){
 //            $this->db->trans_commit();
 //            return true;
 //        }else{
 //            $this->db->trans_rollback();
 //            return false;
 //        }		

	// }

	public function getAllParametros()
    {
        $this->db->select('parametro_cart_debito, parametro_cart_credito');
		return $this->db->get('parametro')->result();
    }



}