<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chat_model extends CI_Model {

    public $tabela = "produto";
    
	public function pesquisar($nome,$categoria = null)
	{	
		if ($this->session->userdata('parametro_chat_preco') == 'P2') {
			# AMBOS...
			$this->db->select("produto_descricao,concat(' Atacado: R$', produto_preco_minimo_venda, ' - Varejo: R$ ', produto_preco_venda) as valor ,produto_estoque");
		};

		if ($this->session->userdata('parametro_chat_preco') == 'PV') {
		 	# Preço minimo Venda...
		 	$this->db->select(" produto_descricao, concat('Varejo: R$ ', produto_preco_venda) as valor ,produto_estoque");
		} 

		if ($this->session->userdata('parametro_chat_preco') == 'PMV') {
		 	# Preço minimo Venda...
		 	$this->db->select(" produto_descricao, concat('Atacado: R$ ', produto_preco_minimo_venda) as valor ,produto_estoque");
		} 
		
		if (($categoria <> null) || ($categoria < 12)){
		$this->db->where('produto_categoria_id',$categoria);
		}
        $this->db->like('produto_descricao', $nome);
		return $this->db->get($this->tabela)->result();
    }

    public function pesquisar2($nome,$categoria = null)
	{	
		
		$this->db->select(" nome_sistema ");	 
		
		if (($categoria <> null) || ($categoria < 12)){
		$this->db->where('produto_categoria_id',$categoria);
		}

        $this->db->where('nome_digitado',$nome);
		return $this->db->get('chat_palavra_chave')->result();
    }

    public function parametro()
	{	
		$this->db->select(' parametro_chat_estoque,parametro_chat_preco,parametro_chat_tentiva_busca,parametro_chat_n_whatsapp ');	
		return $this->db->get('parametro')->result();
    }

    public function salvarPesquisa($dados)
	{

		$this->db->insert('chat_mensagem', $dados);

		if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE; 


	}
    
}