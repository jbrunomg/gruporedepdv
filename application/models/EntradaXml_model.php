<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Entradaxml_model extends CI_Model
{

	public $tabela = "movimentacao_produtos";
	public $visivel = "movimentacao_produto_visivel";
	public $chave = "movimentacao_produto_id";

	public function listar()
	{
		$this->db->select("*, ROUND(SUM(`item_movimentacao_produto_custo` * `item_movimentacao_produto_quantidade`), 2) AS total, fornecedor_nome");
		$this->db->join('itens_movimentacao_produtos', '`movimentacao_produtos`.`movimentacao_produto_id` = `itens_movimentacao_produtos`.`item_movimentacao_produto_movimentacao_produto_id`');
		$this->db->join('fornecedores', '`fornecedores`.`fornecedor_id` = `movimentacao_produtos`.`movimentacao_fornecedor_id`');
		$this->db->where($this->visivel, 1);
		$this->db->where('movimentacao_produto_visivel', 1);
		$this->db->where('movimentacao_produto_chave_xml IS NOT NULL');
		$this->db->group_by('movimentacao_produto_id');
		$this->db->order_by("movimentacao_produto_id", "desc");

		return $this->db->get($this->tabela)->result();
	}

	public function listarId($id)
	{
		$this->db->select("*");
		$this->db->where($this->chave, $id);
		$this->db->join('fornecedores ', ' movimentacao_produtos.`movimentacao_fornecedor_id` = fornecedores.`fornecedor_id` ');
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}

	public function getProdutosList($id)
	{
		$this->db->distinct();
		$this->db->select(
			'
					itens_movimentacao_produtos.item_movimentacao_produto_movimentacao_produto_id AS idMovimentacaoProduto,
					itens_movimentacao_produtos.item_movimentacao_produto_produto_id AS idProduto,
					produto.produto_codigo AS codigoProduto,
					produto.produto_descricao AS descricaoProduto,
					produto.produto_preco_custo AS precocusto,
					produto.produto_categoria_id AS categoriaId,
					produto.produto_estoque AS Estoque,
					itens_de_imei.imei_valor as Imei,
					itens_movimentacao_produtos.item_movimentacao_produto_quantidade AS quantidadeProduto,
					itens_movimentacao_produtos.item_movimentacao_produto_dolar AS dolarProduto,
					itens_movimentacao_produtos.item_movimentacao_produto_custo AS custoProduto,
					itens_movimentacao_produtos.item_movimentacao_produto_minimo AS minimoProduto,
					itens_movimentacao_produtos.item_movimentacao_produto_preco_unitario AS vendaProduto'
		);

		$this->db->from('itens_movimentacao_produtos');

		$this->db->join('produto', 'produto.produto_id = itens_movimentacao_produtos.item_movimentacao_produto_produto_id');
		$this->db->join('itens_de_imei', 'itens_de_imei.produtos_id = itens_movimentacao_produtos.item_movimentacao_produto_produto_id and itens_de_imei.movimentacao_id = itens_movimentacao_produtos.item_movimentacao_produto_movimentacao_produto_id');
		$this->db->join('movimentacao_produtos', 'movimentacao_produtos.movimentacao_produto_id = itens_movimentacao_produtos.item_movimentacao_produto_movimentacao_produto_id');

		$this->db->where('item_movimentacao_produto_movimentacao_produto_id', $id);
		$this->db->where('itens_movimentacao_produtos.item_movimentacao_produto_visivel', 1);
		$this->db->where('movimentacao_produtos.movimentacao_produto_visivel', 1);
		$this->db->where('itens_de_imei.imei_visivel', 1);
		$this->db->order_by('produto.produto_descricao');

		return $this->db->get()->result();
	}

	public function getProdutosList_Agrupado($id)
	{
		$this->db->select(
			'GROUP_CONCAT(itens_movimentacao_produtos.item_movimentacao_produto_id) AS itemMovimentacaoProdutosId,
					produto.produto_id AS idProduto,
					produto.produto_codigo AS codigoProduto,
					produto.produto_descricao AS descricaoProduto,
					produto.produto_preco_custo AS precocusto,
					produto.produto_categoria_id AS categoriaId,
					produto.produto_estoque AS Estoque,
					SUM(itens_movimentacao_produtos.item_movimentacao_produto_quantidade) AS quantidadeProduto,
					AVG(itens_movimentacao_produtos.item_movimentacao_produto_dolar) AS dolarProduto,
					AVG(itens_movimentacao_produtos.item_movimentacao_produto_custo) AS custoProduto,
					MIN(itens_movimentacao_produtos.item_movimentacao_produto_minimo) AS minimoProduto,
					AVG(itens_movimentacao_produtos.item_movimentacao_produto_preco_unitario) AS vendaProduto'
		);

		$this->db->from('itens_movimentacao_produtos');
		$this->db->join('produto', 'produto.produto_id = itens_movimentacao_produtos.item_movimentacao_produto_produto_id');
		$this->db->join('movimentacao_produtos', 'movimentacao_produtos.movimentacao_produto_id = itens_movimentacao_produtos.item_movimentacao_produto_movimentacao_produto_id');

		$this->db->where('item_movimentacao_produto_movimentacao_produto_id', $id);
		$this->db->where('item_movimentacao_produto_visivel', 1);
		$this->db->where('movimentacao_produtos.movimentacao_produto_visivel', 1);
		$this->db->group_by('produto.produto_id');
		$this->db->order_by('produto.produto_descricao');

		return $this->db->get()->result();
	}




	public function adicionar($dados)
	{
		$this->db->insert($this->tabela, $dados);

		if ($this->db->affected_rows() == '1') {
			//return TRUE;
			return $this->db->insert_id($this->tabela);
		}

		return FALSE;
	}

	public function editar($dados, $id)
	{
		$this->db->where($this->chave, $id);

		if ($this->db->update($this->tabela, $dados)) {
			return true;
		}

		return false;
	}

	public function editarTabela($table, $fieldID, $id, $dados)
	{
		$this->db->where($fieldID, $id);

		if ($this->db->update($table, $dados)) {
			return true;
		}

		return false;
	}

	public function listarIdTabelaVisivel($table, $fieldID, $id, $chaveVisivel, $chave = '*')
	{
		$this->db->select($chave);
		$this->db->where($fieldID, $id);
		$this->db->where($chaveVisivel, 1);
		return $this->db->get($table)->result();
	}

	public function pegarProdutos($grupo)
	{
		$this->db->select('*');
		$this->db->where('produto_visivel', 1);
		$this->db->where('produto_categoria_id', $grupo);
		return $this->db->get('produto')->result();
	}

	public function pegarProdutosID($produto, $chave = '*')
	{
		$this->db->select($chave);
		$this->db->where('produto_visivel', 1);
		$this->db->where('produto_id', $produto);
		return $this->db->get('produto')->result();
	}

	public function add($table, $data, $returnId = false)
	{

		$this->db->insert($table, $data);

		if ($this->db->affected_rows() == '1') {
			if ($returnId == true) {

				return $this->db->insert_id($table);
			}
			return TRUE;
		}
		return FALSE;
	}

	public function pegarCategoriaProd($ids)
	{

		$sql = 'SELECT * FROM `categoria_produto` WHERE `categoria_prod_visivel` = 1';

		if ($ids) {
			$sql .= ' AND `categoria_prod_id` IN(' . $ids . ')';
		}


		return $this->db->query($sql)->result();
	}

	public function consultaTipo($id)
	{
		$this->db->where($this->chave, $id);
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}

	public function getProdutos($id = null)
	{
		$this->db->select('IF(`produto_estoque` >= `quantidade`,"success","danger")AS situacao_estoque, movimentacao_produtos.*, produto.*');
		$this->db->from('movimentacao_produtos');
		$this->db->join('produto', 'produto.produto_id = itens_movimentacao_produtos.item_movimentacao_produto_produto_id');
		$this->db->where('item_movimentacao_produto_produto_id', $id);
		$this->db->where('produto.produto_visivel', 1);

		return $this->db->get()->result();
	}

	public function listarImeiValor($imei_valor, $chave = '*')
	{
		$this->db->select($chave);
		$this->db->where('imei_valor', $imei_valor);
		$resposta = $this->db->get('itens_de_imei');

		$erro = $this->db->error();
		if ($erro['code'] !== 0) {
			return [];
		}

		return $resposta->result_array();
	}

	public function validacaoItenDaVenda($produto_codigo, $chave = '*')
	{
		$this->db->select($chave);
		$this->db->where("produto_codigo", $produto_codigo);
		$this->db->where("produto_visivel", 1);
		$resposta = $this->db->get("produto");

		$erro = $this->db->error();
		if ($erro['code'] !== 0) {
			return [];
		}

		return $resposta->result_array();
	}

	public function pegarImeiProdutoVendido($imei_valor, $chave = '*')
	{
		$this->db->select($chave);
		$this->db->where('imei_valor', $imei_valor);
		$this->db->where('vendas_id IS NOT NULL');
		$resposta = $this->db->get("itens_de_imei");
		
		$erro = $this->db->error();
		if ($erro['code'] !== 0) {
			return [];
		}

		return $resposta->result_array();
	}

	public function pegarFornecedorPorCNPJ($cnpj, $chave = '*')
	{

		$this->db->select($chave);
		$this->db->where("fornecedor_cnpj_cpf", $cnpj);
		$this->db->where("fornecedor_visivel", 1);
		$resposta = $this->db->get("fornecedores");

		$erro = $this->db->error();
		if ($erro['code'] !== 0) {
			return [];
		}

		return $resposta->result_array();
	}

	public function pegarEmitentePorCNPJ($cnpj, $chave = '*')
	{

		$this->db->select($chave);
		$this->db->where("emitente_cnpj", $cnpj);
		// não a um emitente_visivel na tabela emitente
		// $this->db->where("emitente_visivel", 1);
		$resposta = $this->db->get("emitente");

		$erro = $this->db->error();
		if ($erro['code'] !== 0) {
			return [];
		}

		return $resposta->result_array();
	}

	public function pegarXmlComInfNFeId($chave_xml, $chave = '*')
	{
		$this->db->select($chave);
		$this->db->where("movimentacao_produto_chave_xml", $chave_xml);
		$this->db->where($this->visivel, 1);
		$resposta = $this->db->get($this->tabela);

		$erro = $this->db->error();
		if ($erro['code'] !== 0) {
			return [];
		}

		return $resposta->result_array();
	}

	public function updateProdutoEntrada($produto)
	{

		if ($produto->dolarProduto != '0.00') {
			$this->db->set('produto_preco_dolar', $produto->dolarProduto);
		}

		if ($produto->custoProduto != '0.00') {
			$this->db->set('produto_preco_custo', $produto->custoProduto);
		}

		if ($produto->minimoProduto != '0.00') {
			$this->db->set('produto_preco_minimo_venda', $produto->minimoProduto);
		}

		if ($produto->vendaProduto != '0.00') {
			$this->db->set('produto_preco_venda', $produto->vendaProduto);
		}

		$this->db->set('produto_estoque', ($produto->Estoque + $produto->quantidadeProduto));
		$this->db->where('produto_id', $produto->idProduto);
		$this->db->update('produto');

		if ($this->db->affected_rows() == '1') {
			return true;
		}

		return false;
	}

	public function remuveProdutoEntrada($produto)
	{

		if ($produto->dolarProduto != '0.00') {
			$this->db->set('produto_preco_dolar', $produto->dolarProduto);
		}

		if ($produto->custoProduto != '0.00') {
			$this->db->set('produto_preco_custo', $produto->custoProduto);
		}

		if ($produto->minimoProduto != '0.00') {
			$this->db->set('produto_preco_minimo_venda', $produto->minimoProduto);
		}

		if ($produto->vendaProduto != '0.00') {
			$this->db->set('produto_preco_venda', $produto->vendaProduto);
		}

		$this->db->set('produto_estoque', ($produto->Estoque - $produto->quantidadeProduto));
		$this->db->where('produto_id', $produto->idProduto);
		$this->db->update('produto');

		if ($this->db->affected_rows() == '1') {
			return true;
		}

		return false;
	}


	public function updateProdutoSaidaEstoque($idProduto, $Estoque, $quantidadeProduto)
	{

		$this->db->set('produto_estoque', ($Estoque - $quantidadeProduto));

		$this->db->where('produto_id', $idProduto);

		$this->db->update('produto');

		if ($this->db->affected_rows() == '1') {
			return true;
		}

		return false;
	}

	public function updateProdutoEntradaEstoque($idProduto, $Estoque, $quantidadeProduto)
	{

		$this->db->set('produto_estoque', ($Estoque + $quantidadeProduto));

		$this->db->where('produto_id', $idProduto);

		$this->db->update('produto');

		if ($this->db->affected_rows() == '1') {
			return true;
		}

		return false;
	}

	public function updateStatusEntradaXml($id)
	{

		$this->db->set('movimentacao_produto_status', 0);
		$this->db->where('movimentacao_produto_id', $id);
		$this->db->update('movimentacao_produtos');


		if ($this->db->affected_rows() == '1') {
			return TRUE;
		}

		return FALSE;
	}

	public function updateStatusMovimentacao($id)
	{
		$this->db->select("*");
		$this->db->where($this->chave, $id);
		$this->db->where($this->visivel, 1);
		$result = $this->db->get($this->tabela)->result();

		$this->db->select("*");
		// $this->db->where('item_movimentacao_produto_produto_id', $result[0]->movimentacao_produto_id);
		$this->db->where('item_entrada_xml_produtos_entrada_xml_produtos_id', $result[0]->entrada_xml_produtos_id);
		$this->db->where('item_entrada_xml_produtos_visivel', 1);
		$resulItens = $this->db->get('itens_entrada_xml_produtos')->result();

		if ($result[0]->entrada_xml_produtos_tipo == 'Saída' and $result[0]->entrada_xml_produtos_saida_tipo == 'filial') {

			$this->db->set('entrada_xml_produtos_status', 0);
			$this->db->where('entrada_xml_produtos_id', $id);
			$this->db->update('entrada_xml_produtos');

			$loja = explode('_', BDCAMINHO);
			$lojadestino = $loja[0] . '_' . $loja[1] . '_' . $result[0]->entrada_xml_produtos_saida_filial;

			$dados = array(
				'entrada_xml_produtos_descricao' => $result[0]->entrada_xml_produtos_descricao . ' </br> Emitido: ' . $loja[2],
				'entrada_xml_produtos_tipo' => 'Entrada',
				'entrada_xml_produtos_saida_tipo' => $result[0]->entrada_xml_produtos_saida_tipo,
				'entrada_xml_produtos_saida_filial' => $result[0]->entrada_xml_produtos_saida_filial,
				'entrada_xml_produtos_fornecedor_id' => $result[0]->entrada_xml_produtos_fornecedor_id,
				'entrada_xml_produtos_usuario_id' => $result[0]->entrada_xml_produtos_usuario_id,
				'entrada_xml_produtos_data_cadastro' => date('Y-m-d'),
				'entrada_xml_produtos_tipo_moeda' => $result[0]->entrada_xml_produtos_tipo_moeda,
				'entrada_xml_produtos_cotacao_moeda' => $result[0]->entrada_xml_produtos_cotacao_moeda,
				'entrada_xml_produtos_porcentagem' => $result[0]->entrada_xml_produtos_porcentagem,
				'entrada_xml_produtos_saida_origem_nome' => $loja[2],
				'entrada_xml_produtos_saida_origem_id' => $result[0]->entrada_xml_produtos_saida_origem_id,
				'entrada_xml_produtos_visivel' => 1
			);

			$this->db->insert($lojadestino . '.' . 'entrada_xml_produtos', $dados);

			if ($this->db->affected_rows() == '1') {
				$novoID = $this->db->insert_id('entrada_xml_produtos');
			}

			foreach ($resulItens as $itens) {

				$dados = array(
					'item_entrada_xml_produtos_quantidade' => $itens->item_entrada_xml_produtos_quantidade,
					'item_entrada_xml_produtos_preco_unitario' => $itens->item_entrada_xml_produtos_preco_unitario,
					'item_entrada_xml_produtos_custo' => $itens->item_entrada_xml_produtos_custo,
					'item_entrada_xml_produtos_minimo' => $itens->item_entrada_xml_produtos_minimo,
					'item_entrada_xml_produtos_produto_id' => $itens->item_entrada_xml_produtos_produto_id,
					'item_entrada_xml_produtos_dolar' => $itens->item_entrada_xml_produtos_dolar,
					'item_entrada_xml_produtos_entrada_xml_produtos_id' => $novoID
				);

				$this->db->insert($lojadestino . '.' . 'itens_entrada_xml_produtos', $dados);
			}
			if ($this->db->affected_rows() == '1') {
				return TRUE;
			}
			return FALSE;
		} else {

			$this->db->set('entrada_xml_produtos_status', 0);
			$this->db->where('entrada_xml_produtos_id', $id);

			$this->db->update('entrada_xml_produtos');

			if ($this->db->affected_rows() == '1') {
				return true;
			}

			return false;
		}
	}

	public function delete($table, $fieldID, $ID)
	{

		$this->db->where($fieldID, $ID);

		$this->db->delete($table);

		if ($this->db->affected_rows() > 0) {
			return TRUE;
		}

		return FALSE;
	}

	public function excluir($table, $fieldID, $id, $dados)
	{

		$this->db->where($fieldID, $id);
		return $this->db->update($table, $dados);
	}

	public function editarItemMovimentacao($dados, $where, $limit = 0)
	{
			if (empty($dados) || empty($where)) {
					return false;
			}

			$this->db->where($where);

			if ($limit > 0) {
					$this->db->limit($limit);
			}

			$result = $this->db->update('itens_movimentacao_produtos', $dados);

			return $result;
	}

	public function autocompleteProduto($texto)
	{
		$sql = "SELECT produto_descricao, produto_id, produto_codigo FROM produto WHERE produto_descricao like '%{$texto}%' OR produto_codigo like '%{$texto}%' limit 50";

		$resultado = $this->db->query($sql)->result();

		if (count($resultado) > 0) {
			foreach ($resultado as $row) {
				$row_set[] = array('label' =>  $row->produto_descricao . ' - ' . $row->produto_codigo, 'descricao' => $row->produto_descricao, 'cod' => $row->produto_codigo, 'id' => $row->produto_id);
			}
			return $row_set;
		}
	}
}
