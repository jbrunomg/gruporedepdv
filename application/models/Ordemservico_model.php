<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ordemservico_model extends CI_Model {

	public $tabela  = "os";
	public $visivel = "os_visivel";
	public $chave   = "os_id";

	public function listar($limit, $start, $search, $order_column_name, $order_dir) {
		$this->db->select('*');
		$this->db->join('clientes','os.`clientes_id` = clientes.`cliente_id`');	
		$this->db->join('usuarios','os.`usuarios_id` = usuarios.`usuario_id`');			
		$this->db->from($this->tabela);
		$this->db->where($this->visivel, 1);
		
		if (!empty($search)) {
			$this->db->group_start();
			$this->db->like('os_id', $search); // Substitua 'seu_campo' pelo campo que deseja pesquisar
			$this->db->or_like('cliente_nome', $search); 
			$this->db->or_like('os_equipamento', $search); 
			$this->db->or_like('os_status', $search); 
			$this->db->group_end();
		}
	
		// Ordenação
		if (!empty($order_column_name and $order_column_name != 'os_id')) {
			$this->db->order_by("TRIM($order_column_name)", $order_dir);
		}else{
			$this->db->order_by($this->chave, 'desc');
		}
		$this->db->limit($limit, $start);
	
		// Execute a consulta e retorne os resultados
		$resultados = $this->db->get()->result();
	
		$this->db->from($this->tabela);
		$this->db->join('clientes','os.`clientes_id` = clientes.`cliente_id`');	
		$this->db->join('usuarios','os.`usuarios_id` = usuarios.`usuario_id`');		
		$this->db->where($this->visivel, 1);	
		if (!empty($search)) {
			$this->db->group_start();
			$this->db->like('os_id', $search); // Substitua 'seu_campo' pelo campo que deseja pesquisar
			$this->db->or_like('cliente_nome', $search); 
			$this->db->or_like('os_equipamento', $search); 
			$this->db->or_like('os_status', $search); 
			$this->db->group_end();
		}
		$total_registros = $this->db->count_all_results();
		
		return array('dados' => $resultados, 'total_registros' => $total_registros);
	}

 public function listarId($id)
	{
		$this->db->select("os.*, clientes.*, usuarios.*,  tec.usuario_nome as tec_nome,");		
		$this->db->where($this->chave, $id);
		$this->db->join('clientes ',' clientes.`cliente_id` = os.`clientes_id` ');
		$this->db->join('usuarios ',' usuarios.`usuario_id` = os.`usuarios_id` ');
		$this->db->join('usuarios as tec ',' tec.`usuario_id` = os.`tec_id` ','LEFT');	

		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}


 public function adicionar($dados)
	{
		$this->db->insert($this->tabela, $dados);

		if ($this->db->affected_rows() == '1')
		{
			//return TRUE;
			return $this->db->insert_id($this->tabela);
		}
		
		return FALSE; 
	}

 public function editar($dados, $id)
	{
		$this->db->where($this->chave,$id);		
		
		if($this->db->update($this->tabela,$dados))
		{
			return true;
		}

		return false;
	}

 public function pegarProdutos($grupo)
	{
		$this->db->select('*');
		$this->db->where('produto_visivel',1);
		$this->db->where('produto_categoria_id',$grupo);
		return $this->db->get('produto')->result();
	}
 
 public function pegarProdutosID($produto)
	{
		$this->db->select('*');
		$this->db->where('produto_visivel',1);
		$this->db->where('produto_id',$produto);
		return $this->db->get('produto')->result();

	}

public function pegarServicosID($servico)
	{
		$this->db->select('*');
		$this->db->where('servico_visivel',1);
		$this->db->where('servico_id',$servico);
		return $this->db->get('servicos')->result();

	}


    public function add($table,$data,$returnId = false){

        $this->db->insert($table, $data);         

        if ($this->db->affected_rows() == '1')

		{
            if($returnId == true){

                return $this->db->insert_id($table);
            }
			 return TRUE;
		}		
		return FALSE;      
    }

  public function addTotal($id, $subTotal){
     
       $sql = "UPDATE os set os_valorTotal = os_valorTotal + ? WHERE os_id = ?";
        $this->db->query($sql, array($subTotal, $id));    

        if ($this->db->affected_rows() == '1'){
			return TRUE;
		}		
		return FALSE;      
    }

 public function pegarFornecedor()
	{
		$this->db->select('*');
		$this->db->where('fornecedor_visivel',1);
		return $this->db->get('fornecedores')->result();
	}

 public function pegarCategoriaProd()
	{
		$this->db->select('*');
		$this->db->where('categoria_prod_visivel',1);
		return $this->db->get('categoria_produto')->result();
	}


public function getProdutos($id = null){

        $this->db->select('IF(`produto_estoque` >= `produtos_os_quantidade`,"success","danger")AS situacao_estoque, itens_de_os_produto.*, produto.*');

        $this->db->from('itens_de_os_produto');

        $this->db->join('produto','produto.produto_id = itens_de_os_produto.produtos_id');

        $this->db->where('os_id',$id);
        $this->db->where('produto.produto_visivel',1);

        return $this->db->get()->result();

    }


public function getServicos($id = null){

        $this->db->select('itens_de_os_servicos.*, servicos.*');
        $this->db->from('itens_de_os_servicos');
        $this->db->join('servicos','servicos.servico_id = itens_de_os_servicos.servicos_id');
        $this->db->where('os_id',$id);
        $this->db->where('servicos.servico_visivel',1);

        return $this->db->get()->result();

    }


 public function getAnexos($id = null){

        $this->db->select('*');
        $this->db->from('anexos');
        $this->db->where('os_id',$id);

        return $this->db->get()->result();

}


public function getProdutosUltimo($id = null, $idItens){

        $this->db->select('IF(`produto_estoque` >= `produtos_os_quantidade`,"success","danger")AS situacao_estoque, itens_de_os_produto.*, produto.*');

        $this->db->from('itens_de_os_produto');

        $this->db->join('produto','produto.produto_id = itens_de_os_produto.produtos_id');

        $this->db->where('os_id',$id);
		$this->db->where('idItens',$idItens);
        $this->db->where('produto.produto_visivel',1);

        return $this->db->get()->result();

    }

public function getServicosUltimo($id = null, $idItens){

		$this->db->select('itens_de_os_servicos.*, servicos.*');
		$this->db->from('itens_de_os_servicos');
		$this->db->join('servicos','servicos.servico_id = itens_de_os_servicos.servicos_id');
		$this->db->where('os_id',$id);
		$this->db->where('idItens',$idItens);
		$this->db->where('servicos.servico_visivel',1);

        return $this->db->get()->result();

    }

public function getAnexoUltimo($id = null, $idItens){

    	$this->db->select('*');
	    $this->db->from('anexos');
		$this->db->where('os_id',$id);
		$this->db->where('anexo_id',$idItens);

        return $this->db->get()->result();

    }
	
  public function Ordemservico($dados,$id)
	{	

		$this->db->where($this->chave,$id);
		
		
		if($this->db->update($this->tabela,$dados))
		{
			return true;
		}

		return false;

	}

 public function validacao($id){

        $this->db->select('quantidade, produtos_id');
        $this->db->from('itens_de_os');
        $this->db->where('os_id',$id);
        
        foreach ($this->db->get()->result() as $row){

            $sqlConsultar = "SELECT IF(`produto_estoque` >= ".$row->quantidade.", 'Y','N') AS quantidade FROM `produto` WHERE `produto_id` = ".$row->produtos_id." ";

            $resultado = $this->db->query($sqlConsultar)->result();

             if ($resultado[0]->quantidade == 'N') {

                return false;
                break;         
             }

        }

      return true;
   }

  public function itensVenda($id, $valorTotal, $cliente, $usuario){


     $sql = "INSERT INTO vendas (dataVenda, valorTotal, clientes_id, usuarios_id, faturado) VALUES (CURRENT_TIMESTAMP, '".$valorTotal."', '".$cliente."', '".$usuario."', '1')";            

     $this->db->query($sql);  

     if ($this->db->affected_rows() == '1'){

          $idVendas = $this->db->insert_id('os');

          $sqlIten = "INSERT INTO `itens_de_vendas` (
                        `subTotal`,
                        `percentual_desc`,
                        `quantidade`,
                        `vendas_id`,
                        `produtos_id`
                      ) 
                      SELECT 
                        `subTotal`,
                        `percentual_desc`,
                        `quantidade`,
                        {$idVendas},
                        `produtos_id` 
                      FROM
                        `itens_de_os` 
                        
                        WHERE os_id = ".$id;

          $this->db->query($sqlIten); 



          $this->db->select('quantidade, produtos_id');
          $this->db->from('itens_de_os');
          $this->db->where('os_id',$id);
          
          foreach ($this->db->get()->result() as $row){
              $sqlAtualizar = "UPDATE produto set produto_estoque = produto_estoque - ? WHERE produto_id = ?";
              $this->db->query($sqlAtualizar, array($row->quantidade,$row->produtos_id));
          } 


      } 

	      $filial = BDCAMINHO;
	      $filial = explode("_", $filial);
	      $sqlConsultarCliente = "SELECT cliente_nome FROM clientes WHERE cliente_id = ".$cliente." ";   
	      $resultado = $this->db->query($sqlConsultarCliente)->result_array();
          $financeiro = array(
            'financeiro_descricao' => 'Fatura de Venda - #'.$idVendas.' - '.strtoupper($filial[2]),
            'financeiro_valor' => $valorTotal,
            'data_vencimento' => date('Y-m-d'),
            'data_pagamento' => date('Y-m-d'),
            'financeiro_baixado' => '1',
            'financeiro_forn_clie' => $resultado[0]['cliente_nome'],
            'financeiro_forn_clie_id' => $cliente,
            'financeiro_forma_pgto' => 'Outros',
            'financeiro_tipo' => 'receita',
            'vendas_id' => $idVendas
          );            

        $this->db->insert('financeiro', $financeiro);
  }

  public function delete($table,$fieldID,$ID){

        $this->db->where($fieldID,$ID);

        $this->db->delete($table);

        if ($this->db->affected_rows() == '1')

		{
			return TRUE;

		}

		return FALSE;        

    }  

  public function retirarValor($subTotal, $idOrdemservico){

        $sql = "UPDATE os set os_valorTotal = os_valorTotal - ? WHERE os_id = ?";
        $this->db->query($sql, array($subTotal, $idOrdemservico));     

        if ($this->db->affected_rows() == '1')

		{
			return TRUE;

		}

		return FALSE;        

    } 

  public function visualizar($id)
	{

		$query =  "SELECT * FROM os
					LEFT JOIN `clientes`
					ON os.`clientes_id` = clientes.`cliente_id`
					LEFT JOIN `usuarios`
					ON os.`usuarios_id` = usuarios.`usuario_id`
				    LEFT JOIN `itens_de_os`
					ON os.`idOrdemservicos` = itens_de_os.`os_id`
					LEFT JOIN `produto`
					ON itens_de_os.`produtos_id` = produto.`produto_id`
    
		      WHERE os.`pedido_visivel` = 1 
		      AND os.`idOrdemservicos` = $id";
		// echo "<pre>";
		// exit($query);
        return $this->db->query($query)->result();

        if (isset($visivel)) {
        	$where =  'vendas.`idVendas` ='. $id;		
       	} else {
       		$where = 'vendas.`venda_visivel` = 1 AND vendas.`idVendas` ='. $id;
       	}		

		$query =  "SELECT *, ROUND((itens_de_vendas.`subTotal` / quantidade), 2) as valor_unitario FROM vendas
					LEFT JOIN `clientes`
					ON vendas.`clientes_id` = clientes.`cliente_id`
					LEFT JOIN `usuarios`
					ON vendas.`usuarios_id` = usuarios.`usuario_id`
				    LEFT JOIN `itens_de_vendas`
					ON vendas.`idVendas` = itens_de_vendas.`vendas_id`
					LEFT JOIN `produto`
					ON itens_de_vendas.`produtos_id` = produto.`produto_id`
    
		      WHERE $where";
		// echo "<pre>";
		// exit($query);
        return $this->db->query($query)->result();



   }

   public function emitente()
	{
		$this->db->select("*");

		return $this->db->get('emitente')->result();
	}

   public function excluir($os_visivel, $id)
	{

		$query =  "UPDATE `os` SET `os_visivel` = $os_visivel WHERE `os_id` = '$id'";
		// echo "<pre>";
		// exit($query);
        return $this->db->query($query);

   }

   public function listarServicos()
   {
	   $this->db->select("*");
	   $this->db->where('servico_visivel', 1);
	   return $this->db->get('servicos')->result();
   }

   public function financeiro($dados)
   {
	 $this->db->insert('financeiro', $dados);

	 if ($this->db->affected_rows() == '1')
	 {     
	   return true;
	 }
	 
	 return FALSE; 
   }


    public function listarSituacaoOsId($id)
    {
	   $this->db->select("*");
	   $this->db->join('usuarios ',' usuarios.`usuario_id` = os_situacao.`usuarios_id` ');
	   $this->db->where('situacao_os_id', $id);
	   $this->db->order_by('situacao_id','DESC');
	   
	   return $this->db->get('os_situacao')->result();
    }


    	public function updateAssinatura($id, $valorAcinatura = null)
	{
		$this->db->where($this->chave, $id);
		$this->db->where($this->visivel, 1);
		return $this->db->update($this->tabela, ['os_assinatura' => $valorAcinatura]);
	}








}