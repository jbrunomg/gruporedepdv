<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categoriaproduto_model extends CI_Model {
    
    public $tabela  = "categoria_produto";
    public $visivel = "categoria_prod_visivel";
    public $chave   = "categoria_prod_id";

    public function listar()
    {
        $this->db->select("*");
        $this->db->where($this->visivel, 1);
        return $this->db->get($this->tabela)->result();
    }

    public function listarId($id)
    {
        $this->db->select("*");
        $this->db->where($this->chave, $id);
        $this->db->where($this->visivel, 1);
        return $this->db->get($this->tabela)->result();
    }


    public function adicionar($dados)
    {
        $sql = "SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE '%_".GRUPOLOJA."_%' ";
        $bases = $this->db->query($sql)->result();;


        foreach ($bases as $b) {
            $this->db->insert($b->schema_name.'.'.$this->tabela, $dados);
        }

        if ($this->db->affected_rows() == '1')
        {
            return TRUE;
        }
            
        return FALSE;
 
    }

    public function editar($dados, $id)
    {
        $sql = "SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE '%_".GRUPOLOJA."_%' ";
        $bases = $this->db->query($sql)->result();;
        
        foreach ($bases as $b) {
            $this->db->where($this->chave,$id); 
            $this->db->update($b->schema_name.'.'.$this->tabela,$dados);

        }

        if($this->db->trans_status() === true){
            $this->db->trans_commit();
            return true;
        }else{
            $this->db->trans_rollback();
            return false;
        }
    }

    public function excluir($dados, $id)
    {
        $sql = "SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE '%_".GRUPOLOJA."_%' ";
        $bases = $this->db->query($sql)->result();;
        
        foreach ($bases as $b) {
            $this->db->where($this->chave,$id); 
            $this->db->update($b->schema_name.'.'.$this->tabela,$dados);

        }

        if($this->db->trans_status() === true){
            $this->db->trans_commit();
            return true;
        }else{
            $this->db->trans_rollback();
            return false;
        }
    }

    public function visualizar($id)
    {
        $this->db->select("*");
        $this->db->where($this->chave, $id);
        $this->db->where($this->visivel, 1);
        return $this->db->get($this->tabela)->result();
    }

    public function getAllFilias()
    {
        $sql = "SHOW DATABASES";

        $result = $this->db->query($sql);

        $bases = array();
        if ($result->num_rows() > 0) {
            foreach (($result->result_array()) as $row) {
              if (strpos($row['Database'], '_'.GRUPOLOJA.'_') !== false && $row['Database'] !=
                'wdm_clientes') {
                $lojas = explode("_".GRUPOLOJA."_", $row['Database']);
                 
                   $bases[]['lojas'] = $lojas[1];
                
              }
            }

          return $bases;
        }
        return FALSE;
    }

    public function listarProdutoId($id)
    {
        $this->db->select("*");
        $this->db->join('categoria_produto','produto.`produto_categoria_id` = categoria_produto.`categoria_prod_id`' );
        $this->db->where('produto_categoria_id', $id);
        $this->db->where('produto_visivel', 1);
        $this->db->where('produto_estoque >', 0);
        return $this->db->get('produto')->result();
    }
}
