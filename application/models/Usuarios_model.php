<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios_model extends CI_Model {
	
	public $tabela  = "usuarios";
	public $visivel = "usuario_visivel";
	public $chave   = "usuario_id";

	public function listar()
	{
		$this->db->select("*");
		$this->db->join('perfis','`usuario_perfil` = `perfil_id`');
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}

	public function listarId($id)
	{
		$this->db->select("*");
		$this->db->where($this->chave, $id);
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}


	public function adicionar($dados)
	{
		$this->db->insert($this->tabela, $dados);

		if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE; 
	}

	public function editar($dados, $id)
	{
		$this->db->where($this->chave,$id);		
		
		if($this->db->update($this->tabela,$dados))
		{
			return true;
		}

		return false;
	}

	public function excluir($dados, $id)
	{
		$this->db->where($this->chave,$id);
		$this->db->update($this->tabela,$dados);
		
		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}

	public function visualizar($id)
	{
		$this->db->select("*");
		$this->db->where($this->chave, $id);
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}

	public function pegarEstados()
	{
		$this->db->select('*');
		return $this->db->get('estados')->result();
	}

	public function pegarPerfil($value='')
	{
		$this->db->select('*');
		$this->db->where('perfil_visivel', 1);
		return $this->db->get('perfis')->result();
	}


	public function venda($id)
  	{
    
    $query =  "  SELECT COUNT(*) AS total FROM `vendas`
	   WHERE `usuarios_id` = {$id}
	   AND `venda_visivel` = 1;  ";
 
    return $this->db->query($query)->result();
  }

	public function grupo($id)
  	{
     
    $query =  "   SELECT COUNT(`itens_de_vendas`.`idItens`) AS qtd, `categoria_prod_descricao` FROM `itens_de_vendas`
	   INNER JOIN `vendas` ON `itens_de_vendas`.`vendas_id` = vendas.`idVendas`
	   INNER JOIN `produto` ON `itens_de_vendas`.`produtos_id` = `produto`.`produto_id`
	   INNER JOIN  `categoria_produto` ON `produto`.`produto_categoria_id` = `categoria_produto`.`categoria_prod_id`
	    WHERE `usuarios_id` =  {$id}
	   AND `venda_visivel` = 1
	   GROUP BY `categoria_produto`.`categoria_prod_id`	
	   ORDER BY qtd DESC
	   LIMIT 1 ";
 
    return $this->db->query($query)->result();
  }


	public function ranck()
  	{

    $loja = BDCAMINHO; $loja = explode("_", $loja);

    $query =  "SELECT ROUND(SUM(`itens_de_vendas`.`subTotal`),2) AS total, `usuarios`.`usuario_nome` AS vendedor
                FROM vendas 
                LEFT JOIN `usuarios`
                ON vendas.`usuarios_id` = `usuarios`.`usuario_id`
                LEFT JOIN `itens_de_vendas`
                ON `vendas`.`idVendas` = `itens_de_vendas`.`vendas_id`
                LEFT JOIN `financeiro`
                ON `vendas`.`idVendas` = `financeiro`.`vendas_id`
                WHERE 
                `venda_visivel` = 1
                AND `itens_de_vendas`.`filial` LIKE '%$loja[2]%' ESCAPE '!'
                AND `financeiro`.`financeiro_descricao` LIKE '%$loja[2]%' ESCAPE '!'
                GROUP BY vendas.`usuarios_id`
                ORDER BY total DESC
                ";
    // echo "<pre>";
    // exit($query);
        return $this->db->query($query)->result();
  }

  public function pegarAniversariantesMes(){
	$this->db->select("usuario_data_nascimento,usuario_nome,usuario_imagem");
	$this->db->where('MONTH(`usuario_data_nascimento`) =  MONTH(CURRENT_DATE())');
	$this->db->where($this->visivel, 1);
	$this->db->order_by("DAY(usuario_data_nascimento) ASC");
	$this->db->limit(8);
	return $this->db->get($this->tabela)->result();
  }


}