<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Devolucao_model extends CI_Model {

	public $tabela  = "devolucao";
	public $visivel = "devolucao_visivel";
	public $chave   = "idDevolucao";

 public function listar()
	{
		$this->db->select("*");
		$this->db->join('fornecedores ',' devolucao.`fornecedor_id` = fornecedores.`fornecedor_id` ');	
		$this->db->join('usuarios ',' devolucao.`usuarios_id` = usuarios.`usuario_id` ');				
		$this->db->where($this->visivel, 1);
		

		return $this->db->get($this->tabela)->result();
	}

 public function listarId($id)
	{
		$this->db->select("*");		
		$this->db->where($this->chave, $id);
		$this->db->join('fornecedores ',' devolucao.`fornecedor_id` = fornecedores.`fornecedor_id` ');	
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}

 public function adicionar($dados)
	{
		$this->db->insert($this->tabela, $dados);

		if ($this->db->affected_rows() == '1')
		{
			//return TRUE;
			return $this->db->insert_id($this->tabela);
		}
		
		return FALSE; 
	}

 public function editar($dados, $id)
	{
		$this->db->where($this->chave,$id);		
		
		if($this->db->update($this->tabela,$dados))
		{
			return true;
		}

		return false;
	}

 public function pegarProdutos($grupo)
	{
		$this->db->select('*');
		$this->db->where('produto_visivel',1);
		$this->db->where('produto_categoria_id',$grupo);
		return $this->db->get('produto')->result();
	}
 
 public function pegarProdutosID($produto)
	{
		$this->db->select('*');
		$this->db->where('produto_visivel',1);
		$this->db->where('produto_id',$produto);
		return $this->db->get('produto')->result();

	}

 public function add($table,$data,$returnId = false){

        $this->db->insert($table, $data);         

        if ($this->db->affected_rows() == '1')

		{
            if($returnId == true){

                return $this->db->insert_id($table);
            }
			return TRUE;
		}		
		return FALSE;      
    }

  public function addTotal($id, $subTotal){
     
       $sql = "UPDATE devolucao set valorTotal = valorTotal + ? WHERE idDevolucao = ?";
        $this->db->query($sql, array($subTotal, $id));    

        if ($this->db->affected_rows() == '1'){
			return TRUE;
		}		
		return FALSE;      
    }

 public function pegarFornecedor()
	{
		$this->db->select('*');
		$this->db->where('fornecedor_visivel',1);
		return $this->db->get('fornecedores')->result();
	}

 // public function pegarCategoriaProd()
	// {
	// 	$this->db->select('*');
	// 	$this->db->where('categoria_prod_visivel',1);
	// 	return $this->db->get('categoria_produto')->result();
	// }

	public function pegarCategoriaProd($ids)
	{

		$sql = 'SELECT * FROM `categoria_produto` WHERE `categoria_prod_visivel` = 1';

		if ($ids) {
		  $sql .= ' AND `categoria_prod_id` IN('.$ids.')';
		}
		 

        return $this->db->query($sql)->result();     
	}

	public function consultaTipo($id)
	{
		$this->db->where($this->chave, $id);
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}

 public function getProdutos($id = null){

        $this->db->select('IF(`produto_estoque` >= `quantidade`,"success","danger")AS situacao_estoque, itens_de_devolucao.*, produto.*');

        $this->db->from('itens_de_devolucao');

        $this->db->join('produto','produto.produto_id = itens_de_devolucao.produtos_id');

        $this->db->where('devolucao_id',$id);
        $this->db->where('produto.produto_visivel',1);

        return $this->db->get()->result();

    }


  public function Devolucao($dados,$id)
	{	

		$this->db->where($this->chave,$id);
		
		
		if($this->db->update($this->tabela,$dados))
		{
			return true;
		}

		return false;

	}

 public function validacao($id){

        $this->db->select('quantidade, produtos_id');
        $this->db->from('itens_de_devolucao');
        $this->db->where('devolucao_id',$id);
        
        foreach ($this->db->get()->result() as $row){

            $sqlConsultar = "SELECT IF(`produto_estoque` >= ".$row->quantidade.", 'Y','N') AS quantidade FROM `produto` WHERE `produto_id` = ".$row->produtos_id." ";

            $resultado = $this->db->query($sqlConsultar)->result();

             if ($resultado[0]->quantidade == 'N') {

                return false;
                break;         
             }

        }

      return true;
   }



  public function delete($table,$fieldID,$ID){

        $this->db->where($fieldID,$ID);

        $this->db->delete($table);

        if ($this->db->affected_rows() == '1')

		{
			return TRUE;

		}

		return FALSE;        

    }  

  public function retirarValor($subTotal, $idDevolucao){

        $sql = "UPDATE devolucao set valorTotal = valorTotal - ? WHERE idDevolucao = ?";
        $this->db->query($sql, array($subTotal, $idDevolucao));     

        if ($this->db->affected_rows() == '1')

		{
			return TRUE;

		}

		return FALSE;        

    } 

  public function visualizar($id)
	{

		$query =  "SELECT SUM(`quantidade`) AS quantidadee,
				devolucao.*, 
				fornecedores.*,
				usuarios.*,
				itens_de_devolucao.*,
				produto.*   
			FROM devolucao
					LEFT JOIN `fornecedores`
					ON devolucao.`fornecedor_id` = fornecedores.`fornecedor_id`
					LEFT JOIN `usuarios`
					ON devolucao.`usuarios_id` = usuarios.`usuario_id`
				    LEFT JOIN `itens_de_devolucao`
					ON devolucao.`idDevolucao` = itens_de_devolucao.`devolucao_id`
					LEFT JOIN `produto`
					ON itens_de_devolucao.`produtos_id` = produto.`produto_id`
    
		      WHERE devolucao.`devolucao_visivel` = 1 
		      AND devolucao.`idDevolucao` = $id
		      GROUP BY `produtos_id`
		      ORDER BY produto.produto_descricao ";
		// echo "<pre>";
		// exit($query);
        return $this->db->query($query)->result();

   }

   public function emitente()
	{
		$this->db->select("*");

		return $this->db->get('emitente')->result();
	}

   public function excluir($devolucao_visivel, $id)
	{

		$query =  "UPDATE `devolucao` SET `devolucao_visivel` = $devolucao_visivel WHERE `idDevolucao` = '$id'";
		// echo "<pre>";
		// exit($query);
        return $this->db->query($query);

   }

}