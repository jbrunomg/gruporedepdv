<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controlecotacao_model extends CI_Model {

    public function adicionar($dados)
    {
        $this->db->insert('cotacao_compra', $dados);

        if ($this->db->affected_rows() == '1')
        {
            //return TRUE;
            return $this->db->insert_id('cotacao_compra');
        }
        
        return FALSE; 
    }

    public function listarCategoriaProduto()
    {
        $this->db->select("*");
        $this->db->where('categoria_prod_visivel', 1);

        return $this->db->get('categoria_produto')->result();
    }

    public function prepararItens($categoria_id, $id_compra,$dataInicio, $dataFim)
    {  
        $sql = "SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE '%_".GRUPOLOJA."_%' ";
        $bases =  $this->db->query($sql)->result();
        $sql = '';



        $sql = 'DROP TABLE IF EXISTS cotacao; ';
        $this->db->query($sql);
        $sql = '';

        $sql =  $sql. 'CREATE TABLE cotacao ';
    
        foreach ($bases as $b) {
           
        $sql = $sql. ' SELECT `produtos_id`,`quantidade` FROM '.$b->schema_name.'.`simulacao_compra` 
            INNER JOIN '.$b->schema_name.'.`itens_simulacao_compra` 
                ON  `simulacao_compra`.`simulacao_compra_id` =  `itens_simulacao_compra`.`simulacao_compra_id`
            LEFT JOIN `produto` 
                ON `produto`.`produto_id` = `itens_simulacao_compra`.`produtos_id` ';

        if ($categoria_id == 0) { // TODOS PRODUTOS
            $sql = $sql. ' WHERE `simulacao_compra_data`  BETWEEN "'.$dataInicio.'" AND "'.$dataFim.'" AND simulacao_compra_visivel = 1 ';              
        }else{
            $sql = $sql. ' WHERE `simulacao_compra_data`  BETWEEN "'.$dataInicio.'" AND "'.$dataFim.'" AND produto_categoria_id ='.$categoria_id.' AND simulacao_compra_visivel = 1 ';            
        };

        $sql = $sql.' UNION ALL ';  

        };

        $sql = substr($sql,0,-10);

        // var_dump($sql);die();

        $this->db->query($sql);
        $sql = '';


        $sql = ' INSERT INTO itens_cotacao_compra (`cotacao_compra_produtos_id`, cotacao_compra_id, cotacao_compra_quantidade )
            SELECT produtos_id, '.$id_compra.', SUM(`quantidade`) AS quantidade FROM cotacao GROUP BY `produtos_id`; ';
               
        return $this->db->query($sql);
    }



    public function listarId($id)
    {
        $sql = 'SELECT 
                    `produto`.`produto_id`,
                    `produto`.`produto_descricao`, 
                    `produto`.`produto_preco_custo`,                    
                    `itens_cotacao_compra`.`cotacao_compra_quantidade`,
                    `cotacao_compra`.`cotacao_compra_observacao`,                    
                    `categoria_produto`.`categoria_prod_descricao` AS categoria,
                    `usuario_nome` as funcionario,
                    DATE_FORMAT(cotacao_compra.`cotacao_compra_simulacao_data_inicio`, "%d/%m/%Y") AS dataInicio,
                    DATE_FORMAT(cotacao_compra.`cotacao_compra_simulacao_data_final`,"%d/%m/%Y") AS dataFim                   
                FROM produto

                INNER JOIN itens_cotacao_compra
                    ON itens_cotacao_compra.`cotacao_compra_produtos_id` = `produto`.`produto_id`
                INNER JOIN cotacao_compra
                    ON `cotacao_compra`.`cotacao_compra_id` = `itens_cotacao_compra`.`cotacao_compra_id`
                LEFT JOIN categoria_produto
                    ON cotacao_compra.`cotacao_compra_categoria_id` = categoria_produto.`categoria_prod_id`
                INNER JOIN usuarios
                    ON usuarios.`usuario_id` = cotacao_compra.`cotacao_compra_usuarios_id`         

                WHERE itens_cotacao_compra.`cotacao_compra_id` = '.$id.'
                AND `itens_cotacao_compra`.`cotacao_compra_quantidade` <> 0
                AND produto.`produto_visivel` = 1 ';

        return $this->db->query($sql)->result();
    }

    public function listarControlecotacao()
    {
        $sql = 'SELECT
                    cotacao_compra.`cotacao_compra_id` AS id,
                    DATE_FORMAT(cotacao_compra.`cotacao_compra_simulacao_data_inicio`, "%d/%m/%Y") AS data_inicio,
                    DATE_FORMAT(cotacao_compra.`cotacao_compra_simulacao_data_final`,"%d/%m/%Y") AS data_final,
                    cotacao_compra.`cotacao_compra_situacao` AS situacao,
                    categoria_produto.`categoria_prod_descricao` AS categoria,
                    COUNT(itens_cotacao_compra.`cotacao_compra_produtos_id`) AS produtos_totais,
                    COUNT(CASE WHEN itens_cotacao_compra.`cotacao_compra_quantidade` > 0 THEN itens_cotacao_compra.`cotacao_compra_produtos_id` END) produtos_adicionados

                FROM cotacao_compra

                LEFT JOIN categoria_produto
                ON cotacao_compra.`cotacao_compra_categoria_id` = categoria_produto.`categoria_prod_id`

                INNER JOIN itens_cotacao_compra
                ON cotacao_compra.`cotacao_compra_id` = itens_cotacao_compra.`cotacao_compra_id`

                WHERE cotacao_compra.`cotacao_compra_visivel` = 1

                GROUP BY cotacao_compra.`cotacao_compra_id`
                ORDER BY id DESC';

        return $this->db->query($sql)->result();
    }

    public function consultarControlecotacao($id)
    {
        $this->db->select('*, categoria_produto.categoria_prod_descricao AS categoria_descricao');
        $this->db->where('cotacao_compra_visivel', 1);
        $this->db->where('cotacao_compra_id', $id);

        $this->db->join('categoria_produto', 'cotacao_compra.cotacao_compra_categoria_id = categoria_produto.categoria_prod_id');

        $result = $this->db->get('cotacao_compra')->result();

        if($result){
            return $result[0];
        } else {
            return false;
        }
    }

    public function excluir($dados, $id)
    {
        $this->db->where('cotacao_compra_id',$id);

        $this->db->update('cotacao_compra',$dados);
        
        if($this->db->affected_rows() == '1')
        {
            return true;
        }

        return false;
    }


    public function editar($tabela, $chave, $dados, $id)
    {
        $this->db->where($chave,$id);     
        
        if($this->db->update($tabela,$dados))
        {
            return true;
        }

        return false;
    }


    public function listarFornecedorCotacao($idCotacao)
    {

        $this->db->select('cotacao_compra_fornecedor_id');
        $this->db->from('cotacao_compra');
        $this->db->where(' `cotacao_compra_id` = ',$idCotacao);
        return $this->db->get()->result();


        
        // $this->db->select('*');
        // $this->db->from('fornecedores');
        // $this->db->where(' `fornecedor_id` IN', '(select `cotacao_compra_fornecedor_id` from `cotacao_compra` where `cotacao_compra_id` = '.$idCotacao.')', false);
        // return $this->db->get()->result();

    }

    public function listarFornecedor($lista)
    {
        
        $sql = " SELECT * FROM `fornecedores`
            WHERE `fornecedor_visivel` = 1 AND `fornecedor_id` IN ({$lista});";

            
        return $this->db->query($sql)->result(); 
    }

    public function prepararFornecedorCotacao($idCotacao,$idforn,$idgrupo)
    {
        $sql = ' INSERT INTO `cotacao_fornecedor`(`cotacao_fornecedor_forn_id`,`cotacao_fornecedor_produto_id`,`cotacao_fornecedor_contacao_id`,`cotacao_fornecedor_quantidade` ) 
        SELECT  '.$idforn.', produto.`produto_id`, itens_cotacao_compra.`cotacao_compra_id`, `cotacao_compra_quantidade`
                 FROM `itens_cotacao_compra`
                 INNER JOIN `produto` ON `itens_cotacao_compra`.`cotacao_compra_produtos_id` = `produto`.`produto_id`
                 WHERE `cotacao_compra_id` = '.$idCotacao.' AND `produto_categoria_id` IN ('.$idgrupo.');';

        // var_dump($sql);die();         

            return $this->db->query($sql);     

    }

    public function listarParticipanteCotacao($id)
    {
       $sql = ' SELECT 
            ct.`cotacao_fornecedor_forn_id`,
           concat("#",ct.`cotacao_fornecedor_forn_id`," - ",`fornecedor_nome`) AS fornecedor_nome,
            `fornecedor_email`, 
           SUM(ct.`cotacao_fornecedor_valor`) AS total ,
           COUNT(ct.`cotacao_fornecedor_produto_id`) AS qtd, 
        (SELECT COUNT(*) FROM cotacao_fornecedor  WHERE `cotacao_fornecedor_valor` IS NOT NULL AND `cotacao_fornecedor_contacao_id`= '.$id.' AND cotacao_fornecedor.`cotacao_fornecedor_forn_id` = ct.`cotacao_fornecedor_forn_id`  ) AS feito
        FROM cotacao_fornecedor AS ct 
        INNER JOIN  `fornecedores` ON ct.`cotacao_fornecedor_forn_id` = fornecedores.`fornecedor_id`
        WHERE ct.`cotacao_fornecedor_contacao_id`= '.$id.' GROUP BY ct.`cotacao_fornecedor_forn_id` ';

        // var_dump($sql);die();  

        return $this->db->query($sql)->result();
    }


    public function listarCotacaoGeral($fornecedores,$id)
    {
       $forn = '';

       $sql = 'SELECT DISTINCT produto_descricao as produto,
         produto_preco_custo, 
      --  `itens_cotacao_compra`.`cotacao_compra_quantidade` AS quantidade,
        `cotacao_fornecedor`.`cotacao_fornecedor_quantidade` AS quantidade,'; 
       foreach ($fornecedores as $f) {

        $sql = $sql.' (SELECT  CASE
        WHEN cotacao_fornecedor_valor IS NULL THEN 9999
        WHEN cotacao_fornecedor_valor = 0.00 THEN 9999
        ELSE cotacao_fornecedor_valor
        END
        FROM cotacao_fornecedor AS forn'.$f->cotacao_fornecedor_forn_id.' 
        WHERE cotacao_fornecedor_forn_id = '.$f->cotacao_fornecedor_forn_id.'  AND `cotacao_fornecedor_contacao_id` = '.$id.' AND forn'.$f->cotacao_fornecedor_forn_id.'.`cotacao_fornecedor_produto_id` = cotacao_fornecedor.`cotacao_fornecedor_produto_id` ) AS fornecedor_'.$f->cotacao_fornecedor_forn_id.'_valor, ';

        $forn = $forn. 'fornecedor_'.$f->cotacao_fornecedor_forn_id.'_valor, ';

        };    


       $sql = $sql.' (SELECT LEAST('.$forn;

      

       $sql = substr($sql,0,-2);

       //Left a mas

       $sql = $sql.' )) AS minimo 
                        FROM `cotacao_fornecedor`
                        INNER JOIN `produto` ON cotacao_fornecedor.`cotacao_fornecedor_produto_id` = produto.`produto_id`

                      --  LEFT JOIN `cotacao_compra`
                      --   ON cotacao_fornecedor.`cotacao_fornecedor_contacao_id` = cotacao_compra.`cotacao_compra_id`
                        
                      --  LEFT JOIN `itens_cotacao_compra` 
                      --   ON cotacao_compra.`cotacao_compra_id` = `itens_cotacao_compra`.`cotacao_compra_id`

                        WHERE `cotacao_fornecedor_contacao_id` = '.$id.' order by produto_descricao ; ';


        // var_dump($sql);die();                

                     

        return $this->db->query($sql)->result();

    }


    public function selecionarFornecedor($grupo)
    {
        $where = "FIND_IN_SET('".$grupo."', fornecedor_grupoprod_id)"; 

        $this->db->select('`fornecedor_id`,`fornecedor_nome`');
        if ($grupo != '0') {
          $this->db->where( $where ); 
        }
        $this->db->where('fornecedor_visivel',1);

        return $this->db->get('fornecedores')->result();
    }




   
}