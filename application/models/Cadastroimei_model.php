<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cadastroimei_model extends CI_Model {

	// public $tabela  = "movimentacao_produtos";
	// public $visivel = "movimentacao_produto_visivel";
	// public $chave   = "movimentacao_produto_id";

	public function listar()
	{
		$this->db->select("*, ROUND(SUM(`item_movimentacao_produto_custo` * `item_movimentacao_produto_quantidade`), 2) AS total, SUM(`item_movimentacao_produto_quantidade`) AS quantPr, (SELECT COUNT(`imei_valor`) FROM itens_de_imei  WHERE `itens_de_imei`.`movimentacao_id` = `movimentacao_produtos`.`movimentacao_produto_id` AND `imei_visivel` = 1) AS QuantIm");
		$this->db->join('itens_movimentacao_produtos','`movimentacao_produtos`.`movimentacao_produto_id` = `itens_movimentacao_produtos`.`item_movimentacao_produto_movimentacao_produto_id`');
        $this->db->where('movimentacao_produto_visivel', 1);
		$this->db->where('item_movimentacao_produto_visivel', 1);
		$this->db->where('movimentacao_produto_imei', 1);
        $this->db->group_by('movimentacao_produto_id');
		$this->db->order_by("movimentacao_produto_id", "desc");

		return $this->db->get('movimentacao_produtos')->result();
	}

    public function listarId($id, $tipoSaida)
	{
		$this->db->select("*");
		if ($tipoSaida == 'fornecedor') {
		  $this->db->join('fornecedores','fornecedores.fornecedor_id = movimentacao_produtos.movimentacao_fornecedor_id');
		}
		$this->db->where('movimentacao_produto_id', $id);
		$this->db->where('movimentacao_produto_visivel', 1);
		return $this->db->get('movimentacao_produtos')->result();
	}

    public function getProdutosList($idMovimentacao){

        $this->db->select('*');
        $this->db->from('itens_de_imei');
        $this->db->join('produto','produto.produto_id = itens_de_imei.produtos_id');
        $this->db->where('movimentacao_id',$idMovimentacao);
        $this->db->where('imei_visivel', 1);
        $this->db->order_by('`itens_de_imei`.`imei_id`');

        return $this->db->get()->result();

    }

    public function pegarCategoriaProd($idMovimentacao)
	{

        $this->db->select('`categoria_produto`.`categoria_prod_id`,`categoria_produto`.`categoria_prod_descricao`');

        $this->db->from('itens_movimentacao_produtos');

        $this->db->join('produto','produto.produto_id = itens_movimentacao_produtos.item_movimentacao_produto_produto_id');
        $this->db->join('categoria_produto','categoria_produto.categoria_prod_id = produto.produto_categoria_id');
        $this->db->join('movimentacao_produtos',
        	'movimentacao_produtos.movimentacao_produto_id = itens_movimentacao_produtos.item_movimentacao_produto_movimentacao_produto_id');

        $this->db->where('item_movimentacao_produto_movimentacao_produto_id',$idMovimentacao);
        $this->db->where('item_movimentacao_produto_visivel', 1);
        $this->db->where('movimentacao_produtos.movimentacao_produto_visivel', 1);
        $this->db->group_by('categoria_produto.`categoria_prod_id`');

        return $this->db->get()->result();

	}

    public function getProdutosListGrupo($idCategoria, $idMovimentacao)
    {

        $this->db->select(
            'produto.`produto_descricao` AS descricaoProduto,
            itens_movimentacao_produtos.`item_movimentacao_produto_produto_id` AS idProduto,
            itens_movimentacao_produtos.`item_movimentacao_produto_quantidade` AS quantidadeProduto'
        );

        $this->db->from('itens_movimentacao_produtos');

        $this->db->join('produto','produto.produto_id = itens_movimentacao_produtos.item_movimentacao_produto_produto_id');
        $this->db->join('categoria_produto','categoria_produto.categoria_prod_id = produto.produto_categoria_id');
        $this->db->join('movimentacao_produtos',
        	'movimentacao_produtos.movimentacao_produto_id = itens_movimentacao_produtos.item_movimentacao_produto_movimentacao_produto_id');

        $this->db->where('item_movimentacao_produto_movimentacao_produto_id',$idMovimentacao);
        $this->db->where(' `categoria_produto`.`categoria_prod_id`',$idCategoria);
        $this->db->where('item_movimentacao_produto_visivel', 1);
        $this->db->where('movimentacao_produtos.movimentacao_produto_visivel', 1);
        $this->db->order_by('produto.`produto_descricao`');

        return $this->db->get()->result();

	}


    public function getImeisList($imei)
    {

        $this->db->select('*');
        $this->db->from('itens_de_imei');
        $this->db->where('imei_valor',"$imei");
        $this->db->where('imei_visivel', '1');

        return $this->db->get()->result();

	}

    public function add($table,$data){

        $this->db->insert($table, $data);

        if ($this->db->affected_rows() == '1'){
			return TRUE;
		}
		return FALSE;
    }

	public function consultaQuantidade($idProduto, $idMovimento)
	{
        $this->db->select('count(*)');
		$this->db->from('itens_de_imei');
	    $this->db->where('itens_de_imei.`movimentacao_id`',$idMovimento);
        $this->db->where('itens_de_imei.`produtos_id`',$idProduto);
        $this->db->where('itens_de_imei.`imei_visivel`',1);
		$num_results = $this->db->count_all_results();
		return $num_results;

	}

    public function excluir($pedido_visivel, $id)
	{
		// $query =  "UPDATE `itens_de_imei` SET `imei_visivel` = $pedido_visivel WHERE `imei_id` = '$id'";
        $query =  "DELETE FROM `itens_de_imei` WHERE  `imei_id` = '$id'";
        return $this->db->query($query);

   }



   function edit($table,$data,$fieldID,$ID)
   {
    $this->db->where($fieldID,$ID);
    $this->db->update($table, $data);

    return $this->db->affected_rows() >= 0;

    }


    public function getImeisListProduto($id)
    {

        $this->db->select('*');
        $this->db->from('itens_de_imei');
        $this->db->where('produtos_id',$id);
        $this->db->where('vendas_id is null');
        $this->db->where('imei_visivel', '1');

        return $this->db->get()->result();

	}

}
