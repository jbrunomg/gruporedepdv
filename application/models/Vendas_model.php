<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendas_model extends CI_Model {
	
	public $tabela  = "vendas";
	public $visivel = "venda_visivel";
	public $chave   = "idVendas";

 public function listar($limit, $start, $search) 
	{
		$this->db->select('*, (select count(*) from produto_avarias pa where pa.venda_id = idVendas) as avaria');
		$this->db->from($this->tabela);
		$this->db->join('clientes ',' vendas.`clientes_id` = clientes.`cliente_id` ');	
		$this->db->join('usuarios ',' vendas.`usuarios_id` = usuarios.`usuario_id` ');	
		$this->db->where($this->visivel, 1);
		if (!empty($search)) {
			$this->db->group_start();
			$this->db->like('usuario_nome', $search); // Substitua 'seu_campo' pelo campo que deseja pesquisar
			$this->db->or_like('cliente_nome', $search); 
			$this->db->or_like('idVendas', $search); 
			$this->db->or_like("DATE_FORMAT(dataVenda, '%d/%m/%Y')", $search);
			$this->db->or_like('valorTotal', $search);
			$this->db->group_end();
		}
		$this->db->order_by("idVendas", 'desc');
		$this->db->limit($limit, $start);
	
		// Execute a consulta e retorne os resultados
		$resultados = $this->db->get()->result();
	
		$this->db->from($this->tabela);
		$this->db->join('clientes ',' vendas.`clientes_id` = clientes.`cliente_id` ');	
		$this->db->join('usuarios ',' vendas.`usuarios_id` = usuarios.`usuario_id` ');	
		$this->db->where($this->visivel, 1);
		if (!empty($search)) {
			$this->db->group_start();
			$this->db->like('usuario_nome', $search); // Substitua 'seu_campo' pelo campo que deseja pesquisar
			$this->db->or_like('cliente_nome', $search); 
			$this->db->or_like('idVendas', $search); 
			$this->db->or_like("DATE_FORMAT(dataVenda, '%d/%m/%Y')", $search);
			$this->db->or_like('valorTotal', $search);

			$this->db->group_end();
		}
		$total_registros = $this->db->count_all_results();
		
		return array('dados' => $resultados, 'total_registros' => $total_registros);
	}

 public function editar($dados, $financeiro, $id)
	{
		$this->db->where($this->chave,$id);		
		
		if($this->db->update($this->tabela,$dados))
		{
			$this->db->where('vendas_id',$id);		
			$this->db->update('financeiro',$financeiro);
			return true;
		}

		return false;
	}

  public function visualizar($id,$visivel = NULL)
	{
        if ($visivel) {
        	$where =  'vendas.`idVendas` ='. $id;		
       	} else {
       		$where = 'vendas.`venda_visivel` = 1 AND vendas.`idVendas` ='. $id;
       	}		

		$query =  "SELECT *, ROUND((itens_de_vendas.`subTotal` / quantidade), 2) as valor_unitario FROM vendas
					LEFT JOIN `clientes`
					ON vendas.`clientes_id` = clientes.`cliente_id`
					LEFT JOIN `usuarios`
					ON vendas.`usuarios_id` = usuarios.`usuario_id`
				    LEFT JOIN `itens_de_vendas`
					ON vendas.`idVendas` = itens_de_vendas.`vendas_id`
					LEFT JOIN `produto`
					ON itens_de_vendas.`produtos_id` = produto.`produto_id`
    
		      WHERE $where 
		      order by produto.`produto_descricao` ";
		// echo "<pre>";
		// exit($query);
        return $this->db->query($query)->result();

   }

   public function financeiroOLD($id)
	{
		$this->db->select("*");
		$this->db->where('vendas_id',$id);

		return $this->db->get('financeiro')->result();
	}


	public function financeiro($id)
	{	
		$this->db->select("*");
		$this->db->where('vendas_id',$id);
		return $this->db->get('financeiro')->result();
	}

    public function emitente()
	{
		$this->db->select("*");

		return $this->db->get('emitente')->result();
	}

    public function pegarCategoriaProd()
	{
		$this->db->select('*');
		$this->db->where('categoria_prod_visivel',1);
		return $this->db->get('categoria_produto')->result();
	}

    public function listarId($id)
	{
		$this->db->select("*");		
		$this->db->where($this->chave, $id);
		$this->db->join('clientes ',' vendas.`clientes_id` = clientes.`cliente_id` ');	
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}

    public function getProdutos($id = null){

        $this->db->select('itens_de_vendas.*, produto.*');

        $this->db->from('itens_de_vendas');

        $this->db->join('produto','produto.produto_id = itens_de_vendas.produtos_id');

        $this->db->where('vendas_id',$id);
        $this->db->where('produto.produto_visivel',1);

        return $this->db->get()->result();

    }

    public function estoqueRetirada($quantidade, $produto)
	{
		try {
			$this->db->trans_begin();
			$query =  "UPDATE produto set produto_estoque = produto_estoque - ? WHERE produto_id = ?";
			$this->db->query($query, array($quantidade, $produto));
	
			if ($this->db->affected_rows() >= '1')
			{
				$this->db->trans_commit();
				return TRUE;
			}

			$this->db->trans_rollback();
			return FALSE;        
		} catch (Exception $e) {
			$this->db->trans_rollback();
			return FALSE;        
		}
    }   


	public function avaria($idVendas, $produto, $quantidade, $motivo, $precoCusto, $precoVendido, $observacao, $usuario, $fornecedor_id, $antigoImei = null, $novoImei = null){
		try {
			$this->db->trans_begin();
			$this->db->set('venda_id', $idVendas);
			$this->db->set('produto_id', $produto);
			$this->db->set('produto_avarias_quantidade', $quantidade);
			$this->db->set('produto_avaria_motivo', $motivo);
			$this->db->set('produto_avaria_preco_custo', $precoCusto);
			$this->db->set('produto_avaria_preco_vendido', $precoVendido);		
			$this->db->set('produto_avaria_cadastro', date('Y-m-d'));
			$this->db->set('produto_avaria_observacao', $observacao);
			$this->db->set('usuario_id', $usuario);
			$this->db->set('fornecedor_id', $fornecedor_id);
	
			$this->db->set('produto_avaria_antigo_imei', $antigoImei);
			$this->db->set('produto_avaria_novo_imei', $novoImei);
			$this->db->insert('produto_avarias');
	
			if ($this->db->affected_rows() >= '1')
			{
				$this->db->trans_commit();
				return TRUE;
			}

			$this->db->trans_rollback();
			return FALSE;        
		} catch (Exception $e) {
			$this->db->trans_rollback();
			return FALSE;        
		}
    }

	public function avariaId($idVendas, $produto, $quantidade, $motivo, $precoCusto, $precoVendido, $observacao, $usuario, $fornecedor_id, $antigoImei = null, $novoImei = null){

		$this->db->set('venda_id', $idVendas);
		$this->db->set('produto_id', $produto);
		$this->db->set('produto_avarias_quantidade', $quantidade);
		$this->db->set('produto_avaria_motivo', $motivo);
		$this->db->set('produto_avaria_preco_custo', $precoCusto);
		$this->db->set('produto_avaria_preco_vendido', $precoVendido);        
		$this->db->set('produto_avaria_cadastro', date('Y-m-d'));
		$this->db->set('produto_avaria_observacao', $observacao);
		$this->db->set('usuario_id', $usuario);
		$this->db->set('fornecedor_id', $fornecedor_id);
	
		$this->db->set('produto_avaria_antigo_imei', $antigoImei);
		$this->db->set('produto_avaria_novo_imei', $novoImei);
		$this->db->insert('produto_avarias');
	
		if ($this->db->affected_rows() == 1) {
			return $this->db->insert_id(); // Retorna o ID do registro inserido
		}
	
		return FALSE;        
	}
	
	public function buscarAvaria($id){
		$this->db->select('*');
		$this->db->from('produto_avarias pa');
		$this->db->join('produto p', 'p.produto_id = pa.produto_id');
		$this->db->join('vendas v', 'v.idVendas = pa.venda_id');
		$this->db->join('clientes c', 'c.cliente_id = v.clientes_id');
		$this->db->join('usuarios u', 'u.usuario_id = v.usuarios_id');
		$this->db->where('pa.produto_avaria_id', $id);
		
		return $this->db->get()->result();      
	}	

	public function buscarAvariaVenda($id){
		$this->db->select('*');
		$this->db->from('produto_avarias pa');
		$this->db->join('produto p', 'p.produto_id = pa.produto_id');
		$this->db->join('vendas v', 'v.idVendas = pa.venda_id');
		$this->db->join('clientes c', 'c.cliente_id = v.clientes_id');
		$this->db->join('usuarios u', 'u.usuario_id = v.usuarios_id');
		$this->db->where('pa.venda_id', $id);
		
		return $this->db->get()->result();      
	}

 	public function estoqueAdd($quantidade, $produto)
	{
		try {
			$this->db->trans_begin();
			$query =  "UPDATE produto set produto_estoque = produto_estoque + ? WHERE produto_id = ?";
			$this->db->query($query, array($quantidade, $produto));
	
			if ($this->db->affected_rows() >= '1')
			{
				$this->db->trans_commit();
				return TRUE;
			}
			$this->db->trans_rollback();
			return FALSE;        
		} catch (Exception $e) {
			$this->db->trans_rollback();
			return FALSE;
		}
    }

   public function retirarItens($quantidade, $idItens)
   {
		try {
			$this->db->trans_begin();
			$this->db->query("SET @vl = (SELECT ((i.`subTotal`/i.`quantidade`) * (i.`quantidade` - ".$quantidade." )) FROM itens_de_vendas AS i WHERE i.`idItens` = ".$idItens.");");
		 	$query= $this->db->query("UPDATE `itens_de_vendas` SET `subTotal` = (SELECT @vl), quantidade = (quantidade - ".$quantidade.") WHERE `idItens` = ".$idItens."");
		 	if ($this->db->affected_rows() >= '1')
		 	{
				$this->db->trans_commit();
				return TRUE;
		 	}
		 	$this->db->trans_rollback();
		 	return FALSE;
		} catch (Exception $e) {
			$this->db->trans_rollback();
			return FALSE;
		}
    } 

   public function atualizarVenda($idVendas)
   {
		try {
			$this->db->trans_begin();
			$query =  "UPDATE `vendas` SET `valorTotal` = (SELECT SUM(`subTotal`) FROM `itens_de_vendas` AS v WHERE v.`vendas_id` = ".$idVendas.") WHERE `idVendas` = ".$idVendas."";
			$this->db->query($query);
	
			if ($this->db->affected_rows() >= '1')
			{
				$this->db->trans_commit();
				return TRUE;
			}
			$this->db->trans_rollback();
			return FALSE;
		} catch (Exception $e) {
			$this->db->trans_rollback();
			return FALSE;
		}
    }

   public function atualizarFinanceiro($idVendas)
   {

        /* $query =  "UPDATE `financeiro` SET `financeiro_valor` = (SELECT SUM(`subTotal`) FROM `itens_de_vendas` AS v WHERE v.`vendas_id` = ".$idVendas.") WHERE `vendas_id` = ".$idVendas.""; */
		try {
			$this->db->trans_begin();
			$query = " UPDATE `financeiro`, (SELECT vendas_id, SUM(`subTotal`) AS total, CONCAT('Fatura de Venda - #',".$idVendas.",' - ',filial) AS fili FROM itens_de_vendas
			WHERE vendas_id = ".$idVendas."
			GROUP BY filial) AS TB1
			SET `financeiro_valor` = TB1.total 
			WHERE TB1.vendas_id = financeiro.`vendas_id` AND TB1.fili = financeiro.`financeiro_descricao` ";
			$this->db->query($query);
			if ($this->db->affected_rows() == '1')
			{
				$this->db->trans_commit();
				return TRUE;
			}

			$this->db->trans_rollback();
			return FALSE;
		} catch(Exception $e) {
			$this->db->trans_rollback();
			return FALSE;
		}
    }


  public function ConsultaEstoque($produto)
	{

		$this->db->select('produto_estoque');				
		$this->db->where('produto_visivel', 1);
		$this->db->where('produto_id', $produto);

		return $this->db->get('produto')->result();

	} 


   public function getItensVendasQuantidadeProdutos($id)
    {
        $this->db->select('itens_de_vendas.produtos_id,itens_de_vendas.quantidade, itens_de_vendas.imei_valor, itens_de_vendas.filial');
        $this->db->from('itens_de_vendas');
        $this->db->join('produto','produto.produto_id = itens_de_vendas.produtos_id');
        $this->db->where('vendas_id',$id);
        // $this->db->where('filial',SUBDOMINIO); // Retirado - pegar todos os itens... 
        return $this->db->get()->result();

    }


   	public function atualizarEstoqueExclusao($itens)
    {
		try {
			$this->db->trans_begin();
			foreach ($itens as $iten) {
				//var_dump($itens);die();
				if(strtolower($iten->filial) == SUBDOMINIO){
				// if(strtolower($iten->filial) == 'cdd'){	
					$sql = "UPDATE 
							produto 
						SET
							produto_estoque = produto_estoque + ".$iten->quantidade."
						WHERE produto_id = ".$iten->produtos_id;            
					$this->db->query($sql);

					if($this->db->affected_rows() <= '0'){
						$this->db->trans_rollback();
						return FALSE;
					}
	
					if(!empty($iten->imei_valor)){				
						$resultado = $this->imeiRetirada($iten->imei_valor);
						if(!$resultado){
							$this->db->trans_rollback();
							return FALSE;
						}
					}
				} else {
					if (!empty($iten->imei_valor)) {
						// Só limpar filial se estiver IMEI no item de venda...
						$resultado = $this->imeiRetiradaFilial($iten->imei_valor, $iten->filial);
						if(!$resultado){
							$this->db->trans_rollback();
							return FALSE;
						}
					}
				}
			}
			
			$this->db->trans_commit();
			return TRUE;
		} catch (Exception $e) {
			$this->db->trans_rollback();
			return FALSE;
		}       
    }


	public function imeiRetirada($imei)
	{
		$sqlAtualizar = "UPDATE itens_de_imei set vendas_id = NULL WHERE imei_valor = '".$imei."'";
		$this->db->query($sqlAtualizar);
		
		if ($this->db->affected_rows() >= '1'){
		  return TRUE;
		}   
		return FALSE;      
	}

	  public function imeiRetiradaFilial($imei, $filial){

		// $sql = "SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE '%_".GRUPOLOJA."_".$filial."%' ";
		// $base = $this->db->query($sql)->result();
		// $base = $base[0]->SCHEMA_NAME;
	
	    $base = BDCAMINHO;
		$base = explode("_", $base);
		$base = $base[0];

		$base = $base.'_'.GRUPOLOJA.'_'.strtolower($filial);
	
		$sqlAtualizar = "UPDATE  $base.itens_de_imei set imei_filial = NULL WHERE imei_valor = '".$imei."'";
		$this->db->query($sqlAtualizar);
		
		if ($this->db->affected_rows() >= '1'){
			return TRUE;
		}   

		return FALSE;      
	  }

	  public function excluir($idVenda, $idUsuario) {
		// Iniciar a transação
		$this->db->trans_start();
	
		// Atualizar a tabela de vendas
		$this->db->where('idVendas', $idVenda);
		$this->db->update('vendas', array(
			'usuarios_id_exclusao' => $idUsuario,
			'venda_visivel' => 0
		));
	
		// Verificar se a atualização foi bem-sucedida
		if($this->db->affected_rows() < 1) { 
			$this->db->trans_rollback(); // Reverter a transação em caso de falha
			return false; // Ou lança uma exceção dependendo da sua estratégia de tratamento de erros
		}
	
		// Atualizar a tabela financeiro
		$this->db->set('financeiro_visivel', 0);
		$this->db->where('vendas_id',$idVenda);
		$this->db->update('financeiro'); 
	
		// Finalizar a transação
		$this->db->trans_complete();
	
		return $this->db->trans_status(); // Retorna true se a transação foi bem-sucedida e false caso contrário
	}
	

    public function pegarFornecedor()
	{

		$sql = "SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE '%_".GRUPOLOJA."_%' ";
        $bases = $this->db->query($sql)->result();;

        foreach ($bases as $b) {

          $this->db->select('*');
		  $this->db->where('emitente_tipo_empresa',1);
		  $result = $this->db->get($b->schema_name.'.emitente')->result();

		  if($result){

		  	$this->db->select('*');
		    $this->db->where('fornecedor_visivel',1);
		    return $this->db->get($b->schema_name.'.fornecedores')->result();

		  }


        }

	
	}


	// public function listarAberto($page, $itens_per_page, $pesquisa = NULL)
	// {

 //    $queryPesquisa = !is_null($pesquisa) ? " AND (cliente_nome LIKE '%".$pesquisa."%' OR usuario_nome LIKE '%".$pesquisa."%' OR dataVenda LIKE '%".$pesquisa."%' OR valorTotal LIKE '%".$pesquisa."%' OR idVendas LIKE '%".$pesquisa."%') " : "";

 //    $query =  "SELECT * FROM vendas
 //    				-- INNER JOIN `itens_de_vendas` ON vendas.`idVendas` = `itens_de_vendas`.`vendas_id`
	// 				LEFT JOIN `clientes`
	// 				ON vendas.`clientes_id` = clientes.`cliente_id`
	// 				LEFT JOIN `usuarios`
	// 				ON vendas.`usuarios_id` = usuarios.`usuario_id`
    
	// 	      WHERE vendas.`venda_visivel` = 1 
	// 	      	AND faturado IS NULL
	// 		  ".$queryPesquisa."
 //                ORDER BY vendas.idVendas DESC
	// 			LIMIT ".(($page - 1) * $itens_per_page).", ".$itens_per_page."; 
				

	// 		  ";
	// 	echo "<pre>";
	// 	echo ($query);
	// 	echo "</pre>";
	// 	return $this->db->query($query)->result();

	// }

	public function listarAbertoContador($page, $pesquisa = NULL)
	{

	    $queryPesquisa = !is_null($pesquisa) ? " AND (cliente_nome LIKE '%".$pesquisa."%' OR usuario_nome LIKE '%".$pesquisa."%' OR dataVenda LIKE '%".$pesquisa."%' OR valorTotal LIKE '%".$pesquisa."%' OR idVendas LIKE '%".$pesquisa."%') " : "";

		$query =  "SELECT 
		COUNT(`vendas`.`idVendas`) AS total
		FROM `vendas` 
			LEFT JOIN `clientes`
			ON vendas.`clientes_id` = clientes.`cliente_id`
			LEFT JOIN `usuarios`
			ON vendas.`usuarios_id` = usuarios.`usuario_id`
		WHERE (
		(vendas.`venda_visivel` = 1 AND faturado IS NULL)
		".$queryPesquisa."
		)
		";

		// echo "<pre>";
		// exit($query);
        return $this->db->query($query)->result();

   }


    public function listarAberto()
	{

		$sql = "SELECT * FROM vendas 
		RIGHT JOIN `itens_de_vendas` ON vendas.`idVendas` = `itens_de_vendas`.`vendas_id`
		LEFT JOIN `clientes`
		ON vendas.`clientes_id` = clientes.`cliente_id`
		LEFT JOIN `usuarios`
		ON vendas.`usuarios_id` = usuarios.`usuario_id`

		WHERE `valorTotal` IS NULL
		AND `faturado` IS NULL 
		AND vendas.`venda_visivel` = 1 
		GROUP BY idvendas  ";

		return $this->db->query($sql)->result(); 

        	
	}


	public function listarZerada()
	{

		$sql = " SELECT usuario_nome, cliente_nome, idVendas, dataVenda, valorTotal FROM vendas 
		INNER JOIN `financeiro` 
		ON vendas.`idVendas` = `financeiro`.`vendas_id`
		LEFT JOIN `clientes`
		ON vendas.`clientes_id` = clientes.`cliente_id`
		LEFT JOIN `usuarios`
		ON vendas.`usuarios_id` = usuarios.`usuario_id`
		WHERE `financeiro_visivel` = 1 AND `venda_visivel` = 0 

		UNION ALL	
		
			
		SELECT usuario_nome, cliente_nome, idVendas, dataVenda, valorTotal FROM vendas 
			RIGHT JOIN `itens_de_vendas` ON vendas.`idVendas` = `itens_de_vendas`.`vendas_id`
			LEFT JOIN `clientes`
			ON vendas.`clientes_id` = clientes.`cliente_id`
			LEFT JOIN `usuarios`
			ON vendas.`usuarios_id` = usuarios.`usuario_id`
			WHERE vendas.`venda_visivel` = 0 AND vendas.`usuarios_id_exclusao` = 0

		ORDER BY `dataVenda` DESC	 ; ";

		return $this->db->query($sql)->result(); 

        	
	}


	public function listarVendaIncompleta()	{

		$sql = " SELECT `categoria_prod_descricao`, p.`produto_descricao`, p.`produto_estoque`, Tb2.IMEI_Cadastrado FROM produto AS p
		INNER JOIN `categoria_produto` ON `categoria_produto`.`categoria_prod_id` = p.`produto_categoria_id`,
		(SELECT `produto_descricao`, `produto_estoque`, COUNT(produtos_id) AS IMEI_Cadastrado, produtos_id FROM itens_de_imei 
		INNER JOIN `produto` ON produto.`produto_id` = `itens_de_imei`.`produtos_id`
		WHERE imei_visivel = 1 AND vendas_id IS NULL
		GROUP BY produtos_id) AS Tb2
		WHERE   Tb2.IMEI_Cadastrado <> p.produto_estoque
		AND Tb2.produtos_id = p.`produto_id`; ";

		return $this->db->query($sql)->result();         	
	}

	public function reativaZeradas($id)
	{
		$this->db->set('venda_visivel', '1');
		$this->db->where($this->chave,$id);		
		
		if($this->db->update($this->tabela))
		{
			return true;
		}

		return false;
	}

	public function buscarImei($produto)
	{

		$this->db->select('*');	
		$this->db->join('produto ',' `produto`.`produto_id` = `itens_de_imei`.`produtos_id`  ');			
		$this->db->where('imei_visivel', 1);
		$this->db->where('vendas_id is NULL', NULL, FALSE);
		$this->db->where('produtos_id', $produto);

		return $this->db->get('itens_de_imei')->result();

	}
	
	public function buscarImeiTodos()
	{
		$this->db->select('*');	
		$this->db->join('produto ',' `produto`.`produto_id` = `itens_de_imei`.`produtos_id`  ');			
		$this->db->where('imei_visivel', 1);
		$this->db->where('vendas_id is NULL', NULL, FALSE);

		return $this->db->get('itens_de_imei')->result();
	} 

	public function imeiBuscar($imei)
	{

		$this->db->select('*');	
		$this->db->join('produto ',' `produto`.`produto_id` = `itens_de_imei`.`produtos_id`  ');			
		$this->db->where('imei_visivel', 1);
		$this->db->where('vendas_id is NULL', NULL, FALSE);
		$this->db->where('imei_valor', $imei);

		return $this->db->get('itens_de_imei')->result();

	}

	public function imeiBuscarAvaria($imei)
	{
		$this->db->select('*');	
		$this->db->join('produto ',' `produto`.`produto_id` = `itens_de_imei`.`produtos_id`  ');			
		$this->db->where('imei_valor', $imei);
		return $this->db->get('itens_de_imei')->result();
	}

	public function trocaImei($tabela, $dados, $chave = null , $id = null, $where = null)
	{
		if ($where) {
			$this->db->where($where);
		}else{
			$this->db->where($chave,$id);
		}
				
		
		if($this->db->update($tabela,$dados))
		{
			return true;
		}

		return false;
	}

	public function inverteVenda($idVendas, $idFinanceiro, $dataVenda, $dataFinanceiro)
    { 

		$this->db->where('idVendas',$idVendas);	
		$this->db->update('vendas',$dataVenda);

		$this->db->where('vendas_id',$idVendas);	
		$this->db->update('financeiro',$dataFinanceiro);


	    if($this->db->trans_status() === true){
	        $this->db->trans_commit();
	        return true;
	    } else {
	        $this->db->trans_rollback();
	        return false;
	    }

    }


}