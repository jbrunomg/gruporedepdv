<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Financeiro_model extends CI_Model
{

	public $tabela  = "financeiro";
	public $visivel = "financeiro_visivel";
	public $chave   = "idFinanceiro";

	public function listar($page, $itens_per_page, $pesquisa = NULL)
	{
		$query =  "SELECT * FROM financeiro";
		if ($pesquisa) {

			$query .= " WHERE (" . $pesquisa . ")";
		}
		$query .= " LIMIT " . (($page - 1) * $itens_per_page) . ", " . $itens_per_page . ";";
		// echo "<pre>";
		// echo ($query);
		// echo "</pre>";
		return $this->db->query($query)->result();
	}

	public function contador($page, $pesquisa = NULL)
	{
		$query =  "SELECT 
		COUNT(`financeiro`.`idFinanceiro`) AS total
		FROM `financeiro`";
		if ($pesquisa) {

			$query .= " WHERE (" . $pesquisa . ")";
		}
		// echo "<pre>";
		// exit($query);
		return $this->db->query($query)->result();
	}


	public function listarId($id)
	{
		$this->db->select("*");
		$this->db->where($this->chave, $id);
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}


	public function add($table, $data)
	{

		$this->db->insert($table, $data);

		if ($this->db->affected_rows() == '1') {
			return TRUE;
		}

		return FALSE;
	}

	public function edit($table, $data, $fieldID, $ID)
	{
		$this->db->where($fieldID, $ID);
		$this->db->update($table, $data);


		if ($this->db->affected_rows() >= 0) {
			return TRUE;
		}

		return FALSE;
	}


	function delete($table, $fieldID, $ID)
	{

		$this->db->where($fieldID, $ID);

		$this->db->delete($table);

		if ($this->db->affected_rows() == '1') {
			return TRUE;
		}

		return FALSE;
	}

	public function visualizar($id)
	{
		$this->db->select("*");
		$this->db->where($this->chave, $id);
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}

	public function getAllTiposCartao()
	{
		$this->db->where('categoria_cart_visivel', 1);
		return $this->db->get('categoria_cartao')->result();
	}

	public function getAllTiposCategorias()
	{
		$this->db->where('categoria_fina_visivel', 1);
		return $this->db->get('categoria_financeiro')->result();
	}

	public function autoCompleteClienteFornecedor($q)
	{
		$sql = "SELECT `cliente_id` AS id  , `cliente_nome` AS nome, 'CLIE' AS tipo FROM clientes where cliente_nome like '%{$q}%'
                UNION    
                SELECT `fornecedor_id` AS id , `fornecedor_nome` AS nome, 'FORN' AS tipo FROM fornecedores where fornecedor_nome like '%{$q}%' ";

		$resultado = $this->db->query($sql)->result();

		if (count($resultado) > 0) {
			foreach ($resultado as $row) {
				$row_set[] = array('label' => $row->nome . ' | ' . $row->tipo, 'id' => $row->id);
			}
			echo json_encode($row_set);
		}
	}

	public function autoCompleteCliente($q)
	{
		$sql = "SELECT `cliente_id` AS id  , `cliente_nome` AS nome, 'CLIE' AS tipo FROM clientes where cliente_nome like '%{$q}%' ";

		$resultado = $this->db->query($sql)->result();

		if (count($resultado) > 0) {
			foreach ($resultado as $row) {
				$row_set[] = array('label' => $row->nome . ' | ' . $row->tipo, 'id' => $row->id);
			}
			echo json_encode($row_set);
		}
	}

	public function financeiroBaixarListar()
	{
		$this->db->select("*");
		$this->db->join('usuarios ', ' `usuarios`.`usuario_id` = `baixar_conta`.`id_usuario`', 'left');
		$this->db->join('clientes ', '`baixar_conta`.`id_cliente` = `clientes`.`cliente_id` ');
		$this->db->order_by('id_baixa', 'desc');
		return $this->db->get('baixar_conta')->result();
	}

	public function clientesPendentes()
	{
		$this->db->select("clientes.`cliente_id`,
		                   clientes.`cliente_nome` AS cliente, 
						   SUM(financeiro.`financeiro_valor`) AS valor,
						   (SELECT SUM(f.`financeiro_valor`) AS valor FROM financeiro f WHERE f.os_id IS NOT NULL AND f.financeiro_baixado = 0 AND f.financeiro_visivel = 1 AND f.financeiro_forn_clie_id = `financeiro`.`financeiro_forn_clie_id`)AS valor_os,
						   (SELECT SUM(f.`financeiro_valor`) AS valor FROM financeiro f WHERE f.vendas_id IS NOT NULL AND f.financeiro_baixado = 0 AND f.financeiro_visivel = 1 AND f.financeiro_forn_clie_id = `financeiro`.`financeiro_forn_clie_id`)AS valor_venda");
		$this->db->join('clientes ', '`financeiro`.`financeiro_forn_clie_id` = `clientes`.`cliente_id` ');
		$this->db->where('financeiro.`financeiro_baixado`', 0);
		$this->db->where('financeiro.`financeiro_visivel`', 1);
		$this->db->group_by(' financeiro.`financeiro_forn_clie_id` ');
		$this->db->order_by('clientes.`cliente_nome`', 'desc');


		return $this->db->get('financeiro')->result();
	}


	public function financeiroBaixarAdd($dados)
	{
		$this->db->insert('baixar_conta', $dados);

		if ($this->db->affected_rows() == '1') {
			return TRUE;
		}

		return FALSE;
	}

	public function financeiroBuscaDivida($cliente)
	{
		$this->db->select("*");
		$this->db->where('financeiro.`financeiro_baixado`', 0);
		$this->db->where('financeiro.`financeiro_visivel`', 1);
		$this->db->where('financeiro.`financeiro_forn_clie_id`', $cliente);

		return $this->db->get('financeiro')->result();
	}

	public function baixaDividaClie($id)
	{
		$this->db->where('idFinanceiro', $id);

		$this->db->set('financeiro_baixado', 1);
		$this->db->set('data_pagamento', date('Y-m-d'));
		$this->db->set('financeiro_forma_pgto', 'Dinheiro');

		if ($this->db->update('financeiro')) {
			// Pegar Vendas_id para update tabela Venda faturado = 1
			$sql = "SELECT  vendas_id FROM `financeiro` WHERE `idFinanceiro` = {$id} ";
			$resultado = $this->db->query($sql)->result();
			if (!empty($resultado[0]->vendas_id)) {
				$this->db->where('idVendas', $resultado[0]->vendas_id);
				$this->db->set('faturado', 1);
				$this->db->update('vendas');
			}
			return true;
		}

		return false;
	}

	public function verificarBaixar($cliente, $valor_pago)
	{

		$this->db->from('financeiro');
		$this->db->where('financeiro_forn_clie_id', $cliente);
		$this->db->where('financeiro_valor', $valor_pago);
		$this->db->where('data_pagamento', date('Y-m-d'));
		$this->db->where($this->visivel, 1);

		if ($this->db->count_all_results() >= '1') {
			return TRUE;
		}

		return FALSE;
	}
}
