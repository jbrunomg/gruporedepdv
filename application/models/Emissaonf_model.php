<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Emissaonf_model extends CI_Model
{
    public $tabela = 'fiscal_nf';

    public function listar()
    {
        $this->db->select('fiscal_nf_id, fiscal_nf_data_Fiscal, fiscal_nf_finalidade, fiscal_nf_fiscal_nfe, fiscal_nf_modelo, fiscal_nf_total, fiscal_nf_status_nota, fiscal_nf_fiscal_danfe, clientes.cliente_nome');
        $this->db->join('clientes', 'clientes.cliente_id = fiscal_nf.fiscal_nf_clientes_id');
        $this->db->where('fiscal_nf_fiscal_visivel', 1);
        // $this->db->order_by('fiscal_nf_id', 'ASC');
        return $this->db->get($this->tabela)->result();
    }

    public function add($dados)
    {
        $this->db->insert($this->tabela, $dados);
        return $this->db->insert_id();
    }

    public function edt($dados, $id)
    {
        // die(var_dump($id));
        $this->db->where('fiscal_nf_id', $id);
        $this->db->update($this->tabela, $dados);

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function addVenda($dados, $fiscal_id, $venda_id)
    {
        $this->db->select('fiscal_nf_vendas_venda_id, fiscal_nf_vendas_fiscal_nf_id');
        $this->db->where('fiscal_nf_vendas_fiscal_nf_id', intval($fiscal_id));
        $this->db->where('fiscal_nf_vendas_venda_id', intval($venda_id));
        $venda = $this->db->get('fiscal_nf_vendas')->result();

        if (empty($venda)) {
            $this->db->insert('fiscal_nf_vendas', $dados);
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function verificarVenda($venda_id)
    {
        $this->db->where('idVendas', $venda_id);
        return $this->db->get('vendas')->row();
    }

    public function listarVendas($fiscal_id)
    {
        $this->db->select('vendas.*');
        $this->db->join('fiscal_nf_vendas', 'fiscal_nf_vendas.fiscal_nf_vendas_venda_id = vendas.idVendas');
        $this->db->where('fiscal_nf_vendas.fiscal_nf_vendas_fiscal_nf_id', $fiscal_id);

        return $this->db->get('vendas')->result();
    }

    public function pegarIdVendas($fiscal_id)
    {
        $this->db->select("GROUP_CONCAT(fiscal_nf_vendas_venda_id SEPARATOR ',') AS vendas_id");
        $this->db->where('fiscal_nf_vendas_fiscal_nf_id', $fiscal_id);
        $resultado = $this->db->get('fiscal_nf_vendas')->row();
        return $resultado->vendas_id;
    }

    public function pegarNota($id)
    {
        $this->db->select('fiscal_nf.*, clientes.cliente_nome, usuarios.usuario_nome');
        $this->db->where('fiscal_nf_id', $id);
        $this->db->join('clientes', 'clientes.cliente_id = fiscal_nf.fiscal_nf_clientes_id');
        $this->db->join('usuarios', 'usuarios.usuario_id = fiscal_nf.fiscal_nf_usuarios_id');
        return $this->db->get($this->tabela)->row();
    }

    public function excluir($id)
    {
        $this->db->where('fiscal_nf_id', $id);
        $this->db->update($this->tabela, array('fiscal_nf_fiscal_visivel' => 0));

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function excluirVenda($fiscal_nf_id, $venda_id)
    {
        $this->db->where('fiscal_nf_vendas_fiscal_nf_id', $fiscal_nf_id);
        $this->db->where('fiscal_nf_vendas_venda_id', $venda_id);
        $this->db->delete('fiscal_nf_vendas');

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    //      EMISSAO.        //
    public function getVendasProdutos($id)
    {
        $sql = "SELECT 
                  vendas.`idVendas`,
                  produto.*,
                  imposto.*,
                  itens_de_vendas.`subTotal`,                  
                  itens_de_vendas.`percentual_desc`,
                  itens_de_vendas.`quantidade`,
                  itens_de_vendas.`imei_valor`,
                  itens_de_vendas.`prod_preco_venda` AS precoVendido

                FROM
                  vendas 
                  INNER JOIN itens_de_vendas 
                    ON vendas.`idVendas` = itens_de_vendas.`vendas_id` 
                  INNER JOIN produto 
                    ON itens_de_vendas.`produtos_id` = produto.`produto_id` 
                  LEFT JOIN imposto 
                    ON produto.`produto_imposto_id` = imposto.`imposto_id`    
         
                WHERE `faturado` = 1 
                  AND `idVendas` IN (" . $id . ")";
        $resultado = $this->db->query($sql);
        // die(var_dump($resultado->result_id->num_rows));
        if ($resultado->result_id->num_rows > 0) {
            return $resultado->result();
        }
        return false;
    }

    function getByIdNota($id)
    {
        $this->db->select('
                        fiscal_nf.*, 
                        usuarios.*, 
                        usuarios.usuario_email AS Uemail,
                        clientes.cliente_id AS cliente_idClientes,
                        clientes.cliente_nome AS cliente_nomeCliente,
                        clientes.cliente_cpf_cnpj AS cliente_documento,
                        clientes.cliente_rg_ie AS cliente_rgie,
                        clientes.cliente_telefone AS cliente_telefone,
                        clientes.cliente_celular AS cliente_celular,
                        clientes.cliente_email AS cliente_email,
                        clientes.cliente_data_cadastro AS cliente_dataCadastro,

                        clientes.cliente_endereco AS cliente_rua,
                        clientes.cliente_numero AS cliente_numero,
                        clientes.cliente_bairro AS cliente_bairro,
                        clientes.cliente_cidade AS cliente_cidade,
                        clientes.cliente_estado AS cliente_estado,
                        clientes.cliente_cep AS cliente_cep,
                        clientes.cliente_data_nasc AS cliente_dataNascimento
                        '); // Bruno 18/08/15 {Dados Vendedor recibo venda.}
        $this->db->from('fiscal_nf');
        $this->db->join('clientes', 'clientes.cliente_id = fiscal_nf.fiscal_nf_clientes_id');
        $this->db->join('usuarios', 'usuarios.usuario_id = fiscal_nf.fiscal_nf_usuarios_id');
        $this->db->where('fiscal_nf.fiscal_nf_id', $id);
        $this->db->where('fiscal_nf.fiscal_nf_fiscal_situacao', 1);
        $this->db->limit(1);
        return $this->db->get()->row();
    }

    public function getPedidoNFCe($id)
    {
        $sql = "SELECT 
        GROUP_CONCAT(forma_pagamento) AS forma_pagamento,
        GROUP_CONCAT(valor_pagamento) AS valor_pagamento,
        GROUP_CONCAT(bandeira_cart) AS bandeira_cart,
        GROUP_CONCAT(autorizacao_NSU) AS autorizacao_NSU,
        GROUP_CONCAT(cnpj_credenciadora) AS cnpj_credenciadora
    FROM (
        SELECT 
            CASE 
                WHEN `financeiro`.`financeiro_forma_pgto` = 'Dinheiro' THEN '01' 
                WHEN `financeiro`.`financeiro_forma_pgto` = 'Cheque' THEN '02' 
                WHEN `financeiro`.`financeiro_forma_pgto` = 'Cartão de Crédito' THEN '03' 
                WHEN `financeiro`.`financeiro_forma_pgto` = 'Cartão de Débito' THEN '04' 
                WHEN `financeiro`.`financeiro_forma_pgto` = 'Crédito Loja' THEN '05' 
                WHEN `financeiro`.`financeiro_forma_pgto` = 'Vale Alimentação' THEN '10' 
                WHEN `financeiro`.`financeiro_forma_pgto` = 'Vale Refeição' THEN '11' 
                WHEN `financeiro`.`financeiro_forma_pgto` = 'Vale Presente' THEN '12' 
                WHEN `financeiro`.`financeiro_forma_pgto` = 'Vale Combustível' THEN '13' 
                WHEN `financeiro`.`financeiro_forma_pgto` = 'Duplicata Mercantil' THEN '14' 
                WHEN `financeiro`.`financeiro_forma_pgto` = 'Boleto Bancário' THEN '15' 
                WHEN `financeiro`.`financeiro_forma_pgto` = 'Transferência bancária' THEN '16' 
                WHEN `financeiro`.`financeiro_forma_pgto` = 'Sem pagamento' THEN '90'
                WHEN `financeiro`.`financeiro_forma_pgto` = 'Pagamento Instantâneo (PIX)' THEN '17'
                ELSE '99' 
            END AS forma_pagamento, 
            SUM(`financeiro`.`financeiro_valor`) AS valor_pagamento, 
            CASE 
                WHEN `categoria_cartao`.`categoria_cart_descricao` = 'Visa / Visa Electron' THEN '01' 
                WHEN `categoria_cartao`.`categoria_cart_descricao` = 'Mastercard / Maestro' THEN '02' 
                WHEN `categoria_cartao`.`categoria_cart_descricao` = 'American Express' THEN '03' 
                WHEN `categoria_cartao`.`categoria_cart_descricao` = 'Sorocred' THEN '04' 
                WHEN `categoria_cartao`.`categoria_cart_descricao` = 'Diners Club' THEN '05' 
                WHEN `categoria_cartao`.`categoria_cart_descricao` = 'Elo' THEN '06' 
                WHEN `categoria_cartao`.`categoria_cart_descricao` = 'Hipercard' THEN '07' 
                WHEN `categoria_cartao`.`categoria_cart_descricao` = 'Aura' THEN '08' 
                WHEN `categoria_cartao`.`categoria_cart_descricao` = 'Cabal' THEN '09' 
                WHEN `categoria_cartao`.`categoria_cart_descricao` = 'Outros' THEN '99' 
                ELSE '' 
            END AS bandeira_cart, 
            GROUP_CONCAT(`financeiro`.`financeiro_autorizacao_NSU`) AS autorizacao_NSU, 
            GROUP_CONCAT(`categoria_cartao`.`categoria_cart_cnpj_credenciadora`) AS cnpj_credenciadora 
        FROM 
            `financeiro` 
        LEFT JOIN 
            `categoria_cartao` 
        ON 
            `categoria_cartao`.`categoria_cart_id` = `financeiro`.`financeiro_bandeira_cart` 
        WHERE 
            `vendas_id` IN ({$id}) 
        GROUP BY 
            `financeiro`.`financeiro_forma_pgto`, 
            `categoria_cartao`.`categoria_cart_descricao`
    ) AS PedidoNFCe;
    ";
        $resultado = $this->db->query($sql);
        if ($resultado->result_id->num_rows > 0) {
            return $resultado->result();
        }
        return false;
    }
    public function atualizarDanfeXml($idNotaFiscal, $dadosAtualizar)
    {
        $this->db->where('fiscal_nf_id', $idNotaFiscal);
        $this->db->update('fiscal_nf', $dadosAtualizar);

        if ($this->db->affected_rows() >= 0) {
            return TRUE;
        }

        return FALSE;
    }
    //     /EMISSAO.        //
}
