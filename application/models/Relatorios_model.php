<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Relatorios_model extends CI_Model {
	
	######## RELATORIO MENU CLIENTE ########
	public function clientes($mes = false)
	{
		$this->db->select("*");
		$this->db->where('cliente_visivel', 1);


		if($mes){
			//$this->db->where('YEAR(cliente_data_cadastro)', date('Y'));
			$this->db->where('MONTH(cliente_data_nasc)', date('m'));
		}
		return $this->db->get('clientes')->result();
	}

	public function clientesTotal()
	{	
		return $this->db->count_all('clientes');
	}

	public function clientesTotalAnive()
	{	
		//$this->db->where('YEAR(cliente_data_cadastro)', date('Y'));
		$this->db->where('MONTH(cliente_data_nasc)', date('m'));
		$this->db->from('clientes');
		return $this->db->count_all_results();
	}

	public function clientesTotalSemComprar()
	{	
		
		$query = " vendas
		INNER JOIN `clientes` ON vendas.`clientes_id` = clientes.`cliente_id`
			WHERE `clientes_id` NOT IN(
		SELECT clientes_id FROM vw_cliente_compra 
			WHERE dataVenda BETWEEN DATE_ADD(CURRENT_DATE(), INTERVAL -30 DAY) AND CURDATE()) 
		AND (clientes_id,dataVenda) IN (SELECT  clientes_id,MAX(dataVenda) FROM vendas GROUP BY  clientes_id)
		ORDER BY `cliente_nome`";    

		return $this->db->count_all_results($query);
	}

	public function clientesSemComprar($mes)
	{	
		
		$query = " SELECT idVendas, dataVenda, `valorRecebido`, `clientes_id`, `cliente_nome`, `cliente_celular` FROM vendas 
		INNER JOIN `clientes` ON vendas.`clientes_id` = clientes.`cliente_id`
			WHERE `clientes_id` NOT IN(
		SELECT clientes_id FROM vw_cliente_compra 
			WHERE dataVenda BETWEEN DATE_ADD(CURRENT_DATE(), INTERVAL -{$mes} DAY) AND CURDATE()) 
		AND (clientes_id,dataVenda) IN (SELECT  clientes_id,MAX(dataVenda) FROM vendas GROUP BY  clientes_id)
		ORDER BY DATE(dataVenda) DESC, cliente_nome DESC ";

    return $this->db->query($query)->result();	
	}


	public function clienteVendasCustomizadas($dataInicio, $dataFim, $cliente,  $ordenacao)
	{
		# code...
	    
	    $this->db->select('*');
		$this->db->join('clientes ','`vendas`.`clientes_id` = `clientes`.`cliente_id`' );

		$this->db->where('dataVenda >=', $dataInicio);
		$this->db->where('dataVenda <=', $dataFim);

		$this->db->where('cliente_id', $cliente);		
	    $this->db->where('venda_visivel', 1);

	    if ($ordenacao == '1') {
	    	$this->db->order_by('idVendas', 'desc');

	    }else{
	    	$this->db->order_by('dataVenda', 'asc');

	    }
	    
		return $this->db->get('vendas')->result_array();
	}


	public function clienteVendasCustomizadasItens($dataInicio, $dataFim, $cliente)
	{
		# code...
	    
	    $this->db->select('*');
		$this->db->join('vendas ','`itens_de_vendas`.`vendas_id` = `vendas`.`idVendas`' );
		$this->db->join('produto ','`produto`.`produto_id` = `itens_de_vendas`.`produtos_id`' );

		$this->db->where('dataVenda >=', $dataInicio);
		$this->db->where('dataVenda <=', $dataFim);

		$this->db->where('clientes_id', $cliente);		
	    $this->db->where('venda_visivel', 1);

	    
		return $this->db->get('itens_de_vendas')->result_array();
	}

    public function detalhamentoPago($cliente)
    {
        $query = " SELECT 
        `financeiro_forn_clie` AS cliente,
        YEAR(data_pagamento) AS ano,
        MONTH(data_pagamento) AS mes,
		SUM(`financeiro_valor`) AS `total`
            FROM `financeiro`
        WHERE `financeiro_baixado` = 1 
           AND `financeiro_visivel` = 1 
           AND `financeiro_forn_clie_id` = $cliente
           AND YEAR(`data_pagamento`) = YEAR(`data_pagamento`)
        GROUP BY YEAR(`data_pagamento`),MONTH(data_pagamento)
        ORDER BY YEAR(`data_pagamento`) DESC, MONTH(data_pagamento) DESC 
            ";

        return $this->db->query($query)->result();

    }

    public function detalhamentoCompra($cliente)
    {
        $query = " SELECT 
        `financeiro_forn_clie` AS cliente,
        YEAR(data_vencimento) AS ano,
        MONTH(data_vencimento) AS mes,
        SUM(`financeiro_valor`) AS `totalCompra`
            FROM `financeiro`
        WHERE  `financeiro_visivel` = 1 
           AND `financeiro_forn_clie_id` = $cliente
           AND YEAR(`data_vencimento`) = YEAR(`data_vencimento`)
        GROUP BY YEAR(`data_vencimento`),MONTH(data_vencimento)
        ORDER BY YEAR(`data_vencimento`) DESC, MONTH(data_vencimento) DESC 
			";

		return $this->db->query($query)->result();

	}

	public function detalhamentoAberto($cliente)
    {
        $query = " SELECT 
        `financeiro_forn_clie` AS cliente,
        YEAR(data_pagamento) AS ano,
        MONTH(data_pagamento) AS mes,
        SUM(`financeiro_valor`) AS `totalAberto`
            FROM `financeiro`
        WHERE `financeiro_baixado` = 0
		   AND `financeiro_visivel` = 1 
           AND `financeiro_forn_clie_id` = $cliente
           AND YEAR(`data_pagamento`) = YEAR(`data_pagamento`)
        GROUP BY YEAR(`data_pagamento`),MONTH(data_pagamento)
        ORDER BY YEAR(`data_pagamento`) DESC, MONTH(data_pagamento) DESC 
			";

		return $this->db->query($query)->result();

	}

	public function clientesRPA()
	{
		$this->db->select('*');
		$this->db->join(' `clientes`',' `financeiro`.`financeiro_forn_clie_id` = `clientes`.`cliente_id` ');
        $this->db->where('financeiro_baixado', 0);
		$this->db->where('financeiro_visivel', 1);
		$this->db->group_by('financeiro_forn_clie_id');
        return $this->db->get('financeiro')->result();
	}


	######## FIM RELATORIO MENU CLIENTE ########


	######## RELATORIO MENU PRODUTO ########
	public function pegarGrupoProduto()
	{
		$this->db->select('*');
		$this->db->where('categoria_prod_visivel', 1);
		return $this->db->get('categoria_produto')->result();
	}

	public function totalProduto()
	{
		//return $this->db->count_all('produto');
		$this->db->where('produto_visivel',1);
		$this->db->from('produto');
		return $this->db->count_all_results();
	}

	public function totalEstoqueMinimo()
	{


		$this->db->where('produto_estoque < produto_estoque_minimo');
		$this->db->where('produto_visivel',1);
		$this->db->from('produto');
		return $this->db->count_all_results();

	}

	public function totalValorEstoque()
	{		
		$this->db->select('  SUM(`produto_preco_custo` * `produto_estoque`) as totalValor ');
		$this->db->where('produto_visivel',1);		
		return $this->db->get('produto')->result();
	}


	public function produtos()
	{
		$this->db->order_by('produto_descricao','asc');
        $this->db->where('produto_visivel',1);
        return $this->db->get('produto')->result();
	}

	public function estoqueMinimo()
	{
		$this->db->order_by('produto_descricao','asc');
  		$this->db->where('produto_estoque < produto_estoque_minimo');
  		$this->db->where('produto_visivel',1);
  		return $this->db->get('produto')->result();
	}

	public function valorEstoque()
	{
        $query = "SELECT produto_descricao, produto_estoque, produto_preco_custo, produto_preco_venda, ROUND( (
            (
            produto_preco_venda - produto_preco_custo
            ) * produto_estoque ) , 2
            ) AS lucro                    
            FROM `produto`  WHERE produto_estoque > 0 and produto_visivel = 1";

        return $this->db->query($query)->result();
	}

	public function valorEstoqueGrupo()
	{
        $query = " SELECT `categoria_produto`.`categoria_prod_descricao` as produto_descricao,  SUM(`produto_estoque`) AS produto_estoque,
        	ROUND(SUM(`produto_preco_custo` * `produto_estoque`) ,2) AS produto_preco_custo,
			ROUND(SUM(`produto_preco_venda` * `produto_estoque`) ,2) AS produto_preco_venda,
			ROUND((
			SUM(`produto_preco_venda` * `produto_estoque`) - SUM(`produto_preco_custo` * `produto_estoque`)
         	) , 2 ) AS lucro
         	   FROM produto
		INNER JOIN `categoria_produto` ON `produto`.`produto_categoria_id` = `categoria_produto`.`categoria_prod_id`
		WHERE `produto`.`produto_visivel` = 1 
		GROUP BY `produto_categoria_id` ";

        return $this->db->query($query)->result();
	}


	public function pegarEtiquetaProduto($grupo)
	{
		$this->db->select('*, "url" ');
		$this->db->where('produto_visivel', 1);
		$this->db->where('produto_categoria_id', $grupo);
		return $this->db->get('`produto`')->result();
	}

	public function produtosAvaria($dataInicio, $dataFim, $motivo)
	{

		$sql = "SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE '%_".GRUPOLOJA."_%' ";
        $bases = $this->db->query($sql)->result();

        foreach ($bases as $b) {

          $this->db->select('*');
		  $this->db->where('emitente_tipo_empresa',1);
		  $result = $this->db->get($b->schema_name.'.emitente')->result();


		  if($result){

				$this->db->select('produto.`produto_descricao`,`produto_avarias`.`produto_avarias_quantidade`,
				`produto_avaria_antigo_imei`,`produto_avaria_novo_imei`, produto_avaria_motivo, DATE_FORMAT(`produto_avarias`.`produto_avaria_cadastro`, "%d/%m/%Y") AS data,`produto_avarias`.`venda_id`, DATEDIFF( `produto_avarias`.`produto_avaria_cadastro`, `vendas`.`dataVenda`) AS dia, usuario_nome, produto_avaria_preco_custo, produto_avaria_preco_vendido, fornecedor_nome');
				$this->db->join('produto ',' `produto`.`produto_id` = `produto_avarias`.`produto_id`', 'left');
				$this->db->join('usuarios ',' `usuarios`.`usuario_id` = `produto_avarias`.`usuario_id`' , 'left');
				$this->db->join('vendas ',' vendas.`idVendas` = `produto_avarias`.`venda_id` ' , 'left');

				$this->db->join($b->schema_name.'.fornecedores',' `fornecedores`.`fornecedor_id` = `produto_avarias`.`fornecedor_id`' , 'left');
				$this->db->where('produto_avaria_visivel', 1);
				if ($dataInicio != '1970-01-01' and  $dataFim != '1970-01-01') {
		          $this->db->where('`produto_avarias`.produto_avaria_cadastro >=', $dataInicio);
		          $this->db->where('`produto_avarias`.produto_avaria_cadastro <=', $dataFim);
		        }
		        if ($motivo <> '') {
		        	$this->db->where('produto_avaria_motivo', $motivo );
		        }  
				return $this->db->get('`produto_avarias`')->result();
	    	} 

		}

	}

	public function produtosVendidos($dataInicio, $dataFim, $limite, $grupo, $unificar)
	{
		
		if($unificar == '1'){ // Cód Produto (agrupado) 
		$this->db->select(' `produto_descricao`, 
			SUM(DISTINCT `produto_estoque`) AS produto_estoque,
		  SUM(`quantidade`) AS qtd , 
			ROUND( AVG(`prod_preco_custo`),2) AS custo_medio,
			ROUND( SUM(`subTotal`) / SUM(`quantidade`) ,2) AS venda_media ');
		} else{
			$this->db->select(' `produto_descricao`, `produto_estoque`, SUM(`quantidade`) AS qtd , 
			ROUND( AVG(`prod_preco_custo`),2) AS custo_medio,
			ROUND( SUM(`subTotal`) / SUM(`quantidade`) ,2) AS venda_media ');
		}
		 	
		$this->db->join('itens_de_vendas ',' vendas.`idVendas` = itens_de_vendas.`vendas_id` ' );
		$this->db->join('produto ',' itens_de_vendas.`produtos_id` = `produto`.`produto_id` ' );

		$this->db->where('venda_visivel', 1);

		if ($dataInicio != '1970-01-01' and  $dataFim != '1970-01-01') {
          $this->db->where('`dataVenda` BETWEEN "'. $dataInicio. '" AND "'.  $dataFim. '" '  );

        }

        if ($grupo <> '') {
        	$this->db->where('produto_categoria_id', $grupo );
        }   

         $this->db->order_by('qtd', 'desc');


        if($unificar == '1'){ // Cód Produto (agrupado)  
        	$this->db->group_by(' produto_codigo ');
        } else{
        	$this->db->group_by(' produto_descricao ');
        }        

        if ($limite <> '') {
        	$this->db->limit($limite);
        }
        
		return $this->db->get('`vendas`')->result();
	}

    public function produtosParado($limite, $grupo)
	{
		$this->db->select('*');
	    $this->db->where('`produto_data_ultima_venda` < SUBDATE(CURRENT_DATE, 20)', NULL, FALSE);
	    $this->db->where('`produto_visivel`',1);
	    $this->db->where('`produto_estoque`!=' , '0');

        if ($grupo <> '') {
        	$this->db->where('produto_categoria_id', $grupo );
        }   

        $this->db->order_by('produto_data_ultima_venda', 'asc');

        if ($limite <> '1') {
        $this->db->limit($limite);
        }

		return $this->db->get('`produto`')->result();
	}


    public function produtosVendas($dataInicio, $dataFim, $produto)
	{
		$this->db->select(' produto_descricao, COUNT(quantidade) AS quantidade, dataVenda ');

		$this->db->join('itens_de_vendas ',' vendas.`idVendas` = itens_de_vendas.`vendas_id` ' );
		$this->db->join('produto ',' itens_de_vendas.`produtos_id` = `produto`.`produto_id` ' );

		$this->db->where('venda_visivel', 1);
		// $this->db->where('faturado', 1);

		if ($dataInicio != '1970-01-01' and  $dataFim != '1970-01-01') {
          $this->db->where('`dataVenda` BETWEEN "'. $dataInicio. '" AND "'.  $dataFim. '" '  );

        }

        if ($produto <> '') {
        	$this->db->where('produto.`produto_id`', $produto );
        }   


        $this->db->order_by('dataVenda', 'asc');
        $this->db->group_by(' dataVenda ');
        
		return $this->db->get('`vendas`')->result();
	}

	public function produtosCustom($categoria, $produto, $estoque, $tipo_ordenacao, $imei = null)
	{	
		if ($imei) {
			$this->db->select('`categoria_produto`.`categoria_prod_descricao`, itens_de_imei.`imei_valor`, `itens_de_imei`.`vendas_id`, produto.`produto_descricao`, produto.`produto_estoque`, produto.`produto_preco_custo`, produto.produto_preco_minimo_venda, produto.`produto_preco_venda`, produto.produto_preco_cart_debito ');

			$this->db->join('categoria_produto',' produto.`produto_categoria_id` = categoria_produto.`categoria_prod_id` ' );

			$this->db->join('itens_de_imei','`produto`.`produto_id` = `itens_de_imei`.`produtos_id` and imei_visivel = 1', 'LEFT');
		}else{

			$this->db->select('`categoria_produto`.`categoria_prod_descricao`, produto.`produto_descricao`, produto.`produto_estoque`, produto.`produto_preco_custo`, produto.produto_preco_minimo_venda, produto.`produto_preco_venda`, produto.produto_preco_cart_debito ');

			$this->db->join('categoria_produto',' produto.`produto_categoria_id` = categoria_produto.`categoria_prod_id` ' );

		}

	

		$this->db->where('produto_visivel', 1);

		if ($estoque == '0') {
			$this->db->where('produto.`produto_estoque` >= 1 ' );
		}

		if ($categoria <> 'todos') {
			$this->db->where('categoria_produto.`categoria_prod_id`', $categoria );
		}
		

    if ($produto <> '') {
    	
    	$this->db->where('produto.`produto_id`', $produto );
    }  

    if (($imei) AND ($estoque == '0')) {
		// $this->db->where('`itens_de_imei`.`vendas_id`', null );
		// $this->db->where('`itens_de_imei`.`imei_valor`', 'is not null' );

		$where = "`itens_de_imei`.`vendas_id` IS NULL AND `itens_de_imei`.`imei_valor` IS NOT NULL";
		$this->db->where($where);			  
    } 


    // 0 = Produto | 1 = Cód Prod | 2 = Estoque A - Z | 3 = Estoque Z - A
    if ($tipo_ordenacao == 0) { 
    	 $this->db->order_by('produto_descricao', 'asc');
    }
    if ($tipo_ordenacao == 1) { 
    	 $this->db->order_by('produto_codigo', 'asc');
    }
    if ($tipo_ordenacao == 2) { 
    	 $this->db->order_by('produto_estoque', 'asc');
    }
    if ($tipo_ordenacao == 3) { 
    	 $this->db->order_by('produto_estoque', 'desc');
    }


   
        
		return $this->db->get('`produto`')->result();
	}

	public function produtosCatalogoCustom($categoria, $produto, $estoque, $tipo_ordenacao)
	{
		if ($tipo_ordenacao == 2) { 
		$this->db->select('`categoria_produto`.`categoria_prod_descricao`, produto.`produto_descricao`, sum(produto.`produto_estoque`) as produto_estoque, produto.`produto_preco_custo`, produto.produto_preco_minimo_venda, produto.`produto_preco_venda`, produto.produto_preco_cart_debito ');
		}else{
		$this->db->select('`categoria_produto`.`categoria_prod_descricao`, produto.`produto_descricao`, produto.`produto_estoque`, produto.`produto_preco_custo`, produto.produto_preco_minimo_venda, produto.`produto_preco_venda`, produto.produto_preco_cart_debito ');
  	}

		$this->db->join('categoria_produto ',' produto.`produto_categoria_id` = categoria_produto.`categoria_prod_id` ' );
		$this->db->where('produto_visivel', 1);
		$this->db->where('categoria_produto.`categoria_prod_id`', $categoria );

        if ($produto <> '') {
        	
        	$this->db->where('produto.`produto_id`', $produto );
        }   

        if ($estoque == 'S') {
        	
        	$this->db->where('produto.`produto_estoque` >= 1');
        }   

        if ($estoque == 'N') {
        	
        	$this->db->where('produto.`produto_estoque` = 0');
        }  

        // $this->db->order_by('produto_descricao', 'asc');

		    // 0 = Produto | 1 = Cód Prod | 2 = Cód Prod (agrupado) |3 = Estoque A - Z | 4 = Estoque Z - A
		    if ($tipo_ordenacao == 0) { 
		    	 $this->db->order_by('produto_descricao', 'asc');
		    }
		    if ($tipo_ordenacao == 1) { 
		    	 $this->db->order_by('produto_codigo', 'asc');
		    }
		    if ($tipo_ordenacao == 2) { 
		    	 $this->db->group_by('produto_codigo');
		    	 $this->db->order_by('produto_codigo', 'asc');
		    }
		    if ($tipo_ordenacao == 3) { 		    	 
		    	 $this->db->order_by('produto_estoque', 'asc');
		    }
		    if ($tipo_ordenacao == 4) { 
		    	 $this->db->order_by('produto_estoque', 'desc');
		    }
        
		return $this->db->get('`produto`')->result();
	}


	public function produtosVendasPMV($dataInicio, $dataFim, $categoria)
	{
		$this->db->select('vendas.`dataVenda`, vendas.`idVendas`, `usuarios`.`usuario_nome`, `categoria_produto`.`categoria_prod_descricao`,  `produto`.`produto_descricao`, `itens_de_vendas`.`prod_preco_minimo_venda`, (`subTotal`/quantidade) AS preco_vendido ');

		$this->db->join('`vendas` ',' `itens_de_vendas`.`vendas_id` = `vendas`.`idVendas` ','LEFT' );
		$this->db->join('`usuarios` ',' `vendas`.`usuarios_id` = `usuarios`.`usuario_id` ' );
		$this->db->join('`produto` ',' `itens_de_vendas`.`produtos_id` = `produto`.`produto_id` ' );
		$this->db->join('`categoria_produto` ',' `produto`.`produto_categoria_id` = `categoria_produto`.`categoria_prod_id` ' );

		$this->db->where('(`subTotal`/quantidade) < `prod_preco_minimo_venda`');
		$this->db->where('`vendas`.`venda_visivel` = 1');		
		$this->db->where('`vendas`.`dataVenda` BETWEEN "'. date('Y-m-d', strtotime($dataInicio)). '" and "'. date('Y-m-d', strtotime($dataFim)).'"');

	  if ($categoria <> '') {
		$this->db->where('categoria_produto.`categoria_prod_id`', $categoria );
  	}

		return $this->db->get('`itens_de_vendas`')->result();
	}


	######## FIM RELATORIO MENU PRODUTO ########

	######## RELATORIO MENU VENDAS ########

    public function pegarGrupoCartao()
	{
		$this->db->select('*');
		$this->db->where('categoria_cart_visivel', 1);
		$this->db->group_by('categoria_cart_nome_credenciadora');

		return $this->db->get('categoria_cartao')->result();
	}

    public function pegarVendedor()
	{
		$this->db->select('*');
		$this->db->where('usuario_visivel', 1);
		$this->db->order_by('usuario_nome', 'asc');
		return $this->db->get('usuarios')->result();
	}


	public function pegarFornecedor()
	{
		$this->db->select('*');
		$this->db->where('fornecedor_visivel', 1);
		$this->db->order_by('fornecedor_nome', 'asc');
		return $this->db->get('fornecedores')->result();
	}


	public function vendasFornecedor($dataIni, $dataFim, $fornecedor, $vendedor, $grupo)
	{

 // var_dump($dataIni[0]);die();

		$this->db->select('`fornecedor_nome`, `usuario_nome`, `idVendas`, `dataVenda`, `categoria_prod_descricao`, `produto_descricao`,  `itens_de_vendas`.`imei_valor`, `item_movimentacao_produto_custo`, ( `itens_de_vendas`.`subTotal` / `itens_de_vendas`.`quantidade` ) AS preco_venda,
   ROUND(`itens_de_vendas`.subTotal - item_movimentacao_produto_custo,2) AS lucro,
    (SELECT SUM(financeiro_valor * (financeiro_percentual_cart/100)) AS Tipo_Desconto_agrupado
                FROM `financeiro` WHERE vendas.`idVendas` = financeiro.`vendas_id`
                GROUP BY `vendas_id` ) AS desconto_cartao  ');

		$this->db->join('`itens_de_vendas`','`itens_de_vendas`.`vendas_id` = vendas.`idVendas`','LEFT' );
		$this->db->join('`produto`','`produto`.`produto_id` = `itens_de_vendas`.`produtos_id`' );
		$this->db->join('`categoria_produto`','`categoria_produto`.`categoria_prod_id` = `produto`.`produto_categoria_id`' );
		$this->db->join('`itens_de_imei`','`itens_de_imei`.`imei_valor` = `itens_de_vendas`.`imei_valor`' );
		$this->db->join('`movimentacao_produtos`','`movimentacao_produtos`.`movimentacao_produto_id` = `itens_de_imei`.`movimentacao_id`' );
		$this->db->join('`usuarios`','`usuarios`.`usuario_id` = `vendas`.`usuarios_id`' );
		$this->db->join('`fornecedores`','`fornecedores`.`fornecedor_id` = movimentacao_produtos.`movimentacao_fornecedor_id`','LEFT' );
		$this->db->join('`itens_movimentacao_produtos`','`itens_movimentacao_produtos`.`item_movimentacao_produto_produto_id` = itens_de_vendas.`produtos_id` 
    AND itens_de_vendas.`imei_valor` = itens_de_imei.`imei_valor` 
    AND itens_movimentacao_produtos.`item_movimentacao_produto_movimentacao_produto_id` = itens_de_imei.`movimentacao_id`' );


		$this->db->where('`faturado` = 1 ');
		$this->db->where('`vendas`.`venda_visivel` = 1');
		$this->db->where('`itens_movimentacao_produtos`.`item_movimentacao_produto_visivel` = 1');
		$this->db->where('itens_de_vendas.`imei_valor` <> ""');		
		$this->db->where('`vendas`.`dataVenda` BETWEEN "'.$dataIni[0]. '" and "'.$dataFim[0].'"');

	  	if ($fornecedor <> '') {
		$this->db->where('fornecedores.`fornecedor_id`', $fornecedor );
  		}

  		if ($vendedor <> '') {
		$this->db->where('usuarios.`usuario_id`', $vendedor );
  		}

  		if ($grupo <> '') {
		$this->db->where('produto.`produto_categoria_id`', $grupo );
  		}

  		$this->db->group_by('itens_de_vendas.`idItens`');
  		$this->db->order_by('dataVenda, horaVenda');

		return $this->db->get('`vendas`')->result();
	}


	public function vendasCelularVendedor($dataIni, $dataFim,  $vendedor, $tipo)
	{

		$this->db->select('`fornecedor_nome`, `usuario_nome`, `idVendas`, `dataVenda`, `categoria_prod_descricao`, `produto_descricao`,  `itens_de_vendas`.`imei_valor`, `item_movimentacao_produto_custo`, ( `itens_de_vendas`.`subTotal` / `itens_de_vendas`.`quantidade` ) AS preco_venda,
   ROUND(`itens_de_vendas`.subTotal - item_movimentacao_produto_custo,2) AS lucro ');

		$this->db->join('`itens_de_vendas`','`itens_de_vendas`.`vendas_id` = vendas.`idVendas`','LEFT' );
		$this->db->join('`produto`','`produto`.`produto_id` = `itens_de_vendas`.`produtos_id`' );
		$this->db->join('`categoria_produto`','`categoria_produto`.`categoria_prod_id` = `produto`.`produto_categoria_id`' );
		$this->db->join('`itens_de_imei`','`itens_de_imei`.`imei_valor` = `itens_de_vendas`.`imei_valor`' );
		$this->db->join('`movimentacao_produtos`','`movimentacao_produtos`.`movimentacao_produto_id` = `itens_de_imei`.`movimentacao_id`' );
		$this->db->join('`usuarios`','`usuarios`.`usuario_id` = `vendas`.`usuarios_id`' );
		$this->db->join('`fornecedores`','`fornecedores`.`fornecedor_id` = movimentacao_produtos.`movimentacao_fornecedor_id`','LEFT' );
		$this->db->join('`itens_movimentacao_produtos`','`itens_movimentacao_produtos`.`item_movimentacao_produto_produto_id` = itens_de_vendas.`produtos_id` 
    AND itens_de_vendas.`imei_valor` = itens_de_imei.`imei_valor` 
    AND itens_movimentacao_produtos.`item_movimentacao_produto_movimentacao_produto_id` = itens_de_imei.`movimentacao_id`' );


		$this->db->where('`faturado` = 1 ');
		$this->db->where('`vendas`.`venda_visivel` = 1');
		$this->db->where('itens_de_vendas.`imei_valor` <> ""');		
		$this->db->where('`vendas`.`dataVenda` BETWEEN "'.$dataIni. '" and "'.$dataFim.'"');

		if ($vendedor <> '') {
		$this->db->where('usuarios.`usuario_id`', $vendedor );
  		}

  	
  		$this->db->group_by('itens_de_vendas.`idItens`');
  		$this->db->order_by('dataVenda, horaVenda');

		return $this->db->get('`vendas`')->result();
	}

    public function totalVendasDia()
	{
		$base = explode('_', BDCAMINHO);
        $data = date('Y-m-d');
        $this->db->select('count(*)');
		$this->db->from('vendas');
		$this->db->join('financeiro ','`vendas`.`idVendas` = `financeiro`.`vendas_id`' , 'left');
		$this->db->join('itens_de_vendas ','`vendas`.`idVendas` = `itens_de_vendas`.`vendas_id`' , 'left');
		$this->db->where('dataVenda', $data);
	    $this->db->where('venda_visivel', 1);
	    //$this->db->where('faturado', 1); // retirada para calcular fiado.
	    $this->db->where('`itens_de_vendas`.filial', $base[2]);
	    $this->db->where('financeiro.`financeiro_visivel`',1);
	    $this->db->where('financeiro.`financeiro_forma_pgto` is NOT NULL', NULL, FALSE);
	    $this->db->group_by('vendas.idVendas');
		$num_results = $this->db->count_all_results();
		return $num_results;

	}

	public function totalVendasMesAtual()
	{
		$base = explode('_', BDCAMINHO);
        $data = date('m');
        $this->db->select('count(*)');
		$this->db->from('vendas');
		$this->db->join('financeiro ','`vendas`.`idVendas` = `financeiro`.`vendas_id`' , 'left');
		$this->db->join('itens_de_vendas ','`vendas`.`idVendas` = `itens_de_vendas`.`vendas_id`' , 'left');
		$this->db->where('MONTH(dataVenda)', $data);
		$this->db->where('venda_visivel', 1);
		// $this->db->where('faturado', 1); // retirada para calcular fiado.
		$this->db->where('`itens_de_vendas`.filial', $base[2]);
	    $this->db->where('financeiro.`financeiro_forma_pgto` is NOT NULL', NULL, FALSE);
	    $this->db->where('financeiro.`financeiro_visivel`',1);
	    $this->db->group_by('vendas.idVendas');
		$num_results = $this->db->count_all_results();
		return $num_results;

	}

	public function totalVendasMesAnterior()
	{
		$base = explode('_', BDCAMINHO);
        $data = date('m', strtotime('last day of last month'));
        $this->db->select('count(*)');
		$this->db->from('vendas');
		$this->db->join('financeiro ','`vendas`.`idVendas` = `financeiro`.`vendas_id`' , 'left');
		$this->db->join('itens_de_vendas ','`vendas`.`idVendas` = `itens_de_vendas`.`vendas_id`' , 'left');
		$this->db->where('MONTH(dataVenda)', $data);
		$this->db->where('venda_visivel', 1);
		// $this->db->where('faturado', 1); // retirada para calcular fiado.
		$this->db->where('`itens_de_vendas`.filial', $base[2]);
		$this->db->where('financeiro.`financeiro_forma_pgto` is NOT NULL', NULL, FALSE);
		$this->db->where('financeiro.`financeiro_visivel`',1);
		$this->db->group_by('vendas.idVendas');
		$num_results = $this->db->count_all_results();
		return $num_results;

	}

   public function totalVendasAnual()
	{
        $data = date('Y');
		$this->db->from('vendas');
		$this->db->where('YEAR(dataVenda)', $data);
		$this->db->where('venda_visivel', 1);
		// $this->db->where('faturado', 1); // retirada para calcular fiado.
	    $this->db->where('valorRecebido is NOT NULL', NULL, FALSE);
		$num_results = $this->db->count_all_results();
		return $num_results;

	}

    public function totalVendasAnualLoja()
	{
		$base = explode('_', BDCAMINHO);
        $data = date('Y');
        $this->db->select('count(*)');
		$this->db->from('vendas');
		$this->db->join('financeiro ','`vendas`.`idVendas` = `financeiro`.`vendas_id`' , 'left');
		$this->db->join('itens_de_vendas ','`vendas`.`idVendas` = `itens_de_vendas`.`vendas_id`' , 'left');
		$this->db->where('YEAR(dataVenda)', $data);
		$this->db->where('venda_visivel', 1);
		// $this->db->where('faturado', 1); // retirada para calcular fiado.
		$this->db->where('`itens_de_vendas`.filial', $base[2]);
	    $this->db->where('financeiro.`financeiro_forma_pgto` is NOT NULL', NULL, FALSE);
	    $this->db->where('financeiro.`financeiro_visivel`',1);
	    $this->db->group_by('vendas.idVendas');
		$num_results = $this->db->count_all_results();
		return $num_results;

	}

	public function VendasDia_OLD()
	{

		$base = explode('_', BDCAMINHO);
		// var_dump($base);die();
        $data = date('Y-m-d');
		$this->db->select('idVendas, clientes.cliente_nome, ROUND(SUM(`subTotal`), 2) AS valorTotal, valorRecebido, usuarios.usuario_nome, financeiro.`financeiro_forma_pgto`');
		$this->db->join('financeiro ','`vendas`.`idVendas` = `financeiro`.`vendas_id`' , 'left');
		$this->db->join('itens_de_vendas ','`vendas`.`idVendas` = `itens_de_vendas`.`vendas_id`' , 'left');
		$this->db->join('clientes ','`vendas`.`clientes_id` = `clientes`.`cliente_id` ');	
		$this->db->join('usuarios ','`vendas`.`usuarios_id` = `usuarios`.`usuario_id` ');
		$this->db->where('dataVenda', $data);
	    $this->db->where('venda_visivel', 1);
	    // $this->db->where('faturado', 1); // retirada para calcular fiado.
	    $this->db->where('`itens_de_vendas`.filial', $base[2]);
	    $this->db->where('financeiro.`financeiro_visivel`',1);
	    $this->db->where('financeiro.`financeiro_forma_pgto` is NOT NULL', NULL, FALSE);
	    $this->db->group_by('vendas.idVendas');
		return $this->db->get('vendas')->result_array();
	}

	public function VendasDia()
	{

		$base = explode('_', BDCAMINHO);
		// var_dump($base);die();
        $data = date('Y-m-d');
		$this->db->select('idVendas, clientes.cliente_nome, ROUND(SUM(`subTotal`), 2) AS valorTotal, valorRecebido, usuarios.usuario_nome, (SELECT GROUP_CONCAT(`financeiro_forma_pgto`) FROM `financeiro` WHERE `vendas_id` IN(`vendas`.`idVendas`)) AS financeiro_forma_pgto');
		
		$this->db->join('itens_de_vendas ','`vendas`.`idVendas` = `itens_de_vendas`.`vendas_id`' , 'left');
		$this->db->join('clientes ','`vendas`.`clientes_id` = `clientes`.`cliente_id` ');	
		$this->db->join('usuarios ','`vendas`.`usuarios_id` = `usuarios`.`usuario_id` ');
		$this->db->where('dataVenda', $data);
	    $this->db->where('venda_visivel', 1);
	    $this->db->where('`itens_de_vendas`.filial', $base[2]);
	    $this->db->group_by('vendas.idVendas');
		return $this->db->get('vendas')->result_array();
	}

	public function VendasDiaDashbord()
	{
		$base = explode('_', BDCAMINHO);
        $data = date('Y-m-d');
		$this->db->select('produto_descricao, quantidade, idVendas, usuarios.usuario_nome, `itens_de_vendas`.`filial`');
		$this->db->join('vendas ','`vendas`.`idVendas` = `itens_de_vendas`.`vendas_id`' , 'left');
		$this->db->join('usuarios ','`vendas`.`usuarios_id` = `usuarios`.`usuario_id` ');
		$this->db->join('produto','produto.`produto_id` = `itens_de_vendas`.`produtos_id`' );
		$this->db->where('dataVenda', $data);
	    $this->db->where('venda_visivel', 1);
	    $this->db->where('faturado', 1);
	    $this->db->where('`itens_de_vendas`.filial <>', $base[2]);
	    $this->db->group_by('vendas.idVendas');
		return $this->db->get('itens_de_vendas')->result_array();
	}

	public function VendasMensal($tipo)
	{
		if ($tipo == 'atual') {
			$data = date('m');
		} else {
			$data = date('m', strtotime('last day of last month'));
		}

		$base = explode('_', BDCAMINHO);	

		$sql = "		 
		  DAY(dataVenda) AS dia,
		  (
		  SELECT ROUND(SUM(financeiro_valor), 2) AS total 
		  FROM  financeiro 
		  WHERE financeiro.financeiro_forma_pgto IN('Dinheiro','Pagamento Instantâneo (PIX)')
		    AND YEAR(data_vencimento)= YEAR(NOW())
		    AND MONTH(data_vencimento) = {$data} 
		    AND dia = DAY(data_vencimento) 
		    AND financeiro.financeiro_forma_pgto IS NOT NULL 
		    AND financeiro.financeiro_visivel = 1
		  GROUP BY financeiro.financeiro_forma_pgto IN ('Dinheiro', 'Pagamento Instantâneo (PIX)'),
		    DAY(data_vencimento) 
		  ORDER BY DAY(data_vencimento),
		    financeiro.financeiro_forma_pgto ) AS dinheiro,

		  
		  ( SELECT ROUND(SUM(financeiro_valor), 2) AS total 
		  FROM  financeiro 
		  WHERE financeiro.financeiro_forma_pgto IN('Sem pagamento','Outros','Crédito Loja') 
		    AND YEAR(data_vencimento)= YEAR(NOW())
		    AND MONTH(data_vencimento) = {$data} 
		    AND dia = DAY(data_vencimento) 
		    AND financeiro.financeiro_forma_pgto IS NOT NULL 
		    AND financeiro.financeiro_visivel = 1
		  GROUP BY financeiro.financeiro_forma_pgto IN('Sem pagamento','Outros','Crédito Loja'),
		    DAY(data_vencimento) 
		  ORDER BY DAY(data_vencimento),
		    financeiro.financeiro_forma_pgto ) AS outros,
		        
		  (
		  SELECT ROUND(SUM(financeiro_valor), 2) AS total 
		  FROM  financeiro 
		  WHERE financeiro.financeiro_forma_pgto = 'Cartão de Crédito' 
		    AND YEAR(data_vencimento)= YEAR(NOW())
		    AND MONTH(data_vencimento) = {$data} 
		    AND dia = DAY(data_vencimento) 
		    AND financeiro.financeiro_forma_pgto IS NOT NULL 
		    AND financeiro.financeiro_visivel = 1
		  GROUP BY financeiro.financeiro_forma_pgto,
		    DAY(data_vencimento) 
		  ORDER BY DAY(data_vencimento),
		    financeiro.financeiro_forma_pgto) AS cart_cred_bruto,
		    
		  '' AS cart_cred_liquido,

		  ( SELECT GROUP_CONCAT(financeiro_valor)  AS total 
		  FROM  financeiro 
		  WHERE financeiro.financeiro_forma_pgto = 'Cartão de Crédito' 
		    AND YEAR(data_vencimento)= YEAR(NOW())
		    AND MONTH(data_vencimento) = {$data} 
		    AND dia = DAY(data_vencimento) 
		    AND financeiro.financeiro_forma_pgto IS NOT NULL 
		    AND financeiro.financeiro_visivel = 1
		   GROUP BY financeiro.financeiro_forma_pgto, DAY(data_vencimento) 
		  ORDER BY DAY(data_vencimento),
		    financeiro.financeiro_forma_pgto ) AS cart_cred_liquido1,
		    
		  ( SELECT  GROUP_CONCAT(financeiro_percentual_cart) AS perc 
		  FROM  financeiro 
		  WHERE financeiro.financeiro_forma_pgto = 'Cartão de Crédito' 
		    AND YEAR(data_vencimento)= YEAR(NOW())
		    AND MONTH(data_vencimento) = {$data} 
		    AND dia = DAY(data_vencimento) 
		    AND financeiro.financeiro_forma_pgto IS NOT NULL 
		    AND financeiro.financeiro_visivel = 1
		   GROUP BY financeiro.financeiro_forma_pgto, DAY(data_vencimento) 
		  ORDER BY DAY(data_vencimento),
		    financeiro.financeiro_forma_pgto ) AS cart_cred_liquido2,
		       
		    
		  ( SELECT ROUND(SUM(financeiro_valor), 2) AS total 
		  FROM  financeiro 
		  WHERE financeiro.financeiro_forma_pgto = 'Cartão de Débito' 
		    AND YEAR(data_vencimento)= YEAR(NOW())
		    AND MONTH(data_vencimento) = {$data} 
		    AND dia = DAY(data_vencimento) 
		    AND financeiro.financeiro_forma_pgto IS NOT NULL 
		    AND financeiro.financeiro_visivel = 1
		  GROUP BY financeiro.financeiro_forma_pgto,
		    DAY(data_vencimento) 
		  ORDER BY DAY(data_vencimento),
		    financeiro.financeiro_forma_pgto ) AS cart_debt_bruto,

		    '' AS cart_debt_liquido,
		    
		    
		      ( SELECT GROUP_CONCAT(financeiro_valor)  AS total 
		  FROM  financeiro 
		  WHERE financeiro.financeiro_forma_pgto = 'Cartão de Débito' 
		    AND YEAR(data_vencimento)= YEAR(NOW())
		    AND MONTH(data_vencimento) = {$data} 
		    AND dia = DAY(data_vencimento) 
		    AND financeiro.financeiro_forma_pgto IS NOT NULL 
		    AND financeiro.financeiro_visivel = 1
		   GROUP BY financeiro.financeiro_forma_pgto, DAY(data_vencimento) 
		  ORDER BY DAY(data_vencimento),
		    financeiro.financeiro_forma_pgto ) AS cart_deb_liquido1,
		    
		  ( SELECT  GROUP_CONCAT(financeiro_percentual_cart) AS perc 
		  FROM  financeiro 
		  WHERE financeiro.financeiro_forma_pgto = 'Cartão de Débito' 
		    AND YEAR(data_vencimento)= YEAR(NOW())
		    AND MONTH(data_vencimento) = {$data} 
		    AND dia = DAY(data_vencimento) 
		    AND financeiro.financeiro_forma_pgto IS NOT NULL 
		    AND financeiro.financeiro_visivel = 1
		   GROUP BY financeiro.financeiro_forma_pgto, DAY(data_vencimento) 
		  ORDER BY DAY(data_vencimento),
		    financeiro.financeiro_forma_pgto ) AS cart_deb_liquido2
		  ";

    	$this->db->select($sql);
		  $this->db->join('financeiro ','`vendas`.`idVendas` = `financeiro`.`vendas_id`' , 'left');
      $this->db->where('venda_visivel',1);
      // $this->db->where('faturado', 1); // retirada para calcular fiado.
      $this->db->where('financeiro.`financeiro_forma_pgto` is NOT NULL', NULL, FALSE);
      $this->db->where('MONTH(dataVenda)', $data);
      $this->db->order_by('DAY(dataVenda),financeiro.`financeiro_forma_pgto`');
      $this->db->group_by(' DAY(dataVenda)');
      return $this->db->get('vendas')->result_array();
	}


	public function VendasAnual($ano = null)
	{
	   $data = date('Y');

	   $data = $ano <> null ? $ano : $data;

	   // var_dump($data);die();

	   $this->db->select('   MONTH(dataVenda) AS `mes`,
           DATE_FORMAT(dataVenda, "%b") AS `mese`,
		  YEAR(dataVenda) AS ano,
		  COUNT(`vendas`.`idVendas`) AS qtVendas,
		 (SELECT ROUND(SUM(`subTotal`), 2) AS total FROM
		  `vendas` 
		  INNER JOIN `itens_de_vendas` ON `itens_de_vendas`.`vendas_id` = `vendas`.`idVendas` 
		   WHERE `venda_visivel` = 1 
		   -- AND faturado = 1
		  AND mes = MONTH(dataVenda)
		  AND YEAR(dataVenda) =  "'.$data.'"
		  GROUP BY MONTH(dataVenda) 
		  ORDER BY MONTH(dataVenda) ) AS total,
		 (SELECT 
		  ROUND(SUM(prod_preco_custo * `quantidade`), 2) AS total
		   FROM `itens_de_vendas` 
		    INNER JOIN `vendas` ON `itens_de_vendas`.`vendas_id` = `vendas`.`idVendas`
		   -- INNER JOIN `financeiro` ON `vendas`.`idVendas` = `financeiro`.`vendas_id` 
		   WHERE `venda_visivel` = 1
		   -- AND faturado = 1
		     AND mes = MONTH(dataVenda)
		     AND YEAR(dataVenda) =  "'.$data.'"
		     GROUP BY MONTH(dataVenda) 
		     ORDER BY MONTH(dataVenda) ) AS custo');
        $this->db->where('venda_visivel',1);
        // $this->db->where('faturado', 1); // retirada para calcular fiado.
        $this->db->where('valorRecebido is NOT NULL', NULL, FALSE);
        $this->db->where('YEAR(dataVenda)', $data);
        $this->db->order_by('MONTH(dataVenda)');
        $this->db->group_by(' MONTH(dataVenda)');
        return $this->db->get('vendas')->result_array();
	}

// Bruno 151220
   public function VendasAnualLoja($ano = null)
	{
	   $base = explode('_', BDCAMINHO);
	   $data = date('Y');
	  
	   $data = $ano <> null ? $ano : $data;

	  //var_dump($data);die();

	   	$this->db->select('   MONTH(dataVenda) AS `mes`,
           DATE_FORMAT(dataVenda, "%b") AS `mese`,
		  YEAR(dataVenda) AS ano,
		  COUNT(`vendas`.`idVendas`) AS qtVendas,
		 (SELECT ROUND(SUM(`subTotal`), 2) AS total FROM
		  `vendas` 
		  INNER JOIN `itens_de_vendas` ON `itens_de_vendas`.`vendas_id` = `vendas`.`idVendas` 
		   WHERE `venda_visivel` = 1 
		   -- AND faturado = 1
		  AND mes = MONTH(dataVenda)
		  AND YEAR(dataVenda) =  "'.$data.'"
		  AND `itens_de_vendas`.filial = "'.$base[2].'"
		  GROUP BY MONTH(dataVenda) 
		  ORDER BY MONTH(dataVenda) ) AS total,
		 (SELECT 
		  ROUND(SUM(prod_preco_custo * `quantidade`), 2) AS total
		   FROM `itens_de_vendas` 
		    INNER JOIN `vendas` ON `itens_de_vendas`.`vendas_id` = `vendas`.`idVendas`
		   -- INNER JOIN `financeiro` ON `vendas`.`idVendas` = `financeiro`.`vendas_id` 
		   WHERE `venda_visivel` = 1
		   -- AND faturado = 1
		     AND mes = MONTH(dataVenda)
		     AND YEAR(dataVenda) =  "'.$data.'"
		     AND `itens_de_vendas`.filial = "'.$base[2].'"
		     GROUP BY MONTH(dataVenda) 
		     ORDER BY MONTH(dataVenda) ) AS custo');
        $this->db->where('venda_visivel',1);
        // $this->db->where('faturado', 1); // retirada para calcular fiado.
        $this->db->where('valorRecebido is NOT NULL', NULL, FALSE);
        $this->db->where('YEAR(dataVenda)', $data);
        $this->db->order_by('MONTH(dataVenda)');
        $this->db->group_by(' MONTH(dataVenda)');
        return $this->db->get('vendas')->result_array();



	  //  $this->db->select('   MONTH(dataVenda) AS `mes`,
    //        DATE_FORMAT(dataVenda, "%b") AS `mese`,
		//   YEAR(dataVenda) AS ano,
		//   COUNT(`vendas`.`idVendas`) AS qtVendas,
		//  (SELECT ROUND(SUM(`subTotal`), 2) AS total FROM
		//   `vendas` 
		//   LEFT JOIN `financeiro` ON `vendas`.`idVendas` = `financeiro`.`vendas_id` 
		//   INNER JOIN `itens_de_vendas` ON `itens_de_vendas`.`vendas_id` = `vendas`.`idVendas` 
		//    WHERE `venda_visivel` = 1 -- AND faturado = 1 
		//   AND mes = MONTH(dataVenda)
		//   AND YEAR(dataVenda) =  "'.$data.'"
		//   AND `itens_de_vendas`.filial = "'.$base[2].'"
		//   AND financeiro.`financeiro_forma_pgto` IS NOT NULL
		//   AND financeiro.`financeiro_tipo` = "receita"
		//   GROUP BY MONTH(dataVenda) 
		//   ORDER BY MONTH(dataVenda) ) AS total,
		//  (SELECT 
		//   ROUND(SUM(prod_preco_custo * `quantidade`), 2) AS total
		//    FROM `itens_de_vendas` 
		//     INNER JOIN `vendas` ON `itens_de_vendas`.`vendas_id` = `vendas`.`idVendas`
		//     INNER JOIN `financeiro` ON `vendas`.`idVendas` = `financeiro`.`vendas_id` 
		//    WHERE `venda_visivel` = 1 
		//   -- AND faturado = 1 
		//      AND mes = MONTH(dataVenda)
		//      AND YEAR(dataVenda) =  "'.$data.'"
		//      AND `itens_de_vendas`.filial = "'.$base[2].'"
		//      AND financeiro.`financeiro_forma_pgto` IS NOT NULL
		//      AND financeiro.`financeiro_tipo` = "receita"
		//      AND financeiro.`financeiro_visivel` = 1
		//      GROUP BY MONTH(dataVenda) 
		//      ORDER BY MONTH(dataVenda) ) AS custo');
		// $this->db->join('financeiro ','`vendas`.`idVendas` = `financeiro`.`vendas_id`' , 'left');
    //     $this->db->where('venda_visivel',1);
    //    // $this->db->where('faturado', 1); // retirada para calcular fiado.
    //     $this->db->where('financeiro.`financeiro_visivel`',1);
    //     $this->db->where('financeiro.`financeiro_forma_pgto` is NOT NULL', NULL, FALSE);
    //     $this->db->where('YEAR(dataVenda)', $data);
    //     $this->db->order_by('MONTH(dataVenda)');
    //     $this->db->group_by(' MONTH(dataVenda)');
    //     return $this->db->get('vendas')->result_array();
	}


	public function VendasMensalGrupoTotal($ano, $mes)
	{
	   $base = explode('_', BDCAMINHO);
	   $this->db->select('   MONTH(dataVenda) AS `mes`,
           DATE_FORMAT(dataVenda, "%b") AS `mese`,
		  YEAR(dataVenda) AS ano,
		  COUNT(`vendas`.`idVendas`) AS qtVendas,
		 (SELECT ROUND(SUM(`subTotal`), 2) AS total FROM
		  `vendas` 
		  LEFT JOIN `financeiro` ON `vendas`.`idVendas` = `financeiro`.`vendas_id` 
		  INNER JOIN `itens_de_vendas` ON `itens_de_vendas`.`vendas_id` = `vendas`.`idVendas` 
		   WHERE `venda_visivel` = 1 -- AND faturado = 1 
		  AND mes = MONTH(dataVenda)
		  AND YEAR(dataVenda) =  "'.$ano.'"
		  AND `itens_de_vendas`.filial = "'.$base[2].'"
		  AND financeiro.`financeiro_forma_pgto` IS NOT NULL
		  AND financeiro.`financeiro_tipo` = "receita"
		  GROUP BY MONTH(dataVenda) 
		  ORDER BY MONTH(dataVenda) ) AS total,
		 (SELECT 
		  ROUND(SUM(prod_preco_custo  * `quantidade`), 2) AS total
		   FROM `itens_de_vendas` 
		    INNER JOIN `vendas` ON `itens_de_vendas`.`vendas_id` = `vendas`.`idVendas`
		    INNER JOIN `financeiro` ON `vendas`.`idVendas` = `financeiro`.`vendas_id` 
		   WHERE `venda_visivel` = 1 -- AND faturado = 1 
		     AND mes = MONTH(dataVenda)
		     AND YEAR(dataVenda) =  "'.$ano.'"
		     AND `itens_de_vendas`.filial = "'.$base[2].'"
		     AND financeiro.`financeiro_forma_pgto` IS NOT NULL
		     AND financeiro.`financeiro_tipo` = "receita"
		     AND financeiro.`financeiro_visivel` = 1
		     GROUP BY MONTH(dataVenda) 
		     ORDER BY MONTH(dataVenda) ) AS custo');
		$this->db->join('financeiro ','`vendas`.`idVendas` = `financeiro`.`vendas_id`' , 'left');
        $this->db->where('venda_visivel',1);
        // $this->db->where('faturado', 1); // retirada para calcular fiado.
        $this->db->where('financeiro.`financeiro_visivel`',1);
        $this->db->where('financeiro.`financeiro_forma_pgto` is NOT NULL', NULL, FALSE);
        $this->db->where('financeiro.`financeiro_tipo`', 'receita');
        $this->db->where('YEAR(dataVenda)', $ano);
        $this->db->where('MONTH(dataVenda)', $mes);
        $this->db->order_by('MONTH(dataVenda)');
        $this->db->group_by(' MONTH(dataVenda)');
        return $this->db->get('vendas')->result_array();
	}


	public function VendasMensalGrupoDetalhe($ano, $mes, $grupo)
	{
		$base = explode('_', BDCAMINHO);
		$this->db->select('  `categoria_prod_descricao` AS grupo,
	    SUM(quantidade) AS qtd_itens,
		ROUND(SUM(`subTotal`), 2) AS total, 
		ROUND(SUM(prod_preco_custo  * `quantidade`), 2) AS custo ');
		$this->db->join('vendas','vendas.`idVendas` = itens_de_vendas.`vendas_id`' );
		$this->db->join('produto','produto.`produto_id` = `itens_de_vendas`.`produtos_id`' );
		$this->db->join('categoria_produto','produto.`produto_categoria_id` = categoria_produto.`categoria_prod_id`' );
		//$this->db->join('financeiro ','`vendas`.`idVendas` = `financeiro`.`vendas_id`' , 'left');
		$this->db->where('venda_visivel',1);
		// $this->db->where('faturado',1); // retirada para calcular fiado.
		//$this->db->where('financeiro.`financeiro_visivel`',1);
        //$this->db->where('financeiro.`financeiro_forma_pgto` is NOT NULL', NULL, FALSE);
        //$this->db->where('financeiro.`financeiro_tipo`', 'receita');
        $this->db->where('`itens_de_vendas`.filial', $base[2]);
		$this->db->where('YEAR(dataVenda)', $ano);
        $this->db->where('MONTH(dataVenda)', $mes);
        if ($grupo != NULL) {
        	$this->db->where('`categoria_prod_id`', $grupo);
        }
        $this->db->group_by('categoria_prod_id');
        return $this->db->get('itens_de_vendas')->result_array();

	}


    public function VendasExcluidas($ano, $mes, $vendedor)
	{
		$this->db->select(' `idVendas`,  `usuarios`.`usuario_nome`, `dataVenda`, `us`.`usuario_nome` AS usu_nome,`data_atualizacao` ');
		$this->db->join('usuarios ','`vendas`.`usuarios_id` = `usuarios`.`usuario_id` ');
		$this->db->join('usuarios as us ','`us`.`usuario_id` = `vendas`.`usuarios_id_exclusao` ');
		$this->db->where('venda_visivel',0);
		$this->db->where('YEAR(dataVenda)', $ano);
        $this->db->where('MONTH(dataVenda)', $mes);
        if ($vendedor != NULL) {
        	$this->db->where('usuarios.`usuario_id`', $vendedor);
        }
        $this->db->order_by('data_atualizacao');
        return $this->db->get('vendas')->result();

	}


	public function vendasCustomizadas($dataInicio, $vedendor, $cliente, $lojas, $ordenacao, $faturado)
	{
		$this->db->select('idVendas, clientes.cliente_nome, ROUND(SUM(`itens_de_vendas`.`subTotal`), 2) AS `valorTotal`, valorRecebido, usuarios.usuario_nome, 
			(SELECT GROUP_CONCAT(`financeiro`.`financeiro_forma_pgto`," ",IF(`financeiro_parcela_cart` IS NOT NULL,CONCAT(`financeiro_parcela_cart`,"x"),"") ) AS financeiro_forma_pgto FROM financeiro WHERE `financeiro`.`vendas_id` = `vendas`.`idVendas` ) AS financeiro_forma_pgto ');

		$this->db->join('clientes ','`vendas`.`clientes_id` = `clientes`.`cliente_id` ');	
		$this->db->join('usuarios ','`vendas`.`usuarios_id` = `usuarios`.`usuario_id` ');
		//$this->db->join('financeiro ','`vendas`.`idVendas` = `financeiro`.`vendas_id`' , 'left');
		$this->db->join('itens_de_vendas ','`vendas`.`idVendas` = `itens_de_vendas`.`vendas_id`' , 'left');


    $this->db->where('venda_visivel',1);
        
		if ($faturado <> '') {
        	$this->db->where('faturado', $faturado);
        }
		
        if ($dataInicio != '1970-01-01') {
          $this->db->where('dataVenda', $dataInicio);
        }

        if ($cliente != NULL) {
          $this->db->where('`vendas`.`clientes_id', $cliente);
        }

        if ($vedendor != NULL) {
          $this->db->where('`vendas`.`usuarios_id`', $vedendor);
        }

        if ($lojas != NULL) {
          $this->db->like('`itens_de_vendas`.`filial`', $lojas);
          // $this->db->like('`financeiro`.`financeiro_descricao`', $lojas);
 
        }else{
          $this->db->where('`financeiro`.`financeiro_visivel`',1);
        }

        if ($ordenacao == '1') {
          $this->db->order_by('clientes.cliente_nome', 'asc');
        }else{
          $this->db->order_by('clientes.cliente_nome', 'desc');
        }

        // $this->db->group_by('`financeiro`.`idFinanceiro`');
        $this->db->group_by('`vendas`.`idVendas`');

        return $this->db->get('vendas')->result_array();
	}

   public function vendasCustomizadasItens($idVendas, $lojas)
	{
		$this->db->select('`produto`.`produto_descricao`, itens_de_vendas`.`quantidade`, ROUND((subTotal / quantidade), 2) AS valor_unitario,`itens_de_vendas`.`filial`,`itens_de_vendas`.`vendas_id`,`itens_de_vendas`.`subTotal`');
        $this->db->join('produto ','`itens_de_vendas`.`produtos_id` = `produto`.`produto_id` ');
        $this->db->where('vendas_id',$idVendas);
        if ($lojas != NULL) {
          $this->db->where('`itens_de_vendas`.`filial`', $lojas);
        }
        return $this->db->get('itens_de_vendas')->result_array();
	}

    public function vendasPagamento($dataIni, $dataFim,  $vendedor)
	{	

		$sql = " idVendas,  `usuarios`.`usuario_nome` AS vendedor, valorTotal AS vl_bruto, financeiro_valor AS vl_pgto,

		(CASE 
		 WHEN  financeiro_forma_pgto = 'Cartão de Crédito' THEN ROUND((financeiro_valor * (financeiro_percentual_cart/100)), 2)
		 WHEN  financeiro_forma_pgto = 'Cartão de Débito'  THEN ROUND((financeiro_valor * (financeiro_percentual_cart/100)), 2)
		 ELSE  '0.00'
		 END) AS desc_cartao,

		(CASE 
		 WHEN  financeiro_forma_pgto = 'Cartão de Crédito' THEN CONCAT(financeiro_forma_pgto,' ',financeiro_parcela_cart,'x')
		 WHEN  financeiro_forma_pgto = 'Cartão de Débito'  THEN CONCAT(financeiro_forma_pgto,' ',financeiro_parcela_cart,'x')
		 ELSE  financeiro_forma_pgto
		 END) AS frm_pgto	

		";

    	$this->db->select($sql);
		
		$this->db->join('vendas','financeiro.vendas_id = vendas.idVendas');

		 $this->db->join('usuarios ','`vendas`.`usuarios_id` = `usuarios`.`usuario_id` ');

		$this->db->where('`vendas`.`dataVenda` BETWEEN "'.$dataIni. '" and "'.$dataFim.'"');

		if ($vendedor != NULL) {
		$this->db->where('`vendas`.`usuarios_id`', $vedendor);
		}

   		$this->db->where('venda_visivel',1);
     
	    return $this->db->get('financeiro')->result_array();
	}


	public function vendasPagamentoCartao($dataIni, $dataFim, $maquina = null )
	{	
		
		//var_dump($maquina);die();

	    $sql = " SELECT 
	    `categoria_cart_nome_credenciadora`, `categoria_cart_descricao`, `financeiro_parcela_cart`, `financeiro_percentual_cart`, `financeiro_valor`,
	    ROUND((`financeiro_valor`-(`financeiro_valor` * (`financeiro_percentual_cart`/100)))) AS liq 
	    FROM `financeiro`
		INNER JOIN `categoria_cartao` ON `categoria_cartao`.`categoria_cart_id` = `financeiro`.`financeiro_bandeira_cart`
		WHERE `vendas_id` IN(SELECT `idVendas` FROM `vendas` WHERE `dataVenda` BETWEEN '$dataIni' AND '$dataFim' AND `venda_visivel` = 1)		
		";

		if ($maquina != NULL) {
	        $sql = $sql. " AND `categoria_cart_nome_credenciadora` = '".$maquina."' ORDER BY `categoria_cart_nome_credenciadora` ";
	    } else {
	    	$sql = $sql. " ORDER BY `categoria_cart_nome_credenciadora` ";
	    }

	    return $this->db->query($sql)->result();
	}


    public function vendasFinanceiroCustomizadas($idVendas)
	{	

     	$this->db->select('financeiro_valor,financeiro_forma_pgto,financeiro_parcela_cart,financeiro_percentual_cart');	

   		$this->db->where_in('vendas_id',$idVendas);
     
	    return $this->db->get('financeiro')->result_array();
	}


	


    ######## FIM RELATORIO MENU VENDAS ########

	######## RELATORIO MENU VENDEDORES ########

   public function vendedoresDia()
	{
		$loja = BDCAMINHO; $loja = explode("_", $loja);	
	    $data = date('Y-m-d');

	    $this->db->select('COUNT(DISTINCT vendas.`idVendas`) AS qtdVendas,	ROUND(SUM(itens_de_vendas.`subTotal`), 2) AS total, `usuarios`.`usuario_nome` AS vendedor');
	    $this->db->join('usuarios ','`vendas`.`usuarios_id` = `usuarios`.`usuario_id` ');
	    $this->db->join('`itens_de_vendas` ','`itens_de_vendas`.`vendas_id` = vendas.`idVendas` ');
	    // $this->db->join('financeiro ','`vendas`.`idVendas` = `financeiro`.`vendas_id`' , 'left');
	    $this->db->where('dataVenda', $data);
	    $this->db->where('venda_visivel',1);
	    // $this->db->where('faturado', 1);
	    $this->db->like('`itens_de_vendas`.`filial`', $loja[2]);
	    // $this->db->like('financeiro`.`financeiro_descricao', $loja[2]);
	    $this->db->group_by('`vendas`.`usuarios_id`');
	    $this->db->order_by('total', 'desc');
	    return $this->db->get('vendas')->result_array();
    }

    public function vendedoresDiaOutraloja()
	{
		$loja = BDCAMINHO; $loja = explode("_", $loja);	
	    $data = date('Y-m-d');

	    $this->db->select('COUNT(DISTINCT vendas.`idVendas`) AS qtdVendas,	ROUND(SUM(itens_de_vendas.`subTotal`), 2) AS total, `usuarios`.`usuario_nome` AS vendedor');
	    $this->db->join('usuarios ','`vendas`.`usuarios_id` = `usuarios`.`usuario_id` ');
	    $this->db->join('`itens_de_vendas` ','`itens_de_vendas`.`vendas_id` = vendas.`idVendas` ');	    
	    $this->db->where('dataVenda', $data);
	    $this->db->where('venda_visivel',1);	    
	    $this->db->where('`itens_de_vendas`.`filial` <>', $loja[2]);	    
	    $this->db->group_by('`vendas`.`usuarios_id`');
	    $this->db->order_by('total', 'desc');
	    return $this->db->get('vendas')->result_array();
    }

    public function totalVendasVendedoresDia()
	{
		$loja = BDCAMINHO; $loja = explode("_", $loja);	
        $data = date('Y-m-d');
		$this->db->from('vendas');
		$this->db->join('financeiro ','`vendas`.`idVendas` = `financeiro`.`vendas_id`' , 'left');
		$this->db->where('dataVenda', $data);
	  $this->db->where('venda_visivel',1);
	  //  $this->db->where('faturado', 1);
	  $this->db->like('financeiro`.`financeiro_descricao', $loja[2]);
		// $this->db->group_by('`vendas`.`usuarios_id`');
		$num_results = $this->db->count_all_results();
		return $num_results;

	}


	public function totalVendasVendedoresDiaOutraloja()
	{
		$loja = BDCAMINHO; $loja = explode("_", $loja);	
        $data = date('Y-m-d');
		$this->db->from('vendas');
		$this->db->join('financeiro ','`vendas`.`idVendas` = `financeiro`.`vendas_id`' , 'left');
		$this->db->where('dataVenda', $data);
	    $this->db->where('venda_visivel',1);
	    $this->db->where('financeiro`.`financeiro_descricao <>', $loja[2]);
	    
	    $num_results = $this->db->count_all_results();
		return $num_results;

	}

	public function vendedoresCustomizadas($dataInicio, $dataFim, $vedendor, $ordenacao, $loja )
	{		

	   // $loja = BDCAMINHO; $loja = explode("_", $loja);		
	    $this->db->select('
	    COUNT(DISTINCT vendas.`idVendas`) AS qtdVendas,	
	    ROUND(SUM(itens_de_vendas.`subTotal`), 2) AS total,
        ROUND(SUM(`itens_de_vendas`.`prod_preco_custo` * `quantidade`), 2) AS  custo, 
        `usuarios`.`usuario_nome` AS vendedor ');

	    $this->db->join('usuarios ','`vendas`.`usuarios_id` = `usuarios`.`usuario_id` ');
	    $this->db->join('`itens_de_vendas` ','`itens_de_vendas`.`vendas_id` = vendas.`idVendas` ');
	    $this->db->where('venda_visivel',1);
	  
	   	if ($loja <> 'todas') {
			$this->db->like('`itens_de_vendas`.`filial`', $loja);
		} 	    
	   
        if ($dataInicio != '1970-01-01' and  $dataFim != '1970-01-01') {
          $this->db->where('dataVenda >=', $dataInicio);
          $this->db->where('dataVenda <=', $dataFim);
        }

        if ($vedendor != NULL) {
          $this->db->where('`vendas`.`usuarios_id`', $vedendor);
        }

        if ($ordenacao == '1') {
          $this->db->order_by('usuarios.usuario_nome', 'asc');
        }else{
          $this->db->order_by('usuarios.usuario_nome', 'desc');
        }

        $this->db->group_by('`vendas`.`usuarios_id`');
	    $this->db->order_by('total', 'desc');

        return $this->db->get('vendas')->result_array();
	}

	public function vendedoresCustoGrupoDetalhe($dataInicio, $dataFim, $vedendor)
	{
		$base = explode('_', BDCAMINHO);
		$this->db->select('  `categoria_prod_descricao` AS grupo,
		ROUND(SUM(`subTotal`), 2) AS total, 
		ROUND(SUM(prod_preco_custo  * `quantidade`), 2) AS custo ');
		$this->db->join('vendas','vendas.`idVendas` = itens_de_vendas.`vendas_id`' );
		$this->db->join('produto','produto.`produto_id` = `itens_de_vendas`.`produtos_id`' );
		$this->db->join('categoria_produto','produto.`produto_categoria_id` = categoria_produto.`categoria_prod_id`' );
		$this->db->join('financeiro ','`vendas`.`idVendas` = `financeiro`.`vendas_id`' , 'left');
		$this->db->where('venda_visivel',1);
		// $this->db->where('faturado',1);
		$this->db->where('financeiro.`financeiro_visivel`',1);
        $this->db->where('financeiro.`financeiro_forma_pgto` is NOT NULL', NULL, FALSE);
        $this->db->where('financeiro.`financeiro_tipo`', 'receita');
        $this->db->where('`itens_de_vendas`.filial', $base[2]);
        $this->db->where('dataVenda >=', $dataInicio);
        $this->db->where('dataVenda <=', $dataFim);

        if ($vedendor != NULL) {
        	$this->db->where('`vendas`.`usuarios_id`', $vedendor);
        }
        $this->db->group_by('categoria_prod_descricao');
        return $this->db->get('itens_de_vendas')->result_array();

	}



	public function vendedoresCelCustomizadas($dataInicio, $dataFim, $vendedor, $loja )
	{		

	   // $loja = BDCAMINHO; $loja = explode("_", $loja);		
	    $this->db->select('	    	
	    COUNT(DISTINCT `itens_de_vendas`.`imei_valor`) AS qtdItens,
	    ROUND(SUM(itens_de_vendas.`subTotal`), 2) AS total,
        ROUND(SUM(`itens_de_vendas`.`prod_preco_custo` * `quantidade`), 2) AS  custo, 
        `usuarios`.`usuario_nome` AS vendedor ');

	    $this->db->join('usuarios ','`vendas`.`usuarios_id` = `usuarios`.`usuario_id` ');
	    $this->db->join('`itens_de_vendas` ','`itens_de_vendas`.`vendas_id` = vendas.`idVendas` ');
	    $this->db->where('venda_visivel',1);
	    $this->db->where('`itens_de_vendas`.`imei_valor` <> ""');

	  
	   	if ($loja <> 'todas') {
			$this->db->like('`itens_de_vendas`.`filial`', $loja);
		} 	    
	   
        if ($dataInicio != '1970-01-01' and  $dataFim != '1970-01-01') {
          $this->db->where('dataVenda >=', $dataInicio);
          $this->db->where('dataVenda <=', $dataFim);
        }

        if ($vendedor != NULL) {
          $this->db->where('`vendas`.`usuarios_id`', $vedendor);
        }

        // if ($ordenacao == '1') {
        //   $this->db->order_by('usuarios.usuario_nome', 'asc');
        // }else{
        //   $this->db->order_by('usuarios.usuario_nome', 'desc');
        // }

        $this->db->group_by('`vendas`.`usuarios_id`');
	    $this->db->order_by('total', 'desc');

        return $this->db->get('vendas')->result_array();
	}


	public function vendedoresCelCustoGrupoDetalhe($dataInicio, $dataFim, $vendedor,  $loja )
	{
		// $base = explode('_', BDCAMINHO);
		$this->db->select('  `categoria_prod_descricao` AS grupo,
		ROUND(SUM(`subTotal`), 2) AS total, 
		ROUND(SUM(prod_preco_custo  * `quantidade`), 2) AS custo ');
		$this->db->join('vendas','vendas.`idVendas` = itens_de_vendas.`vendas_id`' );
		$this->db->join('produto','produto.`produto_id` = `itens_de_vendas`.`produtos_id`' );
		$this->db->join('categoria_produto','produto.`produto_categoria_id` = categoria_produto.`categoria_prod_id`' );
     	$this->db->where('venda_visivel',1);             

        if ($loja <> 'todas') {
			$this->db->like('`itens_de_vendas`.`filial`', $loja);
		} 	  

        $this->db->where('`itens_de_vendas`.`imei_valor` <> ""');
        $this->db->where('dataVenda >=', $dataInicio);
        $this->db->where('dataVenda <=', $dataFim);

        if ($vendedor != NULL) {
        	$this->db->where('`vendas`.`usuarios_id`', $vendedor);
        }
        $this->db->group_by('categoria_prod_descricao');
        return $this->db->get('itens_de_vendas')->result_array();

	}

      ######## FIM RELATORIO MENU VENDEDORES ########

	  ######## RELATORIO MENU SIMULACAO DE COMPRA ########

    public function simulacaoCompra_padrao($dataInicio, $dataFim, $lojas, $grupo)
	{	
        $base = explode('_', BDCAMINHO);
        $basedestino = $base[0].'_'.$base[1].'_';


	    $this->db->select('  `produtos_id`,`produto_descricao`,`quantidade`,"'.strtoupper($lojas).'" AS Filial ');
	    $this->db->join( $basedestino.$lojas.'`.`itens_simulacao_compra`','`simulacao_compra`.`simulacao_compra_id` = `itens_simulacao_compra`.`simulacao_compra_id`');
	    $this->db->join('produto ','`produto`.`produto_id` = `itens_simulacao_compra`.`produtos_id`' , 'left');

        if ($dataInicio != '1970-01-01' and  $dataFim != '1970-01-01') {
          $this->db->where('simulacao_compra_data >=', $dataInicio);
          $this->db->where('simulacao_compra_data <=', $dataFim);
        }

       $this->db->where('produto_categoria_id', $grupo);
       $this->db->where('simulacao_compra_visivel', 1);

        return $this->db->get( $basedestino.$lojas.'`.`simulacao_compra')->result_array();
	}



	  ######## FIM RELATORIO MENU SIMULACAO DE COMPRA ########


	  ######## INICIO RELATORIO DE COMPRA ########

	public function totalComprasMesAtual()
	{
        $data = date('m');
        $this->db->select(' SUM((`item_movimentacao_produto_custo` * `item_movimentacao_produto_quantidade`)) as total');
		$this->db->join('itens_movimentacao_produtos ','`movimentacao_produtos`.`movimentacao_produto_id` = `itens_movimentacao_produtos`.`item_movimentacao_produto_movimentacao_produto_id`');
		$this->db->where('movimentacao_produto_tipo', 'Entrada');
		$this->db->where('MONTH(movimentacao_produto_data_cadastro)', $data);
		$this->db->where('YEAR(movimentacao_produto_data_cadastro) = YEAR(CURDATE())');
		$this->db->where('movimentacao_produto_visivel', 1);
		$this->db->where('movimentacao_produto_status', 0);
	    $this->db->where('item_movimentacao_produto_visivel', 1);

	    return $this->db->get('movimentacao_produtos')->result();		
	}


	public function totalComprasMesAnterior()
	{
        $data = date('m', strtotime('last day of last month'));
        $this->db->select(' SUM((`item_movimentacao_produto_custo` * `item_movimentacao_produto_quantidade`)) as total');
		$this->db->join('itens_movimentacao_produtos ','`movimentacao_produtos`.`movimentacao_produto_id` = `itens_movimentacao_produtos`.`item_movimentacao_produto_movimentacao_produto_id`');
		$this->db->where('movimentacao_produto_tipo', 'Entrada');
		$this->db->where('MONTH(movimentacao_produto_data_cadastro)', $data);
		$this->db->where('YEAR(movimentacao_produto_data_cadastro) = YEAR(CURDATE())');
		$this->db->where('movimentacao_produto_visivel', 1);
		$this->db->where('movimentacao_produto_status', 0);
	    $this->db->where('item_movimentacao_produto_visivel', 1);

	    return $this->db->get('movimentacao_produtos')->result();

	}

	public function compraMensal($mes)
	{
		if ($mes == 'atual') {
			$data = date('m');
		} else {
			$data = date('m', strtotime('last day of last month'));
		}

		$this->db->select('fornecedor_id, fornecedor_nome, SUM((`item_movimentacao_produto_custo` * `item_movimentacao_produto_quantidade`)) AS total ');

		$this->db->join('itens_movimentacao_produtos ','`movimentacao_produtos`.`movimentacao_produto_id` = `itens_movimentacao_produtos`.`item_movimentacao_produto_movimentacao_produto_id`');
		$this->db->join('fornecedores ','`movimentacao_produtos`.`movimentacao_fornecedor_id` = `fornecedores`.`fornecedor_id`');

		
		$this->db->where('movimentacao_produto_tipo', 'Entrada');
		$this->db->where('MONTH(movimentacao_produto_data_cadastro)', $data);
		$this->db->where('YEAR(movimentacao_produto_data_cadastro) = YEAR(CURDATE())');		
		$this->db->where('movimentacao_produto_visivel', 1);
		$this->db->where('movimentacao_produto_status', 0);
	    $this->db->where('item_movimentacao_produto_visivel', 1);
	    $this->db->group_by('fornecedor_nome');

	    return $this->db->get('movimentacao_produtos')->result();

	}    

	public function compraPagaMensal($mes)
	{
		if ($mes == 'atual') {
			$data = date('m');
		} else {
			$data = date('m', strtotime('last day of last month'));
		}

		$this->db->select('fornecedor_id, fornecedor_nome, SUM((valor_baixa)) AS total ');
		
		$this->db->join('fornecedores ','`baixar_compra`.`id_fornecedor` = `fornecedores`.`fornecedor_id`');

		$this->db->where('MONTH(data_baixa)', $data);	
	
	    $this->db->group_by('fornecedor_nome');

	    return $this->db->get('baixar_compra')->result();

	}    


	public function comprasCustomizadas($dataInicio, $dataFim)
	{
		$this->db->select(' fornecedor_nome, SUM((`item_movimentacao_produto_custo` * `item_movimentacao_produto_quantidade`)) AS total ');

		$this->db->join('itens_movimentacao_produtos ','`movimentacao_produtos`.`movimentacao_produto_id` = `itens_movimentacao_produtos`.`item_movimentacao_produto_movimentacao_produto_id`');
		$this->db->join('fornecedores ','`movimentacao_produtos`.`movimentacao_fornecedor_id` = `fornecedores`.`fornecedor_id`');

		
		$this->db->where('movimentacao_produto_tipo', 'Entrada');
		$this->db->where('movimentacao_produto_data_cadastro BETWEEN "'. date('Y-m-d', strtotime($dataInicio)). '" and "'. date('Y-m-d', strtotime($dataFim)).'"');
		
		$this->db->where('movimentacao_produto_visivel', 1);
		$this->db->where('movimentacao_produto_status', 0);
	    $this->db->where('item_movimentacao_produto_visivel', 1);
	    $this->db->group_by('fornecedor_nome');
	    $this->db->order_by('fornecedor_nome');


	    return $this->db->get('movimentacao_produtos')->result();

	}

	public function fornecedorComprasCustomizadasOLD($fornecedor)
	{
		$this->db->select(' movimentacao_produto_saida_filial, SUM((`item_movimentacao_produto_custo` * `item_movimentacao_produto_quantidade`)) AS total ');

		$this->db->join('itens_movimentacao_produtos ','`movimentacao_produtos`.`movimentacao_produto_id` = `itens_movimentacao_produtos`.`item_movimentacao_produto_movimentacao_produto_id`');
		
		
		$this->db->where('movimentacao_produto_tipo', 'Saida');
		$this->db->where('movimentacao_produto_data_cadastro BETWEEN "'. date('Y-m-d', strtotime($dataInicio)). '" and "'. date('Y-m-d', strtotime($dataFim)).'"');
		
		$this->db->where('movimentacao_produto_visivel', 1);
		$this->db->where('movimentacao_produto_status', 0);
	    $this->db->where('item_movimentacao_produto_visivel', 1);
	    $this->db->group_by('movimentacao_produto_saida_filial');


	    return $this->db->get('movimentacao_produtos')->result();

	}


    public function fornecedorComprasCustomizadas($fornecedor)
	{
		$this->db->select('fornecedores.fornecedor_id,
			fornecedores.fornecedor_nome AS fornecedor,
			SUM(itens_movimentacao_produtos.item_movimentacao_produto_custo) AS valor_total_produtos,
			COALESCE(baixar_compra_agg.valor_pago, 0) AS valor_pago,
			SUM(itens_movimentacao_produtos.item_movimentacao_produto_custo) - COALESCE(baixar_compra_agg.valor_pago, 0) AS divida,
			COALESCE(quantidade_aparelhos_estoque_subquery.quantidade_aparelhos, 0) AS quantidade_aparelhos_estoque,
			COALESCE(quantidade_aparelhos_estoque_subquery.valor_total_estoque, 0) AS valor_total_estoque');
		$this->db->from('movimentacao_produtos');
		$this->db->join('itens_movimentacao_produtos', 'movimentacao_produtos.movimentacao_produto_id = itens_movimentacao_produtos.item_movimentacao_produto_movimentacao_produto_id');
		$this->db->join('fornecedores', 'movimentacao_produtos.movimentacao_fornecedor_id = fornecedores.fornecedor_id');
		$this->db->join('(SELECT 
					 subquery.movimentacao_fornecedor_id,
					 COUNT(subquery.imei_valor) AS quantidade_aparelhos,
					 SUM(subquery.item_movimentacao_produto_custo) AS valor_total_estoque
			   FROM (SELECT 
			        mp.movimentacao_fornecedor_id,
			        imp.item_movimentacao_produto_custo,
			        idm.imei_valor,
			        @row_num := IF(@prev_movimentacao_produto_id = mp.movimentacao_produto_id AND @prev_imei_valor = idm.imei_valor, @row_num + 1, 1) AS row_num,
			        @prev_movimentacao_produto_id := mp.movimentacao_produto_id,
			        @prev_imei_valor := idm.imei_valor 
			      FROM
			        (SELECT @row_num := 0, @prev_movimentacao_produto_id := NULL, @prev_imei_valor := NULL) AS vars,
			        movimentacao_produtos mp 
			        INNER JOIN itens_movimentacao_produtos imp 
			          ON mp.movimentacao_produto_id = imp.item_movimentacao_produto_movimentacao_produto_id 
			        INNER JOIN itens_de_imei idm 
			          ON mp.movimentacao_produto_id = idm.movimentacao_id 
			      WHERE idm.vendas_id IS NULL 
			        AND idm.imei_visivel = 1 
			        AND mp.movimentacao_produto_tipo = "Entrada" 
			        AND mp.movimentacao_produto_visivel = 1 
			      ORDER BY mp.movimentacao_produto_id, idm.imei_valor) AS subquery 
			   WHERE subquery.row_num = 1
			   GROUP BY subquery.movimentacao_fornecedor_id) AS quantidade_aparelhos_estoque_subquery', 'movimentacao_produtos.movimentacao_fornecedor_id = quantidade_aparelhos_estoque_subquery.movimentacao_fornecedor_id', 'left');
		$this->db->join('(SELECT 
					 id_fornecedor, 
					 SUM(valor_baixa) AS valor_pago 
			   FROM 
					 baixar_compra 
			   GROUP BY 
					 id_fornecedor) AS baixar_compra_agg', 'movimentacao_produtos.movimentacao_fornecedor_id = baixar_compra_agg.id_fornecedor', 'left');
		$this->db->where('movimentacao_produto_tipo', 'Entrada');
		$this->db->where('movimentacao_produto_visivel', 1);
		
		if ($fornecedor) {
			$this->db->where('fornecedores.fornecedor_id', $fornecedor);
		}
	
		$this->db->group_by('fornecedores.fornecedor_id, fornecedores.fornecedor_nome, baixar_compra_agg.valor_pago, quantidade_aparelhos_estoque_subquery.quantidade_aparelhos, quantidade_aparelhos_estoque_subquery.valor_total_estoque');
	
		$query = $this->db->get();
		return $query->result();
	}

	public function todosFornecedoresComprasCustomizadas()
	{
		$this->db->select('fornecedores.fornecedor_id,
		fornecedores.fornecedor_nome AS fornecedor,
		SUM(itens_movimentacao_produtos.item_movimentacao_produto_custo) AS valor_total_produtos,
		COALESCE(baixar_compra_agg.valor_pago, 0) AS valor_pago,
		SUM(itens_movimentacao_produtos.item_movimentacao_produto_custo) - COALESCE(baixar_compra_agg.valor_pago, 0) AS divida,
		COALESCE(quantidade_aparelhos_estoque_subquery.quantidade_aparelhos, 0) AS quantidade_aparelhos_estoque,
		COALESCE(quantidade_aparelhos_estoque_subquery.valor_total_estoque, 0) AS valor_total_estoque');
		$this->db->from('movimentacao_produtos');
		$this->db->join('itens_movimentacao_produtos', 'movimentacao_produtos.movimentacao_produto_id = itens_movimentacao_produtos.item_movimentacao_produto_movimentacao_produto_id');
		$this->db->join('fornecedores', 'movimentacao_produtos.movimentacao_fornecedor_id = fornecedores.fornecedor_id');
		$this->db->join('(SELECT 
					 subquery.movimentacao_fornecedor_id,
					 COUNT(subquery.imei_valor) AS quantidade_aparelhos,
					 SUM(subquery.item_movimentacao_produto_custo) AS valor_total_estoque
			   FROM (
					 SELECT 
						 mp.movimentacao_fornecedor_id,
						 imp.item_movimentacao_produto_custo,
						 idm.imei_valor,
						 ROW_NUMBER() OVER(PARTITION BY mp.movimentacao_produto_id, idm.imei_valor ORDER BY mp.movimentacao_produto_id) AS row_num
					 FROM movimentacao_produtos mp
					 INNER JOIN itens_movimentacao_produtos imp 
						 ON mp.movimentacao_produto_id = imp.item_movimentacao_produto_movimentacao_produto_id
					 INNER JOIN itens_de_imei idm 
						 ON mp.movimentacao_produto_id = idm.movimentacao_id
					 WHERE idm.vendas_id IS NULL
					   AND idm.imei_visivel = 1
					   AND mp.movimentacao_produto_tipo = "Entrada"
					   AND mp.movimentacao_produto_visivel = 1
			   ) AS subquery
			   WHERE subquery.row_num = 1
			   GROUP BY subquery.movimentacao_fornecedor_id) AS quantidade_aparelhos_estoque_subquery', 'movimentacao_produtos.movimentacao_fornecedor_id = quantidade_aparelhos_estoque_subquery.movimentacao_fornecedor_id', 'left');
		$this->db->join('(SELECT 
					 id_fornecedor, 
					 SUM(valor_baixa) AS valor_pago 
			   FROM 
					 baixar_compra 
			   GROUP BY 
					 id_fornecedor) AS baixar_compra_agg', 'movimentacao_produtos.movimentacao_fornecedor_id = baixar_compra_agg.id_fornecedor', 'left');
		$this->db->where('movimentacao_produto_tipo', 'Entrada');
		$this->db->where('movimentacao_produto_visivel', 1);
		$this->db->group_by('fornecedores.fornecedor_id, fornecedores.fornecedor_nome, baixar_compra_agg.valor_pago, quantidade_aparelhos_estoque_subquery.quantidade_aparelhos, quantidade_aparelhos_estoque_subquery.valor_total_estoque');

		$query = $this->db->get();
		return $query->result();

	}


	public function comprasSaidaCustomizadas($dataInicio, $dataFim)
	{
		$dtI = date('Y-m-d', strtotime($dataInicio));
		$dtF = date('Y-m-d', strtotime($dataFim));

		// $sql = ' SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE "%megacell%" ';
		$sql = "SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE '%_".GRUPOLOJA."_%' ";
        $bases =  $this->db->query($sql)->result();
        $sql = ''; 
        $todaslojadestino = '';       


        $sql = ' SELECT movimentacao_fornecedor_id AS idForn, `fornecedor_nome` as Fornecedor, ';

        foreach ($bases as $b) {        

        $loja = explode('_', $b->schema_name);
        $lojadestino = $loja[2]; 

        $todaslojadestino = $todaslojadestino. ' + IF('.$lojadestino.' IS NOT NULL, '.$lojadestino.', 0)' ; 	

        $sql = $sql. ' 
        	 (SELECT SUM( ( `item_movimentacao_produto_custo` * `item_movimentacao_produto_quantidade` ) ) AS total
			 FROM
			  `movimentacao_produtos` 
			  JOIN `itens_movimentacao_produtos` 
			    ON `movimentacao_produtos`.`movimentacao_produto_id` = `itens_movimentacao_produtos`.`item_movimentacao_produto_movimentacao_produto_id`
			  WHERE `movimentacao_produto_tipo` = "Saida" 
			  AND  movimentacao_fornecedor_id = idForn
			  AND `movimentacao_produto_saida_filial` = "'.$lojadestino.'"
			  AND `movimentacao_produto_saida_tipo` = "filial"
			  AND `movimentacao_produto_data_cadastro` BETWEEN "'.$dtI.'" AND  "'.$dtF.'" 
			  AND `movimentacao_produto_visivel` = 1 
			  AND `movimentacao_produto_status` = 0 
			  AND `item_movimentacao_produto_visivel` = 1 ) AS '.$lojadestino.', ';

        }

        $sql = $sql. ' 
        	(SELECT ROUND(('.$todaslojadestino.'),2) AS valor ) AS total ';

    
		$sql = $sql. ' FROM
		  `movimentacao_produtos` 
		  JOIN `itens_movimentacao_produtos` 
		    ON `movimentacao_produtos`.`movimentacao_produto_id` = `itens_movimentacao_produtos`.`item_movimentacao_produto_movimentacao_produto_id`
		  
		  JOIN `fornecedores`
		    ON fornecedores.`fornecedor_id` = movimentacao_produtos.`movimentacao_fornecedor_id`
		    
		WHERE `movimentacao_produto_tipo` = "Saida" 
		  AND `movimentacao_produto_saida_tipo` = "filial"
		  AND `movimentacao_produto_data_cadastro` BETWEEN  "'.$dtI.'" AND  "'.$dtF.'" 
		  AND `movimentacao_produto_visivel` = 1 
		  AND `movimentacao_produto_status` = 0 
		  AND `item_movimentacao_produto_visivel` = 1 
		GROUP BY movimentacao_fornecedor_id 
		order by fornecedor_nome ';


		// var_dump($sql);die();
			
        return $this->db->query($sql)->result();
       
	}


	 ######## FIM RELATORIO DE COMPRA ########

   public function autoCompleteClientes($termo)
	{
		$this->db->select('*');
        $this->db->limit(5);
        $this->db->like('cliente_nome', $termo);
        $this->db->where('cliente_visivel', 1);
        $query = $this->db->get('clientes')->result();

        if(count($query) > 0){
            foreach ($query as $row){
                $row_set[] = array('label'=>$row->cliente_nome,'id'=>$row->cliente_id);
            }
            echo json_encode($row_set);
        }
	}

   public function autoCompleteUsuarios($termo)
    {
      $this->db->select('*');
          $this->db->limit(5);
          $this->db->like('usuario_nome', $termo);
          $this->db->where('usuario_visivel', 1);
          $query = $this->db->get('usuarios')->result();

          if(count($query) > 0){
              foreach ($query as $row){
                  $row_set[] = array('label'=>$row->usuario_nome,'id'=>$row->usuario_id);
              }
              echo json_encode($row_set);
          }
    }

   public function autoCompleteCentro($termo)
    {
      $this->db->select('*');
          $this->db->limit(5);
          $this->db->like('categoria_fina_descricao', $termo);
          $this->db->where('categoria_fina_visivel', 1);
          $query = $this->db->get('categoria_financeiro')->result();

          if(count($query) > 0){
              foreach ($query as $row){
                  $row_set[] = array('label'=>$row->categoria_fina_descricao,'id'=>$row->categoria_fina_id);
              }
              echo json_encode($row_set);
          }
    }

    public function getAllFilias()
    {
        $sql = "SHOW DATABASES";

        $result = $this->db->query($sql);

        $bases = array();
        if ($result->num_rows() > 0) {
            foreach (($result->result_array()) as $row) {
              if (strpos($row['Database'], '_'.GRUPOLOJA.'_') !== false && $row['Database'] !=
                'wdm_clientes') {
              	$lojas = explode("_".GRUPOLOJA."_", $row['Database']);
                 
                   $bases[]['lojas'] = $lojas[1];
                
              }
            }

          return $bases;
        }
        return FALSE;
    }

   public function getAllParametros()
    {
        $this->db->select('parametro_cart_debito, parametro_cart_credito');
		return $this->db->get('parametro')->result();
    }

    ######## RELATORIO MENU INCONSISTENCIA ########
    public function inconsistenciaProdImei($grupo){
        
    $query = "SELECT produto_descricao, `produto_estoque`, 
				( SELECT 
				  COUNT(`imei_valor`) 
				FROM
				  `itens_de_imei` 
				WHERE `categoria_prod_id` = ".$grupo." 
				  AND imei_visivel = 1 
				  AND vendas_id IS NULL 
				  AND produtos_id = produto.`produto_id`
				  GROUP BY `produtos_id`  ) AS estoque_imei FROM `produto`
				WHERE `produto_categoria_id` = ".$grupo."
				AND `produto_visivel` = 1 
				AND `produto`.`produto_estoque` >= 1 ";

		
			
        return $this->db->query($query)->result_array(); 
    }


    ######## RELATORIO MENU INCONSISTENCIA ########


    ######## RELATORIO MENU CHAT ########
	public function chat($situacao)
	{
		$this->db->select(' COUNT(*) AS qtd, chave, `categoria_prod_descricao` AS grupo ');
		$this->db->join(' `categoria_produto`',' `chat_mensagem`.`categotia_id` = `categoria_produto`.`categoria_prod_id` ');
		$this->db->where(' situacao ', $situacao);
		
		$this->db->order_by(' `categotia_id` ');
		$this->db->order_by('  `qtd` ', 'DESC');
		$this->db->group_by(' chave ');
		
		return $this->db->get('`chat_mensagem`')->result();
	}

	public function chatCustom($dataInicio, $dataFim, $situacao)
	{
		$this->db->select(' COUNT(*) AS qtd, chave, `categoria_prod_descricao` AS grupo ');
		$this->db->join(' `categoria_produto`',' `chat_mensagem`.`categotia_id` = `categoria_produto`.`categoria_prod_id` ');
		$this->db->where(' situacao ', $situacao);
		$this->db->where(" `data_operacao` BETWEEN '{$dataInicio}' AND '{$dataFim}' ");
		
		$this->db->order_by(' `categotia_id` ');
		$this->db->order_by('  `qtd` ', 'DESC');
		$this->db->group_by(' chave ');
		
		return $this->db->get('`chat_mensagem`')->result();
	}


	public function chatEncontradoTotal()
	{	
		$this->db->select(' COUNT(*) ');
		$this->db->where('situacao', 'Encontrado');		
		$this->db->from('chat_mensagem');
		return $this->db->count_all_results();
	}

	public function chatNaoEncontradoTotal()
	{	
		$this->db->select(' COUNT(*) ');
		$this->db->where('situacao', 'Nao Encontrado');		
		$this->db->from('chat_mensagem');
		return $this->db->count_all_results();
	}
	######## FIM RELATORIO MENU CHAT ########


	######## RELATORIO MENU FINANCEIRO ########

	public function financeiroCuston()
	{
		$this->db->select(' COUNT(*) AS qtd, chave, `categoria_prod_descricao` AS grupo ');
		$this->db->join(' `categoria_produto`',' `chat_mensagem`.`categotia_id` = `categoria_produto`.`categoria_prod_id` ');
		$this->db->where(' situacao ', $situacao);
		
		$this->db->order_by(' `categotia_id` ');
		$this->db->order_by('  `qtd` ', 'DESC');
		$this->db->group_by(' chave ');
		
		return $this->db->get('`chat_mensagem`')->result();
	}


	public function financeiroRapid($situacao){
        
        if ($situacao == 'dia') {
        	$dataInicial = date('Y-m-d');
            $dataFinal = date("Y-m-d");
        }

        if ($situacao == 'mes') {
        	$dataInicial = date('Y-m-01');
            $dataFinal = date("Y-m-t");
        }

        if ($situacao == 'mesPassado') {
  
        	$dataInicial = date('Y-m-01', strtotime('-1 months', strtotime(date('Y-m-01'))));
            $dataFinal = date('Y-m-t', strtotime('-1 months', strtotime(date('Y-m-t'))));
        }
      

        $query = "SELECT 
			data_vencimento AS dia,
			(SELECT  ROUND(SUM(`financeiro_valor`), 2 ) AS `valorTotal` 
			FROM `financeiro` WHERE dia = data_vencimento AND financeiro_tipo = 'receita'
			AND `financeiro_baixado` = 1 
			AND `financeiro_visivel` = 1) AS receita,

			(SELECT  ROUND(SUM(`financeiro_valor`), 2) AS `valorTotal` 
  			FROM  `financeiro`  WHERE dia = data_pagamento AND `data_vencimento` <> data_pagamento
    		AND financeiro_tipo = 'receita'  AND `financeiro_baixado` = 1  AND `financeiro_visivel` = 1) AS pag_fiado,

			(SELECT ROUND(SUM(`financeiro_valor`), 2) AS `valorTotal` 
			  FROM `financeiro` 
			WHERE  dia = data_vencimento 
			AND	`financeiro_baixado` = 0
			AND `financeiro_visivel` = 1
			AND financeiro_tipo = 'receita') AS comp_fiado,      

			(SELECT  ROUND(SUM(`financeiro_valor`), 2 ) AS `valorTotal` 
			FROM `financeiro` WHERE dia = data_vencimento AND financeiro_tipo = 'despesa' AND `financeiro_visivel` = 1) AS despesa,
		
			(SELECT ROUND( SUM(`produto_avaria_preco_vendido` * `produto_avarias_quantidade`), 2) AS `valorTotal` 
			FROM `produto_avarias` 
			INNER JOIN `vendas` ON `produto_avarias`.`venda_id` = vendas.`idVendas`
			WHERE (dia = `produto_avaria_cadastro` AND dia <> dataVenda)
			AND `produto_avaria_motivo` IN (2, 3)) AS reembolso,

			(SELECT ROUND(SUM(`valor`), 2) AS `valor`  FROM `sangria_pdv` WHERE `dataRetirada` >= dia ) AS sangria,
  
  			(SELECT  (  (   (receita + IF(pag_fiado IS NOT NULL, pag_fiado, 0)) - (  IF(despesa IS NOT NULL, despesa, 0) + IF(reembolso IS NOT NULL, reembolso, 0)
        	) ) - IF(sangria IS NOT NULL, sangria, 0) )) AS saldo 
			
			FROM `financeiro`
			WHERE data_vencimento BETWEEN ? AND ? 
			AND `financeiro_visivel` = 1
			GROUP BY data_vencimento  ";

		  //var_dump($query);die();	
			
        return $this->db->query($query, array($dataInicial,$dataFinal))->result(); 
    }



    public function financeiroCustom($dataInicio, $dataFim, $pdataInicio, $pdataFim, $categoria = null, $cliente = null,$fornecedor = null, $formaPagamento = null,  $tipo = null, $situacao = null){
        
        $whereTipo = "";
        $whereSituacao = "";
        $clie_forn = ""; 
        $whereCategoriaIn = "";
        $where = "";
        $wherep = "";
        

        if ($categoria) {  
        // $dados = 999;
        // foreach ($categoria as $valor) {        
        //     $dados = $dados.','.$valor;       
        // }
            $categoria = " AND categoria_fin_id in({$categoria})";          
        }

        if ($cliente) {  
        // $dados = 999;
        // foreach ($cliente as $valor) {        
        //     $dadosC = $dados.','.$valor;       
        // }
            $cliente = " AND financeiro_forn_clie_id in({$cliente})";          
        }

        if ($fornecedor) {
        // $dados = 999;
        // foreach ($fornecedor as $valor) {        
        //     $dadosF = $dados.','.$valor;       
        // }    
            $fornecedor = " AND financeiro_forn_clie_id in({$fornecedor})";     
        }

        if (($cliente) and ($fornecedor)) { 
            $clie_forn = "AND financeiro_forn_clie_id in({$cliente}) OR financeiro_forn_clie_id in({$fornecedor}) "; 
            $cliente = "";
            $fornecedor = "";

        }

        if($formaPagamento != 'todos'){
            $whereForPag = "AND financeiro_forma_pgto LIKE '%{$formaPagamento}%'";
        }else{
             $whereForPag = '';
        }   


        if($tipo == 'receita'){
            $whereTipo = "AND financeiro_tipo = 'receita'";
        }

        if($tipo == 'despesa'){
            $whereTipo = "AND financeiro_tipo = 'despesa'";
        }
        if($situacao == 'pendente'){
            $whereSituacao = "AND financeiro_baixado = 0";
        }
        if($situacao == 'pago'){
            $whereSituacao = "AND financeiro_baixado = 1";
        }

        if($dataInicio <> null OR $dataFim <> null ){
           $where = " AND ( data_vencimento BETWEEN '" . $dataInicio ."' AND '". $dataFim ."' ) ";   
        } 
         
        if($pdataInicio <> null OR $pdataFim <> null ){
            $wherep = " AND ( data_pagamento BETWEEN '" . $pdataInicio ."' AND '". $pdataFim ."' ) ";  

        }

        
        
        $query = " SELECT financeiro_forn_clie, financeiro_descricao, financeiro_tipo, financeiro_valor, data_vencimento, data_pagamento, financeiro_baixado, financeiro_forma_pgto, categoria_cart_descricao ".
                 " FROM  financeiro  LEFT JOIN categoria_cartao ON financeiro.`financeiro_bandeira_cart` = categoria_cartao.`categoria_cart_id` ".
                 " WHERE financeiro_visivel = 1   $where $wherep $whereTipo $categoria $cliente $fornecedor $clie_forn $whereSituacao $whereForPag order by data_vencimento, financeiro_descricao";
                 
        return $this->db->query($query, array($dataInicio,$dataFim))->result();

    }


    public function financeiroCustomizadas($dataInicio, $dataFim, $vedendor, $cliente, $centro, $ordenacao){
        
        $query = "SELECT 
			data_vencimento AS dia,
			(SELECT  ROUND(SUM(`financeiro_valor`), 2 ) AS `valorTotal` 
			FROM `financeiro` WHERE dia = data_vencimento AND financeiro_tipo = 'receita' AND `financeiro_visivel` = 1) AS receita,
			(SELECT  ROUND(SUM(`financeiro_valor`), 2 ) AS `valorTotal` 
			FROM `financeiro` WHERE dia = data_vencimento AND financeiro_tipo = 'despesa' AND `financeiro_visivel` = 1) AS despesa,
		
			(SELECT ROUND( SUM(`produto_avaria_preco_vendido` * `produto_avarias_quantidade`), 2) AS `valorTotal` 
			FROM `produto_avarias` 
			INNER JOIN `vendas` ON `produto_avarias`.`venda_id` = vendas.`idVendas`
			WHERE (dia = `produto_avaria_cadastro` AND dia <> dataVenda)
			AND `produto_avaria_motivo` IN (2, 3)) AS reembolso,

			(SELECT  ( receita - (IF(despesa IS NOT NULL, despesa, 0) + IF(reembolso IS NOT NULL, reembolso, 0) ) )) AS saldo 
			FROM `financeiro`

			LEFT JOIN `vendas` 
			ON `vendas`.`idVendas` = `financeiro`.`vendas_id`";

		    if ($dataInicio != '1970-01-01' and  $dataFim != '1970-01-01') {
		       $query.= " WHERE data_vencimento BETWEEN '".$dataInicio."' AND '".$dataFim."'";
	        }else{
	           $query.= " WHERE data_vencimento BETWEEN '".date('Y-01-01')."' AND '". date('Y-12-31')."'";
	        }

	        if ($cliente != NULL) {
	           $query.= " AND `financeiro`.financeiro_forn_clie_id = ".$cliente."";
	        }

	        if ($vedendor != NULL) {
	           $query.= " AND `vendas`.usuarios_id = ".$vedendor."";
	        }

	        if ($centro != NULL) {
	           $query.= " AND `financeiro`.categoria_fin_id = ".$centro."";
	        }

			$query.= " AND `financeiro_visivel` = 1
			GROUP BY data_vencimento ";

			if ($ordenacao == '1') {
	           $query.= " ORDER BY data_vencimento ASC";
	        }else{
	           $query.= " ORDER BY data_vencimento DESC";
	        }
			
        return $this->db->query($query)->result_array(); 
    }


    public function financeiroCustomizadasItens($dataInicio, $dataFim, $vedendor, $cliente, $centro, $tipo){
        
        $query = "SELECT
					  data_vencimento AS dia,
					  financeiro.`financeiro_tipo`,
					  financeiro.`financeiro_descricao`,
					  financeiro.`financeiro_forn_clie`,
					  financeiro.`financeiro_valor`,
					  financeiro.`data_pagamento`,
					  financeiro.`financeiro_baixado`,
					  financeiro.`financeiro_forma_pgto`
					FROM
					  `financeiro`
					  LEFT JOIN `vendas`
					    ON `vendas`.`idVendas` = `financeiro`.`vendas_id`
					 WHERE `financeiro_visivel` = 1 ";

	        if ($cliente != NULL) {
	           $query.= " AND `financeiro`.financeiro_forn_clie_id = ".$cliente."";
	        }

	        if ($tipo != '1') {
	          if ($tipo == '2') {
	          	 $query.= " AND `financeiro`.financeiro_tipo = 'receita' ";
	          } else {
	          	 $query.= " AND `financeiro`.financeiro_tipo = 'despesa' ";
	          }
	        }

	        if ($vedendor != NULL) {
	           $query.= " AND `vendas`.usuarios_id = ".$vedendor."";
	        }

	        if ($centro != NULL) {
	           $query.= " AND `financeiro`.categoria_fin_id = ".$centro."";
	        }

			// $query.= " AND data_vencimento = '".$dia."'";
			
			if ($dataInicio != '1970-01-01' and  $dataFim != '1970-01-01') {
		       $query.= " AND data_vencimento BETWEEN '".$dataInicio."' AND '".$dataFim."'";
	        }else{
	           $query.= " AND data_vencimento BETWEEN '".date('Y-01-01')."' AND '". date('Y-12-31')."'";
	        }

			
        return $this->db->query($query)->result_array(); 
    }

	public function pegarCentroCusto()
	{
		$this->db->select('*');
		$this->db->where('categoria_fina_visivel', 1);
		return $this->db->get('categoria_financeiro')->result();
	}


    public function financeiroPrevisaoFaturamento($ano, $mes)
	{
		 $query = "SELECT
					  data_vencimento AS dia,
					  (SELECT
					    ROUND (SUM(`financeiro_valor`), 2) AS `valorTotal`
					  FROM
					    `financeiro`
					  WHERE dia = data_vencimento
					    AND financeiro_tipo = 'receita'
					    AND `financeiro_visivel` = 1) AS receita,
					  (SELECT
					    ROUND (SUM(`financeiro_valor`), 2) AS `valorTotal`
					  FROM
					    `financeiro`
					  WHERE dia = data_vencimento
					    AND financeiro_tipo = 'despesa'
					    AND `financeiro_visivel` = 1) AS despesa,
					(SELECT
					    ROUND (SUM(`subTotal`), 2) AS total
					    FROM
						    `vendas`
						INNER JOIN `itens_de_vendas`
						    ON `itens_de_vendas`.`vendas_id` = `vendas`.`idVendas`
						WHERE `venda_visivel` = 1
						    AND faturado = 1
						    AND '".$mes."' = MONTH (dataVenda)
						    AND YEAR (dataVenda) = '".$ano."'
						GROUP BY MONTH (dataVenda)
						ORDER BY MONTH (dataVenda)) AS total,
					(SELECT
						ROUND (SUM(prod_preco_custo * `quantidade`), 2 ) AS total
						FROM
						   `itens_de_vendas`
						INNER JOIN `vendas`
						    ON `itens_de_vendas`.`vendas_id` = `vendas`.`idVendas` 
						WHERE `venda_visivel` = 1
						    AND faturado = 1
						    AND '".$mes."' = MONTH (dataVenda)
						    AND YEAR (dataVenda) = '".$ano."'
						GROUP BY MONTH (dataVenda)
						ORDER BY MONTH (dataVenda)) AS custo
					FROM
					  `financeiro`
					  LEFT JOIN `vendas`
					    ON `vendas`.`idVendas` = `financeiro`.`vendas_id`

					  WHERE MONTH(data_vencimento) = '".$mes."'
					  AND YEAR(data_vencimento) = '".$ano."'
					  AND `financeiro_visivel` = 1
					GROUP BY data_vencimento
					ORDER BY data_vencimento ASC";

        return $this->db->query($query)->result(); 
	}
	
	public function financeiroFluxoCaixaReceita($ano, $periodo)
	{
		 $query = "SELECT categoria, "; 

			 if ($periodo == '1') {
			  	$query.= "
							  previstoreceita1,
							  realizadoreceita1,
							  previstoreceita2,
							  realizadoreceita2,
							  previstoreceita3,
							  realizadoreceita3,
							  previstoreceita4,
							  realizadoreceita4,
							  previstoreceita5,
							  realizadoreceita5,
							  previstoreceita6,
							  realizadoreceita6";
			 } else {
			  	$query.= "
					  	  previstoreceita7,
							  realizadoreceita7,
							  previstoreceita8,
							  realizadoreceita8,
							  previstoreceita9,
							  realizadoreceita9,
							  previstoreceita10,
							  realizadoreceita10,
							  previstoreceita11,
							  realizadoreceita11,
							  previstoreceita12,
							  realizadoreceita12";
			 }

     $query.= " FROM vw_fluxocaixareceita WHERE `vw_fluxocaixareceita`.`ano_previstoreceita` = '".$ano."'";

        return $this->db->query($query)->result(); 
	}


	public function financeiroFluxoCaixaReceitaAnterior($ano)
	{
		 $query = "SELECT 
		            SUM(previstoreceita1)  as receitaprevitoTotal1,
							  SUM(realizadoreceita1) as receitarealizadoTotal1,
							  SUM(previstoreceita2)  as receitaprevitoTotal2,
							  SUM(realizadoreceita2) as receitarealizadoTotal2,
							  SUM(previstoreceita3)  as receitaprevitoTotal3,
							  SUM(realizadoreceita3) as receitarealizadoTotal3,
							  SUM(previstoreceita4)  as receitaprevitoTotal4,
							  SUM(realizadoreceita4) as receitarealizadoTotal4,
							  SUM(previstoreceita5)  as receitaprevitoTotal5,
							  SUM(realizadoreceita5) as receitarealizadoTotal5,
							  SUM(previstoreceita6)  as receitaprevitoTotal6,
							  SUM(realizadoreceita6) as receitarealizadoTotal6
							  FROM vw_fluxocaixareceita WHERE `vw_fluxocaixareceita`.`ano_previstoreceita` = '".$ano."'";

        return $this->db->query($query)->result(); 
	}


	public function financeiroFluxoCaixaDespesaAnterior($ano)
	{
		 $query = "SELECT 
							  SUM(previstodespesa1)  as despesaprevitoTotal1,
							  SUM(realizadodespesa1) as despesarealizadoTotal1,
							  SUM(previstodespesa2)  as despesaprevitoTotal2,
							  SUM(realizadodespesa2) as despesarealizadoTotal2,
							  SUM(previstodespesa3)  as despesaprevitoTotal3,
							  SUM(realizadodespesa3) as despesarealizadoTotal3,
							  SUM(previstodespesa4)  as despesaprevitoTotal4,
							  SUM(realizadodespesa4) as despesarealizadoTotal4,
							  SUM(previstodespesa5)  as despesaprevitoTotal5,
							  SUM(realizadodespesa5) as despesarealizadoTotal5,
							  SUM(previstodespesa6)  as despesaprevitoTotal6,
							  SUM(realizadodespesa6) as despesarealizadoTotal6
							  FROM vw_fluxocaixadespesa WHERE `vw_fluxocaixadespesa`.`ano_previstodespesa` = '".$ano."'";

        return $this->db->query($query)->result(); 
	}


	public function financeiroFluxoCaixaDespesa($ano, $periodo)
	{
		 $query = "SELECT categoria, "; 

			 if ($periodo == '1') {
			  	$query.= "
							  previstodespesa1,
							  realizadodespesa1,
							  previstodespesa2,
							  realizadodespesa2,
							  previstodespesa3,
							  realizadodespesa3,
							  previstodespesa4,
							  realizadodespesa4,
							  previstodespesa5,
							  realizadodespesa5,
							  previstodespesa6,
							  realizadodespesa6";
			 } else {
			  	$query.= "
					  	  previstodespesa7,
							  realizadodespesa7,
							  previstodespesa8,
							  realizadodespesa8,
							  previstodespesa9,
							  realizadodespesa9,
							  previstodespesa10,
							  realizadodespesa10,
							  previstodespesa11,
							  realizadodespesa11,
							  previstodespesa12,
							  realizadodespesa12";
			 }

     $query.= " FROM vw_fluxocaixadespesa WHERE `vw_fluxocaixadespesa`.`ano_previstodespesa` = '".$ano."'";

        return $this->db->query($query)->result(); 
	}

	public function financeiroEmAberto($ano, $mes, $cliente, $tipo)
    {   
        $this->db->select("clientes.`cliente_id`, clientes.`cliente_nome` AS cliente, SUM(financeiro.`financeiro_valor`) AS valor, MONTH(financeiro.`data_vencimento`) AS mes,
        	  SUM((CASE WHEN MONTH(financeiro.`data_vencimento`) = 1 THEN financeiro.`financeiro_valor` ELSE 0 END)) AS `pendente1`,
					  SUM((CASE WHEN MONTH(financeiro.`data_vencimento`) = 2 THEN financeiro.`financeiro_valor` ELSE 0 END)) AS `pendente2`,
					  SUM((CASE WHEN MONTH(financeiro.`data_vencimento`) = 3 THEN financeiro.`financeiro_valor` ELSE 0 END)) AS `pendente3`,
					  SUM((CASE WHEN MONTH(financeiro.`data_vencimento`) = 4 THEN financeiro.`financeiro_valor` ELSE 0 END)) AS `pendente4`,
					  SUM((CASE WHEN MONTH(financeiro.`data_vencimento`) = 5 THEN financeiro.`financeiro_valor` ELSE 0 END)) AS `pendente5`,
					  SUM((CASE WHEN MONTH(financeiro.`data_vencimento`) = 6 THEN financeiro.`financeiro_valor` ELSE 0 END)) AS `pendente6`,
					  SUM((CASE WHEN MONTH(financeiro.`data_vencimento`) = 7 THEN financeiro.`financeiro_valor` ELSE 0 END)) AS `pendente7`,
					  SUM((CASE WHEN MONTH(financeiro.`data_vencimento`) = 8 THEN financeiro.`financeiro_valor` ELSE 0 END)) AS `pendente8`,
					  SUM((CASE WHEN MONTH(financeiro.`data_vencimento`) = 9 THEN financeiro.`financeiro_valor` ELSE 0 END)) AS `pendente9`,
					  SUM((CASE WHEN MONTH(financeiro.`data_vencimento`) = 10 THEN financeiro.`financeiro_valor` ELSE 0 END)) AS `pendente10`,
					  SUM((CASE WHEN MONTH(financeiro.`data_vencimento`) = 11 THEN financeiro.`financeiro_valor` ELSE 0 END)) AS `pendente11`,
					  SUM((CASE WHEN MONTH(financeiro.`data_vencimento`) = 12 THEN financeiro.`financeiro_valor` ELSE 0 END)) AS `pendente12`");
        $this->db->join('clientes ','`financeiro`.`financeiro_forn_clie_id` = `clientes`.`cliente_id` ');	
        $this->db->where('financeiro.`financeiro_baixado`', 0);
        $this->db->where('financeiro.`financeiro_visivel`', 1);
        $this->db->where('YEAR(financeiro.`data_vencimento`)', $ano);
        if ($tipo == 1) {
          $this->db->where('MONTH(financeiro.`data_vencimento`)', $mes);
        }
        if ($cliente != NULL) {
        	$this->db->where('`clientes`.`cliente_id`', $cliente);
        }     
        $this->db->group_by(' financeiro.`financeiro_forn_clie_id` ');
        $this->db->order_by('MONTH(financeiro.`data_vencimento`)', 'asc');

		return $this->db->get('financeiro')->result();
    }


	public function financeiroBaixaParcial($dataInicio, $dataFim, $cliente, $tipo)
  {  
  	if ($tipo == 1) { 
     $this->db->select('SUM(`valor_baixa`) AS valor_baixa, SUM(`valor_debito`) AS valor_debito, SUM(`valor_total`) AS valor_total, data_baixa, clientes.*, usuarios.*  ');	
    } else {
    $this->db->select('*');	   	
    }

		$this->db->join('clientes ','`baixar_conta`.`id_cliente` = `clientes`.`cliente_id` ');
		$this->db->join('usuarios ',' `baixar_conta`.`id_usuario` = `usuarios`.`usuario_id`' , 'left');
    if ($cliente != NULL) {
    	$this->db->where('`clientes`.`cliente_id`', $cliente);
    }
		if ($dataInicio != '1970-01-01' and  $dataFim != '1970-01-01') {
			$this->db->where(" DATE(`data_baixa`) BETWEEN '{$dataInicio}' AND '{$dataFim}' ");    
		}

		if ($tipo == 1) { 
			$this->db->group_by("id_cliente");		
		}

    $this->db->order_by('cliente_nome', 'asc');

		return $this->db->get('baixar_conta')->result();
    }


	


	######## FIM RELATORIO MENU FINANCEIRO ########


	######## RELATORIO MENU INCONSISTENCIA ########


	public function totalVendaSemFinanceiro()
	{

		$query = "SELECT COUNT(*) AS total FROM vendas v
		WHERE NOT EXISTS (SELECT * FROM `financeiro` f WHERE v.`idVendas` = f.`vendas_id`  )
		AND v.`valorTotal` IS NOT NULL
		AND v.`venda_visivel` = 1
		AND YEAR(`dataVenda`) = YEAR(CURDATE())
		";

		return $this->db->query($query)->result(); 

	}

	public function VendaSemFinanceiro()
	{
		
		$query = " SELECT  u.`usuario_nome`, c.`cliente_nome`, v.*  FROM vendas v
		INNER JOIN `usuarios` u ON v.`usuarios_id` = u.`usuario_id`
		INNER JOIN `clientes` c ON v.`clientes_id` = c.`cliente_id`
		WHERE NOT EXISTS (SELECT * FROM `financeiro` f WHERE v.`idVendas` = f.`vendas_id`  )
		AND v.`valorTotal` IS NOT NULL
		AND v.`venda_visivel` = 1
		AND YEAR(`dataVenda`) = YEAR(CURDATE())
		";

		return $this->db->query($query)->result(); 
	}


		public function VendaSemFinanceiroCustom($dataInicio = null, $dataFim = null, $cliente = null, $usuario = null )
	{
		$whereData = "";
		$whereCliente = "";
		$whereUsuario = "";

		if($dataInicio != null){
            $whereData = "AND v.`dataVenda` BETWEEN ".$this->db->escape($dataInicio)." AND ".$this->db->escape($dataFim);
        }

        if($cliente != null){
            $whereCliente = "AND v.`clientes_id` =".$this->db->escape($cliente);
        }

        if($usuario != null){
            $whereUsuario = "AND v.`usuarios_id` =".$this->db->escape($usuario);
        }


		$query = " SELECT  u.`usuario_nome`, c.`cliente_nome`, v.*  FROM vendas v
		INNER JOIN `usuarios` u ON v.`usuarios_id` = u.`usuario_id`
		INNER JOIN `clientes` c ON v.`clientes_id` = c.`cliente_id`
		WHERE NOT EXISTS (SELECT * FROM `financeiro` f WHERE v.`idVendas` = f.`vendas_id`  )
		AND v.`valorTotal` IS NOT NULL
		AND v.`venda_visivel` = 1
		$whereData 
		$whereCliente
		$whereUsuario
		";

		return $this->db->query($query)->result(); 
	}



	######## FIM RELATORIO MENU INCONSISTENCIA ########


	public function balancoProdutoImeis($id, $tipo = null, $apenasCompativeis = null)
	{
		$this->db->where('itens_de_balanco_balanco_produto_id', $id);

		if ($tipo) {
			$this->db->where('itens_de_balanco_tipo', $tipo);
		}

		if ($tipo === 1) {
			$this->db->join('itens_de_imei', 'itens_de_imei.imei_valor = itens_de_balanco.itens_de_balanco_imei');
			$this->db->join('produto', 'produto.produto_id = itens_de_imei.produtos_id');
		}

		if ($apenasCompativeis === true) {
			$this->db->where('itens_de_balanco_compativel', 1);
		} elseif ($apenasCompativeis === false) {
			$this->db->where('itens_de_balanco_compativel', 0);
		}

		if ($tipo === 1) {
			$this->db->order_by('itens_de_balanco_imei,imei_valor');
		}else{
			$this->db->order_by('itens_de_balanco_imei');
		}

		

		return $this->db->get('itens_de_balanco')->result();
	}

	public function fechamentoCaixa($data) 
	{
		$dataInicial = $data;
		$dataFinal = $data;

        $query = "SELECT 
			data_vencimento AS dia,			
			(SELECT  ROUND(SUM(`financeiro_valor`), 2 ) AS `valorTotal` 
			FROM `financeiro` WHERE dia = data_vencimento AND financeiro_tipo = 'receita'
			AND `financeiro_forma_pgto` = 'Dinheiro'
			AND `financeiro_baixado` = 1 
			AND `financeiro_visivel` = 1) AS dinheiro,

			(SELECT  ROUND(SUM(`financeiro_valor`), 2 ) AS `valorTotal` 
			FROM `financeiro` WHERE dia = data_vencimento AND financeiro_tipo = 'receita'
			AND `financeiro_forma_pgto` = 'Boleto Bancário'
			AND `financeiro_baixado` = 1 
			AND `financeiro_visivel` = 1) AS boleto,

			(SELECT  ROUND(SUM(`financeiro_valor`), 2 ) AS `valorTotal` 
			FROM `financeiro` WHERE dia = data_vencimento AND financeiro_tipo = 'receita'
			AND `financeiro_forma_pgto` = 'Pagamento Instantâneo (PIX)'
			AND `financeiro_baixado` = 1 
			AND `financeiro_visivel` = 1) AS pix,

			(SELECT  ROUND(SUM(`financeiro_valor`), 2 ) AS `valorTotal` 
			FROM `financeiro` WHERE dia = data_vencimento AND financeiro_tipo = 'receita'
			AND `financeiro_forma_pgto` = 'Crédito Loja'
			AND `financeiro_baixado` = 1 
			AND `financeiro_visivel` = 1) AS credLoja,

			(SELECT  ROUND(SUM(`financeiro_valor`), 2 ) AS `valorTotal` 
			FROM `financeiro` WHERE dia = data_vencimento AND financeiro_tipo = 'receita'
			AND `financeiro_forma_pgto` IN ('Cartão de Crédito','Cartão de Débito')
			AND `financeiro_baixado` = 1 
			AND `financeiro_visivel` = 1) AS cartDC,

			(SELECT  SUM(ROUND((financeiro_valor * (financeiro_percentual_cart/100)), 2)) AS `valorTotal` 
			FROM `financeiro` WHERE dia = data_vencimento AND financeiro_tipo = 'receita'
			AND `financeiro_forma_pgto` IN ('Cartão de Crédito','Cartão de Débito')
			AND `financeiro_baixado` = 1 
			AND `financeiro_visivel` = 1) AS descCart,	

			(SELECT ( (
			IF(dinheiro IS NOT NULL, dinheiro, 0) + IF(pix IS NOT NULL, pix, 0) +  IF(credLoja IS NOT NULL, credLoja, 0) +  IF(cartDC IS NOT NULL, cartDC, 0)
			) - ( 
			IF(descCart IS NOT NULL, descCart, 0)
			) ) ) AS saldo,


			(SELECT  ROUND(SUM(`financeiro_valor`), 2 ) AS `valorTotal` 
			FROM `financeiro` WHERE dia = data_vencimento AND financeiro_tipo = 'despesa'
			AND `financeiro_forma_pgto` = 'Dinheiro'	
			AND `financeiro_baixado` = 1 
			AND `financeiro_visivel` = 1) AS despesaDinheiro,

			(SELECT  ROUND(SUM(`financeiro_valor`), 2 ) AS `valorTotal` 
			FROM `financeiro` WHERE dia = data_vencimento AND financeiro_tipo = 'despesa'
			AND `financeiro_forma_pgto` = 'Pagamento Instantâneo (PIX)'	
			AND `financeiro_baixado` = 1 
			AND `financeiro_visivel` = 1) AS despesaPix,

			(SELECT  ROUND(SUM(`financeiro_valor`), 2 ) AS `valorTotal` 
			FROM `financeiro` WHERE dia = data_vencimento AND financeiro_tipo = 'despesa'
			AND `financeiro_forma_pgto` IN ('Cartão de Crédito','Cartão de Débito')	
			AND `financeiro_baixado` = 1 
			AND `financeiro_visivel` = 1) AS despesaCartao,

			(SELECT  ROUND(SUM(`financeiro_valor`), 2 ) AS `valorTotal` 
			FROM `financeiro` WHERE dia = data_vencimento AND financeiro_tipo = 'despesa'
			AND `financeiro_forma_pgto` = 'Boleto Bancário'	
			AND `financeiro_baixado` = 1 
			AND `financeiro_visivel` = 1) AS despesaBoleto,

			(SELECT  ROUND(SUM(`financeiro_valor`), 2 ) AS `valorTotal` 
			FROM `financeiro` WHERE dia = data_vencimento AND financeiro_tipo = 'despesa'	
			AND `financeiro_baixado` = 1 
			AND `financeiro_visivel` = 1) AS despesa 	
			
			FROM `financeiro`
			WHERE data_vencimento BETWEEN ? AND ? 
			AND `financeiro_visivel` = 1
			GROUP BY data_vencimento  ";

		  //var_dump($query);die();	
			
        return $this->db->query($query, array($dataInicial,$dataFinal))->result(); 

   }
   
   
   	public function VendasMensalGrupoDetalheGrafico($ano, $mes, $grupo)
	{
		$base = explode('_', BDCAMINHO);
		$this->db->select('  `categoria_prod_descricao` AS grupo,
		ROUND(SUM(`subTotal`), 2) AS total, 
		ROUND(SUM(prod_preco_custo  * `quantidade`), 2) AS custo ');
		$this->db->join('vendas','vendas.`idVendas` = itens_de_vendas.`vendas_id`' );
		$this->db->join('produto','produto.`produto_id` = `itens_de_vendas`.`produtos_id`' );
		$this->db->join('categoria_produto','produto.`produto_categoria_id` = categoria_produto.`categoria_prod_id`' );
		$this->db->join('financeiro ','`vendas`.`idVendas` = `financeiro`.`vendas_id`' , 'left');
		$this->db->where('venda_visivel',1);
		// $this->db->where('faturado',1); // retirada para calcular fiado.
		$this->db->where('financeiro.`financeiro_visivel`',1);
        $this->db->where('financeiro.`financeiro_forma_pgto` is NOT NULL', NULL, FALSE);
        $this->db->where('financeiro.`financeiro_tipo`', 'receita');
        $this->db->where('`itens_de_vendas`.filial', $base[2]);
		$this->db->where('YEAR(dataVenda)', $ano);
        $this->db->where('MONTH(dataVenda)', $mes);
        if ($grupo != NULL) {
        	$this->db->where('`categoria_prod_id`', $grupo);
        }
        $this->db->group_by('categoria_prod_id');
        return $this->db->get('itens_de_vendas')->result_array();

	}

	public function pegarVendasPorVendedorGrafico($anoAtual, $mesAtual){
		// SELECT SUM(itens_de_vendas.quantidade) AS totalProduto, usuarios_id FROM vendas
		// INNER JOIN itens_de_vendas ON vendas.idVendas = itens_de_vendas.vendas_id

		// WHERE dataVenda = CURDATE()
		// AND venda_visivel = 1
		// GROUP BY usuarios_id

		$this->db->select('SUM(itens_de_vendas.quantidade) AS totalProduto, usuarios_id');
		$this->db->join('itens_de_vendas','vendas.idVendas = itens_de_vendas.vendas_id');
		$this->db->where('YEAR(dataVenda)', $anoAtual);
		$this->db->where('MONTH(dataVenda)', $mesAtual);
		$this->db->where('venda_visivel', 1);
		$this->db->group_by('usuarios_id');

		return $this->db->get('vendas')->result_array();
	}

	public function pegarVendasDiaAtual($anoAtual, $mesAtual, $diaAtual){
		// SELECT SUM(itens_de_vendas.quantidade) AS totalProduto, usuarios_id FROM vendas
		// INNER JOIN itens_de_vendas ON vendas.idVendas = itens_de_vendas.vendas_id

		// WHERE dataVenda = CURDATE()
		// AND venda_visivel = 1
		// GROUP BY usuarios_id

		

		$this->db->select('SUM(itens_de_vendas.quantidade) AS totalProduto, usuarios_id');
		$this->db->join('itens_de_vendas','vendas.idVendas = itens_de_vendas.vendas_id');
		$this->db->where('YEAR(dataVenda)', $anoAtual);
		$this->db->where('MONTH(dataVenda)', $mesAtual);
		$this->db->where('DAY(dataVenda)', $diaAtual);
		$this->db->where('venda_visivel', 1);
		$this->db->group_by('usuarios_id');

		return $this->db->get('vendas')->result_array();
	}

	public function pegarVendasDiaAnterior($mesAtual, $anoAtual, $diaAnterior)
	{
		// SELECT usuarios.usuario_nome, SUM(itens_de_vendas.quantidade) AS totalProduto FROM vendas
		// INNER JOIN itens_de_vendas ON vendas.idVendas = itens_de_vendas.vendas_id
		// INNER JOIN usuarios ON vendas.usuarios_id = usuarios.usuario_id
		// WHERE dataVenda = CURDATE()
		// AND venda_visivel = 1
		// GROUP BY usuarios.usuario_nome

		$this->db->select('SUM(itens_de_vendas.quantidade) AS totalProduto, usuarios_id');
		$this->db->join('itens_de_vendas', 'vendas.idVendas = itens_de_vendas.vendas_id');
		$this->db->where('YEAR(dataVenda)', $anoAtual);
		$this->db->where('MONTH(dataVenda)', $mesAtual);
		$this->db->where('DAY(dataVenda)', $diaAnterior);
		$this->db->where('venda_visivel', 1);
		$this->db->group_by('usuarios_id');
		
		return $this->db->get('vendas')->result_array();
	}
}