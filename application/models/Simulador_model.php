<?php
class Simulador_model extends CI_Model
{
	public $tabela = "simulador_cartao";
	public $chave = 'idSimulador';

	function getSimular($valorSimular)
	{
		$this->db->select("
				`tipo_parcela`,
				`numero_parcela`,
				`percentual_parcela`,
				(($valorSimular * `percentual_parcela`)/100) AS valor_parcela_old,
				(($valorSimular / (1-`percentual_parcela`/100)) - $valorSimular) AS valor_parcela");
		return $this->db->get($this->tabela)->result();
	}

	public function pegarTaxas()
	{
		$this->db->select('*');
		return $this->db->get($this->tabela)->result();
	}

	public function pegarTaxaPorId($id, $tabelas = '*')
	{
		$this->db->select($tabelas);
		$this->db->where($this->chave, $id);
		return $this->db->get($this->tabela)->result();
	}

	public function editarTaxa($dados, $id)
	{
		$this->db->where($this->chave, $id);
		$this->db->update($this->tabela, $dados);

		if($this->db->affected_rows() > 0) {
			return true;
		}
		return false;
	}
}