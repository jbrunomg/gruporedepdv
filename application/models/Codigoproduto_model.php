<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Codigoproduto_model extends CI_Model {
    
    public $tabela  = "codigo_produto";
    public $visivel = "codigo_prod_visivel";
    public $chave   = "codigo_prod_id";
    

    public function listar()
    {
        $this->db->select("*");
        $this->db->where($this->visivel, 1);
        $this->db->join('categoria_produto', 'categoria_produto.categoria_prod_id = codigo_produto.codigo_prod_categoria_id','LEFT');

        return $this->db->get($this->tabela)->result();

        // $this->db->where($this->visivel, 1);
		// $this->db->join('categoria_produto', 'categoria_produto.categoria_prod_id = balanco_produto.balanco_produto_categoria_produto_id');
		// $this->db->join('codigo_produto', 'codigo_produto.codigo_prod_codigo = balanco_produto.balanco_produto_codigo_produto');

		// return $this->db->get($this->tabela)->result();
    }

    public function listarId($id)
    {
        $this->db->select("*");
        $this->db->where($this->chave, $id);
        $this->db->where($this->visivel, 1);
        return $this->db->get($this->tabela)->result();  
    }

    public function listarCategoria()
    {
        $this->db->select("*");
        $this->db->from("categoria_produto");
        $this->db->where("categoria_prod_visivel", 1);

        return $this->db->get()->result();
    }

    public function adicionar($dados)
    {
        $sql = "SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE '%_".GRUPOLOJA."_%' ";
        $bases = $this->db->query($sql)->result();;


        foreach ($bases as $b) {
            $this->db->insert($b->schema_name.'.'.$this->tabela, $dados);
        }

        if ($this->db->affected_rows() == '1')
        {
            return TRUE;
        }
            
        return FALSE;
    }

    public function editar($dados, $id)
    {

        $sql = "SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE '%_".GRUPOLOJA."_%' ";
        $bases = $this->db->query($sql)->result();;
        
        $this->db->trans_begin();
        
        foreach ($bases as $b) {
            $this->db->where($this->chave,$id); 
            $this->db->update($b->schema_name.'.'.$this->tabela,$dados);

        }

        if($this->db->trans_status() === true){
            $this->db->trans_commit();
            return true;
        }else{
            $this->db->trans_rollback();
            return false;
        }



        // $this->db->where($this->chave,$id);

        // if($dados['codigo_prod_codigo'] == $this->get_current_codigo($dados['codigo_prod_codigo'])) {

        //     unset($dados['codigo_prod_codigo']); // remove o codigo que não foi editado para salvar os outros campos
        //     $this->db->where($this->chave,$id);
        //     $this->db->update($this->tabela, $dados);

        //     return true;
            
        // } else {

        //     if ($this->is_unique_codigo($dados['codigo_prod_codigo'])) {

        //         $this->db->where($this->chave,$id);
        //         $this->db->update($this->tabela, $dados);

        //         return true;

        //     } else {

        //         return false;
                
        //     }
            
        // }
    
  
    
        
    }

    public function visualizar($id)
    {
        $this->db->select("*");
        $this->db->where($this->chave, $id);
        $this->db->where($this->visivel, 1);
        return $this->db->get($this->tabela)->result();
    }

    public function excluir($id)
    {
        // log_message('debug', 'Excluindo ID: ' . $id);
        $sql = "SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE '%".GRUPOLOJA."%';";
        $bases = $this->db->query($sql)->result();

        $dados  = array(
            'codigo_prod_visivel' => 0 
        );
        
        foreach ($bases as $b) {
            $this->db->where($this->chave, $id); 
            $this->db->update($b->SCHEMA_NAME.'.'.$this->tabela, $dados);
           // $this->db->delete($b->SCHEMA_NAME.'.'.$this->tabela, array($this->chave => $id));
        }

        if ($this->db->trans_status() === true) {
            return true;
        } else {
            return false;
        }
    }


    public function get_current_codigo($new_codigo) {
        $this->db->select('codigo_prod_codigo');
        $this->db->from('codigo_produto');
        $this->db->where('codigo_prod_codigo', $new_codigo);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row()->codigo_prod_codigo;
        } else {
            return false;
        }
    }

    // Método para verificar se o código do produto é único
    public function is_unique_codigo($new_codigo) {
        $this->db->where('codigo_prod_codigo', $new_codigo);
        $query = $this->db->get('codigo_produto');
        if ($query->num_rows() == 0) { // Se true, o código é único
            return true;
        } else {
            return false;
        }
    }


    public function getAllFilias()
    {
        $sql = "SHOW DATABASES";

        $result = $this->db->query($sql);

        $bases = array();
        if ($result->num_rows() > 0) {
            foreach (($result->result_array()) as $row) {
              if (strpos($row['Database'], '_'.GRUPOLOJA.'_') !== false && $row['Database'] !=
                'wdm_clientes') {
                $lojas = explode("_".GRUPOLOJA."_", $row['Database']);
                 
                   $bases[]['lojas'] = $lojas[1];
                
              }
            }

          return $bases;
        }
        return FALSE;
    }

}