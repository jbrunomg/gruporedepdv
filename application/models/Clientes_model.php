<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes_model extends CI_Model {
	
	public $tabela  = "clientes";
	public $visivel = "cliente_visivel";
	public $chave   = "cliente_id";

	public function listar($limit, $start, $search, $order_column_name, $order_dir) {
		$this->db->select('*');
		$this->db->from($this->tabela);
		$this->db->where($this->visivel, 1);
		
		if (!empty($search)) {
			$this->db->group_start();
			$this->db->like('cliente_nome', $search); // Substitua 'seu_campo' pelo campo que deseja pesquisar
			$this->db->or_like('cliente_celular', $search); 
			$this->db->or_like('cliente_cpf_cnpj', $search); 
			$this->db->or_like('cliente_email', $search); 
			$this->db->group_end();
		}
	
		// Ordenação
		if (!empty($order_column_name)) {
			$this->db->order_by("TRIM($order_column_name)", $order_dir);
		}
		$this->db->limit($limit, $start);
	
		// Execute a consulta e retorne os resultados
		$resultados = $this->db->get()->result();
	
		$this->db->from($this->tabela);
		$this->db->where($this->visivel, 1);
		if (!empty($search)) {
				$this->db->group_start();
				$this->db->like('cliente_nome', $search); // Substitua 'seu_campo' pelo campo que deseja pesquisar
				$this->db->or_like('cliente_celular', $search); 
				$this->db->or_like('cliente_cpf_cnpj', $search); 
				$this->db->or_like('cliente_email', $search); 
				$this->db->group_end();
		}
		$total_registros = $this->db->count_all_results();
		
		return array('dados' => $resultados, 'total_registros' => $total_registros);
	}
	

	public function listarId($id)
	{
		$this->db->select("*");
		$this->db->where($this->chave, $id);
		// $this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}

    public function listarVendasId($id)
	{
		$this->db->select("idVendas,
						  dataVenda,
						  valorTotal,
						  usuario_nome,
						  financeiro.`financeiro_forma_pgto`,
						  financeiro.`financeiro_baixado`");
		$this->db->where('clientes_id', $id);
		$this->db->where('venda_visivel', 1);
		$this->db->where('financeiro.`financeiro_forma_pgto` is NOT NULL', NULL, FALSE);
		$this->db->join('usuarios ','`vendas`.`usuarios_id` = `usuarios`.`usuario_id` ');
		$this->db->join('financeiro ','`vendas`.`idVendas` = `financeiro`.`vendas_id`' , 'left');
		$this->db->group_by('idVendas');
		return $this->db->get('vendas')->result();
	}

     public function validacaoCPF($cpf)
	{
		$this->db->select("*");
		$this->db->where('cliente_cpf_cnpj', $cpf);
		return $this->db->get($this->tabela)->result();
	}



	public function adicionar($dados)
	{
		$sql = "SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE '%_".GRUPOLOJA."_%' ";
        $bases = $this->db->query($sql)->result();;

        foreach ($bases as $b) {
        	$this->db->insert($b->schema_name.'.'.$this->tabela, $dados);
        }

        if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
			
			return FALSE;
	}

	public function editar($dados, $id)
	{
		$sql = "SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE '%_".GRUPOLOJA."_%' ";
        $bases = $this->db->query($sql)->result();;
        
        foreach ($bases as $b) {
			$this->db->where($this->chave,$id);	
			$this->db->update($b->schema_name.'.'.$this->tabela,$dados);

        }

	    if($this->db->trans_status() === true){
	        $this->db->trans_commit();
	        return true;
	    }else{
	        $this->db->trans_rollback();
	        return false;
	    }
	}

	public function excluir($dados, $id)
	{
		$sql = "SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE '%_".GRUPOLOJA."_%' ";
        $bases = $this->db->query($sql)->result();;
        
        foreach ($bases as $b) {
			$this->db->where($this->chave,$id);	
			$this->db->update($b->schema_name.'.'.$this->tabela,$dados);

        }

	    if($this->db->trans_status() === true){
	        $this->db->trans_commit();
	        return true;
	    }else{
	        $this->db->trans_rollback();
	        return false;
	    }
	}

	public function visualizar($id)
	{
		$this->db->select("*");
		$this->db->where($this->chave, $id);
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}

	public function pegarEstados()
	{
		$this->db->select('*');
		return $this->db->get('estados')->result();
	}

	public function pegarEstadoId($sigla)
	{
		$this->db->select(' id_estado ');
		$this->db->where('sigla', $sigla);
		return $this->db->get('estados')->result();
	}

	public function pegarCidadeId($estado,$cidade)
	{
		$this->db->select(' id_cidade ');
		$this->db->where('nome', $cidade);
		$this->db->where('id_estado', $estado);
		return $this->db->get('cidades')->result();
	}

	public function pegarPerfil($value='')
	{
		$this->db->select('*');
		return $this->db->get('perfis')->result();
	}

	public function pegarCategoria($value='')
	{
		$this->db->select('*');
		return $this->db->get('categoria_cliente')->result();
	}

	public function autoCompleteClientes($termo)
	{
		$this->db->select('*');
        $this->db->limit(5);
        $this->db->like('cliente_nome', $termo);
        $this->db->where($this->visivel, 1);
        $query = $this->db->get($this->tabela)->result();

        if(count($query) > 0){
            foreach ($query as $row){
                $row_set[] = array('label'=>$row->cliente_nome.' | TEL: '.$row->cliente_telefone,'id'=>$row->cliente_id);
            }
            echo json_encode($row_set);
        }
	}
	public function getAllFilias()
    {
        $sql = "SHOW DATABASES";

        $result = $this->db->query($sql);

        $bases = array();
        if ($result->num_rows() > 0) {
            foreach (($result->result_array()) as $row) {
              if (strpos($row['Database'], '_'.GRUPOLOJA.'_') !== false && $row['Database'] !=
                'wdm_clientes') {
              	$lojas = explode("_".GRUPOLOJA."_", $row['Database']);
                 
                   $bases[]['lojas'] = $lojas[1];
                
              }
            }

          return $bases;
        }
        return FALSE;
    }

}