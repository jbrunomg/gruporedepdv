<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produto_model extends CI_Model {
	
	public $tabela  = "produto";
	public $visivel = "produto_visivel";
	public $chave   = "produto_id";

	public function listar($limit, $start, $search, $order_column_name, $order_dir) {
		$this->db->select('*');
		$this->db->from($this->tabela);
		$this->db->where($this->visivel, 1);
		
		if (!empty($search)) {
			$this->db->group_start();
			$this->db->like('produto_codigo', $search); // Substitua 'seu_campo' pelo campo que deseja pesquisar
			$this->db->or_like('produto_codigo_barra', $search); 
			$this->db->or_like('produto_descricao', $search); 
			$this->db->or_like('produto_estoque', $search); 
			$this->db->group_end();
		}
	
		// Ordenação
		if (!empty($order_column_name)) {
			$this->db->order_by("TRIM($order_column_name)", $order_dir);
		}
		$this->db->limit($limit, $start);
	
		// Execute a consulta e retorne os resultados
		$resultados = $this->db->get()->result();
	
		$this->db->from($this->tabela);
		$this->db->where($this->visivel, 1);
		if (!empty($search)) {
				$this->db->group_start();
				$this->db->like('produto_codigo', $search); // Substitua 'seu_campo' pelo campo que deseja pesquisar
				$this->db->or_like('produto_codigo_barra', $search); 
				$this->db->or_like('produto_descricao', $search); 
				$this->db->or_like('produto_estoque', $search); 
				$this->db->group_end();
		}
		$total_registros = $this->db->count_all_results();
		
		return array('dados' => $resultados, 'total_registros' => $total_registros);
	}

	public function listarId($id)
	{
		$this->db->select("*");		
		$this->db->where($this->chave, $id);
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}


	public function adicionar($dados, $dadosGaveta)
	{

		$sql = "SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE '%_".GRUPOLOJA."_%' ";
        $bases = $this->db->query($sql)->result();;

        $lojaAtual = BDCAMINHO ;

        foreach ($bases as $b) {

        	if($lojaAtual == $b->schema_name){
                
                $this->db->insert($b->schema_name.'.'.$this->tabela, $dados);
                $id = $this->db->insert_id($this->tabela);//Pega o ID auto-gerado e armazena na variavel
                $this->db->where($this->chave,$id);	
        		$this->db->update($this->tabela,$dadosGaveta);
        		

        	}else{

        	  $this->db->insert($b->schema_name.'.'.$this->tabela, $dados);

        	}
         	
        }

        if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
			return FALSE;
	}

	public function preencherSelectCodigos($grupo)
	{
		$this->db->select('codigo_prod_codigo, codigo_prod_descricao'); // estava dando select na tabela 'produto', agora na tabela 'codigo_produto'
		$this->db->where('codigo_prod_categoria_id', $grupo);
		$this->db->where('codigo_prod_visivel', 1);
		$this->db->group_by('codigo_prod_codigo');
		$this->db->order_by('codigo_prod_codigo');

		return $this->db->get('codigo_produto')->result();
	}

	public function preencherSelectCodigosListar()
	{
		$this->db->select('codigo_prod_codigo, codigo_prod_descricao, codigo_prod_categoria_id'); // estava dando select na tabela 'produto', agora na tabela 'codigo_produto'
		$this->db->where('codigo_prod_visivel', 1);
		$this->db->group_by('codigo_prod_codigo');
		$this->db->order_by('codigo_prod_codigo');

		return $this->db->get('codigo_produto')->result();
	}

	public function editar($dados, $dadosGaveta, $id)
	{

		$sql = "SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE '%_".GRUPOLOJA."_%' ";
        $bases = $this->db->query($sql)->result();;
        $lojaAtual = BDCAMINHO ;
        
        foreach ($bases as $b) {

        	if($lojaAtual == $b->schema_name){
                
				$this->db->where($this->chave,$id);	
				$this->db->update($b->schema_name.'.'.$this->tabela,$dados);
                $this->db->where($this->chave,$id);	
        		$this->db->update($this->tabela,$dadosGaveta);
 
        	}else{

	        	$this->db->where($this->chave,$id);	
				$this->db->update($b->schema_name.'.'.$this->tabela,$dados);

        	}

        }

	    if($this->db->trans_status() === true){
	        $this->db->trans_commit();
	        return true;
	    }else{
	        $this->db->trans_rollback();
	        return false;
	    }

	}

	public function excluir($dados, $id)
	{
		$sql = "SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE '%_".GRUPOLOJA."_%' ";
        $bases = $this->db->query($sql)->result();;
        
        foreach ($bases as $b) {
			$this->db->where($this->chave,$id);	
			$this->db->update($b->schema_name.'.'.$this->tabela,$dados);

        }

	    if($this->db->trans_status() === true){
	        $this->db->trans_commit();
	        return true;
	    }else{
	        $this->db->trans_rollback();
	        return false;
	    }
	}

	public function visualizar($id)
	{
		$this->db->select("*");
		$this->db->where($this->chave, $id);
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}

	public function pegarGrupoProduto()
	{
		$this->db->select('*');
		$this->db->where('categoria_prod_visivel', 1);
		return $this->db->get('categoria_produto')->result();
	}

	public function getAllFilias()
    {
        $sql = "SHOW DATABASES";

        $result = $this->db->query($sql);

        $bases = array();
        if ($result->num_rows() > 0) {
            foreach (($result->result_array()) as $row) {
              if (strpos($row['Database'], '_'.GRUPOLOJA.'_') !== false && $row['Database'] !=
                'wdm_clientes') {
              	$lojas = explode("_".GRUPOLOJA."_", $row['Database']);
                 
                   $bases[]['lojas'] = $lojas[1];
                
              }
            }

          return $bases;
        }
        return FALSE;
    }

    public function getAllParametros()
    {
        $this->db->select('parametro_cart_debito, parametro_cart_credito');
		return $this->db->get('parametro')->result();
    }

    public function selecionarCategoria($grupo)
	{
		$this->db->select('categoria_prod_margem');
		$this->db->where('categoria_prod_visivel',1);
		$this->db->where('categoria_prod_id',$grupo);
		return $this->db->get('categoria_produto')->result();
	}



public function graficoAno($idProduto)
{

$query = $this->db->query("

	SELECT 'Jan' AS Ano, 
	(SELECT SUM(subTotal) FROM `itens_de_vendas` INNER JOIN `vendas` ON vendas.`idVendas` = `itens_de_vendas`.`vendas_id`
	WHERE  YEAR(`dataVenda`) =  YEAR(CURDATE()) AND  MONTH(dataVenda) = 1 AND venda_visivel = 1 AND `produtos_id` = {$idProduto}  ) AS atual,
	(SELECT SUM(subTotal) FROM `itens_de_vendas` INNER JOIN `vendas` ON vendas.`idVendas` = `itens_de_vendas`.`vendas_id`
	WHERE  YEAR(`dataVenda`) =  YEAR(CURDATE()) -1 AND  MONTH(dataVenda) = 1 AND venda_visivel = 1 AND `produtos_id` = {$idProduto}  ) AS passado
	FROM vendas

	UNION

	SELECT 'Fev' AS Ano, 
	(SELECT SUM(subTotal) FROM `itens_de_vendas` INNER JOIN `vendas` ON vendas.`idVendas` = `itens_de_vendas`.`vendas_id`
	WHERE  YEAR(`dataVenda`) =  YEAR(CURDATE()) AND  MONTH(dataVenda) = 2 AND venda_visivel = 1 AND `produtos_id` = {$idProduto}  ) AS atual,
	(SELECT SUM(subTotal) FROM `itens_de_vendas` INNER JOIN `vendas` ON vendas.`idVendas` = `itens_de_vendas`.`vendas_id`
	WHERE  YEAR(`dataVenda`) =  YEAR(CURDATE()) -1 AND  MONTH(dataVenda) = 2 AND venda_visivel = 1 AND `produtos_id` = {$idProduto}  ) AS passado
	FROM vendas

	UNION

	SELECT 'Mar' AS Ano, 
	(SELECT SUM(subTotal) FROM `itens_de_vendas` INNER JOIN `vendas` ON vendas.`idVendas` = `itens_de_vendas`.`vendas_id`
	WHERE  YEAR(`dataVenda`) =  YEAR(CURDATE()) AND  MONTH(dataVenda) = 3 AND venda_visivel = 1 AND `produtos_id` = {$idProduto}  ) AS atual,
	(SELECT SUM(subTotal) FROM `itens_de_vendas` INNER JOIN `vendas` ON vendas.`idVendas` = `itens_de_vendas`.`vendas_id`
	WHERE  YEAR(`dataVenda`) =  YEAR(CURDATE()) -1 AND  MONTH(dataVenda) = 3 AND venda_visivel = 1 AND `produtos_id` = {$idProduto}  ) AS passado
	FROM vendas

	UNION

	SELECT 'Abr' AS Ano, 
	(SELECT SUM(subTotal) FROM `itens_de_vendas` INNER JOIN `vendas` ON vendas.`idVendas` = `itens_de_vendas`.`vendas_id`
	WHERE  YEAR(`dataVenda`) =  YEAR(CURDATE()) AND  MONTH(dataVenda) = 4 AND venda_visivel = 1 AND `produtos_id` = {$idProduto}  ) AS atual,
	(SELECT SUM(subTotal) FROM `itens_de_vendas` INNER JOIN `vendas` ON vendas.`idVendas` = `itens_de_vendas`.`vendas_id`
	WHERE  YEAR(`dataVenda`) =  YEAR(CURDATE()) -1 AND  MONTH(dataVenda) = 4 AND venda_visivel = 1 AND `produtos_id` = {$idProduto}  ) AS passado
	FROM vendas

	UNION

	SELECT 'Mai' AS Ano, 
	(SELECT SUM(subTotal) FROM `itens_de_vendas` INNER JOIN `vendas` ON vendas.`idVendas` = `itens_de_vendas`.`vendas_id`
	WHERE  YEAR(`dataVenda`) =  YEAR(CURDATE()) AND  MONTH(dataVenda) = 5 AND venda_visivel = 1 AND `produtos_id` = {$idProduto}  ) AS atual,
	(SELECT SUM(subTotal) FROM `itens_de_vendas` INNER JOIN `vendas` ON vendas.`idVendas` = `itens_de_vendas`.`vendas_id`
	WHERE  YEAR(`dataVenda`) =  YEAR(CURDATE()) -1 AND  MONTH(dataVenda) = 5 AND venda_visivel = 1 AND `produtos_id` = {$idProduto}  ) AS passado
	FROM vendas

	UNION

	SELECT 'Jun' AS Ano, 
	(SELECT SUM(subTotal) FROM `itens_de_vendas` INNER JOIN `vendas` ON vendas.`idVendas` = `itens_de_vendas`.`vendas_id`
	WHERE  YEAR(`dataVenda`) =  YEAR(CURDATE()) AND  MONTH(dataVenda) = 6 AND venda_visivel = 1 AND `produtos_id` = {$idProduto}  ) AS atual,
	(SELECT SUM(subTotal) FROM `itens_de_vendas` INNER JOIN `vendas` ON vendas.`idVendas` = `itens_de_vendas`.`vendas_id`
	WHERE  YEAR(`dataVenda`) =  YEAR(CURDATE()) -1 AND  MONTH(dataVenda) = 6 AND venda_visivel = 1 AND `produtos_id` = {$idProduto}  ) AS passado
	FROM vendas

	UNION

	SELECT 'Jul' AS Ano, 
	(SELECT SUM(subTotal) FROM `itens_de_vendas` INNER JOIN `vendas` ON vendas.`idVendas` = `itens_de_vendas`.`vendas_id`
	WHERE  YEAR(`dataVenda`) =  YEAR(CURDATE()) AND  MONTH(dataVenda) = 7 AND venda_visivel = 1 AND `produtos_id` = {$idProduto}  ) AS atual,
	(SELECT SUM(subTotal) FROM `itens_de_vendas` INNER JOIN `vendas` ON vendas.`idVendas` = `itens_de_vendas`.`vendas_id`
	WHERE  YEAR(`dataVenda`) =  YEAR(CURDATE()) -1 AND  MONTH(dataVenda) = 7 AND venda_visivel = 1 AND `produtos_id` = {$idProduto}  ) AS passado
	FROM vendas

	UNION

	SELECT 'Ago' AS Ano, 
	(SELECT SUM(subTotal) FROM `itens_de_vendas` INNER JOIN `vendas` ON vendas.`idVendas` = `itens_de_vendas`.`vendas_id`
	WHERE  YEAR(`dataVenda`) =  YEAR(CURDATE()) AND  MONTH(dataVenda) = 8 AND venda_visivel = 1 AND `produtos_id` = {$idProduto}  ) AS atual,
	(SELECT SUM(subTotal) FROM `itens_de_vendas` INNER JOIN `vendas` ON vendas.`idVendas` = `itens_de_vendas`.`vendas_id`
	WHERE  YEAR(`dataVenda`) =  YEAR(CURDATE()) -1 AND  MONTH(dataVenda) = 8 AND venda_visivel = 1 AND `produtos_id` = {$idProduto}  ) AS passado
	FROM vendas

	UNION

	SELECT 'Set' AS Ano, 
	(SELECT SUM(subTotal) FROM `itens_de_vendas` INNER JOIN `vendas` ON vendas.`idVendas` = `itens_de_vendas`.`vendas_id`
	WHERE  YEAR(`dataVenda`) =  YEAR(CURDATE()) AND  MONTH(dataVenda) = 9 AND venda_visivel = 1 AND `produtos_id` = {$idProduto}  ) AS atual,
	(SELECT SUM(subTotal) FROM `itens_de_vendas` INNER JOIN `vendas` ON vendas.`idVendas` = `itens_de_vendas`.`vendas_id`
	WHERE  YEAR(`dataVenda`) =  YEAR(CURDATE()) -1 AND  MONTH(dataVenda) = 9 AND venda_visivel = 1 AND `produtos_id` = {$idProduto}  ) AS passado
	FROM vendas

	UNION

	SELECT 'Out' AS Ano, 
	(SELECT SUM(subTotal) FROM `itens_de_vendas` INNER JOIN `vendas` ON vendas.`idVendas` = `itens_de_vendas`.`vendas_id`
	WHERE  YEAR(`dataVenda`) =  YEAR(CURDATE()) AND  MONTH(dataVenda) = 10 AND venda_visivel = 1 AND `produtos_id` = {$idProduto}  ) AS atual,
	(SELECT SUM(subTotal) FROM `itens_de_vendas` INNER JOIN `vendas` ON vendas.`idVendas` = `itens_de_vendas`.`vendas_id`
	WHERE  YEAR(`dataVenda`) =  YEAR(CURDATE()) -1 AND  MONTH(dataVenda) = 10 AND venda_visivel = 1 AND `produtos_id` = {$idProduto}  ) AS passado
	FROM vendas

		UNION

	SELECT 'Nov' AS Ano, 
	(SELECT SUM(subTotal) FROM `itens_de_vendas` INNER JOIN `vendas` ON vendas.`idVendas` = `itens_de_vendas`.`vendas_id`
	WHERE  YEAR(`dataVenda`) =  YEAR(CURDATE()) AND  MONTH(dataVenda) = 11 AND venda_visivel = 1 AND `produtos_id` = {$idProduto}  ) AS atual,
	(SELECT SUM(subTotal) FROM `itens_de_vendas` INNER JOIN `vendas` ON vendas.`idVendas` = `itens_de_vendas`.`vendas_id`
	WHERE  YEAR(`dataVenda`) =  YEAR(CURDATE()) -1 AND  MONTH(dataVenda) = 11 AND venda_visivel = 1 AND `produtos_id` = {$idProduto}  ) AS passado
	FROM vendas

		UNION

	SELECT 'Dez' AS Ano, 
	(SELECT SUM(subTotal) FROM `itens_de_vendas` INNER JOIN `vendas` ON vendas.`idVendas` = `itens_de_vendas`.`vendas_id`
	WHERE  YEAR(`dataVenda`) =  YEAR(CURDATE()) AND  MONTH(dataVenda) = 12 AND venda_visivel = 1 AND `produtos_id` = {$idProduto}  ) AS atual,
	(SELECT SUM(subTotal) FROM `itens_de_vendas` INNER JOIN `vendas` ON vendas.`idVendas` = `itens_de_vendas`.`vendas_id`
	WHERE  YEAR(`dataVenda`) =  YEAR(CURDATE()) -1 AND  MONTH(dataVenda) = 12 AND venda_visivel = 1 AND `produtos_id` = {$idProduto}  ) AS passado
	FROM vendas


    ");

       return $query->result();
	
	}



	public function graficoMes($idProduto)
	{

	$query = $this->db->query("

	SELECT 
	  DAY(dataVenda) AS `dia`,
	  dataVenda AS venda,  
	  (SELECT SUM(`subTotal`) AS total 
	  FROM  `vendas` 
	  INNER JOIN `itens_de_vendas`  ON vendas.`idVendas` = `itens_de_vendas`.`vendas_id`
	  WHERE  venda_visivel = 1 
	  AND MONTH(dataVenda) = MONTH(CURDATE())
	  AND `produtos_id` = {$idProduto}
	  AND dia = DAY(dataVenda) ) AS atual,
	  
	    (SELECT SUM(`subTotal`) AS total 
	  FROM  `vendas` 
	  INNER JOIN `itens_de_vendas`  ON vendas.`idVendas` = `itens_de_vendas`.`vendas_id`
	  WHERE  venda_visivel = 1 
	  AND MONTH(dataVenda) = MONTH(CURDATE()) -1
	  AND `produtos_id` = {$idProduto}
	  AND dia = DAY(dataVenda) ) AS passado
	    
	   FROM `vendas` 
	   INNER JOIN `itens_de_vendas`  ON vendas.`idVendas` = `itens_de_vendas`.`vendas_id`
	   WHERE  venda_visivel = 1   
	   AND MONTH(`dataVenda`) BETWEEN MONTH(CURDATE()) -1 AND MONTH(CURDATE())
	   AND `produtos_id` = {$idProduto}
	   GROUP BY dia 
	   ORDER BY dia

	");

    return $query->result();
	
	}

	public function adicionarEstoqueRotina($dados, $quantidade, $produto, $estoque)
    { 

		$idEstoque = $this->adicionarEstoque($dados);
		$idEstoqueItens = $this->prepararItensEstoque($idEstoque, $produto, $estoque);
		$this->editarProdutoEstoque($produto, $quantidade);
		$this->editarAjusteEstoque($produto, $quantidade, $idEstoque);
		
	    if($this->db->trans_status() === true){
	        $this->db->trans_commit();
	        return true;
	    }else{
	        $this->db->trans_rollback();
	        return false;
	    }

    }

	public function adicionarEstoque($dados)
	{
		$this->db->insert('estoque_ajuste', $dados);
		if ($this->db->affected_rows() == '1'){
			return $this->db->insert_id('estoque_ajuste');
		}	
		return FALSE; 
	}

	public function prepararItensEstoque($idEstoque, $produto, $estoque)
	{
		$this->db->set('estoque', $estoque);
		$this->db->set('produtos_id', $produto);
		$this->db->set('estoque_ajuste_id', $idEstoque);
		$this->db->insert('itens_estoque_ajuste');

		if ($this->db->affected_rows() == '1'){
			return TRUE;
		}	
		return FALSE; 
	}

	public function editarProdutoEstoque($produto, $quantidade)
	{
		$this->db->where('produto_id', $produto);
		$this->db->set('produto_estoque', $quantidade);	
		
		if($this->db->update('produto'))
		{
			return true;
		}

		return false;
	}

	public function editarAjusteEstoque($produto, $quantidade, $idEstoque)
	{
		$this->db->where('produtos_id', $produto);
		$this->db->where('estoque_ajuste_id', $idEstoque);

		$this->db->set('quantidade_atual', $quantidade);
		$this->db->set('itens_estoque_ajuste_usuario_id', $this->session->userdata('usuario_id'));

		if($this->db->update('itens_estoque_ajuste'))
		{
			return true;
		}

		return false;
	}

	public function	listarImpostos()
	{
		$this->db->select("imposto_id, imposto_descricao");
		$this->db->order_by('imposto_id', 'desc');
		return $this->db->get('imposto')->result();
	}

	public function	movimentacao($idProduto)
	{
		$this->db->select("*");
		$this->db->where('item_movimentacao_produto_visivel', 1);
		$this->db->where('item_movimentacao_produto_produto_id', $idProduto);
		$this->db->order_by('item_movimentacao_produto_movimentacao_produto_id', 'desc');
		return $this->db->get('itens_movimentacao_produtos')->result();
	}

}