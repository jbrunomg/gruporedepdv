<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Imposto_model extends CI_Model
{
    public function listarImpostos()
	{
		$this->db->select("imposto_id, imposto_descricao, imposto_tipo_tributacao, imposto_icms_codigo_CFOP, imposto_icms_situacao_tributaria");
		$this->db->order_by('imposto_id', 'desc');
		return $this->db->get('imposto')->result();
	}

	public function adicionar($dados)
	{
		$this->db->insert('imposto', $dados);
		return $this->db->insert_id();
	}

	public function editar($id)
	{
		$this->db->where('imposto_id', $id);
		return $this->db->get('imposto')->row();
	}

	public function atualizar($id, $dados)
	{
		$this->db->where('imposto_id', $id);
		$this->db->update('imposto', $dados);

		if ($this->db->affected_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function excluir($id)
	{
		$this->db->where('imposto_id', $id);
		$this->db->delete('imposto');

		if ($this->db->affected_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}
}
