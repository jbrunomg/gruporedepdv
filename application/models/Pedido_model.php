<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pedido_model extends CI_Model {

	public $tabela  = "pedidos";
	public $visivel = "pedido_visivel";
	public $chave   = "idPedidos";

 public function listar()
	{
		$this->db->select("*");
		$this->db->join('clientes ',' pedidos.`clientes_id` = clientes.`cliente_id` ');	
		$this->db->join('usuarios ',' pedidos.`usuarios_id` = usuarios.`usuario_id` ');		
		$this->db->order_by('idPedidos', "desc");		
		$this->db->where($this->visivel, 1);
		// $this->db->where('pedidos.finalizado', 0);

		return $this->db->get($this->tabela)->result();
	}

 public function listarId($id)
	{
		$this->db->select("*");		
		$this->db->where($this->chave, $id);
		$this->db->join('clientes ',' pedidos.`clientes_id` = clientes.`cliente_id` ');	
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}

 public function adicionar($dados)
	{
		$this->db->insert($this->tabela, $dados);

		if ($this->db->affected_rows() == '1')
		{
			//return TRUE;
			return $this->db->insert_id($this->tabela);
		}
		
		return FALSE; 
	}

 public function editar($dados, $id)
	{
		$this->db->where($this->chave,$id);		
		
		if($this->db->update($this->tabela,$dados))
		{
			return true;
		}

		return false;
	}

 public function pegarProdutos($grupo)
	{
		$this->db->select('*');
		$this->db->where('produto_visivel',1);
		$this->db->where('produto_categoria_id',$grupo);
		return $this->db->get('produto')->result();
	}
 
 public function pegarProdutosID($produto)
	{
		$this->db->select('*');
		$this->db->where('produto_visivel',1);
		$this->db->where('produto_id',$produto);
		return $this->db->get('produto')->result();

	}

 public function add($table,$data,$returnId = false){

        $this->db->insert($table, $data);         

        if ($this->db->affected_rows() == '1')

		{
            if($returnId == true){

                return $this->db->insert_id($table);
            }
			return TRUE;
		}		
		return FALSE;      
    }

  public function addTotal($id, $subTotal){
     
       $sql = "UPDATE pedidos set valorTotal = valorTotal + ? WHERE idPedidos = ?";
        $this->db->query($sql, array($subTotal, $id));    

        if ($this->db->affected_rows() == '1'){
			return TRUE;
		}		
		return FALSE;      
    }

 public function pegarFornecedor()
	{
		$this->db->select('*');
		$this->db->where('fornecedor_visivel',1);
		return $this->db->get('fornecedores')->result();
	}

 public function pegarCategoriaProd()
	{
		$this->db->select('*');
		$this->db->where('categoria_prod_visivel',1);
		return $this->db->get('categoria_produto')->result();
	}

 public function getProdutos($id = null){

        $this->db->select('IF(`produto_estoque` >= `quantidade`,"success","danger")AS situacao_estoque, itens_de_pedidos.*, produto.*');

        $this->db->from('itens_de_pedidos');

        $this->db->join('produto','produto.produto_id = itens_de_pedidos.produtos_id');

        $this->db->where('pedidos_id',$id);
        $this->db->where('produto.produto_visivel',1);

        return $this->db->get()->result();

    }


  public function Pedido($dados,$id)
	{	

		$this->db->where($this->chave,$id);
		
		
		if($this->db->update($this->tabela,$dados))
		{
			return true;
		}

		return false;

	}

 public function validacao($id){

        $this->db->select('quantidade, produtos_id');
        $this->db->from('itens_de_pedidos');

        $this->db->join('produto','produto.produto_id = itens_de_pedidos.produtos_id');

        $this->db->where('produto.produto_visivel',1);

        $this->db->where('pedidos_id',$id);
		$dados = $this->db->get()->result();
		
		if (empty($dados)) {
			return false;
		}

        foreach ($dados as $row){

            $sqlConsultar = "SELECT IF(`produto_estoque` >= ".$row->quantidade.", 'Y','N') AS quantidade FROM `produto` WHERE `produto_id` = ".$row->produtos_id." ";

            $resultado = $this->db->query($sqlConsultar)->result();

             if ($resultado[0]->quantidade == 'N') {

                return false;
                break;         
             }

        }

      return true;
   }

   public function itensVenda($id, $valorTotal, $cliente, $usuario) {
		// Inicia a transação
		$this->db->trans_begin();

		// Inserção na tabela 'vendas'
		$sql = "INSERT INTO vendas (dataVenda, horaVenda, valorTotal, clientes_id, usuarios_id) 
				VALUES (CURRENT_DATE, CURRENT_TIME, ?, ?, ?)";
		$this->db->query($sql, array($valorTotal, $cliente, $usuario));

		if ($this->db->affected_rows() == 1) {
			// Recupera o ID da venda
			$idVendas = $this->db->insert_id();

			// Inserção na tabela 'itens_de_vendas'
			$sqlIten = "INSERT INTO `itens_de_vendas` (
							`subTotal`,
							`percentual_desc`,
							`quantidade`,
							`vendas_id`,
							`produtos_id`,
							`produtos_codigo`,
							`prod_preco_custo`,
							`prod_preco_minimo_venda`,
							`prod_preco_venda`,
							`prod_preco_cart_debito`,
							`prod_preco_cart_credito`,
							`filial`,
							`imei_valor`
						) 
						SELECT 
							`subTotal`,
							`percentual_desc`,
							`quantidade`,
							{$idVendas},
							`produtos_id`,
							`produtos_codigo`,
							`prod_preco_custo`,
							`prod_preco_minimo_venda`,
							`prod_preco_venda`,
							`prod_preco_cart_debito`,
							`prod_preco_cart_credito`,
							`filial`,
							`imei_valor`
						FROM
							`itens_de_pedidos` 
						WHERE pedidos_id = ?";
			$this->db->query($sqlIten, array($id));

			// Atualizar a tabela 'itens_de_imei' para cada 'imei_valor' inserido
			$sqlSelecionarImei = "SELECT imei_valor FROM itens_de_pedidos WHERE pedidos_id = ?";
			$resultado = $this->db->query($sqlSelecionarImei, array($id))->result();

			foreach ($resultado as $row) {
				$imei = $row->imei_valor;
				
				// Atualização do 'vendas_id' na tabela 'itens_de_imei' para o 'imei_valor' correspondente
				$sqlAtualizar = "UPDATE itens_de_imei SET vendas_id = ? WHERE imei_valor = ?";
				$this->db->query($sqlAtualizar, array($idVendas, $imei));
			}

			// Atualiza o estoque
			$this->db->select('quantidade, produtos_id');
			$this->db->from('itens_de_pedidos');
			$this->db->where('pedidos_id', $id);

			foreach ($this->db->get()->result() as $row) {
				$sqlAtualizar = "UPDATE produto SET produto_estoque = produto_estoque - ? WHERE produto_id = ?";
				$this->db->query($sqlAtualizar, array($row->quantidade, $row->produtos_id));
			}

			// Verifica o status da transação
			if ($this->db->trans_status() === FALSE) {
				// Se der erro, faz o rollback
				$this->db->trans_rollback();
				return false;
			} else {
				// Se der tudo certo, faz o commit e retorna o ID da venda
				$this->db->trans_commit();
				return $idVendas;
			}
		} else {
			// Caso a inserção na tabela 'vendas' falhe, faz rollback
			$this->db->trans_rollback();
			return false;
		}
	}

  public function delete($table,$fieldID,$ID){

        $this->db->where($fieldID,$ID);

        $this->db->delete($table);

        if ($this->db->affected_rows() == '1')

		{
			return TRUE;

		}

		return FALSE;        

    }  

  public function retirarValor($subTotal, $idPedido){

        $sql = "UPDATE pedidos set valorTotal = valorTotal - ? WHERE idPedidos = ?";
        $this->db->query($sql, array($subTotal, $idPedido));     

        if ($this->db->affected_rows() == '1')

		{
			return TRUE;

		}

		return FALSE;        

    } 

  public function visualizar($id)
	{

		$query =  "SELECT * FROM pedidos
					LEFT JOIN `clientes`
					ON pedidos.`clientes_id` = clientes.`cliente_id`
					LEFT JOIN `usuarios`
					ON pedidos.`usuarios_id` = usuarios.`usuario_id`
				    LEFT JOIN `itens_de_pedidos`
					ON pedidos.`idPedidos` = itens_de_pedidos.`pedidos_id`
					LEFT JOIN `produto`
					ON itens_de_pedidos.`produtos_id` = produto.`produto_id`
    
		      WHERE pedidos.`pedido_visivel` = 1 
		      AND pedidos.`idPedidos` = $id";
		// echo "<pre>";
		// exit($query);
        return $this->db->query($query)->result();

   }

   public function emitente()
	{
		$this->db->select("*");

		return $this->db->get('emitente')->result();
	}

   public function excluir($pedido_visivel, $id)
	{

		$query =  "UPDATE `pedidos` SET `pedido_visivel` = $pedido_visivel WHERE `idPedidos` = '$id'";
		// echo "<pre>";
		// exit($query);
        return $this->db->query($query);

   }

}