<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fornecedores_model extends CI_Model {
	
	public $tabela  = "fornecedores";
	public $visivel = "fornecedor_visivel";
	public $chave   = "fornecedor_id";

	public function listar()
	{
		$this->db->select("*");		
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}

	public function listarId($id)
	{
		$this->db->select("*");
		$this->db->where($this->chave, $id);
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}

	public function listarPedidos($id)
	{
		$this->db->select(" `idPedidos`, `dataPedido`, `observacao`, COUNT(idItens) AS itens, SUM(`quantidade`) AS volume ");
		$this->db->join('itens_de_pedidos_fornecedor ',' `pedidos_fornecedor`.`idPedidos` = `itens_de_pedidos_fornecedor`.`pedidos_fornecedor_id` ','LEFT');
		$this->db->where('fornecedor_id', $id);
		$this->db->where('pedido_visivel', 1);
		$this->db->group_by('idPedidos');
		
		return $this->db->get('pedidos_fornecedor')->result();
	}


	public function adicionar($dados)
	{
		$this->db->insert($this->tabela, $dados);

		if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE; 
	}

	public function editar($dados, $id)
	{
		$this->db->where($this->chave,$id);		
		
		if($this->db->update($this->tabela,$dados))
		{
			return true;
		}

		return false;
	}

	public function excluir($dados, $id)
	{
		$this->db->where($this->chave,$id);
		$this->db->update($this->tabela,$dados);
		
		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}

	public function visualizar($id)
	{
		$this->db->select("*");
		$this->db->where($this->chave, $id);
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}


	public function pegarEstados()
	{
		$this->db->select('*');
		return $this->db->get('estados')->result();
	}

	public function pegarEstadoId($sigla)
	{
		$this->db->select(' id_estado ');
		$this->db->where('sigla', $sigla);
		return $this->db->get('estados')->result();
	}

	public function pegarCidadeId($estado,$cidade)
	{
		$this->db->select(' id_cidade ');
		$this->db->where('nome', $cidade);
		$this->db->where('id_estado', $estado);
		return $this->db->get('cidades')->result();
	}

	public function pegarBancos()
	{
		$this->db->select('*');
		return $this->db->get('bancos')->result();
	}

	public function pegarGrupoProduto()
	{
		$this->db->select('*');
		$this->db->where('categoria_prod_visivel', 1);
		return $this->db->get('categoria_produto')->result();
	}

	public function autoCompleteFornecedor($termo)
	{
		$this->db->select('*');
        $this->db->limit(5);
        $this->db->like('fornecedor_nome', $termo);
        $this->db->where($this->visivel, 1);
        $query = $this->db->get($this->tabela)->result();

        if(count($query) > 0){
            foreach ($query as $row){
                $row_set[] = array('label'=>$row->fornecedor_nome.' | TEL: '.$row->fornecedor_telefone,'id'=>$row->fornecedor_id);
            }
            echo json_encode($row_set);
        }
	}


}