<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Balancoproduto_model extends CI_Model {
    public $tabela = 'balanco_produto';
	public $visivel = 'balanco_produto_visivel';
	public $chave = 'balanco_produto_id';

    public function preencherSelectCodigos($grupo)
	{
		$this->db->select('codigo_prod_codigo, codigo_prod_descricao'); // estava dando select na tabela 'produto', agora na tabela 'codigo_produto'
		$this->db->where('codigo_prod_categoria_id', $grupo);
		$this->db->where('codigo_prod_visivel', 1);
		$this->db->group_by('codigo_prod_codigo');
		$this->db->order_by('codigo_prod_codigo');

		return $this->db->get('codigo_produto')->result();

	}

	public function pegarBalancoProdutoCompleto()
	{
		$this->db->where($this->visivel, 1);
		$this->db->join('categoria_produto', 'categoria_produto.categoria_prod_id = balanco_produto.balanco_produto_categoria_produto_id');
		$this->db->join('codigo_produto', 'codigo_produto.codigo_prod_codigo = balanco_produto.balanco_produto_codigo_produto','LEFT');

		return $this->db->get($this->tabela)->result();
	}

    public function pegarGrupoProduto()
	{
		$this->db->select('*');
		$this->db->where('categoria_prod_visivel', 1);

		return $this->db->get('categoria_produto')->result();
	}

    public function adicionar($dados)
    {
        $this->db->insert($this->tabela, $dados);

		return $this->db->affected_rows() == '1' ? $this->db->insert_id($this->tabela) : false; 
    }

    public function pegarPorId($id)
    {
        $this->db->where($this->chave, $id);
        $this->db->where($this->visivel, 1);
		$this->db->join('categoria_produto', 'categoria_produto.categoria_prod_id = balanco_produto.balanco_produto_categoria_produto_id');

		return $this->db->get($this->tabela)->row();
    }

	public function editar($dados, $id)
	{
		$this->db->where($this->chave, $id);
		
		return $this->db->update($this->tabela, $dados);
	}

	public function pegarImeisDosProdutosDeUmCodigo($codigo)
	{
		$this->db->select('imei_valor');
		$this->db->where('vendas_id', null);
		$this->db->where('imei_visivel', 1);
		$this->db->where('produtos_id IN (SELECT produto_id FROM produto WHERE produto_codigo = "' . $codigo . '" AND produto_visivel = 1)', NULL, FALSE);

		$registros = $this->db->get('itens_de_imei')->result();
		$resultado = array();

		foreach ($registros as $registro) {
			$resultado[] = $registro->imei_valor;
		}

		return $resultado;
	}

	public function adicionarRelacaoProduto($dados)
	{
		$this->db->insert('itens_de_balanco', $dados);

		return $this->db->affected_rows() == '1' ? $this->db->insert_id('itens_de_balanco') : false; 
	}
}
