<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Estoque_model extends CI_Model {
	
	public $tabela  = "estoque_ajuste";
	public $visivel = "estoque_ajuste_visivel";
	public $chave   = "estoque_ajuste_id";

	public function listar()
	{
		$query = $this->db->query(" SELECT es.estoque_ajuste_id, ct.categoria_prod_descricao, es.estoque_ajuste_data,
			(SELECT COUNT(*) FROM itens_estoque_ajuste WHERE `quantidade_atual` IS NOT NULL AND itens_estoque_ajuste.`estoque_ajuste_id` = es.estoque_ajuste_id) AS feito,
			(SELECT COUNT(*) FROM itens_estoque_ajuste WHERE `quantidade_atual` IS NULL AND itens_estoque_ajuste.`estoque_ajuste_id` = es.estoque_ajuste_id) AS falta
			FROM `estoque_ajuste` AS es JOIN `categoria_produto` AS ct
			ON es.`estoque_ajuste_categoria_id` = ct.`categoria_prod_id` WHERE es.`estoque_ajuste_visivel` = 1 ORDER BY es.estoque_ajuste_id desc  ");
    	return $query->result();		
	}

	public function listarId($id)
	{
		$query = $this->db->query(' SELECT `idItens`,`produto_codigo`,`estoque_ajuste_data`, `estoque_ajuste_observacao`, `categoria_prod_descricao`, `produto_descricao`, `produto_gaveta`, `produto_estoque`, quantidade_atual, itens_estoque_ajuste.`produtos_id`
		    FROM `itens_estoque_ajuste` 
			INNER JOIN `produto` ON `itens_estoque_ajuste`.`produtos_id` = `produto`.`produto_id`
			INNER JOIN `categoria_produto` ON `categoria_produto`.`categoria_prod_id` = `produto`.`produto_categoria_id`
			INNER JOIN `estoque_ajuste`  ON `estoque_ajuste`.`estoque_ajuste_id` = `itens_estoque_ajuste`.`estoque_ajuste_id`  
			WHERE `produto`.`produto_visivel`= 1   AND  `itens_estoque_ajuste`.`estoque_ajuste_id` = '.$id.' ORDER BY `produto_descricao`;');
    	return $query->result();
	}

	public function visualizar($id)
	{
		$query = $this->db->query(' SELECT `idItens`,`produto_codigo`,`estoque_ajuste_data`, `estoque_ajuste_observacao`, `categoria_prod_descricao`, `produto_descricao`, `produto_gaveta`, `produto_estoque`, `estoque`, `usuario_nome`, quantidade_atual FROM `itens_estoque_ajuste` 
			
			 JOIN `produto` ON `itens_estoque_ajuste`.`produtos_id` = `produto`.`produto_id`
			LEFT JOIN `categoria_produto` ON `categoria_produto`.`categoria_prod_id` = `produto`.`produto_categoria_id`
			LEFT JOIN `estoque_ajuste`  ON `estoque_ajuste`.`estoque_ajuste_id` = `itens_estoque_ajuste`.`estoque_ajuste_id`
			LEFT JOIN `usuarios`  ON `usuarios`.`usuario_id` = `itens_estoque_ajuste`.`itens_estoque_ajuste_usuario_id`   
			WHERE `produto`.`produto_visivel`= 1   AND  `itens_estoque_ajuste`.`estoque_ajuste_id` = '.$id );
    	return $query->result();
	}


	public function adicionar($dados)
	{
		$this->db->insert($this->tabela, $dados);

		if ($this->db->affected_rows() == '1')
		{
			//return TRUE;
			return $this->db->insert_id($this->tabela);
		}
		
		return FALSE; 
	}

	public function editar($dados)
	{
		$this->db->where('produto_id',$dados['idProduto']);
		$this->db->set('produto_estoque', $dados['quantidade_atual']);	
		
		if($this->db->update('produto'))
		{
			return true;
		}

		return false;
	}

	public function editarAjusteEstoque($dados)
	{
		$this->db->where('produtos_id', $dados['idProduto']);
		$this->db->where('estoque_ajuste_id', $dados['idAjuste']);

		$this->db->set('quantidade_atual', $dados['quantidade_atual']);
		$this->db->set('itens_estoque_ajuste_usuario_id', $dados['itens_estoque_ajuste_usuario_id']);

		if($this->db->update('itens_estoque_ajuste'))
		{
			return true;
		}

		return false;
	}

	public function excluir($dados, $id)
	{
		$this->db->where($this->chave,$id);
		$this->db->update($this->tabela,$dados);
		
		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}

	public function pegarGrupoProduto() 
	{
		$this->db->select('*');
		$this->db->where('categoria_prod_visivel', 1);
		return $this->db->get('categoria_produto')->result();
	}

	public function prepararItens($categoria,$id)
	{
		$sql = ' INSERT INTO `itens_estoque_ajuste` (`estoque`,`produtos_id`,`estoque_ajuste_id`)
	        SELECT `produto_estoque`, `produto_id`, '.$id.' FROM produto WHERE `produto_categoria_id` = '.$categoria.' and `produto_visivel` = 1 ';
		return $this->db->query($sql);

	}



}