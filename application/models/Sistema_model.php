<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sistema_model extends CI_Model {
	/**
	 * [$tabela, $campoChave description] Atributo para facilitar a alteração da classe model com a tabela a ser utilizada
	 * @var string
	 */
	public $tabela = "usuarios";
	public $visivel = "usuario_visivel";
	
	
	public function botaoPanico()
	{	
		// code...
		$servidorAtual = BDCAMINHO ;
	    $servidorAtual = explode("_", $servidorAtual);
	    $servidorAtual = $servidorAtual[0].'_'.$servidorAtual[1];

		$this->db->select('emitente_botao_panico');
		return $this->db->get($servidorAtual.'_matriz.`emitente`')->result_array();
	}


	public function processarLogin($dados)
	{
		
		$this->db->select('*');
		$this->db->join('perfis','usuario_perfil = perfil_id');
		$this->db->where('usuario_email',$dados['usuario']);
		$this->db->where('usuario_senha',$dados['senha']);
		$this->db->where('usuario_ativo','1');
		$this->db->where($this->tabela.".".$this->visivel,"1");

		return $this->db->get('`emitente,`'.$this->tabela)->result();

	}	

	public function selecionarCidades($estado ='')
	{
		$this->db->select('  `cidades`.`id_cidade`, `cidades`.`nome` ');
		$this->db->join('`estados`','`cidades`.`id_estado` = `estados`.`id_estado`');
		$this->db->where('sigla',$estado);
		return $this->db->get('cidades')->result();
	}


	public function totalUsuarios()
	{
		$this->db->like($this->visivel, '1');
		$this->db->from($this->tabela);
		return $this->db->count_all_results();
	}
	public function totalClientes()
	{
		$this->db->like('cliente_visivel', '1');
		$this->db->from('clientes');
		return $this->db->count_all_results();
	}
	public function totalVendas()
	{
		$this->db->like('venda_visivel', '1');
		$this->db->from('vendas');
		return $this->db->count_all_results();
	}
	public function totalPedidos()
	{
		$this->db->like('pedido_visivel', '1');
		$this->db->from('pedidos');
		return $this->db->count_all_results();
	}

	public function graficoHora()
	{

        $this->db->distinct();
		$this->db->select("(SELECT COUNT(vendas.idVendas) FROM vendas
				WHERE `dataVenda` = CURDATE() AND HOUR(horaVenda) = 8 ) AS 8hs,
				(SELECT COUNT(vendas.idVendas)  FROM vendas
				WHERE `dataVenda` = CURDATE() AND HOUR(horaVenda) = 9 ) AS 9hs,
				(SELECT COUNT(vendas.idVendas)  FROM vendas
				WHERE `dataVenda` = CURDATE() AND HOUR(horaVenda) = 10 ) AS 10hs,
				(SELECT COUNT(vendas.idVendas)  FROM vendas
				WHERE `dataVenda` = CURDATE() AND HOUR(horaVenda) = 11 ) AS 11hs,
				(SELECT COUNT(vendas.idVendas)  FROM vendas
				WHERE `dataVenda` = CURDATE() AND HOUR(horaVenda) = 12 ) AS 12hs,
				(SELECT COUNT(vendas.idVendas)  FROM vendas
				WHERE `dataVenda` = CURDATE() AND HOUR(horaVenda) = 13 ) AS 13hs,
				(SELECT COUNT(vendas.idVendas)  FROM vendas
				WHERE `dataVenda` = CURDATE() AND HOUR(horaVenda) = 14 ) AS 14hs,
				(SELECT COUNT(vendas.idVendas)  FROM vendas
				WHERE `dataVenda` = CURDATE() AND HOUR(horaVenda) = 15 ) AS 15hs,
				(SELECT COUNT(vendas.idVendas)  FROM vendas
				WHERE `dataVenda` = CURDATE() AND HOUR(horaVenda) = 16 ) AS 16hs,
				(SELECT COUNT(vendas.idVendas)  FROM vendas
				WHERE `dataVenda` = CURDATE() AND HOUR(horaVenda) = 17 ) AS 17hs,
				(SELECT COUNT(vendas.idVendas)  FROM vendas
				WHERE `dataVenda` = CURDATE() AND HOUR(horaVenda) = 18 ) AS 18hs       
				  FROM vendas 
				 WHERE `dataVenda` = CURDATE()
				 AND `venda_visivel` = 1
				 
				 UNION ALL
				 
				 SELECT DISTINCT
				(SELECT COUNT(vendas.idVendas) FROM vendas
				WHERE `dataVenda` = SUBDATE(CURDATE(),7) AND HOUR(horaVenda) = 7 ) AS 8hs,
				(SELECT COUNT(vendas.idVendas)  FROM vendas
				WHERE `dataVenda` = SUBDATE(CURDATE(),7) AND HOUR(horaVenda) = 9 ) AS 9hs,
				(SELECT COUNT(vendas.idVendas)  FROM vendas
				WHERE `dataVenda` = SUBDATE(CURDATE(),7) AND HOUR(horaVenda) = 10 ) AS 10hs,
				(SELECT COUNT(vendas.idVendas)  FROM vendas
				WHERE `dataVenda` = SUBDATE(CURDATE(),7) AND HOUR(horaVenda) = 11 ) AS 11hs,
				(SELECT COUNT(vendas.idVendas)  FROM vendas
				WHERE `dataVenda` = SUBDATE(CURDATE(),7) AND HOUR(horaVenda) = 12 ) AS 12hs,
				(SELECT COUNT(vendas.idVendas)  FROM vendas
				WHERE `dataVenda` = SUBDATE(CURDATE(),7) AND HOUR(horaVenda) = 13 ) AS 13hs,
				(SELECT COUNT(vendas.idVendas)  FROM vendas
				WHERE `dataVenda` = SUBDATE(CURDATE(),7) AND HOUR(horaVenda) = 14 ) AS 14hs,
				(SELECT COUNT(vendas.idVendas)  FROM vendas
				WHERE `dataVenda` = SUBDATE(CURDATE(),7) AND HOUR(horaVenda) = 15 ) AS 15hs,
				(SELECT COUNT(vendas.idVendas)  FROM vendas
				WHERE `dataVenda` = SUBDATE(CURDATE(),7) AND HOUR(horaVenda) = 16 ) AS 16hs,
				(SELECT COUNT(vendas.idVendas)  FROM vendas
				WHERE `dataVenda` = SUBDATE(CURDATE(),7) AND HOUR(horaVenda) = 17 ) AS 17hs,
				(SELECT COUNT(vendas.idVendas)  FROM vendas
				WHERE `dataVenda` = SUBDATE(CURDATE(),7) AND HOUR(horaVenda) = 18 ) AS 18hs ");
		$this->db->where('`venda_visivel`',1);
		$this->db->where("`dataVenda` = SUBDATE(CURDATE(),7)");
		return $this->db->get('vendas')->result_array();
	}

	public function dadosGraficosAux()
	{
		$this->db->select("SUM(`itens_de_vendas`.`quantidade`)AS total_hoje,
				`categoria_produto`.`categoria_prod_descricao` AS tb1_desc,
				( 
				SELECT SUM(`itens_de_vendas`.`quantidade`)
				FROM `itens_de_vendas`
				INNER JOIN vendas ON `itens_de_vendas`.`vendas_id` = vendas.`idVendas`
				INNER JOIN `produto` ON `itens_de_vendas`.`produtos_id` = `produto`.`produto_id`
				INNER JOIN `categoria_produto` ON `categoria_produto`.`categoria_prod_id` = `produto`.`produto_categoria_id` 

				WHERE vendas.`dataVenda` = SUBDATE(CURDATE(),7)
				AND venda_visivel = 1
				AND tb1_desc = `categoria_produto`.`categoria_prod_descricao`
				GROUP BY `categoria_produto`.`categoria_prod_descricao`

				 )AS total_passado");
		$this->db->join('vendas ','`itens_de_vendas`.`vendas_id` = vendas.`idVendas` ');
		$this->db->join('produto ','`itens_de_vendas`.`produtos_id` = `produto`.`produto_id` ');
		$this->db->join('categoria_produto ','`categoria_produto`.`categoria_prod_id` = `produto`.`produto_categoria_id` ');
		$this->db->where('venda_visivel',1);
		$this->db->where("vendas.`dataVenda` = CURDATE()");
		$this->db->order_by('total_hoje', 'desc');
        $this->db->group_by('`categoria_produto`.`categoria_prod_descricao`');
        $this->db->limit(4);
		return $this->db->get('itens_de_vendas')->result_array();
	}

	public function pegarEmitente()
	{
		$this->db->select('*');
		return $this->db->get('emitente')->result();
	}
	
	public function editarEmitente($dados, $id)
	{
		$this->db->where('emitente_id',$id);		
		
		if($this->db->update('emitente',$dados))
		{
			return true;
		}

		return false;
	}

	public function editarLogoEmitente($dados, $id)
	{
		$this->db->where('emitente_id',$id);		
		
		if($this->db->update('emitente',$dados))
		{
			return true;
		}

		return false;
	}

	public function alterarSenha($dados)
	{
		$this->db->where('usuario_id',$this->session->userdata('usuario_id'));		
		
		if($this->db->update('usuarios',$dados))
		{
			return true;
		}

		return false;
	}

	public function pesquisarClientes($pesquisa)
	{
		$this->db->select("cliente_id,
							cliente_nome,
							cliente_cpf_cnpj,
							cliente_telefone,
							cliente_celular,		
							IF(cliente_tipo = 1,'FÍSICA','JURÍDICA') AS tipo");
		
		$this->db->where('cliente_visivel', 1);

		// if(!verificarPermissao('vTudo')){
		// 	$this->db->where('cpf_consultor', $this->session->userdata('usuario_cpf'));
		// }

		$this->db->like('cliente_nome', $pesquisa);
		$this->db->or_like('cliente_cpf_cnpj', $pesquisa);
		$this->db->limit(10);
		return $this->db->get('clientes')->result();
		
	}

	public function pesquisarfornecedores($pesquisa)
	{
		$this->db->select("fornecedor_id,
							fornecedor_nome,
							fornecedor_cnpj_cpf,
							fornecedor_telefone,
							fornecedor_celular,		
							IF(fornecedor_tipo = 1,'FÍSICA','JURÍDICA') AS tipo");
		
		$this->db->where('fornecedor_visivel', 1);

		// if(!verificarPermissao('vTudo')){
		// 	$this->db->where('cpf_consultor', $this->session->userdata('usuario_cpf'));
		// }

		$this->db->like('fornecedor_nome', $pesquisa);
		$this->db->or_like('fornecedor_cnpj_cpf', $pesquisa);
		$this->db->limit(10);
		return $this->db->get('fornecedores')->result();
		
	}

	public function pesquisarVendas($pesquisa)
	{
		$this->db->select('clientes.cliente_nome, idVendas, venda_visivel, usuarios.usuario_nome');
		$this->db->join('clientes ','`vendas`.`clientes_id` = `clientes`.`cliente_id` ');	
		$this->db->join('usuarios ','`vendas`.`usuarios_id` = `usuarios`.`usuario_id` ');
		
		$this->db->where('venda_visivel', 1);

		$this->db->like('clientes.cliente_nome', $pesquisa);
		$this->db->or_like('idVendas', $pesquisa);
		$this->db->order_by('idVendas', 'desc');
		$this->db->limit(10);
		return $this->db->get('vendas')->result();
		
	}

	public function pesquisarFinanceiro($pesquisa)
	{
		$this->db->select('financeiro_forn_clie, clientes_id, vendas_id, idFinanceiro, financeiro_tipo,financeiro_baixado,financeiro_valor');
		$this->db->join('vendas ','`vendas`.`idVendas` = `financeiro`.`vendas_id`' , 'left');
		
		$this->db->where('venda_visivel', 1);

		$this->db->like('financeiro_forn_clie', $pesquisa);
		$this->db->or_like('vendas_id', $pesquisa);
		$this->db->order_by('idFinanceiro', 'desc');
		$this->db->limit(10);
		return $this->db->get('financeiro')->result();
		
	}

    public function pesquisarProdutosSSSS($pesquisa)
	{

	$query = $this->db->query(" 
	SELECT produto_id, produto_codigo, produto_descricao, produto_estoque AS estoque_carlos,

	(SELECT produto_estoque  FROM `mixcel17_megacell_mega`.`produto` 
		WHERE produto_visivel = 1 AND `mixcel17_megacell_carlos`.`produto`.`produto_id` = `mixcel17_megacell_mega`.`produto`.`produto_id` ) AS estoque_mega,

	(SELECT produto_estoque  FROM `mixcel17_megacell_multpecas`.`produto` 
		WHERE produto_visivel = 1 AND `mixcel17_megacell_carlos`.`produto`.`produto_id` = `mixcel17_megacell_multpecas`.`produto`.`produto_id` ) AS estoque_multipecas,
	(SELECT produto_estoque  FROM `mixcel17_megacell_paloma`.`produto` 
		WHERE produto_visivel = 1 AND `mixcel17_megacell_carlos`.`produto`.`produto_id` = `mixcel17_megacell_paloma`.`produto`.`produto_id` ) AS estoque_paloma,	
	(SELECT produto_estoque  FROM `mixcel17_megacell_cell`.`produto` 
		WHERE produto_visivel = 1 AND `mixcel17_megacell_carlos`.`produto`.`produto_id` = `mixcel17_megacell_cell`.`produto`.`produto_id` ) AS estoque_cell,	
	(SELECT produto_estoque  FROM `mixcel17_megacell_mix`.`produto` 
		WHERE produto_visivel = 1 AND `mixcel17_megacell_carlos`.`produto`.`produto_id` = `mixcel17_megacell_mix`.`produto`.`produto_id` ) AS estoque_mix,				
	produto_preco_minimo_venda, produto_preco_venda FROM `mixcel17_megacell_carlos`.`produto`
	WHERE produto_visivel = 1 AND (produto_codigo LIKE '%".$pesquisa."%' OR produto_descricao LIKE '%".$pesquisa."%')
	LIMIT 15  ");

	return $query->result();
		
	}

	public function pesquisarProdutos($pesquisa)
	{
		$sql = " SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE '%_".GRUPOLOJA."_%' ";
        $bases =  $this->db->query($sql)->result();       

        $loja1 = explode('_', $bases[0]->schema_name);
        $sql = ' SELECT produto_id, produto_codigo, produto_descricao, produto_estoque AS estoque_'.$loja1[2].', ';

        // var_dump($bases);die();

        foreach ($bases as $b) {

        	if ($bases[0]->schema_name == $b->schema_name) {
        	 	continue;
        	 } 
        		

        	$loja = explode('_', $b->schema_name);   
        	
        	$sql = $sql. ' (SELECT produto_estoque  FROM '.$b->schema_name.'.`produto` 
		WHERE produto_visivel = 1 AND '.$bases[0]->schema_name.'.`produto`.`produto_id` = '.$b->schema_name.'.`produto`.`produto_id` ) AS estoque_'.$loja[2].', ';
        
        };

        $sql = $sql. ' produto_preco_minimo_venda, produto_preco_venda FROM '.$bases[0]->schema_name.'.`produto`
		WHERE produto_visivel = 1 AND (produto_codigo LIKE "%'.$pesquisa.'%" OR produto_descricao LIKE "%'.$pesquisa.'%") LIMIT 15 ';


        // echo $sql; die();
        // var_dump($sql);die();	

        return $this->db->query($sql)->result();


    }


    public function getAllFilias()
    {

        $sql = " SELECT schema_name, SUBSTRING_INDEX(schema_name,'_',-1) AS loja FROM information_schema.schemata WHERE schema_name LIKE '%_".GRUPOLOJA."_%' ";

        
        $bases = $this->db->query($sql)->result();
 
        return $bases;

    }


	public function pesquisarProdutoVenda($pesquisa)
	{
		$this->db->select("vendas.`idVendas`, vendas.`dataVenda`, `itens_de_vendas`.`quantidade`, produto.`produto_descricao`, imei_valor");				
		$this->db->join('itens_de_vendas ','`vendas`.`idVendas` = `itens_de_vendas`.`vendas_id`');
		$this->db->join('produto ','`itens_de_vendas`.`produtos_id` = `produto`.`produto_id`');
		
		$this->db->like('produto_descricao', $pesquisa);
		$this->db->or_like('imei_valor', $pesquisa);
		$this->db->where('venda_visivel', 1);	
		$this->db->order_by('dataVenda','DESC');	
		$this->db->limit(10);

		return $this->db->get('vendas')->result();
		
	}

	public function pesquisarprodMercadoria($pesquisa)	{
		
		$this->db->select("`movimentacao_produto_id`, `movimentacao_produto_tipo`,  `movimentacao_produto_data_cadastro`, `produto_descricao`, `item_movimentacao_produto_quantidade` ");
		$this->db->join('itens_movimentacao_produtos ','`movimentacao_produtos`.`movimentacao_produto_id` = itens_movimentacao_produtos.`item_movimentacao_produto_movimentacao_produto_id`');
		$this->db->join('produto ','produto.`produto_id` = itens_movimentacao_produtos.`item_movimentacao_produto_produto_id`');

		$this->db->join('itens_de_imei', '`itens_de_imei`.`produtos_id` = `produto`.`produto_id` 
	    AND `movimentacao_produtos`.`movimentacao_produto_id` = `itens_de_imei`.`movimentacao_id`
	    AND `itens_de_imei`.`imei_visivel` = 1 ', 'LEFT'); //  modulo - Cell  

		$this->db->where('movimentacao_produto_visivel', 1);	
	
		$this->db->like('produto_descricao', $pesquisa);

		if (is_numeric($pesquisa)) {
        $this->db->or_like('imei_valor',$pesquisa); //  modulo - Cell
        $this->db->group_by('movimentacao_produto_tipo');
        }

		$this->db->order_by('movimentacao_produto_data_cadastro','DESC');
		$this->db->limit(10);
		return $this->db->get('movimentacao_produtos')->result();
	}


	public function pesquisarprodAjusteEstoque($pesquisa)	{
		$this->db->select(" `estoque_ajuste`.`estoque_ajuste_id`, `estoque_ajuste_data`, `produto_descricao`, itens_estoque_ajuste.`estoque`, itens_estoque_ajuste.`quantidade_atual` ");
		$this->db->join('itens_estoque_ajuste ','estoque_ajuste.`estoque_ajuste_id` = itens_estoque_ajuste.`estoque_ajuste_id`');
		$this->db->join('produto ',' itens_estoque_ajuste.`produtos_id` = `produto`.`produto_id`');
		$this->db->where('estoque_ajuste_visivel', 1);
		$this->db->like('produto_descricao', $pesquisa);
		$this->db->order_by('estoque_ajuste_data','DESC');
		$this->db->limit(10);
		
		return $this->db->get('estoque_ajuste')->result();	
	}

	public function pesquisarprodEstornado($pesquisa)	{
		$this->db->select(" `produto_avaria_id`, `produto_avaria_cadastro`, produto_descricao, `produto_avarias_quantidade`, `produto_avaria_motivo`  ");
		$this->db->join('produto ','produto_avarias.`produto_id` = `produto`.`produto_id`');
		$this->db->where('produto_avaria_visivel', 1);
		$this->db->like('produto_descricao', $pesquisa);
		$this->db->or_like('produto_avaria_antigo_imei', $pesquisa);	
		$this->db->or_like('produto_avaria_novo_imei', $pesquisa);
		$this->db->order_by('produto_avaria_cadastro','DESC');
		$this->db->limit(10);
		return $this->db->get('produto_avarias')->result();
	}


	public function pesquisarOs($pesquisa)	{
		$this->db->select(" usuarios.usuario_nome,  os.* ");
		$this->db->join('usuarios ','`os`.`tec_id` = `usuarios`.`usuario_id` ','LEFT');		

		$this->db->where('os_visivel', 1);
		$this->db->like('os_id', $pesquisa);	
		$this->db->or_like('os_marca', $pesquisa);
		$this->db->or_like('os_modelo', $pesquisa);
		$this->db->or_like('os_serie', $pesquisa);		
		$this->db->order_by('os_id','DESC');
		$this->db->limit(10);
		return $this->db->get('os')->result();
	}


	public function apiEntrada()
	{

		$query = $this->db->query(" SELECT COUNT(*) AS total FROM `movimentacao_produtos` WHERE `movimentacao_produto_saida_filial` <> '' AND `movimentacao_produto_status` = 1 AND movimentacao_produto_visivel = 1; ");

	    return $query->result();

	}	

	public function apiProd20()
	{

		$query = $this->db->query(" SELECT COUNT(*) as total FROM `produto`
					WHERE `produto_data_ultima_venda` <  SUBDATE(CURRENT_DATE, 20); ");

	    return $query->result();

	}

	public function apiProdLjParc()
	{
		$lojaAtual = BDCAMINHO ;
	    $lojaAtual = explode("_", $lojaAtual);
	    $lojaAtual = $lojaAtual[2];

		$query = $this->db->query(" SELECT COUNT(`idItens`) as total FROM `itens_de_vendas`
		INNER JOIN vendas ON `itens_de_vendas`.`vendas_id` = `vendas`.`idVendas`
		WHERE `filial` <> '{$lojaAtual}' AND dataVenda = CURDATE() ");

	    return $query->result();

	}

	public function apiVenda()
	{

		$query = $this->db->query(" SELECT  idvendas AS total FROM `vendas`
		RIGHT JOIN `itens_de_vendas` ON vendas.`idVendas` = `itens_de_vendas`.`vendas_id`
		WHERE `valorTotal` IS NULL
		 AND `faturado` IS NULL 
		 AND vendas.`venda_visivel` = 1 
		GROUP BY idvendas  ; ");

	    return $query->result();

	}

	public function apiVendaZerada()
	{

		$query = $this->db->query(" SELECT idVendas FROM vendas 
		INNER JOIN `financeiro` ON vendas.`idVendas` = `financeiro`.`vendas_id`
		WHERE `financeiro_visivel` = 1 AND `venda_visivel` = 0 

		UNION ALL			
			
		SELECT  idVendas FROM vendas 
			RIGHT JOIN `itens_de_vendas` ON vendas.`idVendas` = `itens_de_vendas`.`vendas_id`
			LEFT JOIN `clientes`
			ON vendas.`clientes_id` = clientes.`cliente_id`
			LEFT JOIN `usuarios`
			ON vendas.`usuarios_id` = usuarios.`usuario_id`
			WHERE vendas.`venda_visivel` = 0 AND vendas.`usuarios_id_exclusao` = 0 ; ");

	    return $query->result();

	}


	public function apiVendaIncompleta()
	{

		$query = $this->db->query(" SELECT p.`produto_descricao`, p.`produto_estoque`, Tb2.IMEI_Cadastrado FROM produto AS p,
		(SELECT `produto_descricao`, `produto_estoque`, COUNT(produtos_id) AS IMEI_Cadastrado, produtos_id FROM itens_de_imei 
		INNER JOIN `produto` ON produto.`produto_id` = `itens_de_imei`.`produtos_id`
		WHERE imei_visivel = 1 AND vendas_id IS NULL
		GROUP BY produtos_id) AS Tb2
		WHERE   Tb2.IMEI_Cadastrado <> p.produto_estoque
		AND Tb2.produtos_id = p.`produto_id`;");

	    return $query->result();

	}



	public function graficoEmpresaReceita()
	{

       $query = $this->db->query("

	SELECT 'Jan' AS Ano, 
	( SELECT SUM(financeiro_valor) FROM `financeiro` WHERE YEAR(data_pagamento) = YEAR(CURDATE())    AND  MONTH(data_pagamento) = 1 AND `financeiro_tipo` = 'receita' AND `financeiro_visivel` = 1) AS atual,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -1  AND  MONTH(data_pagamento) = 1 AND `financeiro_tipo` = 'receita' AND `financeiro_visivel` = 1) AS meio,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -2  AND  MONTH(data_pagamento) = 1 AND `financeiro_tipo` = 'receita' AND `financeiro_visivel` = 1) AS fim
	FROM financeiro


	UNION

	SELECT 'Fev' AS Ano, 
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE())     AND  MONTH(data_pagamento) = 2 AND `financeiro_tipo` = 'receita' AND `financeiro_visivel` = 1) AS atual,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -1  AND  MONTH(data_pagamento) = 2 AND `financeiro_tipo` = 'receita' AND `financeiro_visivel` = 1) AS meio,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -2  AND  MONTH(data_pagamento) = 2 AND `financeiro_tipo` = 'receita' AND `financeiro_visivel` = 1) AS fim
	FROM financeiro

	UNION

	SELECT 'Mar' AS Ano, 
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE())     AND  MONTH(data_pagamento) = 3 AND `financeiro_tipo` = 'receita' AND `financeiro_visivel` = 1) AS atual,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -1  AND  MONTH(data_pagamento) = 3 AND `financeiro_tipo` = 'receita' AND `financeiro_visivel` = 1) AS meio,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -2  AND  MONTH(data_pagamento) = 3 AND `financeiro_tipo` = 'receita' AND `financeiro_visivel` = 1) AS fim
	FROM financeiro

	UNION

	SELECT 'Abr' AS Ano, 
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE())     AND  MONTH(data_pagamento) = 4 AND `financeiro_tipo` = 'receita' AND `financeiro_visivel` = 1) AS atual,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -1  AND  MONTH(data_pagamento) = 4 AND `financeiro_tipo` = 'receita' AND `financeiro_visivel` = 1) AS meio,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -2  AND  MONTH(data_pagamento) = 4 AND `financeiro_tipo` = 'receita' AND `financeiro_visivel` = 1) AS fim
	FROM financeiro

	UNION

	SELECT 'Mai' AS Ano, 
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE())     AND  MONTH(data_pagamento) = 5 AND `financeiro_tipo` = 'receita' AND `financeiro_visivel` = 1) AS atual,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -1  AND  MONTH(data_pagamento) = 5 AND `financeiro_tipo` = 'receita' AND `financeiro_visivel` = 1) AS meio,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -2  AND  MONTH(data_pagamento) = 5 AND `financeiro_tipo` = 'receita' AND `financeiro_visivel` = 1) AS fim
	FROM financeiro

	UNION

	SELECT 'Jun' AS Ano, 
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE())     AND  MONTH(data_pagamento) = 6 AND `financeiro_tipo` = 'receita' AND `financeiro_visivel` = 1) AS atual,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -1  AND  MONTH(data_pagamento) = 6 AND `financeiro_tipo` = 'receita' AND `financeiro_visivel` = 1) AS meio,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -2  AND  MONTH(data_pagamento) = 6 AND `financeiro_tipo` = 'receita' AND `financeiro_visivel` = 1) AS fim
	FROM financeiro

	UNION

	SELECT 'Jul' AS Ano, 
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE())     AND  MONTH(data_pagamento) = 7 AND `financeiro_tipo` = 'receita' AND `financeiro_visivel` = 1) AS atual,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -1  AND  MONTH(data_pagamento) = 7 AND `financeiro_tipo` = 'receita' AND `financeiro_visivel` = 1) AS meio,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -2  AND  MONTH(data_pagamento) = 7 AND `financeiro_tipo` = 'receita' AND `financeiro_visivel` = 1) AS fim
	FROM financeiro

	UNION

	SELECT 'Ago' AS Ano, 
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE())     AND  MONTH(data_pagamento) = 8 AND `financeiro_tipo` = 'receita' AND `financeiro_visivel` = 1) AS atual,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -1  AND  MONTH(data_pagamento) = 8 AND `financeiro_tipo` = 'receita' AND `financeiro_visivel` = 1) AS meio,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -2  AND  MONTH(data_pagamento) = 8 AND `financeiro_tipo` = 'receita' AND `financeiro_visivel` = 1) AS fim
	FROM financeiro

	UNION

	SELECT 'Set' AS Ano, 
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE())     AND  MONTH(data_pagamento) = 9 AND `financeiro_tipo` = 'receita' AND `financeiro_visivel` = 1) AS atual,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -1  AND  MONTH(data_pagamento) = 9 AND `financeiro_tipo` = 'receita' AND `financeiro_visivel` = 1) AS meio,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -2  AND  MONTH(data_pagamento) = 9 AND `financeiro_tipo` = 'receita' AND `financeiro_visivel` = 1) AS fim
	FROM financeiro

	UNION

	SELECT 'Out' AS Ano, 
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE())     AND  MONTH(data_pagamento) = 10 AND `financeiro_tipo` = 'receita' AND `financeiro_visivel` = 1) AS atual,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -1  AND  MONTH(data_pagamento) = 10 AND `financeiro_tipo` = 'receita' AND `financeiro_visivel` = 1) AS meio,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -2  AND  MONTH(data_pagamento) = 10 AND `financeiro_tipo` = 'receita' AND `financeiro_visivel` = 1) AS fim
	FROM financeiro

	UNION

	SELECT 'Nov' AS Ano, 
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE())     AND  MONTH(data_pagamento) = 11 AND `financeiro_tipo` = 'receita' AND `financeiro_visivel` = 1) AS atual,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -1  AND  MONTH(data_pagamento) = 11 AND `financeiro_tipo` = 'receita' AND `financeiro_visivel` = 1) AS meio,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -2  AND  MONTH(data_pagamento) = 11 AND `financeiro_tipo` = 'receita' AND `financeiro_visivel` = 1) AS fim
	FROM financeiro

	UNION

	SELECT 'Dez' AS Ano, 
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE())     AND  MONTH(data_pagamento) = 12 AND `financeiro_tipo` = 'receita' AND `financeiro_visivel` = 1) AS atual,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -1  AND  MONTH(data_pagamento) = 12 AND `financeiro_tipo` = 'receita' AND `financeiro_visivel` = 1) AS meio,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -2  AND  MONTH(data_pagamento) = 12 AND `financeiro_tipo` = 'receita' AND `financeiro_visivel` = 1) AS fim
	FROM financeiro

	       	");

	       return $query->result();
		
	}


	public function graficoEmpresaDespesa()
	{

       $query = $this->db->query("

	SELECT 'Jan' AS Ano, 
	( SELECT SUM(financeiro_valor) FROM `financeiro` WHERE YEAR(data_pagamento) = YEAR(CURDATE())    AND  MONTH(data_pagamento) = 1 AND `financeiro_tipo` = 'despesa' AND `financeiro_visivel` = 1) AS atual,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -1  AND  MONTH(data_pagamento) = 1 AND `financeiro_tipo` = 'despesa' AND `financeiro_visivel` = 1) AS meio,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -2  AND  MONTH(data_pagamento) = 1 AND `financeiro_tipo` = 'despesa' AND `financeiro_visivel` = 1) AS fim
	FROM financeiro


	UNION

	SELECT 'Fev' AS Ano, 
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE())     AND  MONTH(data_pagamento) = 2 AND `financeiro_tipo` = 'despesa' AND `financeiro_visivel` = 1) AS atual,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -1  AND  MONTH(data_pagamento) = 2 AND `financeiro_tipo` = 'despesa' AND `financeiro_visivel` = 1) AS meio,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -2  AND  MONTH(data_pagamento) = 2 AND `financeiro_tipo` = 'despesa' AND `financeiro_visivel` = 1) AS fim
	FROM financeiro

	UNION

	SELECT 'Mar' AS Ano, 
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE())     AND  MONTH(data_pagamento) = 3 AND `financeiro_tipo` = 'despesa' AND `financeiro_visivel` = 1) AS atual,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -1  AND  MONTH(data_pagamento) = 3 AND `financeiro_tipo` = 'despesa' AND `financeiro_visivel` = 1) AS meio,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -2  AND  MONTH(data_pagamento) = 3 AND `financeiro_tipo` = 'despesa' AND `financeiro_visivel` = 1) AS fim
	FROM financeiro

	UNION

	SELECT 'Abr' AS Ano, 
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE())     AND  MONTH(data_pagamento) = 4 AND `financeiro_tipo` = 'despesa' AND `financeiro_visivel` = 1) AS atual,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -1  AND  MONTH(data_pagamento) = 4 AND `financeiro_tipo` = 'despesa' AND `financeiro_visivel` = 1) AS meio,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -2  AND  MONTH(data_pagamento) = 4 AND `financeiro_tipo` = 'despesa' AND `financeiro_visivel` = 1) AS fim
	FROM financeiro

	UNION

	SELECT 'Mai' AS Ano, 
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE())     AND  MONTH(data_pagamento) = 5 AND `financeiro_tipo` = 'despesa' AND `financeiro_visivel` = 1) AS atual,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -1  AND  MONTH(data_pagamento) = 5 AND `financeiro_tipo` = 'despesa' AND `financeiro_visivel` = 1) AS meio,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -2  AND  MONTH(data_pagamento) = 5 AND `financeiro_tipo` = 'despesa' AND `financeiro_visivel` = 1) AS fim
	FROM financeiro

	UNION

	SELECT 'Jun' AS Ano, 
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE())     AND  MONTH(data_pagamento) = 6 AND `financeiro_tipo` = 'despesa' AND `financeiro_visivel` = 1) AS atual,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -1  AND  MONTH(data_pagamento) = 6 AND `financeiro_tipo` = 'despesa' AND `financeiro_visivel` = 1) AS meio,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -2  AND  MONTH(data_pagamento) = 6 AND `financeiro_tipo` = 'despesa' AND `financeiro_visivel` = 1) AS fim
	FROM financeiro

	UNION

	SELECT 'Jul' AS Ano, 
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE())     AND  MONTH(data_pagamento) = 7 AND `financeiro_tipo` = 'despesa' AND `financeiro_visivel` = 1) AS atual,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -1  AND  MONTH(data_pagamento) = 7 AND `financeiro_tipo` = 'despesa' AND `financeiro_visivel` = 1) AS meio,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -2  AND  MONTH(data_pagamento) = 7 AND `financeiro_tipo` = 'despesa' AND `financeiro_visivel` = 1) AS fim
	FROM financeiro

	UNION

	SELECT 'Ago' AS Ano, 
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE())     AND  MONTH(data_pagamento) = 8 AND `financeiro_tipo` = 'despesa' AND `financeiro_visivel` = 1) AS atual,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -1  AND  MONTH(data_pagamento) = 8 AND `financeiro_tipo` = 'despesa' AND `financeiro_visivel` = 1) AS meio,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -2  AND  MONTH(data_pagamento) = 8 AND `financeiro_tipo` = 'despesa' AND `financeiro_visivel` = 1) AS fim
	FROM financeiro

	UNION

	SELECT 'Set' AS Ano, 
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE())     AND  MONTH(data_pagamento) = 9 AND `financeiro_tipo` = 'despesa' AND `financeiro_visivel` = 1) AS atual,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -1  AND  MONTH(data_pagamento) = 9 AND `financeiro_tipo` = 'despesa' AND `financeiro_visivel` = 1) AS meio,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -2  AND  MONTH(data_pagamento) = 9 AND `financeiro_tipo` = 'despesa' AND `financeiro_visivel` = 1) AS fim
	FROM financeiro

	UNION

	SELECT 'Out' AS Ano, 
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE())     AND  MONTH(data_pagamento) = 10 AND `financeiro_tipo` = 'despesa' AND `financeiro_visivel` = 1) AS atual,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -1  AND  MONTH(data_pagamento) = 10 AND `financeiro_tipo` = 'despesa' AND `financeiro_visivel` = 1) AS meio,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -2  AND  MONTH(data_pagamento) = 10 AND `financeiro_tipo` = 'despesa' AND `financeiro_visivel` = 1) AS fim
	FROM financeiro

	UNION

	SELECT 'Nov' AS Ano, 
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE())     AND  MONTH(data_pagamento) = 11 AND `financeiro_tipo` = 'despesa' AND `financeiro_visivel` = 1) AS atual,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -1  AND  MONTH(data_pagamento) = 11 AND `financeiro_tipo` = 'despesa' AND `financeiro_visivel` = 1) AS meio,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -2  AND  MONTH(data_pagamento) = 11 AND `financeiro_tipo` = 'despesa' AND `financeiro_visivel` = 1) AS fim
	FROM financeiro

	UNION

	SELECT 'Dez' AS Ano, 
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE())     AND  MONTH(data_pagamento) = 12 AND `financeiro_tipo` = 'despesa' AND `financeiro_visivel` = 1) AS atual,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -1  AND  MONTH(data_pagamento) = 12 AND `financeiro_tipo` = 'despesa' AND `financeiro_visivel` = 1) AS meio,
	( SELECT  SUM(financeiro_valor) FROM financeiro WHERE YEAR(data_pagamento) = YEAR(CURDATE()) -2  AND  MONTH(data_pagamento) = 12 AND `financeiro_tipo` = 'despesa' AND `financeiro_visivel` = 1) AS fim
	FROM financeiro

	       	");

	       return $query->result();
		
	}


	public function totalEntrada()
	{
		// ENTRADA POR CATEGORIA
		$query = $this->db->query(" SELECT SUM(`item_movimentacao_produto_quantidade`)  AS Total FROM `movimentacao_produtos`
		INNER JOIN  `itens_movimentacao_produtos` ON `itens_movimentacao_produtos`.`item_movimentacao_produto_movimentacao_produto_id` = `movimentacao_produtos`.`movimentacao_produto_id`
		INNER JOIN `produto` ON `produto`.`produto_id` = `itens_movimentacao_produtos`.`item_movimentacao_produto_produto_id` 
		 WHERE `movimentacao_produto_tipo` = 'Entrada'
		 AND `movimentacao_produto_imei` = 1
		 AND `movimentacao_produto_visivel` = 1
		 -- AND `produto_categoria_id`  = 27 ");

		return $query->result();

	}

	public function totalVenda()
	{
		// VENDAS POR CATEGORIA 
		$query = $this->db->query("  SELECT COUNT(`idItens`)  AS Total FROM vendas
		 INNER JOIN `itens_de_vendas` ON itens_de_vendas.`vendas_id` = vendas.`idVendas`
		 INNER JOIN `produto` ON `produto`.`produto_id` = itens_de_vendas.`produtos_id`
		 WHERE `venda_visivel` = 1
		 AND `itens_de_vendas`.`imei_valor` <> '' 
		-- AND `produto_categoria_id`  = 27 ");

		return $query->result();


	}

	public function totalEstoque()
	{
		// ESTOQUE POR CATEGORIA  
		$query = $this->db->query(" SELECT SUM(produto_estoque)  AS Total FROM `produto`
		 WHERE `produto_visivel` = 1
		 AND `produto_estoque` > 0
		 AND `produto_categoria_id`  IN (26,27,28,30,31)");

		return $query->result();
	}

	public function totalImei()
	{
		// IMEI POR CATEGORIA  
		$query = $this->db->query(" SELECT COUNT(`imei_valor`) AS Total FROM `itens_de_imei`
		  INNER JOIN `produto` ON itens_de_imei.`produtos_id` = `produto`.`produto_id`
		  WHERE imei_visivel = 1 AND vendas_id IS NULL  
		  -- AND  `produto_categoria_id` = 27 ");

		return $query->result();
	}
		
		
		
	 //////////////////////// DASHBOARD V2 /////////////////////////////


    public function resumoFinanceiro()
	{

		$query = $this->db->query(" SELECT SUM(valor) AS valor, financeiro_tipo FROM view_com_rec_des_ano 
									WHERE mes = MONTH(CURDATE())
									GROUP BY `financeiro_tipo`
									ORDER BY financeiro_tipo  ");

		return $query->result();

	}


	public function resumoFinanceiroDia()
	{

		$query = $this->db->query(" SELECT dia, SUM(valor) AS valor, mes, financeiro_tipo  FROM view_com_rec_des_dia
									WHERE  dia > DAY(CURRENT_DATE)-10
									GROUP BY dia, financeiro_tipo
									ORDER BY dia DESC
									LIMIT 7 ");

		return $query->result();

	}



	public function resumoFinanceiroReceitaDia()
	{

		$query = $this->db->query(" SELECT dia, SUM(valor) AS valor, mes, financeiro_tipo  FROM view_com_rec_des_dia
									WHERE financeiro_tipo = 'receita'
									AND dia > DAY(CURRENT_DATE)-10
									GROUP BY dia
									order by dia desc
									LIMIT 7 ");

		return $query->result();

	}


	public function resumoFinanceiroCompraDia()
	{

		$query = $this->db->query(" SELECT dia, SUM(valor) AS valor, mes, financeiro_tipo  FROM view_com_rec_des_dia
									WHERE financeiro_tipo = 'compras'
									AND dia > DAY(CURRENT_DATE)-10
									GROUP BY dia
									order by dia desc
									LIMIT 7 ");

		return $query->result();

	}


	public function resumoFinanceiroDespesaDia()
	{

		$query = $this->db->query(" SELECT dia, SUM(valor) AS valor, mes, financeiro_tipo  FROM view_com_rec_des_dia
									WHERE financeiro_tipo = 'despesa'
									AND dia > DAY(CURRENT_DATE)-10
									GROUP BY dia
									order by dia desc
									LIMIT 7 ");

		return $query->result();

	}

   
   	public function VendasAnoDashboardv2()
	{
		$this->db->select('*');
		return $this->db->get('view_vendas_ano')->result();
		
	}


	public function VendasMesDashboardv2()
	{
		$this->db->select('*');
		return $this->db->get('view_vendas_mes')->result();
		
	}


	public function VendasDiaDashboardv2()
	{
		$this->db->select('*');
		return $this->db->get('view_vendas_dia')->result();
		
	}

	public function vendasAnoDetalhe($loja)
    {     
        $query = " SELECT loja, ano, SUM(`qtVendas`) AS qtVendas,  SUM(total) AS total, SUM(custo) AS custo, SUM(lucro) AS lucro, ROUND(AVG(`percentual`),2) AS percentual
                     FROM view_vendas_{$loja} 
                    GROUP BY ano 
                     ORDER BY ano DESC    ";


        return $this->db->query($query)->result();
    }

	public function vendasMesDetalhe($loja)
    {
	    $query = " SELECT * FROM view_vendas_{$loja} WHERE ano = YEAR(CURDATE()) ORDER BY mes DESC  ";
	    return $this->db->query($query)->result();
	}

	public function valorEstoque()
    {   
        $query = "  SELECT * FROM `view_valor_estoque` ";
        return $this->db->query($query)->result();
    }

    public function valorEstoqueGrupo($loja)
    {   
        
        $query = " SELECT view_valor_estoque_grupo.* ,
                   ROUND( ( ( (`produto_preco_venda`/`produto_preco_custo`) *100) -100) ,2) AS percentual
                   FROM `view_valor_estoque_grupo` where loja = '{$loja}' ";


        return $this->db->query($query)->result();
    }

	public function maisVendidoT()
    {     

        $query = "  SELECT loja, SUM(qtd) AS qtd, SUM(venda_media*qtd) AS valorTotal, SUM(custo_medio*qtd) AS custoTotal,
                    ROUND( ( (  (  (SUM(`venda_media`)/SUM(`custo_medio`)) *100) -100) /100) ,2) AS percentual, 
                    label 
                    FROM view_prod_mais_vendidos_trimestre 
                    GROUP BY loja ";


        return $this->db->query($query)->result();
    }

    public function maisVendido()
    {     

        $query = "  SELECT loja, SUM(qtd) AS qtd, SUM(venda_media*qtd) AS valorTotal, SUM(custo_medio*qtd) AS custoTotal,
                    ROUND( ( (  (  (SUM(`venda_media`)/SUM(`custo_medio`)) *100) -100) /100) ,2) AS percentual, 
                    label 
                    FROM view_prod_mais_vendidos_mes 
                    GROUP BY loja ";


        return $this->db->query($query)->result();
    }

	public function maisVendidoS()
    {     

        $query = "  SELECT loja, SUM(qtd) AS qtd, SUM(venda_media*qtd) AS valorTotal, SUM(custo_medio*qtd) AS custoTotal,
                    ROUND( ( (  (  (SUM(`venda_media`)/SUM(`custo_medio`)) *100) -100) /100) ,2) AS percentual, 
                    label 
                    FROM view_prod_mais_vendidos_semestre 
                    GROUP BY loja ";

        return $this->db->query($query)->result();
    }

    public function maisVendidoDetalheS($loja)
    {   
        
        $query = "  SELECT view_prod_mais_vendidos_semestre.*, (venda_media - custo_medio) AS lucro_medio,
                    ROUND( ( ( (`venda_media`/`custo_medio`) *100) -100) ,2) AS percentual
                    FROM `view_prod_mais_vendidos_semestre`
                    WHERE loja = '{$loja}' ORDER BY `qtd` DESC  ";


        return $this->db->query($query)->result();
    }

    public function maisVendidoDetalheT($loja)
    {   
        
        $query = "  SELECT view_prod_mais_vendidos_trimestre.*, (venda_media - custo_medio) AS lucro_medio,
                    ROUND( ( ( (`venda_media`/`custo_medio`) *100) -100) ,2) AS percentual
                    FROM `view_prod_mais_vendidos_trimestre`
                    WHERE loja = '{$loja}' ORDER BY `qtd` DESC  ";


        return $this->db->query($query)->result();
    }

    public function maisVendidoDetalhe($loja)
    {   
        
        $query = "  SELECT view_prod_mais_vendidos_mes.*, (venda_media - custo_medio) AS lucro_medio,
                    ROUND( ( ( (`venda_media`/`custo_medio`) *100) -100) ,2) AS percentual
                    FROM `view_prod_mais_vendidos_mes`
                    WHERE loja = '{$loja}' ORDER BY `qtd` DESC  ";


        return $this->db->query($query)->result();
    }

    public function maisParadoS()
    {     

        $query = " SELECT loja, SUM(estoque) AS qtd, SUM(valorTotal) AS valorTotal, SUM(custoTotal) AS custoTotal,
                    ROUND( ( (  (  (SUM(`valorTotal`)/SUM(`custoTotal`)) *100) -100) /100) ,2) AS percentual, 
                    label 
                    FROM view_prod_parado_semestre 
                    GROUP BY loja ";


        return $this->db->query($query)->result();
    }   

    public function maisParadoT()
    {     

        $query = " SELECT loja, SUM(estoque) AS qtd, SUM(valorTotal) AS valorTotal, SUM(custoTotal) AS custoTotal,
                    ROUND( ( (  (  (SUM(`valorTotal`)/SUM(`custoTotal`)) *100) -100) /100) ,2) AS percentual, 
                    label 
                    FROM view_prod_parado_trimestre 
                    GROUP BY loja ";


        return $this->db->query($query)->result();
    }

    public function maisParado()
    {     

        $query = " SELECT loja, SUM(estoque) AS qtd, SUM(valorTotal) AS valorTotal, SUM(custoTotal) AS custoTotal,
                    ROUND( ( (  (  (SUM(`valorTotal`)/SUM(`custoTotal`)) *100) -100) /100) ,2) AS percentual, 
                    label 
                    FROM view_prod_parado_mes 
                    GROUP BY loja ";


        return $this->db->query($query)->result();
    }

    public function maisParadoDetalheS($loja)
    {     

        $query = " SELECT view_prod_parado_semestre.*, (valorTotal - custoTotal ) AS lucro, DATE_FORMAT(produto_data_ultima_venda,'%d/%m/%Y') AS produto_data_ultima_venda
                   FROM `view_prod_parado_semestre` 
                   WHERE loja = '{$loja}' ORDER BY `estoque` DESC   ";


        return $this->db->query($query)->result();
    }

    public function maisParadoDetalheT($loja)
    {     

        $query = " SELECT view_prod_parado_trimestre.*, (valorTotal - custoTotal ) AS lucro, DATE_FORMAT(produto_data_ultima_venda,'%d/%m/%Y') AS produto_data_ultima_venda
                   FROM `view_prod_parado_trimestre` 
                   WHERE loja = '{$loja}' ORDER BY `estoque` DESC   ";


        return $this->db->query($query)->result();
    }

    public function maisParadoDetalhe($loja)
    {     

        $query = " SELECT view_prod_parado_mes.*, (valorTotal - custoTotal ) AS lucro, DATE_FORMAT(produto_data_ultima_venda,'%d/%m/%Y') AS produto_data_ultima_venda
                   FROM `view_prod_parado_mes` 
                   WHERE loja = '{$loja}' ORDER BY `estoque` DESC   ";


        return $this->db->query($query)->result();
    }

	public function previsaoFinanceiroAno()
    {
        $query = " SELECT loja, label, `ano`,
                 SUM(dias_lancamento) AS dias_lancamento, SUM(receita) AS receita, SUM(despesa) AS despesa, SUM(custo) AS custo,
                 ROUND(AVG(media),2) AS media, 
                 ROUND( ( (  (  (SUM(`receita`)/SUM(`custo`)) *100) -100) /100) ,2) AS percentual FROM view_financeiro_previsao
                    WHERE
                    ano = YEAR(CURDATE())
                    GROUP BY loja  ";

        return $this->db->query($query)->result();
    }

	public function previsaoFinanceiro()
    {
        $query = " SELECT view_financeiro_previsao.* ,ROUND( ( (  (  (SUM(`receita`)/SUM(`custo`)) *100) -100) /100) ,2) AS percentual FROM view_financeiro_previsao
                    WHERE mes = MONTH(CURDATE())
                    AND ano = YEAR(CURDATE())
                    GROUP BY loja ";

        return $this->db->query($query)->result();
    }

    public function previsaoFinanceiroDetalhe($loja)
    {
        $query = " SELECT *  FROM view_financeiro_previsao
                    WHERE mes = MONTH(CURDATE())
                    AND ano = YEAR(CURDATE())
                    AND loja = '{$loja}' ";

        return $this->db->query($query)->result();
    }

    public function previsaoFinanceiroDetalheAno($loja)
    {
        $query = " SELECT loja, label, `ano`,
                 SUM(dias_lancamento) AS dias_lancamento, SUM(receita) AS receita, SUM(despesa) AS despesa, SUM(custo) AS custo,
                 ROUND(AVG(media),2) AS media, 
                 ROUND( ( (  (  (SUM(`receita`)/SUM(`custo`)) *100) -100) /100) ,2) AS percentual,
                 ROUND(((0.0 * SUM(`receita`))/100),2) AS comissao 
                 FROM view_financeiro_previsao
                WHERE loja = '{$loja}'
                GROUP BY loja, ano 
                ORDER BY ano DESC  ";

        return $this->db->query($query)->result();
    }

	public function top10Mes()
    {
        $query = " SELECT loja, SUM(qtdVenda) AS qtd, SUM(valorTotal) AS valorTotal, SUM(custoTotal) AS custoTotal,
                    ROUND( ( (  (  (SUM(`valorTotal`)/SUM(`custoTotal`)) *100) -100) /100) ,2) AS percentual, 
                    label 
                    FROM view_clie_top10_mes 
                    GROUP BY loja   ";

        return $this->db->query($query)->result();

    }

    public function top10Ano()
    {
        $query = "  SELECT loja, SUM(qtdVenda) AS qtd, SUM(valorTotal) AS valorTotal, SUM(custoTotal) AS custoTotal,
                    ROUND( ( ( ( (SUM(`valorTotal`)/SUM(`custoTotal`)) *100) -100) /100) ,2) AS percentual, 
                    label 
                    FROM view_clie_top10_ano 
                    GROUP BY loja    ";

        return $this->db->query($query)->result();

    }

    public function top10MesDetalhe($loja)
    {
        $query = "  SELECT view_clie_top10_mes.*, (valorTotal - custoTotal) AS lucro, 
                    ROUND( ( ( (`valorTotal`/`custoTotal`) *100) -100) ,2) AS percentual
                    FROM `view_clie_top10_mes` where loja = '{$loja}' ORDER BY `valorTotal` DESC  ";

        return $this->db->query($query)->result();

    }

    public function top10AnoDetalhe($loja)
    {
        $query = "  SELECT view_clie_top10_ano.*, (valorTotal - custoTotal) AS lucro, 
                    ROUND( ( ( (`valorTotal`/`custoTotal`) *100) -100) ,2) AS percentual
                    FROM `view_clie_top10_ano` where loja = '{$loja}' ORDER BY `valorTotal` DESC  ";

        return $this->db->query($query)->result();

    }

    public function vendedortop10Ano()
    {
        $query = " SELECT loja, SUM(qtdVenda) AS qtd, SUM(valorTotal) AS valorTotal, SUM(custoTotal) AS custoTotal,
                    ROUND( ( (  (  (SUM(`valorTotal`)/SUM(`custoTotal`)) *100) -100) /100) ,2) AS percentual, 
                    label 
                    FROM `view_func_top10_ano` 
                    GROUP BY loja ";
        return $this->db->query($query)->result();
    }

    public function vendedortop10Mes()
    {
        $query = " SELECT loja, SUM(qtdVenda) AS qtd, SUM(valorTotal) AS valorTotal, SUM(custoTotal) AS custoTotal,
                    ROUND( ( (  (  (SUM(`valorTotal`)/SUM(`custoTotal`)) *100) -100) /100) ,2) AS percentual, 
                    label 
                    FROM `view_func_top10_mes` 
                    GROUP BY loja ";
        return $this->db->query($query)->result();
    }

    public function vendedortop10Dia()
    {
        $query = " SELECT loja, SUM(qtdVenda) AS qtd, SUM(valorTotal) AS valorTotal, SUM(custoTotal) AS custoTotal,
                    ROUND( ( (  (  (SUM(`valorTotal`)/SUM(`custoTotal`)) *100) -100) /100) ,2) AS percentual, 
                    label 
                    FROM `view_func_top10_dia` 
                    GROUP BY loja ";
        return $this->db->query($query)->result();
    }

    public function vendedortop10AnoDetalhe($loja)
    {
        $query = "  SELECT view_func_top10_ano.*, (valorTotal - custoTotal) AS lucro,
                    ROUND( ( ( (`valorTotal`/`custoTotal`) *100) -100) ,2) AS percentual
                    FROM `view_func_top10_ano`
                    WHERE loja = '{$loja}' ORDER BY `valorTotal` DESC  ";

        return $this->db->query($query)->result();

    }

    public function vendedortop10DiaDetalhe($loja)
    {
        $query = "  SELECT view_func_top10_dia.*, (valorTotal - custoTotal) AS lucro,
                    ROUND( ( ( (`valorTotal`/`custoTotal`) *100) -100) ,2) AS percentual 
                    FROM `view_func_top10_dia`
                    WHERE loja = '{$loja}'  ORDER BY `valorTotal` DESC ";

        return $this->db->query($query)->result();

    }

    public function vendedortop10MesDetalhe($loja)
    {
        $query = "  SELECT view_func_top10_mes.*, (valorTotal - custoTotal) AS lucro,
                    ROUND( ( ( (`valorTotal`/`custoTotal`) *100) -100) ,2) AS percentual
                    FROM `view_func_top10_mes` 
                    WHERE loja = '{$loja}'  ORDER BY `valorTotal` DESC  ";

        return $this->db->query($query)->result();

    }
	

	public function vendedoresCelCustomizadas()
	{		
	   		
	    $this->db->select('	    	
	    COUNT(DISTINCT `itens_de_vendas`.`imei_valor`) AS qtdItens,
	    ROUND(SUM(itens_de_vendas.`subTotal`), 2) AS total,
        ROUND(SUM(`itens_de_vendas`.`prod_preco_custo` * `quantidade`), 2) AS  custo, 
        `usuarios`.`usuario_nome` AS vendedor ');

	    $this->db->join('usuarios ','`vendas`.`usuarios_id` = `usuarios`.`usuario_id` ');
	    $this->db->join('`itens_de_vendas` ','`itens_de_vendas`.`vendas_id` = vendas.`idVendas` ');
	    $this->db->where('venda_visivel',1);
	    $this->db->where('`itens_de_vendas`.`imei_valor` <> ""');
	    $this->db->where('dataVenda = CURDATE()');
 

        $this->db->group_by('`vendas`.`usuarios_id`');
	    $this->db->order_by('total', 'desc');

        return $this->db->get('vendas')->result_array();
	}


}