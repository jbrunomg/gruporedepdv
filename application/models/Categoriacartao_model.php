<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Categoriacartao_model extends CI_Model
{

    public $tabela  = "categoria_cartao";
    public $visivel = "categoria_cart_visivel";
    public $chave   = "categoria_cart_id";

    public function listar()
    {
        $this->db->select("*");
        $this->db->where($this->visivel, 1);

        return $this->db->get($this->tabela)->result();
    }

    public function listarDescricao()
    {
        $this->db->select("categoria_cart_descricao, categoria_cart_id, categoria_cart_numero_parcela, categoria_cart_percentual_parcela, categoria_cart_nome_credenciadora");
        $this->db->where($this->visivel, 1);

        return $this->db->get($this->tabela)->result();
    }

    public function adicionar($dados)
    {
        $this->db->insert($this->tabela, $dados);

        if ($this->db->affected_rows() == '1') {
            return TRUE;
        }

        return FALSE;
    }

    public function excluir($campo, $dados, $id)
    {
        $this->db->where($campo, $id);
        $this->db->update($this->tabela, $dados);

        if ($this->db->affected_rows() > '1') {
            return true;
        }

        return false;
    }

    public function getCartaoCategoria($id)
    {
        $this->db->select("*");
        $this->db->where($this->chave, $id);
        $this->db->where($this->visivel, 1);

        return $this->db->get($this->tabela)->result();
    }

    public function editar($dados, $id)
    {
        $this->db->where($this->chave, $id);
        $this->db->update($this->tabela, $dados);

        if ($this->db->affected_rows() == '1') {
            return TRUE;
        }

        return FALSE;
    }
}
