<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Compra_model extends CI_Model {

    public function adicionar($dados)
    {
        $this->db->insert('simulacao_compra', $dados);

        if ($this->db->affected_rows() == '1')
        {
            //return TRUE;
            return $this->db->insert_id('simulacao_compra');
        }
        
        return FALSE; 
    }

    public function listarCategoriaProduto()
    {
        $this->db->select("*");
        $this->db->where('categoria_prod_visivel', 1);

        return $this->db->get('categoria_produto')->result();
    }

    public function prepararItens($categoria_id, $id_compra)
    {

        $sql =' INSERT INTO itens_simulacao_compra (itens_simulacao_compra.produtos_id, itens_simulacao_compra.produtos_codigo, itens_simulacao_compra.simulacao_compra_id)
                SELECT produto.produto_id, produto.produto_codigo, '.$id_compra.' FROM produto WHERE produto_categoria_id = '.$categoria_id;
                
        
        return $this->db->query($sql);
    }

    public function listarId($id, $dataInicio, $dataFim, $categoriaId, $orderBy = 'produto_descricao')
    {
        $sql = '
        
            SELECT DISTINCT * FROM (
                
                SELECT 
                    `produto`.`produto_id`,
                    `produto`.`produto_codigo`,
                    `produto`.`produto_descricao`, 
                    `produto`.produto_preco_custo,
                    `produto`.`produto_estoque`,
                    
                    `itens_simulacao_compra`.`quantidade`,
                    
                     SUM(`itens_de_vendas`.`quantidade` ) AS vendidos,
                     
                     usuarios.`usuario_nome` AS funcionario
                    
                FROM produto

                INNER JOIN `itens_de_vendas` 
                ON `produto`.`produto_id` = `itens_de_vendas`.`produtos_id`

                INNER JOIN `vendas` 
                ON vendas.`idVendas` = `itens_de_vendas`.`vendas_id`

                INNER JOIN itens_simulacao_compra
                ON itens_simulacao_compra.`produtos_id` = produto.produto_id AND itens_simulacao_compra.`simulacao_compra_id`  = '.$id.'

                INNER JOIN simulacao_compra
                ON itens_simulacao_compra.`simulacao_compra_id` = simulacao_compra.`simulacao_compra_id`

                LEFT JOIN usuarios
                ON usuarios.`usuario_id` = itens_simulacao_compra.`simulacao_compra_usuario_id`

                WHERE `produto`.`produto_categoria_id` = '.$categoriaId.' 
				AND produto.`produto_visivel` = 1 
				AND vendas.`venda_visivel` = 1 
				AND simulacao_compra.`simulacao_compra_visivel` = 1 
                
                AND vendas.`dataVenda` BETWEEN "'.$dataInicio.'" AND "'.$dataFim.'"
                
                GROUP BY produto.`produto_id` 
                
                UNION ALL 
                
                SELECT 
                    `produto`.`produto_id`,
                    `produto`.`produto_codigo`,
                    `produto`.`produto_descricao`, 
                    `produto`.produto_preco_custo,
                    `produto`.`produto_estoque`,
                    `itens_simulacao_compra`.`quantidade`,
                    0 AS vendidos,
                     
                     usuarios.`usuario_nome` AS funcionario
                    
                FROM produto          

                INNER JOIN itens_simulacao_compra
                ON itens_simulacao_compra.`produtos_id` = produto.produto_id AND itens_simulacao_compra.`simulacao_compra_id`  = '.$id.'

                INNER JOIN simulacao_compra
                ON itens_simulacao_compra.`simulacao_compra_id` = simulacao_compra.`simulacao_compra_id`

                LEFT JOIN usuarios
                ON usuarios.`usuario_id` = itens_simulacao_compra.`simulacao_compra_usuario_id`

                WHERE `produto`.`produto_categoria_id` = '.$categoriaId.' AND produto.`produto_visivel` = 1 AND simulacao_compra.`simulacao_compra_visivel` = 1


                ) AS produto GROUP BY  produto.`produto_id`

          
                ORDER BY `produto`.'.$orderBy;                   

               // v($sql);

        return $this->db->query($sql)->result();
    }

    public function consultarCompra($id)
    {
        $this->db->select('*, categoria_produto.categoria_prod_descricao AS categoria_descricao');
        $this->db->where('simulacao_compra_visivel', 1);
        $this->db->where('simulacao_compra_id', $id);

        $this->db->join('categoria_produto', 'simulacao_compra.simulacao_compra_categoria_id = categoria_produto.categoria_prod_id');

        $result = $this->db->get('simulacao_compra')->result();

        if($result){
            return $result[0];
        } else {
            return false;
        }
    }

    public function atualizaQuantidadeProdutos($dados, $id)
    {
        $this->db->where('simulacao_compra_id', $id);
        $this->db->where('produtos_id', $dados['produtos_id']);

        $this->db->set('quantidade', $dados['quantidade']);
        $this->db->set('simulacao_compra_usuario_id', $dados['simulacao_compra_usuario_id']);

        $this->db->update('itens_simulacao_compra');

        if($this->db->affected_rows() == '1')
        {
            return true;
        }

        return false;
    }

    public function listarCompras()
    {
        $sql = 'SELECT
                    simulacao_compra.`simulacao_compra_id` AS id,
                    DATE_FORMAT(simulacao_compra.`simulacao_compra_vendas_data_inicio`, "%d/%m/%Y") AS data_inicio,
                    DATE_FORMAT(simulacao_compra.`simulacao_compra_vendas_data_final`,"%d/%m/%Y") AS data_final,
                    simulacao_compra.`simulacao_compra_situacao` AS situacao,
                    categoria_produto.`categoria_prod_descricao` AS categoria,
                    COUNT(itens_simulacao_compra.`produtos_id`) AS produtos_totais,
                    COUNT(CASE WHEN itens_simulacao_compra.`quantidade` > 0 THEN itens_simulacao_compra.`produtos_id` END) produtos_adicionados

                FROM simulacao_compra

                INNER JOIN categoria_produto
                ON simulacao_compra.`simulacao_compra_categoria_id` = categoria_produto.`categoria_prod_id`

                INNER JOIN itens_simulacao_compra
                ON simulacao_compra.`simulacao_compra_id` = itens_simulacao_compra.`simulacao_compra_id`

                WHERE simulacao_compra.`simulacao_compra_visivel` = 1

                GROUP BY simulacao_compra.`simulacao_compra_id`

                ORDER BY simulacao_compra.`simulacao_compra_id` DESC';

        return $this->db->query($sql)->result();
    }

    public function excluir($dados, $id)
    {
        $this->db->where('simulacao_compra_id',$id);

        $this->db->update('simulacao_compra',$dados);
        
        if($this->db->affected_rows() == '1')
        {
            return true;
        }

        return false;
    }

    public function prepararCompra($idCompra)
    {
        $this->db->where('simulacao_compra_id', $idCompra);

        $this->db->set('simulacao_compra_situacao', 'Preparada');

        $this->db->update('simulacao_compra');

        if($this->db->affected_rows() == '1')
        {
            return true;
        }

        return false;
    }

    public function excluirItensCompra($idCompra){
        
        $this->db->where('simulacao_compra_id', $idCompra);
        $this->db->where('quantidade <= ', 0);

        $this->db->delete('itens_simulacao_compra');

        if($this->db->affected_rows() >= 1)
        {
            return true;
        }

        return false;
    }


    // public function carregarProdutosPorCategoria($idCategoria){
    // 	$this->db->select("*");
    //     $this->db->where('produto_visivel', 1);
    //     $this->db->where('produto_categoria_id', $idCategoria);

    //     return $this->db->get('produto')->result();

    // }
}