<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parametro_model extends CI_Model {
	
	public $tabela  = "parametro";	
	public $chave   = "parametro_id";


	public function editar($dados)
	{

	    $sql = "SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE '%_".GRUPOLOJA."_%' ";
        $bases = $this->db->query($sql)->result();;
        
        foreach ($bases as $b) {
			$this->db->where($this->chave,1);
			$this->db->update($b->schema_name.'.'.$this->tabela,$dados);

        }

	    if($this->db->trans_status() === true){
	        $this->db->trans_commit();
	        return true;
	    }else{
	        $this->db->trans_rollback();
	        return false;
	    }

	}


	public function listarId($id)
    {
        $this->db->select("*");
        $this->db->where($this->chave, $id);        
        return $this->db->get($this->tabela)->result();
    }

    public function updateMassa($debito, $credito){

        $sql = "SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE '%_".GRUPOLOJA."_%' ";
        $bases = $this->db->query($sql)->result();
        
        foreach ($bases as $b) {

        $sqlAtualizar = "UPDATE ".$b->schema_name.".produto set produto_preco_cart_debito = produto_preco_venda + ((produto_preco_venda/100) * ?), 
        produto_preco_cart_credito =  produto_preco_venda + ((produto_preco_venda/100) * ?)";
        $this->db->query($sqlAtualizar, array($debito,$credito));

		}

	    if($this->db->trans_status() === true){
	        $this->db->trans_commit();
	        return true;
	    }else{
	        $this->db->trans_rollback();
	        return false;
	    }

    }


    public function editarChat($dados)
	{

	    $this->db->where($this->chave,1);	
		
		if($this->db->update($this->tabela,$dados))
		{
			return true;
		}

		return false;

	}


}